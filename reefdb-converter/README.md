${project.name} Help
-

Command line usage :

To show help (list all options) :
 > launch.bat -h

Convert an old version of context file (including filters) to new version :
 > launch.bat --convert-context <context_file.dat>

Convert an old version of filter file to new version :
 > launch.bat --convert-filter <filter_file.dat>

Note: the file path can be absolute or relative to the current directory