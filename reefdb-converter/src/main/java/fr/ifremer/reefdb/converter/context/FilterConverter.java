package fr.ifremer.reefdb.converter.context;

/*-
 * #%L
 * Reef DB :: Converter
 * %%
 * Copyright (C) 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.gson.Gsons;
import fr.ifremer.reefdb.converter.ConverterMain;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author peck7 on 18/09/2019.
 */
public class FilterConverter extends AbstractContextConverter<FilterDTO> {

    public static final String COMMAND = "--convert-filter";

    public FilterConverter(List<String> options) {
        super(options);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected List<FilterDTO> importOldFormat(Path file) {

        try (FileInputStream fis = new FileInputStream(file.toFile());
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            List<FilterDTO> filters = (List<FilterDTO>) ois.readObject();

            filters.forEach(filter -> filter.setId(null));

            return filters;

        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(ConverterMain.getString("converter.exception", e.getLocalizedMessage()), e);
        }
    }

    @Override
    protected void exportNewFormat(List<FilterDTO> filters, Path file) {

        FilterProxy filtersToExport = new FilterProxy(filters.stream().map(this::toFilterVO).collect(Collectors.toList()));
        Gsons.serializeToFile(filtersToExport, file.toFile());

    }

}
