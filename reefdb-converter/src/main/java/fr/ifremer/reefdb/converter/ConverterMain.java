package fr.ifremer.reefdb.converter;

/*-
 * #%L
 * Reef DB :: Converter
 * %%
 * Copyright (C) 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.converter.context.ContextConverter;
import fr.ifremer.reefdb.converter.context.FilterConverter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.*;

/**
 * Launcher application that do post-update logic, launch application and listen to exit code
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class ConverterMain {

    private static ResourceBundle bundle;
    private static Path baseDir;

    /**
     * <p>main.</p>
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<String> arguments = new ArrayList<>();
        if (args != null) {
            arguments.addAll(Arrays.asList(args));
        }

        // Get the resource bundle
        initBundle();
        System.out.println(getString("converter.title"));

        // Get base dir (from where the exe file is launched)
        baseDir = Paths.get(System.getProperty("user.dir"));

        ConverterMain converterMain = new ConverterMain();
        converterMain.execute(arguments);
    }

    /**
     * <p>execute.</p>
     *
     * @param arguments a {@link java.util.List} object.
     */
    private void execute(List<String> arguments) {

        if (arguments.isEmpty() || arguments.contains("/?") || arguments.contains("-?") || arguments.contains("/h") || arguments.contains("-h")) {
            System.out.println(getString("converter.help"));
            System.exit(0);
        }

        String command = arguments.remove(0);
        Runnable action = null;

        switch (command) {
            case ContextConverter.COMMAND:
                action = new ContextConverter(arguments);
                break;
            case FilterConverter.COMMAND:
                action = new FilterConverter(arguments);
                break;
        }

        if (action == null) {
            System.err.println("unknown command " + command);
            System.exit(1);
        }

        action.run();
    }

    private static void initBundle() {
        if (bundle == null)
            bundle = ResourceBundle.getBundle("i18n/converter", new UTF8Control());
    }

    public static String getString(String resourceName, Object... params) {
        initBundle();
        return MessageFormat.format(bundle.getString(resourceName), params);
    }

    public static Path getBaseDir() {
        return baseDir;
    }

    private static class UTF8Control extends ResourceBundle.Control {

        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IOException {
            // The below is a copy of the default implementation.
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try {
                    // Only this line is changed to make it to read properties files as UTF-8.
                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, StandardCharsets.UTF_8));
                } finally {
                    stream.close();
                }
            }
            return bundle;
        }

    }
}
