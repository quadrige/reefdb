package fr.ifremer.reefdb.converter.context;

/*-
 * #%L
 * Reef DB :: Converter
 * %%
 * Copyright (C) 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.converter.ConverterMain;
import fr.ifremer.reefdb.dto.CodeOnly;
import fr.ifremer.reefdb.dto.ReefDbBean;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author peck7 on 19/09/2019.
 */
public abstract class AbstractContextConverter<E extends Serializable> implements Runnable {

    private final List<String> options;

    AbstractContextConverter(List<String> options) {
        this.options = options;
    }

    @Override
    public void run() {

        if (CollectionUtils.isEmpty(options))
            throw new IllegalArgumentException(ConverterMain.getString("converter.optionMissing"));

        String fileOption = options.get(0);
        Path inputFile = Paths.get(fileOption);
        if (!inputFile.isAbsolute()) {
            inputFile = ConverterMain.getBaseDir().resolve(fileOption);
        }
        if (!Files.isRegularFile(inputFile))
            throw new IllegalArgumentException(ConverterMain.getString("converter.fileNotExists", inputFile));

        Path outputFile = inputFile.resolveSibling(inputFile.getFileName().toString().replaceFirst("(.*?)(\\.[^.]+)?$", "$1 (new format)$2"));
        if (Files.exists(outputFile))
            throw new IllegalArgumentException(ConverterMain.getString("converter.fileAlreadyExists", outputFile));

        // import from old format
        List<E> objects = importOldFormat(inputFile);

        // export to new format
        exportNewFormat(objects, outputFile);

        System.out.println(ConverterMain.getString("converter.fileCreated", outputFile));
    }

    protected abstract List<E> importOldFormat(Path file);

    protected abstract void exportNewFormat(List<E> objects, Path file);

    ContextVO toContextVO(ContextDTO context) {
        ContextVO vo = new ContextVO();
        vo.setName(context.getName());
        vo.setDescription(context.getDescription());
        vo.setFilters(context.getFilters().stream().map(this::toFilterVO).collect(Collectors.toList()));
        return vo;
    }

    FilterVO toFilterVO(FilterDTO filter) {
        FilterVO vo = new FilterVO();
        vo.setType(filter.getFilterTypeId());
        vo.setName(filter.getName());
        vo.setElementIds(getIdsAsString(filter.getElements()));
        return vo;
    }

    private List<String> getIdsAsString(List<? extends ReefDbBean> beans) {
        if (beans == null)
            return null;

        return beans.stream().map(bean -> bean instanceof CodeOnly ? ((CodeOnly) bean).getCode() : bean.getId().toString()).collect(Collectors.toList());
    }

}
