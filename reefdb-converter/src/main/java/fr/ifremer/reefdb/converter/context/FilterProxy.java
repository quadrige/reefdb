package fr.ifremer.reefdb.converter.context;

/*-
 * #%L
 * Reef DB :: Converter
 * %%
 * Copyright (C) 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

/**
 * Proxy used to Serialize/Deserialize Filters
 *
 * @author peck7 on 18/09/2019.
 */
public class FilterProxy implements Serializable {

    public static final String CURRENT_VERSION = "1.0";
    private final String version;
    private final List<? extends FilterVO> filters;

    public FilterProxy(List<? extends FilterVO> filters) {
        this.version = CURRENT_VERSION;
        this.filters = filters;
    }

    public final String getVersion() {
        return version;
    }

    public List<? extends FilterVO> getFilters() {
        return filters;
    }

}
