Pour démarrer ReefDb
--------------------

# Sous Linux

./ReefDb.sh

# Sous windows

ReefDb.exe

Consulter l'aide
----------------

En attendant que l'aide soit finalisée, vous pouvez consulter le site : 

http://www.ifremer.fr/maven/reports/quadrige2/reefdb/
