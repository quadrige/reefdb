#!/bin/bash

READLINK=`which readlink`
if [ -z "$READLINK"  ]; then
  message "Required tool 'readlink' is missing. Please install before launch \"$0\" file."
  exit 1
fi

# ------------------------------------------------------------------
# Ensure BASEDIR points to the directory where the soft is installed.
# ------------------------------------------------------------------
SCRIPT_LOCATION=$0
if [ -x "$READLINK" ]; then
  while [ -L "$SCRIPT_LOCATION" ]; do
    SCRIPT_LOCATION=`"$READLINK" -e "$SCRIPT_LOCATION"`
  done
fi

export REEFDB_BASEDIR=`dirname "$SCRIPT_LOCATION"`
export REEFDB_HOME=$REEFDB_BASEDIR/reefdb
export REEFDB_CONFIG_DIR=$REEFDB_BASEDIR/config
export JAVA_HOME=$REEFDB_BASEDIR/jre
export JAVA_COMMAND=$JAVA_HOME/bin/java

cd $REEFDB_BASEDIR


# add executable permission to java command
chmod +x $JAVA_COMMAND

while true; do

  echo "Run Updater"
  cp -f $REEFDB_HOME/updater.jar .
  $JAVA_COMMAND -jar updater.jar
  exitcode=$?
  rm -fv updater.jar
  if [ "$exitcode" -eq  "90" ]; then
    # quit now!
    exit $exitcode
  fi

  echo "Read launcher properties"
  . $REEFDB_HOME/launcher.properties
  . launcher.properties

  echo "Running Reef DB..."
  echo "   basedir: $REEFDB_BASEDIR"
  echo "  app home: $REEFDB_HOME"
  echo "  jre home: $JAVA_HOME"
  echo "  jvm opts: $JAVA_OPTS"
  echo "       log: $LOGFILE"

  $JAVA_COMMAND $JAVA_OPTS -Dreefdb.log.file=$LOGFILE -jar $JAR --option reefdb.launch.mode full --option reefdb.basedir $REEFDB_BASEDIR --option config.path $REEFDB_CONFIG_DIR
  exitcode=$?
  echo "Stop Reef DB with exitcode: $exitcode"

  if [ "$exitcode" -eq  "89" ]; then
    # delete db directory and restart
    rm -rfv data/db
    rm -rfv data/dbcache
    rm -rfv data/dbconf
    exitcode=88
  fi

  if [ ! "$exitcode" -eq  "88" ]; then
    # quit now!
    exit $exitcode
  fi
done
