#!/bin/bash

export BASE_DIR=$(pwd)
export APP_DIR=$BASE_DIR/application
export JAVA_HOME=$BASE_DIR/jre
export JAVA_COMMAND=$JAVA_HOME/bin/java

cd $BASE_DIR

echo "basedir:  $BASE_DIR"
echo "application home: $APP_DIR"
echo "jre home: $JAVA_HOME"

echo "Read launcher properties"
. $APP_DIR/launcher.properties
. launcher.properties

echo "Run Application in silent mode to export database"
$JAVA_COMMAND $JAVA_OPTS -Dreefdb.log.file=$LOGFILE -jar $JAR --option quadrige3.launch.mode silent --export-db --option reefdb.basedir $BASE_DIR --option config.path $BASE_DIR/config
exitcode=$?
echo "Stop with exitcode: $exitcode"

cd $BASE_DIR
  
  