package fr.ifremer.reefdb.ui.swing.action;

/*-
 * #%L
 * ReefDb :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peck7 on 04/07/2017.
 */
public abstract class AbstractReefDbSaveAction<M extends AbstractBeanUIModel, UI extends ReefDbUI<M, ?>, H extends AbstractUIHandler<M, UI>>
        extends AbstractReefDbAction<M, UI, H> {

    private boolean fromChangeScreenAction = false;
    private boolean fromCheckModelAction = false;
    private boolean allowChangeScreen = true;

    /**
     * <p>Constructor for AbstractAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractReefDbSaveAction(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    public boolean isFromChangeScreenAction() {
        return fromChangeScreenAction;
    }

    public void setFromChangeScreenAction(boolean fromChangeScreenAction) {
        this.fromChangeScreenAction = fromChangeScreenAction;
    }

    public boolean isFromCheckModelAction() {
        return fromCheckModelAction;
    }

    public void setFromCheckModelAction(boolean fromCheckModelAction) {
        this.fromCheckModelAction = fromCheckModelAction;
    }

    public boolean isAllowChangeScreen() {
        return allowChangeScreen;
    }

    public void setAllowChangeScreen(boolean allowChangeScreen) {
        this.allowChangeScreen = allowChangeScreen;
    }

    @Override
    public boolean prepareAction() throws Exception {
        fromChangeScreenAction = false;
        fromCheckModelAction = false;
        allowChangeScreen = true;
        return super.prepareAction();
    }

    // Override this method to gather models to set as not modified at the end of action
    protected List<AbstractBeanUIModel> getModelsToModify() {
        return new ArrayList<>();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // Set each model as not modified
        getModelsToModify().forEach(model -> model.setModify(false));

        // Set main model as not modified
        getModel().setModify(false);
    }

}
