package fr.ifremer.reefdb.ui.swing.content.observation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Save action.
 */
public class SaveAndNextAction extends SaveAction {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAndNextAction(ObservationUIHandler handler) {
        super(handler);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        if (getModel().isEditable() && getModel().isModify() && getModel().isValid()) {

            // call the super method when conditions matches
            return super.prepareAction();
        }

        // by default, don't save
        return false;
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {

        // but always select next tab
        getHandler().selectNextTab();

        super.releaseAction();
    }
}
