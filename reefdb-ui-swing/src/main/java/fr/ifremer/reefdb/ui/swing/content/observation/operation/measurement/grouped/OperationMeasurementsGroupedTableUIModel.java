package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * Modele pour les mesures des prelevements (ecran prelevements/mesure).
 */
public class OperationMeasurementsGroupedTableUIModel
    extends AbstractMeasurementsGroupedTableUIModel<MeasurementDTO, OperationMeasurementsGroupedRowModel, OperationMeasurementsGroupedTableUIModel> {

    /**
     * Constant <code>PROPERTY_SAMPLING_OPERATION="samplingOperation"</code>
     * property identifier used to propagate row sampling operation property change to parent
     */
    public static final String PROPERTY_SAMPLING_OPERATION = "samplingOperation";
    public static final String PROPERTY_PIT_TRANSITION_LENGTH_PMFM_ID = "pitTransitionLengthPmfmId";

    // pmfm ids for grid initialization (pit protocol)
    private Integer pitTransectOriginPmfmId;
    private Integer pitTransectLengthPmfmId;
    private Integer pitTransitionLengthPmfmId;

    // pmfm ids for auto calculation of transect length
    private Integer calculatedLengthEndPositionPmfmId;
    private Integer calculatedLengthTransitionPmfmId;

    /**
     * <p>getSamplingOperations.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SamplingOperationDTO> getSamplingOperations() {
        return getSurvey() == null ? null : (List<SamplingOperationDTO>) getSurvey().getSamplingOperations();
    }

    public Integer getPitTransectOriginPmfmId() {
        return pitTransectOriginPmfmId;
    }

    public void setPitTransectOriginPmfmId(Integer pitTransectOriginPmfmId) {
        this.pitTransectOriginPmfmId = pitTransectOriginPmfmId;
    }

    public Integer getPitTransectLengthPmfmId() {
        return pitTransectLengthPmfmId;
    }

    public void setPitTransectLengthPmfmId(Integer pitTransectLengthPmfmId) {
        this.pitTransectLengthPmfmId = pitTransectLengthPmfmId;
    }

    public Integer getPitTransitionLengthPmfmId() {
        return pitTransitionLengthPmfmId;
    }

    public void setPitTransitionLengthPmfmId(Integer pitTransitionLengthPmfmId) {
        this.pitTransitionLengthPmfmId = pitTransitionLengthPmfmId;
        firePropertyChange(PROPERTY_PIT_TRANSITION_LENGTH_PMFM_ID, null, pitTransitionLengthPmfmId);
    }

    public boolean isPitTransitionGridInitializationEnabled() {
        return /* Mantis #38345 The grid initialisation is enabled even if the origin and transect length identifiers are missing
                    getTransectOriginPmfmId() != null
                && getTransectLengthPmfmId() != null
                &&*/ getPitTransitionLengthPmfmId() != null
                /* Mantis #50045: the grid initialisation is enabled even if there are measurements already inside
                && !getSamplingOperationsWithoutMeasurement().isEmpty()*/;
    }

    public Integer getCalculatedLengthEndPositionPmfmId() {
        return calculatedLengthEndPositionPmfmId;
    }

    public void setCalculatedLengthEndPositionPmfmId(Integer calculatedLengthEndPositionPmfmId) {
        this.calculatedLengthEndPositionPmfmId = calculatedLengthEndPositionPmfmId;
    }

    public Integer getCalculatedLengthTransitionPmfmId() {
        return calculatedLengthTransitionPmfmId;
    }

    public void setCalculatedLengthTransitionPmfmId(Integer calculatedLengthTransitionPmfmId) {
        this.calculatedLengthTransitionPmfmId = calculatedLengthTransitionPmfmId;
    }

    public List<SamplingOperationDTO> getSamplingOperationsWithoutMeasurement() {
        List<SamplingOperationDTO> samplingOperationsWithoutMeasurement = Lists.newArrayList();

        if (CollectionUtils.isNotEmpty(getSamplingOperations())) {
            for (final SamplingOperationDTO samplingOperation : getSamplingOperations()) {

                List<OperationMeasurementsGroupedRowModel> individualMeasurements = ReefDbBeans.filterCollection(getRows(),
                        input -> input.getSamplingOperation() != null && Objects.equals(input.getSamplingOperation().getName(), samplingOperation.getName()));

                if (CollectionUtils.isEmpty(individualMeasurements)) {
                    samplingOperationsWithoutMeasurement.add(samplingOperation);
                }
            }
        }
        return samplingOperationsWithoutMeasurement;
    }

}
