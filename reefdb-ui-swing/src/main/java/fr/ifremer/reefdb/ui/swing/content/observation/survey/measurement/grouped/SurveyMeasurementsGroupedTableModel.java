package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des mesures des observation (ecran observation/mesure).
 */
public class SurveyMeasurementsGroupedTableModel extends AbstractMeasurementsGroupedTableModel<SurveyMeasurementsGroupedRowModel> {

    /**
     * Identifiant pour la colonne groupe taxon.
     */
    public static final ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel> TAXON_GROUP = ReefDbColumnIdentifier.newId(
        SurveyMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP,
        n("reefdb.property.taxonGroup.short"),
        n("reefdb.survey.measurement.grouped.taxonGroup.tip"),
        TaxonGroupDTO.class);

    /**
     * Identifiant pour la colonne taxon.
     */
    public static final ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel> TAXON = ReefDbColumnIdentifier.newId(
        SurveyMeasurementsGroupedRowModel.PROPERTY_TAXON,
        n("reefdb.property.taxon"),
        n("reefdb.survey.measurement.grouped.taxon.tip"),
        TaxonDTO.class,
        DecoratorService.WITH_CITATION);

    /**
     * Identifiant pour la colonne taxon saisi.
     */
    public static final ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel> INPUT_TAXON_NAME = ReefDbColumnIdentifier.newId(
        SurveyMeasurementsGroupedRowModel.PROPERTY_INPUT_TAXON_NAME,
        n("reefdb.property.inputTaxon"),
        n("reefdb.survey.measurement.grouped.inputTaxon.tip"),
        String.class);

    /**
     * Identifiant pour la colonne valeur.
     */
    public static final ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        SurveyMeasurementsGroupedRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.survey.measurement.grouped.comment.tip"),
        String.class);

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public SurveyMeasurementsGroupedTableModel(final TableColumnModelExt columnModel, boolean createNewRow) {
        super(columnModel, createNewRow);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyMeasurementsGroupedRowModel createNewRow() {
        return new SurveyMeasurementsGroupedRowModel(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel> getFirstColumnEditing() {
        return TAXON_GROUP;
    }

    @Override
    public ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel> getPmfmInsertPosition() {
        return INPUT_TAXON_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeasurementsGroupedTableUIModel getTableUIModel() {
        return (AbstractMeasurementsGroupedTableUIModel) super.getTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStateContext() {
        if (getTableUIModel().getSurvey() != null && getTableUIModel().getSurvey().getProgram() != null) {

            return SurveyMeasurementsGroupedTableUIModel.PROPERTY_SURVEY + '_'
                + SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS + '_'
                + getTableUIModel().getSurvey().getProgram().getCode();
        }

        return super.getStateContext();
    }

}
