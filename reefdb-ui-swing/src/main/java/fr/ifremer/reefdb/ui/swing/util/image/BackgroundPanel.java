package fr.ifremer.reefdb.ui.swing.util.image;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.config.ReefDbConfiguration;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.util.PaintUtils;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.image.BufferedImage;

/*
 *  Support custom painting on a panel in the form of
 *
 *  a) images - that can be scaled, tiled or painted at original size
 *  b) non solid painting - that can be done by using a Paint object
 *
 *  Also, any component added directly to this panel will be made
 *  non-opaque so that the custom painting can show through.
 */

/**
 * <p>BackgroundPanel class.</p>
 */
public class BackgroundPanel extends JXPanel {

    private Image image;
    private BufferedImage scaledImage;
    private boolean selected = false;

    private static final Border outsideBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);
    private float alignmentX = 0.5f;
    private float alignmentY = 0.5f;
    public static final double NO_SCALE = 1d;
    private boolean scaled = false;
    private double ratio = NO_SCALE;

    /**
     * <p>Constructor for BackgroundPanel.</p>
     */
    public BackgroundPanel() {
        this(null, false);
    }

    /*
     *  Set image as the background
     */

    /**
     * <p>Constructor for BackgroundPanel.</p>
     *
     * @param image          a {@link java.awt.Image} object.
     * @param withBackground a boolean.
     */
    public BackgroundPanel(Image image, boolean withBackground) {
        setLayout(new BorderLayout());
        if (withBackground) {
            setBackgroundPainter(new MattePainter(PaintUtils.getCheckerPaint(Color.white, new Color(250, 250, 250), 50)));
        }
        setImage(image);
        setSelected(false);
    }

    /*
     *	Set the image used as the background
     */

    /**
     * <p>Setter for the field <code>image</code>.</p>
     *
     * @param image a {@link java.awt.Image} object.
     */
    public void setImage(Image image) {
        this.image = image;
        repaint();
    }

    /**
     * <p>isSelected.</p>
     *
     * @return a boolean.
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * <p>Setter for the field <code>selected</code>.</p>
     *
     * @param selected a boolean.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
        setBorder(BorderFactory.createCompoundBorder(outsideBorder,
                selected ? BorderFactory.createMatteBorder(2, 2, 2, 2, ReefDbConfiguration.getInstance().getColorSelectedRow()) : null));
    }

    /*
     *  Specify the horizontal alignment of the image when using ACTUAL style
     */

    /**
     * <p>setImageAlignmentX.</p>
     *
     * @param alignmentX a float.
     */
    public void setImageAlignmentX(float alignmentX) {
        this.alignmentX = alignmentX > 1.0f ? 1.0f : alignmentX < 0.0f ? 0.0f : alignmentX;
        repaint();
    }

    /*
     *  Specify the horizontal alignment of the image when using ACTUAL style
     */

    /**
     * <p>setImageAlignmentY.</p>
     *
     * @param alignmentY a float.
     */
    public void setImageAlignmentY(float alignmentY) {
        this.alignmentY = alignmentY > 1.0f ? 1.0f : alignmentY < 0.0f ? 0.0f : alignmentY;
        repaint();
    }

    /**
     * <p>Setter for the field <code>scaled</code>.</p>
     *
     * @param scaled a boolean.
     */
    public void setScaled(boolean scaled) {
        this.scaled = scaled;
        scaleImage();
        repaint();
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
        setScaled(ratio != NO_SCALE);
    }

    /**
     * <p>scaleImage.</p>
     */
    private void scaleImage() {
        if (scaled && image != null) {

            int imageWidth = image.getWidth(this);
            int imageHeight = image.getHeight(this);

            if (ratio == NO_SCALE) {
                int pw = super.getPreferredSize().width;   //panel width
                int ph = super.getPreferredSize().height;  //panel height

                // use doubles so division below won't round
                if (pw < imageWidth || ph < imageHeight) {

                    double rw = (double) pw / (double) imageWidth;
                    double rh = (double) ph / (double) imageHeight;

                    ratio = Math.min(rw, rh);

                }
            }
            int newWidth = Math.max((int) (imageWidth * ratio), 1);
            int newHeight = Math.max((int) (imageHeight * ratio), 1);

            scaledImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = scaledImage.createGraphics();
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.drawImage(image, 0, 0, newWidth, newHeight, 0, 0, imageWidth, imageHeight, null);
            g.dispose();

        } else {
            scaledImage = null;
        }
    }
    /*
     *  Override method so we can make the component transparent
     */

    /**
     * <p>add.</p>
     *
     * @param component a {@link javax.swing.JComponent} object.
     */
    public void add(JComponent component) {
        add(component, null);
    }

    /*
     *  Override to provide a preferred size equal to the image size
     */

    /**
     * {@inheritDoc}
     */
    @Override
    public Dimension getPreferredSize() {
        Image imageToRender = scaled ? scaledImage : image;
        if (imageToRender != null) {
            Insets insets = getInsets();
            return new Dimension(imageToRender.getWidth(this) + insets.left + insets.right, imageToRender.getHeight(this) + insets.top + insets.bottom);
        } else {
            return super.getPreferredSize();
        }
    }

    /*
     *  Add custom painting
     */

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //  Draw the image
        Image imageToDraw = scaled ? scaledImage : image;
        if (imageToDraw != null) {
            Dimension d = getSize();
            Insets insets = getInsets();
            int width = d.width - insets.left - insets.right;
            int height = d.height - insets.top - insets.left;
            float x = (width - imageToDraw.getWidth(null)) * alignmentX;
            float y = (height - imageToDraw.getHeight(null)) * alignmentY;
            g.drawImage(imageToDraw, (int) x + insets.left, (int) y + insets.top, this);
        }
    }

}
