package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;

import java.util.List;

/**
 * Created by Ludovic on 26/05/2015.
 */
public class InitGridUIModel extends AbstractEmptyUIModel<InitGridUIModel> {

    private List<SamplingOperationDTO> samplingOperations;
    public static final String PROPERTY_SAMPLING_OPERATIONS = "samplingOperations";

    private SamplingOperationDTO samplingOperation;
    public static final String PROPERTY_SAMPLING_OPERATION = "samplingOperation";

    private SamplingOperationDTO lastSamplingOperation;

    private boolean allSamplingOperations;
    public static final String PROPERTY_ALL_SAMPLING_OPERATIONS = "allSamplingOperations";

    private boolean contiguousSamplingOperations;
    public static final String PROPERTY_CONTIGUOUS_SAMPLING_OPERATIONS = "contiguousSamplingOperations";

    private Integer origin;
    public static final String PROPERTY_ORIGIN = "origin";

    private Integer transition;
    public static final String PROPERTY_TRANSITION = "transition";

    private Integer length;
    public static final String PROPERTY_LENGTH = "length";

    private int originPmfmId;
    private int lengthPmfmId;
    private int transitionPmfmId;

    private UnitDTO originUnit;
    private UnitDTO lengthUnit;
    private UnitDTO transitionUnit;
    public static final String PROPERTY_TRANSITION_UNIT = "transitionUnit";

    // map of result
    private Multimap<SamplingOperationDTO, Integer> resultMap = ArrayListMultimap.create();
    private Multimap<SamplingOperationDTO, Integer> currentMap = ArrayListMultimap.create();

    public Multimap<SamplingOperationDTO, Integer> getResultMap() {
        return resultMap;
    }

    public Multimap<SamplingOperationDTO, Integer> getCurrentMap() {
        return currentMap;
    }

    public List<SamplingOperationDTO> getSamplingOperations() {
        return samplingOperations;
    }

    public void setSamplingOperations(List<SamplingOperationDTO> samplingOperations) {
        this.samplingOperations = samplingOperations;
        firePropertyChange(PROPERTY_SAMPLING_OPERATIONS, null, samplingOperations);
    }

    public boolean isAllSamplingOperations() {
        return allSamplingOperations;
    }

    public void setAllSamplingOperations(boolean allSamplingOperations) {
        boolean oldValue = isAllSamplingOperations();
        this.allSamplingOperations = allSamplingOperations;
        firePropertyChange(PROPERTY_ALL_SAMPLING_OPERATIONS, oldValue, allSamplingOperations);
        if (!allSamplingOperations)
            setContiguousSamplingOperations(false);
    }

    public boolean isContiguousSamplingOperations() {
        return contiguousSamplingOperations;
    }

    public void setContiguousSamplingOperations(boolean contiguousSamplingOperations) {
        boolean oldValue = isContiguousSamplingOperations();
        this.contiguousSamplingOperations = contiguousSamplingOperations;
        firePropertyChange(PROPERTY_CONTIGUOUS_SAMPLING_OPERATIONS, oldValue, contiguousSamplingOperations);
    }

    public SamplingOperationDTO getSamplingOperation() {
        return samplingOperation;
    }

    public void setSamplingOperation(SamplingOperationDTO samplingOperation) {
        SamplingOperationDTO oldValue = getSamplingOperation();
        this.samplingOperation = samplingOperation;
        firePropertyChange(PROPERTY_SAMPLING_OPERATION, oldValue, samplingOperation);
    }

    public SamplingOperationDTO getLastSamplingOperation() {
        return lastSamplingOperation;
    }

    public void setLastSamplingOperation(SamplingOperationDTO lastSamplingOperation) {
        this.lastSamplingOperation = lastSamplingOperation;
    }

    public Integer getOrigin() {
        return origin;
    }

    public void setOrigin(Integer origin) {
        Integer oldValue = getOrigin();
        this.origin = origin;
        firePropertyChange(PROPERTY_ORIGIN, oldValue, origin);
    }

    public Integer getTransition() {
        return transition;
    }

    public void setTransition(Integer transition) {
        Integer oldValue = getTransition();
        this.transition = transition;
        firePropertyChange(PROPERTY_TRANSITION, oldValue, transition);
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        Integer oldValue = getLength();
        this.length = length;
        firePropertyChange(PROPERTY_LENGTH, oldValue, length);
    }

    public boolean areValuesValid() {

        // values must be filled
        if (getOrigin() == null || getTransition() == null || getLength() == null) return false;

        if (isAllSamplingOperations() && isContiguousSamplingOperations()) {

            // the length must be a multiple of number of sampling operations
            if (getLength() % getSamplingOperations().size() != 0) return false;

            // the effective length per sampling operation (length/nbSamplingOperation - origin) must be a multiple of transition
            // wrong test: return (getLength() / getSamplingOperations().size() - getOrigin()) % getTransition() == 0;
            // Mantis #50450: use this correct test:
            return (((getLength() - getOrigin()) / getTransition()) + 1) % getSamplingOperations().size() == 0;

        }

        // the effective length (length - origin) must be a multiple of transition
        return (getLength() - getOrigin()) % getTransition() == 0;

    }

    public int getOriginPmfmId() {
        return originPmfmId;
    }

    public void setOriginPmfmId(int originPmfmId) {
        this.originPmfmId = originPmfmId;
    }

    public int getLengthPmfmId() {
        return lengthPmfmId;
    }

    public void setLengthPmfmId(int lengthPmfmId) {
        this.lengthPmfmId = lengthPmfmId;
    }

    public int getTransitionPmfmId() {
        return transitionPmfmId;
    }

    public void setTransitionPmfmId(int transitionPmfmId) {
        this.transitionPmfmId = transitionPmfmId;
    }

    public UnitDTO getOriginUnit() {
        return originUnit;
    }

    public void setOriginUnit(UnitDTO originUnit) {
        this.originUnit = originUnit;
    }

    public UnitDTO getLengthUnit() {
        return lengthUnit;
    }

    public void setLengthUnit(UnitDTO lengthUnit) {
        this.lengthUnit = lengthUnit;
    }

    public UnitDTO getTransitionUnit() {
        return transitionUnit;
    }

    public void setTransitionUnit(UnitDTO transitionUnit) {
        this.transitionUnit = transitionUnit;
        firePropertyChange(PROPERTY_TRANSITION_UNIT, null, transitionUnit);
    }
}
