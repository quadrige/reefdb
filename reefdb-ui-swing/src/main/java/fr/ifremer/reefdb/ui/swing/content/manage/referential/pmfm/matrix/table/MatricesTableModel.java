package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class MatricesTableModel extends AbstractReefDbTableModel<MatricesTableRowModel> {

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<MatricesTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class,
            true);

    /** Constant <code>DESCRIPTION</code> */
    public static final ReefDbColumnIdentifier<MatricesTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.property.description"),
            String.class);

    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<MatricesTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class,
            true);

    /** Constant <code>ASSOCIATED_FRACTIONS</code> */
    public static final ReefDbColumnIdentifier<MatricesTableRowModel> ASSOCIATED_FRACTIONS = ReefDbColumnIdentifier.newId(
            MatricesTableRowModel.PROPERTY_FRACTIONS,
            n("reefdb.property.pmfm.matrix.associatedFractions"),
            n("reefdb.property.pmfm.matrix.associatedFractions"),
            FractionDTO.class,
            DecoratorService.COLLECTION_SIZE,
            true);


    public static final ReefDbColumnIdentifier<MatricesTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        MatricesTableRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<MatricesTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        MatricesTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<MatricesTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        MatricesTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);

    /**
     * <p>Constructor for MatricesTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param createNewRowAllowed a boolean.
     */
    public MatricesTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
    }

    /** {@inheritDoc} */
    @Override
    public MatricesTableRowModel createNewRow() {
        return new MatricesTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<MatricesTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
