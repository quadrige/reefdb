package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.FilterableComboBoxCellEditor;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.menu.DepartmentMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.table.DepartmentRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.table.DepartmentTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JOptionPane;
import java.util.List;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Departments au niveau local
 */
public class ManageDepartmentsLocalUIHandler extends
        AbstractReefDbTableUIHandler<DepartmentRowModel, ManageDepartmentsLocalUIModel, ManageDepartmentsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageDepartmentsLocalUIHandler.class);

    private FilterableComboBoxCellEditor<DepartmentDTO> parentDepartmentCellEditor;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageDepartmentsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageDepartmentsLocalUIModel model = new ManageDepartmentsLocalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageDepartmentsLocalUI ui) {
        initUI(ui);

        // hide context filter panel
        ui.getDepartmentMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getDepartmentMenuUI().getHandler().forceLocal(true);

        // listen to search result
        ui.getDepartmentMenuUI().getModel().addPropertyChangeListener(DepartmentMenuUIModel.PROPERTY_RESULTS, evt -> loadTable((List<DepartmentDTO>) evt.getNewValue()));

        // first load of all departments
        getModel().setAllDepartments(getContext().getReferentialService().getDepartments(StatusFilter.ALL));

        // Initialisation du tableau
        initTable();

        getModel().addPropertyChangeListener(AbstractReefDbTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            if (getModel().getSingleSelectedRow() != null && parentDepartmentCellEditor != null) {

                // remove actual department from parent combo box
                List<DepartmentDTO> departments = ReefDbBeans.filterCollection(getModel().getAllDepartments(), (Predicate<DepartmentDTO>) input -> !Objects.equals(input.getId(), getModel().getSingleSelectedRow().getId()));

                parentDepartmentCellEditor.getCombo().setData(departments);

            }
        });

        getUI().getManageDepartmentsLocalTableDeleteBouton().setEnabled(false);
        getUI().getManageDepartmentsLocalTableReplaceBouton().setEnabled(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        /* columns */
        // code
        TableColumnExt codeCol = addColumn(DepartmentTableModel.CODE);
        codeCol.setSortable(true);

        // name
        TableColumnExt nameCol = addColumn(DepartmentTableModel.NAME);
        nameCol.setSortable(true);

        // description
        TableColumnExt descCol = addColumn(DepartmentTableModel.DESCRIPTION);
        descCol.setSortable(true);

        // parent
        parentDepartmentCellEditor = newExtendedComboBoxCellEditor(
                getModel().getAllDepartments(),
                DepartmentTableModel.PARENT_DEPARTMENT,
                false);
        TableColumnExt parentCol = addColumn(
                parentDepartmentCellEditor,
                newTableCellRender(DepartmentTableModel.PARENT_DEPARTMENT),
                DepartmentTableModel.PARENT_DEPARTMENT);
        parentCol.setSortable(true);

        // email
        TableColumnExt emailCol = addColumn(DepartmentTableModel.EMAIL);
        emailCol.setSortable(true);

        // phone
        TableColumnExt phoneCol = addColumn(DepartmentTableModel.PHONE);
        phoneCol.setSortable(true);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                DepartmentTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        // address
        TableColumnExt addressCol = addColumn(DepartmentTableModel.ADDRESS);
        addressCol.setSortable(true);

        // siret & url
        TableColumnExt siretCol = addColumn(DepartmentTableModel.SIRET);
        siretCol.setSortable(true);
        TableColumnExt urlCol = addColumn(DepartmentTableModel.URL);
        urlCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(DepartmentTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(DepartmentTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(DepartmentTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        DepartmentTableModel tableModel = new DepartmentTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.departments.local"));

        // Initialisation du tableau
        initTable(getTable());

        // optional columns are hidden
        addressCol.setVisible(false);
        siretCol.setVisible(false);
        urlCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getModel().setAllDepartments(getContext().getReferentialService().getDepartments(StatusFilter.ALL));

        parentDepartmentCellEditor.getCombo().setData(null);
        parentDepartmentCellEditor.getCombo().setData(getModel().getAllDepartments());
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, DepartmentRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        if (DepartmentDTO.PROPERTY_STATUS.equals(propertyName)) {
            StatusDTO oldStatus = (StatusDTO) oldValue;
            StatusDTO newStatus = (StatusDTO) newValue;
            // ask confirmation for setting DISABLE to this department
            if (StatusCode.LOCAL_ENABLE.getValue().equals(oldStatus.getCode())
                    && StatusCode.LOCAL_DISABLE.getValue().equals(newStatus.getCode())) {
                if (getContext().getDialogHelper().showConfirmDialog(t("reefdb.manage.department.status.disable.message"), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                    // restore old status
                    row.setStatus(oldStatus);
                }
            }
        }

        row.setDirty(true);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<DepartmentRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            final DepartmentRowModel rowModel = addedRows.get(0);

            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(rowModel);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(DepartmentRowModel row) {

        row.getErrors().clear();
        return super.isRowValid(row) && isUnique(row);
    }

    private boolean isUnique(DepartmentRowModel row) {

        if (StringUtils.isNotBlank(row.getCode())) {
            boolean duplicateFound = false;

            // check in current table
            for (DepartmentRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getCode().equalsIgnoreCase(otherRow.getCode())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.department"), row.getCode(), t("reefdb.property.referential.local")),
                            DepartmentRowModel.PROPERTY_CODE);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {

                // check unicity in database
                DepartmentCriteriaDTO searchCriteria = ReefDbBeanFactory.newDepartmentCriteriaDTO();
                searchCriteria.setCode(row.getCode());
                searchCriteria.setStrictName(true);
                List<DepartmentDTO> existingDepartments = getContext().getReferentialService().searchDepartments(searchCriteria);
                if (CollectionUtils.isNotEmpty(existingDepartments)) {
                    for (DepartmentDTO department: existingDepartments) {
                        if (!department.getId().equals(row.getId())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.department"), row.getCode(), ReefDbBeans.isLocalStatus(department.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    DepartmentRowModel.PROPERTY_CODE);
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<DepartmentRowModel> getTableModel() {
        return (DepartmentTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageDepartmentsLocalTable();
    }

    /**
     * <p>loadTable.</p>
     *
     * @param departments a {@link java.util.List} object.
     */
    public void loadTable(List<DepartmentDTO> departments) {
        getModel().setBeans(departments);
    }

}
