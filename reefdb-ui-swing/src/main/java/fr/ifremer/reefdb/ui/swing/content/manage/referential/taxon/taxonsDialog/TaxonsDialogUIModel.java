package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.taxonsDialog;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

import java.util.List;

/**
 * Created by Ludovic on 24/11/2015.
 */
public class TaxonsDialogUIModel extends AbstractEmptyUIModel<TaxonsDialogUIModel> {

    /** Constant <code>PROPERTY_TAXON_GROUP="taxonGroup"</code> */
    public static final String PROPERTY_TAXON_GROUP = "taxonGroup";
    /** Constant <code>PROPERTY_TAXONS="taxons"</code> */
    public static final String PROPERTY_TAXONS = "taxons";
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";
    private TaxonGroupDTO taxonGroup;
    private List<TaxonDTO> taxons;
    private boolean editable;

    /**
     * <p>Getter for the field <code>taxonGroup</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public TaxonGroupDTO getTaxonGroup() {
        return taxonGroup;
    }

    /**
     * <p>Setter for the field <code>taxonGroup</code>.</p>
     *
     * @param taxonGroup a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public void setTaxonGroup(TaxonGroupDTO taxonGroup) {
        this.taxonGroup = taxonGroup;
        firePropertyChange(PROPERTY_TAXON_GROUP, null, taxonGroup);
    }

    /**
     * <p>Getter for the field <code>taxons</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<TaxonDTO> getTaxons() {
        return taxons;
    }

    /**
     * <p>Setter for the field <code>taxons</code>.</p>
     *
     * @param taxons a {@link java.util.List} object.
     */
    public void setTaxons(List<TaxonDTO> taxons) {
        this.taxons = taxons;
        firePropertyChange(PROPERTY_TAXONS, null, taxons);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }
}
