package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>TaxonGroupTableRowModel class.</p>
 *
 * @author Antoine
 */
public class TaxonGroupTableRowModel extends AbstractReefDbRowUIModel<TaxonGroupDTO, TaxonGroupTableRowModel> implements TaxonGroupDTO, ErrorAware {

    private static final Binder<TaxonGroupDTO, TaxonGroupTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(TaxonGroupDTO.class, TaxonGroupTableRowModel.class);

    private static final Binder<TaxonGroupTableRowModel, TaxonGroupDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(TaxonGroupTableRowModel.class, TaxonGroupDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for TaxonGroupTableRowModel.</p>
     *
     * @param readOnly a boolean.
     */
    public TaxonGroupTableRowModel(boolean readOnly) {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
        setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEditable() {
        return super.isEditable() && !isReadOnly();
    }

    /** {@inheritDoc} */
    @Override
    protected TaxonGroupDTO newBean() {
        return ReefDbBeanFactory.newTaxonGroupDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        return delegateObject.getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLabel(String label) {
        delegateObject.setLabel(label);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public String getType() {
        return delegateObject.getType();
    }

    /** {@inheritDoc} */
    @Override
    public void setType(String type) {
        delegateObject.setType(type);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUpdate() {
        return delegateObject.isUpdate();
    }

    /** {@inheritDoc} */
    @Override
    public void setUpdate(boolean update) {
        delegateObject.setUpdate(update);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isExclusive() {
        return delegateObject.isExclusive();
    }

    /** {@inheritDoc} */
    @Override
    public void setExclusive(boolean exclusive) {
        delegateObject.setExclusive(exclusive);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonDTO getTaxons(int index) {
        return delegateObject.getTaxons(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTaxonsEmpty() {
        return delegateObject.isTaxonsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeTaxons() {
        return delegateObject.sizeTaxons();
    }

    /** {@inheritDoc} */
    @Override
    public void addTaxons(TaxonDTO taxons) {
        delegateObject.addTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllTaxons(Collection<TaxonDTO> taxons) {
        delegateObject.addAllTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeTaxons(TaxonDTO taxons) {
        return delegateObject.removeTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllTaxons(Collection<TaxonDTO> taxons) {
        return delegateObject.removeAllTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsTaxons(TaxonDTO taxons) {
        return delegateObject.containsTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllTaxons(Collection<TaxonDTO> taxons) {
        return delegateObject.containsAllTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonDTO> getTaxons() {
        return delegateObject.getTaxons();
    }

    /** {@inheritDoc} */
    @Override
    public void setTaxons(List<TaxonDTO> taxons) {
        delegateObject.setTaxons(taxons);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupDTO getParentTaxonGroup() {
        return delegateObject.getParentTaxonGroup();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentTaxonGroup(TaxonGroupDTO parentTaxonGroup) {
        delegateObject.setParentTaxonGroup(parentTaxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
