package fr.ifremer.reefdb.ui.swing.content.extraction.filters.period;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Created by Ludovic on 18/01/2016.
 */
public class ExtractionPeriodTableModel extends AbstractReefDbTableModel<ExtractionPeriodRowModel> {

    /** Constant <code>START_DATE</code> */
    public static final ReefDbColumnIdentifier<ExtractionPeriodRowModel> START_DATE = ReefDbColumnIdentifier.newId(
            ExtractionPeriodRowModel.PROPERTY_START_DATE,
            n("reefdb.extraction.period.startDate.label"),
            n("reefdb.extraction.period.startDate.label"),
            LocalDate.class,
            true);

    /** Constant <code>END_DATE</code> */
    public static final ReefDbColumnIdentifier<ExtractionPeriodRowModel> END_DATE = ReefDbColumnIdentifier.newId(
            ExtractionPeriodRowModel.PROPERTY_END_DATE,
            n("reefdb.extraction.period.endDate.label"),
            n("reefdb.extraction.period.endDate.label"),
            LocalDate.class,
            true);

    /**
     * <p>Constructor for ExtractionPeriodTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    public ExtractionPeriodTableModel(TableColumnModelExt columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<ExtractionPeriodRowModel> getFirstColumnEditing() {
        return START_DATE;
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionPeriodRowModel createNewRow() {
        return new ExtractionPeriodRowModel();
    }
}
