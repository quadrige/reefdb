/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>DepartmentTableModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class DepartmentTableModel extends AbstractReefDbTableModel<DepartmentRowModel> {

    private final boolean readOnly;

    /**
     * <p>Constructor for DepartmentTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param createNewRowAllowed a boolean.
     */
    public DepartmentTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
        readOnly = !createNewRowAllowed;
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentRowModel createNewRow() {
        return new DepartmentRowModel(readOnly);
    }

    /** Constant <code>CODE</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> CODE = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_CODE,
            n("reefdb.property.code"),
            n("reefdb.property.code"),
            String.class,
            true);

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> NAME = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class,
            true);

    /** Constant <code>DESCRIPTION</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.property.description"),
            String.class);

    /** Constant <code>PARENT_DEPARTMENT</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> PARENT_DEPARTMENT = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_PARENT_DEPARTMENT,
            n("reefdb.property.department.parent"),
            n("reefdb.property.department.parent"),
            DepartmentDTO.class);

    /** Constant <code>EMAIL</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> EMAIL = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_EMAIL,
            n("reefdb.property.email"),
            n("reefdb.property.email"),
            String.class);

    /** Constant <code>PHONE</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> PHONE = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_PHONE,
            n("reefdb.property.phone"),
            n("reefdb.property.phone"),
            String.class);

    /** Constant <code>ADDRESS</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> ADDRESS = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_ADDRESS,
            n("reefdb.property.address"),
            n("reefdb.property.address"),
            String.class);

    public static final ReefDbColumnIdentifier<DepartmentRowModel> SIRET = ReefDbColumnIdentifier.newId(
        DepartmentRowModel.PROPERTY_SIRET,
        n("reefdb.property.siret"),
        n("reefdb.property.siret"),
        String.class);

    public static final ReefDbColumnIdentifier<DepartmentRowModel> URL = ReefDbColumnIdentifier.newId(
        DepartmentRowModel.PROPERTY_URL,
        n("reefdb.property.url"),
        n("reefdb.property.url"),
        String.class);

    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<DepartmentRowModel> STATUS = ReefDbColumnIdentifier.newId(
            DepartmentRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class,
            true);

    public static final ReefDbColumnIdentifier<DepartmentRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        DepartmentRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<DepartmentRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        DepartmentRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<DepartmentRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        DepartmentRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);



    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<DepartmentRowModel> getFirstColumnEditing() {
        return CODE;
    }
}
