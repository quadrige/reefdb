package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;

/**
 * Bean used to set filter on grouped measurement
 * <p/>
 * Created by Ludovic on 12/06/2015.
 */
public class MeasurementsFilter {

    private SamplingOperationDTO samplingOperation;
    private TaxonGroupDTO taxonGroup;
    private TaxonDTO taxon;

    /**
     * <p>Getter for the field <code>samplingOperation</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    public SamplingOperationDTO getSamplingOperation() {
        return samplingOperation;
    }

    /**
     * <p>Setter for the field <code>samplingOperation</code>.</p>
     *
     * @param samplingOperation a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    public void setSamplingOperation(SamplingOperationDTO samplingOperation) {
        this.samplingOperation = samplingOperation;
    }

    /**
     * <p>Getter for the field <code>taxonGroup</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public TaxonGroupDTO getTaxonGroup() {
        return taxonGroup;
    }

    /**
     * <p>Setter for the field <code>taxonGroup</code>.</p>
     *
     * @param taxonGroup a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public void setTaxonGroup(TaxonGroupDTO taxonGroup) {
        this.taxonGroup = taxonGroup;
    }

    /**
     * <p>Getter for the field <code>taxon</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public TaxonDTO getTaxon() {
        return taxon;
    }

    /**
     * <p>Setter for the field <code>taxon</code>.</p>
     *
     * @param taxon a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public void setTaxon(TaxonDTO taxon) {
        this.taxon = taxon;
    }
}
