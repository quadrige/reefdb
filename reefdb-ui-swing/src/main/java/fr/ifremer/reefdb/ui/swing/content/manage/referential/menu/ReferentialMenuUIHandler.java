package fr.ifremer.reefdb.ui.swing.content.manage.referential.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;

import javax.swing.*;
import java.awt.Component;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ludovic on 23/03/2016.
 */
public abstract class ReferentialMenuUIHandler<M extends AbstractReferentialMenuUIModel, UI extends ReferentialMenuUI<M, ?>> extends AbstractReefDbUIHandler<M, UI> {

    /** {@inheritDoc} */
    @Override
    public void afterInit(UI ui) {
        initUI(ui);

        // set filter list
        initBeanFilterableComboBox(getApplyFilterUI().getApplyFilterCombo(), getFilters(), null);
    }

    /**
     * Search activaton.
     *
     * @param enabled a boolean.
     */
    public abstract void enableSearch(boolean enabled);

    /**
     * get filters to display in ApplyFilterUI
     *
     * @return a {@link java.util.List} object.
     */
    public abstract List<FilterDTO> getFilters();

    /**
     * get the ApplyFilterUI
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI} object.
     */
    public abstract ApplyFilterUI getApplyFilterUI();


    /**
     * <p>enableContextFilter.</p>
     *
     * @param enabled a boolean.
     */
    public void enableContextFilter(boolean enabled) {
        getApplyFilterUI().setVisible(enabled);
    }

    /**
     * <p>getLocalFilterPanel.</p>
     *
     * @return a {@link javax.swing.JComponent} object.
     */
    public abstract JComponent getLocalFilterPanel();

    /**
     * <p>enableLocalFilter.</p>
     *
     * @param enabled a boolean.
     */
    public void enableLocalFilter(boolean enabled){
        if (getLocalFilterPanel() != null) {
            getLocalFilterPanel().setVisible(enabled);
            getLocalFilterPanel().getParent().setVisible(Arrays.stream(getLocalFilterPanel().getParent().getComponents()).anyMatch(Component::isVisible));
        }
    }

    /**
     * <p>forceLocal.</p>
     *
     * @param local a {@link java.lang.Boolean} object.
     */
    public void forceLocal(Boolean local) {

        // set force status in model
        getModel().setForceStatus(local != null);

        // force the local filter in menu
        getModel().setLocal(local);

        // hide the local filter component
        enableLocalFilter(local == null);
    }
}
