package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>FractionsTableModel class.</p>
 *
 * @author Antoine
 */
public class FractionsTableModel extends AbstractReefDbTableModel<FractionsTableRowModel> {

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<FractionsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class,
            true);
    /** Constant <code>DESCRIPTION</code> */
    public static final ReefDbColumnIdentifier<FractionsTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.property.description"),
            String.class);
    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<FractionsTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class,
            true);
    /** Constant <code>ASSOCIATED_SUPPORTS</code> */
    public static final ReefDbColumnIdentifier<FractionsTableRowModel> ASSOCIATED_SUPPORTS = ReefDbColumnIdentifier.newId(
            FractionsTableRowModel.PROPERTY_MATRIXES,
            n("reefdb.property.pmfm.fraction.associatedMatrices"),
            n("reefdb.property.pmfm.fraction.associatedMatrices"),
            MatrixDTO.class,
            DecoratorService.COLLECTION_SIZE,
            true);


    public static final ReefDbColumnIdentifier<FractionsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        FractionsTableRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<FractionsTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        FractionsTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<FractionsTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        FractionsTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);



    /**
     * <p>Constructor for FractionsTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    public FractionsTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public FractionsTableRowModel createNewRow() {
        return new FractionsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<FractionsTableRowModel> getFirstColumnEditing() {
        return null;
    }
}
