package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Delete extractions Action
 */
public class DeleteExtractionAction extends AbstractReefDbAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    /**
     * Loggeur.
     */
    private static final Log LOG = LogFactory.getLog(DeleteExtractionAction.class);

    private Collection<? extends ExtractionDTO> extractionsToDelete;

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    public DeleteExtractionAction(ExtractionsTableUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("no selected extraction");
            return false;
        }

        extractionsToDelete = getModel().getSelectedRows();

        return askBeforeDelete(t("reefdb.action.delete.extraction.titre"), t("reefdb.action.delete.extraction.message"));
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Selected observation ID
        List<Integer> idExtractionToDelete = ReefDbBeans.collectIds(extractionsToDelete);

        // Remove observations service
        getContext().getExtractionService().deleteExtractions(idExtractionToDelete);

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        // Remove form model
        getModel().deleteSelectedRows();

        getUI().getParentContainer(ExtractionUI.class).getHandler().reloadComboBox();

        super.postSuccessAction();
    }
}
