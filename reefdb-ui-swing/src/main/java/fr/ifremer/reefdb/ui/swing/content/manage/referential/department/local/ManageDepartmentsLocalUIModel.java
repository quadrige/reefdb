package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.table.DepartmentRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

import java.util.List;

/**
 * Model pour le tableau des Departments en local
 */
public class ManageDepartmentsLocalUIModel extends AbstractReefDbTableUIModel<DepartmentDTO, DepartmentRowModel, ManageDepartmentsLocalUIModel> {

    private List<DepartmentDTO> allDepartments;

    /**
     * Constructor.
     */
    public ManageDepartmentsLocalUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>allDepartments</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<DepartmentDTO> getAllDepartments() {
        return allDepartments;
    }

    /**
     * <p>Setter for the field <code>allDepartments</code>.</p>
     *
     * @param allDepartments a {@link java.util.List} object.
     */
    public void setAllDepartments(List<DepartmentDTO> allDepartments) {
        this.allDepartments = allDepartments;
    }
}
