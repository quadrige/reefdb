package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class MethodsTableModel extends AbstractReefDbTableModel<MethodsTableRowModel> {

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class, true);
    /** Constant <code>REFERENCE</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> REFERENCE = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_REFERENCE,
            n("reefdb.property.pmfm.method.reference"),
            n("reefdb.property.pmfm.method.reference"),
            String.class);
    /** Constant <code>NUMBER</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> NUMBER = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_NUMBER,
            n("reefdb.property.pmfm.method.number"),
            n("reefdb.property.pmfm.method.number"),
            String.class);
    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class, true);
    /** Constant <code>DESCRIPTION</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.property.description"),
            String.class, true);
    /** Constant <code>DESCRIPTIONPACKAGING</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> DESCRIPTIONPACKAGING = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION_PACKAGING,
            n("reefdb.property.pmfm.method.descriptionPackaging"),
            n("reefdb.property.pmfm.method.descriptionPackaging"),
            String.class);
    /** Constant <code>DESCRIPTIONPREPARATION</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> DESCRIPTIONPREPARATION = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION_PREPARATION,
            n("reefdb.property.pmfm.method.descriptionPreparation"),
            n("reefdb.property.pmfm.method.descriptionPreparation"),
            String.class);
    /** Constant <code>DESCRIPTIONPRESERVATION</code> */
    public static final ReefDbColumnIdentifier<MethodsTableRowModel> DESCRIPTIONPRESERVATION = ReefDbColumnIdentifier.newId(
            MethodsTableRowModel.PROPERTY_DESCRIPTION_PRESERVATION,
            n("reefdb.property.pmfm.method.descriptionPreservation"),
            n("reefdb.property.pmfm.method.descriptionPreservation"),
            String.class);

    public static final ReefDbColumnIdentifier<MethodsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        MethodsTableRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<MethodsTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        MethodsTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<MethodsTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        MethodsTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);



    /**
     * <p>Constructor for MethodsTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param createNewRowAllowed a boolean.
     */
    public MethodsTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
    }

    /** {@inheritDoc} */
    @Override
    public MethodsTableRowModel createNewRow() {
        return new MethodsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<MethodsTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
