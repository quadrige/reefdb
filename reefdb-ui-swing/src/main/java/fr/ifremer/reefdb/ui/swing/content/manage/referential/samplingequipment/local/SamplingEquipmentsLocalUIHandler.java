package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.menu.SamplingEquipmentsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.table.SamplingEquipmentsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.table.SamplingEquipmentsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des enginsPrelevement au niveau local
 */
public class SamplingEquipmentsLocalUIHandler extends
        AbstractReefDbTableUIHandler<SamplingEquipmentsTableRowModel, SamplingEquipmentsLocalUIModel, SamplingEquipmentsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SamplingEquipmentsLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SamplingEquipmentsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SamplingEquipmentsLocalUIModel model = new SamplingEquipmentsLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final SamplingEquipmentsLocalUI ui) {
        initUI(ui);

        // hide context filter panel
        ui.getSamplingEquipmentsLocalMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getSamplingEquipmentsLocalMenuUI().getHandler().forceLocal(true);

        // init listener on found analysis instruments
        ui.getSamplingEquipmentsLocalMenuUI().getModel().addPropertyChangeListener(SamplingEquipmentsMenuUIModel.PROPERTY_RESULTS, evt -> loadTable((List<SamplingEquipmentDTO>) evt.getNewValue()));

        // Initialisation du tableau
        initTable();

        getUI().getReferentialSamplingEquipmentsLocalTableDeleteBouton().setEnabled(false);
        getUI().getReferentialSamplingEquipmentsLocalTableReplaceBouton().setEnabled(false);

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(
                SamplingEquipmentsTableModel.NAME);
        mnemonicCol.setSortable(true);

        // description
        final TableColumnExt descriptionCol = addColumn(
                SamplingEquipmentsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // size
        final TableColumnExt sizeCol = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.SIGNED_HIGH_DECIMAL_DIGITS_PATTERN),
                newNumberCellRenderer(10),
                SamplingEquipmentsTableModel.SIZE);
        sizeCol.setSortable(true);

        // unit
        final TableColumnExt unitCol = addFilterableComboDataColumnToModel(
                SamplingEquipmentsTableModel.UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.ACTIVE),
                false);
        unitCol.setSortable(true);


        // Comment, creation and update dates
        addCommentColumn(SamplingEquipmentsTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(SamplingEquipmentsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(SamplingEquipmentsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                SamplingEquipmentsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        final SamplingEquipmentsTableModel tableModel = new SamplingEquipmentsTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.samplingEquipments.local"));

        // Initialisation du tableau
        initTable(getTable());

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
        getModel().setBeans(null);
    }

    /**
     * <p>loadTable.</p>
     *
     * @param samplingEquipments a {@link java.util.List} object.
     */
    public void loadTable(List<SamplingEquipmentDTO> samplingEquipments) {
        getModel().setBeans(samplingEquipments);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<SamplingEquipmentsTableRowModel> getTableModel() {
        return (SamplingEquipmentsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getReferentialSamplingEquipmentsLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<SamplingEquipmentsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            SamplingEquipmentsTableRowModel rowModel = addedRows.get(0);

            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));

            setFocusOnCell(rowModel);
        }

    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, SamplingEquipmentsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(SamplingEquipmentsTableRowModel row) {
        row.getErrors().clear();
        return !row.isEditable() || super.isRowValid(row) && isSamplingEquipmentValid(row);
    }

    private boolean isSamplingEquipmentValid(SamplingEquipmentsTableRowModel row) {

        // check unit not null if size not null
        if (row.getSize() != null && row.getUnit() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.field.empty"), SamplingEquipmentsTableRowModel.PROPERTY_UNIT);
        }

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (SamplingEquipmentsTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.samplingEquipment"), row.getName(), t("reefdb.property.referential.local")),
                            SamplingEquipmentsTableRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<SamplingEquipmentDTO> existingSamplingEquipments = getContext().getReferentialService().searchSamplingEquipments(StatusFilter.ALL, row.getName(), null);
                if (CollectionUtils.isNotEmpty(existingSamplingEquipments)) {
                    for (SamplingEquipmentDTO samplingEquipment : existingSamplingEquipments) {
                        if (!samplingEquipment.getId().equals(row.getId())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.samplingEquipment"), row.getName(), ReefDbBeans.isLocalStatus(samplingEquipment.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    SamplingEquipmentsTableRowModel.PROPERTY_NAME);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();
    }

}
