/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import java.util.List;

/**
 * <p>ApplyFilterAction class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class ApplyFilterAction extends AbstractReefDbAction<ApplyFilterUIModel, ApplyFilterUI, ApplyFilterUIHandler> {

    private List<? extends QuadrigeBean> elements;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public ApplyFilterAction(ApplyFilterUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        if (getModel().getFilter() != null) {

            // load filtered elements if needed
            if (!getModel().getFilter().isFilterLoaded()) {
                getContext().getContextService().loadFilteredElements(getModel().getFilter());
            }

            elements = getModel().getFilter().getElements();

        } else {

            // empty elements
            elements = null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setElements(elements);

        super.postSuccessAction();
    }
}
