package fr.ifremer.reefdb.ui.swing.content.extraction.filters.period;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Row model for extraction period
 *
 * Created by Ludovic on 18/01/2016.
 */
public class ExtractionPeriodRowModel extends AbstractReefDbRowUIModel<ExtractionPeriodDTO, ExtractionPeriodRowModel> implements ExtractionPeriodDTO, ErrorAware {

    private static final Binder<ExtractionPeriodDTO, ExtractionPeriodRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(ExtractionPeriodDTO.class, ExtractionPeriodRowModel.class);

    private static final Binder<ExtractionPeriodRowModel, ExtractionPeriodDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ExtractionPeriodRowModel.class, ExtractionPeriodDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for ExtractionPeriodRowModel.</p>
     */
    public ExtractionPeriodRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected ExtractionPeriodDTO newBean() {
        return ReefDbBeanFactory.newExtractionPeriodDTO();
    }

    /** {@inheritDoc} */
    @Override
    public LocalDate getStartDate() {
        return delegateObject.getStartDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setStartDate(LocalDate startDate) {
        delegateObject.setStartDate(startDate);
    }

    /** {@inheritDoc} */
    @Override
    public LocalDate getEndDate() {
        return delegateObject.getEndDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setEndDate(LocalDate endDate) {
        delegateObject.setEndDate(endDate);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }
}
