package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.action.AbstractMainUIAction;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUI;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;

/**
 * <p>Abstract AbstractReefDbMainUIAction class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 * @since 1.0
 */
public abstract class AbstractReefDbMainUIAction extends AbstractMainUIAction<ReefDbUIContext, ReefDbMainUI> {

    /**
     * <p>Constructor for AbstractReefDbMainUIAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     * @param hideBody a boolean.
     */
    protected AbstractReefDbMainUIAction(ReefDbMainUIHandler handler, boolean hideBody) {
        super(handler, hideBody);
    }

    @Override
    public ReefDbMainUIHandler getHandler() {
        return (ReefDbMainUIHandler) super.getHandler();
    }

    @Override
    protected ReefDbConfiguration getConfig() {
        return (ReefDbConfiguration) super.getConfig();
    }

    @Override
    public ReefDbUIContext getContext() {
        return (ReefDbUIContext) super.getContext();
    }
}
