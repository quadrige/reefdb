package fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.local.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.*;

/**
 * Created on 7/6/14.
 */
public class ReplaceUnitUIHandler extends AbstractReefDbUIHandler<ReplaceUnitUIModel, ReplaceUnitUI> implements Cancelable {

    /**
     * Logger.
     */
    private static final Log log =
            LogFactory.getLog(ReplaceUnitUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void afterInit(ReplaceUnitUI ui) {

        initUI(ui);

        ReplaceUnitUIModel model = getModel();
        initBeanFilterableComboBox(ui.getSourceListComboBox(), model.getSourceList(), model.getSelectedSource(), model.getDecoratorContext());
        initBeanFilterableComboBox(ui.getTargetListComboBox(), model.getTargetList(), model.getSelectedTarget(), model.getDecoratorContext());

        SwingValidator validator = ui.getValidator();
        listenValidatorValid(validator, model);

        registerValidators(validator);
    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getSourceListComboBox();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public SwingValidator getValidator() {
        return ui.getValidator();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setValid(false);
        onCloseUI();
    }

}
