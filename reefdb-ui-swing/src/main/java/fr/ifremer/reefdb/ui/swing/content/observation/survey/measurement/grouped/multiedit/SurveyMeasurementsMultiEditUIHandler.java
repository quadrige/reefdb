package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.multiedit;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsMultiEditUIHandler;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableUI;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

/**
 * Controleur pour les mesures de l'observation.
 */
public class SurveyMeasurementsMultiEditUIHandler
    extends AbstractMeasurementsMultiEditUIHandler<SurveyMeasurementsGroupedRowModel, SurveyMeasurementsMultiEditUIModel, SurveyMeasurementsMultiEditUI>
    implements Cancelable {

    /**
     * <p>Constructor for SurveyMeasurementsMultiEditUIHandler.</p>
     */
    public SurveyMeasurementsMultiEditUIHandler() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeasurementsGroupedTableModel<SurveyMeasurementsGroupedRowModel> getTableModel() {
        return (SurveyMeasurementsGroupedTableModel) getTable().getModel();
    }

    @Override
    protected SurveyMeasurementsMultiEditUIModel createNewModel() {
        return new SurveyMeasurementsMultiEditUIModel();
    }

    @Override
    protected SurveyMeasurementsGroupedRowModel createNewRow(boolean readOnly, MeasurementAware parentBean) {
        return new SurveyMeasurementsGroupedRowModel(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getSurveyGroupedMeasurementMultiEditTable();
    }

    @Override
    protected SwingTable getReferentTable() {
        return ((SurveyMeasurementsGroupedTableUI) ApplicationUIUtil.getParentUI(getUI())).getSurveyGroupedMeasurementTable();
    }

    /**
     * Initialisation du tableau.
     */
    @Override
    protected void initTable() {

        // Colonne groupe taxon
        addColumn(
            taxonGroupCellEditor,
            newTableCellRender(SurveyMeasurementsGroupedTableModel.TAXON_GROUP),
            SurveyMeasurementsGroupedTableModel.TAXON_GROUP);

        // Colonne taxon
        addColumn(
            taxonCellEditor,
            newTableCellRender(SurveyMeasurementsGroupedTableModel.TAXON),
            SurveyMeasurementsGroupedTableModel.TAXON);

        // Colonne taxon saisi
        final TableColumnExt colInputTaxon = addColumn(SurveyMeasurementsGroupedTableModel.INPUT_TAXON_NAME);
        colInputTaxon.setEditable(false);

        // Colonne analyste
        addColumn(
            departmentCellEditor,
            newTableCellRender(AbstractMeasurementsGroupedTableModel.ANALYST),
            (ColumnIdentifier<SurveyMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.ANALYST);

        // No comment column because comment editor is also a JDialog
//        final TableColumnExt colComment = addCommentColumn(SurveyMeasurementsGroupedTableModel.COMMENT);

        // Modele de la table
        final SurveyMeasurementsGroupedTableModel tableModel = new SurveyMeasurementsGroupedTableModel(getTable().getColumnModel(), false);
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable());

        // border
        addEditionPanelBorder();
    }


}
