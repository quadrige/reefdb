package fr.ifremer.reefdb.ui.swing.content.synchro.changes.duplicate;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.common.synchro.service.RejectedRow;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroOperationType;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.reefdb.ui.swing.content.synchro.changes.SynchroChangesRowModel;
import fr.ifremer.reefdb.ui.swing.content.synchro.changes.SynchroChangesTableModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.annotation.Nullable;
import javax.swing.SortOrder;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class SynchroDuplicatesUIHandler extends AbstractReefDbTableUIHandler<SynchroChangesRowModel, SynchroDuplicatesUIModel, SynchroDuplicatesUI> implements Cancelable {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SynchroDuplicatesUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SynchroDuplicatesUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SynchroDuplicatesUIModel model = new SynchroDuplicatesUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(SynchroDuplicatesUI ui) {

        // Init UI
        initUI(ui);

        // Init table
        initTable();
        ui.getTablePanel().setVisible(false);
        ui.pack();

        getModel().addPropertyChangeListener(SynchroDuplicatesUIModel.PROPERTY_CHANGES, evt -> populateUI());

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Column name
        final TableColumnExt columnName = addColumn(
                SynchroChangesTableModel.NAME);
        columnName.setSortable(true);
        columnName.setEditable(false);
        columnName.setPreferredWidth(500);
        columnName.setWidth(500);

        // Modele de la table
        final SynchroChangesTableModel tableModel = new SynchroChangesTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Les columns obligatoire sont toujours presentes
        columnName.setHideable(false);

        getTable().setEditable(true);

        // Initialisation de la table
        initTable(getTable(), false, true);

        // Tri par defaut
        getTable().setSortOrder(SynchroChangesTableModel.NAME, SortOrder.ASCENDING);

        getTable().setVisibleRowCount(5);
    }

    private void populateUI() {

        Assert.notNull(getModel().getChanges());

        // This filter is mandatory because this UI handles only one table type
        Assert.notNull(getModel().getTableNameFilter());

        // get the change
        SynchroTableDTO synchroTable = ReefDbBeans.findByProperty(getModel().getChanges().getTables(), SynchroTableDTO.PROPERTY_NAME, getModel().getTableNameFilter());
        Assert.notNull(synchroTable);
        getModel().setTableChange(synchroTable);

        int nbInsert = getInsertRows(synchroTable).size();
        int nbDuplicate = getDuplicateRows(synchroTable).size();
        boolean hasChanges = (nbInsert > 0 || nbDuplicate > 0);

        List<String> strings = new ArrayList<>();

        if (nbInsert > 0) {
            strings.add(t("reefdb.synchro.duplicates.nbInsert", nbInsert, decorate(synchroTable)));
        }
        if (nbDuplicate > 0) {
            strings.add(t("reefdb.synchro.duplicates.nbDuplicate", nbDuplicate, decorate(synchroTable)));
            getUI().getShowDuplicatesButton().setVisible(true);
            // populate table
            populateTable(synchroTable);
        } else {
            getUI().getShowDuplicatesButton().setVisible(false);
        }
        getUI().getChangesLabel().setText(ReefDbUIs.getHtmlString(strings));


        // Display a message on survey ignored because of missing programs (mantis #37518)
        List<SynchroRowDTO> ignoredRows = getRowsByType(synchroTable, SynchroOperationType.IGNORE);
        if (ignoredRows.size() > 0) {
            List<String> progCds = ReefDbBeans.collectProperties(ignoredRows, SynchroRowDTO.PROPERTY_NAME);
            String message = t("reefdb.synchro.duplicates.nbIgnore",
                    ignoredRows.size(),
                    decorate(synchroTable),
                    ReefDbUIs.formatHtmlList(Sets.newHashSet(progCds))); // Use a Set, to remove duplicated prog_cd
            getUI().getIgnoreLabel().setText(ReefDbUIs.getHtmlString(Lists.newArrayList(message)));

            // No other changes: user can only cancel
            if (!hasChanges) {
                getUI().getIgnoreHelpLabel().setText(ReefDbUIs.getHtmlString(t("reefdb.synchro.duplicates.nbIgnore.help.cancelOnly")));
                getUI().getConfirmBouton().setEnabled(false);
                getUI().getChangesLabel().setVisible(false);
            }
            else {
                getUI().getIgnoreHelpLabel().setText(ReefDbUIs.getHtmlString(t("reefdb.synchro.duplicates.nbIgnore.help")));
            }
        }
        else {
            getUI().getIgnorePanel().setVisible(false);
        }


    }

    /**
     * <p>populateTable.</p>
     *
     * @param synchroTable a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO} object.
     */
    protected void populateTable(SynchroTableDTO synchroTable) {

        TableColumnExt nameColumn = getTable().getColumnExt(SynchroChangesTableModel.NAME);

        if (synchroTable == null || CollectionUtils.isEmpty(synchroTable.getRows())) {

            nameColumn.setTitle(t("reefdb.synchro.duplicates.name.short.default"));

        } else {

            nameColumn.setTitle(t("reefdb.synchro.duplicates.name.short", decorate(synchroTable)));
            getModel().setBeans(getDuplicateRows(synchroTable));
        }

    }

    /**
     * <p>showDuplicates.</p>
     */
    public void showDuplicates() {

        getUI().getTablePanel().setVisible(true);
        getUI().getShowDuplicatesButton().setVisible(false);
        getUI().pack();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<SynchroChangesRowModel> getTableModel() {
        return (SynchroChangesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTable();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setChangesValidated(false);
        closeDialog();
    }

    /**
     * <p>confirm.</p>
     */
    public void confirm() {

        // remove old table from changes
        getModel().getChanges().removeTables(getModel().getTableChange());

        // build another SynchroTableDTO
        SynchroTableDTO synchroTable = ReefDbBeanFactory.newSynchroTableDTO();
        synchroTable.setName(getModel().getTableChange().getName());
        synchroTable.setRows(new ArrayList<>());

        // add insertRows by adding strategy
        synchroTable.addAllRows(ReefDbBeans.transformCollection(getInsertRows(getModel().getTableChange()), new Function<SynchroRowDTO, SynchroRowDTO>() {
            @Nullable
            @Override
            public SynchroRowDTO apply(@Nullable SynchroRowDTO input) {
                if (input != null) {
                    input.setStrategy(RejectedRow.ResolveStrategy.UPDATE.toString());
                }
                return input;
            }
        }));

        // add duplicates rows
        synchroTable.addAllRows(ReefDbBeans.transformCollection(getModel().getSelectedBeans(), new Function<SynchroRowDTO, SynchroRowDTO>() {
            @Nullable
            @Override
            public SynchroRowDTO apply(@Nullable SynchroRowDTO input) {
                if (input != null) {
                    input.setStrategy(RejectedRow.ResolveStrategy.DUPLICATE.toString());
                }
                return input;
            }
        }));

        getModel().getChanges().addTables(synchroTable);

        getModel().setChangesValidated(true);
        closeDialog();
    }

    /* -- Internal methods -- */

    private List<SynchroRowDTO> getDuplicateRows(SynchroTableDTO synchroTable) {
        return getRowsByType(synchroTable, SynchroOperationType.DUPLICATE);
    }

    private List<SynchroRowDTO> getInsertRows(SynchroTableDTO synchroTable) {
        return getRowsByType(synchroTable, SynchroOperationType.INSERT);
    }

    private List<SynchroRowDTO> getRowsByType(final SynchroTableDTO synchroTable, final SynchroOperationType operationType) {
        final String typeStr = operationType.name();

        return ReefDbBeans.filterCollection(synchroTable.getRows(), (Predicate<SynchroRowDTO>) input -> input != null && typeStr.equalsIgnoreCase(input.getOperationType()));
    }
}
