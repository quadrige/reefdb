package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.menu.SamplingEquipmentsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.table.SamplingEquipmentsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.table.SamplingEquipmentsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des enginsPrelevement au niveau national
 */
public class SamplingEquipmentsNationalUIHandler extends
        AbstractReefDbTableUIHandler<SamplingEquipmentsTableRowModel, SamplingEquipmentsNationalUIModel, SamplingEquipmentsNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SamplingEquipmentsNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(SamplingEquipmentsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        SamplingEquipmentsNationalUIModel model = new SamplingEquipmentsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(SamplingEquipmentsNationalUI ui) {
        initUI(ui);

        // force national
        ui.getSamplingEquipmentsNationalMenuUI().getHandler().forceLocal(false);

        // init listener on found samplingEquipments
        ui.getSamplingEquipmentsNationalMenuUI().getModel().addPropertyChangeListener(SamplingEquipmentsMenuUIModel.PROPERTY_RESULTS, evt -> {
            if (evt.getNewValue() != null) {
                getModel().setBeans((List<SamplingEquipmentDTO>) evt.getNewValue());
            }
        });

        // listen to 'apply filter' results
        ui.getSamplingEquipmentsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            // load only national referential (Mantis #29668)
            getModel().setBeans(ReefDbBeans.filterNationalReferential((List<SamplingEquipmentDTO>) evt.getNewValue()));
        });

        initTable();

    }

    private void initTable() {

        // mnemonic
        TableColumnExt mnemonicCol = addColumn(SamplingEquipmentsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        TableColumnExt descriptionCol = addColumn(SamplingEquipmentsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // size
        TableColumnExt sizeCol = addColumn(SamplingEquipmentsTableModel.SIZE);
        sizeCol.setSortable(true);
        sizeCol.setEditable(false);

        // unit
        TableColumnExt unitCol = addFilterableComboDataColumnToModel(
                SamplingEquipmentsTableModel.UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.ACTIVE),
                false);
        unitCol.setSortable(true);
        unitCol.setEditable(false);


        // Comment, creation and update dates
        addCommentColumn(SamplingEquipmentsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(SamplingEquipmentsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(SamplingEquipmentsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                SamplingEquipmentsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        SamplingEquipmentsTableModel tableModel = new SamplingEquipmentsTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.samplingEquipments.national"));

        // Initialisation du tableau
        initTable(getTable(), true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
        getModel().setBeans(null);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<SamplingEquipmentsTableRowModel> getTableModel() {
        return (SamplingEquipmentsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getSamplingEquipmentsNationalTable();
    }
}
