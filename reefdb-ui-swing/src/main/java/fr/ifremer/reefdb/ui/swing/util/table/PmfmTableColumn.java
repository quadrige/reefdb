package fr.ifremer.reefdb.ui.swing.util.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.jdesktop.swingx.table.TableColumnExt;

import java.math.BigDecimal;

/**
 * A TableColumnExt for Pmfm
 *
 * Created by Ludovic on 12/06/2015.
 */
public class PmfmTableColumn extends TableColumnExt {

    /**
     * <p>Constructor for PmfmTableColumn.</p>
     *
     * @param columnCount a int.
     */
    public PmfmTableColumn(int columnCount) {
        super(columnCount);
    }

    /**
     * <p>getPmfmIdentifier.</p>
     *
     * @param <R> a R object.
     * @return a {@link ReefDbPmfmColumnIdentifier} object.
     */
    @SuppressWarnings("unchecked")
    public <R extends AbstractReefDbRowUIModel> ReefDbPmfmColumnIdentifier<R> getPmfmIdentifier() {
        if (getIdentifier() instanceof ReefDbPmfmColumnIdentifier){
            return (ReefDbPmfmColumnIdentifier<R>) getIdentifier();
        }
        throw new IllegalArgumentException("the identifier is not instance of ReefDbPmfmColumnIdentifier");
    }

    /**
     * <p>getPmfmId.</p>
     *
     * @return a int.
     */
    public int getPmfmId() {
        return getPmfmIdentifier().getPmfmId();
    }

    /**
     * <p>isNumerical.</p>
     *
     * @return a boolean.
     */
    public boolean isNumerical() {
        return BigDecimal.class.isAssignableFrom(getPmfmIdentifier().getPropertyType());
    }
}
