package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local.password;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIContext;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.spi.UIHandler;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.Arrays;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/29/14.
 */
public class PasswordUIHandler implements UIHandler<PasswordUI> {

    protected PasswordUI ui;
    protected ApplicationUIContext context;

    protected String newPassword;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(PasswordUI ui) {
        this.ui = ui;
        this.context = ReefDbUIContext.getInstance();
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(PasswordUI ui) {
        addAutoSelectOnFocus(ui.getPasswordField());
        addAutoSelectOnFocus(ui.getPassword2Field());
    }

    /**
     * <p>open.</p>
     *
     * @param login a {@link java.lang.String} object.
     */
    public void open(String login) {

        ui.getInfoMessage().setText(t("reefdb.user.password.infoMessage", login));
        ui.pack();

        Container parent = context.getMainUI();
        if (parent != null) {
            SwingUtil.center(parent, ui);
        }
        // set modality to Toolkit : cause ReefDbMainUI to block until this Dialog is closed
        ui.setModalityType(Dialog.ModalityType.TOOLKIT_MODAL);
        ui.setAlwaysOnTop(true);
        ui.setVisible(true);
    }

    /**
     * <p>cancel.</p>
     */
    public void cancel() {
        newPassword = null;
        ui.dispose();
    }

    /**
     * <p>accept.</p>
     */
    public void accept() {

        if (!Arrays.equals(ui.getPasswordField().getPassword(), ui.getPassword2Field().getPassword())) {
            return;
        }

        if (ui.getPasswordField().getPassword().length == 0) {
            if (context.getDialogHelper().showConfirmDialog(ui, t("reefdb.user.password.reset"), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return;
            }
            newPassword = "";
        } else {
            newPassword = String.copyValueOf(ui.getPasswordField().getPassword());
        }
        ui.dispose();
    }

    /**
     * <p>Getter for the field <code>newPassword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    protected String getNewPassword() {
        return newPassword;
    }

    /**
     * <p>addAutoSelectOnFocus.</p>
     *
     * @param jTextField a {@link javax.swing.JTextField} object.
     */
    protected void addAutoSelectOnFocus(JTextField jTextField) {
        jTextField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(final FocusEvent e) {
                SwingUtilities.invokeLater(() -> {
                    JTextField source = (JTextField) e.getSource();
                    source.selectAll();
                });

            }
        });
    }
}
