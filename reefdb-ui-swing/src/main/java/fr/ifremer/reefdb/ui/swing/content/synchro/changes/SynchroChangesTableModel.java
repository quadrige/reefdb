package fr.ifremer.reefdb.ui.swing.content.synchro.changes;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model for change log table.
 */
public class SynchroChangesTableModel extends AbstractReefDbTableModel<SynchroChangesRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final ReefDbColumnIdentifier<SynchroChangesRowModel> NAME = ReefDbColumnIdentifier.newId(
    		SynchroChangesRowModel.PROPERTY_NAME,
            n("reefdb.synchro.changes.name.short"),
            n("reefdb.synchro.changes.name.tip"),
            String.class);

	/**
	 * Identifier for column operation type
	 */
	public static final ReefDbColumnIdentifier<SynchroChangesRowModel> OPERATION_TYPE = ReefDbColumnIdentifier.newId(
			SynchroChangesRowModel.PROPERTY_OPERATION_TYPE,
			null,
			n("reefdb.synchro.changes.operationType.tip"),
			String.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public SynchroChangesTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public SynchroChangesRowModel createNewRow() {
		return new SynchroChangesRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<SynchroChangesRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
