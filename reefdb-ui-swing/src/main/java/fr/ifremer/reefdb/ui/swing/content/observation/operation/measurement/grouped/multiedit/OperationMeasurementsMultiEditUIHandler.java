package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.multiedit;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableUI;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsMultiEditUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

/**
 * Controleur pour les mesures de l'observation.
 */
public class OperationMeasurementsMultiEditUIHandler
    extends AbstractMeasurementsMultiEditUIHandler<OperationMeasurementsGroupedRowModel, OperationMeasurementsMultiEditUIModel, OperationMeasurementsMultiEditUI>
    implements Cancelable {

    /**
     * <p>Constructor for OperationMeasurementsMultiEditUIHandler.</p>
     */
    public OperationMeasurementsMultiEditUIHandler() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeasurementsGroupedTableModel<OperationMeasurementsGroupedRowModel> getTableModel() {
        return (OperationMeasurementsGroupedTableModel) getTable().getModel();
    }

    @Override
    protected OperationMeasurementsMultiEditUIModel createNewModel() {
        return new OperationMeasurementsMultiEditUIModel();
    }

    @Override
    protected OperationMeasurementsGroupedRowModel createNewRow(boolean readOnly, MeasurementAware parentBean) {
        return new OperationMeasurementsGroupedRowModel(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getOperationGroupedMeasurementMultiEditTable();
    }

    @Override
    protected SwingTable getReferentTable() {
        return ((OperationMeasurementsGroupedTableUI) ApplicationUIUtil.getParentUI(getUI())).getOperationGroupedMeasurementTable();
    }

    /**
     * Initialisation du tableau.
     */
    @Override
    protected void initTable() {

        final TableColumnExt samplingCol = addColumn(OperationMeasurementsGroupedTableModel.SAMPLING);
        samplingCol.setEditable(false);

        // Colonne groupe taxon
        addColumn(
            taxonGroupCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON_GROUP),
            OperationMeasurementsGroupedTableModel.TAXON_GROUP);

        // Colonne taxon
        addColumn(
            taxonCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON),
            OperationMeasurementsGroupedTableModel.TAXON);

        // Colonne taxon saisi
        final TableColumnExt colInputTaxon = addColumn(OperationMeasurementsGroupedTableModel.INPUT_TAXON_NAME);
        colInputTaxon.setEditable(false);

        // Colonne analyste
        addColumn(
            departmentCellEditor,
            newTableCellRender(AbstractMeasurementsGroupedTableModel.ANALYST),
            (ColumnIdentifier<OperationMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.ANALYST);

        // No comment column because comment editor is also a JDialog
//        final TableColumnExt colComment = addCommentColumn(OperationMeasurementsGroupedTableModel.COMMENT);

        // Modele de la table
        final OperationMeasurementsGroupedTableModel tableModel = new OperationMeasurementsGroupedTableModel(getTable().getColumnModel(), false);
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable());

        // border
        addEditionPanelBorder();
    }


}
