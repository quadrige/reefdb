package fr.ifremer.reefdb.ui.swing.util.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.table.PmfmColumnIdentifier;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationAware;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.math.BigDecimal;
import java.util.List;

/**
 * Colonne identifier pour colonne dynamic.
 *
 * @param <R> Objet de la colonne
 */
public class ReefDbPmfmColumnIdentifier<R extends AbstractReefDbRowUIModel> extends ReefDbColumnIdentifier<R> implements PmfmColumnIdentifier {

    private String headerLabel;

    private String headerLabelTip;

    private PmfmDTO pmfm;

    private boolean notMandatoryOverride = false;

    private final transient PropertyChangeSupport pcs;

    /**
     * <p>newId.</p>
     *
     * @param propertyName a {@link java.lang.String} object.
     * @param pmfm a {@link PmfmDTO} object.
     * @param headerLabel a {@link java.lang.String} object.
     * @param headerLabelTip a {@link java.lang.String} object.
     * @param propertyType a {@link java.lang.Class} object.
     * @param <R> a R object.
     * @return a {@link ReefDbPmfmColumnIdentifier} object.
     */
    public static <R extends AbstractReefDbRowUIModel> ReefDbPmfmColumnIdentifier<R> newId(
            String propertyName, PmfmDTO pmfm, String headerLabel, String headerLabelTip, Class<?> propertyType) {
        return new ReefDbPmfmColumnIdentifier<>(propertyName, pmfm, headerLabel, headerLabelTip, propertyType, null, false);
    }

    /**
     * <p>newId.</p>
     *
     * @param propertyName a {@link java.lang.String} object.
     * @param pmfm a {@link PmfmDTO} object.
     * @param headerLabel a {@link java.lang.String} object.
     * @param headerLabelTip a {@link java.lang.String} object.
     * @param propertyType a {@link java.lang.Class} object.
     * @param decoratorName a {@link java.lang.String} object.
     * @param <R> a R object.
     * @return a {@link ReefDbPmfmColumnIdentifier} object.
     */
    public static <R extends AbstractReefDbRowUIModel> ReefDbPmfmColumnIdentifier<R> newId(
            String propertyName, PmfmDTO pmfm, String headerLabel, String headerLabelTip, Class<?> propertyType, String decoratorName) {
        return new ReefDbPmfmColumnIdentifier<>(propertyName, pmfm, headerLabel, headerLabelTip, propertyType, decoratorName, false);
    }

    /** {@inheritDoc} */
    public static <R extends AbstractReefDbRowUIModel> ReefDbPmfmColumnIdentifier<R> newId(
            String propertyName, PmfmDTO pmfm, String headerLabel, String headerLabelTip, Class<?> propertyType, boolean mandatory) {
        return new ReefDbPmfmColumnIdentifier<>(propertyName, pmfm, headerLabel, headerLabelTip, propertyType, null, mandatory);
    }

    /**
     * <p>newId.</p>
     *
     * @param propertyName a {@link java.lang.String} object.
     * @param pmfm a {@link PmfmDTO} object.
     * @param headerLabel a {@link java.lang.String} object.
     * @param headerLabelTip a {@link java.lang.String} object.
     * @param propertyType a {@link java.lang.Class} object.
     * @param decoratorName a {@link java.lang.String} object.
     * @param mandatory a boolean.
     * @param <R> a R object.
     * @return a {@link ReefDbPmfmColumnIdentifier} object.
     */
    public static <R extends AbstractReefDbRowUIModel> ReefDbPmfmColumnIdentifier<R> newId(
            String propertyName, PmfmDTO pmfm, String headerLabel, String headerLabelTip, Class<?> propertyType, String decoratorName, boolean mandatory) {
        return new ReefDbPmfmColumnIdentifier<>(propertyName, pmfm, headerLabel, headerLabelTip, propertyType, decoratorName, mandatory);
    }

    /**
     * <p>newReadOnlyId.</p>
     *
     * @param propertyName a {@link java.lang.String} object.
     * @param pmfm a {@link PmfmDTO} object.
     * @param headerLabel a {@link java.lang.String} object.
     * @param headerLabelTip a {@link java.lang.String} object.
     * @param propertyType a {@link java.lang.Class} object.
     * @param <R> a R object.
     * @return a {@link ReefDbPmfmColumnIdentifier} object.
     */
    public static <R extends AbstractReefDbRowUIModel> ReefDbPmfmColumnIdentifier<R> newReadOnlyId(
            String propertyName, PmfmDTO pmfm, String headerLabel, String headerLabelTip, Class<?> propertyType) {
        return new ReefDbPmfmColumnIdentifier<R>(propertyName, pmfm, headerLabel, headerLabelTip, propertyType, null, false) {

            private static final long serialVersionUID = 1L;

            @Override
            public void setValue(R entry, Object value) {
                // no set
            }
        };
    }

    /**
     * <p>Constructor for ReefDbPmfmColumnIdentifier.</p>
     *
     * @param propertyName a {@link java.lang.String} object.
     * @param pmfm a {@link PmfmDTO} object.
     * @param headerLabel a {@link java.lang.String} object.
     * @param headerLabelTip a {@link java.lang.String} object.
     * @param propertyType a {@link java.lang.Class} object.
     * @param decoratorName a {@link java.lang.String} object.
     * @param mandatory a boolean.
     */
    protected ReefDbPmfmColumnIdentifier(
            final String propertyName,
            final PmfmDTO pmfm,
            final String headerLabel,
            final String headerLabelTip,
            final Class<?> propertyType,
            final String decoratorName,
            final boolean mandatory) {
        super(propertyName, null, null, propertyType, decoratorName, mandatory);

        this.pmfm = pmfm;
        this.headerLabel = headerLabel;
        this.headerLabelTip = headerLabelTip;

        pcs = new PropertyChangeSupport(this);
    }

    /** {@inheritDoc} */
    @Override
    public Object getValue(R entry) {
        Object result = null;
        if (getPropertyName() != null && entry != null) {
            result = getProperty(entry, getPropertyName());
        }
        return result;
    }

    private Object getProperty(final Object bean, final String property) {
        Assert.notNull(bean);
        Assert.notBlank(property);
        // might not be assignable
        Assert.isInstanceOf(MeasurementAware.class, bean);

        MeasurementAware measurementBean = (MeasurementAware) bean;
        boolean isIndividual = property.equals(SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS);

        List<MeasurementDTO> measurements = isIndividual
                ? measurementBean.getIndividualMeasurements()
                : measurementBean.getMeasurements();

        MeasurementDTO measurement = ReefDbBeans.findByProperty(measurements, ReefDbBeans.PROPERTY_PMFM_ID, getPmfmId());
        if (measurement != null) {
            if (measurement.getPmfm().getParameter().isQualitative()) {
                return measurement.getQualitativeValue();
            } else if (measurement.getNumericalValue() != null) {
                return measurement.getNumericalValue();
            }
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public void setValue(final R entry, final Object value) {
        if (getPropertyName() != null) {

            Object oldValue = getValue(entry);

            setProperty(entry, getPropertyName(), value);

            // fire property change on entry
            entry.fireIndexedPropertyChanged(getPropertyName(), getPmfmId(), oldValue, value);
        }
    }

    private void setProperty(final Object bean, final String property, final Object value) {
        Assert.notNull(bean);
        Assert.notNull(property);
        Assert.isInstanceOf(MeasurementAware.class, bean);

        MeasurementAware measurementBean = (MeasurementAware) bean;

        boolean isIndividual = property.equals(SurveyDTO.PROPERTY_INDIVIDUAL_PMFMS);
        List<PmfmDTO> pmfms = isIndividual
                ? measurementBean.getIndividualPmfms()
                : measurementBean.getPmfms();
        Assert.isTrue(pmfms.contains(pmfm), "The PMFMU is this identifier is not part of measurements PMFMU");

        List<MeasurementDTO> measurements = isIndividual
                ? measurementBean.getIndividualMeasurements()
                : measurementBean.getMeasurements();

        // assert value and pmfm type
        if (value != null) {
            if (pmfm.getParameter().isQualitative()) {
                Assert.isInstanceOf(QualitativeValueDTO.class, value);
            } else {
                Assert.isInstanceOf(BigDecimal.class, value);
            }
        }

        SamplingOperationDTO samplingOperation = null;
        if (bean instanceof SamplingOperationDTO) {
            samplingOperation = (SamplingOperationDTO) bean;
        } else if (bean instanceof SamplingOperationAware) {
            samplingOperation = ((SamplingOperationAware) bean).getSamplingOperation();
        }

        MeasurementDTO measurement = ReefDbBeans.findByProperty(measurements, ReefDbBeans.PROPERTY_PMFM_ID, getPmfmId());
        String measurementProperty;
        if (pmfm.getParameter().isQualitative()) {
            measurementProperty = MeasurementDTO.PROPERTY_QUALITATIVE_VALUE;
        } else {
            measurementProperty = MeasurementDTO.PROPERTY_NUMERICAL_VALUE;
        }

        if (value != null) {

            // Create measurement if not exists
            if (measurement == null) {
                measurement = ReefDbBeanFactory.newMeasurementDTO();
                measurement.setSamplingOperation(samplingOperation);
                measurement.setPmfm(pmfm);
                measurements.add(measurement);
            }

            // Set the correct value
            if (pmfm.getParameter().isQualitative()) {

                // Set the qualitative value
                measurement.setQualitativeValue((QualitativeValueDTO) value);

            } else {

                // Set the numerical value
                BigDecimal bigDecimal = (BigDecimal) value;
                measurement.setNumericalValue(bigDecimal);
                // Set digit count (but NOT precision - mantis #37438)
                measurement.setDigitNb(bigDecimal.scale());

            }
        } else {

            // remove measurement if value is null
            if (measurement != null) {
                measurements.remove(measurement);
            }
        }

        // create and fire event
        PmfmChangeEvent event = new PmfmChangeEvent(this, samplingOperation, measurementProperty, value);
        pcs.firePropertyChange(event);
    }

    /**
     * <p>Getter for the field <code>headerLabel</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHeaderLabel() {
        return headerLabel;
    }

    /**
     * <p>Getter for the field <code>headerLabelTip</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHeaderLabelTip() {
        return headerLabelTip;
    }

    /**
     * <p>Getter for the field <code>pmfmId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public int getPmfmId() {
        return pmfm.getId();
    }

    public PmfmDTO getPmfm() {
        return pmfm;
    }

    /**
     * <p>setNotMandatory.</p>
     */
    public void setNotMandatory() {
        this.notMandatoryOverride = true;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMandatory() {
        return !notMandatoryOverride && super.isMandatory();
    }

    /**
     * <p>setPropertyChangeListener.</p>
     *
     * @param listener a {@link java.beans.PropertyChangeListener} object.
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    private void removeAllListeners() {
        for (PropertyChangeListener listener : pcs.getPropertyChangeListeners()) {
            pcs.removePropertyChangeListener(listener);
        }
    }

    public class PmfmChangeEvent extends PropertyChangeEvent {

        private final SamplingOperationDTO samplingOperation;

        /**
         * Constructs a new <code>PropertyChangeEvent</code>.
         *
         * @param source            The bean that fired the event.
         * @param propertyName      The programmatic name of the property
         *                          that was changed.
         * @param samplingOperation The measurement.
         * @param newValue          The new value of the property.
         */
        PmfmChangeEvent(Object source, SamplingOperationDTO samplingOperation, String propertyName, Object newValue) {
            super(source, propertyName, null, newValue);
            this.samplingOperation = samplingOperation;
        }

        public SamplingOperationDTO getSamplingOperation() {
            return samplingOperation;
        }
    }
}
