package fr.ifremer.reefdb.ui.swing.content.extraction.filters;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;

/**
 * extractions table row model
 */
public class ExtractionsFiltersRowModel extends AbstractReefDbRowUIModel<QuadrigeBean, ExtractionsFiltersRowModel> implements ErrorAware {

    private FilterTypeDTO filterType;
    /** Constant <code>PROPERTY_FILTER_TYPE="filterType"</code> */
    public static final String PROPERTY_FILTER_TYPE = "filterType";

    private FilterDTO filter;
    /** Constant <code>PROPERTY_FILTER="filter"</code> */
    public static final String PROPERTY_FILTER = "filter";

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for ExtractionsFiltersRowModel.</p>
     */
    public ExtractionsFiltersRowModel() {
        super(null, null);
        errors = Lists.newArrayList();
    }

    /** {@inheritDoc} */
    @Override
    protected ExtractionDTO newBean() {
        return null;
    }

    /**
     * <p>getFilterTypeId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFilterTypeId() {
        return filterType == null ? null : filterType.getId();
    }

    /**
     * <p>Getter for the field <code>filterType</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO} object.
     */
    public FilterTypeDTO getFilterType() {
        return filterType;
    }

    /**
     * <p>Setter for the field <code>filterType</code>.</p>
     *
     * @param filterType a {@link fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO} object.
     */
    public void setFilterType(FilterTypeDTO filterType) {
        this.filterType = filterType;
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public FilterDTO getFilter() {
        return filter;
    }

    /**
     * <p>Setter for the field <code>filter</code>.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public void setFilter(FilterDTO filter) {
        this.filter = filter;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    /**
     * <p>isFilterEmpty.</p>
     *
     * @return a boolean.
     */
    public boolean isFilterEmpty() {
        return getFilter() == null || CollectionUtils.isEmpty(getFilter().getElements());
    }

    /**
     * <p>getFilteredElements.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<? extends QuadrigeBean> getFilteredElements() {
        return isFilterEmpty() ? null : getFilter().getElements();
    }
}
