package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ManageContextsListTableUITableModel extends AbstractReefDbTableModel<ManageContextsListTableUIRowModel> {

	/** Constant <code>LIBELLE</code> */
	public static final ReefDbColumnIdentifier<ManageContextsListTableUIRowModel> LIBELLE = ReefDbColumnIdentifier.newId(
			ManageContextsListTableUIRowModel.PROPERTY_NAME,
			n("reefdb.property.name"),
			n("reefdb.context.contextsList.name.tip"),
			String.class,
            true);
	
	/** Constant <code>DESCRIPTION</code> */
	public static final ReefDbColumnIdentifier<ManageContextsListTableUIRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
			ManageContextsListTableUIRowModel.PROPERTY_DESCRIPTION,
			n("reefdb.property.description"),
			n("reefdb.context.contextsList.description.tip"),
			String.class);
 
	/**
	 * <p>Constructor for ManageContextsListTableUITableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	public ManageContextsListTableUITableModel(final TableColumnModelExt columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public ManageContextsListTableUIRowModel createNewRow() {
		return new ManageContextsListTableUIRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ManageContextsListTableUIRowModel> getFirstColumnEditing() {
		return LIBELLE;
	}
}
