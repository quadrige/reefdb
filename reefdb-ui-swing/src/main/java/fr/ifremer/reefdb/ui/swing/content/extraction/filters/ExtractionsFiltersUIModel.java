package fr.ifremer.reefdb.ui.swing.content.extraction.filters;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIModel;
import fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

import java.util.List;

/**
 * Created by Ludovic on 02/12/2015.
 */
public class ExtractionsFiltersUIModel extends AbstractReefDbTableUIModel<QuadrigeBean, ExtractionsFiltersRowModel, ExtractionsFiltersUIModel> {

    private ExtractionUIModel extractionUIModel;

    private List<FilterTypeDTO> filterTypes;

    /**
     * <p>Getter for the field <code>extractionUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIModel} object.
     */
    public ExtractionUIModel getExtractionUIModel() {
        return extractionUIModel;
    }

    /**
     * <p>Setter for the field <code>extractionUIModel</code>.</p>
     *
     * @param extractionUIModel a {@link fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIModel} object.
     */
    public void setExtractionUIModel(ExtractionUIModel extractionUIModel) {
        this.extractionUIModel = extractionUIModel;
    }

    /**
     * <p>getSelectedExtraction.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsRowModel} object.
     */
    public ExtractionsRowModel getSelectedExtraction() {
        return extractionUIModel == null ? null : extractionUIModel.getSelectedExtraction();
    }

    /**
     * <p>Getter for the field <code>filterTypes</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<FilterTypeDTO> getFilterTypes() {
        return filterTypes;
    }

    /**
     * <p>Setter for the field <code>filterTypes</code>.</p>
     *
     * @param filterTypes a {@link java.util.List} object.
     */
    public void setFilterTypes(List<FilterTypeDTO> filterTypes) {
        this.filterTypes = filterTypes;
    }

}
