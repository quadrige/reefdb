package fr.ifremer.reefdb.ui.swing.content.manage.context.filtercontent;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ManageFilterContentTableUITableModel extends AbstractReefDbTableModel<ManageFilterContentTableUIRowModel> {

	/** Constant <code>LABEL</code> */
	public static final ReefDbColumnIdentifier<ManageFilterContentTableUIRowModel> LABEL = ReefDbColumnIdentifier.newId(
			ManageFilterContentTableUIRowModel.PROPERTY_LABEL,
			n("reefdb.filter.selected.label"),
			n("reefdb.filter.selected.tip"),
			String.class);
 
	/**
	 * <p>Constructor for ManageFilterContentTableUITableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	public ManageFilterContentTableUITableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ManageFilterContentTableUIRowModel createNewRow() {
		return new ManageFilterContentTableUIRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ManageFilterContentTableUIRowModel> getFirstColumnEditing() {
		return LABEL;
	}
}
