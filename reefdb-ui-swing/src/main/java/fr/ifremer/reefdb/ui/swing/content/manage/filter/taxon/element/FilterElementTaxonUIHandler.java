package fr.ifremer.reefdb.ui.swing.content.manage.filter.taxon.element;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.menu.TaxonMenuUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Controler.
 */
public class FilterElementTaxonUIHandler extends AbstractFilterElementUIHandler<TaxonDTO, FilterElementTaxonUI, TaxonMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(FilterElementTaxonUIHandler.class);

    /** {@inheritDoc} */
    @Override
    protected TaxonMenuUI createNewReferentialMenuUI() {
        return new TaxonMenuUI(getUI());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(FilterElementTaxonUI ui) {

        // Force bean type on double list
        getUI().getFilterDoubleList().setBeanType(TaxonDTO.class);
        // Set specific decorator context
        getUI().getFilterDoubleList().setDecoratorContext(DecoratorService.WITH_CITATION_AND_REFERENT);

        super.afterInit(ui);

        // hide name editor by default
        getReferentialMenuUI().getNameEditor().getParent().setVisible(false);
    }


}
