package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.numerical;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.referential.ReferentialNameAlphanumericComparator;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.NumericPreconditionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePrecondNumUIHandler
        extends AbstractReefDbTableUIHandler<RulePrecondNumRowModel, RulePrecondNumUIModel, RulePrecondNumUI>
        implements Cancelable {

    private static final Log LOG = LogFactory.getLog(RulePrecondNumUIHandler.class);

    @Override
    public void beforeInit(RulePrecondNumUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new RulePrecondNumUIModel());
    }

    @Override
    public void afterInit(RulePrecondNumUI rulePrecondNumUI) {
        initUI(rulePrecondNumUI);

        initTable();

        initListeners();

        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    private void initTable() {

        TableColumnExt qvCol = addColumn(
                null,
                newTableCellRender(RulePrecondNumTableModel.QUALITATIVE_VALUE),
                RulePrecondNumTableModel.QUALITATIVE_VALUE);
        qvCol.setSortable(true);
        qvCol.setEditable(false);

        addColumn(
                newNumberCellEditor(Double.class, true, ReefDbUI.SIGNED_HIGH_DECIMAL_DIGITS_PATTERN),
                newNumberCellRenderer(10),
                RulePrecondNumTableModel.MIN);
        addColumn(
                newNumberCellEditor(Double.class, true, ReefDbUI.SIGNED_HIGH_DECIMAL_DIGITS_PATTERN),
                newNumberCellRenderer(10),
                RulePrecondNumTableModel.MAX);

        RulePrecondNumTableModel tableModel = new RulePrecondNumTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable(), true);

        // Mantis #42541 Allow Qualitative value name sorting with enhanced sorting comparator
        getSortController().setComparator(qvCol.getModelIndex(), ReferentialNameAlphanumericComparator.instance());
        getTable().setSortOrder(RulePrecondNumTableModel.QUALITATIVE_VALUE, SortOrder.ASCENDING);
        getTable().setHorizontalScrollEnabled(false);
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {

                case RulePrecondNumUIModel.PROPERTY_BASE_PMFM:
                    initBasePmfm();
                    break;

                case RulePrecondNumUIModel.PROPERTY_USED_PMFM:
                    initUsedPmfm();
                    break;

                case RulePrecondNumUIModel.PROPERTY_BEANS_LOADED:
                    updateTable();
                    break;
            }
        });
    }

    /**
     * Update the table content with missing qualitative values
     */
    private void updateTable() {
        Assert.notNull(getModel().getBasePmfm());

        List<QualitativeValueDTO> existingValues = getModel().getBeans().stream().map(NumericPreconditionDTO::getQualitativeValue).collect(Collectors.toList());

        List<QualitativeValueDTO> missingValues = ListUtils.removeAll(
                getContext().getReferentialService().getUniquePmfmFromPmfm(getModel().getBasePmfm()).getQualitativeValues(),
                existingValues);

        missingValues.forEach(qualitativeValueDTO -> {
            NumericPreconditionDTO newBean = ReefDbBeanFactory.newNumericPreconditionDTO();
            newBean.setQualitativeValue(qualitativeValueDTO);
            getModel().addBean(newBean);
        });
    }

    private void initBasePmfm() {
        PmfmDTO basePmfm = getModel().getBasePmfm();
        if (basePmfm == null) {
            LOG.error("base PMFMU should be set");
            return;
        }
        if (!basePmfm.getParameter().isQualitative()) {
            LOG.error("base PMFMU should be qualitative");
            return;
        }

        initLabel();

        // select first line
        if (getModel().getRowCount() > 0)
            SwingUtilities.invokeLater(() -> selectCell(0, null));
    }

    private void initUsedPmfm() {
        PmfmDTO usedPmfm = getModel().getUsedPmfm();
        if (usedPmfm == null) {
            LOG.error("used PMFMU should be set");
            return;
        }
        if (usedPmfm.getParameter().isQualitative()) {
            LOG.error("used PMFMU should be numerical");
            return;
        }

        initLabel();
    }

    private void initLabel() {
        if (getModel().getBasePmfm() == null || getModel().getUsedPmfm() == null) return;

        ((TitledBorder) getUI().getNumericalPreconditionPanel().getBorder()).setTitle(
                t("reefdb.rule.rulePrecondition.numerical.table.title",
                    decorate(getModel().getBasePmfm(), DecoratorService.NAME_WITH_UNIT),
                    decorate(getModel().getUsedPmfm(), DecoratorService.NAME_WITH_UNIT)
                )
        );
        getUI().getNumericalPreconditionPanel().setToolTipText(
                t("reefdb.rule.rulePrecondition.numerical.table.title",
                        decorate(getModel().getBasePmfm()),
                        decorate(getModel().getUsedPmfm())
                )
        );

    }

    @Override
    public SwingValidator<RulePrecondNumUIModel> getValidator() {
        return getUI().getValidator();
    }

    public void valid() {
        if (getModel().isValid()) {
            stopListenValidatorValid(getValidator());
            closeDialog();
        }
    }

    @Override
    public void cancel() {
        getModel().setModify(false);
//        getModel().setValid(false);
        stopListenValidatorValid(getValidator());
        closeDialog();
    }

    @Override
    public AbstractTableModel<RulePrecondNumRowModel> getTableModel() {
        return (RulePrecondNumTableModel) getTable().getModel();
    }

    @Override
    public SwingTable getTable() {
        return getUI().getNumericalPreconditionTable();
    }
}
