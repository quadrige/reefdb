package fr.ifremer.reefdb.ui.swing.content.manage.campaign.menu;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.CampaignsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.CampaignsUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * Action permettant d effacer la recherche.
 */
public class ClearAction extends AbstractCheckModelAction<CampaignsMenuUIModel, CampaignsMenuUI, CampaignsMenuUIHandler> {

	/**
	 * Constructor.
	 *
	 * @param handler Controller
	 */
	public ClearAction(final CampaignsMenuUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() throws Exception {
		
		getModel().clear();
        // remove selected item directly because model binding can't works here
		getUI().getStartDateCombo().setSelectedItem(null);
		getUI().getEndDateCombo().setSelectedItem(null);

    	// Suppression des donnees dans les tableaux
        if (getLocalModel() != null && getLocalModel().getCampaignsTableUIModel() != null)
        	getLocalModel().getCampaignsTableUIModel().setBeans(null);
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
		return SaveAction.class;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelModify() {
        final CampaignsUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

	/** {@inheritDoc} */
	@Override
	protected void setModelModify(boolean modelModify) {
		CampaignsUIModel model = getLocalModel();
		if (model != null) {
			model.setModify(modelModify);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelValid() {
        final CampaignsUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

	/** {@inheritDoc} */
	@Override
	protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
		return getHandler().getCampaignsUI().getHandler();
	}

	private CampaignsUIModel getLocalModel() {
		final CampaignsUI ui = getHandler().getCampaignsUI();
		if (ui != null) {
			return ui.getModel();
		}
		return null;
	}

}
