package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import fr.ifremer.reefdb.dto.enums.ControlFunctionValues;
import fr.ifremer.reefdb.dto.FunctionDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlRuleTableModel extends AbstractReefDbTableModel<ControlRuleRowModel> {

    /**
     * Constant <code>CODE</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> CODE = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_CODE,
            n("reefdb.property.code"),
            n("reefdb.rule.controlRule.code.tip"),
            String.class);

    /**
     * Constant <code>FUNCTION</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> FUNCTION = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_FUNCTION,
            n("reefdb.rule.controlRule.function.short"),
            n("reefdb.rule.controlRule.function.tip"),
            FunctionDTO.class, true);

    /**
     * Constant <code>CONTROL_ELEMENT</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> CONTROL_ELEMENT = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_CONTROL_ELEMENT,
            n("reefdb.rule.controlRule.controlElement.short"),
            n("reefdb.rule.controlRule.controlElement.tip"),
            ControlElementDTO.class, true);

    /**
     * Constant <code>CONTROL_FEATURE</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> CONTROL_FEATURE = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_CONTROL_FEATURE,
            n("reefdb.rule.controlRule.controlFeature.short"),
            n("reefdb.rule.controlRule.controlFeature.tip"),
            ControlFeatureDTO.class, true);

    /**
     * Constant <code>ACTIVE</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> ACTIVE = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_ACTIVE,
            n("reefdb.rule.controlRule.active.short"),
            n("reefdb.rule.controlRule.active.tip"),
            Boolean.class);

    /**
     * Constant <code>BLOCKING</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> BLOCKING = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_BLOCKING,
            n("reefdb.rule.controlRule.blocking.short"),
            n("reefdb.rule.controlRule.blocking.tip"),
            Boolean.class);

    /**
     * Constant <code>MIN</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> MIN = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_MIN,
            n("reefdb.rule.controlRule.min.short"),
            n("reefdb.rule.controlRule.min.tip"),
            Object.class);

    /**
     * Constant <code>MAX</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> MAX = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_MAX,
            n("reefdb.rule.controlRule.max.short"),
            n("reefdb.rule.controlRule.max.tip"),
            Object.class);

    /**
     * Constant <code>ALLOWED_VALUES</code>
     */
    static final ReefDbColumnIdentifier<ControlRuleRowModel> ALLOWED_VALUES = ReefDbColumnIdentifier.newId(
            ControlRuleRowModel.PROPERTY_ALLOWED_VALUES,
            n("reefdb.rule.controlRule.allowedValues.short"),
            n("reefdb.rule.controlRule.allowedValues.tip"),
            String.class);

    /**
     * <p>Constructor for ControlRuleTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    ControlRuleTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, true, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControlRuleRowModel createNewRow() {
        return new ControlRuleRowModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReefDbColumnIdentifier<ControlRuleRowModel> getFirstColumnEditing() {
        return FUNCTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<ControlRuleRowModel> propertyName) {
        boolean editable = super.isCellEditable(rowIndex, columnIndex, propertyName);

        if (editable) {
            if (MIN.equals(propertyName) || MAX.equals(propertyName)) {
                ControlRuleRowModel rowModel = getEntry(rowIndex);
                FunctionDTO function = rowModel.getFunction();
                editable = ControlFunctionValues.MIN_MAX.equals(function) || ControlFunctionValues.MIN_MAX_DATE.equals(function);
//            } else if (ALLOWED_VALUES.equals(propertyName)) {
//                ControlRuleRowModel rowModel = getEntry(rowIndex);
//                editable = ControlFunctionValues.IS_AMONG.equals(rowModel.getFunction())
//                        || (ReefDbBeans.isPreconditionRule(rowModel) && rowModel.isPmfmValid());
            } else if (CONTROL_ELEMENT.equals(propertyName)
                    || CONTROL_FEATURE.equals(propertyName)
                    || BLOCKING.equals(propertyName)) {
                ControlRuleRowModel rowModel = getEntry(rowIndex);
                editable = !ReefDbBeans.isPreconditionRule(rowModel) && !ReefDbBeans.isGroupedRule(rowModel);
            }
        }

        if (ALLOWED_VALUES.equals(propertyName)) {
            ControlRuleRowModel rowModel = getEntry(rowIndex);
            editable = ControlFunctionValues.IS_AMONG.equals(rowModel.getFunction())
                || (ReefDbBeans.isPreconditionRule(rowModel) && rowModel.isPmfmValid());
        }

        return editable;
    }
}
