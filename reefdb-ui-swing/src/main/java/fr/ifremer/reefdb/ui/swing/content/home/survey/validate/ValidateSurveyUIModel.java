package fr.ifremer.reefdb.ui.swing.content.home.survey.validate;

/*-
 * #%L
 * Reef DB :: UI
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * @author peck7 on 26/09/2017.
 */
public class ValidateSurveyUIModel extends AbstractEmptyUIModel<ValidateSurveyUIModel> {

    private boolean unValidation;
    public static final String PROPERTY_UN_VALIDATION = "unValidation";

    private String comment;
    public static final String PROPERTY_COMMENT = "comment";

    public boolean isUnValidation() {
        return unValidation;
    }

    public void setUnValidation(boolean unValidation) {
        boolean oldValue = isUnValidation();
        this.unValidation = unValidation;
        firePropertyChange(PROPERTY_UN_VALIDATION, oldValue, unValidation);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        String oldValue = getComment();
        this.comment = comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }
}
