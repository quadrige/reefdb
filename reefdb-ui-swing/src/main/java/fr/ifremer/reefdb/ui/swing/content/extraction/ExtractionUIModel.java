package fr.ifremer.reefdb.ui.swing.content.extraction;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.content.extraction.filters.ExtractionsFiltersUIModel;
import fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsTableUIModel;

/**
 * Home model.
 */
public class ExtractionUIModel extends AbstractEmptyUIModel<ExtractionUIModel> {

    /** Constant <code>PROPERTY_PROGRAM="program"</code> */
    public static final String PROPERTY_PROGRAM = "program";
    /** Constant <code>PROPERTY_EXTRACTION="extraction"</code> */
    public static final String PROPERTY_EXTRACTION = "extraction";
    /** Constant <code>PROPERTY_SELECTED_EXTRACTION="selectedExtraction"</code> */
    public static final String PROPERTY_SELECTED_EXTRACTION = "selectedExtraction";
    /** Constant <code>PROPERTY_CONTEXT="context"</code> */
    public static final String PROPERTY_CONTEXT = "context";
    private ProgramDTO program;
    private ExtractionDTO extraction;
    private ExtractionsRowModel selectedExtraction;
    private ExtractionsTableUIModel extractionsTableUIModel;
    private ExtractionsFiltersUIModel extractionsFiltersUIModel;
    private ContextDTO context;

    private boolean adjusting;

    /**
     * <p>isAdjusting.</p>
     *
     * @return a boolean.
     */
    public boolean isAdjusting() {
        return adjusting;
    }

    /**
     * <p>Setter for the field <code>adjusting</code>.</p>
     *
     * @param adjusting a boolean.
     */
    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    /**
     * <p>Getter for the field <code>program</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public ProgramDTO getProgram() {
        return program;
    }

    /**
     * <p>Setter for the field <code>program</code>.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void setProgram(ProgramDTO program) {
        ProgramDTO oldValue = getProgram();
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
    }

    /**
     * <p>getProgramCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProgramCode() {
        return getProgram() == null ? null : getProgram().getCode();
    }

    /**
     * <p>Getter for the field <code>extraction</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO} object.
     */
    public ExtractionDTO getExtraction() {
        return extraction;
    }

    /**
     * <p>Setter for the field <code>extraction</code>.</p>
     *
     * @param extraction a {@link fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO} object.
     */
    public void setExtraction(ExtractionDTO extraction) {
        ExtractionDTO oldValue = getExtraction();
        this.extraction = extraction;
        firePropertyChange(PROPERTY_EXTRACTION, oldValue, extraction);
    }

    /**
     * <p>getExtractionId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getExtractionId() {
        return extraction == null ? null : extraction.getId();
    }

    /**
     * <p>Getter for the field <code>selectedExtraction</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsRowModel} object.
     */
    public ExtractionsRowModel getSelectedExtraction() {
        return selectedExtraction;
    }

    /**
     * <p>Setter for the field <code>selectedExtraction</code>.</p>
     *
     * @param selectedExtraction a {@link fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsRowModel} object.
     */
    public void setSelectedExtraction(ExtractionsRowModel selectedExtraction) {
        this.selectedExtraction = selectedExtraction;
        firePropertyChange(PROPERTY_SELECTED_EXTRACTION, null, selectedExtraction);
    }

    /**
     * <p>Getter for the field <code>extractionsTableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsTableUIModel} object.
     */
    public ExtractionsTableUIModel getExtractionsTableUIModel() {
        return extractionsTableUIModel;
    }

    /**
     * <p>Setter for the field <code>extractionsTableUIModel</code>.</p>
     *
     * @param extractionsTableUIModel a {@link fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsTableUIModel} object.
     */
    public void setExtractionsTableUIModel(ExtractionsTableUIModel extractionsTableUIModel) {
        this.extractionsTableUIModel = extractionsTableUIModel;
        extractionsTableUIModel.setExtractionUIModel(this);
    }

    /**
     * <p>Getter for the field <code>extractionsFiltersUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.extraction.filters.ExtractionsFiltersUIModel} object.
     */
    public ExtractionsFiltersUIModel getExtractionsFiltersUIModel() {
        return extractionsFiltersUIModel;
    }

    /**
     * <p>Setter for the field <code>extractionsFiltersUIModel</code>.</p>
     *
     * @param extractionsFiltersUIModel a {@link fr.ifremer.reefdb.ui.swing.content.extraction.filters.ExtractionsFiltersUIModel} object.
     */
    public void setExtractionsFiltersUIModel(ExtractionsFiltersUIModel extractionsFiltersUIModel) {
        this.extractionsFiltersUIModel = extractionsFiltersUIModel;
        extractionsFiltersUIModel.setExtractionUIModel(this);
    }

    /**
     * <p>Getter for the field <code>context</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public ContextDTO getContext() {
        return context;
    }

    /**
     * <p>Setter for the field <code>context</code>.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public void setContext(ContextDTO context) {
        ContextDTO oldContext = getContext();
        this.context = context;
        firePropertyChange(PROPERTY_CONTEXT, oldContext, context);
    }
}
