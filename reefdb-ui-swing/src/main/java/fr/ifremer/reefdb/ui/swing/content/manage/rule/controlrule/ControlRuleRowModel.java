package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.FunctionDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.*;
import fr.ifremer.reefdb.dto.enums.ControlFunctionValues;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Date;
import java.util.function.Predicate;

/**
 * <p>ControlRuleRowModel class.</p>
 *
 * @author Antoine
 */
public class ControlRuleRowModel extends AbstractReefDbRowUIModel<ControlRuleDTO, ControlRuleRowModel> implements ControlRuleDTO {

    private static final Binder<ControlRuleDTO, ControlRuleRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(ControlRuleDTO.class, ControlRuleRowModel.class);

    private static final Binder<ControlRuleRowModel, ControlRuleDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ControlRuleRowModel.class, ControlRuleDTO.class);

    static final Predicate<ControlRuleRowModel> qualitativeValuePredicate = ReefDbBeans::isQualitativeControlRule;

    static final Predicate<ControlRuleRowModel> preconditionQualitativePredicate = rule -> ControlFunctionValues.PRECONDITION_QUALITATIVE.equals(rule.getFunction());

    static final Predicate<ControlRuleRowModel> preconditionNumericalPredicate = rule -> ControlFunctionValues.PRECONDITION_NUMERICAL.equals(rule.getFunction());

    public static final String PROPERTY_PMFM_VALID = "pmfmValid";
    private boolean pmfmValid;

    /**
     * <p>Constructor for ControlRuleRowModel.</p>
     */
    public ControlRuleRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    public boolean isPmfmValid() {
        return pmfmValid;
    }

    public void setPmfmValid(boolean pmfmValid) {
        boolean oldValue = isPmfmValid();
        this.pmfmValid = pmfmValid;
        firePropertyChange(PROPERTY_PMFM_VALID, oldValue, pmfmValid);
    }

    /** {@inheritDoc} */
    @Override
    protected ControlRuleDTO newBean() {
        return ReefDbBeanFactory.newControlRuleDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return delegateObject.getCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setCode(String code) {
        delegateObject.setCode(code);
    }

    /** {@inheritDoc} */
    @Override
    public FunctionDTO getFunction() {
        return delegateObject.getFunction();
    }

    /** {@inheritDoc} */
    @Override
    public void setFunction(FunctionDTO Function) {
        delegateObject.setFunction(Function);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    @Override
    public PreconditionRuleDTO getPreconditions(int index) {
        return delegateObject.getPreconditions(index);
    }

    @Override
    public boolean isPreconditionsEmpty() {
        return delegateObject.isPreconditionsEmpty();
    }

    @Override
    public int sizePreconditions() {
        return delegateObject.sizePreconditions();
    }

    @Override
    public void addPreconditions(PreconditionRuleDTO preconditions) {
        delegateObject.addPreconditions(preconditions);
    }

    @Override
    public void addAllPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        delegateObject.addAllPreconditions(preconditions);
    }

    @Override
    public boolean removePreconditions(PreconditionRuleDTO preconditions) {
        return delegateObject.removePreconditions(preconditions);
    }

    @Override
    public boolean removeAllPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        return delegateObject.removeAllPreconditions(preconditions);
    }

    @Override
    public boolean containsPreconditions(PreconditionRuleDTO preconditions) {
        return delegateObject.containsPreconditions(preconditions);
    }

    @Override
    public boolean containsAllPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        return delegateObject.containsAllPreconditions(preconditions);
    }

    @Override
    public Collection<PreconditionRuleDTO> getPreconditions() {
        return delegateObject.getPreconditions();
    }

    @Override
    public void setPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        delegateObject.setPreconditions(preconditions);
    }

    @Override
    public RuleGroupDTO getGroups(int index) {
        return delegateObject.getGroups(index);
    }

    @Override
    public boolean isGroupsEmpty() {
        return delegateObject.isGroupsEmpty();
    }

    @Override
    public int sizeGroups() {
        return delegateObject.sizeGroups();
    }

    @Override
    public void addGroups(RuleGroupDTO groups) {
        delegateObject.addGroups(groups);
    }

    @Override
    public void addAllGroups(Collection<RuleGroupDTO> groups) {
        delegateObject.addAllGroups(groups);
    }

    @Override
    public boolean removeGroups(RuleGroupDTO groups) {
        return delegateObject.removeGroups(groups);
    }

    @Override
    public boolean removeAllGroups(Collection<RuleGroupDTO> groups) {
        return delegateObject.removeAllGroups(groups);
    }

    @Override
    public boolean containsGroups(RuleGroupDTO groups) {
        return delegateObject.containsGroups(groups);
    }

    @Override
    public boolean containsAllGroups(Collection<RuleGroupDTO> groups) {
        return delegateObject.containsAllGroups(groups);
    }

    @Override
    public Collection<RuleGroupDTO> getGroups() {
        return delegateObject.getGroups();
    }

    @Override
    public void setGroups(Collection<RuleGroupDTO> groups) {
        delegateObject.setGroups(groups);
    }

    /** {@inheritDoc} */
    @Override
    public ControlElementDTO getControlElement() {
        return delegateObject.getControlElement();
    }

    /** {@inheritDoc} */
    @Override
    public void setControlElement(ControlElementDTO controlElement) {

        // Remove caracteristique controle value if change element controle
        final ControlElementDTO oldValue = delegateObject.getControlElement();
        if (oldValue != null && (controlElement == null || !oldValue.getName().equals(controlElement.getName()))) {
            setControlFeature(null);
        }
        delegateObject.setControlElement(controlElement);
    }

    /** {@inheritDoc} */
    @Override
    public ControlFeatureDTO getControlFeature() {
        return delegateObject.getControlFeature();
    }

    /** {@inheritDoc} */
    @Override
    public void setControlFeature(ControlFeatureDTO ControlFeature) {
        delegateObject.setControlFeature(ControlFeature);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isActive() {
        return delegateObject.isActive();
    }

    /** {@inheritDoc} */
    @Override
    public void setActive(boolean Active) {
        delegateObject.setActive(Active);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isBlocking() {
        return delegateObject.isBlocking();
    }

    /** {@inheritDoc} */
    @Override
    public void setBlocking(boolean Blocking) {
        delegateObject.setBlocking(Blocking);
    }

    /** {@inheritDoc} */
    @Override
    public Object getMin() {
        return delegateObject.getMin();
    }

    /** {@inheritDoc} */
    @Override
    public void setMin(Object min) {
        delegateObject.setMin(min);
    }

    /** {@inheritDoc} */
    @Override
    public Object getMax() {
        return delegateObject.getMax();
    }

    /** {@inheritDoc} */
    @Override
    public void setMax(Object max) {
        delegateObject.setMax(max);
    }

    /** {@inheritDoc} */
    @Override
    public String getAllowedValues() {
        return delegateObject.getAllowedValues();
    }

    /** {@inheritDoc} */
    @Override
    public void setAllowedValues(String allowedValues) {
        delegateObject.setAllowedValues(allowedValues);
    }

    /** {@inheritDoc} */
    @Override
    public RulePmfmDTO getRulePmfms(int index) {
        return delegateObject.getRulePmfms(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isRulePmfmsEmpty() {
        return delegateObject.isRulePmfmsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeRulePmfms() {
        return delegateObject.sizeRulePmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void addRulePmfms(RulePmfmDTO rulePmfm) {
        delegateObject.addRulePmfms(rulePmfm);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllRulePmfms(Collection<RulePmfmDTO> rulePmfms) {
        delegateObject.addAllRulePmfms(rulePmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeRulePmfms(RulePmfmDTO rulePmfm) {
        return delegateObject.removeRulePmfms(rulePmfm);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllRulePmfms(Collection<RulePmfmDTO> rulePmfms) {
        return delegateObject.removeAllRulePmfms(rulePmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsRulePmfms(RulePmfmDTO rulePmfm) {
        return delegateObject.containsRulePmfms(rulePmfm);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllRulePmfms(Collection<RulePmfmDTO> rulePmfms) {
        return delegateObject.containsAllRulePmfms(rulePmfms);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<RulePmfmDTO> getRulePmfms() {
        return delegateObject.getRulePmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void setRulePmfms(Collection<RulePmfmDTO> rulePmfms) {
        delegateObject.setRulePmfms(rulePmfms);
    }

    /** {@inheritDoc} */
    @Override
    public String getMessage() {
        return delegateObject.getMessage();
    }

    /** {@inheritDoc} */
    @Override
    public void setMessage(String Message) {
        delegateObject.setMessage(Message);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isNewCode() {
        return delegateObject.isNewCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setNewCode(boolean newCode) {
        delegateObject.setNewCode(newCode);
    }

    @Override
    public String getName() {
        return delegateObject.getName();
    }

    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }

}
