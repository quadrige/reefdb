package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.context.ManageContextsUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action pour le remplacement d'un lieu local par un lieu national
 */
public class ImportContextAction extends AbstractReefDbAction<ManageContextsListTableUIModel, ManageContextsListTableUI, ManageContextsListTableUIHandler> {

    private List<ContextDTO> importedContexts;
    
    private File importFile;
    
	/**
	 * Constructor.
	 *
	 * @param handler Controlleur
	 */
	public ImportContextAction(ManageContextsListTableUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
    public boolean prepareAction() throws Exception {
        importFile = null;
        if (super.prepareAction()) {
            // get importFile
            importFile = chooseFile(t("reefdb.action.context.import.title"),
                                                t("reefdb.action.common.chooseFile.buttonLabel"),
                                                "^.*\\.dat", t("reefdb.common.file.dat"));
            
        }
        return importFile != null && importFile.exists();
    }

	/** {@inheritDoc} */
	@Override
	public void doAction() {
        importedContexts = getContext().getContextService().importContexts(importFile);
	}

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        getModel().addBeans(importedContexts);
        // fix Mantis #30776
        ((ManageContextsUI) ReefDbUIs.getParentUI(getUI())).getManageContextsListMenuUI().getHandler().reloadComboBox();
    }
}
