package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele du menu pour la gestion des Users au niveau National
 */
public class UserMenuUIModel extends AbstractReferentialMenuUIModel<PersonCriteriaDTO, UserMenuUIModel> implements PersonCriteriaDTO {

    private static final Binder<UserMenuUIModel, PersonCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(UserMenuUIModel.class, PersonCriteriaDTO.class);

    private static final Binder<PersonCriteriaDTO, UserMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(PersonCriteriaDTO.class, UserMenuUIModel.class);

    /**
     * Constructor.
     */
    public UserMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected PersonCriteriaDTO newBean() {
        return ReefDbBeanFactory.newPersonCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getFirstName() {
        return delegateObject.getFirstName();
    }

    /** {@inheritDoc} */
    @Override
    public void setFirstName(String firstname) {
        delegateObject.setFirstName(firstname);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getDepartment() {
        return delegateObject.getDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepartment(DepartmentDTO department) {
        delegateObject.setDepartment(department);
    }

    /** {@inheritDoc} */
    @Override
    public PrivilegeDTO getPrivilege() {
        return delegateObject.getPrivilege();
    }

    /** {@inheritDoc} */
    @Override
    public void setPrivilege(PrivilegeDTO privilege) {
        delegateObject.setPrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public String getLogin() {
        return delegateObject.getLogin();
    }

    /** {@inheritDoc} */
    @Override
    public void setLogin(String login) {
        delegateObject.setLogin(login);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setFirstName(null);
        setDepartment(null);
        setPrivilege(null);
        setLogin(null);
    }

}
