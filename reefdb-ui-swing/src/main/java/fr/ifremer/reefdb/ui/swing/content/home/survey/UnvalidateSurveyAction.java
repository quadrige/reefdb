package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.synchro.vo.SynchroImportContextVO;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIContext;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportDataSynchroAtOnceAction;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUIModel;
import fr.ifremer.reefdb.ui.swing.content.home.SearchAction;
import fr.ifremer.reefdb.ui.swing.content.home.survey.validate.ValidateSurveyUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de changer l'état d'une observation dans le tableau des
 * observations de l ecran d accueil.
 */
public class UnvalidateSurveyAction extends AbstractReefDbAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

    private enum Action {
        UNVALIDATE,
        IMPORT_DATA
    }

    private Action action;

    private Collection<? extends SurveyDTO> surveyRows;

    private String unvalidationComment;

    /**
     * Loggeur.
     */
    private static final Log LOG = LogFactory.getLog(UnvalidateSurveyAction.class);

    /**
     * Constructor.
     *
     * @param handler Controleur
     */
    public UnvalidateSurveyAction(final SurveysTableUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {

        // Default Action
        action = Action.UNVALIDATE;

        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucune Observation de selectionne");
            return false;
        }

        // check if selected surveys can be unvalidated
        surveyRows = getModel().getSelectedRows();
        boolean isSynchronized = false;
        boolean isQualified = false;
        int userId = getContext().getDataContext().getRecorderPersonId();
        int departmentId = getContext().getDataContext().getRecorderDepartmentId();

        for (SurveyDTO survey : surveyRows) {

            boolean isManager = getContext().isAuthenticatedAsNationalAdmin() || ReefDbBeans.isProgramManager(survey.getProgram(), userId, departmentId);
            boolean isValidator = ReefDbBeans.isProgramValidator(survey.getProgram(), userId, departmentId);

            if (!isManager && !isValidator) {
                displayErrorMessage(t("reefdb.action.unvalidate.survey.title"), t("reefdb.action.unvalidate.survey.error.forbidden"));
                return false;
            }

            //On ne peut pas de-valider une observation qui n'a jamais été validée
            if (survey.getValidationDate() == null) {
                displayWarningMessage(t("reefdb.action.unvalidate.survey.title"), t("reefdb.action.unvalidate.survey.error.notValid"));
                return false;
            }

            if (SynchronizationStatusValues.SYNCHRONIZED.equals(survey.getSynchronizationStatus())) {
                isSynchronized = true;
            }

            if (getContext().getObservationService().isQualified(survey)) {
                isQualified = true;

                // A validator without qualifier profile can't unvalidate a qualified survey
                if (!isManager && isValidator && !getContext().isAuthenticatedAsQualifier()) {
                    displayErrorMessage(t("reefdb.action.unvalidate.survey.title"), t("reefdb.action.unvalidate.survey.error.isQualified"));
                    return false;
                }
            }
        }

        if (isSynchronized) {
            displayWarningMessage(t("reefdb.action.unvalidate.survey.title"), t("reefdb.action.unvalidate.survey.warning.isSynchronized"));
        }
        if (isQualified) {
            displayWarningMessage(t("reefdb.action.unvalidate.survey.title"), t("reefdb.action.unvalidate.survey.warning.isQualified"));
        }

        // Check data updates if server available
        try {
            boolean doUpdate = checkDataUpdate();
            if (doUpdate) {
                action = Action.IMPORT_DATA;
            }
        } catch (Exception e) {
            LOG.warn("Connection to synchronization server failed, data updates ignored");
        }

        if (action == Action.UNVALIDATE) {
            // Ask confirmation with mandatory comment
            ValidateSurveyUI validateSurveyUI = new ValidateSurveyUI(getContext());
            validateSurveyUI.getModel().setUnValidation(true);
            getHandler().openDialog(validateSurveyUI);

            if (validateSurveyUI.getModel().isValid()) {
                unvalidationComment = validateSurveyUI.getModel().getComment();
            } else {
                // Confirmation rejected
                return false;
            }
        }

        return true;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        if (action == Action.UNVALIDATE) {

            boolean isModifyModelState = getModel().isModify();

            // Unvalidate
            getContext().getObservationService().unvalidateSurveys(surveyRows, unvalidationComment, createProgressionUIModel());

            getModel().setModify(isModifyModelState);

        } else if (action == Action.IMPORT_DATA) {

            doImportData();
            LOG.info("Data have been imported, stop unvalidation action");

        }
    }

    private boolean checkDataUpdate() {
        SynchroUIContext synchroContext = getContext().getSynchroContext();
        synchroContext.loadImportContext();

        Set<String> programCodes = surveyRows.stream().map(survey -> survey.getProgram().getCode()).collect(Collectors.toSet());
        synchroContext.setImportDataProgramCodes(programCodes);

        // Delay update date to check by default referential offset (=1min) plus potential delay (=30sec) (Mantis #59243)
        synchroContext.setImportDataUpdateDate(
            Dates.addSeconds(synchroContext.getImportDataUpdateDate(),
                QuadrigeConfiguration.getInstance().getImportReferentialUpdateDateOffsetInSecond()
                    // Configure this delay with option quadrige3.synchro.export.updateDate.offset.short
                    + QuadrigeConfiguration.getInstance().getExportDataUpdateDateShortDelayInSecond()
            )
        );

        SynchroImportContextVO contextResult = ReefDbServiceLocator.instance().getSynchroRestClientService().checkImportDataContext(
            getContext().getAuthenticationInfo(),
            synchroContext.getSynchroImportContext()
        );

        if (contextResult.isWithData()) {

            // ask use to update to start import data
            return getContext().getDialogHelper().showConfirmDialog(
                t("reefdb.action.unvalidate.survey.updateAvailable"),
                t("reefdb.action.unvalidate.survey.title"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;

        }

        return false;
    }

    private void doImportData() {

        // Keep old action description
        String oldDescription = getActionDescription();

        // Create import data action
        ImportDataSynchroAtOnceAction importAction = getContext().getActionFactory().createLogicAction(getContext().getMainUI().getHandler(), ImportDataSynchroAtOnceAction.class);

        // override action description
        forceActionDescription(t("reefdb.action.synchro.import.data"));

        // run internally
        getContext().getActionEngine().runInternalAction(importAction);

        // Restore description
        forceActionDescription(oldDescription);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        if (action == Action.IMPORT_DATA) {
            // run search to update surveys list
            SearchAction searchAction = getActionFactory().createLogicAction(getHandler().getHomeUIHandler(), SearchAction.class);
            getActionEngine().runInternalAction(searchAction);
        }

        // force reloading operations
        getModel().getMainUIModel().firePropertyChanged(HomeUIModel.PROPERTY_SELECTED_SURVEY, null, null);

        super.postSuccessAction();
        // do something ?
    }

}
