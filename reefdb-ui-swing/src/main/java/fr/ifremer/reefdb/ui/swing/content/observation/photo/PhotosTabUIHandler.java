package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Images;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.util.image.PhotoViewer;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import javax.swing.SwingWorker;
import javax.swing.table.TableCellRenderer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet photo.
 */
public class PhotosTabUIHandler extends AbstractReefDbTableUIHandler<PhotosTableRowModel, PhotosTabUIModel, PhotosTabUI> implements TabHandler {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PhotosTabUIHandler.class);

    private ExtendedComboBoxCellEditor<SamplingOperationDTO> samplingOperationCellEditor;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final PhotosTabUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final PhotosTabUIModel model = new PhotosTabUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final PhotosTabUI ui) {
        initUI(ui);

        // Init viewer
        SwingUtil.setLayerUI(getUI().getPhotoViewer(), getUI().getPhotoBlockLayer());

        // init table
        initTable();

        // Init listeners
        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

        // initial state
        ui.getDownloadPhotoButton().setEnabled(false);
        ui.getExportPhotoButton().setEnabled(false);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, PhotosTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{PhotoDTO.PROPERTY_DIRTY};
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(PhotosTableRowModel row) {

        return super.isRowValid(row) && isPhotoValid(row);
    }

    private boolean isPhotoValid(PhotosTableRowModel row) {

        // check name duplicates
        if (getModel().getRowCount() >= 2) {
            for (PhotosTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (StringUtils.isNotBlank(row.getName()) && row.getName().equals(otherRow.getName())) {
                    // duplicate found
                    ReefDbBeans.addError(row, t("reefdb.photo.name.duplicate"), PhotosTableRowModel.PROPERTY_NAME);
                }
            }
        }

        return ReefDbBeans.hasNoBlockingError(row);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // La colonne mnemonique
        final TableColumnExt nameCol = addColumn(PhotosTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setMinWidth(100);

        // La colonne type
        final TableColumnExt typeCol = addFilterableComboDataColumnToModel(PhotosTableModel.TYPE,
                getContext().getReferentialService().getPhotoTypes(), false);
        typeCol.setSortable(true);
        typeCol.setMinWidth(100);

        // La colonne legende
        final TableColumnExt captionCol = addColumn(PhotosTableModel.CAPTION);
        captionCol.setSortable(true);
        captionCol.setMinWidth(100);

        // La colonne date
        final TableColumnExt dateCol = addDatePickerColumnToModel(PhotosTableModel.DATE, getConfig().getDateFormat());
        dateCol.setSortable(true);
        dateCol.setMinWidth(100);

        // La colonne prelevement
        samplingOperationCellEditor = newExtendedComboBoxCellEditor(null, PhotosTableModel.SAMPLING_OPERATION, false);
        final TableColumnExt samplingCol = addColumn(
                samplingOperationCellEditor,
                newTableCellRender(PhotosTableModel.SAMPLING_OPERATION),
                PhotosTableModel.SAMPLING_OPERATION);
        samplingCol.setSortable(true);
        samplingCol.setMinWidth(200);

        // La colonne direction
        final TableColumnExt directionCol = addColumn(PhotosTableModel.DIRECTION);
        directionCol.setSortable(true);
        directionCol.setMinWidth(200);

        // La colonne chemin physique
        final TableColumnExt pathCol = addColumn(
                null,
                (table, value, isSelected, hasFocus, row, column) -> {

                    if (!getTableModel().getEntry(getTable().convertRowIndexToModel(row)).isFileExists()) {
                        value = t("reefdb.photo.unavailable");
                    }

                    TableCellRenderer defaultRenderer = table.getDefaultRenderer(PhotosTableModel.PATH.getPropertyType());
                    return defaultRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                },
                PhotosTableModel.PATH);
        pathCol.setSortable(true);
        pathCol.setMinWidth(200);
        pathCol.setEditable(false);

        // Modele de la table
        final PhotosTableModel tableModel = new PhotosTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Colonne toujours visible
        nameCol.setHideable(false);
        samplingCol.setHideable(false);

        // Initialisation de la table
        initTable(getTable());

        // Les colonne non visibles
        directionCol.setVisible(false);
        pathCol.setVisible(false);

        getTable().setVisibleRowCount(5);

        // border
        addEditionPanelBorder();
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<PhotosTabUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * Load observation.
     */
    private void load() {

        // update combo
        samplingOperationCellEditor.getCombo().setData(new ArrayList<>(getModel().getObservationModel().getSamplingOperations()));

        // All photos
        final List<PhotoDTO> photos = getModel().getObservationModel().getPhotos();

        // Init photos table
        getTableModel().setReadOnly(!getModel().getObservationModel().isEditable());
        getModel().setBeans(photos);
        recomputeRowsValidState();

        if (getModel().getPhotoIndex() == null && getModel().getRowCount() > 0) {
            getModel().setPhotoIndex(getModel().getFirstPhotoIndex());
        }

    }

    /**
     * <p>removePhoto.</p>
     */
    public void removePhoto() {

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("No selected photo");
            return;
        }

        // Confirm deleting
        if (askBeforeDelete(t("reefdb.action.delete.photos.title"), t("reefdb.action.delete.photos.message"))) {
            // Remove from model
            getModel().deleteSelectedRows();
            // refresh photo index
            updatePhotoViewerContent(false);

            getModel().setModify(true);
        }
    }

    /**
     * <p>firstPhoto.</p>
     */
    public void firstPhoto() {
        if (!getModel().getPhotoIndex().equals(getModel().getFirstPhotoIndex())) {
            getModel().setPhotoIndex(getModel().getFirstPhotoIndex());
        }
    }

    /**
     * <p>previousPhoto.</p>
     */
    public void previousPhoto() {
        if (!getModel().getPhotoIndex().equals(getModel().getFirstPhotoIndex())) {
            getModel().setPhotoIndex(getModel().getPhotoIndex() - 1);
        }
    }

    /**
     * <p>nextPhoto.</p>
     */
    public void nextPhoto() {
        if (!getModel().getPhotoIndex().equals(getModel().getLastPhotoIndex())) {
            getModel().setPhotoIndex(getModel().getPhotoIndex() + 1);
        }
    }

    /**
     * <p>lastPhoto.</p>
     */
    public void lastPhoto() {
        if (!getModel().getPhotoIndex().equals(getModel().getLastPhotoIndex())) {
            getModel().setPhotoIndex(getModel().getLastPhotoIndex());
        }
    }

    public void fullScreen() {
        if (getModel().getPhotoIndex() == null) return;
        PhotosTableRowModel selectedPhoto = getModel().getSelectedPhoto();
        if (selectedPhoto == null) return;

        PhotoViewer<PhotoDTO> photoViewer = new PhotoViewer<>();

        photoViewer.setPhoto(selectedPhoto, Images.ImageType.BASE);
        openFrame(photoViewer, selectedPhoto.getName(), null, getUI().getBounds());
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(PhotosTabUIModel.PROPERTY_OBSERVATION_MODEL, evt -> load());

        getModel().addPropertyChangeListener(PhotosTabUIModel.PROPERTY_PHOTO_INDEX, evt -> {
            if (getModel().isModelAdjusting())
                return;

            PhotosTableRowModel selectedPhoto = getModel().getSelectedPhoto();
            if (selectedPhoto != null) {
                setFocusOnCell(selectedPhoto);
            }
        });

        // listener on select photo in viewer
        getUI().getPhotoViewer().addPropertyChangeListener(PhotoViewer.EVENT_PHOTO_CLICKED, evt -> {

            if (evt.getNewValue() instanceof PhotoDTO) {

                if (getModel().isModelAdjusting()) {
                    return;
                }
                try {

                    getModel().setModelAdjusting(true);

                    PhotoDTO photo = (PhotoDTO) evt.getNewValue();

                    PhotosTableRowModel rowModel = null;
                    if (photo instanceof PhotosTableRowModel) {
                        rowModel = (PhotosTableRowModel) photo;
                    } else {
                        for (PhotosTableRowModel row : getModel().getRows()) {
                            if (Objects.equals(row.getFullPath(), photo.getFullPath())) {
                                rowModel = row;
                                break;
                            }
                        }
                    }

                    if (rowModel != null) {

                        if (rowModel.isFileDownloadable()) {

                            // Download the photo
                            DownloadAction downloadAction = getContext().getActionFactory().createLogicAction(this, DownloadAction.class);
                            downloadAction.setToDownload(rowModel);
                            getContext().getActionEngine().runAction(downloadAction);

                        } else if (rowModel != getModel().getSingleSelectedRow()) {

                            // select the photo in table
                            setFocusOnCell(rowModel);
                            getModel().setPhotoIndex(getTableModel().getRowIndex(rowModel));
                        }
                    }

                } finally {

                    getModel().setModelAdjusting(false);
                }
            }
        });

        // listener on selected photo in table
        getModel().addPropertyChangeListener(AbstractReefDbTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            if (getModel().isModelAdjusting())
                return;

            getModel().setModelAdjusting(true);
            getModel().setPhotoIndex(getTableModel().getRowIndex(getModel().getSingleSelectedRow()));
            updatePhotoViewerContent(true);
            getModel().setModelAdjusting(false);

        });

        // Listener on combo
        getUI().getTypeDiaporamaComboBox().addActionListener(e -> updatePhotoViewerContent(false));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void onShowTab(int currentIndex, int newIndex) {

    }

    /** {@inheritDoc} */
    @Override
    public boolean onRemoveTab() {
        return false;
    }

    /**
     * <p>save.</p>
     */
    public void save() {
        getModel().setModelAdjusting(true);
        getModel().getObservationModel().setPhotos(getModel().getBeans());
        getModel().setModelAdjusting(false);
    }

    /** {@inheritDoc} */
    @Override
    public PhotosTableModel getTableModel() {
        return (PhotosTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getPhotoTable();
    }

    /**
     * Mettre a jour la ou les photos dans l'écran.
     */
    public void updatePhotoViewerContent(boolean selectionOnly) {

        // update photo index if model has changed
        Integer photoIndex = getModel().getPhotoIndex();
        if (photoIndex != null) {
            photoIndex = Math.min(photoIndex, getModel().getLastPhotoIndex());
            photoIndex = photoIndex == -1 ? null : photoIndex;
            if (!Objects.equals(getModel().getPhotoIndex(), photoIndex)) {
                getModel().setPhotoIndex(photoIndex);
                return;
            }
        }

        PhotoLoader worker = new PhotoLoader(selectionOnly);
        getModel().setLoading(true);
        worker.execute();

    }

    private class PhotoLoader extends SwingWorker<Object, Object> {

        private final boolean selectionOnly;

        private PhotoLoader(boolean selectionOnly) {
            this.selectionOnly = selectionOnly;
        }

        @Override
        protected Object doInBackground() {

            PhotoViewType viewType = getUI().getTypeDiaporamaComboBox().getSelectedItem();

            if (PhotoViewType.VIEW_DIAPO.equals(viewType)) {

                getUI().getPhotoViewer().setPhoto(getModel().getSingleSelectedRow(), viewType.getImageType());

            } else if (PhotoViewType.VIEW_THUMBNAIL.equals(viewType)) {

                if (!selectionOnly) {
                    // load all photos from model
                    getUI().getPhotoViewer().setPhotos(getModel().getRows(), viewType.getImageType());
                }
                getUI().getPhotoViewer().setSelected(getModel().getSelectedRows());

            }

            return null;
        }

        @Override
        protected void done() {

            try {
                get();
            } catch (InterruptedException | ExecutionException e) {
                throw new ReefDbTechnicalException(e.getMessage(), e);
            }

            // affiche l indice photos
            getUI().getPhotoIndexLabel().setText(String.format("%s / %s", getModel().getPhotoIndex() == null ? 0 : getModel().getPhotoIndex() + 1, getModel().getRowCount()));

            // Gestion des boutons de parcours des photos
            getUI().getFirstPhotoButton().setEnabled(getModel().getPhotoIndex() != null && getModel().getPhotoIndex() != getModel().getFirstPhotoIndex());
            getUI().getPreviousPhotoButton().setEnabled(getModel().getPhotoIndex() != null && getModel().getPhotoIndex() != getModel().getFirstPhotoIndex());
            getUI().getNextPhotoButton().setEnabled(getModel().getPhotoIndex() != null && getModel().getPhotoIndex() != getModel().getLastPhotoIndex());
            getUI().getLastPhotoButton().setEnabled(getModel().getPhotoIndex() != null && getModel().getPhotoIndex() != getModel().getLastPhotoIndex());

            getModel().setLoading(false);

        }
    }
}
