package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.privileges;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Handler.
 */
public class PrivilegesDialogUIHandler extends AbstractReefDbUIHandler<PrivilegesDialogUIModel, PrivilegesDialogUI> implements Cancelable {

    /** Constant <code>DOUBLE_LIST="doubleList"</code> */
    public static final String DOUBLE_LIST = "doubleList";
    /** Constant <code>LIST="list"</code> */
    public static final String LIST = "list";

    /** {@inheritDoc} */
    @Override
    public void beforeInit(PrivilegesDialogUI ui) {
        super.beforeInit(ui);

        PrivilegesDialogUIModel model = new PrivilegesDialogUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(final PrivilegesDialogUI ui) {
        initUI(ui);

        initBeanList(getUI().getPrivilegesDoubleList(), getContext().getUserService().getAvailablePrivileges(), null);
        getUI().getPrivilegesList().setCellRenderer(newListCellRender(PrivilegeDTO.class));

        getModel().addPropertyChangeListener(PrivilegesDialogUIModel.PROPERTY_USER, evt -> {

            // update list & double list (as a copy)
            List<PrivilegeDTO> privileges = Lists.newArrayList(getModel().getUser().getPrivilege());
            getUI().getPrivilegesDoubleList().getModel().setSelected(privileges);
            getUI().getPrivilegesList().setListData(privileges.toArray(new PrivilegeDTO[privileges.size()]));
        });

        getModel().addPropertyChangeListener(PrivilegesDialogUIModel.PROPERTY_EDITABLE, evt -> {

            // select list only if model is not editable
            if (getModel().isEditable()) {
                getUI().getListPanelLayout().setSelected(DOUBLE_LIST);
            } else {
                getUI().getListPanelLayout().setSelected(LIST);
            }

        });
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // set privilege to user
        if (getModel().isEditable()) {
            getModel().getUser().setPrivilege(getUI().getPrivilegesDoubleList().getModel().getSelected());
        }

        // close the dialog box
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
