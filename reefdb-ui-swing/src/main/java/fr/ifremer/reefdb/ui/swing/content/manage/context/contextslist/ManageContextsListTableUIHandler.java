package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.context.ManageContextsUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

/**
 * <p>ManageContextsListTableUIHandler class.</p>
 *
 */
public class ManageContextsListTableUIHandler extends
        AbstractReefDbTableUIHandler<ManageContextsListTableUIRowModel, ManageContextsListTableUIModel, ManageContextsListTableUI> {

    /**
     * {@inheritDoc}
     *
     * Logger.
     */
//    private static final Log LOG = LogFactory.getLog(ManageContextsListTableUIHandler.class);
    @Override
    public void beforeInit(final ManageContextsListTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageContextsListTableUIModel model = new ManageContextsListTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageContextsListTableUI ui) {

        // Initialize the screen
        initUI(ui);

        // Initialize the table
        initTable();

        // Initialize the listeners
        initListeners();

        getUI().getDuplicateButton().setEnabled(false);
        getUI().getDeleteButton().setEnabled(false);
        getUI().getActivateButton().setEnabled(false);
        getUI().getExportButton().setEnabled(false);
        
        // TODO remove button ? hide Activate button
        getUI().getActivateButton().setVisible(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // label
        TableColumnExt libelleCol = addColumn(ManageContextsListTableUITableModel.LIBELLE);
        libelleCol.setSortable(true);
        libelleCol.setEditable(true);

        // Description
        TableColumnExt descriptionCol = addColumn(ManageContextsListTableUITableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(true);

        ManageContextsListTableUITableModel tableModel = new ManageContextsListTableUITableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialize du tableau
        initTable(getTable());

        getTable().setVisibleRowCount(5);
    }

    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(ManageContextsListTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {
            if (getModel().getSingleSelectedRow() != null) {
                final ContextDTO localContext = getModel().getSingleSelectedRow();
                getUI().getParentContainer(ManageContextsUI.class).getHandler().loadLocalContext(localContext);
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ManageContextsListTableUIRowModel> getTableModel() {
        return (ManageContextsListTableUITableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageContextsListTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<ManageContextsListTableUIRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        // should be only one row
        if (addedRows.size() == 1) {
            ManageContextsListTableUIRowModel row = addedRows.get(0);

            getModel().setModify(true);

            // add focus on Code column
            setFocusOnCell(row);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, ManageContextsListTableUIRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[] {ContextDTO.PROPERTY_DIRTY};
    }
}
