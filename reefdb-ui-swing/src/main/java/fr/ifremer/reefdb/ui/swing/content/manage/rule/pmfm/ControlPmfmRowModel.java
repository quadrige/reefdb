package fr.ifremer.reefdb.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;

/**
 * <p>ControlPmfmRowModel class.</p>
 *
 * @author Antoine
 */
public class ControlPmfmRowModel extends AbstractReefDbRowUIModel<RulePmfmDTO, ControlPmfmRowModel> implements RulePmfmDTO {

    private static final Binder<RulePmfmDTO, ControlPmfmRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(RulePmfmDTO.class, ControlPmfmRowModel.class);

    private static final Binder<ControlPmfmRowModel, RulePmfmDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ControlPmfmRowModel.class, RulePmfmDTO.class);

    /**
     * <p>Constructor for ControlPmfmRowModel.</p>
     */
    public ControlPmfmRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected RulePmfmDTO newBean() {
        return ReefDbBeanFactory.newRulePmfmDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    @Override
    public PmfmDTO getPmfm() {
        return delegateObject.getPmfm();
    }

    @Override
    public void setPmfm(PmfmDTO pmfm) {
        delegateObject.setPmfm(pmfm);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }

}
