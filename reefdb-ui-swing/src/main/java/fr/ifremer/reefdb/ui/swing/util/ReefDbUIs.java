package fr.ifremer.reefdb.ui.swing.util;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;

import javax.swing.JComponent;
import java.awt.Dimension;

/**
 * Created: 14/06/12
 */
public final class ReefDbUIs extends ApplicationUIUtil {

    /**
     * La largeur des combobox.
     */
    public static final int REEFDB_COMPONENT_WIDTH = 200;

    /**
     * La hauteur des combobox.
     */
    public static final int REEFDB_COMPONENT_HEIGHT = 30;

    /**
     * Size of a ReefDb double list
     */
    public static final int REEFDB_DOUBLE_LIST_SIZE = 260;

    /**
     * Size of a ReefDb checkbox column
     */
    public static final int REEFDB_CHECKBOX_WIDTH = 80;

    /**
     * Size of a ReefDb image thumbnail
     */
    public static final int REEFDB_IMAGE_WIDTH = 200;

    /**
     * Affecte une largeur et une hauteur a un composant.
     *
     * @param component La combobox
     */
    public static void forceComponentSize(JComponent component) {
        forceComponentSize(component, REEFDB_COMPONENT_WIDTH);
    }

    /**
     * Affecte une largeur et une hauteur a une combobox.
     *
     * @param component La combobox
     * @param width     La largeur
     */
    public static void forceComponentSize(JComponent component, int width) {
        component.setPreferredSize(new Dimension(width, REEFDB_COMPONENT_HEIGHT));
        component.setSize(component.getPreferredSize());
        component.setMaximumSize(component.getPreferredSize());
        component.setMinimumSize(component.getPreferredSize());
    }


}
