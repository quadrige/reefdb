package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.referential.GroupingTypeDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

/**
 * extractions table row model
 */
public class ExtractionsRowModel extends AbstractReefDbRowUIModel<ExtractionDTO, ExtractionsRowModel> implements ExtractionDTO {

    private static final Binder<ExtractionDTO, ExtractionsRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(ExtractionDTO.class, ExtractionsRowModel.class);

    private static final Binder<ExtractionsRowModel, ExtractionDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ExtractionsRowModel.class, ExtractionDTO.class);

    private boolean filtersValid;

    /** Constant <code>PROPERTY_GROUPING_TYPE="groupingType"</code> */
    public static final String PROPERTY_GROUPING_TYPE = "groupingType";

    /**
     * <p>Constructor for ExtractionsRowModel.</p>
     */
    public ExtractionsRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected ExtractionDTO newBean() {
        return ReefDbBeanFactory.newExtractionDTO();
    }

    /**
     * <p>getFilterOfType.</p>
     *
     * @param filterTypeId a int.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public FilterDTO getFilterOfType(int filterTypeId) {
        if (!isFiltersEmpty()) {
            for (FilterDTO filter : getFilters()) {
                if (filter.getFilterTypeId() == filterTypeId) {
                    return filter;
                }
            }
        }
        return null;
    }

    /**
     * <p>isFiltersValid.</p>
     *
     * @return a boolean.
     */
    public boolean isFiltersValid() {
        return filtersValid;
    }

    /**
     * <p>Setter for the field <code>filtersValid</code>.</p>
     *
     * @param filtersValid a boolean.
     */
    public void setFiltersValid(boolean filtersValid) {
        this.filtersValid = filtersValid;
    }

    /**
     * <p>replaceFilter.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public void replaceFilter(FilterDTO filter) {
        Assert.notNull(filter);
        Assert.notNull(filter.getFilterTypeId());

        if (!isFiltersEmpty()) {
            removeFilters(getFilterOfType(filter.getFilterTypeId()));
        }
        if (CollectionUtils.isNotEmpty(filter.getElements())) {
            addFilters(filter);
        }
        setDirty(true);
        setModify(true);
    }

    /**
     * <p>getGroupingType.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.GroupingTypeDTO} object.
     */
    public GroupingTypeDTO getGroupingType() {

        FilterDTO filter = getFilterOfType(ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId());
        if (filter != null && CollectionUtils.isNotEmpty(filter.getElements())) {
            return (GroupingTypeDTO) filter.getElements().get(0);

        }
        return null;
    }

    /**
     * <p>setGroupingType.</p>
     *
     * @param groupingType a {@link fr.ifremer.reefdb.dto.referential.GroupingTypeDTO} object.
     */
    public void setGroupingType(GroupingTypeDTO groupingType) {

        // update the specific filter
        FilterDTO filter = getFilterOfType(ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId());
        if (filter == null) {
            filter = ReefDbBeanFactory.newFilterDTO();
            filter.setFilterTypeId(ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId());
        }
        filter.setElements(Collections.singletonList(groupingType));
        filter.setFilterLoaded(true);
        filter.setDirty(true);
        replaceFilter(filter);
    }

    /* DELEGATE METHODS */

    /**
     * <p>getName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /**
     * <p>isDirty.</p>
     *
     * @return a boolean.
     */
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /**
     * <p>getUser.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    public PersonDTO getUser() {
        return delegateObject.getUser();
    }

    /** {@inheritDoc} */
    public void setUser(PersonDTO user) {
        delegateObject.setUser(user);
    }

    /** {@inheritDoc} */
    public FilterDTO getFilters(int index) {
        return delegateObject.getFilters(index);
    }

    /**
     * <p>isFiltersEmpty.</p>
     *
     * @return a boolean.
     */
    public boolean isFiltersEmpty() {
        return delegateObject.isFiltersEmpty();
    }

    /**
     * <p>sizeFilters.</p>
     *
     * @return a int.
     */
    public int sizeFilters() {
        return delegateObject.sizeFilters();
    }

    /** {@inheritDoc} */
    public void addFilters(FilterDTO filters) {
        delegateObject.addFilters(filters);
    }

    /** {@inheritDoc} */
    public void addAllFilters(Collection<FilterDTO> filters) {
        delegateObject.addAllFilters(filters);
    }

    /** {@inheritDoc} */
    public boolean removeFilters(FilterDTO filters) {
        return delegateObject.removeFilters(filters);
    }

    /** {@inheritDoc} */
    public boolean removeAllFilters(Collection<FilterDTO> filters) {
        return delegateObject.removeAllFilters(filters);
    }

    /** {@inheritDoc} */
    public boolean containsFilters(FilterDTO filters) {
        return delegateObject.containsFilters(filters);
    }

    /** {@inheritDoc} */
    public boolean containsAllFilters(Collection<FilterDTO> filters) {
        return delegateObject.containsAllFilters(filters);
    }

    /**
     * <p>getFilters.</p>
     *
     * @return a {@link java.util.Collection} object.
     */
    public Collection<FilterDTO> getFilters() {
        return delegateObject.getFilters();
    }

    /** {@inheritDoc} */
    public void setFilters(Collection<FilterDTO> filters) {
        delegateObject.setFilters(filters);
    }

    /** {@inheritDoc} */
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /**
     * <p>isErrorsEmpty.</p>
     *
     * @return a boolean.
     */
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /**
     * <p>sizeErrors.</p>
     *
     * @return a int.
     */
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /**
     * <p>getErrors.</p>
     *
     * @return a {@link java.util.Collection} object.
     */
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
