package fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;
import fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox;
import fr.ifremer.quadrige3.ui.swing.table.editor.FilterableComboBoxCellEditor;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.service.administration.context.ContextService;

import javax.swing.JTable;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>FilterComboCellEditor class.</p>
 *
 */
public class FilterComboCellEditor extends FilterableComboBoxCellEditor<FilterDTO> {

	private final ManageFiltersListTableUIModel uiModel;
	private final ContextService service;

	/**
	 * <p>Constructor for FilterComboCellEditor.</p>
	 *
	 * @param uiModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist.ManageFiltersListTableUIModel} object.
	 * @param comboBox a {@link fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox} object.
	 * @param decorator a {@link Decorator} object.
	 * @param service a {@link fr.ifremer.reefdb.service.administration.context.ContextService} object.
	 */
	public FilterComboCellEditor(ManageFiltersListTableUIModel uiModel, ExtendedComboBox<FilterDTO> comboBox,
								 Decorator<FilterDTO> decorator, ContextService service) {
		super(comboBox);
        // Init combo
        getCombo().init(decorator, new ArrayList<>());
        
        this.uiModel = uiModel;
        this.service = service;
	}

	/** {@inheritDoc} */
	@Override
	public Component getTableCellEditorComponent(
			final JTable table, final Object value, final boolean isSelected, final int row, final int column) {
		
		// Selected ControlElementDTO
		final Integer filterTypeId = this.uiModel.getSingleSelectedRow().getFilterTypeId();
		if (filterTypeId == null) {
			
			// Empty list
	        getCombo().setData(new ArrayList<>());
		} else {
			
			// New ComboBox data
			final List<FilterDTO> data = service.getFiltersByType(filterTypeId);
	        getCombo().setData(Lists.newArrayList(data));
		}
		
		// Call super method 
		return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	}
}
