package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Files;
import fr.ifremer.quadrige3.core.dao.technical.Images;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Import action.
 */
public class ImportAction extends AbstractReefDbAction<PhotosTabUIModel, PhotosTabUI, PhotosTabUIHandler> {

    private List<File> originalImages;
    private List<PhotoDTO> newPhotos;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public ImportAction(PhotosTabUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        originalImages = chooseFiles(
            t("reefdb.action.photo.import.chooseFile.title"),
            t("reefdb.action.photo.import.chooseFile.buttonLabel"),
            "(.+(\\.(?i)(" + Images.AVAILABLE_EXTENSION + "))$)",
            t("reefdb.action.photo.import.chooseFile.filterDescription", Images.AVAILABLE_EXTENSION_LIST.toString().toUpperCase()));

        if (CollectionUtils.isNotEmpty(originalImages)) {

            // get file size
            long maxSize = getConfig().getSynchroPhotoMaxSize();
            if (maxSize > 0 && originalImages.stream().anyMatch(file -> file.length() > maxSize)) {
                displayErrorMessage(getActionDescription(),
                    t("reefdb.action.photo.import.fileTooLarge.message", maxSize, Files.byteCountToDisplaySize(maxSize)));
                return false;
            }

            return true;

        } else {

            // no selection
            return false;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        getModel().setLoading(true);
        newPhotos = new ArrayList<>();

        originalImages.forEach(originalImage -> {

            File tempFile = Images.importAndPrepareImageFile(originalImage, getConfig().getTempDirectory());

            PhotoDTO newPhoto = ReefDbBeanFactory.newPhotoDTO();
            newPhoto.setDirty(true);
            newPhoto.setDate(new Date());
            newPhoto.setName(originalImage.getName());
            // set path file to null
            newPhoto.setPath(null);
            newPhoto.setFullPath(tempFile.getAbsolutePath());

            // set sampling operation
            if (getContext().getSelectedSamplingOperationId() != null) {
                for (SamplingOperationDTO samplingOperation : getModel().getObservationModel().getSamplingOperations()) {
                    if (Objects.equals(samplingOperation.getId(), getContext().getSelectedSamplingOperationId())) {
                        newPhoto.setSamplingOperation(samplingOperation);
                        break;
                    }
                }
            }

            newPhotos.add(newPhoto);

        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (CollectionUtils.isNotEmpty(newPhotos)) {

            PhotosTableRowModel firstRow = null;
            for (PhotoDTO newPhoto : newPhotos) {
                PhotosTableRowModel newRow = getModel().addNewRow(newPhoto);
                if (firstRow == null)
                    firstRow = newRow;
            }
            if (firstRow != null)
                getHandler().setFocusOnCell(firstRow);

            getModel().setModify(true);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void releaseAction() {
        super.releaseAction();

        originalImages = null;
        newPhotos = null;
        getModel().setLoading(false);
    }
}
