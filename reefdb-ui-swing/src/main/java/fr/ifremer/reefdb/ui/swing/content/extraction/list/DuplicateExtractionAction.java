package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * Duplicate Extraction Action
 */
public class DuplicateExtractionAction extends AbstractReefDbAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

	/**
	 * Row.
	 */
	private ExtractionsRowModel newRow;

	/**
	 * Constructor.
	 *
	 * @param handler the handler
	 */
	public DuplicateExtractionAction(ExtractionsTableUIHandler handler) {
		super(handler, false);
	}

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || getModel().getSelectedRows().size() != 1) {
            return false;
        }

		return getContext().getDialogHelper().showConfirmDialog(
				getUI(),
				t("reefdb.action.duplicate.extraction.message"),
                t("reefdb.action.duplicate.extraction.title"),
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

    /** {@inheritDoc} */
    @Override
	public void doAction() {
		
		// Selected observation
		final ExtractionsRowModel extractionsRowModel = getModel().getSelectedRows().iterator().next();
		if (extractionsRowModel != null) {
			
			// Duplicate observation
			final ExtractionDTO duplicateObservation = getContext().getExtractionService().duplicateExtraction(extractionsRowModel.toBean());
			if (duplicateObservation != null) {
				
				// Add duplicate observation to table
				newRow = getModel().addNewRow(duplicateObservation);
				
                // Set it dirty
                newRow.setDirty(true);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void postSuccessAction() {
		super.postSuccessAction();

        getModel().setModify(true);

		// Add focus on duplicate row
		getHandler().setFocusOnCell(newRow);
	}
}
