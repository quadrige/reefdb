package fr.ifremer.reefdb.ui.swing.util.table.renderer;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.renderer.CollectionSizeButtonCellRenderer;
import jaxx.runtime.SwingUtil;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>AssociatedSupportsCellRenderer class.</p>
 *
 */
public class AssociatedSupportsCellRenderer extends CollectionSizeButtonCellRenderer {

    /**
     * Constructor.
     */
    public AssociatedSupportsCellRenderer() {

        // TODO find a better icon
        super(SwingUtil.createActionIcon("qualitative-value"), false);
    }

    /** {@inheritDoc} */
    @Override
    protected String getTooltipTextSize(int size) {
        return t("reefdb.property.pmfm.fraction.associatedMatrices.number", size);
    }

    /** {@inheritDoc} */
    @Override
    protected String getTooltipTextEmpty() {
        return t("reefdb.property.pmfm.fraction.associatedMatrices.empty");
    }

}
