package fr.ifremer.reefdb.ui.swing.content.manage.context;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.context.menu.ManageContextsListMenuUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.util.Collection;

/**
 * <p>ManageContextsUIHandler class.</p>
 *
 */
public class ManageContextsUIHandler extends AbstractReefDbUIHandler<ManageContextsUIModel, ManageContextsUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageContextsUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageContextsUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageContextsUIModel model = new ManageContextsUIModel();
        ui.setContextValue(model);

        ui.setContextValue(SwingUtil.createActionIcon("config"));

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageContextsUI ui) {
        initUI(ui);

        // Affect models
        getModel().setContextListModel(getUI().getManageContextsListUI().getModel());
        getModel().setFilterListModel(getUI().getManageFiltersListUI().getModel());

        // Initialiser les parametres des ecrans Observation et prelevemnts
        getContext().clearObservationPrelevementsIds();

        // Binding save button
        getUI().applyDataBinding(ManageContextsUI.BINDING_SAVE_BUTTON_ENABLED);

        initListeners();

        // reset model modify
        getModel().setModify(false);

    }

    private void initListeners() {

        // Listen modify
        listenModelModify(getModel().getContextListModel());
        getModel().getFilterListModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (modify != null) {
                getModel().setModify(modify);
                if (modify && getModel().getContextListModel().getSingleSelectedRow() != null) {
                    getModel().getContextListModel().getSingleSelectedRow().setDirty(true);
                }
            }
        });

        // Listen valid
        listenModelValid(getModel().getContextListModel());
        listenModelValid(getModel().getFilterListModel());

        // Validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());


        // listen to search result
        getUI().getManageContextsListMenuUI().getModel().addPropertyChangeListener(ManageContextsListMenuUIModel.PROPERTY_RESULTS, evt -> {

            getModel().getContextListModel().setBeans((Collection<ContextDTO>) evt.getNewValue());

            getUI().getManageFiltersListUI().getHandler().disable();
            getUI().getManageFiltersListUI().getHandler().clearTable();
            getUI().getManageFilterContentUI().getHandler().clearTable();

            // Auto select if unique row
            if (getModel().getContextListModel().getRowCount() == 1) {
                ManageContextsListTableUIRowModel rowModel = getModel().getContextListModel().getRows().get(0);
                SwingUtilities.invokeLater(() -> {
                    getUI().getManageContextsListUI().getHandler().selectRow(rowModel);
                    getModel().getContextListModel().setSingleSelectedRow(rowModel);
                });
            }

        });
    }


    /** {@inheritDoc} */
    @Override
    public SwingValidator<ManageContextsUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * <p>loadLocalContext.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public void loadLocalContext(final ContextDTO context) {
        getUI().getManageFilterContentUI().getHandler().clearTable();
        getUI().getManageFiltersListUI().getHandler().loadContext(context);
    }

    /**
     * <p>loadFilterElements.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public void loadFilterElements(FilterDTO filter) {
        if (filter == null) {
            getUI().getManageFilterContentUI().getHandler().clearTable();
        } else {
            getUI().getManageFilterContentUI().getHandler().loadFilterElements(filter.getId());
        }
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }
}
