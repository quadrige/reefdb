package fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Date;

/**
 * <p>RuleListRowModel class.</p>
 *
 * @author Antoine
 */
public class RuleListRowModel extends AbstractReefDbRowUIModel<RuleListDTO, RuleListRowModel> implements RuleListDTO {

    private static final Binder<RuleListDTO, RuleListRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(RuleListDTO.class, RuleListRowModel.class);

    private static final Binder<RuleListRowModel, RuleListDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(RuleListRowModel.class, RuleListDTO.class);

    public static final String PROPERTY_LOCAL = "local";

    private boolean localEditable;

    /**
     * <p>Constructor for RuleListRowModel.</p>
     */
    RuleListRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    @Override
    public boolean isEditable() {
        // a rule list can be read only if not compatible
        return super.isEditable() && !isReadOnly();
    }

    /** {@inheritDoc} */
    @Override
    protected RuleListDTO newBean() {
        return ReefDbBeanFactory.newRuleListDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return delegateObject.getCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setCode(String code) {
        delegateObject.setCode(code);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isActive() {
        return delegateObject.isActive();
    }

    /** {@inheritDoc} */
    @Override
    public void setActive(boolean Active) {
        delegateObject.setActive(Active);
    }

    /** {@inheritDoc} */
    @Override
    public MonthDTO getStartMonth() {
        return delegateObject.getStartMonth();
    }

    /** {@inheritDoc} */
    @Override
    public void setStartMonth(MonthDTO StartMonth) {
        delegateObject.setStartMonth(StartMonth);
    }

    /** {@inheritDoc} */
    @Override
    public MonthDTO getEndMonth() {
        return delegateObject.getEndMonth();
    }

    /** {@inheritDoc} */
    @Override
    public void setEndMonth(MonthDTO EndMonth) {
        delegateObject.setEndMonth(EndMonth);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    /** {@inheritDoc} */
    @Override
    public ProgramDTO getPrograms(int index) {
        return delegateObject.getPrograms(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isProgramsEmpty() {
        return delegateObject.isProgramsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizePrograms() {
        return delegateObject.sizePrograms();
    }

    /** {@inheritDoc} */
    @Override
    public void addPrograms(ProgramDTO Program) {
        delegateObject.addPrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllPrograms(Collection<ProgramDTO> Program) {
        delegateObject.addAllPrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removePrograms(ProgramDTO Program) {
        return delegateObject.removePrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllPrograms(Collection<ProgramDTO> Program) {
        return delegateObject.removeAllPrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsPrograms(ProgramDTO Program) {
        return delegateObject.containsPrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllPrograms(Collection<ProgramDTO> Program) {
        return delegateObject.containsAllPrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ProgramDTO> getPrograms() {
        return delegateObject.getPrograms();
    }

    /** {@inheritDoc} */
    @Override
    public void setPrograms(Collection<ProgramDTO> Program) {
        delegateObject.setPrograms(Program);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getDepartments(int index) {
        return delegateObject.getDepartments(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDepartmentsEmpty() {
        return delegateObject.isDepartmentsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeDepartments() {
        return delegateObject.sizeDepartments();
    }

    /** {@inheritDoc} */
    @Override
    public void addDepartments(DepartmentDTO service) {
        delegateObject.addDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllDepartments(Collection<DepartmentDTO> service) {
        delegateObject.addAllDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeDepartments(DepartmentDTO service) {
        return delegateObject.removeDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllDepartments(Collection<DepartmentDTO> service) {
        return delegateObject.removeAllDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsDepartments(DepartmentDTO service) {
        return delegateObject.containsDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllDepartments(Collection<DepartmentDTO> service) {
        return delegateObject.containsAllDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<DepartmentDTO> getDepartments() {
        return delegateObject.getDepartments();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepartments(Collection<DepartmentDTO> service) {
        delegateObject.setDepartments(service);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public ControlRuleDTO getControlRules(int index) {
        return delegateObject.getControlRules(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isControlRulesEmpty() {
        return delegateObject.isControlRulesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeControlRules() {
        return delegateObject.sizeControlRules();
    }

    /** {@inheritDoc} */
    @Override
    public void addControlRules(ControlRuleDTO ControlRule) {
        delegateObject.addControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllControlRules(Collection<ControlRuleDTO> ControlRule) {
        delegateObject.addAllControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeControlRules(ControlRuleDTO ControlRule) {
        return delegateObject.removeControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllControlRules(Collection<ControlRuleDTO> ControlRule) {
        return delegateObject.removeAllControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsControlRules(ControlRuleDTO ControlRule) {
        return delegateObject.containsControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllControlRules(Collection<ControlRuleDTO> ControlRule) {
        return delegateObject.containsAllControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ControlRuleDTO> getControlRules() {
        return delegateObject.getControlRules();
    }

    /** {@inheritDoc} */
    @Override
    public void setControlRules(Collection<ControlRuleDTO> ControlRule) {
        delegateObject.setControlRules(ControlRule);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isNewCode() {
        return delegateObject.isNewCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setNewCode(boolean newCode) {
        delegateObject.setNewCode(newCode);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public String getName() {
        return delegateObject.getName();
    }

    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    public boolean isLocal() {
        return getStatus() != null && ReefDbBeans.isLocalStatus(getStatus());
    }

    public void setLocal(boolean local) {
        setStatus(Daos.getStatus(local ? StatusCode.LOCAL_ENABLE : StatusCode.ENABLE));
    }

    boolean isLocalEditable() {
        return localEditable;
    }

    void setLocalEditable(boolean localEditable) {
        this.localEditable = localEditable;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
