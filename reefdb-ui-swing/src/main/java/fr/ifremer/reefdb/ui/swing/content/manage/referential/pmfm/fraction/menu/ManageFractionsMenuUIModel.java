package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des Fractions au niveau local
 */
public class ManageFractionsMenuUIModel extends DefaultReferentialMenuUIModel {

    /** Constant <code>PROPERTY_FRACTION="fraction"</code> */
    public static final String PROPERTY_FRACTION = "fraction";
    private FractionDTO fraction;

    /**
     * <p>getFractionId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFractionId() {
        return getFraction() == null ? null : getFraction().getId();
    }

    /**
     * <p>Getter for the field <code>fraction</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO} object.
     */
    public FractionDTO getFraction() {
        return fraction;
    }

    /**
     * <p>Setter for the field <code>fraction</code>.</p>
     *
     * @param fraction a {@link fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO} object.
     */
    public void setFraction(FractionDTO fraction) {
        this.fraction = fraction;
        firePropertyChange(PROPERTY_FRACTION, null, fraction);
    }

}
