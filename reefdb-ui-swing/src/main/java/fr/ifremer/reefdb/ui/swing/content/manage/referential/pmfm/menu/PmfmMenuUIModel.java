package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.pmfm.PmfmCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele du menu pour la gestion des Quadruplets au niveau National
 */
public class PmfmMenuUIModel extends AbstractReferentialMenuUIModel<PmfmCriteriaDTO, PmfmMenuUIModel> implements PmfmCriteriaDTO {

    private static final Binder<PmfmMenuUIModel, PmfmCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(PmfmMenuUIModel.class, PmfmCriteriaDTO.class);

    private static final Binder<PmfmCriteriaDTO, PmfmMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(PmfmCriteriaDTO.class, PmfmMenuUIModel.class);

    /**
     * Constructor.
     */
    public PmfmMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected PmfmCriteriaDTO newBean() {
        return ReefDbBeanFactory.newPmfmCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public ParameterDTO getParameter() {
        return delegateObject.getParameter();
    }

    /** {@inheritDoc} */
    @Override
    public void setParameter(ParameterDTO parameter) {
        delegateObject.setParameter(parameter);
    }

    /**
     * <p>getParameterCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getParameterCode() {
        return getParameter() != null ? getParameter().getCode() : null;
    }

    /** {@inheritDoc} */
    @Override
    public MatrixDTO getMatrix() {
        return delegateObject.getMatrix();
    }

    /** {@inheritDoc} */
    @Override
    public void setMatrix(MatrixDTO matrix) {
        delegateObject.setMatrix(matrix);
    }

    /**
     * <p>getMatrixId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getMatrixId() {
        return getMatrix() != null ? getMatrix().getId() : null;
    }

    /** {@inheritDoc} */
    @Override
    public FractionDTO getFraction() {
        return delegateObject.getFraction();
    }

    /** {@inheritDoc} */
    @Override
    public void setFraction(FractionDTO fraction) {
        delegateObject.setFraction(fraction);
    }

    /**
     * <p>getFractionId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFractionId() {
        return getFraction() != null ? getFraction().getId() : null;
    }

    /** {@inheritDoc} */
    @Override
    public MethodDTO getMethod() {
        return delegateObject.getMethod();
    }

    /** {@inheritDoc} */
    @Override
    public void setMethod(MethodDTO method) {
        delegateObject.setMethod(method);
    }

    /**
     * <p>getMethodId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getMethodId() {
        return getMethod() != null ? getMethod().getId() : null;
    }

    @Override
    public UnitDTO getUnit() {
        return delegateObject.getUnit();
    }

    @Override
    public void setUnit(UnitDTO unit) {
        delegateObject.setUnit(unit);
    }

    public Integer getUnitId() {
        return getUnit() != null ? getUnit().getId() : null;
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setParameter(null);
        setMatrix(null);
        setFraction(null);
        setMethod(null);
        setUnit(null);
    }
}
