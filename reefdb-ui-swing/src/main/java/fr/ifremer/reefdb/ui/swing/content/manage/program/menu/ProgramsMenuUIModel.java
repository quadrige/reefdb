package fr.ifremer.reefdb.ui.swing.content.manage.program.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu de l onglet prelevements mesures.
 */
public class ProgramsMenuUIModel extends DefaultReferentialMenuUIModel {

    private ProgramDTO program;

    private boolean onlyManagedPrograms = false;

    /**
     * <p>Getter for the field <code>program</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public ProgramDTO getProgram() {
        return program;
    }

    /**
     * <p>Setter for the field <code>program</code>.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void setProgram(ProgramDTO program) {
        this.program = program;
    }

    /**
     * <p>getProgramCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProgramCode() {
        return getProgram() != null ? getProgram().getCode() : null;
    }

    public boolean isOnlyManagedPrograms() {
        return onlyManagedPrograms;
    }

    public void setOnlyManagedPrograms(boolean onlyManagedPrograms) {
        this.onlyManagedPrograms = onlyManagedPrograms;
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setResults(null);
        setProgram(null);
    }
}
