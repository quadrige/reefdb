package fr.ifremer.reefdb.ui.swing.action;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.BadUpdateDtException;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.exception.SaveForbiddenException;
import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 08/07/2019.
 */
public abstract class AbstractReefDbRemoteSaveAction<M extends AbstractBeanUIModel, UI extends ReefDbUI<M, ?>, H extends AbstractUIHandler<M, UI>>
        extends AbstractReefDbSaveAction<M, UI, H> {

    private static final Log LOG = LogFactory.getLog(AbstractReefDbRemoteSaveAction.class);

    private boolean saveAborted = false;

    protected abstract void doSave();

    /**
     * <p>Constructor for AbstractAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractReefDbRemoteSaveAction(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    public boolean isSaveAborted() {
        return saveAborted;
    }

    @Override
    public boolean prepareAction() throws Exception {

        if (!super.prepareAction()) {
            return false;
        }

        // if the screen can be saved and the screen is valid
        if (!getModel().isModify()) {
            return false;
        }
        if (!getModel().isValid()) {
            displayErrorMessage(t("reefdb.action.save.errors.title"), t("reefdb.action.save.errors.remove"));
            return false;
        }

        return true;

    }

    @Override
    public void doAction() throws Exception {

        // Check server connection (Mantis #47999)
        try {
            getContext().getProgramStrategyService().getRemoteWritableProgramsByUser(getContext().getAuthenticationInfo());
        } catch (QuadrigeTechnicalException ex) {
            LOG.warn("Connection to synchronization server failed");
            displayWarningMessage(t("reefdb.action.save.errors.title"), t("reefdb.error.synchro.serverUnavailable"));
            saveAborted = true;
            return;
        }

        // Perform save action and reload
        try {

            doSave();

            reload();

        } catch (Throwable throwable) {

            // If a concurrent save occurs, ask user to import referential
            if (Exceptions.hasCause(throwable, BadUpdateDtException.class)) {
                LOG.warn(throwable.getMessage(), throwable);
                boolean confirmImportReferential = getContext().getDialogHelper().showOptionDialog(null,
                        ApplicationUIUtil.getHtmlString(t("reefdb.action.common.import.referential.confirm")),
                        t("reefdb.action.save.errors.title"),
                        JOptionPane.ERROR_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("reefdb.common.import"),
                        t("reefdb.common.cancel")
                ) == JOptionPane.OK_OPTION;

                if (confirmImportReferential) {

                    // Import referential
                    doImportReferential();

                    // then reload
                    reload();

                } else {

                    // Abort save, don't reload
                    saveAborted = true;
                }

            } else if (Exceptions.hasCause(throwable, SaveForbiddenException.class)) {
                SaveForbiddenException exception = (SaveForbiddenException) Exceptions.getCause(throwable, SaveForbiddenException.class);

                onSaveForbiddenException(exception);

                saveAborted = true;

            } else
                // throw other exceptions
                throw throwable;

        }
    }

    protected void reload() {
        // nothing by default
    }

    protected void onSaveForbiddenException(SaveForbiddenException exception) {
        // nothing by default
    }

    @Override
    public void postSuccessAction() {
        if (isSaveAborted()) {
            // no reload if aborted
            return;
        }

        super.postSuccessAction();
    }

    @Override
    protected void releaseAction() {

        saveAborted = false;

        super.releaseAction();
    }
}
