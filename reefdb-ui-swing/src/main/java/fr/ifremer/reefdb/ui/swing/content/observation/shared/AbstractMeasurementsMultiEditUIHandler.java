package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.state.SwingTableSessionState;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.MultipleValueCellRenderer;
import jaxx.runtime.swing.session.State;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * abstract handler for multiline edition
 */
public abstract class AbstractMeasurementsMultiEditUIHandler<
    R extends AbstractMeasurementsGroupedRowModel<MeasurementDTO, R>,
    M extends AbstractMeasurementsMultiEditUIModel<MeasurementDTO, R, M>,
    UI extends ReefDbUI<M, ?>
    >
    extends AbstractMeasurementsGroupedTableUIHandler<R, M, UI>
    implements Cancelable {

    /**
     * <p>Constructor for AbstractMeasurementsMultiEditUIHandler.</p>
     */
    public AbstractMeasurementsMultiEditUIHandler() {
        super(R.PROPERTY_TAXON_GROUP,
            R.PROPERTY_TAXON,
            R.PROPERTY_INDIVIDUAL_PMFMS,
            R.PROPERTY_COMMENT);
    }

    /**
     * get the referent table for columns adjustments
     *
     * @return the referent table
     */
    protected abstract SwingTable getReferentTable();

    /**
     * Don't really build rows but create a multi edit row
     *
     * @param readOnly readOnly
     * @return rows from model
     */
    @Override
    protected List<R> buildRows(boolean readOnly) {

        if (getModel().getRowsToEdit().isEmpty())
            return null;

        // Create a ghost row without parent
        R multiEditRow = createNewRow(readOnly, null);
        multiEditRow.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));
        ReefDbBeans.createEmptyMeasurements(multiEditRow);

        boolean firstRow = true;
        for (R row : getModel().getRowsToEdit()) {

            // find multiple value on each known columns
            findMultipleValueOnIdentifier(row, multiEditRow, firstRow);

            // find multiple value on each measurement
            findMultipleValueOnPmfmIdentifier(row, multiEditRow, firstRow);

            firstRow = false;
        }

        overrideColumnRenderersAndEditors(multiEditRow);

        overrideColumnWidthAndPosition();

        return ImmutableList.of(multiEditRow);
    }

    // detect multiple values on each column
    private void findMultipleValueOnIdentifier(R row, R multiEditRow, boolean firstRow) {

        for (ReefDbColumnIdentifier<R> identifier : getModel().getIdentifiersToCheck()) {
            if (!multiEditRow.getMultipleValuesOnIdentifier().contains(identifier)) {
                if (!Objects.equals(identifier.getValue(row), identifier.getValue(multiEditRow))) {
                    if (firstRow) {
                        // on first row, consider a unique value
                        identifier.setValue(multiEditRow, identifier.getValue(row));
                    } else {
                        // on other rows, if the value differs, set it as multiple
                        identifier.setValue(multiEditRow, null);
                        multiEditRow.addMultipleValuesOnIdentifier(identifier);
                    }
                }
            }
        }
    }

    // detect multiple values on each pmfm column
    private void findMultipleValueOnPmfmIdentifier(R row, R multiEditRow, boolean firstRow) {

        for (PmfmDTO pmfm : getModel().getPmfms()) {
            if (!multiEditRow.getMultipleValuesOnPmfmIds().contains(pmfm.getId())) {
                MeasurementDTO multiMeasurement = ReefDbBeans.getIndividualMeasurementByPmfmId(multiEditRow, pmfm.getId());
                Assert.notNull(multiMeasurement, "should be already present");
                MeasurementDTO measurement = ReefDbBeans.getIndividualMeasurementByPmfmId(row, pmfm.getId());
                if (measurement == null) {
                    // create ghost measurement
                    measurement = ReefDbBeanFactory.newMeasurementDTO();
                    measurement.setPmfm(pmfm);
                }

                if (!ReefDbBeans.measurementValuesEquals(multiMeasurement, measurement)) {
                    if (firstRow) {
                        // on first row, consider a unique value
                        multiMeasurement.setNumericalValue(measurement.getNumericalValue());
                        multiMeasurement.setQualitativeValue(measurement.getQualitativeValue());
                    } else {
                        // on other rows, if the value differs, set it as multiple
                        multiMeasurement.setNumericalValue(null);
                        multiMeasurement.setQualitativeValue(null);
                        multiEditRow.addMultipleValuesOnPmfmId(pmfm.getId());
                    }
                }
            }
        }
    }

    /**
     * Override all column renderers with specific behavior if multiple values is found
     *
     * @param multiEditRow
     */
    private void overrideColumnRenderersAndEditors(R multiEditRow) {

        multiEditRow.getMultipleValuesOnIdentifier().forEach(identifier -> {
            TableColumn col = getTable().getColumnExt(identifier);
            // override the renderer
            col.setCellRenderer(new MultipleValueCellRenderer(col.getCellRenderer()));
        });
        multiEditRow.getMultipleValuesOnPmfmIds().forEach(pmfmId ->
            getModel().getPmfmColumns().stream().filter(pmfmTableColumn -> pmfmTableColumn.getPmfmId() == pmfmId).findFirst()
                .ifPresent(pmfmTableColumn -> {
                        // override the renderer
                        pmfmTableColumn.setCellRenderer(new MultipleValueCellRenderer(pmfmTableColumn.getCellRenderer()));
                        // and set not editable if the pmfm is set as read-only
                        if (getModel().getReadOnlyPmfmIds().contains(pmfmId))
                            pmfmTableColumn.setEditable(false);
                    }
                ));
    }

    private void overrideColumnWidthAndPosition() {

        // get the state of the referent table
        State referentState = getContext().getSwingSession().findStateByComponentName(".*" + getReferentTable().getName());

        // apply this state to the current table
        if (referentState instanceof SwingTableSessionState) {
            referentState.setState(getTable(), referentState);
        }
    }

    @Override
    protected void onRowsAdded(List<R> addedRows) {
        // Don't call super method because the row is already initialized in buildRows()
//        super.onRowsAdded(addedRows);
    }

    @Override
    protected List<? extends MeasurementAware> getMeasurementAwareModels() {
        // nothing to return, this handler don't save model
        return null;
    }

    @Override
    protected MeasurementAware getMeasurementAwareModelForRow(R row) {
        // nothing to return, this handler don't save model
        return null;
    }

    @Override
    protected void filterMeasurements() {
    }

    public void valid() {
        if (getModel().isValid()) {
            closeDialog();
        }
    }

    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }

}
