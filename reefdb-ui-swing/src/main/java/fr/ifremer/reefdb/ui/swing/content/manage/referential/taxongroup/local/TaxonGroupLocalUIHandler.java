package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.menu.TaxonGroupMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.table.TaxonGroupTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.table.TaxonGroupTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedTaxonCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedTaxonCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des groupeTaxons au niveau local
 */
public class TaxonGroupLocalUIHandler extends
        AbstractReefDbTableUIHandler<TaxonGroupTableRowModel, TaxonGroupLocalUIModel, TaxonGroupLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(TaxonGroupLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final TaxonGroupLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final TaxonGroupLocalUIModel model = new TaxonGroupLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final TaxonGroupLocalUI ui) {
        initUI(ui);

        // hide context filter panel
        ui.getTaxonGroupLocalMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getTaxonGroupLocalMenuUI().getHandler().forceLocal(true);

        // listen to search result
        ui.getTaxonGroupLocalMenuUI().getModel().addPropertyChangeListener(TaxonGroupMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<TaxonGroupDTO>) evt.getNewValue()));

        // Initialisation du tableau
        initTable();

        getUI().getGererGroupeTaxonsLocalTableSupprimerBouton().setEnabled(false);
        getUI().getGererGroupeTaxonsLocalTableRemplacerBouton().setEnabled(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // parent
        TableColumnExt parentCol = addFilterableComboDataColumnToModel(
                TaxonGroupTableModel.PARENT,
                getContext().getReferentialService().getTaxonGroups(),
                true);

        parentCol.setSortable(true);

        // label
        TableColumnExt labelCol = addColumn(TaxonGroupTableModel.LABEL);
        labelCol.setSortable(true);

        // name
        TableColumnExt nameCol = addColumn(TaxonGroupTableModel.NAME);
        nameCol.setSortable(true);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                TaxonGroupTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        // comment
        final TableColumnExt commentCol = addCommentColumn(TaxonGroupTableModel.COMMENT);
        commentCol.setSortable(false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(TaxonGroupTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(TaxonGroupTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // taxons
        TableColumnExt taxonsCol = addColumn(
                new AssociatedTaxonCellEditor(getTable(), getUI(), true),
                new AssociatedTaxonCellRenderer(),
                TaxonGroupTableModel.TAXONS
        );
        taxonsCol.setSortable(true);

        TaxonGroupTableModel tableModel = new TaxonGroupTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.taxonGroups.local"), TaxonGroupTableModel.TAXONS);

        // Initialisation du tableau
        initTable(getTable());

        // optionnal columns are hidden
        commentCol.setVisible(false);
        taxonsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(5);

    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(TaxonGroupTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isUnique(row);
    }

    private boolean isUnique(TaxonGroupTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (TaxonGroupTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.taxonGroup"), row.getName(), t("reefdb.property.referential.local")),
                            TaxonGroupTableRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                TaxonGroupCriteriaDTO criteria = ReefDbBeanFactory.newTaxonGroupCriteriaDTO();
                criteria.setName(row.getName());
                criteria.setStrictName(true);
                List<TaxonGroupDTO> existingTaxonGroups = getContext().getReferentialService().searchTaxonGroups(criteria);
                if (CollectionUtils.isNotEmpty(existingTaxonGroups)) {
                    for (TaxonGroupDTO taxonGroup : existingTaxonGroups) {
                        if (!taxonGroup.getId().equals(row.getId())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential", t("reefdb.property.taxonGroup"), row.getName(),
                                            ReefDbBeans.isLocalStatus(taxonGroup.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    TaxonGroupTableRowModel.PROPERTY_NAME);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();

    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<TaxonGroupTableRowModel> getTableModel() {
        return (TaxonGroupTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getTaxonGroupLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<TaxonGroupTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            TaxonGroupTableRowModel rowModel = addedRows.get(0);
            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(rowModel);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, TaxonGroupTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

}
