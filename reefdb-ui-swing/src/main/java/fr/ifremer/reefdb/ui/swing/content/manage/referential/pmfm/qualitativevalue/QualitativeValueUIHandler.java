package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handler.
 */
public class QualitativeValueUIHandler extends AbstractReefDbTableUIHandler<QualitativeValueTableRowModel, QualitativeValueUIModel, QualitativeValueUI> implements Cancelable {

    private static final Log LOG = LogFactory.getLog(QualitativeValueUIHandler.class);

    /** Constant <code>DOUBLE_LIST="doubleList"</code> */
    public static final String DOUBLE_LIST = "doubleList";
    /** Constant <code>TABLE="table"</code> */
    public static final String TABLE = "table";

    /** {@inheritDoc} */
    @Override
    public void beforeInit(QualitativeValueUI ui) {
        super.beforeInit(ui);

        QualitativeValueUIModel model = new QualitativeValueUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(QualitativeValueUI qualitativeValueUI) {

        initUI(qualitativeValueUI);

        // Init table columns and properties
        initTable();

        initBeanList(getUI().getAssociatedQualitativeValuesDoubleList(), null, null);

        getModel().addPropertyChangeListener(QualitativeValueUIModel.PROPERTY_PARENT_ROW_MODEL, evt -> {

            // here, detect parentRowModel type , if PmfmDTO and local, set a DoubleList view
            getModel().setDoubleList(
                getModel().isEditable()
                    && (
                    PmfmDTO.class.isAssignableFrom(getModel().getParentRowModel().getClass())
                        || PmfmStrategyDTO.class.isAssignableFrom(getModel().getParentRowModel().getClass())
                )
            );

            if (getModel().isDoubleList()) {
                List<QualitativeValueDTO> universeValues = null;
                List<QualitativeValueDTO> selectedValues = null;

                if (PmfmDTO.class.isAssignableFrom(getModel().getParentRowModel().getClass())) {
                    PmfmDTO pmfm = (PmfmDTO) getModel().getParentRowModel();
                    if (pmfm.getParameter() != null) {
                        // gather qualitative values from service (Mantis #49916,49708)
                        List<QualitativeValueDTO> qualitativeValues = getContext().getReferentialService().getQualitativeValues(pmfm.getParameter().getCode());
                        // filter active qualitative values only
                        universeValues = ReefDbBeans.filterCollection(qualitativeValues, (Predicate<QualitativeValueDTO>) input ->
                            input != null && input.getStatus() != null && StatusFilter.ACTIVE.toStatusCodes().contains(input.getStatus().getCode()));
                        // sort by label
                        universeValues.sort(Comparator.comparing(BaseReferentialDTO::getName));
                    }
                    selectedValues = Lists.newArrayList(pmfm.getQualitativeValues());

                } else if (PmfmStrategyDTO.class.isAssignableFrom(getModel().getParentRowModel().getClass())) {

                    PmfmStrategyDTO pmfmStrategy = (PmfmStrategyDTO) getModel().getParentRowModel();
                    if (pmfmStrategy.getPmfm() != null && pmfmStrategy.getPmfm().getParameter() != null) {
                        List<QualitativeValueDTO> qualitativeValues = pmfmStrategy.getPmfm().getQualitativeValues();
                        // sort by label
                        universeValues = qualitativeValues.stream().sorted(Comparator.comparing(BaseReferentialDTO::getName)).collect(Collectors.toList());
                    }
                    selectedValues = Lists.newArrayList(pmfmStrategy.getQualitativeValues());
                }

                getUI().getAssociatedQualitativeValuesDoubleList().getModel().setUniverse(universeValues);
                getUI().getAssociatedQualitativeValuesDoubleList().getModel().setSelected(selectedValues);

                getUI().getListPanelLayout().setSelected(DOUBLE_LIST);
            } else {
                List<QualitativeValueDTO> qualitativeValues = getQualitativeValuesFromRow(getModel().getParentRowModel());
                if (qualitativeValues != null) {
                    qualitativeValues.sort(Comparator.comparing(BaseReferentialDTO::getName));
                }
                getModel().setBeans(qualitativeValues);
                getUI().getListPanelLayout().setSelected(TABLE);
            }
        });
    }

    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(QualitativeValueTableModel.MNEMONIC);
        mnemonicCol.setSortable(true);

        // Description
        final TableColumnExt descriptionCol = addColumn(QualitativeValueTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                QualitativeValueTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        // model for table
        final QualitativeValueTableModel tableModel = new QualitativeValueTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable());

        // 5 lines visibles
        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<QualitativeValueTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            QualitativeValueTableRowModel rowModel = addedRows.get(0);
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            getModel().setModify(true);
            setFocusOnCell(rowModel);
        }
    }

    private List<QualitativeValueDTO> getQualitativeValuesFromRow(AbstractReefDbRowUIModel rowModel) {
        if (ParameterDTO.class.isAssignableFrom(rowModel.getClass())) {
            return ((ParameterDTO) rowModel).getQualitativeValues();
        }
        if (PmfmDTO.class.isAssignableFrom(rowModel.getClass())) {
            return ((PmfmDTO) rowModel).getQualitativeValues();
        }
        if (PmfmStrategyDTO.class.isAssignableFrom(rowModel.getClass())) {
            return ((PmfmStrategyDTO) rowModel).getQualitativeValues();
        }
        return null;
    }

    private void setQualitativeValuesToRow(List<QualitativeValueDTO> qualitativeValues, AbstractReefDbRowUIModel rowModel) {
        if (ParameterDTO.class.isAssignableFrom(rowModel.getClass())) {
            ((ParameterDTO) rowModel).setQualitativeValues(qualitativeValues);
        } else if (PmfmDTO.class.isAssignableFrom(rowModel.getClass())) {
            ((PmfmDTO) rowModel).setQualitativeValues(qualitativeValues);
        } else if (PmfmStrategyDTO.class.isAssignableFrom(rowModel.getClass())) {
            ((PmfmStrategyDTO) rowModel).setQualitativeValues(qualitativeValues);
        }
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // Replace the old associated qualitative value liste by the new list (no save here !)

        setQualitativeValuesToRow(
                getModel().isDoubleList()
                        ? getUI().getAssociatedQualitativeValuesDoubleList().getModel().getSelected()
                        : getModel().getBeans(),
                getModel().getParentRowModel());

        getModel().getParentRowModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);

        // close the dialog box
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<QualitativeValueTableRowModel> getTableModel() {
        return (QualitativeValueTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getAssociatedQualitativeValuesTable();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
