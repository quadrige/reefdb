package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.menu.ManageFractionsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedMatricesCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedSupportsCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Fractions au niveau local
 */
public class ManageFractionsLocalUIHandler extends AbstractReefDbTableUIHandler<FractionsTableRowModel, ManageFractionsLocalUIModel, ManageFractionsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageFractionsLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageFractionsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageFractionsLocalUIModel model = new ManageFractionsLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageFractionsLocalUI ui) {
        initUI(ui);

        // Initialisation du tableau
        initTable();

        getUI().getManageFractionsLocalTableDeleteBouton().setEnabled(false);
        getUI().getManageFractionsLocalTableReplaceBouton().setEnabled(false);

        ManageFractionsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(true);

        menuUIModel.addPropertyChangeListener(ManageFractionsMenuUIModel.PROPERTY_RESULTS, evt -> loadFractionsManageLocal((List<FractionDTO>) evt.getNewValue()));
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(FractionsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(true);

        // Comment, creation and update dates
        addCommentColumn(FractionsTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(FractionsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(FractionsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                FractionsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);
        fixDefaultColumnWidth(statusCol);

        // description
        final TableColumnExt descriptionCol = addColumn(FractionsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(true);

        // associated fractions
        final TableColumnExt associatedSupportsCol = addColumn(
                new AssociatedMatricesCellEditor(getTable(), getUI(), true),
                new AssociatedSupportsCellRenderer(),
                FractionsTableModel.ASSOCIATED_SUPPORTS);
        associatedSupportsCol.setSortable(true);

        FractionsTableModel tableModel = new FractionsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        associatedSupportsCol.setVisible(false);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.fractions.local"), FractionsTableModel.ASSOCIATED_SUPPORTS);

        // Initialisation du tableau
        initTable(getTable());

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<FractionsTableRowModel> getTableModel() {
        return (FractionsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageFractionsLocalTable();
    }

    /**
     * Fractione permettant le changement des manage de fraction au niveau local.
     *
     * @param fractions La liste des manage de fraction au niveau local
     */
    public void loadFractionsManageLocal(final List<FractionDTO> fractions) {
        // Enabeld new button
        getUI().getManageFractionsLocalTableNouveauBouton().setEnabled(true);

        // Chargement des lignes du tableau
        getModel().setBeans(fractions);
        getModel().setModify(false);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<FractionsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            FractionsTableRowModel row = addedRows.get(0);
            // Set default status
            row.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(row);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, FractionsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(FractionsTableRowModel row) {
        row.getErrors().clear();

        return super.isRowValid(row) && row.sizeMatrixes() > 0 && isUnique(row);
    }

    private boolean isUnique(FractionsTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (FractionsTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.pmfm.fraction"), row.getName(), t("reefdb.property.referential.local")),
                            FractionsTableRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<FractionDTO> existingFractions = getContext().getReferentialService().searchFractions(StatusFilter.ALL, null, null);
                if (CollectionUtils.isNotEmpty(existingFractions)) {
                    for (FractionDTO fraction : existingFractions) {
                        if (!fraction.getId().equals(row.getId())
                                && row.getName().equalsIgnoreCase(fraction.getName())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.pmfm.fraction"), row.getName(), ReefDbBeans.isLocalStatus(fraction.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    FractionsTableRowModel.PROPERTY_NAME);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();

    }

}
