package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.menu.AnalysisInstrumentsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.table.AnalysisInstrumentsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.table.AnalysisInstrumentsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des analysisInstruments au niveau national
 */
public class AnalysisInstrumentsNationalUIHandler extends AbstractReefDbTableUIHandler<AnalysisInstrumentsTableRowModel, AnalysisInstrumentsNationalUIModel, AnalysisInstrumentsNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(AnalysisInstrumentsNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AnalysisInstrumentsNationalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        AnalysisInstrumentsNationalUIModel model = new AnalysisInstrumentsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(AnalysisInstrumentsNationalUI ui) {
        initUI(ui);

        // force national
        ui.getAnalysisInstrumentsNationalMenuUI().getHandler().forceLocal(false);

        // init listener on found analysis instruments
        ui.getAnalysisInstrumentsNationalMenuUI().getModel().addPropertyChangeListener(AnalysisInstrumentsMenuUIModel.PROPERTY_RESULTS, evt -> {
            if (evt.getNewValue() != null) {
                getModel().setBeans((List<AnalysisInstrumentDTO>) evt.getNewValue());
            }
        });
        
        // listen to 'apply filter' results
        ui.getAnalysisInstrumentsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            // load only national referential (Mantis #29668)
            getModel().setBeans(ReefDbBeans.filterNationalReferential((List<AnalysisInstrumentDTO>) evt.getNewValue()));
        });

        initTable();

    }

    private void initTable() {

        // mnemonic
        TableColumnExt mnemonicCol = addColumn(AnalysisInstrumentsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        TableColumnExt descriptionCol = addColumn(AnalysisInstrumentsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(AnalysisInstrumentsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(AnalysisInstrumentsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(AnalysisInstrumentsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                AnalysisInstrumentsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        AnalysisInstrumentsTableModel tableModel = new AnalysisInstrumentsTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.analysisInstruments.national"));

        // Initialisation du tableau
        initTable(getTable(), true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(5);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
    	getModel().setBeans(null);
    }
    
    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<AnalysisInstrumentsTableRowModel> getTableModel() {
        return (AnalysisInstrumentsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getAnalysisInstrumentsNationalTable();
    }
}
