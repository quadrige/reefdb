package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.synchro.meta.data.DataSynchroTables;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRejectedRowResolver;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroClientImportFromFileResult;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroOperationType;
import fr.ifremer.quadrige3.synchro.vo.SynchroProgressionStatus;
import fr.ifremer.quadrige3.ui.swing.ApplicationUI;
import fr.ifremer.quadrige3.ui.swing.action.AbstractReloadCurrentScreenAction;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroDirection;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIContext;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIHandler;
import fr.ifremer.quadrige3.ui.swing.synchro.action.AbstractSynchroAction;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroStartAction;
import fr.ifremer.quadrige3.ui.swing.synchro.resolver.SynchroRejectedRowUIResolver;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.synchro.SynchroClientService;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;
import fr.ifremer.reefdb.ui.swing.content.synchro.changes.duplicate.SynchroDuplicatesUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Dimension;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ImportFromFileSynchroAction class.</p>
 *
 * @author benoit.lavenier@e-is.pro
 * @since 1.0
 */
public class ImportFromFileSynchroAction extends AbstractReloadCurrentScreenAction {
    private static final Log log = LogFactory.getLog(ImportFromFileSynchroAction.class);

    private AbstractSynchroAction nextSynchroAction;
    private File file = null;
    private boolean referentialOnly = false;

    /**
     * <p>Constructor for ImportFromFileSynchroAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    public ImportFromFileSynchroAction(ReefDbMainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("reefdb.action.synchro.importFromFile.title"));
    }

    private SynchroUIContext getSynchroUIContext() {
        return getContext().getSynchroContext();
    }

    private SynchroUIHandler getSynchroHandler() {
        return getContext().getSynchroHandler();
    }

    /**
     * <p>Setter for the field <code>referentialOnly</code>.</p>
     *
     * @param referentialOnly a boolean.
     */
    public void setReferentialOnly(boolean referentialOnly) {
        this.referentialOnly = referentialOnly;
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            if (file == null) {

                // choose file to import
                file = chooseFile(
                        t("reefdb.action.synchro.importFromFile.choose.title"),
                        t("reefdb.action.synchro.importFromFile.choose.buttonLabel"),
                        "^.*\\.zip", t("reefdb.common.file.zip")
                );

                if (file == null) {

                    displayWarningMessage(
                            t("reefdb.action.synchro.importFromFile.choose.title"),
                            t("reefdb.action.synchro.importFromFile.choose.noFile")
                    );

                    doAction = false;
                }
            }
        }
        return doAction;
    }

    /** {@inheritDoc} */
    @Override
    public void doActionBeforeReload() {
        // test actual progression
        getSynchroUIContext().getProgressionModel().clear();
        getSynchroUIContext().setDirection(SynchroDirection.IMPORT);
        getSynchroUIContext().loadImportContext();
        boolean isSynchronizationUsingServer = getConfig().isSynchronizationUsingServer();

        // If last synchronization has failedAction
        if (getSynchroUIContext().getStatus() == SynchroProgressionStatus.FAILED) {
            getSynchroHandler().report(t("quadrige3.synchro.report.failed"));
            return;
        }

        // If synchronisation file is already here, apply it
        if (isSynchronizationUsingServer && getSynchroUIContext().getStatus() == SynchroProgressionStatus.SUCCESS && getSynchroUIContext().getWorkingDirectory() != null) {
            // already downloaded
            getSynchroHandler().showValidApplyPopup();
            return;
        }

        // If last status was RUNNING
        // AND there is a jobId (mantis #24916)
        if (isSynchronizationUsingServer
                && getSynchroUIContext().isRunningStatus()) {
            if (getSynchroUIContext().getImportJobId() != null) {
                nextSynchroAction = getContext().getActionFactory().createNonBlockingUIAction(getSynchroHandler(), ImportSynchroStartAction.class);
                return;
            } else {
                // This should never append, but reset some attributes anyway (NOT a complete reset, to avoid Direction reset...)
                getSynchroUIContext().setImportJobId(null);
                getSynchroUIContext().setStatus(SynchroProgressionStatus.NOT_STARTED);
            }
        }

        // auto select referential AND data updates (only if not referential only)
        getSynchroUIContext().setImportReferential(true);
        getSynchroUIContext().setImportData(!referentialOnly);
        getSynchroUIContext().setStatus(SynchroProgressionStatus.RUNNING);
        getSynchroUIContext().saveImportContext();

        SynchroClientService synchroService = ReefDbServiceLocator.instance().getSynchroClientService();
        int userId = SecurityContextHelper.getQuadrigeUserId();

        // First, get import changes
        createProgressionUIModel(100);
        SynchroChangesDTO changes = synchroService.getImportFileChangesAsDTO(
                userId,
                file,
                getSynchroUIContext().getSynchroImportContext(),
                getProgressionUIModel(),
                100
        );
        getProgressionUIModel().setTotal(100);

        // If no changes detected: exit
        if (changes.isTablesEmpty()) {
            getSynchroUIContext().resetImportContext();
            getSynchroHandler().report(t("reefdb.error.synchro.importFromFile.noData"), true);
            setSkipScreenReload(true);
            return;
        }

        // need a full import when program/strategy or rules is present
        boolean hasProgramOrRulesChanges = synchroService.hasProgramOrRulesChanges(changes);

        // Filter changes
        SynchroChangesDTO referentialChangesToApply = synchroService.getReferentialSynchroChangesToApplyFromFile(changes);

        // If user is a local admin : get changelog first
        if (SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.LOCAL_ADMIN)) {

            /* TODO LP 05/01/2018 : Simplification du CU 'Import par fichier' : pas d'import des programmes / règles
            if (hasProgramOrRulesChanges) {

                if (getContext().getDialogHelper().showConfirmDialog(getUI(),
                        t("reefdb.action.synchro.importFromFile.fullImport.message"),
                        t("reefdb.action.synchro.importFromFile.title"),
                        JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                    getSynchroUIContext().resetImportContext();
                    getSynchroHandler().report(t("reefdb.synchro.report.idle"), false);
                    setSkipScreenReload(true);
                    return;
                }

            } else if (!referentialChangesToApply.isTablesEmpty()) {

                if (getContext().getDialogHelper().showConfirmDialog(getUI(),
                        t("reefdb.action.synchro.importFromFile.newImport.message"),
                        t("reefdb.action.synchro.importFromFile.title"),
                        JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                    getSynchroUIContext().resetImportContext();
                    getSynchroHandler().report(t("reefdb.synchro.report.idle"), false);
                    setSkipScreenReload(true);
                    return;
                }

            }*/

            /* LP 16/11/2015 : SynchroChangesUI is disabled for the moment
            SynchroChangesUI diffUI = new SynchroChangesUI(getUI());
            diffUI.getModel().setChanges(changes);
            handler.openDialog(diffUI, t("reefdb.action.synchro.importFromFile.changes.title"), new Dimension(800, 400));

            // no change to apply (or user cancelled) : exit
            if (!diffUI.getModel().isChangesValidated()) {

                getSynchroUIContext().resetImportContext();
                setSkipScreenReload(true);
                return;
            }
            */
        }

        // get survey DUPLICATION or INSERT
        SynchroChangesDTO surveyChangesToApply = synchroService.getSurveySynchroChangesFromFile(changes);

        // Select Survey duplicate to import as insert
        if (surveyChangesToApply != null && !surveyChangesToApply.isTablesEmpty()) {

            SynchroDuplicatesUI duplicatesUI = new SynchroDuplicatesUI((ApplicationUI) getUI());
            duplicatesUI.getModel().setTableNameFilter(DataSynchroTables.SURVEY.name());
            duplicatesUI.getModel().setChanges(surveyChangesToApply);
            getHandler().openDialog(duplicatesUI, new Dimension(800, 400));
            if (duplicatesUI.getModel().isChangesValidated()) {
                surveyChangesToApply = duplicatesUI.getModel().getChanges();
            } else {
                surveyChangesToApply = null;
            }

        }

        // abort here if nothing to import
        if (referentialChangesToApply.isTablesEmpty() && isSurveyChangesEmpty(surveyChangesToApply)) {
            getSynchroUIContext().resetImportContext();
            getSynchroHandler().report(t("reefdb.error.synchro.importFromFile.noData"), true);
            setSkipScreenReload(true);
            return;
        }

        // Build referential PK includes Map
        Multimap<String, String> referentialPkIncludes = synchroService.toPkToIncludesMap(referentialChangesToApply);
        getSynchroUIContext().setImportReferentialPkIncludes(referentialPkIncludes);

        // build surveys PK include Map if surveyChangesToApply if not null
        if (isSurveyChangesEmpty(surveyChangesToApply)) {
            // disable import data if user cancel survey import (see Mantis#0029876)
            getSynchroUIContext().setImportData(false);
        } else {
            Multimap<String, String> surveyPkIncludes = synchroService.toPkToIncludesMap(surveyChangesToApply);
            getSynchroUIContext().setImportDataPkIncludes(surveyPkIncludes);
            // enable force duplication in context only if replace (see Mantis #34569)
            if (hasSurveyReplace(surveyChangesToApply)) {
                getSynchroUIContext().setForceDuplication(true);
            }
        }

        // build temp database and export local to temp
        getProgressionUIModel().setTotal(100);
        getProgressionUIModel().setMessage(t("quadrige3.synchro.progress.import"));
        SynchroRejectedRowResolver rejectResolver = new SynchroRejectedRowUIResolver(getContext().getDialogHelper(), true /*isImport*/);

        // export directory is set by the synchronization service
        SynchroClientImportFromFileResult importResult = synchroService.importFromFile(
                userId,
                file,
                getSynchroUIContext().getSynchroImportContext(),
                rejectResolver,
                getProgressionUIModel(),
                100);

        boolean isReferentialUpdated = importResult.getReferentialResult() != null && importResult.getReferentialResult().getTotalTreated() > 0;
        boolean isDataUpdated = importResult.getDataResult() != null && importResult.getDataResult().getTotalTreated() > 0;

        // reset cache
        if (isReferentialUpdated) {
            if (log.isInfoEnabled()) {
                log.info("Reset all caches.");
            }
            getProgressionUIModel().setMessage(t("quadrige3.synchro.progress.resetCache"));
            getContext().reloadDbCache(getProgressionUIModel());
        }

        if (!getContext().isAuthenticated()) {
            // If the login has changed to another user, the re-authentication could have failed
            // Do not try to store the import context (mantis #23535) because the user id is not known
            if (log.isInfoEnabled()) {
                log.warn(t("reefdb.action.synchro.import.success.notAuthenticated"));
            }
            // reset synchronization context
            getSynchroUIContext().resetImportContext();
            getSynchroHandler().report(t("reefdb.action.synchro.import.success.notAuthenticated"), true);
            getProgressionUIModel().increments(2);
        } else {
            getSynchroHandler().report(t("reefdb.action.synchro.import.success"), true);
        }

        // delegate progression model from SynchroUIContext
        getProgressionUIModel().setTotal(100);

        // Skip screen reloading if NO updates
        setSkipScreenReload(!isDataUpdated && !isReferentialUpdated);
    }

    private boolean hasSurveyReplace(SynchroChangesDTO surveyChangesToApply) {

        SynchroTableDTO surveyTable = null;
        for (SynchroTableDTO table : surveyChangesToApply.getTables()) {
            if (table.getName().equalsIgnoreCase(DataSynchroTables.SURVEY.name())) {
                surveyTable = table;
                break;
            }
        }

        return surveyTable != null && !ReefDbBeans.filterCollection(surveyTable.getRows(), (Predicate<SynchroRowDTO>) input -> input != null && SynchroOperationType.DUPLICATE.toString().equalsIgnoreCase(input.getOperationType())).isEmpty();
    }

    private boolean isSurveyChangesEmpty(SynchroChangesDTO surveyChanges) {
        if (surveyChanges == null || surveyChanges.isTablesEmpty()) {
            return true;
        }
        for (SynchroTableDTO table : surveyChanges.getTables()) {
            if (DataSynchroTables.SURVEY.name().equalsIgnoreCase(table.getName())) {
                return table.isRowsEmpty();
            }
        }
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getSynchroUIContext().saveImportContext();

        if (nextSynchroAction != null && getConfig().isSynchronizationUsingServer()) {
            nextSynchroAction.execute();
        }

    }

    /** {@inheritDoc} */
    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);

        getSynchroHandler().report(t("quadrige3.synchro.report.failed"), true);
        getSynchroUIContext().setStatus(SynchroProgressionStatus.FAILED);

        log.error("Synchronization import from file failed");
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        super.releaseAction();
        file = null;
        referentialOnly = false;
        nextSynchroAction = null;
    }

}
