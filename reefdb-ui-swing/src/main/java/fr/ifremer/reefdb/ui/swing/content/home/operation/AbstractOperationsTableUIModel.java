package fr.ifremer.reefdb.ui.swing.content.home.operation;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * @author peck7 on 28/06/2018.
 */
public abstract class AbstractOperationsTableUIModel<M extends AbstractOperationsTableUIModel<M>>
        extends AbstractReefDbTableUIModel<SamplingOperationDTO, OperationsTableRowModel, M> {

    private SurveysTableRowModel survey;
    public static final String PROPERTY_SURVEY = "survey";
    public static final String PROPERTY_SURVEY_EDITABLE = "surveyEditable";

    /**
     * <p>Getter for the field <code>survey</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public SurveysTableRowModel getSurvey() {
        return survey;
    }

    /**
     * <p>Setter for the field <code>survey</code>.</p>
     *
     * @param survey a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public void setSurvey(SurveysTableRowModel survey) {
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, null, survey);
        firePropertyChange(PROPERTY_SURVEY_EDITABLE, null, isSurveyEditable());
    }

    /**
     * <p>isSurveyEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isSurveyEditable() {
        return survey != null && survey.isEditable();
    }
}
