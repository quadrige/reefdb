package fr.ifremer.reefdb.ui.swing.content.manage.program.programs.departments;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.program.ProgramPrivilegeIds;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * <p>DepartmentsDialogUIModel class.</p>
 *
 */
public class DepartmentsDialogUIModel extends AbstractEmptyUIModel<DepartmentsDialogUIModel> {

    private ProgramPrivilegeIds privilege;

    private ProgramsTableRowModel program;
    /** Constant <code>PROPERTY_PROGRAM="program"</code> */
    public static final String PROPERTY_PROGRAM = "program";

    /**
     * <p>Getter for the field <code>program</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public ProgramsTableRowModel getProgram() {
        return program;
    }

    /**
     * <p>Setter for the field <code>program</code>.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public void setProgram(ProgramsTableRowModel program) {
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, null, program);
    }

    public ProgramPrivilegeIds getPrivilege() {
        return privilege;
    }

    public void setPrivilege(ProgramPrivilegeIds privilege) {
        this.privilege = privilege;
    }
}
