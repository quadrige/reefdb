package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.associatedFractions;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Modele pour l administration des Fractions au niveau national
 */
public class AssociatedFractionsUIModel extends AbstractReefDbTableUIModel<FractionDTO, FractionsTableRowModel, AssociatedFractionsUIModel> {

    private MatrixDTO matrix;
    /** Constant <code>PROPERTY_MATRIX="matrix"</code> */
    public static final String PROPERTY_MATRIX = "matrix";

    private boolean editable;
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";

    /**
     * <p>Getter for the field <code>matrix</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     */
    public MatrixDTO getMatrix() {
        return matrix;
    }

    /**
     * <p>Setter for the field <code>matrix</code>.</p>
     *
     * @param matrix a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     */
    public void setMatrix(MatrixDTO matrix) {
        this.matrix = matrix;
        firePropertyChange(PROPERTY_MATRIX, null, matrix);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }
}
