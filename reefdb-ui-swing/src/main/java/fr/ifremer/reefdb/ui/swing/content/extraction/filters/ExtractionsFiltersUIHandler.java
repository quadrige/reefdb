package fr.ifremer.reefdb.ui.swing.content.extraction.filters;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.content.extraction.filters.period.ExtractionPeriodUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des observations.
 */
public class ExtractionsFiltersUIHandler extends
        AbstractReefDbTableUIHandler<ExtractionsFiltersRowModel, ExtractionsFiltersUIModel, ExtractionsFiltersUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ExtractionsFiltersUIHandler.class);
    FilterLoader filterLoader = new FilterLoader();

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ExtractionsFiltersUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ExtractionsFiltersUIModel model = new ExtractionsFiltersUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ExtractionsFiltersUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Initialisation du tableau
        initTable();
        SwingUtil.setLayerUI(ui.getExtractionsFiltersScrollPane(), ui.getTableBlockLayer());

        // Initialisation des listeners
        initListeners();

        // first load (empty filters)
        loadFilters();
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // filter type
        TableColumnExt filterTypeCol = addColumn(ExtractionsFiltersTableModel.FILTER_TYPE);
        fixColumnWidth(filterTypeCol, 150);

        // filter
        addColumn(
                null,
                new FilterElementsCellRenderer(getContext().getDecoratorService()),
                ExtractionsFiltersTableModel.FILTER);

        ExtractionsFiltersTableModel tableModel = new ExtractionsFiltersTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable(), true);

        // no sort
        getTable().setSortable(false);
        getTable().setEditable(false);

        // border
        addEditionPanelBorder();

        // add double click auto edit line
        getTable().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    getUI().getEditFilterButton().doClick();
                }
            }
        });

    }

    private void initListeners() {

    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(ExtractionsFiltersRowModel row) {
        return getModel().getSelectedExtraction() == null || (super.isRowValid(row) && isFilterValid(row));
    }

    private boolean isFilterValid(ExtractionsFiltersRowModel row) {

        row.getErrors().clear();
        ExtractionFilterValues extractionFilter = ExtractionFilterValues.getExtractionFilter(row.getFilterTypeId());

        if (extractionFilter == ExtractionFilterValues.PERIOD) {

            if (row.isFilterEmpty()) {
                ReefDbBeans.addError(row, t("reefdb.extraction.filters.error.period.empty.message"), ExtractionsFiltersRowModel.PROPERTY_FILTER);
            }

        } else {

            Assert.notNull(extractionFilter);
            switch (extractionFilter) {
                case PROGRAM:
                    if (row.isFilterEmpty()) {
                        // search for location
                        boolean locationFound = false;
                        for (ExtractionsFiltersRowModel otherRow : getModel().getRows()) {
                            if (otherRow == row) continue;
                            if (Objects.equals(otherRow.getFilterTypeId(), ExtractionFilterValues.LOCATION.getFilterTypeId())) {
                                locationFound = !otherRow.isFilterEmpty();
                            }
                        }
                        if (!locationFound) {
                            ReefDbBeans.addError(row, t("reefdb.extraction.filters.error.program.empty.message"), ExtractionsFiltersRowModel.PROPERTY_FILTER);
                        }
                    }
                    break;

                case LOCATION:
                    if (row.isFilterEmpty()) {
                        // search for program
                        boolean programFound = false;
                        for (ExtractionsFiltersRowModel otherRow : getModel().getRows()) {
                            if (otherRow == row) continue;
                            if (Objects.equals(otherRow.getFilterTypeId(), ExtractionFilterValues.PROGRAM.getFilterTypeId())) {
                                programFound = !otherRow.isFilterEmpty();
                            }
                        }
                        if (!programFound) {
                            ReefDbBeans.addError(row, t("reefdb.extraction.filters.error.location.empty.message"), ExtractionsFiltersRowModel.PROPERTY_FILTER);
                        }
                    }
                    break;

            }

        }
        return row.getErrors().isEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ExtractionsFiltersRowModel> getTableModel() {
        return (ExtractionsFiltersTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getExtractionsFiltersTable();
    }

    /**
     * <p>loadFilters.</p>
     */
    public void loadFilters() {

        if (!filterLoader.isDone()) {
            filterLoader.cancel(true);
        }

        getModel().setLoading(true);
        filterLoader = new FilterLoader();
        filterLoader.execute();
    }

    /**
     * <p>editFilter.</p>
     */
    @SuppressWarnings("unchecked")
    public void editFilter() {

        final ExtractionsFiltersRowModel rowModel = getModel().getSingleSelectedRow();

        if (rowModel == null) {
            LOG.warn("no filter selected");
            return;
        }

        SwingUtilities.invokeLater(() -> {

            if (ExtractionFilterValues.getExtractionFilter(rowModel.getFilterTypeId()) == ExtractionFilterValues.PERIOD) {

                // open period editor
                ExtractionPeriodUI periodUI = new ExtractionPeriodUI(getUI());
                periodUI.getModel().setBeans((Collection<ExtractionPeriodDTO>) rowModel.getFilteredElements());
                periodUI.getHandler().recomputeRowsValidState(false);
                openDialog(periodUI);

                // affect to model if valid
                if (periodUI.getModel().isValid()) {
                    FilterDTO periodFilter = getModel().getSelectedExtraction().getFilterOfType(ExtractionFilterValues.PERIOD.getFilterTypeId());
                    if (periodFilter == null) {
                        periodFilter = ReefDbBeanFactory.newFilterDTO();
                        periodFilter.setFilterTypeId(ExtractionFilterValues.PERIOD.getFilterTypeId());
                    }
                    periodFilter.setElements(periodUI.getModel().getBeans());

                    getModel().getSelectedExtraction().replaceFilter(periodFilter);
                    rowModel.setFilter(periodFilter);
                }

            } else {

                // open filter ui
                SelectFilterUI filterUI = new SelectFilterUI(getContext(), rowModel.getFilterTypeId());
                filterUI.getModel().setSelectedElements(rowModel.getFilteredElements());
                filterUI.setTitle(t("reefdb.extraction.filters.select.title", decorate(rowModel.getFilterType())));
                openDialog(filterUI);

                if (filterUI.getModel().isValid()) {
                    FilterDTO filter;
                    if (rowModel.getFilter() == null) {
                        filter = ReefDbBeanFactory.newFilterDTO();
                        filter.setFilterTypeId(rowModel.getFilterTypeId());
                        rowModel.setFilter(filter);
                    } else {
                        filter = rowModel.getFilter();
                    }
                    filter.setFilterLoaded(true);
                    filter.setElements(filterUI.getModel().getSelectedElements());
                    filter.setDirty(true);

                    getModel().getSelectedExtraction().replaceFilter(filter);
                }

            }

            recomputeRowsValidState();
            getModel().firePropertyChanged(ExtractionsFiltersUIModel.PROPERTY_VALID, null, getModel().isValid());
            getTable().repaint();

        });

    }

    private List<FilterTypeDTO> getFilterTypes() {
        if (getModel().getFilterTypes() == null) {
            getModel().setFilterTypes(getContext().getExtractionService().getFilterTypes());
        }
        return getModel().getFilterTypes();
    }

    private class FilterLoader extends SwingWorker<Object, Object> {

        final List<ExtractionsFiltersRowModel> rows = Lists.newArrayList();

        @Override
        protected Object doInBackground() {
            Map<Integer, FilterDTO> filtersByFilterTypeId = null;
            if (getModel().getSelectedExtraction() != null) {
                // load filters
                getContext().getExtractionService().loadFilteredElements(getModel().getSelectedExtraction());
                // split by filter type id
                filtersByFilterTypeId = ReefDbBeans.mapByProperty(getModel().getSelectedExtraction().getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID);
            }

            for (FilterTypeDTO filterType : getFilterTypes()) {
                ExtractionsFiltersRowModel rowModel = getTableModel().createNewRow();
                rowModel.setValid(true);
                rowModel.setFilterType(filterType);

                if (filtersByFilterTypeId != null) {
                    rowModel.setFilter(filtersByFilterTypeId.get(filterType.getId()));
                }

                rows.add(rowModel);
            }

            return null;
        }

        @Override
        protected void done() {
            try {
                if (isCancelled()) {
                    return;
                }
                try {
                    get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new ReefDbTechnicalException(e.getMessage(), e);
                }

                getModel().setLoading(true);
                getTable().setVisibleRowCount(rows.size());
                getModel().setRows(rows);
                recomputeRowsValidState();
                getModel().firePropertyChanged(ExtractionsFiltersUIModel.PROPERTY_VALID, null, getModel().isValid());

            } finally {
                getModel().setLoading(false);
            }
        }
    }
}
