package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIHandler;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.ungrouped.SurveyMeasurementsUngroupedTableUIModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Modele pour l onglet observation mesures.
 */
public class SurveyMeasurementsTabUIModel extends AbstractEmptyUIModel<SurveyMeasurementsTabUIModel> implements TabContentModel {

    /** Constant <code>PROPERTY_OBSERVATION_MODEL="observationModel"</code> */
    public static final String PROPERTY_OBSERVATION_MODEL = "observationModel";
    /** Constant <code>PROPERTY_TAXON_GROUP="taxonGroup"</code> */
    public static final String PROPERTY_TAXON_GROUP = "taxonGroup";
    /** Constant <code>PROPERTY_TAXON="taxon"</code> */
    public static final String PROPERTY_TAXON = "taxon";
    private ObservationUIModel observationModel;
    private ObservationUIHandler observationHandler;
    private TaxonGroupDTO taxonGroup;
    private TaxonDTO taxon;
    private boolean adjusting;

    private SurveyMeasurementsUngroupedTableUIModel ungroupedTableUIModel;

    private SurveyMeasurementsGroupedTableUIModel groupedTableUIModel;

    /** {@inheritDoc} */
    @Override
    public void setModify(boolean modify) {

        if (!modify) {
            ungroupedTableUIModel.setModify(false);
            groupedTableUIModel.setModify(false);
        }

        super.setModify(modify);
    }

    /**
     * <p>Getter for the field <code>observationModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public ObservationUIModel getObservationModel() {
        return observationModel;
    }

    /**
     * <p>Setter for the field <code>observationModel</code>.</p>
     *
     * @param observationModel a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public void setObservationModel(ObservationUIModel observationModel) {
        this.observationModel = observationModel;
        firePropertyChange(PROPERTY_OBSERVATION_MODEL, null, observationModel);
    }

    public ObservationUIHandler getObservationUIHandler() {
        return observationHandler;
    }

    public void setObservationHandler(ObservationUIHandler observationHandler) {
        this.observationHandler = observationHandler;
        groupedTableUIModel.setObservationHandler(observationHandler);
    }

    /**
     * <p>Getter for the field <code>taxonGroup</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public TaxonGroupDTO getTaxonGroup() {
        return taxonGroup;
    }

    /**
     * <p>Setter for the field <code>taxonGroup</code>.</p>
     *
     * @param taxonGroup a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public void setTaxonGroup(TaxonGroupDTO taxonGroup) {
        TaxonGroupDTO oldValue = getTaxonGroup();
        this.taxonGroup = taxonGroup;
        firePropertyChange(PROPERTY_TAXON_GROUP, oldValue, taxonGroup);
    }

    /**
     * <p>Getter for the field <code>taxon</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public TaxonDTO getTaxon() {
        return taxon;
    }

    /**
     * <p>Setter for the field <code>taxon</code>.</p>
     *
     * @param taxon a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public void setTaxon(TaxonDTO taxon) {
        TaxonDTO oldValue = getTaxon();
        this.taxon = taxon;
        firePropertyChange(PROPERTY_TAXON, oldValue, taxon);
    }

    /**
     * <p>isAdjusting.</p>
     *
     * @return a boolean.
     */
    public boolean isAdjusting() {
        return adjusting;
    }

    /**
     * <p>Setter for the field <code>adjusting</code>.</p>
     *
     * @param adjusting a boolean.
     */
    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    /**
     * <p>Getter for the field <code>ungroupedTableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.ungrouped.SurveyMeasurementsUngroupedTableUIModel} object.
     */
    public SurveyMeasurementsUngroupedTableUIModel getUngroupedTableUIModel() {
        return ungroupedTableUIModel;
    }

    /**
     * <p>Setter for the field <code>ungroupedTableUIModel</code>.</p>
     *
     * @param ungroupedTableUIModel a {@link fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.ungrouped.SurveyMeasurementsUngroupedTableUIModel} object.
     */
    public void setUngroupedTableUIModel(SurveyMeasurementsUngroupedTableUIModel ungroupedTableUIModel) {
        this.ungroupedTableUIModel = ungroupedTableUIModel;
    }

    /**
     * <p>Getter for the field <code>groupedTableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableUIModel} object.
     */
    public SurveyMeasurementsGroupedTableUIModel getGroupedTableUIModel() {
        return groupedTableUIModel;
    }

    /**
     * <p>Setter for the field <code>groupedTableUIModel</code>.</p>
     *
     * @param groupedTableUIModel a {@link fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableUIModel} object.
     */
    public void setGroupedTableUIModel(SurveyMeasurementsGroupedTableUIModel groupedTableUIModel) {
        this.groupedTableUIModel = groupedTableUIModel;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEmpty() {
        return false;
//        return (ungroupedTableUIModel == null || ungroupedTableUIModel.getRowCount() == 0)
//                && (groupedTableUIModel == null || groupedTableUIModel.getRowCount() == 0);
    }

    /** {@inheritDoc} */
    @Override
    public String getTitle() {
        return n("reefdb.survey.measurement.title");
    }

    /** {@inheritDoc} */
    @Override
    public String getIcon() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCloseable() {
        return false;
    }

}
