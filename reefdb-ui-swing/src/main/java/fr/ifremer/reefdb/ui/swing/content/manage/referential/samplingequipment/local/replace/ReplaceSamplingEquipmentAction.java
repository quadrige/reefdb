package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractReplaceAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.ManageSamplingEquipmentsUI;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 */
public class ReplaceSamplingEquipmentAction extends AbstractReplaceAction<SamplingEquipmentDTO, ReplaceSamplingEquipmentUIModel, ReplaceSamplingEquipmentUI, ReplaceSamplingEquipmentUIHandler> {

    /**
     * <p>Constructor for ReplaceSamplingEquipmentAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local.replace.ReplaceSamplingEquipmentUIHandler} object.
     */
    public ReplaceSamplingEquipmentAction(ReplaceSamplingEquipmentUIHandler handler) {
        super(handler);
    }

    /** {@inheritDoc} */
    @Override
    protected String getReferentialLabel() {
        return t("reefdb.property.samplingEquipment");
    }

    /** {@inheritDoc} */
    @Override
    protected boolean prepareReplaceReferential(ReferentialService service, SamplingEquipmentDTO source, SamplingEquipmentDTO target) {

        if (service.isSamplingEquipmentUsedInValidatedData(source.getId())) {
            String decoratedSource = decorate(source);
            if (getContext().getDialogHelper().showConfirmDialog(getReplaceUI(), t("reefdb.replaceReferential.used.validatedData.message", decoratedSource, decoratedSource),
                    getReplaceUI().getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return false;
            }
            setDelete(false);
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    protected void replaceReferential(ReferentialService service, SamplingEquipmentDTO source, SamplingEquipmentDTO target, boolean delete) {

        service.replaceSamplingEquipment(source, target, delete);
    }

    /** {@inheritDoc} */
    @Override
    protected void resetCaches() {
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        final ManageSamplingEquipmentsUI ui = (ManageSamplingEquipmentsUI) getContext().getMainUI().getHandler().getCurrentBody();
        // reload table
        if (ui != null) {
            SwingUtilities.invokeLater(() -> getActionEngine().runAction(ui.getSamplingEquipmentsLocalUI().getSamplingEquipmentsLocalMenuUI().getSearchButton()));
        }

    }
}
