package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.AlphanumericComparator;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.initGrid.InitGridUI;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.initGrid.InitGridUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.multiedit.OperationMeasurementsMultiEditUI;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import jaxx.runtime.SwingUtil;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Controleur pour les mesures des prelevements (ecran prelevements/mesure).
 */
public class OperationMeasurementsGroupedTableUIHandler
    extends AbstractMeasurementsGroupedTableUIHandler<OperationMeasurementsGroupedRowModel, OperationMeasurementsGroupedTableUIModel, OperationMeasurementsGroupedTableUI> {

    // editor for sampling operations column
    private ExtendedComboBoxCellEditor<SamplingOperationDTO> samplingOperationCellEditor;
    // initial border of full screen toggle button
    private Border initialToggleButtonBorder;

    /**
     * <p>Constructor for OperationMeasurementsGroupedTableUIHandler.</p>
     */
    public OperationMeasurementsGroupedTableUIHandler() {
        super(OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION,
            OperationMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP,
            OperationMeasurementsGroupedRowModel.PROPERTY_TAXON,
            OperationMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS,
            OperationMeasurementsGroupedRowModel.PROPERTY_COMMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeasurementsGroupedTableModel<OperationMeasurementsGroupedRowModel> getTableModel() {
        return (OperationMeasurementsGroupedTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getOperationGroupedMeasurementTable();
    }

    @Override
    protected OperationMeasurementsGroupedTableUIModel createNewModel() {
        return new OperationMeasurementsGroupedTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final OperationMeasurementsGroupedTableUI ui) {

        super.afterInit(ui);

        // Les boutons sont desactives
        getUI().getDeleteButton().setEnabled(false);
        getUI().getDuplicateButton().setEnabled(false);
        SwingUtil.setLayerUI(ui.getTableauBasScrollPane(), ui.getTableBlockLayer());

        // Keep initial border
        initialToggleButtonBorder = getUI().getFullScreenToggleButton().getBorder();

    }

    @Override
    public void toggleFullScreen(JPanel panel, JToggleButton toggleButton) {
        super.toggleFullScreen(panel, toggleButton);

        // Set highlight border when selected (Mantis #48592)
        toggleButton.setBorder(
            toggleButton.isSelected()
                ? BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), initialToggleButtonBorder)
                : initialToggleButtonBorder
        );

    }

    /**
     * Initialiser les listeners
     */
    protected void initListeners() {
        super.initListeners();

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {
                case OperationMeasurementsGroupedTableUIModel.PROPERTY_SURVEY:

                    // update sampling operation cell combo box
                    samplingOperationCellEditor.getCombo().setData(getModel().getSamplingOperations());
                    break;

                case AbstractReefDbTableUIModel.PROPERTY_ROWS:

                    // update some controls
                    getUI().processDataBinding(OperationMeasurementsGroupedTableUI.BINDING_INIT_DATA_GRID_BUTTON_ENABLED);
                    break;

                // TODO ? force sort on sampling operation and transition column
//                case OperationMeasurementsGroupedTableUIModel.PROPERTY_PIT_TRANSITION_LENGTH_PMFM_ID:
//
//                    // Try to apply sort on sampling operation and transition column
//                    if (getModel().getPitTransitionLengthPmfmId() != null) {
//                        TableColumnExt samplingColumn = getTable().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING);
//                        PmfmTableColumn transitionColumn = findPmfmColumnByPmfmId(getModel().getPmfmColumns(), getModel().getPitTransitionLengthPmfmId());
//                        if (samplingColumn != null && transitionColumn != null) {
//                            getTable().getRowSorter().setSortKeys(ImmutableList.of(
//                                new RowSorter.SortKey(samplingColumn.getModelIndex(), SortOrder.ASCENDING),
//                                new RowSorter.SortKey(transitionColumn.getModelIndex(), SortOrder.ASCENDING)
//                            ));
//                        }
//                    }
//                    break;
            }

        });

    }

    @Override
    protected void filterMeasurements() {

        // sampling operation column should not be editable if only one sampling operation
        getTable().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING).setEditable(getModel().getSamplingFilter() == null);
        getTable().getColumnExt(OperationMeasurementsGroupedTableModel.TAXON_GROUP).setEditable(getModel().getTaxonGroupFilter() == null);
        getTable().getColumnExt(OperationMeasurementsGroupedTableModel.TAXON).setEditable(getModel().getTaxonFilter() == null);

        if (getModel().getSamplingFilter() == null && getModel().getTaxonGroupFilter() == null && getModel().getTaxonFilter() == null) {
            // remove filter
            getTable().setRowFilter(null);
        } else {
            // add filter
            getTable().setRowFilter(new RowFilter<OperationMeasurementsGroupedTableModel, Integer>() {
                @Override
                public boolean include(Entry<? extends OperationMeasurementsGroupedTableModel, ? extends Integer> entry) {
                    return (getModel().getSamplingFilter() == null
                        || getModel().getSamplingFilter().equals(entry.getValue(getTable().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING).getModelIndex())))
                        && (getModel().getTaxonGroupFilter() == null
                        || getModel().getTaxonGroupFilter().equals(entry.getValue(getTable().getColumnExt(OperationMeasurementsGroupedTableModel.TAXON_GROUP).getModelIndex())))
                        && (getModel().getTaxonFilter() == null
                        || getModel().getTaxonFilter().equals(entry.getValue(getTable().getColumnExt(OperationMeasurementsGroupedTableModel.TAXON).getModelIndex())));
                }
            });
        }
    }

    @Override
    protected List<? extends MeasurementAware> getMeasurementAwareModels() {
        return getModel().getSamplingOperations().stream()
            // sort by name
            .sorted(Comparator.comparing(SamplingOperationDTO::getName))
            .collect(Collectors.toList());
    }

    @Override
    protected OperationMeasurementsGroupedRowModel createNewRow(boolean readOnly, MeasurementAware parentBean) {
        OperationMeasurementsGroupedRowModel row = new OperationMeasurementsGroupedRowModel(readOnly);
        row.setSamplingOperation((SamplingOperationDTO) parentBean);
        return row;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, OperationMeasurementsGroupedRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // when sampling operation changes
        if (OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION.equals(propertyName)) {

            // remove measurement from old sampling
            if (oldValue != null) {
                SamplingOperationDTO oldSamplingOperation = (SamplingOperationDTO) oldValue;
                resetIndividualMeasurementIds(row);
                setDirty(oldSamplingOperation);
            }

            // recalculate individual id
            if (newValue != null) {

                // affect new sampling operation now
                SamplingOperationDTO newSamplingOperation = (SamplingOperationDTO) newValue;
                row.setSamplingOperation(newSamplingOperation);
                setDirty(newSamplingOperation);
            }
        }

        // fire modify event at the end
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
    }

    @Override
    protected MeasurementAware getMeasurementAwareModelForRow(OperationMeasurementsGroupedRowModel row) {
        return row.getSamplingOperation();
    }

    @Override
    protected void updateMeasurementFromRow(MeasurementDTO measurement, OperationMeasurementsGroupedRowModel row) {
        measurement.setSamplingOperation(row.getSamplingOperation());
        super.updateMeasurementFromRow(measurement, row);
    }

    /**
     * Initialisation du tableau.
     */
    @Override
    protected void initTable() {

        // Colonne mnemonique
        samplingOperationCellEditor = newExtendedComboBoxCellEditor(new ArrayList<>(), SamplingOperationDTO.class, false);
        final TableColumnExt colonneMnemonique = addColumn(
            samplingOperationCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.SAMPLING),
            OperationMeasurementsGroupedTableModel.SAMPLING);
        colonneMnemonique.setCellEditor(samplingOperationCellEditor);
        colonneMnemonique.setSortable(true);
        setDefaultColumnMinWidth(colonneMnemonique);

        if (getConfig().isDebugMode()) {
            TableColumnExt indivIdCol = addColumn((ColumnIdentifier<OperationMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.INDIVIDUAL_ID);
            indivIdCol.setSortable(true);
            setDefaultColumnMinWidth(indivIdCol);
        }

        // Colonne groupe taxon
        final TableColumnExt colTaxonGroup = addColumn(
            taxonGroupCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON_GROUP),
            OperationMeasurementsGroupedTableModel.TAXON_GROUP);
        colTaxonGroup.setSortable(true);
        setDefaultColumnMinWidth(colTaxonGroup);

        // Colonne taxon
        final TableColumnExt colTaxon = addColumn(
            taxonCellEditor,
            newTableCellRender(OperationMeasurementsGroupedTableModel.TAXON),
            OperationMeasurementsGroupedTableModel.TAXON);
        colTaxon.setSortable(true);
        setDefaultColumnMinWidth(colTaxon);

        // Colonne taxon saisi
        final TableColumnExt colInputTaxon = addColumn(OperationMeasurementsGroupedTableModel.INPUT_TAXON_NAME);
        colInputTaxon.setSortable(true);
        colInputTaxon.setEditable(false);
        setDefaultColumnMinWidth(colInputTaxon);

        // Colonne analyste
        final TableColumnExt colAnalyst = addColumn(
            departmentCellEditor,
            newTableCellRender(AbstractMeasurementsGroupedTableModel.ANALYST),
            (ColumnIdentifier<OperationMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.ANALYST);
        colAnalyst.setSortable(true);
        setDefaultColumnMinWidth(colAnalyst);

        // Colonne commentaire
        final TableColumnExt colComment = addCommentColumn(OperationMeasurementsGroupedTableModel.COMMENT);

        // Modele de la table
        final OperationMeasurementsGroupedTableModel tableModel = new OperationMeasurementsGroupedTableModel(getTable().getColumnModel(), true);
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable());

        colComment.setVisible(false);
        colAnalyst.setVisible(false);
        colInputTaxon.setVisible(false);

        // border
        addEditionPanelBorder();
    }

    @Override
    protected void installSortController() {
        super.installSortController();

        getSortController().setComparator(
            getTable().getColumnModel().getColumnExt(OperationMeasurementsGroupedTableModel.SAMPLING).getModelIndex(),
            new SamplingOperationComparator()
        );
    }

    @Override
    protected void initAddedRow(OperationMeasurementsGroupedRowModel row) {
        if (getModel().getSamplingFilter() != null) {
            row.setSamplingOperation(getModel().getSamplingFilter());
        }
        super.initAddedRow(row);
    }

    @Override
    protected boolean isMeasurementNotEmpty(MeasurementDTO measurement) {
        return !measurement.getPmfm().getId().equals(getModel().getPitTransitionLengthPmfmId()) && super.isMeasurementNotEmpty(measurement);
    }

    @Override
    protected void setDirty(MeasurementAware bean) {
        super.setDirty(bean);
        if (bean instanceof SamplingOperationDTO)
            getModel().firePropertyChanged(OperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, null, bean);
    }

    /**
     * Initialize the data grid (see Mantis #34695)
     */
    void initializeDataGrid() {

        InitGridUI initGridUI = new InitGridUI(getUI());
        InitGridUIModel initModel = initGridUI.getModel();
        initModel.setSamplingOperations(getModel().getSamplingOperations());

        // set last sampling operation which has been input (use the last not sorted line of the model)
        if (getModel().getRowCount() > 0) {
            initModel.setLastSamplingOperation(getModel().getRows().get(getModel().getRowCount() - 1).getSamplingOperation());
        }

        if (getModel().getPitTransectOriginPmfmId() != null) {
            initModel.setOriginPmfmId(getModel().getPitTransectOriginPmfmId());
            PmfmDTO pmfm = getContext().getReferentialService().getPmfm(getModel().getPitTransectOriginPmfmId());
            initModel.setOriginUnit(pmfm != null ? pmfm.getUnit() : null);
        }
        if (getModel().getPitTransectLengthPmfmId() != null) {
            initModel.setLengthPmfmId(getModel().getPitTransectLengthPmfmId());
            PmfmDTO pmfm = getContext().getReferentialService().getPmfm(getModel().getPitTransectLengthPmfmId());
            initModel.setLengthUnit(pmfm != null ? pmfm.getUnit() : null);
        }
        initModel.setTransitionPmfmId(getModel().getPitTransitionLengthPmfmId());
        initModel.setTransitionUnit(getContext().getReferentialService().getPmfm(getModel().getPitTransitionLengthPmfmId()).getUnit());
        openDialog(initGridUI);

        if (initModel.isValid()) {
            // init the grid and values
            List<OperationMeasurementsGroupedRowModel> rows = Lists.newArrayList();

            for (SamplingOperationDTO samplingOperation : initModel.getResultMap().keySet()) {

                for (Integer transition : initModel.getResultMap().get(samplingOperation)) {

                    // Initialize the row
                    OperationMeasurementsGroupedRowModel row = createNewRow(false, samplingOperation);
                    // Affect individual pmfms from parent model
                    row.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));
                    // Create empty measurements
                    ReefDbBeans.createEmptyMeasurements(row);
                    // Get the measurement
                    MeasurementDTO measurement = ReefDbBeans.getIndividualMeasurementByPmfmId(row, initModel.getTransitionPmfmId());
                    if (measurement == null) {
                        throw new ReefDbTechnicalException("No transition measurement found");
                    }
                    // Set its value
                    measurement.setNumericalValue(BigDecimal.valueOf(transition));
                    row.setValid(true);
                    rows.add(row);
                }
                setDirty(samplingOperation);
            }

            resetCellEditors();
            getModel().addRows(rows);
            getModel().setModify(true);
        }
    }

    @Override
    protected boolean isRowToSave(OperationMeasurementsGroupedRowModel row) {
        boolean toSave = super.isRowToSave(row);

        if (toSave && getModel().getPitTransitionLengthPmfmId() != null) {
            // If this row has been initialized but nothing else has been set, it can be ignored
            Set<Integer> pmfmIds = ReefDbBeans.getPmfmIdsOfNonEmptyIndividualMeasurements(row);
            pmfmIds.remove(getModel().getPitTransitionLengthPmfmId());
            toSave = !pmfmIds.isEmpty() || row.hasTaxonInformation();
        }

        return toSave;
    }

    public void editSelectedMeasurements() {

        OperationMeasurementsMultiEditUI editUI = new OperationMeasurementsMultiEditUI(getUI());

        // add the calculated transition pmfm as read-only
        editUI.getModel().setReadOnlyPmfmIds(
            Stream.of(
                getModel().getPitTransitionLengthPmfmId(),
                getModel().getPitTransectLengthPmfmId(),
                getModel().getPitTransectOriginPmfmId(),
                getModel().getCalculatedLengthEndPositionPmfmId(),
                getModel().getCalculatedLengthTransitionPmfmId()
            ).filter(Objects::nonNull).collect(Collectors.toList())
        );

        editSelectedMeasurements(editUI);

    }

    static class SamplingOperationComparator implements Comparator<SamplingOperationDTO> {

        @Override
        public int compare(SamplingOperationDTO o1, SamplingOperationDTO o2) {
            if (o1 == o2) return 0;
            if (o1 == null) return -1;
            if (o2 == null) return 1;
            return AlphanumericComparator.instance().compare(o1.getName(), o2.getName());
        }
    }
}
