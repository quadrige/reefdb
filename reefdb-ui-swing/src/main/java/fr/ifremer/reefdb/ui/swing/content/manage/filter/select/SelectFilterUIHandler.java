package fr.ifremer.reefdb.ui.swing.content.manage.filter.select;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.campaign.element.FilterElementCampaignUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.department.element.FilterElementDepartmentUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.FilterElementUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.location.element.FilterElementLocationUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.pmfm.element.FilterElementPmfmUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.program.element.FilterElementProgramUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.taxon.element.FilterElementTaxonUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.taxongroup.element.FilterElementTaxonGroupUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

/**
 * Handler.
 */
public class SelectFilterUIHandler extends AbstractReefDbUIHandler<SelectFilterUIModel, SelectFilterUI> implements Cancelable {

    private FilterElementUI filterElementUI = null;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(SelectFilterUI ui) {
        super.beforeInit(ui);

        SelectFilterUIModel model = new SelectFilterUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(final SelectFilterUI ui) {
        initUI(ui);

        ExtractionFilterValues extractionFilter = ExtractionFilterValues.getExtractionFilter(ui.filterTypeId);
        Assert.notNull(extractionFilter);

        // load correct filterElementUI
        switch (extractionFilter) {
            case PROGRAM:
                filterElementUI = new FilterElementProgramUI(ui);
                break;
            case CAMPAIGN:
                filterElementUI = new FilterElementCampaignUI(ui);
                break;
            case LOCATION:
                filterElementUI = new FilterElementLocationUI(ui);
                break;
            case TAXON:
                filterElementUI = new FilterElementTaxonUI(ui);
                break;
            case TAXON_GROUP:
                filterElementUI = new FilterElementTaxonGroupUI(ui);
                break;
            case DEPARTMENT:
                filterElementUI = new FilterElementDepartmentUI(ui);
                break;
            case PMFM:
                filterElementUI = new FilterElementPmfmUI(ui);
                break;
        }

        Assert.notNull(filterElementUI);

        ui.getFilterElementPanel().add(filterElementUI);
        ui.get$objectMap().put("filterElementUI", filterElementUI);

        getFilterElementUIHandler().enable();

        // Set default status
        setDefaultStatus(
            getContext().getReferentialService().getStatus(StatusFilter.NATIONAL_ACTIVE).get(0) // fixme should be LOCAL_ACTIVE if context is local ?
        );

        // apply context filter if selected
        if (getContext().getSelectedContext() != null && !getContext().getSelectedContext().isFiltersEmpty()) {
            FilterDTO filter = ReefDbBeans.findByProperty(getContext().getSelectedContext().getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID, extractionFilter.getFilterTypeId());
            if (filter != null) {
                ApplyFilterUI applyFilterUI = getFilterElementUIHandler().getReferentialMenuUI().getHandler().getApplyFilterUI();
                applyFilterUI.getModel().setFilter(filter);
                applyFilterUI.getApplyButton().doClick();
            }
        }

        getModel().addPropertyChangeListener(SelectFilterUIModel.PROPERTY_SELECTED_ELEMENTS, evt -> ((AbstractFilterElementUIHandler) filterElementUI.getHandler()).loadSelectedElements(getModel().getSelectedElements()));
    }

    public AbstractFilterElementUIHandler getFilterElementUIHandler() {
        Assert.notNull(filterElementUI);
        return (AbstractFilterElementUIHandler) filterElementUI.getHandler();
    }

    public void setDefaultStatus(StatusDTO status) {
        AbstractFilterElementUIHandler filterElementUIHandler = ((AbstractFilterElementUIHandler) filterElementUI.getHandler());
        filterElementUIHandler.getReferentialMenuUI().getModel().setStatus(status);
    }

    /**
     * <p>valid.</p>
     */
    @SuppressWarnings("unchecked")
    public void valid() {

        getModel().setSelectedElements(filterElementUI.getModel().getElements());
        getModel().setValid(true);

        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {

        getModel().setValid(false);

        closeDialog();
    }
}
