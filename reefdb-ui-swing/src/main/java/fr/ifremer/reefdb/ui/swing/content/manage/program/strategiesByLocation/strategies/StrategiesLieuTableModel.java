package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.strategies;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class StrategiesLieuTableModel extends AbstractReefDbTableModel<StrategiesLieuTableRowModel> {

    /**
     * Identifiant pour la colonne local.
     */
    public static final ReefDbColumnIdentifier<StrategiesLieuTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
    		StrategiesLieuTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.local"),
            n("reefdb.program.strategies.strategy.local.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne programme.
     */
    public static final ReefDbColumnIdentifier<StrategiesLieuTableRowModel> PROGRAM = ReefDbColumnIdentifier.newId(
    		StrategiesLieuTableRowModel.PROPERTY_PROGRAM,
            n("reefdb.property.program"),
            n("reefdb.program.strategies.strategy.program.tip"),
            ProgramDTO.class);

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final ReefDbColumnIdentifier<StrategiesLieuTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		StrategiesLieuTableRowModel.PROPERTY_NAME,
            n("reefdb.program.strategies.strategy.name.short"),
            n("reefdb.program.strategies.strategy.name.tip"),
            String.class);

	/**
	 * Identifiant pour la colonne date debut.
	 */
    public static final ReefDbColumnIdentifier<StrategiesLieuTableRowModel> START_DATE = ReefDbColumnIdentifier.newId(
    		StrategiesLieuTableRowModel.PROPERTY_START_DATE,
            n("reefdb.program.strategies.strategy.startDate.short"),
            n("reefdb.program.strategies.strategy.startDate.tip"),
			LocalDate.class);

	/**
	 * Identifiant pour la colonne date fin.
	 */
    public static final ReefDbColumnIdentifier<StrategiesLieuTableRowModel> END_DATE = ReefDbColumnIdentifier.newId(
    		StrategiesLieuTableRowModel.PROPERTY_END_DATE,
            n("reefdb.program.strategies.strategy.endDate.short"),
            n("reefdb.program.strategies.strategy.endDate.tip"),
			LocalDate.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public StrategiesLieuTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public StrategiesLieuTableRowModel createNewRow() {
		return new StrategiesLieuTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<StrategiesLieuTableRowModel> getFirstColumnEditing() {
		return PROGRAM;
	}
}
