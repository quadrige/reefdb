package fr.ifremer.reefdb.ui.swing.content.manage.filter.select;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;

import java.util.List;

/**
 * Created by Ludovic on 19/05/2015.
 */
public class SelectFilterUIModel extends AbstractEmptyUIModel<SelectFilterUIModel> {

    private List<? extends QuadrigeBean> selectedElements;
    /** Constant <code>PROPERTY_SELECTED_ELEMENTS="selectedElements"</code> */
    public static final String PROPERTY_SELECTED_ELEMENTS = "selectedElements";

    /**
     * <p>Getter for the field <code>selectedElements</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<? extends QuadrigeBean> getSelectedElements() {
        return selectedElements;
    }

    /**
     * <p>Setter for the field <code>selectedElements</code>.</p>
     *
     * @param selectedElements a {@link java.util.List} object.
     */
    public void setSelectedElements(List<? extends QuadrigeBean> selectedElements) {
        this.selectedElements = selectedElements;
        firePropertyChange(PROPERTY_SELECTED_ELEMENTS, null, selectedElements);
    }
}
