package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.ManageDepartmentsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local.replace.ReplaceDepartmentUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local.replace.ReplaceDepartmentUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceDepartmentAction extends AbstractReefDbAction<ManageDepartmentsLocalUIModel, ManageDepartmentsLocalUI, ManageDepartmentsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceDepartmentAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local.ManageDepartmentsLocalUIHandler} object.
     */
    public OpenReplaceDepartmentAction(ManageDepartmentsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<DepartmentDTO, ReplaceDepartmentUIModel, ReplaceDepartmentUI> openAction =
                new AbstractOpenReplaceUIAction<DepartmentDTO, ReplaceDepartmentUIModel, ReplaceDepartmentUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.department");
                    }

                    @Override
                    protected ReplaceDepartmentUIModel createNewModel() {
                        return new ReplaceDepartmentUIModel();
                    }

                    @Override
                    protected ReplaceDepartmentUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceDepartmentUI(ctx);
                    }

                    @Override
                    protected List<DepartmentDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getDepartments(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected DepartmentDTO getSelectedSource() {
                        List<DepartmentDTO> selectedBeans = OpenReplaceDepartmentAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected DepartmentDTO getSelectedTarget() {
                        ManageDepartmentsUI ui = OpenReplaceDepartmentAction.this.getUI().getParentContainer(ManageDepartmentsUI.class);
                        List<DepartmentDTO> selectedBeans = ui.getManageDepartmentsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
