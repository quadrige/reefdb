package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table.UserTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de delete un user local
 */
public class DeleteUserAction extends AbstractReefDbAction<UserTableUIModel, ManageUsersLocalUI, ManageUsersLocalUIHandler> {

    Set<? extends PersonDTO> toDelete;
    boolean deleteOk = false;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteUserAction(final ManageUsersLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        toDelete = getModel().getSelectedRows();

        return super.prepareAction()
                && CollectionUtils.isNotEmpty(toDelete)
                && askBeforeDelete(t("reefdb.action.delete.user.title"), t("reefdb.action.delete.user.message"));
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        List<String> used = Lists.newArrayList();

        // check usage in data
        for (PersonDTO person : toDelete) {
            if (person.getId() != null) {
                if (getContext().getUserService().isUserUsedInData(person.getId())) {
                    used.add(decorate(person));
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.data.message"));
            return;
        }

        // check usage in rules
        for (PersonDTO person : toDelete) {
            if (person.getId() != null) {
                if (getContext().getUserService().isUserUsedInRules(person.getId())) {
                    used.add(decorate(person));
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.rule.message"));
            return;
        }

        // check usage in program/strategy
        for (PersonDTO person : toDelete) {
            if (person.getId() != null) {
                if (getContext().getUserService().isUserUsedInProgram(person.getId())) {
                    used.add(decorate(person));
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.strategy.message"));
            return;
        }

        // delete
        getContext().getUserService().deleteUsers(ReefDbBeans.collectIds(toDelete));
        deleteOk = true;
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        if (deleteOk) {
            getModel().deleteSelectedRows();
        }

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        deleteOk = false;
        super.releaseAction();
    }

}
