package fr.ifremer.reefdb.ui.swing.content.observation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.action.GoToObservationAction;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import fr.ifremer.reefdb.ui.swing.util.tab.AbstractReefDbTabContainerUIHandler;
import fr.ifremer.reefdb.ui.swing.util.tab.ReefDbTabIndexes;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker;
import java.awt.Color;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour observation.
 */
public class ObservationUIHandler extends AbstractReefDbTabContainerUIHandler<ObservationUIModel, ObservationUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ObservationUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ObservationUI ui) {
        super.beforeInit(ui);

        // Create model and register to the JAXX context
        final ObservationUIModel model = new ObservationUIModel();
        ui.setContextValue(model);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ObservationUI ui) {

        initUI(ui);
        SwingUtil.setLayerUI(getTabPanel(), ui.getSurveyBlockLayer());

        // Affect models
        getModel().setSurveyDetailsTabUIModel(getUI().getSurveyDetailsTabUI().getModel());
        getModel().setSurveyMeasurementsTabUIModel(getUI().getSurveyMeasurementsTabUI().getModel());
        getModel().setOperationMeasurementsTabUIModel(getUI().getOperationMeasurementsTabUI().getModel());
        getModel().setPhotosTabUIModel(getUI().getPhotosTabUI().getModel());

        // Add main handler to other model (Mantis #48309)
        getModel().getSurveyMeasurementsTabUIModel().setObservationHandler(this);
        getModel().getOperationMeasurementsTabUIModel().setObservationHandler(this);

        // Ajout des onglets
        setCustomTab(ReefDbTabIndexes.ONGLET_OBSERVATION_GENERAL, getModel().getSurveyDetailsTabUIModel());
        setCustomTab(ReefDbTabIndexes.ONGLET_OBSERVATION_MESURES, getModel().getSurveyMeasurementsTabUIModel());
        setCustomTab(ReefDbTabIndexes.ONGLET_PRELEVEMENTS_MESURES, getModel().getOperationMeasurementsTabUIModel());
        setCustomTab(ReefDbTabIndexes.ONGLET_PHOTOS, getModel().getPhotosTabUIModel());

        // set label colors
        ReefDbUIs.setChildrenLabelForeground(ui, getConfig().getColorThematicLabel());
        ui.getObservationDescriptionLabel().setForeground(Color.BLACK);

        // Selection de l onglet actif
        getTabPanel().setSelectedIndex(getUI().getTabIndex());

        // Binding save button
        getUI().applyDataBinding(ObservationUI.BINDING_SAVE_BUTTON_ENABLED);

        // button border
        getUI().getNextButton().setBorder(
                BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), ui.getNextButton().getBorder())
        );

        // Listen modify
        listenModelModify(getModel().getSurveyDetailsTabUIModel());
        listenModelModify(getModel().getSurveyMeasurementsTabUIModel());
        listenModelModify(getModel().getOperationMeasurementsTabUIModel());
        listenModelModify(getModel().getPhotosTabUIModel());

        // Listen valid
        listenModelValid(getModel().getSurveyDetailsTabUIModel());
        listenModelValid(getModel().getSurveyMeasurementsTabUIModel());
        listenModelValid(getModel().getOperationMeasurementsTabUIModel());
        listenModelValid(getModel().getPhotosTabUIModel());

        // REgister validators
        registerValidators(
                getUI().getSurveyDetailsTabUI().getValidator(),
                getUI().getSurveyMeasurementsTabUI().getValidator(),
                getUI().getOperationMeasurementsTabUI().getValidator(),
                getUI().getPhotosTabUI().getValidator());

        // listener on properties that updates the title
        getModel().addPropertyChangeListener(evt -> {
            if (ObservationUIModel.PROPERTY_LOCATION.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_DATE.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_TIME.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_PROGRAM.equals(evt.getPropertyName())
                    || ObservationUIModel.PROPERTY_NAME.equals(evt.getPropertyName())
            ) {
                getUI().applyDataBinding(ObservationUI.BINDING_OBSERVATION_DESCRIPTION_LABEL_TEXT);
            }
        });

        // load model
        loadSurvey();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTitle() {
        String contextName;
        if (getContext().getSelectedContext() == null) {
            contextName = t("reefdb.main.title.noContext");
        } else {
            contextName = getContext().getSelectedContext().getName();
        }

        return String.format("%s [%s]", super.getTitle(), contextName);
    }

    private void loadSurvey() {

        getModel().setLoading(true);
        SurveyLoader loader = new SurveyLoader();
        loader.execute();

    }

    /**
     * Select next available tab
     */
    void selectNextTab() {

        int index = getTabPanel().getSelectedIndex() + 1;

        if (index >= getTabPanel().getTabCount()) {
            // if last tab is reached, return to HomeUI
            changeScreenAction(GoToObservationAction.class);
        }

        while (index < getTabPanel().getTabCount()) {
            if (getTabPanel().isEnabledAt(index)) {
                getTabPanel().setSelectedIndex(index);
                return;
            }
            index++;
        }
    }

    /**
     * <p>refreshModels.</p>
     */
    void refreshModels() {

        // force components to update enabled state if survey is editable or not
        getModel().getSurveyDetailsTabUIModel().setEditable(getModel().isEditable());

        // TODO add other models
        getModel().getSurveyMeasurementsTabUIModel().setObservationModel(getModel());
        getModel().getOperationMeasurementsTabUIModel().setObservationModel(getModel());

        getModel().getPhotosTabUIModel().setObservationModel(getModel());
    }

    public List<TaxonDTO> getAvailableTaxons(TaxonGroupDTO taxonGroup, boolean forceNoContext) {

        if (taxonGroup == null) {

            // if no taxon group specified, means that all available taxons will be returned

            // check first if the list is not already set or the context has changed
            if (CollectionUtils.isEmpty(getModel().getAllAvailableTaxons())
                    || getModel().getForceNoContextOnTaxons() == null
                    || getModel().getForceNoContextOnTaxons() != forceNoContext) {

                // the list is filled or the context have changed
                getModel().setForceNoContextOnTaxons(forceNoContext);
                getModel().setAllAvailableTaxons(
                        getContext().getObservationService().getAvailableTaxons(forceNoContext)
                );
            }
            // return existing list
            return getModel().getAllAvailableTaxons();

        } else {

            // call service directly if taxon group specified (use now as reference date (see Mantis #45148)
            return getContext().getObservationService().getAvailableTaxons(taxonGroup, forceNoContext);
        }
    }

    public List<TaxonGroupDTO> getAvailableTaxonGroups(TaxonDTO taxon, boolean forceNoContext) {

        if (taxon == null) {

            // if no taxon specified, means that all available taxon groups will be returned

            // check first if the list is not already set or the context has changed
            if (CollectionUtils.isEmpty(getModel().getAllAvailableTaxonGroups())
                    || getModel().getForceNoContextOnTaxonGroups() == null
                    || getModel().getForceNoContextOnTaxonGroups() != forceNoContext) {

                // the list is filled or the context have changed
                getModel().setForceNoContextOnTaxonGroups(forceNoContext);
                getModel().setAllAvailableTaxonGroups(
                        getContext().getObservationService().getAvailableTaxonGroups(forceNoContext)
                );
            }
            // return existing list
            return getModel().getAllAvailableTaxonGroups();

        } else {

            // call service directly if taxon group specified (use now as reference date (see Mantis #45148)
            return getContext().getObservationService().getAvailableTaxonGroups(taxon, forceNoContext);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JTabbedPane getTabPanel() {
        return getUI().getObservationTabbedPane();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeTab(int i) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private class SurveyLoader extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() {

            // Observation
            final SurveyDTO survey = getContext().getObservationService().getSurvey(getContext().getSelectedSurveyId());

            // Determine if current user is allowed to edit survey
            boolean userIsRecorder = getContext().getDataContext().isSurveyEditable(survey);

            // Prepare editable state for general tab to speed up some behaviours
            getModel().setEditable(userIsRecorder && !ReefDbBeans.isSurveyValidated(survey));
            getModel().getSurveyDetailsTabUIModel().setEditable(getModel().isEditable());

            // load observation into parent model (this)
            getModel().fromBean(survey);

            // load pmfm strategies
            getModel().setPmfmStrategies(getContext().getProgramStrategyService().getPmfmStrategiesBySurvey(survey));

            // load preconditioned rules
            getModel().setPreconditionedRules(getContext().getRuleListService().getPreconditionedControlRulesForSurvey(survey));

            // load grouped rules
            getModel().setGroupedRules(getContext().getRuleListService().getGroupedControlRulesForSurvey(survey));

            // load pmfms under moratorium
            getModel().setPmfmsUnderMoratorium(getContext().getObservationService().getPmfmsUnderMoratorium(survey));

            // affect other models
            refreshModels();

            return null;
        }

        @Override
        protected void done() {
            try {
                try {
                    get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new ReefDbTechnicalException(e.getMessage(), e);
                }

                getUI().applyDataBinding(ObservationUI.BINDING_OBSERVATION_DESCRIPTION_LABEL_TEXT);

                // Reset modify flag
                getModel().setModify(false);

            } finally {
                getModel().setLoading(false);
            }
        }
    }

}
