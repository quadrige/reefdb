package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.util.List;

/**
 * Controlleur du menu pour la gestion des analysisInstruments au niveau National
 */
public class AnalysisInstrumentsMenuUIHandler extends ReferentialMenuUIHandler<AnalysisInstrumentsMenuUIModel, AnalysisInstrumentsMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(AnalysisInstrumentsMenuUIHandler.class);
    
    /** {@inheritDoc} */
    @Override
    public void beforeInit(final AnalysisInstrumentsMenuUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        final AnalysisInstrumentsMenuUIModel model = new AnalysisInstrumentsMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final AnalysisInstrumentsMenuUI ui) {
        super.afterInit(ui);

        // listen to model changes on 'local' to adapt combo box content
        getModel().addPropertyChangeListener(AnalysisInstrumentsMenuUIModel.PROPERTY_LOCAL, evt -> {
            getUI().getNameCombo().setData(getContext().getReferentialService().getAnalysisInstruments(getModel().getStatusFilter()));
            getUI().getStatusCombo().setData(getContext().getReferentialService().getStatus(getModel().getStatusFilter()));
        });
        
        // Initialiser les combobox
        initComboBox();
    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getLocalCombo().setEnabled(enabled);
        getUI().getNameCombo().setEnabled(enabled);
        getUI().getStatusCombo().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllAnalysisInstrumentFilters();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /** {@inheritDoc} */
    @Override
    public JComponent getLocalFilterPanel() {
        return getUI().getLocalPanel();
    }

    /**
	 * Initialisation des combobox
	 */
	private void initComboBox() {

        // Combo local
		initBeanFilterableComboBox(
				getUI().getLocalCombo(), 
				getContext().getSystemService().getBooleanValues(),
				null);
        
		initBeanFilterableComboBox(
				getUI().getNameCombo(),
				getContext().getReferentialService().getAnalysisInstruments(getModel().getStatusFilter()),
				null);
		
		initBeanFilterableComboBox(getUI().getStatusCombo(), 
				getContext().getReferentialService().getStatus(getModel().getStatusFilter()),
				null);

        ReefDbUIs.forceComponentSize(getUI().getLocalCombo());
        ReefDbUIs.forceComponentSize(getUI().getNameCombo());
        ReefDbUIs.forceComponentSize(getUI().getStatusCombo());
	}

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getUI().getNameCombo().setData(getContext().getReferentialService().getAnalysisInstruments(getModel().getStatusFilter()));
    }
}
