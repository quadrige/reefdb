package fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.edit.EditPmfmDialogUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des PSFM.
 */
public class PmfmsTableUIHandler extends AbstractReefDbTableUIHandler<PmfmsTableRowModel, PmfmsTableUIModel, PmfmsTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PmfmsTableUIHandler.class);

    private ExtendedComboBoxCellEditor<DepartmentDTO> departmentCellEditor;

    /**
     * <p>Constructor for PmfmsTableUIHandler.</p>
     */
    public PmfmsTableUIHandler() {
        super(PmfmsTableRowModel.PROPERTY_ANALYSIS_DEPARTMENT,
                PmfmsTableRowModel.PROPERTY_SURVEY,
                PmfmsTableRowModel.PROPERTY_SAMPLING,
                PmfmsTableRowModel.PROPERTY_GROUPING,
                PmfmsTableRowModel.PROPERTY_UNIQUE);
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{PmfmsTableRowModel.PROPERTY_ERRORS};
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<PmfmsTableRowModel> getTableModel() {
        return (PmfmsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getPmfmsTable();
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final PmfmsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final PmfmsTableUIModel model = new PmfmsTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(PmfmsTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initTable();
        SwingUtil.setLayerUI(ui.getTableScrollPane(), ui.getTableBlockLayer());

        // Initialiser les listeners
        initListeners();

    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, PmfmsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        saveToStrategy();
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(final PmfmsTableRowModel row) {
        row.getErrors().clear();
        return (!row.isEditable() || super.isRowValid(row)) && isLevelValid(row);
    }

    private boolean isLevelValid(PmfmsTableRowModel row) {
        if (!row.isSurvey() && !row.isSampling()) {
            ReefDbBeans.addError(row, t("reefdb.program.pmfm.surveyAndSamplingOperation.null"), PmfmsTableRowModel.PROPERTY_SURVEY, PmfmsTableRowModel.PROPERTY_SAMPLING);
        }
        return row.getErrors().isEmpty();
    }

    /**
     * <p>load.</p>
     *
     * @param selectedProgram a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     * @param selectedStrategy a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel} object.
     */
    public void load(ProgramsTableRowModel selectedProgram, StrategiesTableRowModel selectedStrategy) {

        getModel().setEditable(selectedProgram.isEditable());

        // filter analyst department to national only if program is national
        departmentCellEditor.getCombo().setData(getContext().getReferentialService().getDepartments(
                !selectedProgram.isLocal() ? StatusFilter.NATIONAL_ACTIVE : StatusFilter.ACTIVE));

        getModel().setBeans(selectedStrategy.getPmfmStrategies());

        getModel().getRows().forEach(row -> row.setEditable(selectedProgram.isEditable()));

        recomputeRowsValidState();

        getModel().setLoading(false);
        getModel().setLoaded(true);
    }

    /**
     * Suppression de tous les PSFMs
     */
    public void clearTable() {

        // Suppression des psfms
        getModel().setBeans(null);

        getModel().setLoaded(false);
    }

    /**
     * Monter un psfm.
     */
    public void upPmfm() {

        if (getModel().getSelectedRows().size() != 1) {
            return;
        }

        // Le psfm selectionne
        final PmfmsTableRowModel psfmToUp = getModel().getSelectedRows().iterator().next();
        int rowModelIndex = getTableModel().getRowIndex(psfmToUp);

        if (rowModelIndex > 0) {

            getTableModel().moveUp(psfmToUp);

            int rowViewIndex = rowModelIndex - 1; //getTable().convertRowIndexToView(rowModelIndex);
            selectCell(rowViewIndex, null);

            saveToStrategy();
            getModel().setModify(true);
        }

    }

    /**
     * Descendre un psfm.
     */
    public void downPmfm() {

        if (getModel().getSelectedRows().size() != 1) {
            return;
        }

        // Le psfm selectionne
        final PmfmsTableRowModel pmfmToDown = getModel().getSelectedRows().iterator().next();
        int rowModelIndex = getTableModel().getRowIndex(pmfmToDown);

        if (rowModelIndex < getTableModel().getRowCount() - 1) {

            getTableModel().moveDown(pmfmToDown);

            int rowViewIndex = rowModelIndex + 1; // getTable().convertRowIndexToView(rowModelIndex);
            selectCell(rowViewIndex, null);

            saveToStrategy();
            getModel().setModify(true);
        }

    }

    /**
     * <p>addPmfms.</p>
     */
    public void addPmfms() {

        ProgramsTableRowModel program = getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow();
        StrategyDTO strategy = getProgramsUI().getStrategiesTableUI().getModel().getSingleSelectedRow();
        Assert.notNull(program);
        Assert.notNull(strategy);

        final SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.PMFM.getFilterTypeId());
        dialog.setTitle(t("reefdb.program.pmfm.new.dialog.title"));
        List<PmfmDTO> pmfms = getModel().getBeans().stream().map(PmfmStrategyDTO::getPmfm)
                .sorted(getDecorator(PmfmDTO.class, DecoratorService.NAME_WITH_ID).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(pmfm -> pmfm.setReadOnly(true))
                .collect(Collectors.toList());

        dialog.getModel().setSelectedElements(pmfms);

        if (!program.isLocal()) {
            // if program is national, location menu must search only national locations
            dialog.getHandler().getFilterElementUIHandler().forceLocal(false);
        }

        openDialog(dialog, new Dimension(1024, 768));

        if (dialog.getModel().isValid()) {

            List<PmfmDTO> newPmfms = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((PmfmDTO) element))
                    .filter(newPmfm -> getModel().getBeans().stream().noneMatch(pmfmStrategy -> pmfmStrategy.getPmfm().equals(newPmfm)))
                    .collect(Collectors.toList());

            if (!newPmfms.isEmpty()) {

                // Add new ones
                strategy.addAllPmfmStrategies(newPmfms.stream().map(ReefDbBeans::pmfmToPmfmStrategy).collect(Collectors.toList()));

                // re-affect lines
                getModel().setBeans(strategy.getPmfmStrategies());
                recomputeRowsValidState();
                saveToStrategy();
                getModel().setModify(true);
            }
        }
    }

    /**
     * <p>getProgramsUI.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI} object.
     */
    public ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

    /**
     * <p>editPmfm.</p>
     */
    public void editPmfm() {

        ProgramsTableRowModel program = getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow();
        Assert.notNull(program);

        final EditPmfmDialogUI dialogue = new EditPmfmDialogUI(getUI());
        dialogue.getModel().setTableModel(getModel());

        if (!program.isLocal()) {
            dialogue.getHandler().forceIsLocal(false);
        }

        openDialog(dialogue, new Dimension(600, 250));

        getTable().repaint();
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(PmfmsTableUIModel.PROPERTY_SELECTED_ROWS, evt -> {

            // Si un seul element a ete selectionne
            if (getModel().getSelectedRows().size() == 1) {

                // Recuperation de l index du psfm selectionne
                final int index = getModel().getRows().indexOf(getModel().getSelectedRows().iterator().next());
                if (index == 0) {

                    // Premier element : ne peut etre monte
                    getModel().setUpAllowed(false);
                    getModel().setDownAllowed(true);

                } else if (index == getModel().getBeans().size() - 1) {

                    // Dernier element : ne peut etre descendu
                    getModel().setUpAllowed(true);
                    getModel().setDownAllowed(false);
                } else {

                    // Bouton active
                    getModel().setUpAllowed(true);
                    getModel().setDownAllowed(true);
                }
            } else {
                // disable all
                getModel().setUpAllowed(false);
                getModel().setDownAllowed(false);
            }
        });
    }

    /**
     * <p>saveToStrategy.</p>
     */
    public void saveToStrategy() {

        if (getModel().isLoading()) return;

        // save modifications on master object
        getProgramsUI().getStrategiesTableUI().getHandler().keepModificationOnPmfmsTable();
        // save modification on master object
        getProgramsUI().getProgramsTableUI().getHandler().keepModificationOnStrategiesTable();

        // force modify state
        recomputeRowsValidState();
        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);

    }

    /**
     * <p>removePmfms.</p>
     */
    public void removePmfms() {
        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucun PSFMU de selectionne");
            return;
        }

        // Demande de confirmation avant la suppression
        if (askBeforeDelete(t("reefdb.program.pmfm.delete.titre"), t("reefdb.program.pmfm.delete.message"))) {

            // Suppression des lignes
            getModel().deleteSelectedRows();

            saveToStrategy();

        }

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        final TableColumnExt pmfmIdCol = addColumn(PmfmsTableModel.PMFM_ID);
        pmfmIdCol.setSortable(false);

        // Colonne libelle psfm
        final TableColumnExt colonneLibelleParametre = addColumn(PmfmsTableModel.PMFM_NAME);
        colonneLibelleParametre.setSortable(false);

        // Colonne code parametre
        final TableColumnExt colonneParametre = addColumn(PmfmsTableModel.PARAMETER_CODE);
        colonneParametre.setSortable(false);

        // Colonne support
        final TableColumnExt colonneSupport = addColumn(PmfmsTableModel.MATRIX);
        colonneSupport.setSortable(false);

        // Colonne fraction
        final TableColumnExt colonneFraction = addColumn(PmfmsTableModel.FRACTION);
        colonneFraction.setSortable(false);

        // Colonne methode
        final TableColumnExt colonneMethode = addColumn(PmfmsTableModel.METHOD);
        colonneMethode.setSortable(false);

        // Colonne unite
        final TableColumnExt colonneUnite = addColumn(PmfmsTableModel.UNIT);
        colonneUnite.setSortable(false);

        // Colonne analyste
        departmentCellEditor = newExtendedComboBoxCellEditor(getContext().getReferentialService().getDepartments(), PmfmsTableModel.ANALYSTE, false);
        final TableColumnExt colonneAnalyste = addColumn(
                departmentCellEditor,
                newTableCellRender(PmfmsTableModel.ANALYSTE),
                PmfmsTableModel.ANALYSTE);
        colonneAnalyste.setSortable(false);

        // Colonne passage
        final TableColumnExt colonnePassage = addBooleanColumnToModel(
                PmfmsTableModel.SURVEY,
                getTable());
        colonnePassage.setSortable(false);

        // Colonne prelevement
        final TableColumnExt colonnePrelevement = addBooleanColumnToModel(
                PmfmsTableModel.SAMPLING,
                getTable());
        colonnePrelevement.setSortable(false);

        // Colonne regroupement
        final TableColumnExt colonneRegroupement = addBooleanColumnToModel(
                PmfmsTableModel.GROUPING,
                getTable());
        colonneRegroupement.setSortable(false);

        // Colonne unicite
        final TableColumnExt colonneUnicite = addBooleanColumnToModel(
                PmfmsTableModel.UNIQUE,
                getTable());
        colonneUnicite.setSortable(false);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
            new AssociatedQualitativeValueCellEditor(getTable(), getUI(), true),
            new AssociatedQualitativeValueCellRenderer(),
            PmfmsTableModel.QUALITATIVE_VALUES);
        associatedQualitativeValueCol.setSortable(true);
        fixColumnWidth(associatedQualitativeValueCol, 120);

        // Modele de la table
        final PmfmsTableModel tableModel = new PmfmsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Set non editable col (do not use table.setEditable(false) because it make non editable the selection check box col non usable)
        tableModel.setNoneEditableCols(PmfmsTableModel.PMFM_ID, PmfmsTableModel.PMFM_NAME, PmfmsTableModel.PARAMETER_CODE, PmfmsTableModel.MATRIX,
                PmfmsTableModel.FRACTION, PmfmsTableModel.METHOD, PmfmsTableModel.UNIT);

        // Les colonnes obligatoire sont toujours presentes
        // colonneParametre.setHideable(false);
        colonneLibelleParametre.setHideable(false);
        // colonneSupport.setHideable(false);
        // colonneFraction.setHideable(false);
        // colonneMethode.setHideable(false);
        // colonneUnite.setHideable(false);
        colonneAnalyste.setHideable(false);
        colonnePassage.setHideable(false);
        colonnePrelevement.setHideable(false);
        colonneRegroupement.setHideable(false);
        colonneUnicite.setHideable(false);
        associatedQualitativeValueCol.setHideable(false);

        // Initialisation de la table
        initTable(getTable());

        pmfmIdCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(4);
    }
}
