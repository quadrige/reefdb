package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Date;

/**
 * <p>ManageContextsListTableUIRowModel class.</p>
 *
 * @author Antoine
 */
public class ManageContextsListTableUIRowModel extends AbstractReefDbRowUIModel<ContextDTO, ManageContextsListTableUIRowModel> implements ContextDTO {

	private static final Binder<ContextDTO, ManageContextsListTableUIRowModel> FROM_BEAN_BINDER = 
			BinderFactory.newBinder(ContextDTO.class, ManageContextsListTableUIRowModel.class);

	private static final Binder<ManageContextsListTableUIRowModel, ContextDTO> TO_BEAN_BINDER = 
			BinderFactory.newBinder(ManageContextsListTableUIRowModel.class, ContextDTO.class);

	
	/**
	 * <p>Constructor for ManageContextsListTableUIRowModel.</p>
	 */
	public ManageContextsListTableUIRowModel() {
		super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
	}

	/** {@inheritDoc} */
	@Override
	protected ContextDTO newBean() {
		return ReefDbBeanFactory.newContextDTO();
	}

	/** {@inheritDoc} */
	@Override
	public String getName() {
		return delegateObject.getName();
	}

	/** {@inheritDoc} */
	@Override
	public void setName(String name) {
		delegateObject.setName(name);
	}

	/** {@inheritDoc} */
	@Override
	public String getDescription() {
		return delegateObject.getDescription();
	}

	/** {@inheritDoc} */
	@Override
	public void setDescription(String description) {
		delegateObject.setDescription(description);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isDirty() {
		return delegateObject.isDirty();
	}

	/** {@inheritDoc} */
	@Override
	public void setDirty(boolean dirty) {
		delegateObject.setDirty(dirty);
	}

	@Override
	public boolean isReadOnly() {
		return delegateObject.isReadOnly();
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		delegateObject.setReadOnly(readOnly);
	}

	@Override
	public StatusDTO getStatus() {
		return delegateObject.getStatus();
	}

	@Override
	public void setStatus(StatusDTO status) {
		delegateObject.setStatus(status);
	}

	/** {@inheritDoc} */
	@Override
	public FilterDTO getFilters(int index) {
		return delegateObject.getFilters(index);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isFiltersEmpty() {
		return delegateObject.isFiltersEmpty();
	}

	/** {@inheritDoc} */
	@Override
	public int sizeFilters() {
		return delegateObject.sizeFilters();
	}

	/** {@inheritDoc} */
	@Override
	public void addFilters(FilterDTO filter) {
		delegateObject.addFilters(filter);
	}

	/** {@inheritDoc} */
	@Override
	public void addAllFilters(Collection<FilterDTO> filter) {
		delegateObject.addAllFilters(filter);
	}

	/** {@inheritDoc} */
	@Override
	public boolean removeFilters(FilterDTO filter) {
		return delegateObject.removeFilters(filter);
	}

	/** {@inheritDoc} */
	@Override
	public boolean removeAllFilters(Collection<FilterDTO> filter) {
		return delegateObject.removeAllFilters(filter);
	}

	/** {@inheritDoc} */
	@Override
	public boolean containsFilters(FilterDTO filter) {
		return delegateObject.containsFilters(filter);
	}

	/** {@inheritDoc} */
	@Override
	public boolean containsAllFilters(Collection<FilterDTO> filter) {
		return delegateObject.containsAllFilters(filter);
	}

	/** {@inheritDoc} */
	@Override
	public Collection<FilterDTO> getFilters() {
		return delegateObject.getFilters();
	}

	/** {@inheritDoc} */
	@Override
	public void setFilters(Collection<FilterDTO> filter) {
		delegateObject.setFilters(filter);
	}

	@Override
	public Date getCreationDate() {
		return delegateObject.getCreationDate();
	}

	@Override
	public void setCreationDate(Date date) {
		delegateObject.setCreationDate(date);
	}

	@Override
	public Date getUpdateDate() {
		return delegateObject.getUpdateDate();
	}

	@Override
	public void setUpdateDate(Date date) {
		delegateObject.setUpdateDate(date);
	}


}
