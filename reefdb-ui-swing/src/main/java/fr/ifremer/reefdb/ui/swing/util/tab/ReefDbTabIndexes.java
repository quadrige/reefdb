package fr.ifremer.reefdb.ui.swing.util.tab;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Les index des onglets.
 */
public interface ReefDbTabIndexes {

	/**
	 * Onglet observations general.
	 */
    int ONGLET_OBSERVATION_GENERAL = 0;
    
    /**
     * Onglet observation mesures.
     */
    int ONGLET_OBSERVATION_MESURES = 1;
    
    /**
     * Onglet prelevement mesures.
     */
    int ONGLET_PRELEVEMENTS_MESURES = 2;
    
    /**
     * Onglet photos.
     */
    int ONGLET_PHOTOS = 3;
}
