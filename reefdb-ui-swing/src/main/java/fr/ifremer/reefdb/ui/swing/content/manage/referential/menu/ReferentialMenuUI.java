package fr.ifremer.reefdb.ui.swing.content.manage.referential.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;

/**
 * Created by Ludovic on 23/03/2016.
 */
public interface ReferentialMenuUI<M extends AbstractReferentialMenuUIModel, H extends ReferentialMenuUIHandler<M, ?>> extends ReefDbUI<M, H> {

    /** {@inheritDoc} */
    @Override
    H getHandler();

    /** {@inheritDoc} */
    @Override
    M getModel();

}
