package fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des Units au niveau local
 */
public class ReferentialUnitsMenuUIModel extends DefaultReferentialMenuUIModel {

    /** Constant <code>PROPERTY_UNIT="unit"</code> */
    public static final String PROPERTY_UNIT = "unit";
    private UnitDTO unit;

    /**
     * <p>getUnitId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getUnitId() {
        return getUnit() == null ? null : getUnit().getId();
    }

    /**
     * <p>Getter for the field <code>unit</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.UnitDTO} object.
     */
    public UnitDTO getUnit() {
        return unit;
    }

    /**
     * <p>Setter for the field <code>unit</code>.</p>
     *
     * @param unit a {@link fr.ifremer.reefdb.dto.referential.UnitDTO} object.
     */
    public void setUnit(UnitDTO unit) {
        this.unit = unit;
        firePropertyChange(PROPERTY_UNIT, null, unit);
    }

}
