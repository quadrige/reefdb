package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.FilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * Clear action.
 */
public class ClearAction extends AbstractCheckModelAction<FilterListUIModel, FilterListUI, FilterListUIHandler> {

	/**
	 * Constructor.
	 *
	 * @param handler Controller
	 */
	public ClearAction(final FilterListUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() {
		
		// Clear combo
		getUI().getFiltersCombo().setSelectedItem(null);
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
		return SaveAction.class;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelModify() {
		return getParentModel().isModify();
	}

	/** {@inheritDoc} */
	@Override
	protected void setModelModify(boolean modelModify) {
		getParentModel().setModify(modelModify);
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelValid() {
		return getParentModel().isValid();
	}

	private FilterUIModel getParentModel() {
		return getHandler().getParentUI().getModel();
	}

	/** {@inheritDoc} */
	@Override
	protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
		return getHandler().getParentUI().getHandler();
	}
}
