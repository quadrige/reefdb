package fr.ifremer.reefdb.ui.swing.content.home;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Save action.
 */
public class SaveAction extends AbstractReefDbSaveAction<HomeUIModel, HomeUI, HomeUIHandler> {

    private Collection<? extends SurveyDTO> surveysToSave;

    private ControlRuleMessagesBean controlMessages;

    private boolean controlSilently = false;

    private boolean showControlIfSuccess = false;

    private boolean updateControlDateWhenControlSucceed = false;

    /**
     * <p>Constructor for SaveAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.home.HomeUIHandler} object.
     */
    public SaveAction(final HomeUIHandler handler) {
        super(handler, false);
    }

    /**
     * <p>Setter for the field <code>surveysToSave</code>.</p>
     *
     * @param surveysToSave a {@link java.util.Collection} object.
     */
    public void setSurveysToSave(Collection<? extends SurveyDTO> surveysToSave) {
        this.surveysToSave = surveysToSave;
    }

    /**
     * <p>Setter for the field <code>controlSilently</code>.</p>
     *
     * @param controlSilently a boolean.
     */
    public void setControlSilently(boolean controlSilently) {
        this.controlSilently = controlSilently;
    }

    /**
     * <p>Setter for the field <code>showControlIfSuccess</code>.</p>
     *
     * @param showControlIfSuccess a boolean.
     */
    public void setShowControlIfSuccess(boolean showControlIfSuccess) {
        this.showControlIfSuccess = showControlIfSuccess;
    }

    /**
     * <p>Setter for the field <code>updateControlDateWhenControlSucceed</code>.</p>
     *
     * @param updateControlDateWhenControlSucceed a boolean.
     */
    public void setUpdateControlDateWhenControlSucceed(boolean updateControlDateWhenControlSucceed) {
        this.updateControlDateWhenControlSucceed = updateControlDateWhenControlSucceed;
    }

    /**
     * <p>Getter for the field <code>controlMessages</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean} object.
     */
    public ControlRuleMessagesBean getControlMessages() {
        return controlMessages;
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }
        // Model is valid
        // the model can be valid if control is valid (Mantis #50538)
        if (!getModel().isValid() && !getModel().isControlValid()) {

            // Remove error
            displayErrorMessage(t("reefdb.action.save.errors.title"), t("reefdb.action.save.errors.remove"));

            // Stop saving
            return false;
        }

        // Selected surveys
        if (surveysToSave == null) {
            surveysToSave = getUI().getSurveysTable().getModel().getRows();
        }

        if (CollectionUtils.isEmpty(surveysToSave)) {
            return false;
        }

        // Additional control before save
        for (SurveyDTO survey : surveysToSave) {
            if (survey.isDirty()) {

                // Check existing surveys on same date, same location in table
                // removed (Mantis #42580)
//                boolean skipSearchInDb = false;
//                for (SurveyDTO otherSurvey : surveysToSave) {
//                    if (survey == otherSurvey) continue;
//                    if (survey.getDate().equals(otherSurvey.getDate())
//                            && survey.getLocation().equals(otherSurvey.getLocation())) {
//                        if (getContext().getDialogHelper().showConfirmDialog(
//                                t("reefdb.action.save.observation.alreadyExistsOnLocationAndDate", ReefDbBeans.toString(otherSurvey)),
//                                t("reefdb.action.save.observations"),
//                                JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
//                            return false;
//                        } else {
//                            skipSearchInDb = true;
//                        }
//                    }
//                }

                // Check existing surveys on same date, same location in database
                // removed (Mantis #42580)
//                if (!skipSearchInDb) {
//                    SurveyFilterDTO surveyFilter = ReefDbBeanFactory.newSurveyFilterDTO();
//                    surveyFilter.setLocationId(survey.getLocation().getId());
//                    surveyFilter.setDate1(survey.getDate());
//                    surveyFilter.setSearchDateId(SearchDateValues.EQUALS.ordinal());
//                    List<SurveyDTO> existingSurveys = getContext().getObservationService().getSurveys(surveyFilter);
//                    if (CollectionUtils.isNotEmpty(existingSurveys)) {
//                        for (SurveyDTO existingSurvey : existingSurveys) {
//                            if (existingSurvey.getId().equals(survey.getId())) continue;
//                            if (getContext().getDialogHelper().showConfirmDialog(
//                                    t("reefdb.action.save.observation.alreadyExistsOnLocationAndDate", ReefDbBeans.toString(existingSurvey)),
//                                    t("reefdb.action.save.observations"),
//                                    JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
//                                return false;
//                            }
//                        }
//                    }
//                }

                // Check survey date within campaign dates
                if (survey.getCampaign() != null) {
                    if (survey.getDate().isBefore(survey.getCampaign().getStartDate())
                            || (survey.getCampaign().getEndDate() != null && survey.getDate().isAfter(survey.getCampaign().getEndDate()))) {
                        // The save is invalid if the survey is outside campaign dates
                        getContext().getDialogHelper().showErrorDialog(
                                t("reefdb.action.save.observation.notInCampaign", ReefDbBeans.toString(survey), ReefDbBeans.toString(survey.getCampaign())),
                                t("reefdb.action.save.observations"));
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        createProgressionUIModel();

        // Save observations
        getContext().getObservationService().saveSurveys(surveysToSave, getProgressionUIModel());

        // control them
        controlMessages = getContext().getControlRuleService().controlSurveys(surveysToSave,
                updateControlDateWhenControlSucceed,
                true /*Reset control date, when failed */,
                getProgressionUIModel());

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        sendMessage(t("reefdb.action.save.observations.success", surveysToSave.size()));

        // reset model modify state only if all surveys in table have been saved
        if (surveysToSave.size() == getModel().getSurveysTableUIModel().getRowCount()) {
            getModel().getSurveysTableUIModel().setModify(false);
            getModel().getOperationsTableUIModel().setModify(false);
            getModel().setModify(false);
        }

        // update selected ids
        if (getModel().getSelectedSurvey() != null) {
            getContext().setSelectedSurveyId(getModel().getSelectedSurvey().getId());
        }

        getUI().getOperationsTable().getHandler().loadOperations(getModel().getSelectedSurvey());

        // If error messages
        if (!controlSilently) {
            getUI().getSurveysTable().getHandler().ensureColumnsWithErrorAreVisible(surveysToSave);
            if (isFromChangeScreenAction()) {
                setAllowChangeScreen(showControlResultAndAskContinue(controlMessages));
            } else {
                showControlResult(controlMessages, showControlIfSuccess);
            }
        }

    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        super.releaseAction();

        surveysToSave = null;
        controlSilently = false;
        showControlIfSuccess = false;
        updateControlDateWhenControlSucceed = false;
    }
}
