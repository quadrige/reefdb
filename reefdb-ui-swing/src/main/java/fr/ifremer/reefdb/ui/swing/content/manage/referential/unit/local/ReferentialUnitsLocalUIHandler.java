package fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.menu.ReferentialUnitsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.table.UnitTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.table.UnitTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Units au niveau local
 */
public class ReferentialUnitsLocalUIHandler extends AbstractReefDbTableUIHandler<UnitTableRowModel, ReferentialUnitsLocalUIModel, ReferentialUnitsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReferentialUnitsLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ReferentialUnitsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ReferentialUnitsLocalUIModel model = new ReferentialUnitsLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ReferentialUnitsLocalUI ui) {
        initUI(ui);

        // Initialisation du tableau
        initTable();

        // Enabled elements
        getUI().getReferentialUnitsLocalTableDeleteBouton().setEnabled(false);
        getUI().getReferentialUnitsLocalTableReplaceBouton().setEnabled(false);

        ReferentialUnitsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(true);

        menuUIModel.addPropertyChangeListener(ReferentialUnitsMenuUIModel.PROPERTY_RESULTS, evt -> loadUnits((Collection<UnitDTO>) evt.getNewValue()));

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(UnitTableModel.NAME);
        mnemonicCol.setSortable(true);

        // Symbol
        final TableColumnExt symbolCol = addColumn(UnitTableModel.SYMBOL);
        symbolCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(UnitTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(UnitTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(UnitTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                UnitTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        final UnitTableModel tableModel = new UnitTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.units.local"));

        // Initialisation du tableau
        initTable(getTable());

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<UnitTableRowModel> getTableModel() {
        return (UnitTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getReferentialUnitsLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<UnitTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            UnitTableRowModel rowModel = addedRows.get(0);

            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));

            setFocusOnCell(rowModel);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, UnitTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(UnitTableRowModel row) {
        row.getErrors().clear();
        return !row.isEditable() || (super.isRowValid(row) && isUnique(row));
    }

    private boolean isUnique(UnitTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName()) && StringUtils.isNotBlank(row.getSymbol())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (UnitTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName()) && row.getSymbol().equalsIgnoreCase(otherRow.getSymbol())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential",
                                    t("reefdb.property.pmfm.unit"), decorate(row, DecoratorService.WITH_SYMBOL), t("reefdb.property.referential.local")),
                            UnitTableRowModel.PROPERTY_NAME,
                            UnitTableRowModel.PROPERTY_SYMBOL);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<UnitDTO> existingUnits = getContext().getReferentialService().getUnits(StatusFilter.ALL);
                if (CollectionUtils.isNotEmpty(existingUnits)) {
                    for (UnitDTO unit : existingUnits) {
                        if (!unit.getId().equals(row.getId())
                                && row.getName().equalsIgnoreCase(unit.getName())
                                && row.getSymbol().equalsIgnoreCase(unit.getSymbol())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.pmfm.unit"), decorate(row, DecoratorService.WITH_SYMBOL),
                                            ReefDbBeans.isLocalStatus(unit.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    UnitTableRowModel.PROPERTY_NAME,
                                    UnitTableRowModel.PROPERTY_SYMBOL);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();
    }

    /**
     * Load units.
     *
     * @param units a {@link java.util.Collection} object.
     */
    public void loadUnits(Collection<UnitDTO> units) {
        getModel().setBeans(units);

        // Model not modify
        getModel().setModify(false);
    }

}
