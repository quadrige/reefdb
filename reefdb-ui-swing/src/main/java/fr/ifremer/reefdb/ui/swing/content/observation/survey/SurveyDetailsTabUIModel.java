package fr.ifremer.reefdb.ui.swing.content.observation.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.SynchronizationStatusDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.CoordinateAware;
import fr.ifremer.reefdb.dto.data.LocationCoordinateAware;
import fr.ifremer.reefdb.dto.data.PositioningPrecisionAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.OccasionDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * Modele pour l onglet observation general.
 */
public class SurveyDetailsTabUIModel extends AbstractReefDbBeanUIModel<SurveyDTO, SurveyDetailsTabUIModel>
        implements SurveyDTO, TabContentModel, PositioningPrecisionAware, CoordinateAware, LocationCoordinateAware {

    /** Constant <code>PROPERTY_OBSERVATION_MODEL="observationModel"</code> */
    public static final String PROPERTY_OBSERVATION_MODEL = "observationModel";
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";
    private static final Binder<SurveyDetailsTabUIModel, SurveyDTO> TO_BEAN_BINDER
            = BinderFactory.newBinder(SurveyDetailsTabUIModel.class, SurveyDTO.class);
    private static final Binder<SurveyDTO, SurveyDetailsTabUIModel> FROM_BEAN_BINDER
            = BinderFactory.newBinder(SurveyDTO.class, SurveyDetailsTabUIModel.class);
    private SurveyDTO observationModel;
    private boolean adjusting;
    private boolean editable;
    private List<PersonDTO> selectedObservers;

    /**
     * Constructor.
     */
    public SurveyDetailsTabUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }

    /**
     * <p>Getter for the field <code>selectedObservers</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PersonDTO> getSelectedObservers() {
        return selectedObservers;
    }

    /**
     * <p>Setter for the field <code>selectedObservers</code>.</p>
     *
     * @param selectedObservers a {@link java.util.List} object.
     */
    public void setSelectedObservers(List<PersonDTO> selectedObservers) {
        this.selectedObservers = selectedObservers;
        setModify(true);
    }

    /**
     * <p>Getter for the field <code>observationModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     */
    public SurveyDTO getObservationModel() {
        return observationModel;
    }

    /**
     * <p>Setter for the field <code>observationModel</code>.</p>
     *
     * @param observationModel a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     */
    public void setObservationModel(SurveyDTO observationModel) {
        this.observationModel = observationModel;
        firePropertyChange(PROPERTY_OBSERVATION_MODEL, null, observationModel);
    }

    /**
     * <p>isAdjusting.</p>
     *
     * @return a boolean.
     */
    public boolean isAdjusting() {
        return adjusting;
    }

    /**
     * <p>Setter for the field <code>adjusting</code>.</p>
     *
     * @param adjusting a boolean.
     */
    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEmpty() {
        return false; //isCreate();
    }

    /** {@inheritDoc} */
    @Override
    public String getTitle() {
        return n("reefdb.survey.general.title");
    }

    /** {@inheritDoc} */
    @Override
    public String getIcon() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCloseable() {
        return false;
    }

    /** {@inheritDoc} */
    @Override
    protected SurveyDTO newBean() {
        return ReefDbBeanFactory.newSurveyDTO();
    }

    private CoordinateDTO getSurveyCoordinateToModify() {
        if (getCoordinate() == null) {
            setCoordinate(ReefDbBeanFactory.newCoordinateDTO());
        }
        return getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public Double getLatitude() {
        return getSurveyCoordinateToModify().getMinLatitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setLatitude(final Double latitude) {
        Double oldValue = getLatitude();
        getSurveyCoordinateToModify().setMinLatitude(latitude);
        firePropertyChange(PROPERTY_LATITUDE, oldValue, latitude);
        setModify(true);
    }

    /** {@inheritDoc} */
    @Override
    public Double getLongitude() {
        return getSurveyCoordinateToModify().getMinLongitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setLongitude(final Double longitude) {
        Double oldValue = getLongitude();
        getSurveyCoordinateToModify().setMinLongitude(longitude);
        firePropertyChange(PROPERTY_LONGITUDE, oldValue, longitude);
        setModify(true);
    }

    /** {@inheritDoc} */
    @Override
    public Double getLocationMinLatitude() {
        return getLocation() == null || getLocation().getCoordinate() == null ? null : getLocation().getCoordinate().getMinLatitude();
    }

    /**
     * <p>setLocationMinLatitude.</p>
     *
     * @param dummy a {@link java.lang.Double} object.
     */
    public void setLocationMinLatitude(Double dummy) {
    }

    /** {@inheritDoc} */
    @Override
    public Double getLocationMaxLatitude() {
        return getLocation() == null || getLocation().getCoordinate() == null ? null : getLocation().getCoordinate().getMaxLatitude();
    }

    /**
     * <p>setLocationMaxLatitude.</p>
     *
     * @param dummy a {@link java.lang.Double} object.
     */
    public void setLocationMaxLatitude(Double dummy) {
    }

    /** {@inheritDoc} */
    @Override
    public Double getLocationMinLongitude() {
        return getLocation() == null || getLocation().getCoordinate() == null ? null : getLocation().getCoordinate().getMinLongitude();
    }

    /**
     * <p>setLocationMinLongitude.</p>
     *
     * @param dummy a {@link java.lang.Double} object.
     */
    public void setLocationMinLongitude(Double dummy) {
    }

    /** {@inheritDoc} */
    @Override
    public Double getLocationMaxLongitude() {
        return getLocation() == null || getLocation().getCoordinate() == null ? null : getLocation().getCoordinate().getMaxLongitude();
    }

    /**
     * <p>setLocationMaxLongitude.</p>
     *
     * @param dummy a {@link java.lang.Double} object.
     */
    public void setLocationMaxLongitude(Double dummy) {
    }

    /** {@inheritDoc} */
    @Override
    public String getPositioningPrecision() {
        return getPositioning() == null ? null : getPositioning().getPrecision();
    }

    /**
     * <p>setPositioningPrecision.</p>
     *
     * @param dummy a {@link java.lang.String} object.
     */
    public void setPositioningPrecision(String dummy) {
    }

    /* DELEGATE METHODS */

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public LocalDate getDate() {
        return delegateObject.getDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setDate(LocalDate date) {
        delegateObject.setDate(date);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getTime() {
        return delegateObject.getTime();
    }

    /** {@inheritDoc} */
    @Override
    public void setTime(Integer time) {
        delegateObject.setTime(time);
    }

    /** {@inheritDoc} */
    @Override
    public Double getPreciseDepth() {
        return delegateObject.getPreciseDepth();
    }

    /** {@inheritDoc} */
    @Override
    public void setPreciseDepth(Double preciseDepth) {
        delegateObject.setPreciseDepth(preciseDepth);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public String getPositioningComment() {
        return delegateObject.getPositioningComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setPositioningComment(String positioningComment) {
        delegateObject.setPositioningComment(positioningComment);
    }

    /** {@inheritDoc} */
    @Override
    public String getQualificationComment() {
        return delegateObject.getQualificationComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setQualificationComment(String qualificationComment) {
        delegateObject.setQualificationComment(qualificationComment);
    }

    /** {@inheritDoc} */
    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setUpdateDate(Date updateDate) {
        delegateObject.setUpdateDate(updateDate);
    }

    /** {@inheritDoc} */
    @Override
    public Date getControlDate() {
        return delegateObject.getControlDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setControlDate(Date controlDate) {
        delegateObject.setControlDate(controlDate);
    }

    /** {@inheritDoc} */
    @Override
    public Date getValidationDate() {
        return delegateObject.getValidationDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setValidationDate(Date validationDate) {
        delegateObject.setValidationDate(validationDate);
    }

    @Override
    public String getValidationComment() {
        return delegateObject.getValidationComment();
    }

    @Override
    public void setValidationComment(String validationComment) {
        delegateObject.setValidationComment(validationComment);
    }

    /** {@inheritDoc} */
    @Override
    public Date getQualificationDate() {
        return delegateObject.getQualificationDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setQualificationDate(Date validationDate) {
        delegateObject.setQualificationDate(validationDate);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isSamplingOperationsLoaded() {
        return delegateObject.isSamplingOperationsLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setSamplingOperationsLoaded(boolean samplingOperationsLoaded) {
        delegateObject.setSamplingOperationsLoaded(samplingOperationsLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isObserversLoaded() {
        return delegateObject.isObserversLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setObserversLoaded(boolean observersLoaded) {
        delegateObject.setObserversLoaded(observersLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMeasurementsLoaded() {
        return delegateObject.isMeasurementsLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setMeasurementsLoaded(boolean measurementsLoaded) {
        delegateObject.setMeasurementsLoaded(measurementsLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPhotosLoaded() {
        return delegateObject.isPhotosLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setPhotosLoaded(boolean photosLoaded) {
        delegateObject.setPhotosLoaded(photosLoaded);
    }

    @Override
    public boolean isActualPosition() {
        return delegateObject.isActualPosition();
    }

    @Override
    public void setActualPosition(boolean actualPosition) {
        delegateObject.setActualPosition(actualPosition);
    }

    @Override
    public List<Integer> getInheritedRecorderDepartmentIds() {
        return delegateObject.getInheritedRecorderDepartmentIds();
    }

    @Override
    public void setInheritedRecorderDepartmentIds(List<Integer> inheritedRecorderDepartmentIds) {
        delegateObject.setInheritedRecorderDepartmentIds(inheritedRecorderDepartmentIds);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getRecorderDepartment() {
        return delegateObject.getRecorderDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setRecorderDepartment(DepartmentDTO department) {
        delegateObject.setRecorderDepartment(department);
    }

    /** {@inheritDoc} */
    @Override
    public DepthDTO getDepth() {
        return delegateObject.getDepth();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepth(DepthDTO depth) {
        delegateObject.setDepth(depth);
    }

    /** {@inheritDoc} */
    @Override
    public CoordinateDTO getCoordinate() {
        return delegateObject.getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public void setCoordinate(CoordinateDTO coordinate) {
        delegateObject.setCoordinate(coordinate);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioning() {
        return delegateObject.getPositioning();
    }

    /** {@inheritDoc} */
    @Override
    public void setPositioning(PositioningSystemDTO positioning) {
        delegateObject.setPositioning(positioning);
    }

    /** {@inheritDoc} */
    @Override
    public CampaignDTO getCampaign() {
        return delegateObject.getCampaign();
    }

    /** {@inheritDoc} */
    @Override
    public void setCampaign(CampaignDTO campaign) {
        delegateObject.setCampaign(campaign);
    }

    /** {@inheritDoc} */
    @Override
    public OccasionDTO getOccasion() {
        return delegateObject.getOccasion();
    }

    /** {@inheritDoc} */
    @Override
    public void setOccasion(OccasionDTO occasion) {
        delegateObject.setOccasion(occasion);
    }

    /** {@inheritDoc} */
    @Override
    public LocationDTO getLocation() {
        return delegateObject.getLocation();
    }

    /** {@inheritDoc} */
    @Override
    public void setLocation(LocationDTO location) {
        delegateObject.setLocation(location);
    }

    @Override
    public DepartmentDTO getDepthAnalyst() {
        return delegateObject.getDepthAnalyst();
    }

    @Override
    public void setDepthAnalyst(DepartmentDTO depthAnalyst) {
        delegateObject.setDepthAnalyst(depthAnalyst);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmDTO getIndividualPmfms(int index) {
        return delegateObject.getIndividualPmfms(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isIndividualPmfmsEmpty() {
        return delegateObject.isIndividualPmfmsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeIndividualPmfms() {
        return delegateObject.sizeIndividualPmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void addIndividualPmfms(PmfmDTO individualPmfms) {
        delegateObject.addIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllIndividualPmfms(Collection<PmfmDTO> individualPmfms) {
        delegateObject.addAllIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeIndividualPmfms(PmfmDTO individualPmfms) {
        return delegateObject.removeIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllIndividualPmfms(Collection<PmfmDTO> individualPmfms) {
        return delegateObject.removeAllIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsIndividualPmfms(PmfmDTO individualPmfms) {
        return delegateObject.containsIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllIndividualPmfms(Collection<PmfmDTO> individualPmfms) {
        return delegateObject.containsAllIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmDTO> getIndividualPmfms() {
        return delegateObject.getIndividualPmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void setIndividualPmfms(List<PmfmDTO> individualPmfms) {
        delegateObject.setIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmDTO getPmfms(int index) {
        return delegateObject.getPmfms(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmsEmpty() {
        return delegateObject.isPmfmsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizePmfms() {
        return delegateObject.sizePmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void addPmfms(PmfmDTO pmfms) {
        delegateObject.addPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllPmfms(Collection<PmfmDTO> pmfms) {
        delegateObject.addAllPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removePmfms(PmfmDTO pmfms) {
        return delegateObject.removePmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllPmfms(Collection<PmfmDTO> pmfms) {
        return delegateObject.removeAllPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsPmfms(PmfmDTO pmfms) {
        return delegateObject.containsPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllPmfms(Collection<PmfmDTO> pmfms) {
        return delegateObject.containsAllPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmDTO> getPmfms() {
        return delegateObject.getPmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void setPmfms(List<PmfmDTO> pmfms) {
        delegateObject.setPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public SamplingOperationDTO getSamplingOperations(int index) {
        return delegateObject.getSamplingOperations(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isSamplingOperationsEmpty() {
        return delegateObject.isSamplingOperationsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeSamplingOperations() {
        return delegateObject.sizeSamplingOperations();
    }

    /** {@inheritDoc} */
    @Override
    public void addSamplingOperations(SamplingOperationDTO samplingOperations) {
        delegateObject.addSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllSamplingOperations(Collection<SamplingOperationDTO> samplingOperations) {
        delegateObject.addAllSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeSamplingOperations(SamplingOperationDTO samplingOperations) {
        return delegateObject.removeSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllSamplingOperations(Collection<SamplingOperationDTO> samplingOperations) {
        return delegateObject.removeAllSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsSamplingOperations(SamplingOperationDTO samplingOperations) {
        return delegateObject.containsSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllSamplingOperations(Collection<SamplingOperationDTO> samplingOperations) {
        return delegateObject.containsAllSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<SamplingOperationDTO> getSamplingOperations() {
        return delegateObject.getSamplingOperations();
    }

    /** {@inheritDoc} */
    @Override
    public void setSamplingOperations(Collection<SamplingOperationDTO> samplingOperations) {
        delegateObject.setSamplingOperations(samplingOperations);
    }

    /** {@inheritDoc} */
    @Override
    public PhotoDTO getPhotos(int index) {
        return delegateObject.getPhotos(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPhotosEmpty() {
        return delegateObject.isPhotosEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizePhotos() {
        return delegateObject.sizePhotos();
    }

    /** {@inheritDoc} */
    @Override
    public void addPhotos(PhotoDTO photo) {
        delegateObject.addPhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllPhotos(Collection<PhotoDTO> photo) {
        delegateObject.addAllPhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removePhotos(PhotoDTO photo) {
        return delegateObject.removePhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllPhotos(Collection<PhotoDTO> photo) {
        return delegateObject.removeAllPhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsPhotos(PhotoDTO photo) {
        return delegateObject.containsPhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllPhotos(Collection<PhotoDTO> photo) {
        return delegateObject.containsAllPhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public List<PhotoDTO> getPhotos() {
        return delegateObject.getPhotos();
    }

    /** {@inheritDoc} */
    @Override
    public void setPhotos(List<PhotoDTO> photo) {
        delegateObject.setPhotos(photo);
    }

    /** {@inheritDoc} */
    @Override
    public ProgramDTO getProgram() {
        return delegateObject.getProgram();
    }

    /** {@inheritDoc} */
    @Override
    public void setProgram(ProgramDTO program) {
        delegateObject.setProgram(program);
    }

    /** {@inheritDoc} */
    @Override
    public MeasurementDTO getMeasurements(int index) {
        return delegateObject.getMeasurements(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMeasurementsEmpty() {
        return delegateObject.isMeasurementsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeMeasurements() {
        return delegateObject.sizeMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void addMeasurements(MeasurementDTO measurements) {
        delegateObject.addMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllMeasurements(Collection<MeasurementDTO> measurements) {
        delegateObject.addAllMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeMeasurements(MeasurementDTO measurements) {
        return delegateObject.removeMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllMeasurements(Collection<MeasurementDTO> measurements) {
        return delegateObject.removeAllMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsMeasurements(MeasurementDTO measurements) {
        return delegateObject.containsMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllMeasurements(Collection<MeasurementDTO> measurements) {
        return delegateObject.containsAllMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public List<MeasurementDTO> getMeasurements() {
        return delegateObject.getMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void setMeasurements(List<MeasurementDTO> measurements) {
        delegateObject.setMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getObservers(int index) {
        return delegateObject.getObservers(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isObserversEmpty() {
        return delegateObject.isObserversEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeObservers() {
        return delegateObject.sizeObservers();
    }

    /** {@inheritDoc} */
    @Override
    public void addObservers(PersonDTO observers) {
        delegateObject.addObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllObservers(Collection<PersonDTO> observers) {
        delegateObject.addAllObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeObservers(PersonDTO observers) {
        return delegateObject.removeObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllObservers(Collection<PersonDTO> observers) {
        return delegateObject.removeAllObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsObservers(PersonDTO observers) {
        return delegateObject.containsObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllObservers(Collection<PersonDTO> observers) {
        return delegateObject.containsAllObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PersonDTO> getObservers() {
        return delegateObject.getObservers();
    }

    /** {@inheritDoc} */
    @Override
    public void setObservers(Collection<PersonDTO> observers) {
        delegateObject.setObservers(observers);
    }

    /** {@inheritDoc} */
    @Override
    public SynchronizationStatusDTO getSynchronizationStatus() {
        return delegateObject.getSynchronizationStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setSynchronizationStatus(SynchronizationStatusDTO synchronizationStatus) {
        delegateObject.setSynchronizationStatus(synchronizationStatus);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO error) {
        delegateObject.addErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> error) {
        delegateObject.addAllErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO error) {
        return delegateObject.removeErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> error) {
        return delegateObject.removeAllErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO error) {
        return delegateObject.containsErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> error) {
        return delegateObject.containsAllErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> error) {
        delegateObject.setErrors(error);
    }

    /** {@inheritDoc} */
    @Override
    public MeasurementDTO getIndividualMeasurements(int index) {
        return delegateObject.getIndividualMeasurements(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isIndividualMeasurementsEmpty() {
        return delegateObject.isIndividualMeasurementsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeIndividualMeasurements() {
        return delegateObject.sizeIndividualMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void addIndividualMeasurements(MeasurementDTO individualMeasurements) {
        delegateObject.addIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllIndividualMeasurements(Collection<MeasurementDTO> individualMeasurements) {
        delegateObject.addAllIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeIndividualMeasurements(MeasurementDTO individualMeasurements) {
        return delegateObject.removeIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllIndividualMeasurements(Collection<MeasurementDTO> individualMeasurements) {
        return delegateObject.removeAllIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsIndividualMeasurements(MeasurementDTO individualMeasurements) {
        return delegateObject.containsIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllIndividualMeasurements(Collection<MeasurementDTO> individualMeasurements) {
        return delegateObject.containsAllIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public List<MeasurementDTO> getIndividualMeasurements() {
        return delegateObject.getIndividualMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void setIndividualMeasurements(List<MeasurementDTO> individualMeasurements) {
        delegateObject.setIndividualMeasurements(individualMeasurements);
    }

    /**
     * <p>getCampaignId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getCampaignId() {
        return getCampaign() == null ? null : getCampaign().getId();
    }

    /**
     * <p>getLocationId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLocationId() {
        return getLocation() == null ? null : getLocation().getId();
    }

    /**
     * <p>getProgramCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProgramCode() {
        return getProgram() == null ? null : getProgram().getCode();
    }
}
