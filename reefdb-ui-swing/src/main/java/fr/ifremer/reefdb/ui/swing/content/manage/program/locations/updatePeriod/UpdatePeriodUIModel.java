package fr.ifremer.reefdb.ui.swing.content.manage.program.locations.updatePeriod;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * Created by Ludovic on 26/05/2015.
 */
public class UpdatePeriodUIModel extends AbstractEmptyUIModel<UpdatePeriodUIModel> {

    /**
     * Table model.
     */
    private LocationsTableUIModel tableModel;

    /**
     * <p>Getter for the field <code>tableModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel} object.
     */
    public LocationsTableUIModel getTableModel() {
        return tableModel;
    }

    /**
     * <p>Setter for the field <code>tableModel</code>.</p>
     *
     * @param tableModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel} object.
     */
    public void setTableModel(LocationsTableUIModel tableModel) {
        this.tableModel = tableModel;
    }
}
