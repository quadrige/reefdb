package fr.ifremer.reefdb.ui.swing.content.manage.rule.department;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;

import java.util.Date;

/**
 * <p>ControlDepartmentTableRowModel class.</p>
 *
 * @author Antoine
 */
public class ControlDepartmentTableRowModel extends AbstractReefDbRowUIModel<DepartmentDTO, ControlDepartmentTableRowModel> implements DepartmentDTO {

	private static final Binder<DepartmentDTO, ControlDepartmentTableRowModel> FROM_BEAN_BINDER =
			BinderFactory.newBinder(DepartmentDTO.class, ControlDepartmentTableRowModel.class);

	private static final Binder<ControlDepartmentTableRowModel, DepartmentDTO> TO_BEAN_BINDER =
			BinderFactory.newBinder(ControlDepartmentTableRowModel.class, DepartmentDTO.class);

	
	/**
	 * <p>Constructor for ControlDepartmentTableRowModel.</p>
	 */
	public ControlDepartmentTableRowModel() {
		super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
	}

	/** {@inheritDoc} */
	@Override
	protected DepartmentDTO newBean() {
		return ReefDbBeanFactory.newDepartmentDTO();
	}

	/** {@inheritDoc} */
	@Override
	public String getCode() {
		return delegateObject.getCode();
	}

	/** {@inheritDoc} */
	@Override
	public void setCode(String code) {
		delegateObject.setCode(code);
	}

	/** {@inheritDoc} */
	@Override
	public StatusDTO getStatus() {
		return delegateObject.getStatus();
	}

	/** {@inheritDoc} */
	@Override
	public void setStatus(StatusDTO status) {
		delegateObject.setStatus(status);
	}

	/** {@inheritDoc} */
	@Override
	public String getName() {
		return delegateObject.getName();
	}

	/** {@inheritDoc} */
	@Override
	public void setName(String mnemonique) {
		delegateObject.setName(mnemonique);
	}

	/** {@inheritDoc} */
	@Override
	public String getEmail() {
		return delegateObject.getEmail();
	}

	/** {@inheritDoc} */
	@Override
	public void setEmail(String email) {
		delegateObject.setEmail(email);
	}

	/** {@inheritDoc} */
	@Override
	public String getAddress() {
		return delegateObject.getAddress();
	}

	/** {@inheritDoc} */
	@Override
	public void setAddress(String address) {
		delegateObject.setAddress(address);
	}

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    /** {@inheritDoc} */
    @Override
    public String getPhone() {
        return delegateObject.getPhone();
    }

    /** {@inheritDoc} */
    @Override
    public void setPhone(String phone) {
        delegateObject.setPhone(phone);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getParentDepartment() {
        return delegateObject.getParentDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentDepartment(DepartmentDTO parentDepartment) {
        delegateObject.setParentDepartment(parentDepartment);
    }

	/** {@inheritDoc} */
	@Override
	public boolean isDirty() {
		return delegateObject.isDirty();
	}

	/** {@inheritDoc} */
	@Override
	public void setDirty(boolean dirty) {
		delegateObject.setDirty(dirty);
	}

	@Override
	public boolean isReadOnly() {
		return delegateObject.isReadOnly();
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		delegateObject.setReadOnly(readOnly);
	}

	@Override
	public Date getCreationDate() {
		return delegateObject.getCreationDate();
	}

	@Override
	public void setCreationDate(Date date) {
		delegateObject.setCreationDate(date);
	}

	@Override
	public Date getUpdateDate() {
		return delegateObject.getUpdateDate();
	}

	@Override
	public void setUpdateDate(Date date) {
		delegateObject.setUpdateDate(date);
	}

	@Override
	public String getComment() {
		return delegateObject.getComment();
	}

	@Override
	public void setComment(String comment) {
		delegateObject.setComment(comment);
	}

	@Override
	public String getSiret() {
		return delegateObject.getSiret();
	}

	@Override
	public void setSiret(String siret) {
		delegateObject.setSiret(siret);
	}

	@Override
	public String getUrl() {
		return delegateObject.getUrl();
	}

	@Override
	public void setUrl(String url) {
		delegateObject.setUrl(url);
	}


}
