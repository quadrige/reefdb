package fr.ifremer.reefdb.ui.swing.content.manage.referential.user;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import org.apache.commons.collections4.CollectionUtils;

import javax.swing.JOptionPane;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action fermer une observation.
 */
public class SaveAction extends AbstractReefDbSaveAction<ManageUsersUIModel, ManageUsersUI, ManageUsersUIHandler> {

    private List<PersonDTO> users;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ManageUsersUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        // if the screen can be saved and the screen is valid
        if (!super.prepareAction() || !getModel().isModify() || !getModel().isValid()) {
            return false;
        }

        users = getModel().getLocalUIModel().getBeans();

        // check uniqueness of users in national referential
        for (PersonDTO user : users) {
            if (user.isDirty()) {
                PersonCriteriaDTO criteria = ReefDbBeanFactory.newPersonCriteriaDTO();
                criteria.setLogin(user.getIntranetLogin());
                List<PersonDTO> result = getContext().getUserService().searchUser(criteria);
                if (CollectionUtils.isNotEmpty(result)) {
                    for (PersonDTO existingUser : result) {
                        if (!existingUser.getId().equals(user.getId())) {
                            getContext().getDialogHelper().showErrorDialog(t("reefdb.user.existing.login.message", user.getIntranetLogin()));
                            return false;
                        }
                    }
                }

                criteria.setLogin(null);
                criteria.setName(user.getName());
                criteria.setFirstName(user.getFirstName());
                criteria.setStrictName(true);
                result = getContext().getUserService().searchUser(criteria);
                if (CollectionUtils.isNotEmpty(result)) {
                    for (PersonDTO existingUser : result) {
                        if (!existingUser.getId().equals(user.getId()) && getContext().getDialogHelper().showConfirmDialog(
                                ReefDbBeans.isLocalStatus(existingUser.getStatus())
                                        ? t("reefdb.user.existing.local.message", decorate(existingUser))
                                        : t("reefdb.user.existing.national.message", decorate(existingUser)),
                                JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                            return false;
                        }
                    }
                }

            }
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        getContext().getUserService().saveUsers(users);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        getModel().getLocalUIModel().setBeans(users);
        getModel().setModify(false);
        super.postSuccessAction();
    }

}
