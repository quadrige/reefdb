package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.referential.GroupingDTO;
import fr.ifremer.reefdb.dto.referential.HarbourDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.PositioningSystemDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;

/**
 * Modele pour le tableau de programmes.
 */
public class LieuxProgrammeTableRowModel extends AbstractReefDbRowUIModel<LocationDTO, LieuxProgrammeTableRowModel> implements LocationDTO {

    /**
     * Binder from.
     */
    private static final Binder<LocationDTO, LieuxProgrammeTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(LocationDTO.class, LieuxProgrammeTableRowModel.class);

    /**
     * Binder to.
     */
    private static final Binder<LieuxProgrammeTableRowModel, LocationDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(LieuxProgrammeTableRowModel.class, LocationDTO.class);

    /**
     * Constructor.
     */
    public LieuxProgrammeTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected LocationDTO newBean() {
        return ReefDbBeanFactory.newLocationDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        return delegateObject.getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLabel(String label) {
        delegateObject.setLabel(label);
    }

    /** {@inheritDoc} */
    @Override
    public Double getBathymetry() {
        return delegateObject.getBathymetry();
    }

    /** {@inheritDoc} */
    @Override
    public void setBathymetry(Double bathymetry) {
        delegateObject.setBathymetry(bathymetry);
    }

    /** {@inheritDoc} */
    @Override
    public Double getUtFormat() {
        return delegateObject.getUtFormat();
    }

    /** {@inheritDoc} */
    @Override
    public void setUtFormat(Double utFormat) {
        delegateObject.setUtFormat(utFormat);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean getDayLightSavingTime() {
        return delegateObject.getDayLightSavingTime();
    }

    /** {@inheritDoc} */
    @Override
    public void setDayLightSavingTime(Boolean dayLightSavingTime) {
        delegateObject.setDayLightSavingTime(dayLightSavingTime);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioning() {
        return delegateObject.getPositioning();
    }

    /** {@inheritDoc} */
    @Override
    public void setPositioning(PositioningSystemDTO positioning) {
        delegateObject.setPositioning(positioning);
    }

    /** {@inheritDoc} */
    @Override
    public CoordinateDTO getCoordinate() {
        return delegateObject.getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public void setCoordinate(CoordinateDTO coordinate) {
        delegateObject.setCoordinate(coordinate);
    }

    /** {@inheritDoc} */
    @Override
    public GroupingDTO getGrouping() {
        return delegateObject.getGrouping();
    }

    /** {@inheritDoc} */
    @Override
    public void setGrouping(GroupingDTO grouping) {
        delegateObject.setGrouping(grouping);
    }

    /** {@inheritDoc} */
    @Override
    public HarbourDTO getHarbour() {
        return delegateObject.getHarbour();
    }

    /** {@inheritDoc} */
    @Override
    public void setHarbour(HarbourDTO harbour) {
        delegateObject.setHarbour(harbour);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
