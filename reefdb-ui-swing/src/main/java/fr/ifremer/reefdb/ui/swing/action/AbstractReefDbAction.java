package fr.ifremer.reefdb.ui.swing.action;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.quadrige3.ui.swing.action.AbstractAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportReferentialSynchroAtOnceAction;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 05/01/2018.
 */
public abstract class AbstractReefDbAction<M extends AbstractBeanUIModel, UI extends ReefDbUI<M, ?>, H extends AbstractUIHandler<M, UI>>
        extends AbstractAction<M, UI, H> {

    /**
     * <p>Constructor for AbstractReefDbAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractReefDbAction(H handler, boolean hideBody) {
        super(handler, hideBody);
    }

    @Override
    protected ReefDbConfiguration getConfig() {
        return (ReefDbConfiguration) super.getConfig();
    }

    @Override
    public ReefDbUIContext getContext() {
        return (ReefDbUIContext) super.getContext();
    }

    /* Utility methods */

    /**
     * <p>showControlResult.</p>
     *
     * @param messages a {@link fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean} object.
     * @param showIfSuccess a boolean.
     */
    protected void showControlResult(ControlRuleMessagesBean messages, boolean showIfSuccess) {

        if (messages == null || (!messages.isErrorMessages() && !messages.isWarningMessages()) ) {
            if (showIfSuccess) {
                displayInfoMessage(t("reefdb.rulesControl.title"), t("reefdb.rulesControl.noMessage.message"));
            }
        } else if (messages.isWarningMessages() && messages.isErrorMessages()) {
            displayErrorMessage(t("reefdb.rulesControl.title"), t("reefdb.rulesControl.errorAndWarningMessages.message"));
        } else if (messages.isErrorMessages()) {
            displayErrorMessage(t("reefdb.rulesControl.title"), t("reefdb.rulesControl.errorMessages.message"));
        } else if (messages.isWarningMessages()) {
            displayWarningMessage(t("reefdb.rulesControl.title"), t("reefdb.rulesControl.warningMessages.message"));
        }
    }

    /**
     * <p>showControlResultAndAskContinue.</p>
     *
     * @param messages      a {@link ControlRuleMessagesBean} object.
     */
    protected boolean showControlResultAndAskContinue(ControlRuleMessagesBean messages) {

        // TODO show all detailed messages ?
        boolean canContinue = true;
        if (messages != null) {
            if (messages.isWarningMessages() && messages.isErrorMessages()) {
                canContinue = getContext().getDialogHelper().showOptionDialog(null,
                        t("reefdb.rulesControl.errorAndWarningMessages.message"),
                        null,
                        t("reefdb.rulesControl.continue.message"),
                        t("reefdb.rulesControl.title"),
                        JOptionPane.ERROR_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("reefdb.common.continue"),
                        t("reefdb.common.cancel")
                ) == JOptionPane.OK_OPTION;
            } else if (messages.isErrorMessages()) {
                canContinue = getContext().getDialogHelper().showOptionDialog(null,
                        t("reefdb.rulesControl.errorMessages.message"),
                        null,
                        t("reefdb.rulesControl.continue.message"),
                        t("reefdb.rulesControl.title"),
                        JOptionPane.ERROR_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("reefdb.common.continue"),
                        t("reefdb.common.cancel")
                ) == JOptionPane.OK_OPTION;
            } else if (messages.isWarningMessages()) {
                canContinue = getContext().getDialogHelper().showOptionDialog(null,
                        t("reefdb.rulesControl.warningMessages.message"),
                        null,
                        t("reefdb.rulesControl.continue.message"),
                        t("reefdb.rulesControl.title"),
                        JOptionPane.WARNING_MESSAGE,
                        DialogHelper.CUSTOM_OPTION,
                        t("reefdb.common.continue"),
                        t("reefdb.common.cancel")
                ) == JOptionPane.OK_OPTION;
            }
        }
        return canContinue;
    }

    /**
     * Do an import of referential data
     */
    protected void doImportReferential() {

        getModel().setModify(false); // avoid modified model detection

        // Will clear the updateDt cache, then reimport referential tables
        ImportReferentialSynchroAtOnceAction importAction = getContext().getActionFactory().createLogicAction(getContext().getMainUI().getHandler(), ImportReferentialSynchroAtOnceAction.class);
        importAction.setForceClearUpdateDateCache(true);

        // force hide body
        importAction.getContext().setHideBody(false);

        // override action description
        forceActionDescription(t("reefdb.action.synchro.import.referential"));

        // run internally
        getContext().getActionEngine().runInternalAction(importAction);

        // restore body
        importAction.getContext().setHideBody(true);

    }

}
