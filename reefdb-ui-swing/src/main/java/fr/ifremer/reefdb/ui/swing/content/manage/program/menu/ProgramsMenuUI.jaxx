<!--
  #%L
  Reef DB :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JPanel decorator='help'
        implements='fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUI&lt;ProgramsMenuUIModel, ProgramsMenuUIHandler&gt;'>
  <import>
    fr.ifremer.reefdb.ui.swing.ReefDbHelpBroker
    fr.ifremer.reefdb.ui.swing.ReefDbUIContext
    fr.ifremer.reefdb.ui.swing.util.ReefDbUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI

    fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO
    fr.ifremer.reefdb.dto.BooleanDTO

    java.awt.FlowLayout
    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.BorderLayout
    javax.swing.SwingConstants

    fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox

    static org.nuiton.i18n.I18n.*
  </import>

  <ProgramsMenuUIModel id='model' initializer='getContextValue(ProgramsMenuUIModel.class)'/>

  <ReefDbHelpBroker id='broker' constructorParams='"reefdb.home.help"'/>

  <script><![CDATA[
		public ProgramsMenuUI(ApplicationUI parentUI) {
		    ApplicationUIUtil.setParentUI(this, parentUI);
		}
	]]></script>

  <JPanel id="menuPanel" layout='{new BoxLayout(menuPanel, BoxLayout.PAGE_AXIS)}'>

    <ApplyFilterUI id='applyFilterUI' constructorParams='this'/>

    <Table id="selectionPanel" fill="both">
      <row>
        <cell>
          <JPanel id='localPanel' layout='{new BorderLayout()}'>
            <JLabel id='localLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='localCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='BooleanDTO'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='programCodeLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='programCodeCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='ProgramDTO'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='programMnemonicLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='programMnemonicCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='ProgramDTO'/>
          </JPanel>
        </cell>
      </row>
    </Table>

    <JPanel id="selectionButtonsPanel" layout='{new FlowLayout()}'>
      <JButton id='clearButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='searchButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
    </JPanel>

  </JPanel>
</JPanel>