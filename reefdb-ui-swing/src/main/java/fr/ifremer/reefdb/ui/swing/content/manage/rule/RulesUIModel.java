package fr.ifremer.reefdb.ui.swing.content.manage.rule;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.ControlRuleTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.department.ControlDepartmentTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.pmfm.ControlPmfmTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.program.ControlProgramTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist.RuleListRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist.RuleListUIModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * Modele pour l administration de contextes.
 */
public class RulesUIModel extends AbstractEmptyUIModel<RulesUIModel> {

    private RuleListUIModel ruleListUIModel;
    private ControlProgramTableUIModel programsUIModel;
    private ControlDepartmentTableUIModel departmentsUIModel;
    private ControlRuleTableUIModel controlRuleUIModel;
    private ControlPmfmTableUIModel pmfmUIModel;

    private RuleListRowModel selectedRuleList;

    private boolean userIsAdmin;
    private boolean managedProgramsAvailable = false;

    public static final String PROPERTY_RULE_MESSAGE = "ruleMessage";
    public static final String PROPERTY_RULE_DESCRIPTION = "ruleDescription";
    private String ruleMessage;
    private String ruleDescription;

    private boolean saveEnabled;
    public static final String PROPERTY_SAVE_ENABLED = "saveEnabled";

    /** {@inheritDoc} */
    @Override
    public void setModify(boolean modify) {

        if (!modify) {
            ruleListUIModel.setModify(false);
            programsUIModel.setModify(false);
            departmentsUIModel.setModify(false);
            controlRuleUIModel.setModify(false);
            pmfmUIModel.setModify(false);
        }
        super.setModify(modify);
    }

    /**
     * <p>Getter for the field <code>ruleListUIModel</code>.</p>
     *
     * @return a {@link RuleListUIModel} object.
     */
    public RuleListUIModel getRuleListUIModel() {
        return ruleListUIModel;
    }

    /**
     * <p>Setter for the field <code>ruleListUIModel</code>.</p>
     *
     * @param ruleListUIModel a {@link RuleListUIModel} object.
     */
    public void setRuleListUIModel(RuleListUIModel ruleListUIModel) {
        this.ruleListUIModel = ruleListUIModel;
        this.ruleListUIModel.setParentModel(this);
    }

    /**
     * <p>Getter for the field <code>programsUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.program.ControlProgramTableUIModel} object.
     */
    public ControlProgramTableUIModel getProgramsUIModel() {
        return programsUIModel;
    }

    /**
     * <p>Setter for the field <code>programsUIModel</code>.</p>
     *
     * @param programsUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.program.ControlProgramTableUIModel} object.
     */
    public void setProgramsUIModel(ControlProgramTableUIModel programsUIModel) {
        this.programsUIModel = programsUIModel;
        this.programsUIModel.setParentModel(this);
    }

    /**
     * <p>Getter for the field <code>departmentsUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.department.ControlDepartmentTableUIModel} object.
     */
    public ControlDepartmentTableUIModel getDepartmentsUIModel() {
        return departmentsUIModel;
    }

    /**
     * <p>Setter for the field <code>departmentsUIModel</code>.</p>
     *
     * @param departmentsUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.department.ControlDepartmentTableUIModel} object.
     */
    public void setDepartmentsUIModel(ControlDepartmentTableUIModel departmentsUIModel) {
        this.departmentsUIModel = departmentsUIModel;
        this.departmentsUIModel.setParentModel(this);
    }

    /**
     * <p>Getter for the field <code>controlRuleUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.ControlRuleTableUIModel} object.
     */
    public ControlRuleTableUIModel getControlRuleUIModel() {
        return controlRuleUIModel;
    }

    /**
     * <p>Setter for the field <code>controlRuleUIModel</code>.</p>
     *
     * @param controlRuleUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.ControlRuleTableUIModel} object.
     */
    public void setControlRuleUIModel(ControlRuleTableUIModel controlRuleUIModel) {
        this.controlRuleUIModel = controlRuleUIModel;
        this.controlRuleUIModel.setParentModel(this);
    }

    /**
     * <p>Getter for the field <code>pmfmUIModel</code>.</p>
     *
     * @return a {@link ControlPmfmTableUIModel} object.
     */
    public ControlPmfmTableUIModel getPmfmUIModel() {
        return pmfmUIModel;
    }

    /**
     * <p>Setter for the field <code>pmfmUIModel</code>.</p>
     *
     * @param pmfmUIModel a {@link ControlPmfmTableUIModel} object.
     */
    public void setPmfmUIModel(ControlPmfmTableUIModel pmfmUIModel) {
        this.pmfmUIModel = pmfmUIModel;
        this.pmfmUIModel.setParentModel(this);
    }

    public RuleListRowModel getSelectedRuleList() {
        return selectedRuleList;
    }

    public void setSelectedRuleList(RuleListRowModel selectedRuleList) {
        this.selectedRuleList = selectedRuleList;
    }

    public boolean isEditable() {
        return userIsAdmin && selectedRuleList != null && selectedRuleList.isEditable();
    }

    public boolean isUserIsAdmin() {
        return userIsAdmin;
    }

    public void setUserIsAdmin(boolean userIsAdmin) {
        this.userIsAdmin = userIsAdmin;
    }

    public boolean isManagedProgramsAvailable() {
        return managedProgramsAvailable;
    }

    public void setManagedProgramsAvailable(boolean managedProgramsAvailable) {
        this.managedProgramsAvailable = managedProgramsAvailable;
    }

    public String getRuleMessage() {
        return ruleMessage;
    }

    public void setRuleMessage(String ruleMessage) {
        String oldValue = getRuleMessage();
        this.ruleMessage = ruleMessage;
        firePropertyChange(PROPERTY_RULE_MESSAGE, oldValue, ruleMessage);
    }

    public String getRuleDescription() {
        return ruleDescription;
    }

    public void setRuleDescription(String ruleDescription) {
        String oldValue = getRuleDescription();
        this.ruleDescription = ruleDescription;
        firePropertyChange(PROPERTY_RULE_DESCRIPTION, oldValue, ruleDescription);
    }

    public boolean isSaveEnabled() {
        return saveEnabled && isManagedProgramsAvailable();
    }

    public void setSaveEnabled(boolean saveEnabled) {
        this.saveEnabled = saveEnabled;
        firePropertyChange(PROPERTY_SAVE_ENABLED, null, saveEnabled);
    }
}
