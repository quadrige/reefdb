package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.PhotoTypeDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des photos.
 */
public class PhotosTableModel extends AbstractReefDbTableModel<PhotosTableRowModel> {

    /**
     * Identifiant pour la colonne mnemonique.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.photo.name.tip"),
            String.class,
            true);
    
    /**
     * Identifiant pour la colonne type.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> TYPE = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_PHOTO_TYPE,
            n("reefdb.photo.type"),
            n("reefdb.photo.type.tip"),
            PhotoTypeDTO.class);
    
    /**
     * Identifiant pour la colonne legende.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> CAPTION = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_CAPTION,
            n("reefdb.photo.caption"),
            n("reefdb.photo.caption.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne date.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> DATE = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_DATE,
            n("reefdb.property.date"),
            n("reefdb.photo.date.tip"),
            Date.class,
            true);
    
    /**
     * Identifiant pour la colonne prelevement.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> SAMPLING_OPERATION = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_SAMPLING_OPERATION,
            n("reefdb.property.samplingOperation"),
            n("reefdb.photo.samplingOperation.tip"),
            SamplingOperationDTO.class,
            DecoratorService.CONCAT);
    
    /**
     * Identifiant pour la colonne direction.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> DIRECTION = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_DIRECTION,
            n("reefdb.photo.direction"),
            n("reefdb.photo.direction.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne chemin physique.
     */
    public static final ReefDbColumnIdentifier<PhotosTableRowModel> PATH = ReefDbColumnIdentifier.newId(
    		PhotosTableRowModel.PROPERTY_PATH,
            n("reefdb.photo.path"),
            n("reefdb.photo.path.tip"),
            String.class);

    private boolean readOnly;

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public PhotosTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
        this.readOnly = false;
    }

    /**
     * <p>Setter for the field <code>readOnly</code>.</p>
     *
     * @param readOnly a boolean.
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /** {@inheritDoc} */
    @Override
	public PhotosTableRowModel createNewRow() {
		return new PhotosTableRowModel(readOnly);
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<PhotosTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
