package fr.ifremer.reefdb.ui.swing.content.manage.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.exception.SaveForbiddenException;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbRemoteSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.program.menu.ClearAction;
import fr.ifremer.reefdb.ui.swing.content.manage.program.menu.SearchAction;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action save a program.
 */
public class SaveAction extends AbstractReefDbRemoteSaveAction<ProgramsUIModel, ProgramsUI, ProgramsUIHandler> {

    private List<ProgramsTableRowModel> programsToSave;

    private List<ProgramDTO> reloadedPrograms;

    /**
     * constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ProgramsUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {

        if (!super.prepareAction()) {
            return false;
        }

        // Displayed programs
        programsToSave = getModel().getProgramsUIModel().getRows();

        return true;
    }

    @Override
    protected void doSave() {

        getContext().getProgramStrategyService().savePrograms(getContext().getAuthenticationInfo(), programsToSave);

    }

    @Override
    protected void reload() {

        reloadedPrograms = getUI().getMenuUI().getModel().isOnlyManagedPrograms()
            ? getContext().getProgramStrategyService().getManagedPrograms()
            : getContext().getProgramStrategyService().getReadablePrograms();

    }

    @Override
    protected void onSaveForbiddenException(SaveForbiddenException exception) {

        if (exception.getType() == SaveForbiddenException.Type.PERMISSION) {

            if (CollectionUtils.isNotEmpty(exception.getObjectIds())) {
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.action.save.programs.forbidden.topMessage"),
                        ApplicationUIUtil.getHtmlString(exception.getObjectIds()),
                        t("reefdb.action.save.programs.forbidden.bottomMessage"),
                        t("reefdb.action.save.errors.title"));
            } else {
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.action.save.programs.forbidden.message"),
                        t("reefdb.action.save.errors.title"));
            }

        } else if (exception.getType() == SaveForbiddenException.Type.ATTACHED_DATA) {

            if (CollectionUtils.isNotEmpty(exception.getObjectIds())) {
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.action.save.programs.forbidden.attachedData.topMessage"),
                        ApplicationUIUtil.getHtmlString(exception.getObjectIds()),
                        t("reefdb.action.save.programs.forbidden.attachedData.bottomMessage"),
                        t("reefdb.action.save.errors.title"));
            } else {
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.action.save.programs.forbidden.attachedData.message"),
                        t("reefdb.action.save.errors.title"));
            }

        } else {

            // if no information on Type, throw it as is
            throw exception;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // don't finish action if remote save is aborted (Mantis #48235)
        if (isSaveAborted())
            return;

        // Reload list with saved data
        getUI().getMenuUI().getProgramMnemonicCombo().setData(reloadedPrograms);
        getUI().getMenuUI().getProgramCodeCombo().setData(reloadedPrograms);

        getModel().setModify(false);

        // Don't search again here because if this action comes from a CheckModelAction, the search will be performed by it (Mantis #46633)
        if (!isFromCheckModelAction()) {

            // After removing the user itself from program's managers, which is selected in menu combo, clear all (Mantis #59226)
            String selectedProgramCode = getUI().getMenuUI().getModel().getProgramCode();
            if (StringUtils.isNotBlank(selectedProgramCode) && reloadedPrograms.stream().noneMatch(programDTO -> programDTO.getCode().equals(selectedProgramCode))) {
                getActionEngine().runInternalAction(getUI().getMenuUI().getHandler(), ClearAction.class);
            } else {
                // Just re-run search
                getActionEngine().runInternalAction(getUI().getMenuUI().getHandler(), SearchAction.class);
            }

            // if selected program, reload strategies
            getHandler().reselectProgram();
        }

    }

    @Override
    protected void releaseAction() {
        super.releaseAction();

        programsToSave = null;
        reloadedPrograms = null;
    }

}
