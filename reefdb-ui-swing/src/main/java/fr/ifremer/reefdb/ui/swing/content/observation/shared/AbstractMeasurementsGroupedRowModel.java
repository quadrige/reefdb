package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.ui.core.dto.CommentAware;
import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author peck7 on 08/01/2019.
 */
public abstract class AbstractMeasurementsGroupedRowModel<B extends MeasurementDTO, R extends AbstractMeasurementsGroupedRowModel<B, R>>
    extends AbstractReefDbRowUIModel<B, R> implements CommentAware, MeasurementAware, ErrorAware {

    public static String PROPERTY_INDIVIDUAL_ID = "individualId";
    public static String PROPERTY_TAXON = "taxon";
    public static String PROPERTY_INPUT_TAXON_ID = "inputTaxonId";
    public static String PROPERTY_INPUT_TAXON_NAME = "inputTaxonName";
    public static String PROPERTY_TAXON_GROUP = "taxonGroup";
    public static String PROPERTY_COMMENT = "comment";
    public static String PROPERTY_ANALYST = "analyst";
    public static String PROPERTY_INDIVIDUAL_PMFMS = "individualPmfms";
    public static String PROPERTY_INDIVIDUAL_MEASUREMENTS = "individualMeasurements";

    private Integer individualId;
    private TaxonDTO taxon;
    private Integer inputTaxonId;
    private String inputTaxonName;
    private TaxonGroupDTO taxonGroup;

    private String comment;
    private DepartmentDTO analyst;
    private List<PmfmDTO> individualPmfms;
    private List<MeasurementDTO> individualMeasurements;
    private final List<ErrorDTO> errors;
    private final boolean readOnly;

    private final AllowedQualitativeValuesMap allowedQualitativeValuesMap;
    private final List<ErrorDTO> preconditionErrors;

    // used with multi edit model
    private List<ColumnIdentifier<? extends R>> multipleValuesOnIdentifier;
    private List<Integer> multipleValuesOnPmfmIds;

    /**
     * <p>Constructor for AbstractReefDbRowUIModel.</p>
     */
    public AbstractMeasurementsGroupedRowModel(boolean readOnly) {
        super(null, null);
        this.individualMeasurements = new ArrayList<>();
        this.errors = new ArrayList<>();
        this.preconditionErrors = new ArrayList<>();
        this.allowedQualitativeValuesMap = new AllowedQualitativeValuesMap();
        this.multipleValuesOnIdentifier = new ArrayList<>();
        this.multipleValuesOnPmfmIds = new ArrayList<>();
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEditable() {
        return !readOnly && super.isEditable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected B newBean() {
        return null;
    }

    /**
     * <p>Getter for the field <code>individualId</code>.</p>
     *
     * @return the indivualId
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * <p>Setter for the field <code>individualId</code>.</p>
     *
     * @param individualId the indivualId to set
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * <p>Getter for the field <code>taxonGroup</code>.</p>
     *
     * @return the groupTaxon
     */
    public TaxonGroupDTO getTaxonGroup() {
        return taxonGroup;
    }

    /**
     * <p>Setter for the field <code>taxonGroup</code>.</p>
     *
     * @param taxonGroup the taxonGroup to set
     */
    public void setTaxonGroup(TaxonGroupDTO taxonGroup) {
        TaxonGroupDTO oldValue = this.taxonGroup;
        this.taxonGroup = taxonGroup;
        firePropertyChange(PROPERTY_TAXON_GROUP, oldValue, taxonGroup);
    }

    /**
     * <p>Getter for the field <code>taxon</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public TaxonDTO getTaxon() {
        return taxon;
    }

    /**
     * <p>Setter for the field <code>taxon</code>.</p>
     *
     * @param taxon a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public void setTaxon(TaxonDTO taxon) {
        TaxonDTO oldValue = this.taxon;
        this.taxon = taxon;
        firePropertyChange(PROPERTY_TAXON, oldValue, taxon);
    }

    public Integer getInputTaxonId() {
        return inputTaxonId;
    }

    public void setInputTaxonId(Integer inputTaxonId) {
        Integer oldValue = this.inputTaxonId;
        this.inputTaxonId = inputTaxonId;
        firePropertyChange(PROPERTY_INPUT_TAXON_ID, oldValue, inputTaxonId);
    }

    public String getInputTaxonName() {
        return inputTaxonName;
    }

    public void setInputTaxonName(String inputTaxonName) {
        String oldValue = this.inputTaxonName;
        this.inputTaxonName = inputTaxonName;
        firePropertyChange(PROPERTY_INPUT_TAXON_NAME, oldValue, inputTaxonName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getComment() {
        return comment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setComment(String comment) {
        String oldValue = this.comment;
        this.comment = comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    /**
     * <p>Getter for the field <code>analyst</code>.</p>
     *
     * @return the analyst
     */
    public DepartmentDTO getAnalyst() {
        return analyst;
    }

    /**
     * <p>Setter for the field <code>analyst</code>.</p>
     *
     * @param analyst the analyst to set
     */
    public void setAnalyst(DepartmentDTO analyst) {
        DepartmentDTO oldValue = this.analyst;
        this.analyst = analyst;
        firePropertyChange(PROPERTY_ANALYST, oldValue, analyst);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfms() {
        return getIndividualPmfms();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MeasurementDTO> getMeasurements() {
        return getIndividualMeasurements();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getIndividualPmfms() {
        return individualPmfms;
    }

    /**
     * <p>Setter for the field <code>individualPmfms</code>.</p>
     *
     * @param individualPmfms a {@link java.util.List} object.
     */
    public void setIndividualPmfms(List<PmfmDTO> individualPmfms) {
        List<PmfmDTO> oldValue = this.individualPmfms;
        this.individualPmfms = individualPmfms;
        firePropertyChange(PROPERTY_INDIVIDUAL_PMFMS, oldValue, individualPmfms);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MeasurementDTO> getIndividualMeasurements() {
        return individualMeasurements;
    }

    /**
     * <p>Setter for the field <code>individualMeasurements</code>.</p>
     *
     * @param individualMeasurements a {@link java.util.List} object.
     */
    public void setIndividualMeasurements(List<MeasurementDTO> individualMeasurements) {
        List<MeasurementDTO> oldValue = this.individualMeasurements;
        this.individualMeasurements = individualMeasurements;
        firePropertyChange(PROPERTY_INDIVIDUAL_MEASUREMENTS, oldValue, individualMeasurements);
    }

    public AllowedQualitativeValuesMap getAllowedQualitativeValuesMap() {
        return allowedQualitativeValuesMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    public List<ErrorDTO> getPreconditionErrors() {
        return preconditionErrors;
    }

    public List<ColumnIdentifier<? extends R>> getMultipleValuesOnIdentifier() {
        return multipleValuesOnIdentifier;
    }

    public void addMultipleValuesOnIdentifier(ColumnIdentifier<? extends R> identifier) {
        multipleValuesOnIdentifier.add(identifier);
    }

    public List<Integer> getMultipleValuesOnPmfmIds() {
        return multipleValuesOnPmfmIds;
    }

    public void addMultipleValuesOnPmfmId(Integer pmfmId) {
        multipleValuesOnPmfmIds.add(pmfmId);
    }

    public boolean isSameRow(AbstractMeasurementsGroupedRowModel<?, ?> that) {
        return Objects.equals(this.getTaxonGroup(), that.getTaxonGroup())
            && Objects.equals(this.getTaxon(), that.getTaxon());
    }

    public List<String> getDefaultProperties() {
        return Lists.newArrayList(PROPERTY_TAXON_GROUP, PROPERTY_TAXON);
    }

    public boolean hasTaxonInformation() {
        return getTaxonGroup() != null || getTaxon() != null;
    }

    public boolean hasNonEmptyMeasurements() {
        return getIndividualMeasurements().stream().anyMatch(measurement -> !ReefDbBeans.isMeasurementEmpty(measurement));
    }


    /**
     * Return a unique hashCode for the row
     *
     * @return hashCode
     */
    public String rowWithMeasurementsHashCode() {
        return rowWithMeasurementsHashCode(x -> true);
    }

    /**
     * Return a unique hashCode for the row
     *
     * @return hashCode
     */
    public String rowWithMeasurementsHashCode(Collection<PmfmDTO> filterPmfms) {
        return rowWithMeasurementsHashCode(measurement -> filterPmfms.contains(measurement.getPmfm()));
    }

    /**
     * Return a unique hashCode for the row with specified filter
     *
     * @return hashCode
     */
    private String rowWithMeasurementsHashCode(Predicate<MeasurementDTO> filterPredicate) {
        StringBuilder sb = new StringBuilder();
        sb.append(getTaxonGroup() != null ? getTaxonGroup().getId() : null).append('|');
        sb.append(getTaxon() != null ? getTaxon().getId() : null).append('|');
        if (getIndividualMeasurements() != null) {
            sb.append(
                getIndividualMeasurements().stream()
                    .filter(filterPredicate)
                    .filter(measurement -> !ReefDbBeans.isMeasurementEmpty(measurement))
                    .sorted(Comparator.comparing(o -> o.getPmfm().getId()))
                    .map(measurement -> measurement.getPmfm().getId().toString() + ':' +
                        (measurement.getPmfm().getParameter().isQualitative()
                            ? String.valueOf(measurement.getQualitativeValue() != null ? measurement.getQualitativeValue().getId() : null)
                            : String.valueOf(measurement.getNumericalValue())))
                    .collect(Collectors.joining("|"))
            );
        } else {
            sb.append('N');
        }
        return sb.toString();
    }
}
