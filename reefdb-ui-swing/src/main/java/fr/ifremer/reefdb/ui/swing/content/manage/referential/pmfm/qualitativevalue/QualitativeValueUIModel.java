package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Modele pour la zone des prelevements de l'ecran d'accueil.
 */
public class QualitativeValueUIModel extends AbstractReefDbTableUIModel<QualitativeValueDTO, QualitativeValueTableRowModel, QualitativeValueUIModel> {

    private boolean editable;
    public static final String PROPERTY_EDITABLE = "editable";

    private AbstractReefDbRowUIModel parentRowModel = null;
    /** Constant <code>PROPERTY_PARENT_ROW_MODEL="parentRowModel"</code> */
    public static final String PROPERTY_PARENT_ROW_MODEL = "parentRowModel";

    private boolean doubleList;
    /** Constant <code>PROPERTY_DOUBLE_LIST="doubleList"</code> */
    public static final String PROPERTY_DOUBLE_LIST = "doubleList";

    /**
     * Constructor.
     */
    public QualitativeValueUIModel() {
        super();
    }

    /**
     * <p>isLocal.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>local</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }

    /**
     * <p>Getter for the field <code>parentRowModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel} object.
     */
    public AbstractReefDbRowUIModel getParentRowModel() {
        return parentRowModel;
    }

    /**
     * <p>Setter for the field <code>parentRowModel</code>.</p>
     *
     * @param parentRowModel a {@link fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel} object.
     */
    public void setParentRowModel(AbstractReefDbRowUIModel parentRowModel) {
        this.parentRowModel = parentRowModel;
        firePropertyChange(PROPERTY_PARENT_ROW_MODEL, null, parentRowModel);
    }

    /**
     * <p>isDoubleList.</p>
     *
     * @return a boolean.
     */
    public boolean isDoubleList() {
        return doubleList;
    }

    /**
     * <p>Setter for the field <code>doubleList</code>.</p>
     *
     * @param doubleList a boolean.
     */
    public void setDoubleList(boolean doubleList) {
        this.doubleList = doubleList;
        firePropertyChange(PROPERTY_DOUBLE_LIST, null, doubleList);
    }
}
