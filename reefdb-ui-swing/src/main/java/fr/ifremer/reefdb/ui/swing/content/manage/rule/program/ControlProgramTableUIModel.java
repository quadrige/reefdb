package fr.ifremer.reefdb.ui.swing.content.manage.rule.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model pour le tableau des observations.
 */
public class ControlProgramTableUIModel extends AbstractReefDbTableUIModel<ProgramDTO, ControlProgramTableRowModel, ControlProgramTableUIModel> {

    private RulesUIModel parentModel;

	/**
	 * Constructor.
	 */
	public ControlProgramTableUIModel() {
		super();
	}

	/**
	 * <p>Getter for the field <code>parentModel</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel} object.
	 */
	public RulesUIModel getParentModel() {
		return parentModel;
	}

	/**
	 * <p>Setter for the field <code>parentModel</code>.</p>
	 *
	 * @param parentModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel} object.
	 */
	public void setParentModel(RulesUIModel parentModel) {
		this.parentModel = parentModel;
	}
}
