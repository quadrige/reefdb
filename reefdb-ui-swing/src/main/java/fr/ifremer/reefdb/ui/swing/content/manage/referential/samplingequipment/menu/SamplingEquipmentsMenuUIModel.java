package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des enginsPrelevement
 */
public class SamplingEquipmentsMenuUIModel extends DefaultReferentialMenuUIModel {

    /** Constant <code>PROPERTY_SAMPLING_EQUIPMENT="samplingEquipment"</code> */
    public static final String PROPERTY_SAMPLING_EQUIPMENT = "samplingEquipment";
    private SamplingEquipmentDTO samplingEquipment;

    /**
     * <p>Getter for the field <code>samplingEquipment</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO} object.
     */
    public SamplingEquipmentDTO getSamplingEquipment() {
        return samplingEquipment;
    }

    /**
     * <p>Setter for the field <code>samplingEquipment</code>.</p>
     *
     * @param samplingEquipment a {@link fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO} object.
     */
    public void setSamplingEquipment(SamplingEquipmentDTO samplingEquipment) {
        this.samplingEquipment = samplingEquipment;
        firePropertyChange(PROPERTY_SAMPLING_EQUIPMENT, null, samplingEquipment);
    }

    /**
     * <p>getSamplingEquipmentId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSamplingEquipmentId() {
        return getSamplingEquipment() != null ? getSamplingEquipment().getId() : null;
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setSamplingEquipment(null);
    }
}
