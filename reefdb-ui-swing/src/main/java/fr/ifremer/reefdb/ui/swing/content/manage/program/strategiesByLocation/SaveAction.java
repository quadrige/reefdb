package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;

import java.util.List;

/**
 * Action save a strategy of location
 */
public class SaveAction extends AbstractReefDbSaveAction<StrategiesLieuxUIModel, StrategiesLieuxUI, StrategiesLieuxUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final StrategiesLieuxUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        return super.prepareAction() && getModel().isModify() && getModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // get location
        LocationDTO location = getUI().getLieuxProgrammeTableUI().getModel().getSingleSelectedRow();
        Assert.notNull(location);

        // get beans
        List<ProgStratDTO> toSave = getUI().getStrategiesProgrammeTableUI().getModel().getBeans();

        getContext().getProgramStrategyService().saveStrategiesByProgramAndLocation(toSave, location.getId());

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getModel().getTableUIModel().setModify(false);
        getModel().setModify(false);
    }
}
