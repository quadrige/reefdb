package fr.ifremer.reefdb.ui.swing.content.manage.rule.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Model for RuleLists
 */
public class RulesMenuUIModel extends DefaultReferentialMenuUIModel {

    public static final String PROPERTY_RULE_LIST = "ruleList";
    public static final String PROPERTY_PROGRAM = "program";
    private RuleListDTO ruleList;
    private ProgramDTO program;

    public RuleListDTO getRuleList() {
        return ruleList;
    }

    public void setRuleList(RuleListDTO ruleList) {
        this.ruleList = ruleList;
        firePropertyChange(PROPERTY_RULE_LIST, null, ruleList);
    }

    public String getRuleListCode() {
        return getRuleList() != null ? getRuleList().getCode() : null;
    }

    public ProgramDTO getProgram() {
        return program;
    }

    public String getProgramCode() {
        return getProgram() != null ? getProgram().getCode() : null;
    }

    public void setProgram(ProgramDTO program) {
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, null, program);
    }

    @Override
    public void clear() {
        super.clear();
        setRuleList(null);
        setProgram(null);
    }
}
