package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Modele pour la zone des lieux.
 */
public class LieuxProgrammeTableUIModel extends AbstractReefDbTableUIModel<LocationDTO, LieuxProgrammeTableRowModel, LieuxProgrammeTableUIModel> {
	
    /**
     * Constructor.
     */
    public LieuxProgrammeTableUIModel() {
        super();
    }
    
    /**
     * Programme.
     */
    private ProgramDTO programme;

	/**
	 * <p>Getter for the field <code>programme</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
	 */
	public ProgramDTO getProgramme() {
		return programme;
	}

	/**
	 * <p>Setter for the field <code>programme</code>.</p>
	 *
	 * @param programme a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
	 */
	public void setProgramme(ProgramDTO programme) {
		this.programme = programme;
	}
}
