package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.taxon.TaxonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.TaxonomicLevelDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele du menu pour la gestion des taxons au niveau National
 */
public class TaxonMenuUIModel extends AbstractReferentialMenuUIModel<TaxonCriteriaDTO, TaxonMenuUIModel> implements TaxonCriteriaDTO {

    private static final Binder<TaxonMenuUIModel, TaxonCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(TaxonMenuUIModel.class, TaxonCriteriaDTO.class);

    private static final Binder<TaxonCriteriaDTO, TaxonMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(TaxonCriteriaDTO.class, TaxonMenuUIModel.class);

    /**
     * Constructor.
     */
    public TaxonMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected TaxonCriteriaDTO newBean() {
        return ReefDbBeanFactory.newTaxonCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public TaxonomicLevelDTO getLevel() {
        return delegateObject.getLevel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLevel(TaxonomicLevelDTO level) {
        delegateObject.setLevel(level);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFullProperties() {
        return delegateObject.isFullProperties();
    }

    /** {@inheritDoc} */
    @Override
    public void setFullProperties(boolean fullProperties) {
        delegateObject.setFullProperties(fullProperties);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setLevel(null);
    }
}
