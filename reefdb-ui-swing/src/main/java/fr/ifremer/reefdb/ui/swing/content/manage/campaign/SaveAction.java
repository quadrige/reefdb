package fr.ifremer.reefdb.ui.swing.content.manage.campaign;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbRemoteSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.menu.SearchAction;

/**
 * Action save a program.
 */
public class SaveAction extends AbstractReefDbRemoteSaveAction<CampaignsUIModel, CampaignsUI, CampaignsUIHandler> {

    /**
     * constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final CampaignsUIHandler handler) {
        super(handler, false);
    }

    @Override
    protected void doSave() {

        getContext().getCampaignService().saveCampaigns(getContext().getAuthenticationInfo(), getModel().getCampaignsTableUIModel().getRows());

    }

    @Override
    protected void reload() {

        getActionEngine().runInternalAction(getUI().getMenuUI().getHandler(), SearchAction.class);

    }
}
