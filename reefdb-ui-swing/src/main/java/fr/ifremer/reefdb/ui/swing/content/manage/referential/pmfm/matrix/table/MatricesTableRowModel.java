package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.*;

/**
 * <p>MatricesTableRowModel class.</p>
 *
 * @author Antoine
 */
public class MatricesTableRowModel extends AbstractReefDbRowUIModel<MatrixDTO, MatricesTableRowModel> implements MatrixDTO, ErrorAware {

    private static final Binder<MatrixDTO, MatricesTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(MatrixDTO.class, MatricesTableRowModel.class);

    private static final Binder<MatricesTableRowModel, MatrixDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(MatricesTableRowModel.class, MatrixDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for MatricesTableRowModel.</p>
     */
    public MatricesTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected MatrixDTO newBean() {
        return ReefDbBeanFactory.newMatrixDTO();
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String mnemonique) {
        delegateObject.setName(mnemonique);
    }

    /** {@inheritDoc} */
    @Override
    public FractionDTO getFractions(int index) {
        return delegateObject.getFractions(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFractionsEmpty() {
        return delegateObject.isFractionsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeFractions() {
        return delegateObject.sizeFractions();
    }

    /** {@inheritDoc} */
    @Override
    public void addFractions(FractionDTO Fractions) {
        delegateObject.addFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllFractions(Collection<FractionDTO> Fractions) {
        delegateObject.addAllFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeFractions(FractionDTO Fractions) {
        return delegateObject.removeFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllFractions(Collection<FractionDTO> Fractions) {
        return delegateObject.removeAllFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsFractions(FractionDTO Fractions) {
        return delegateObject.containsFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllFractions(Collection<FractionDTO> Fractions) {
        return delegateObject.containsAllFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public Set<FractionDTO> getFractions() {
        return delegateObject.getFractions();
    }

    /** {@inheritDoc} */
    @Override
    public void setFractions(Set<FractionDTO> Fractions) {
        delegateObject.setFractions(Fractions);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
