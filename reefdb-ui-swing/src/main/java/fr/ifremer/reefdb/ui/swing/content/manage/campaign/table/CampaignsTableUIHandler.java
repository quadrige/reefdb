package fr.ifremer.reefdb.ui.swing.content.manage.campaign.table;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class CampaignsTableUIHandler extends AbstractReefDbTableUIHandler<CampaignsTableRowModel, CampaignsTableUIModel, CampaignsTableUI> {

    /**
     * <p>Constructor for CampaignsTableUIHandler.</p>
     */
    public CampaignsTableUIHandler() {
        super(CampaignsTableRowModel.PROPERTY_NAME,
                CampaignsTableRowModel.PROPERTY_SISMER_LINK,
                CampaignsTableRowModel.PROPERTY_START_DATE,
                CampaignsTableRowModel.PROPERTY_END_DATE,
                CampaignsTableRowModel.PROPERTY_MANAGER,
                CampaignsTableRowModel.PROPERTY_COMMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{CampaignsTableRowModel.PROPERTY_ERRORS};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<CampaignsTableRowModel> getTableModel() {
        return (CampaignsTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getCampaignsTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final CampaignsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final CampaignsTableUIModel model = new CampaignsTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(CampaignsTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initTable();

        // Ajout des listeners
        initListeners();

        getModel().setModify(false);
        recomputeRowsValidState(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRowValid(final CampaignsTableRowModel row) {
        row.getErrors().clear();
        return !row.isEditable() || (super.isRowValid(row) && isCampaignValid(row));
    }

    private boolean isCampaignValid(CampaignsTableRowModel row) {

        // check for name duplicates in table
        boolean nameDuplicateInTable = false;
        for (CampaignsTableRowModel otherRow: getModel().getRows()) {
            if (otherRow == row) continue;
            if (otherRow.getName().equals(row.getName())) {
                ReefDbBeans.addError(row, t("reefdb.campaign.name.duplicates"), CampaignsTableRowModel.PROPERTY_NAME);
                nameDuplicateInTable = true;
                break;
            }
        }
        // check for name duplicate in database
        if (!nameDuplicateInTable && row.isDirty()) {
            if (getContext().getCampaignService().checkCampaignNameDuplicates(row.getId(), row.getName())) {
                ReefDbBeans.addError(row, t("reefdb.campaign.name.duplicates"), CampaignsTableRowModel.PROPERTY_NAME);
            }
        }

        // check end date
        if (row.getStartDate() != null && row.getEndDate() != null && row.getEndDate().isBefore(row.getStartDate())) {
            ReefDbBeans.addError(row, t("reefdb.campaign.endDate.before"), CampaignsTableRowModel.PROPERTY_END_DATE);
        }

        return row.isErrorsEmpty();
    }

    @Override
    protected void onRowsAdded(List<CampaignsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            CampaignsTableRowModel newRow = addedRows.get(0);

            setFocusOnCell(newRow);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, CampaignsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Add listener on saveEnabled property to disable change (Mantis #47532)
        getModel().addPropertyChangeListener(CampaignsTableUIModel.PROPERTY_SAVE_ENABLED, evt -> {
            getModel().getRows().forEach(campaignsTableRowModel -> campaignsTableRowModel.setEditable(getModel().isSaveEnabled()));
            getUI().getCampaignsTable().invalidate();
            getUI().getCampaignsTable().repaint();
        });

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Column name
        final TableColumnExt columnName = addColumn(
                CampaignsTableModel.NAME);
        columnName.setSortable(true);

        // Column sismer
        final TableColumnExt columnSismer = addColumn(
                CampaignsTableModel.SISMER_LINK);
        columnSismer.setSortable(true);

        final TableColumnExt columnStartDate = addLocalDatePickerColumnToModel(
                CampaignsTableModel.START_DATE,
                getConfig().getDateFormat(),
                true
        );
        columnStartDate.setSortable(true);

        final TableColumnExt columnEndDate = addLocalDatePickerColumnToModel(
                CampaignsTableModel.END_DATE,
                getConfig().getDateFormat(),
                true
        );
        columnEndDate.setSortable(true);

        final TableColumnExt columnRecorderPerson = addExtendedComboDataColumnToModel(
                CampaignsTableModel.MANAGER,
                getContext().getUserService().getActiveUsers(),
                false
        );
        columnRecorderPerson.setSortable(true);

        // Column comment
        addCommentColumn(CampaignsTableModel.COMMENT);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(CampaignsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // Modele de la table
        final CampaignsTableModel tableModel = new CampaignsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Les columns obligatoire sont toujours presentes
        columnName.setHideable(false);
        columnStartDate.setHideable(false);
        columnRecorderPerson.setHideable(false);

        // Initialisation de la table
        initTable(getTable());

        // Tri par defaut
        getTable().setSortOrder(CampaignsTableModel.NAME, SortOrder.ASCENDING);
    }
}
