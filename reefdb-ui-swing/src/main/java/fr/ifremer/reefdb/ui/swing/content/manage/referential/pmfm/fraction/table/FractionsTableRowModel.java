package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.*;

/**
 * <p>FractionsTableRowModel class.</p>
 *
 * @author Antoine
 */
public class FractionsTableRowModel extends AbstractReefDbRowUIModel<FractionDTO, FractionsTableRowModel> implements FractionDTO, ErrorAware {

    private static final Binder<FractionDTO, FractionsTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(FractionDTO.class, FractionsTableRowModel.class);

    private static final Binder<FractionsTableRowModel, FractionDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(FractionsTableRowModel.class, FractionDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for FractionsTableRowModel.</p>
     */
    public FractionsTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected FractionDTO newBean() {
        return ReefDbBeanFactory.newFractionDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String mnemonique) {
        delegateObject.setName(mnemonique);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public MatrixDTO getMatrixes(int index) {
        return delegateObject.getMatrixes(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMatrixesEmpty() {
        return delegateObject.isMatrixesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeMatrixes() {
        return delegateObject.sizeMatrixes();
    }

    /** {@inheritDoc} */
    @Override
    public void addMatrixes(MatrixDTO Matrixes) {
        delegateObject.addMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllMatrixes(
            Collection<MatrixDTO> Matrixes) {
        delegateObject.addAllMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeMatrixes(MatrixDTO Matrixes) {
        return delegateObject.removeMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllMatrixes(
            Collection<MatrixDTO> Matrixes) {
        return delegateObject.removeAllMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsMatrixes(MatrixDTO Matrixes) {
        return delegateObject.containsMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllMatrixes(
            Collection<MatrixDTO> Matrixes) {
        return delegateObject.containsAllMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public Set<MatrixDTO> getMatrixes() {
        return delegateObject.getMatrixes();
    }

    /** {@inheritDoc} */
    @Override
    public void setMatrixes(Set<MatrixDTO> Matrixes) {
        delegateObject.setMatrixes(Matrixes);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
