package fr.ifremer.reefdb.ui.swing.content.extraction.filters;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.swing.table.renderer.CollectionDecoratorCellRenderer;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import org.apache.commons.collections4.CollectionUtils;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import java.util.List;

/**
 * Specific cell renderer for extraction filters
 * Created by Ludovic on 08/12/2015.
 */
public class FilterElementsCellRenderer extends DefaultTableCellRenderer {

    private static final String SEPARATOR = "; ";

    private final CollectionDecoratorCellRenderer periodRenderer;
    private final CollectionDecoratorCellRenderer programRenderer;
    private final CollectionDecoratorCellRenderer campaignRenderer;
    private final CollectionDecoratorCellRenderer locationRenderer;
    private final CollectionDecoratorCellRenderer taxonRenderer;
    private final CollectionDecoratorCellRenderer taxonGroupRenderer;
    private final CollectionDecoratorCellRenderer departmentRenderer;
    private final CollectionDecoratorCellRenderer pmfmRenderer;


    /**
     * <p>Constructor for FilterElementsCellRenderer.</p>
     *
     * @param decoratorService a {@link fr.ifremer.reefdb.decorator.DecoratorService} object.
     */
    public FilterElementsCellRenderer(DecoratorService decoratorService) {
        periodRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(ExtractionPeriodDTO.class), SEPARATOR);
        programRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(ProgramDTO.class, DecoratorService.CODE), SEPARATOR);
        campaignRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(CampaignDTO.class), SEPARATOR);
        locationRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(LocationDTO.class), SEPARATOR);
        taxonRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(TaxonDTO.class), SEPARATOR);
        taxonGroupRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(TaxonGroupDTO.class), SEPARATOR);
        departmentRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(DepartmentDTO.class), SEPARATOR);
        pmfmRenderer = new CollectionDecoratorCellRenderer(decoratorService.getDecoratorByType(PmfmDTO.class, DecoratorService.NAME), SEPARATOR);
    }

    /** {@inheritDoc} */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        if (value instanceof FilterDTO) {
            CollectionDecoratorCellRenderer renderer = null;
            List<? extends QuadrigeBean> elements = ((FilterDTO) value).getElements();
            if (CollectionUtils.isNotEmpty(elements)) {
                // get the element type
                QuadrigeBean firstElement = elements.get(0);
                if (firstElement instanceof ExtractionPeriodDTO) {
                    renderer = periodRenderer;
                } else if (firstElement instanceof ProgramDTO) {
                    renderer = programRenderer;
                } else if (firstElement instanceof CampaignDTO) {
                    renderer = campaignRenderer;
                } else if (firstElement instanceof LocationDTO) {
                    renderer = locationRenderer;
                } else if (firstElement instanceof TaxonDTO) {
                    renderer = taxonRenderer;
                } else if (firstElement instanceof TaxonGroupDTO) {
                    renderer = taxonGroupRenderer;
                } else if (firstElement instanceof DepartmentDTO) {
                    renderer = departmentRenderer;
                } else if (firstElement instanceof PmfmDTO) {
                    renderer = pmfmRenderer;
                }

                if (renderer != null) {
                    return renderer.getTableCellRendererComponent(table, elements, isSelected, hasFocus, row, column);
                }
            }
        }

        // default empty value
        return super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);
    }
}
