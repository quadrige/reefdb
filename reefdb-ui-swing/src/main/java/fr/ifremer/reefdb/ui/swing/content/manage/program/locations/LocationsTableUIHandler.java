package fr.ifremer.reefdb.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.locations.updatePeriod.UpdatePeriodUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JOptionPane;
import javax.swing.SortOrder;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class LocationsTableUIHandler extends AbstractReefDbTableUIHandler<LocationsTableRowModel, LocationsTableUIModel, LocationsTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(LocationsTableUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<LocationsTableRowModel> getTableModel() {
        return (LocationsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getLocationsTable();
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final LocationsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final LocationsTableUIModel model = new LocationsTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(LocationsTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau sans les colonnes 'Date debut', 'Date fin' et 'Preleveur'
        getModel().setPeriodsEnabled(false);
        initTable();
        SwingUtil.setLayerUI(ui.getTableScrollPane(), ui.getTableBlockLayer());

        // Initialiser listeners
        initListeners();

        // Initilisation du bouton editer
        initActionComboBox(getUI().getEditCombobox());
//        forceComponentSize(getUI().getEditCombobox());

    }

    /**
     * Load all program's monitoring location
     *
     * @param selectedProgram a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public void loadMonitoringLocationsFromProgram(ProgramsTableRowModel selectedProgram) {

        // Initialiser le tableau sans les colonnes 'Date debut', 'Date fin' et 'Preleveur'
        if (getModel().isPeriodsEnabled()) {
            getModel().setPeriodsEnabled(false);
            initTable();
        }

        getModel().setSelectedProgram(selectedProgram);
        getModel().setSelectedStrategy(null);

        // Chargement des lieux
        load();
    }

    /**
     * Load all applied periods of the selected strategy
     *
     * @param selectedProgram a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     * @param selectedStrategy a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel} object.
     */
    public void loadAppliedStrategiesFromStrategy(ProgramsTableRowModel selectedProgram, StrategiesTableRowModel selectedStrategy) {

        // Initialiser le tableau avec les colonnes 'Date debut', 'Date fin' et 'Preleveur'
        if (!getModel().isPeriodsEnabled()) {
            getModel().setPeriodsEnabled(true);
            initTable();
        }

        getModel().setSelectedProgram(selectedProgram);
        getModel().setSelectedStrategy(selectedStrategy);

        // Chargement des appliedPeriods
        load();
    }

    /**
     * Chargement des lieux final.
     */
    private void load() {
        try {
            // Chargement des lieux dans le model avec le filtrage si besoin
            if (getModel().getSelectedProgram() == null) {
                return;
            }

            getModel().setEditable(getModel().getSelectedProgram().isEditable());

            if (getModel().getSelectedStrategy() == null) {

                // Ajouter tous les lieux
                getModel().setBeans(ReefDbBeans.locationsToAppliedStrategyDTOs(getModel().getSelectedProgram().getLocations()));

            } else {

                // Traitement si la case a cocher est cocher ou non
                if (getUI().getStrategyFilterCheckbox().isSelected()) {

                    // Ajout de la liste des lieux filtres dans le tableau
                    getModel().setBeans(ReefDbBeans.filterNotEmptyAppliedPeriod(getModel().getSelectedStrategy().getAppliedStrategies()));

                } else {

                    // Ajouter tous les lieux
                    getModel().setBeans(getModel().getSelectedStrategy().getAppliedStrategies());
                }
            }

            if (getContext().getSelectedLocationId() != null) {
                selectRowById(getContext().getSelectedLocationId());
            }

            recomputeRowsValidState();
            getModel().setLoaded(true);

        } finally {
            getModel().setLoading(false);
        }
    }

    /**
     * Suppression des lieux.
     */
    public void clearTable() {

        // Suppression des lieux
        getModel().setBeans(null);

        getModel().setLoaded(false);
    }

    /**
     * <p>getProgramsUI.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI} object.
     */
    public ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

    /**
     * <p>addLocations.</p>
     */
    public void addLocations() {

        ProgramsTableRowModel program = getModel().getSelectedProgram(); // getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow();
        StrategiesTableRowModel strategy = getModel().getSelectedStrategy(); // getProgramsUI().getStrategiesTableUI().getModel().getSingleSelectedRow();

        Assert.notNull(program);

        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.LOCATION.getFilterTypeId());
        dialog.setTitle(t("reefdb.program.location.new.dialog.title"));
        List<LocationDTO> locations = program.getLocations().stream()
                .sorted(getDecorator(LocationDTO.class, null).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(location -> location.setReadOnly(true))
                .collect(Collectors.toList());

        dialog.getModel().setSelectedElements(locations);

        if (!program.isLocal()) {
            // if program is national, location menu must search only national locations
            dialog.getHandler().getFilterElementUIHandler().forceLocal(false);
        }

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            // get new location only
            List<LocationDTO> newLocations = dialog.getModel().getSelectedElements().stream()
                    .map(newLocation -> ((LocationDTO) newLocation))
                    .filter(newLocation -> program.getLocations().stream().noneMatch(location -> location.equals(newLocation)))
                    .collect(Collectors.toList());

            if (!newLocations.isEmpty()) {

                // affect to program
                newLocations.forEach(program::addLocations);

                // affect to strategy if selected
                if (strategy != null) {
                    newLocations.stream()
                            // filter by precaution but it should be absent too
                            .filter(newLocation -> strategy.getAppliedStrategies().stream().noneMatch(appliedStrategy -> appliedStrategy.getId().equals(newLocation.getId())))
                            .forEach(newLocation -> strategy.addAppliedStrategies(ReefDbBeans.locationToAppliedStrategyDTO(newLocation)));
                }

                // re-affect lines
                List<AppliedStrategyDTO> beans = strategy != null ? strategy.getAppliedStrategies() : ReefDbBeans.locationsToAppliedStrategyDTOs(program.getLocations());
                getModel().setBeans(beans);

                saveToStrategy();

                // select new lines
                unselectAllRows();
                List<Integer> newLocIds = ReefDbBeans.collectIds(newLocations);
                selectRows(getModel().getRows().stream().filter(row -> newLocIds.contains(row.getId())).collect(Collectors.toList()));

            }
        }
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(LocationsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // save selected location id
            getContext().setSelectedLocationId(getModel().getSingleSelectedRow() == null ? null : getModel().getSingleSelectedRow().getId());

        });

        // Listener sur la checkbox
        getUI().getStrategyFilterCheckbox().addItemListener(e -> {

            // Filtrer les lieux suivant la strategie
            load();
        });
    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // empty previous rows
        getModel().setBeans(null);

        // Force a new column to avoid column duplication
        getTable().setColumnModel(new SwingTableColumnModel());

        // Disabled highlighter
        Highlighter disabledHighlighter = new AbstractHighlighter() {

            @Override
            protected boolean canHighlight(Component component, ComponentAdapter adapter) {
                int modelRow = adapter.convertRowIndexToModel(adapter.row);
                LocationsTableRowModel row = getTableModel().getEntry(modelRow);
                return ReefDbBeans.isDisabledReferential(row);
            }

            @Override
            protected Component doHighlight(Component component, ComponentAdapter adapter) {
                component.setForeground(Color.ORANGE.darker());
                component.setFont(component.getFont().deriveFont(Font.ITALIC));
                return component;
            }
        };

        // Colonne code
        final TableColumnExt colonneCode = addColumn(LocationsTableModel.CODE);
        colonneCode.setSortable(true);
        colonneCode.setCellRenderer(newNumberCellRenderer(0));
        colonneCode.setHighlighters(disabledHighlighter);

        // Colonne name
        final TableColumnExt colonneName = addColumn(LocationsTableModel.LABEL);
        colonneName.setSortable(true);
        colonneName.setHighlighters(disabledHighlighter);

        // Colonne comment
        final TableColumnExt colonneComment = addColumn(LocationsTableModel.NAME);
        colonneComment.setSortable(true);
        colonneComment.setHighlighters(disabledHighlighter);

        // On affiche ces colonnes que si un programme et une strategie sont selectioné
        if (getModel().isPeriodsEnabled()) {

            // Colonne date debut
            final TableColumnExt colonneStartDate = addLocalDatePickerColumnToModel(
                    LocationsTableModel.START_DATE, getConfig().getDateFormat());
            colonneStartDate.setSortable(true);
            colonneStartDate.setHideable(false);

            // Colonne date fin
            final TableColumnExt colonneEndDate = addLocalDatePickerColumnToModel(
                    LocationsTableModel.END_DATE, getConfig().getDateFormat());
            colonneEndDate.setSortable(true);
            colonneEndDate.setHideable(false);

            // Colonne preleveur
            final TableColumnExt colonnePreleveur = addFilterableComboDataColumnToModel(
                    LocationsTableModel.PRELEVEUR,
                    getContext().getReferentialService().getDepartments(getModel().getSelectedProgram().isLocal() ? StatusFilter.ACTIVE : StatusFilter.NATIONAL_ACTIVE),
                    false);
            colonnePreleveur.setSortable(true);
            colonnePreleveur.setHideable(false);
        }

        // Modele de la table
        final LocationsTableModel tableModel = new LocationsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Colonnes non editable
        tableModel.setNoneEditableCols();

        // Les colonnes obligatoire sont toujours presentes
        colonneCode.setHideable(false);
        colonneName.setHideable(false);
        colonneComment.setHideable(false);

        // Les colonnes non editables
        colonneCode.setEditable(false);
        colonneName.setEditable(false);
        colonneComment.setEditable(false);

        // Initialisation de la table
        initTable(getTable());

        // Number rows visible
        getTable().setVisibleRowCount(4);

        // Tri par defaut
        getTable().setSortOrder(LocationsTableModel.CODE, SortOrder.ASCENDING);

        if (getModel().isPeriodsEnabled()) {
            overrideTabKeyActions(getTable(), true);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{LocationsTableRowModel.PROPERTY_ERRORS};
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(LocationsTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isDatesValid(row);
    }

    private boolean isDatesValid(LocationsTableRowModel row) {
        if (getModel().isPeriodsEnabled()) {

            // check first if start or end date has been cleared (Mantis #43965)
            if (row.getStartDate() == null && row.getPreviousStartDate() != null
                    && row.getEndDate() == null && row.getPreviousEndDate() != null) {
                checkExistingSurveysInsideStrategy(row);
            }

            // both start and end dates must be null or not null
            if (row.getStartDate() == null && row.getEndDate() != null) {
                ReefDbBeans.addError(row, t("reefdb.program.location.startDate.null"), LocationsTableRowModel.PROPERTY_START_DATE);

            } else if (row.getStartDate() != null && row.getEndDate() == null) {
                ReefDbBeans.addError(row, t("reefdb.program.location.endDate.null"), LocationsTableRowModel.PROPERTY_END_DATE);

            } else if (row.getStartDate() != null && row.getEndDate() != null) {
                // end date must be after start date
                if (row.getStartDate().isAfter(row.getEndDate())) {
                    ReefDbBeans.addError(row, t("reefdb.program.location.endDate.before"), LocationsTableRowModel.PROPERTY_END_DATE);
                } else {

                    // if dates valid, check surveys outside dates
                    checkExistingSurveysOutsideStrategy(row);
                }
            }
        }
        return row.getErrors().isEmpty();
    }

    private void checkExistingSurveysInsideStrategy(LocationsTableRowModel row) {
        if (row.getAppliedStrategyId() != null && row.getId() != null) {
            Long surveysCount = getContext().getObservationService().countSurveysWithProgramLocationAndInsideDates(
                    getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow().getCode(),
                    row.getAppliedStrategyId(),
                    row.getId(), // = location id
                    row.getPreviousStartDate(),
                    row.getPreviousEndDate()
            );
            if (surveysCount != null && surveysCount > 0) {
                ReefDbBeans.addError(row, t("reefdb.program.location.period.surveyInsidePeriod"),
                        LocationsTableRowModel.PROPERTY_START_DATE, LocationsTableRowModel.PROPERTY_END_DATE);
            }
        }
    }

    private void checkExistingSurveysOutsideStrategy(LocationsTableRowModel row) {
        if (row.getAppliedStrategyId() != null && row.getId() != null) {
            Long surveysCount = getContext().getObservationService().countSurveysWithProgramLocationAndOutsideDates(
                    getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow().getCode(),
                    row.getAppliedStrategyId(),
                    row.getId(), // = location id
                    row.getStartDate(),
                    row.getEndDate()
            );
            if (surveysCount != null && surveysCount > 0) {
                ReefDbBeans.addError(row, t("reefdb.program.location.period.surveyOutsidePeriod"),
                        LocationsTableRowModel.PROPERTY_START_DATE, LocationsTableRowModel.PROPERTY_END_DATE);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, LocationsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // Remove department if no dates (Mantis #43233)
        if (getModel().isPeriodsEnabled()) {
            if (LocationsTableRowModel.PROPERTY_START_DATE.equals(propertyName) || LocationsTableRowModel.PROPERTY_END_DATE.equals(propertyName)) {
                if (row.getStartDate() == null && row.getEndDate() == null && row.getDepartment() != null) {
                    row.setDepartment(null);
                }
            }
        }

        saveToStrategy();
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
    }

    /**
     * <p>saveToStrategy.</p>
     */
    public void saveToStrategy() {

        if (getModel().isLoading()) return;

        // programs contains strategies, strategies contains pmfm
        getProgramsUI().getStrategiesTableUI().getHandler().keepModificationOnLocationsTable();
        getProgramsUI().getProgramsTableUI().getHandler().keepModificationOnStrategiesTable();

        // force model modify and valid
        recomputeRowsValidState();
        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);
        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_VALID, null, getModel().getRowCount() > 0);

    }

    /** {@inheritDoc} */
    @Override
    public void recomputeRowsValidState() {
        super.recomputeRowsValidState();

        // recompute strategy valid state
        getProgramsUI().getStrategiesTableUI().getHandler().recomputeRowsValidState();
    }

    /**
     * <p>removeFromModels.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     */
    public void removeFromModels(List<Integer> locationIds) {

        // fix list of locations in program model
        getProgramsUI().getProgramsTableUI().getHandler().removeLocations(locationIds);

        // fix applied strategies lists in existing strategy models
        for (StrategyDTO strategy : getProgramsUI().getStrategiesTableUI().getModel().getRows()) {
            if (!strategy.isAppliedStrategiesEmpty()) {
                strategy.getAppliedStrategies().removeIf(appliedStrategy -> locationIds.contains(appliedStrategy.getId()));
            }
        }
    }

    /**
     * <p>removeLocations.</p>
     */
    public void removeLocations() {

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucun lieu de selectionne");
            return;
        }

        // the selected program
        ProgramsTableRowModel program = getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow();
        List<Integer> locationIds = Lists.newArrayList();
        for (final AppliedStrategyDTO row : getModel().getSelectedRows()) {
            // list of location ids to delete
            locationIds.add(row.getId());
        }

        // check delete is for program locations or applied strategies
        if (getModel().isPeriodsEnabled()) {

            // selected rows are applied strategies (with applied period)
            if (!askBeforeDelete(t("reefdb.program.location.delete.titre"), t("reefdb.program.location.delete.appliedStrategy.message"))) {
                return;
            }

        } else {

            // check usage of location and program in surveys
            Long surveysCount = getContext().getObservationService().countSurveysWithProgramAndLocations(program.getCode(), locationIds);
            if (surveysCount > 0) {
                getContext().getDialogHelper().showErrorDialog(
                        surveysCount == 1
                                ? t("reefdb.program.location.delete.usedInData")
                                : t("reefdb.program.location.delete.usedInData.many", surveysCount),
                        t("reefdb.program.location.delete.titre"));
                return;
            }

            // selected rows are program location
            if (!askBeforeDelete(t("reefdb.program.location.delete.titre"), t("reefdb.program.location.delete.location.message"))) {
                return;
            }

            // check usage of locations in strategies
            Set<ProgStratDTO> usedStrategies = Sets.newHashSet();
            for (Integer locationId : locationIds) {
                List<ProgStratDTO> appliedStrategies = getContext().getProgramStrategyService().getStrategyUsageByProgramAndLocationId(program.getCode(), locationId);
                usedStrategies.addAll(appliedStrategies);
            }
            if (!usedStrategies.isEmpty()) {
                List<String> locationNames = Lists.newArrayList();
                for (AppliedStrategyDTO row : getModel().getSelectedRows()) {
                    locationNames.add(decorate(row));
                }
                List<String> strategyNames = ReefDbBeans.collectProperties(usedStrategies, ProgStratDTO.PROPERTY_NAME);

                String topPart = t("reefdb.program.location.delete.location.formStrategy");
                String bottomMessage = t("reefdb.program.location.delete.location.fromStrategy.list", ReefDbUIs.formatHtmlList(strategyNames));

                if (locationNames.size() > 8) {

                    if (getContext().getDialogHelper().showConfirmDialog(topPart, ReefDbUIs.getHtmlString(locationNames), bottomMessage, JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        return;
                    }

                } else {

                    String message = String.format("%s\n%s\n%s", topPart, ReefDbUIs.formatHtmlList(locationNames), bottomMessage);
                    if (getContext().getDialogHelper().showConfirmDialog(message, JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                        return;
                    }
                }

            }
        }

        // remove rows
        getModel().deleteSelectedRows();

        if (!getModel().isPeriodsEnabled()) {
            // remove from program model
            removeFromModels(locationIds);
        }

        saveToStrategy();

    }

    /**
     * <p>updatePeriod.</p>
     */
    public void updatePeriod() {

        final UpdatePeriodUI dialogue = new UpdatePeriodUI(getUI());
        dialogue.getModel().setTableModel(getModel());

        if (!getModel().getSelectedProgram().isLocal()) {
            dialogue.getHandler().forceIsLocal(false);
        }

        openDialog(dialogue, new Dimension(400, 180));

        getModel().setModify(true);
        saveToStrategy();
        getTable().repaint();
    }

}
