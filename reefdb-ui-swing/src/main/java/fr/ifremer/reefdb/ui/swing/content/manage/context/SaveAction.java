package fr.ifremer.reefdb.ui.swing.content.manage.context;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;

import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Action sauvegarder les contextes
 */
public class SaveAction extends AbstractReefDbSaveAction<ManageContextsUIModel, ManageContextsUI, ManageContextsUIHandler> {

    List<? extends ContextDTO> contextsToSave;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ManageContextsUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        if (!super.prepareAction() || !getModel().isModify() || !getModel().isValid()) {
            return false;
        }

        contextsToSave = getUI().getManageContextsListUI().getModel().getRows();
        if (contextsToSave.isEmpty()) {
            return false;
        }

        // check name uniqueness in ui
        List<String> names = Lists.newArrayList();
        for (ContextDTO contextToSave : contextsToSave) {
            if (names.contains(contextToSave.getName())) {
                // duplicate found
                getContext().getDialogHelper().showErrorDialog(t("reefdb.error.alreadyExists.label.ui", contextToSave.getName()));
                return false;
            } else {
                names.add(contextToSave.getName());
            }
        }

        // check name uniqueness in db
        List<ContextDTO> allContexts = getContext().getContextService().getAllContexts();
        Map<String, Integer> contextIdsByNames = Maps.newHashMap();
        for (ContextDTO context : allContexts) {
            contextIdsByNames.put(context.getName(), context.getId());
        }
        for (ContextDTO contextToSave : contextsToSave) {
            if (contextToSave.isDirty()) {
                Integer existingId = contextIdsByNames.get(contextToSave.getName());
                if (existingId != null && !existingId.equals(contextToSave.getId())) {
                    // duplicate found
                    getContext().getDialogHelper().showErrorDialog(t("reefdb.error.alreadyExists.label.db", contextToSave.getName()));
                    return false;
                }
            }
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        getContext().getContextService().saveContexts(contextsToSave);

    }

    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        return ImmutableList.of(
                getModel().getContextListModel(),
                getModel().getFilterListModel()
        );
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getUI().getManageContextsListMenuUI().getHandler().reloadComboBox();

        super.postSuccessAction();

    }
}
