package fr.ifremer.reefdb.ui.swing.content.manage.context;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist.ManageFiltersListTableUIModel;

/**
 * <p>ManageContextsUIModel class.</p>
 *
 */
public class ManageContextsUIModel extends AbstractEmptyUIModel<ManageContextsUIModel> {

    private ManageContextsListTableUIModel contextListModel;

    private ManageFiltersListTableUIModel filterListModel;

    /**
     * <p>Getter for the field <code>contextListModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIModel} object.
     */
    public ManageContextsListTableUIModel getContextListModel() {
        return contextListModel;
    }

    /**
     * <p>Setter for the field <code>contextListModel</code>.</p>
     *
     * @param contextListModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIModel} object.
     */
    public void setContextListModel(ManageContextsListTableUIModel contextListModel) {
        this.contextListModel = contextListModel;
    }

    /**
     * <p>Getter for the field <code>filterListModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist.ManageFiltersListTableUIModel} object.
     */
    public ManageFiltersListTableUIModel getFilterListModel() {
        return filterListModel;
    }

    /**
     * <p>Setter for the field <code>filterListModel</code>.</p>
     *
     * @param filterListModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist.ManageFiltersListTableUIModel} object.
     */
    public void setFilterListModel(ManageFiltersListTableUIModel filterListModel) {
        this.filterListModel = filterListModel;
    }

}
