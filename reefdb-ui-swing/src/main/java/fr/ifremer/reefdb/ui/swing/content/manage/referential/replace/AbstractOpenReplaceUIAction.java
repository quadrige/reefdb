package fr.ifremer.reefdb.ui.swing.content.manage.referential.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbMainUIAction;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;
import jaxx.runtime.context.JAXXInitialContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @param <E>  type of DTO implementing BaseReferentialDTO
 * @param <M>
 * @param <UI>
 * @since 3.6
 */
public abstract class AbstractOpenReplaceUIAction<
        E extends BaseReferentialDTO,
        M extends AbstractReplaceUIModel<E>,
        UI extends AbstractReplaceUI<E, M>>
        extends AbstractReefDbMainUIAction {

    /**
     * Logger.
     */
    private static final Log log =
            LogFactory.getLog(AbstractOpenReplaceUIAction.class);

    /**
     * <p>getEntityLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    protected abstract String getEntityLabel();

    /**
     * <p>createNewModel.</p>
     *
     * @return a M object.
     */
    protected abstract M createNewModel();

    /**
     * <p>createUI.</p>
     *
     * @param ctx a {@link jaxx.runtime.context.JAXXInitialContext} object.
     * @return a UI object.
     */
    protected abstract UI createUI(JAXXInitialContext ctx);

    /**
     * <p>Getter for the field <code>referentialList</code>.</p>
     *
     * @param referentialService a {@link fr.ifremer.reefdb.service.referential.ReferentialService} object.
     * @return a {@link java.util.List} object.
     */
    protected abstract List<E> getReferentialList(ReferentialService referentialService);

    /**
     * <p>getSourceListFromReferentialList.</p>
     *
     * @param list a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    protected List<E> getSourceListFromReferentialList(List<E> list) {
        // by default, filter out the local referential
        return ReefDbBeans.filterLocalReferential(list);
    }

    /**
     * <p>getSelectedSource.</p>
     *
     * @return a E object.
     */
    protected abstract E getSelectedSource();

    /**
     * <p>getSelectedTarget.</p>
     *
     * @return a E object.
     */
    protected abstract E getSelectedTarget();

    protected E source;

    protected E target;

    private boolean showDialog = true;

    private List<E> referentialList;

    private List<E> sourceList;

    /**
     * <p>Constructor for AbstractOpenReplaceUIAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    protected AbstractOpenReplaceUIAction(ReefDbMainUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            createProgressionUIModel(3);

        }
        return doAction;
    }

    /** {@inheritDoc} */
    @Override
    public void releaseAction() {
        source = target = null;
        super.releaseAction();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        ReferentialService referentialService = getContext().getReferentialService();

        String entityLabel = getEntityLabel();

        getProgressionUIModel().increments(t("reefdb.replaceReferential.loading.target", entityLabel));

        // Get target list
        referentialList = getReferentialList(referentialService);

        getProgressionUIModel().increments(t("reefdb.replaceReferential.loading.source", entityLabel));

        // Get source list
        sourceList = getSourceListFromReferentialList(referentialList);

        if (log.isDebugEnabled()) {
            log.debug("Loaded local referential: " + (source == null ? "null" : sourceList.size()));
            log.debug("Loaded target referential: " + (referentialList == null ? "null" : referentialList.size()));
        }

        if (CollectionUtils.isEmpty(referentialList)) {

            displayWarningMessage(t("reefdb.replaceReferential.noTarget.title", entityLabel),
                    t("reefdb.replaceReferential.noTarget.message", entityLabel));

            showDialog = false;
        }

        if (CollectionUtils.isEmpty(sourceList)) {

            displayWarningMessage(t("reefdb.replaceReferential.noSource.title", entityLabel),
                    t("reefdb.replaceReferential.noSource.message", entityLabel));

            showDialog = false;
        }

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        if (showDialog) {

            getProgressionUIModel().increments(t("reefdb.replaceReferential.open.dialog"));
            // Load model
            M model = createNewModel();

            model.setTargetList(referentialList);
            model.setSourceList(sourceList);
            model.setSelectedSource(getSelectedSource());
            model.setSelectedTarget(getSelectedTarget());

            JAXXInitialContext ctx = new JAXXInitialContext();
            ctx.add(getUI());
            ctx.add(model);
            ctx.add(ReefDbUIContext.PROPERTY_APPLICATION_CONTEXT, getContext());

            final UI dialog = createUI(ctx);
            dialog.setResizable(false);
            SwingUtilities.invokeLater(() -> getHandler().openDialog(dialog, new Dimension(800, 180)));
        }

    }
}
