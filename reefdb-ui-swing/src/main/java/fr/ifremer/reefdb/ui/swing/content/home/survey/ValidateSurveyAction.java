package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUI;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUIModel;
import fr.ifremer.reefdb.ui.swing.content.home.SaveAction;
import fr.ifremer.reefdb.ui.swing.content.home.survey.validate.ValidateSurveyUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de changer l'état d'une observation dans le tableau des
 * observations de l ecran d accueil.
 */
public class ValidateSurveyAction extends AbstractReefDbAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

    private Collection<? extends SurveyDTO> selectedSurveys;

    private List<SurveyDTO> controlledSurveys;

    private String validationComment;

    private boolean isControlError;

    /**
     * Loggeur.
     */
    private static final Log LOG = LogFactory.getLog(ValidateSurveyAction.class);

    /**
     * Constructor.
     *
     * @param handler Controleur
     */
    public ValidateSurveyAction(final SurveysTableUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucune Observation de selectionne");
            return false;
        }

        selectedSurveys = getModel().getSelectedRows();
        controlledSurveys = Lists.newArrayList();
        int userId = getContext().getDataContext().getRecorderPersonId();
        int departmentId = getContext().getDataContext().getRecorderDepartmentId();

        // check if selected surveys can be validated
        for (SurveyDTO survey : selectedSurveys) {

            if (!getContext().isAuthenticatedAsNationalAdmin()
                && !ReefDbBeans.isProgramManager(survey.getProgram(), userId, departmentId)
                && !ReefDbBeans.isProgramValidator(survey.getProgram(), userId, departmentId)
            ) {
                displayErrorMessage(t("reefdb.action.validate.survey.title"), t("reefdb.action.validate.survey.error.forbidden"));
                return false;
            }

            // On ne peut pas valider une observation qui a déja été validée
            if (survey.getValidationDate() != null) {
                displayWarningMessage(t("reefdb.action.validate.survey.title"), t("reefdb.action.validate.survey.error.alreadyValid"));
                return false;
            }

        }

        // Demande de confirmation avant la validation
        ValidateSurveyUI validateSurveyUI = new ValidateSurveyUI(getContext());
        validateSurveyUI.getModel().setUnValidation(false);
        getHandler().openDialog(validateSurveyUI);

        if (validateSurveyUI.getModel().isValid()) {
            validationComment = validateSurveyUI.getModel().getComment();
            return true;
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {
        boolean isModifyModelState = getModel().isModify();

        List<SurveyDTO> surveysToControl = Lists.newArrayList();
        for (SurveyDTO survey : selectedSurveys) {
            if (survey.isDirty() || survey.getControlDate() == null) {
                surveysToControl.add(survey);
            }
        }

        if (!surveysToControl.isEmpty()) {
            // Must save and control dirty or uncontrolled surveys
            SaveAction saveAction = getContext().getActionFactory().createLogicAction(getUI().getParentContainer(HomeUI.class).getHandler(), SaveAction.class);
            saveAction.setSurveysToSave(surveysToControl);
            saveAction.setControlSilently(true);
            saveAction.setShowControlIfSuccess(false);
            saveAction.setUpdateControlDateWhenControlSucceed(true);
            if (!saveAction.prepareAction()) {
                // return silently when save action can not be prepared
                return;
            }
            getContext().getActionEngine().runInternalAction(saveAction);
            isControlError = saveAction.getControlMessages().isErrorMessages();
        }

        // Check dirty state to continue validation & Filter controlled surveys
        controlledSurveys.clear();
        for (SurveyDTO survey : selectedSurveys) {
            if (survey.isDirty()) {
                // something goes wrong when save or control
                throw new ReefDbBusinessException("something goes wrong when save or control");
            }
            if (survey.getControlDate() != null) {
                controlledSurveys.add(survey);
            }
        }

        // Validate controlled surveys only
        getContext().getObservationService().validateSurveys(controlledSurveys, validationComment, createProgressionUIModel());

        getModel().setModify(isModifyModelState);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        // force reloading operations
        getModel().getMainUIModel().firePropertyChanged(HomeUIModel.PROPERTY_SELECTED_SURVEY, null, null);

        // Show warning when not all selected surveys have been validated
        if (isControlError) {
            displayWarningMessage(t("reefdb.action.validate.survey.title"), t("reefdb.action.validate.survey.warning.notControlled"));
        }

        // if controlledSurveys is not empty: they have been validated
        List<SurveyDTO> notReadyToSynchronizeSurveys = Lists.newArrayList();
        for (SurveyDTO validatedSurvey : controlledSurveys) {

            // check the synchronization status on survey with national program - mantis #39393
            boolean nationalProgram = validatedSurvey.getProgram() != null && !ReefDbBeans.isLocalStatus(validatedSurvey.getProgram().getStatus());
            if (nationalProgram && (validatedSurvey.getValidationDate() == null ||
                    !SynchronizationStatusValues.READY_TO_SYNCHRONIZE.equals(validatedSurvey.getSynchronizationStatus()))) {
                notReadyToSynchronizeSurveys.add(validatedSurvey);
            }
        }

        // show warning message if some surveys on national program are not ready to synchronize
        // e.g. if there use local referential - mantis #39393
        if (CollectionUtils.isNotEmpty(notReadyToSynchronizeSurveys)) {

            List<String> list = ReefDbBeans.transformCollection(notReadyToSynchronizeSurveys, (Function<SurveyDTO, String>) ReefDbBeans::toString);
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.validate.survey.error.localReferentialNotAllow.message"),
                    ReefDbUIs.getHtmlString(list),
                    t("reefdb.action.validate.survey.error.localReferentialNotAllow.help"),
                    t("reefdb.action.validate.survey.title")
            );
        }

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {

        isControlError = false;
        controlledSurveys = Lists.newArrayList();

        super.releaseAction();
    }
}
