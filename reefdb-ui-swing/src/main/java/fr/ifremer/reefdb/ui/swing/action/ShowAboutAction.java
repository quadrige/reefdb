package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.quadrige3.ui.swing.action.AbstractAction;
import fr.ifremer.quadrige3.ui.swing.action.UpdateApplicationAction;
import fr.ifremer.quadrige3.ui.swing.callback.ApplicationUpdaterCallBack;
import fr.ifremer.quadrige3.ui.swing.callback.DatabaseUpdaterCallBack;
import fr.ifremer.quadrige3.ui.swing.content.db.InstallDbAction;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.swing.AboutPanel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.updater.ApplicationUpdater;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.HyperlinkEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * To show about panel.
 *
 * @since 1.2
 */
public class ShowAboutAction extends AbstractReefDbMainUIAction {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ShowAboutAction.class);

    protected AboutPanel about;
    protected boolean canUpdateApplication;
    protected boolean canUpdateData;

    /**
     * <p>Constructor for ShowAboutAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    public ShowAboutAction(ReefDbMainUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {
            // check db url is reachable
            canUpdateApplication = getContext().checkUpdateReachable(getConfig().getUpdateApplicationUrl(), false);
            canUpdateData = getContext().checkUpdateReachable(getConfig().getUpdateDataUrl(), false);

        }

        return doAction;
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        about.showInDialog(getUI(), true);

        // register on swing session
        getContext().getSwingSession().add(about);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        about = null;

        String iconPath = "/icons/reefdb_about.png";
        String name = "reefdb";
        String licensePath = "META-INF/" + name + "-LICENSE.txt";
        String thirdPartyPath = "META-INF/" + name + "-THIRD-PARTY.txt";

        about = new AboutPanel();
        about.setTitle(t("reefdb.about.title"));
        about.setAboutText(t("reefdb.about.message"));

        ReefDbConfiguration config = getConfig();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int inceptionYear = config.getInceptionYear();
        String years;
        if (currentYear != inceptionYear) {
            years = inceptionYear + "-" + currentYear;
        } else {
            years = inceptionYear + "";
        }

        about.setBottomText(t("reefdb.about.bottomText",
                config.getOrganizationName(),
                years,
                config.getVersion()));
        about.setIconPath(iconPath);
        about.setLicenseFile(licensePath);
        about.setThirdpartyFile(thirdPartyPath);
        about.buildTopPanel();

        // translate tab
        addTranslateTab();

        // configuration tab
        addConfigTab(getContext().getConfiguration());

        if (canUpdateApplication || canUpdateData) {

            // update tab
            addUpdateTab(config);

        }
        about.init();
    }

    private void addConfigTab(ReefDbConfiguration config) {
        JScrollPane configPane = new JScrollPane();
        configPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JEditorPane configArea = new JEditorPane();
        configArea.setEditable(false);
        if (configArea.getFont() != null) {
            configArea.setFont(configArea.getFont().deriveFont((float) 11));
        }
        configArea.setBorder(null);

        StringBuilder configContent = new StringBuilder();
        Properties properties = config.getApplicationConfig().getFlatOptions();
        Set<String> optionNames = properties.stringPropertyNames().stream()
                .filter(propertyName -> propertyName.startsWith("reefdb") || propertyName.startsWith("quadrige3"))
                .collect(Collectors.toCollection(TreeSet::new));

        optionNames.forEach(optionName -> configContent.append(optionName).append("=").append(properties.getProperty(optionName)).append(System.lineSeparator()));

        configArea.setText(configContent.toString());
        configPane.getViewport().add(configArea);
        about.getTabs().add(t("reefdb.about.config.title"), configPane);
    }

    private void addTranslateTab() throws MalformedURLException {
        JScrollPane translatePane = new JScrollPane();
        JEditorPane translateArea = new JEditorPane();
        translateArea.setContentType("text/html");
        translateArea.setEditable(false);
        if (translateArea.getFont() != null) {
            translateArea.setFont(translateArea.getFont().deriveFont((float) 11));
        }

        translateArea.setBorder(null);
        File csvFile = new File(getConfig().getI18nDirectory(), "reefdb-i18n.csv");
        String translateText = t("reefdb.about.translate.content", csvFile.toURI().toURL());
        translateArea.setText(translateText);
        translatePane.getViewport().add(translateArea);
        translateArea.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                if (LOG.isInfoEnabled()) {
                    LOG.info("edit url: " + url);
                }
                ReefDbUIs.openLink(url);
            }
        });
        about.getTabs().add(t("reefdb.about.translate.title"), translatePane);
    }

    /**
     * <p>addUpdate.</p>
     *
     * @param source a {@link java.util.Map} object.
     * @param target a {@link java.util.Map} object.
     * @param type a {@link java.lang.String} object.
     */
    protected void addUpdate(Map<String, ApplicationInfo> source,
                             Map<String, ApplicationInfo> target,
                             String type) {
        ApplicationInfo info = source.get(type.toLowerCase());
        target.put(type, info);
    }

    /**
     * <p>addUpdateTab.</p>
     *
     * @param config a {@link fr.ifremer.reefdb.config.ReefDbConfiguration} object.
     */
    protected void addUpdateTab(ReefDbConfiguration config) {
        File current = config.getBaseDirectory();
        String urlApplication = config.getUpdateApplicationUrl();
        String urlData = config.getUpdateDataUrl();

        ApplicationUpdater up = new ApplicationUpdater();

        // create final update map
        final Map<String, ApplicationInfo> versions = Maps.newLinkedHashMap();

        if (canUpdateApplication) {

            // get application updates
            Map<String, ApplicationInfo> applicationVersions = up.getVersions(urlApplication, current);

            addUpdate(applicationVersions, versions, ApplicationUpdaterCallBack.UpdateType.JRE.name());
            addUpdate(applicationVersions, versions, ApplicationUpdaterCallBack.UpdateType.APPLICATION.name());
            addUpdate(applicationVersions, versions, ApplicationUpdaterCallBack.UpdateType.I18N.name());
            addUpdate(applicationVersions, versions, ApplicationUpdaterCallBack.UpdateType.HELP.name());
        }

        if (canUpdateData) {

            // get db updates
            Map<String, ApplicationInfo> dbVersions = up.getVersions(urlData, config.getDataDirectory());
            addUpdate(dbVersions, versions, DatabaseUpdaterCallBack.DB_UPDATE_NAME);
        }
        JScrollPane updatePane = new JScrollPane();
        JEditorPane updateArea = new JEditorPane();
        updateArea.setContentType("text/html");
        updateArea.setEditable(false);
        if (updateArea.getFont() != null) {
            updateArea.setFont(updateArea.getFont().deriveFont((float) 11));
        }
        updateArea.setBorder(null);

        List<String> params = Lists.newArrayList();
        for (Map.Entry<String, ApplicationInfo> entry : versions.entrySet()) {
            String appName = entry.getKey();
            ApplicationInfo info = entry.getValue();
            String oldVersion = info != null ? info.oldVersion : t("reefdb.about.update.app.undefined");
            String newVersion = info != null ? info.newVersion : null;
            String i18nKey = "quadrige3.update." + appName.toLowerCase(); // keep quadrige3. i18n base
            String appLabel = t(i18nKey);

            if (LOG.isInfoEnabled()) {
                LOG.info(String.format(
                        "Module %s, version courante %s, nouvelle version %s",
                        appLabel, oldVersion, newVersion));
            }
            if (newVersion == null) {

                // no update
                params.add(t("reefdb.about.update.app.noup.detail", appLabel, oldVersion));
            } else {
                // update exists
                params.add(t("reefdb.about.update.app.up.detail", appLabel, oldVersion, newVersion, appName));
            }
        }

        String updateText = t("reefdb.about.update.content", urlApplication, urlData, Joiner.on("\n").join(params));
        updateArea.setText(updateText);
        updatePane.getViewport().add(updateArea);
        updateArea.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                if (url != null) {
                    ReefDbUIs.openLink(url);
                } else {
                    String appType = e.getDescription();

                    if (LOG.isInfoEnabled()) {
                        LOG.info("Open url: " + appType);
                    }
                    AbstractAction<?, ?, ?> action;

                    if (DatabaseUpdaterCallBack.DB_UPDATE_NAME.equals(appType)) {
                        if (getContext().isDbExist()) {
                            // Update DB referential
                            action = getContext().getActionFactory().createLogicAction(getHandler(), ImportReferentialSynchroAction.class);
                        } else {
                            // Install db
                            action = getContext().getActionFactory().createLogicAction(getHandler(), InstallDbAction.class);
                            action.setActionDescription(t("reefdb.dbManager.action.installDb.tip"));
                        }
                    } else {

                        ApplicationUpdaterCallBack.UpdateType updateType
                                = ApplicationUpdaterCallBack.UpdateType.valueOf(appType.toUpperCase());

                        UpdateApplicationAction logicAction = getContext().getActionFactory().createLogicAction(getHandler(), UpdateApplicationAction.class);
                        logicAction.setTypes(updateType);
                        logicAction.setActionDescription(t("reefdb.main.action.updateSpecificApplication.tip", updateType.getLabel()));
                        action = logicAction;
                    }

                    // close this dialog
                    getActionEngine().runAction(about.getClose());

                    // do update
                    getActionEngine().runAction(action);
                }

            }
        });
        about.getTabs().add(t("reefdb.about.update.title"), updatePane);
    }

}
