package fr.ifremer.reefdb.ui.swing.content.home.operation.add;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.StringIterator;
import fr.ifremer.quadrige3.ui.swing.component.coordinate.CoordinateEditor;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.LocalTimeCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableModel;
import fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableRowModel;
import fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour l'écran principale de l'acceuil.
 */
public class AddOperationTableUIHandler extends AbstractReefDbTableUIHandler<OperationsTableRowModel, AddOperationTableUIModel, AddOperationTableUI> implements Cancelable {

    private ExtendedComboBoxCellEditor<SamplingEquipmentDTO> samplingEquipmentCellEditor;
    private ExtendedComboBoxCellEditor<DepartmentDTO> samplingDepartmentCellEditor;
    private ExtendedComboBoxCellEditor<DepartmentDTO> analystDepartmentCellEditor;

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<OperationsTableRowModel> getTableModel() {
        return (OperationsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getAddOperationTable();
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AddOperationTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final AddOperationTableUIModel model = new AddOperationTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(AddOperationTableUI addOperationTableUI) {

        // Initialiser l UI
        initUI(ui);

        createSamplingDepartmentCellEditor();
        createAnalystDepartmentCellEditor();
        createSamplingEquipmentCellEditor();

        initTable();

        // add listener on TAB key to follow focus to table
        String tabActionName = "newTABAction";
        JTextField field = getUI().getOperationNamePrefixEditor();
        field.setFocusTraversalKeysEnabled(false);
        field.getInputMap().put(KeyStroke.getKeyStroke("pressed TAB"), tabActionName);
        field.getActionMap().put(tabActionName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setFocusOnCell(getModel().getRows().get(0));
            }
        });

//        getContext().getSwingSession().add(ui, true);
    }

    /**
     * Charge les colonnes dynamiques.
     */
    public void load(final OperationsTableUIHandler parentHandler) {

        getModel().setSurvey(parentHandler.getModel().getSurvey());

        // Chargement des psfms du tableau
        List<PmfmDTO> pmfms = parentHandler.getModel().getPmfms();
        getModel().setPmfms(pmfms);
        addPmfmColumns(
                getModel().getPmfms(),
                SamplingOperationDTO.PROPERTY_PMFMS,
                DecoratorService.NAME_WITH_UNIT,
                OperationsTableModel.TIME);

        boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfmColumns());

        // Le prelevement
        SamplingOperationDTO newSamplingOperation = getContext().getObservationService().newSamplingOperation(getModel().getSurvey(), pmfms);

        // init operation name prefix editor
        getModel().setOperationNamePrefix(
                StringIterator.newStringIteratorByProperty(getModel().getSurvey().getSamplingOperations(), SamplingOperationDTO.PROPERTY_NAME).getPrefix()
        );

        // Chargement du tableau avec les prelevements
        getModel().setRows(null);
        OperationsTableRowModel newRow = getModel().addNewRow(newSamplingOperation);

        // If no PMFM, hide analysis column and set default (Mantis #42619)
//        forceColumnVisibleAtLastPosition(OperationsTableModel.ANALYST, notEmpty);
        // Don't force position (Mantis #49537)
        forceColumnVisible(OperationsTableModel.ANALYST, notEmpty);

        // Set analyst from pmfm strategies
        if (notEmpty) {
            newRow.setAnalyst(getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
                    getModel().getSurvey(),
                    getModel().getPmfms()
            ));
        }

        recomputeRowsValidState(false);

        // try to copy column position and visibility from parent operation table (Mantis #54569)
        copyParentTableState(parentHandler.getTable());

    }


    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Colonne engin
        final TableColumnExt colSamplingEquipment = addColumn(
                samplingEquipmentCellEditor,
                newTableCellRender(OperationsTableModel.SAMPLING_EQUIPMENT),
                OperationsTableModel.SAMPLING_EQUIPMENT
        );
        colSamplingEquipment.setSortable(true);
        colSamplingEquipment.setMinWidth(200);

        // Colonne Heure
        final TableColumnExt colTime = addColumn(
                new LocalTimeCellEditor(),
                newTableCellRender(Integer.class, DecoratorService.TIME_IN_HOURS_MINUTES),
                OperationsTableModel.TIME);
        colTime.setSortable(true);
        colTime.setMinWidth(50);

        //------ Colonnes optionnelles

        // Colonne Taille
        final TableColumnExt colSize = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL7_PATTERN),
                newNumberCellRenderer(7),
                OperationsTableModel.SIZE);
        colSize.setSortable(true);
        colSize.setMinWidth(50);

        // Colonne Unite taille
        final TableColumnExt colSizeUnit = addExtendedComboDataColumnToModel(
                OperationsTableModel.SIZE_UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.ACTIVE),
                false);
        colSizeUnit.setSortable(true);
        colSizeUnit.setMinWidth(100);

        // Colonne service préleveur
        final TableColumnExt colSamplingDepartment = addColumn(
                samplingDepartmentCellEditor,
                newTableCellRender(OperationsTableModel.SAMPLING_DEPARTMENT),
                OperationsTableModel.SAMPLING_DEPARTMENT
        );
        colSamplingDepartment.setSortable(true);
        colSamplingDepartment.setMinWidth(100);

        // Colonne service analyste
        final TableColumnExt colAnalystDepartment = addColumn(
                analystDepartmentCellEditor,
                newTableCellRender(OperationsTableModel.ANALYST),
                OperationsTableModel.ANALYST
        );
        colAnalystDepartment.setSortable(true);
        colAnalystDepartment.setMinWidth(100);

        // Colonne immersion
        final TableColumnExt colDepth = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN),
                newNumberCellRenderer(2),
                OperationsTableModel.DEPTH);
        colDepth.setSortable(true);
        colDepth.setMinWidth(100);

        // Colonne immersion min
        final TableColumnExt colMinDepth = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN),
                newNumberCellRenderer(2),
                OperationsTableModel.MIN_DEPTH);
        colMinDepth.setSortable(true);
        colMinDepth.setMinWidth(100);

        // Colonne immersion
        final TableColumnExt colMaxDepth = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN),
                newNumberCellRenderer(2),
                OperationsTableModel.MAX_DEPTH);
        colMaxDepth.setSortable(true);
        colMaxDepth.setMinWidth(100);

        // Colonne Coordonnées réelles
        final TableColumnExt colLatitude = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MIN,
                OperationsTableModel.LATITUDE);
        colLatitude.setSortable(true);
        colLatitude.setMinWidth(100);

        final TableColumnExt colLongitude = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MIN,
                OperationsTableModel.LONGITUDE);
        colLongitude.setSortable(true);
        colLongitude.setMinWidth(100);

        // Colonne informations de positionnement
        final TableColumnExt colPositioning = addExtendedComboDataColumnToModel(
                OperationsTableModel.POSITIONING,
                getContext().getReferentialService().getPositioningSystems(),
                false);
        colPositioning.setSortable(true);
        colPositioning.setMinWidth(100);

        final TableColumnExt colPositioningPrecision = addColumn(
                OperationsTableModel.POSITIONING_PRECISION);
        colPositioningPrecision.setSortable(true);
        colPositioningPrecision.setMinWidth(100);
        colPositioningPrecision.setEditable(false);

        // Modele de la table
        final OperationsTableModel tableModel = new OperationsTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable(), true);

        // Les colonnes optionnelles sont invisibles
        colSize.setVisible(false);
        colSizeUnit.setVisible(false);
        colSamplingDepartment.setVisible(false);
        colDepth.setVisible(false);
        colMinDepth.setVisible(false);
        colMaxDepth.setVisible(false);
        colLatitude.setVisible(false);
        colLongitude.setVisible(false);
        colPositioning.setVisible(false);
        colPositioningPrecision.setVisible(false);

    }

    private void createSamplingEquipmentCellEditor() {

        samplingEquipmentCellEditor = newExtendedComboBoxCellEditor(null,
                SamplingEquipmentDTO.class, false);

        samplingEquipmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateSamplingEquipmentCellEditor(true);
        });

        updateSamplingEquipmentCellEditor(false);
    }

    private void updateSamplingEquipmentCellEditor(boolean forceNoFilter) {

        samplingEquipmentCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.SAMPLING_EQUIPMENT));

        samplingEquipmentCellEditor.getCombo().setData(
                getContext().getObservationService().getAvailableSamplingEquipments(forceNoFilter));
    }

    private void createSamplingDepartmentCellEditor() {

        samplingDepartmentCellEditor = newExtendedComboBoxCellEditor(null,
                DepartmentDTO.class, false);

        samplingDepartmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateSamplingDepartmentCellEditor(true);
        });

        updateSamplingDepartmentCellEditor(false);
    }

    private void updateSamplingDepartmentCellEditor(boolean forceNoFilter) {

        samplingDepartmentCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        samplingDepartmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    private void createAnalystDepartmentCellEditor() {

        analystDepartmentCellEditor = newExtendedComboBoxCellEditor(null,
                DepartmentDTO.class, false);

        analystDepartmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateAnalystDepartmentCellEditor(true);
        });

        updateAnalystDepartmentCellEditor(false);
    }

    private void updateAnalystDepartmentCellEditor(boolean forceNoFilter) {

        analystDepartmentCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        analystDepartmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(final OperationsTableRowModel row) {

        return super.isRowValid(row) & isOperationValid(row);
    }

    private boolean isOperationValid(OperationsTableRowModel row) {

        // coordinates
        if (row.getLatitude() == null ^ row.getLongitude() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.coordinate.invalid"), OperationsTableRowModel.PROPERTY_LONGITUDE, OperationsTableRowModel.PROPERTY_LATITUDE);
        }

        // positioning
        if (row.getLatitude() != null && row.getLongitude() != null && row.getPositioning() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.positioning.required"), OperationsTableRowModel.PROPERTY_POSITIONING);
        }

        // analyst
        if (row.getAnalyst() == null && row.getMeasurements().stream().anyMatch(measurement -> !ReefDbBeans.isMeasurementEmpty(measurement))) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.analyst.required"), OperationsTableRowModel.PROPERTY_ANALYST);
        }

        // length
        if (row.getSize() != null && row.getSizeUnit() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.sizeUnit.required"), OperationsTableRowModel.PROPERTY_SIZE_UNIT);
        }

        boolean hasNoError = ReefDbBeans.hasNoBlockingError(row);

        if (!hasNoError) {
            ensureColumnsWithErrorAreVisible(row);
        }

        return hasNoError;
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // Recuperation du nombre de prelevements a creer
        final Integer nbOperation = getModel().getNbOperation();
        if (nbOperation == null || nbOperation <= 0) {

            // Le nombre de prelevements doit être renseigne
            getContext().getDialogHelper().showErrorDialog(getUI(),
                    t("reefdb.home.samplingOperation.new.number.error.message"),
                    t("reefdb.home.samplingOperation.new.number.error.title"));
        } else {
            // Prelevement saisi
            OperationsTableRowModel rowModel = getModel().getRows().get(0);

            // keep analyst (not bound property)
            DepartmentDTO analyst = rowModel.getAnalyst();

            // convert and clean rows
            SamplingOperationDTO operation = rowModel.toBean();
            getModel().setRows(null);

            // Add default service from observation, if any
            if (operation.getSamplingDepartment() == null) {
                // should not happened
                operation.setSamplingDepartment(getModel().getSurvey().getRecorderDepartment());
            }

            // Creation des prelevements
            final List<SamplingOperationDTO> operations = new ArrayList<>();

            for (int i = 0; i < nbOperation; i++) {

                // Ajout du prelevement dans la liste
                final SamplingOperationDTO newOperation = ReefDbBeans.duplicate(operation);
                operations.add(newOperation);

                // do it at creation time
                newOperation.setMeasurementsLoaded(true);
            }

            // Ajout des prelevements
            getModel().setBeans(operations);

            // restore analyst
            getModel().getRows().forEach(row -> row.setAnalyst(analyst));

            // Quitter la dialogue
            closeDialog();
        }
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getNbOperationEditor();
    }

    /**
    /** {@inheritDoc} */
    @Override
    public Component getNextComponentToFocus() {
        return getUI().getValidButton();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setRows(null);
        getModel().setValid(false);
        closeDialog();
    }
}
