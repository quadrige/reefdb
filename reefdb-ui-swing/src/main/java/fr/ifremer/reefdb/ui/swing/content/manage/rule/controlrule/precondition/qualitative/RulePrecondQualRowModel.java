package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.qualitative;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Date;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePrecondQualRowModel extends AbstractReefDbRowUIModel<QualitativeValueDTO, RulePrecondQualRowModel> implements QualitativeValueDTO {

    private static final Binder<QualitativeValueDTO, RulePrecondQualRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(QualitativeValueDTO.class, RulePrecondQualRowModel.class);
    private static final Binder<RulePrecondQualRowModel, QualitativeValueDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(RulePrecondQualRowModel.class, QualitativeValueDTO.class);

    public RulePrecondQualRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }



    // Delegate methods

    @Override
    protected QualitativeValueDTO newBean() {
        return ReefDbBeanFactory.newQualitativeValueDTO();
    }

    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    @Override
    public String getName() {
        return delegateObject.getName();
    }

    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }

}
