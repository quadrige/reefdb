package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.menu.ManageParametersMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.table.ParameterTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.table.ParameterTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SwingUtilities;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Parameters au niveau local
 */
public class ManageParametersLocalUIHandler extends AbstractReefDbTableUIHandler<ParameterTableRowModel, ManageParametersLocalUIModel, ManageParametersLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageParametersLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageParametersLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageParametersLocalUIModel model = new ManageParametersLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageParametersLocalUI ui) {
        initUI(ui);

        // Initialisation du tableau
        initTable();

        // Disabled buttons
        getUI().getDeleteButton().setEnabled(false);
        getUI().getReplaceButton().setEnabled(false);

        ManageParametersMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(true);

        //listen to results
        menuUIModel.addPropertyChangeListener(ManageParametersMenuUIModel.PROPERTY_RESULTS, evt -> loadParametersManageLocal((List<ParameterDTO>) evt.getNewValue()));

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // code
        final TableColumnExt codeCol = addColumn(ParameterTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(ParameterTableModel.NAME);
        mnemonicCol.setSortable(true);

        // description
        final TableColumnExt descriptionCol = addColumn(ParameterTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // qualitative
        final TableColumnExt qualitativeCol = addBooleanColumnToModel(ParameterTableModel.IS_QUALITATIVE, getTable());
        qualitativeCol.setSortable(true);

        // calculated
        final TableColumnExt calculatedCol = addBooleanColumnToModel(ParameterTableModel.IS_CALCULATED, getTable());
        calculatedCol.setSortable(true);

        // taxonomic
        final TableColumnExt taxonomicCol = addBooleanColumnToModel(ParameterTableModel.IS_TAXONOMIC, getTable());
        taxonomicCol.setSortable(true);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
                new AssociatedQualitativeValueCellEditor(getTable(), getUI(), true),
                new AssociatedQualitativeValueCellRenderer(),
                ParameterTableModel.ASSOCIATED_QUALITATIVE_VALUE);
        associatedQualitativeValueCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(ParameterTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(ParameterTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(ParameterTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                ParameterTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);
        fixDefaultColumnWidth(statusCol);

        // parameterGroup
        final TableColumnExt parameterGroupCol = addFilterableComboDataColumnToModel(
                ParameterTableModel.PARAMETER_GROUP,
                getContext().getReferentialService().getParameterGroup(StatusFilter.ACTIVE),
                false);
        parameterGroupCol.setSortable(true);

        // Add column model
        final ParameterTableModel tableModel = new ParameterTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.parameters.local", ParameterTableModel.ASSOCIATED_QUALITATIVE_VALUE));

        // Initialisation du tableau
        initTable(getTable());

        // hidden columns
        parameterGroupCol.setVisible(false);
        calculatedCol.setVisible(false);
        taxonomicCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ParameterTableRowModel> getTableModel() {
        return (ParameterTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getParametersLocalTable();
    }

    /**
     * Methode permettant le changement des manage de parameter au niveau local.
     *
     * @param parameters La liste des manage de parameter au niveau local
     */
    public void loadParametersManageLocal(final List<ParameterDTO> parameters) {

        // Enabeld new button
        getUI().getNewButton().setEnabled(true);

        // Chargement des lignes du tableau
        getModel().setBeans(parameters);
        getModel().setModify(false);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<ParameterTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            final ParameterTableRowModel rowModel = addedRows.get(0);

            if (checkNewCode(rowModel)) {

                // Set default status
                rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
                rowModel.setNewCode(true);
                setFocusOnCell(rowModel);

            } else {
                // the code is invalid, delete this row
                SwingUtilities.invokeLater(() -> {
                    getModel().deleteRow(rowModel);
                    getModel().setSingleSelectedRow(null);
                });
            }
        }
    }

    private boolean checkNewCode(ParameterTableRowModel row) {

        boolean edit = StringUtils.isNotBlank(row.getCode());
        String newCode = (String) getContext().getDialogHelper().showInputDialog(
                getUI(),
                t("reefdb.property.pmfm.parameter.code"),
                edit ? t("reefdb.parameter.editCode.title") : t("reefdb.parameter.setCode.title"),
                null,
                row.getCode());
        if (StringUtils.isBlank(newCode)) {
            return false;
        }
        newCode = newCode.trim();

        // check in current table
        for (ParameterTableRowModel otherRow : getModel().getRows()) {
            if (row == otherRow) continue;
            if (newCode.equalsIgnoreCase(otherRow.getCode())) {
                // duplicate found in table
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.error.alreadyExists.referential", t("reefdb.property.pmfm.parameter"), newCode, t("reefdb.property.referential.local")),
                        edit ? t("reefdb.parameter.editCode.title") : t("reefdb.parameter.setCode.title")
                );
                return false;
            }
        }

        // check unicity in database
        List<ParameterDTO> existingParameters = getContext().getReferentialService().searchParameters(StatusFilter.ALL, newCode, null, null);
        if (CollectionUtils.isNotEmpty(existingParameters)) {
            ParameterDTO parameter = existingParameters.get(0);
            // duplicate found in database
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.error.alreadyExists.referential", t("reefdb.property.pmfm.parameter"), newCode,
                            ReefDbBeans.isLocalStatus(parameter.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                    edit ? t("reefdb.parameter.editCode.title") : t("reefdb.parameter.setCode.title")
            );
            return false;
        }

        row.setCode(newCode);
        return true;

    }

    /**
     * <p>editParameterCode.</p>
     */
    public void editParameterCode() {
        ParameterTableRowModel row = getModel().getSingleSelectedRow();
        if (row != null && row.isLocal() && row.isNewCode()) {
            if (checkNewCode(row)) {
                row.setDirty(true);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, ParameterTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModifyStateChanged(int rowIndex, ParameterTableRowModel row, Boolean oldValue, Boolean newValue) {
        super.onRowModifyStateChanged(rowIndex, row, oldValue, newValue);
        row.setDirty(true);
        getModel().setModify(true);
    }

}
