package fr.ifremer.reefdb.ui.swing.content.manage.rule.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.menu.ProgramsMenuUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des programmes
 */
public class ControlProgramTableUIHandler extends
        AbstractReefDbTableUIHandler<ControlProgramTableRowModel, ControlProgramTableUIModel, ControlProgramTableUI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ControlProgramTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlProgramTableUIModel model = new ControlProgramTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ControlProgramTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Desactivation des boutons
        getUI().getAddProgramButton().setEnabled(false);
        getUI().getRemoveProgramButton().setEnabled(false);

        // Initialisation du tableau
        initTable();

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Code
        TableColumnExt codeCol = addColumn(ControlProgramTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // Libelle
        TableColumnExt nameCol = addColumn(ControlProgramTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // Description
        TableColumnExt descriptionCol = addColumn(ControlProgramTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        ControlProgramTableModel tableModel = new ControlProgramTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable());

        // Number rows visible
        getTable().setVisibleRowCount(3);
    }

    /**
     * <p>loadPrograms.</p>
     *
     * @param programs a {@link java.util.Collection} object.
     */
    public void loadPrograms(final Collection<ProgramDTO> programs) {

        // Load les programmesControle dans le model
        getModel().setBeans(programs);
        getTable().setEditable(getModel().getParentModel().isEditable());

        // Activation du bouton nouveau
        getUI().applyDataBinding(ControlProgramTableUI.BINDING_ADD_PROGRAM_BUTTON_ENABLED);

        recomputeRowsValidState(false);
        getModel().setModify(false);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {

        // Suppression des programmesControle dans le model
        getModel().setBeans(null);

        // Desactivation du bouton nouveau
        getUI().getAddProgramButton().setEnabled(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<ControlProgramTableRowModel> getTableModel() {
        return (ControlProgramTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getControlProgramTable();
    }

    /**
     * <p>openAddDialog.</p>
     */
    void openAddDialog() {

        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.PROGRAM.getFilterTypeId());
        dialog.setTitle(t("reefdb.filter.program.addDialog.title"));
        List<ProgramDTO> programs = getModel().getBeans().stream()
                .sorted(getDecorator(ProgramDTO.class, null).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(programDTO -> programDTO.setReadOnly(true))
                .collect(Collectors.toList());

        dialog.getModel().setSelectedElements(programs);

        ProgramsMenuUI menuUI = (ProgramsMenuUI) dialog.getHandler().getFilterElementUIHandler().getReferentialMenuUI();

        if (!getModel().getParentModel().getSelectedRuleList().isLocal()) {
            // filter only national programs if rule list is national
            menuUI.getHandler().forceLocal(false);
        }

        // filter only on managed programs in all cases (Mantis #43106)
        menuUI.getModel().setOnlyManagedPrograms(true);

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            List<ProgramDTO> programsToAdd = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((ProgramDTO) element))
                    .filter(program -> !getModel().getBeans().contains(program))
                    .collect(Collectors.toList());

            if (!programsToAdd.isEmpty()) {

                // no need to test this list against the managed programs, it should be already filtered

                getModel().addBeans(programsToAdd);
                getModel().setModify(true);
                saveToParentModel();
            }
        }
    }

    /**
     * <p>removePrograms.</p>
     */
    void removePrograms() {
        getModel().deleteSelectedRows();
        saveToParentModel();
    }

    private void saveToParentModel() {
        getModel().getParentModel().getSelectedRuleList().setPrograms(getModel().getBeans());
        recomputeRowsValidState(false);

        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);
//        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_VALID, null, getModel().getRowCount() > 0);
    }

}
