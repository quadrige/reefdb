package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model pour le tableau des observations.
 */
public class SurveysTableUIModel extends AbstractReefDbTableUIModel<SurveyDTO, SurveysTableRowModel, SurveysTableUIModel> {

    private HomeUIModel mainUIModel;

    private boolean selectedSurveyEditable;
    /** Constant <code>PROPERTY_SELECTED_SURVEY_EDITABLE="selectedSurveyEditable"</code> */
    public static final String PROPERTY_SELECTED_SURVEY_EDITABLE = "selectedSurveyEditable";

    private boolean samplingOperationsLoading;
    /** Constant <code>PROPERTY_SAMPLING_OPERATIONS_LOADING="samplingOperationsLoading"</code> */
    public static final String PROPERTY_SAMPLING_OPERATIONS_LOADING = "samplingOperationsLoading";

    /**
     * Constructor.
     */
    public SurveysTableUIModel() {
        super();
    }

    @Override
    public boolean isModify() {
        // Check at least one row is dirty (Mantis #59690)
        return super.isModify() && getRows().stream().anyMatch(SurveysTableRowModel::isDirty);
    }

    /**
     * <p>Getter for the field <code>mainUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.home.HomeUIModel} object.
     */
    public HomeUIModel getMainUIModel() {
        return mainUIModel;
    }

    /**
     * <p>Setter for the field <code>mainUIModel</code>.</p>
     *
     * @param mainUIModel a {@link fr.ifremer.reefdb.ui.swing.content.home.HomeUIModel} object.
     */
    public void setMainUIModel(HomeUIModel mainUIModel) {
        this.mainUIModel = mainUIModel;
    }

    /**
     * <p>isSamplingOperationsLoading.</p>
     *
     * @return a boolean.
     */
    public boolean isSamplingOperationsLoading() {
        return samplingOperationsLoading;
    }

    /**
     * <p>Setter for the field <code>samplingOperationsLoading</code>.</p>
     *
     * @param samplingOperationsLoading a boolean.
     */
    public void setSamplingOperationsLoading(boolean samplingOperationsLoading) {
        this.samplingOperationsLoading = samplingOperationsLoading;
        firePropertyChange(PROPERTY_SAMPLING_OPERATIONS_LOADING, null, samplingOperationsLoading);
    }

    /**
     * <p>isSelectedSurveyEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isSelectedSurveyEditable() {
        return selectedSurveyEditable;
    }

    /**
     * <p>Setter for the field <code>selectedSurveyEditable</code>.</p>
     *
     * @param selectedSurveyEditable a boolean.
     */
    public void setSelectedSurveyEditable(boolean selectedSurveyEditable) {
        this.selectedSurveyEditable = selectedSurveyEditable;
        firePropertyChange(PROPERTY_SELECTED_SURVEY_EDITABLE, null, selectedSurveyEditable);
    }
}
