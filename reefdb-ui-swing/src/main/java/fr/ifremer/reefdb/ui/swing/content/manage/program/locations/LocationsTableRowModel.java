package fr.ifremer.reefdb.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.GroupingDTO;
import fr.ifremer.reefdb.dto.referential.HarbourDTO;
import fr.ifremer.reefdb.dto.referential.PositioningSystemDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

/**
 * Modele pour le tableau de programmes.
 */
public class LocationsTableRowModel extends AbstractReefDbRowUIModel<AppliedStrategyDTO, LocationsTableRowModel> implements AppliedStrategyDTO {

    /**
     * Binder from.
     */
    private static final Binder<AppliedStrategyDTO, LocationsTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(AppliedStrategyDTO.class, LocationsTableRowModel.class);

    /**
     * Binder to.
     */
    private static final Binder<LocationsTableRowModel, AppliedStrategyDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(LocationsTableRowModel.class, AppliedStrategyDTO.class);

    /**
     * Constructor.
     */
    public LocationsTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected AppliedStrategyDTO newBean() {
        return ReefDbBeanFactory.newAppliedStrategyDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        return delegateObject.getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLabel(String label) {
        delegateObject.setLabel(label);
    }

    /** {@inheritDoc} */
    @Override
    public Double getBathymetry() {
        return delegateObject.getBathymetry();
    }

    /** {@inheritDoc} */
    @Override
    public void setBathymetry(Double bathymetry) {
        delegateObject.setBathymetry(bathymetry);
    }

    /** {@inheritDoc} */
    @Override
    public Double getUtFormat() {
        return delegateObject.getUtFormat();
    }

    /** {@inheritDoc} */
    @Override
    public void setUtFormat(Double utFormat) {
        delegateObject.setUtFormat(utFormat);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean getDayLightSavingTime() {
        return delegateObject.getDayLightSavingTime();
    }

    /** {@inheritDoc} */
    @Override
    public void setDayLightSavingTime(Boolean dayLightSavingTime) {
        delegateObject.setDayLightSavingTime(dayLightSavingTime);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioning() {
        return delegateObject.getPositioning();
    }

    /** {@inheritDoc} */
    @Override
    public void setPositioning(PositioningSystemDTO positioning) {
        delegateObject.setPositioning(positioning);
    }

    /** {@inheritDoc} */
    @Override
    public CoordinateDTO getCoordinate() {
        return delegateObject.getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public void setCoordinate(CoordinateDTO coordinate) {
        delegateObject.setCoordinate(coordinate);
    }

    /** {@inheritDoc} */
    @Override
    public LocalDate getStartDate() {
        return delegateObject.getStartDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setStartDate(LocalDate startDate) {
        delegateObject.setStartDate(startDate);
    }

    /** {@inheritDoc} */
    @Override
    public LocalDate getEndDate() {
        return delegateObject.getEndDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setEndDate(LocalDate endDate) {
        delegateObject.setEndDate(endDate);
    }

    @Override
    public LocalDate getPreviousStartDate() {
        return delegateObject.getPreviousStartDate();
    }

    @Override
    public void setPreviousStartDate(LocalDate previousStartDate) {
        delegateObject.setPreviousStartDate(previousStartDate);
    }

    @Override
    public LocalDate getPreviousEndDate() {
        return delegateObject.getPreviousEndDate();
    }

    @Override
    public void setPreviousEndDate(LocalDate previousEndDate) {
        delegateObject.setPreviousEndDate(previousEndDate);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getDepartment() {
        return delegateObject.getDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepartment(DepartmentDTO departement) {
        delegateObject.setDepartment(departement);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getAppliedStrategyId() {
        return delegateObject.getAppliedStrategyId();
    }

    /** {@inheritDoc} */
    @Override
    public void setAppliedStrategyId(Integer appliedStrategyId) {
        delegateObject.setAppliedStrategyId(appliedStrategyId);
    }

    /** {@inheritDoc} */
    @Override
    public GroupingDTO getGrouping() {
        return delegateObject.getGrouping();
    }

    /** {@inheritDoc} */
    @Override
    public void setGrouping(GroupingDTO grouping) {
        delegateObject.setGrouping(grouping);
    }

    /** {@inheritDoc} */
    @Override
    public HarbourDTO getHarbour() {
        return delegateObject.getHarbour();
    }

    /** {@inheritDoc} */
    @Override
    public void setHarbour(HarbourDTO harbour) {
        delegateObject.setHarbour(harbour);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
