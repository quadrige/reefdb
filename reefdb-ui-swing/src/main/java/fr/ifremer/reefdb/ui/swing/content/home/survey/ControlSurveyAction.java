package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUI;
import fr.ifremer.reefdb.ui.swing.content.home.SaveAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de changer l'état d'une observation dans le tableau des
 * observations de l ecran d accueil.
 */
public class ControlSurveyAction extends AbstractReefDbAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

    /**
     * Loggeur.
     */
    private static final Log LOG = LogFactory.getLog(ControlSurveyAction.class);

    Set<SurveysTableRowModel> selectedSurveys;

    /**
     * Constructor.
     *
     * @param handler Controleur
     */
    public ControlSurveyAction(final SurveysTableUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucune Observation de selectionne");
            return false;
        }

        selectedSurveys = getModel().getSelectedRows();

        boolean alreadyControlled = false;
        for (SurveysTableRowModel rowModel : selectedSurveys) {
            if (rowModel.getValidationDate() != null) {
                // On ne peut pas controler une observation qui a déja été validée
                getContext().getDialogHelper().showWarningDialog(t("reefdb.action.control.survey.error.alreadyValid"));
                return false;
            }
            if (!alreadyControlled && rowModel.getControlDate() != null) {
                alreadyControlled = true;
            }
        }

        return getContext().getDialogHelper().showOptionDialog(null,
                ApplicationUIUtil.getHtmlString(alreadyControlled ? t("reefdb.action.control.survey.multiple.message") : t("reefdb.action.control.survey.message")),
                alreadyControlled ? t("reefdb.action.control.survey.multiple.titre") : t("reefdb.action.control.survey.titre"),
                JOptionPane.QUESTION_MESSAGE,
                DialogHelper.CUSTOM_OPTION,
                t("reefdb.common.control"),
                t("reefdb.common.cancel")
        ) == JOptionPane.OK_OPTION;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        SaveAction saveAction = getContext().getActionFactory().createLogicAction(getUI().getParentContainer(HomeUI.class).getHandler(), SaveAction.class);
        saveAction.setSurveysToSave(selectedSurveys);
        saveAction.setShowControlIfSuccess(true);
        saveAction.setUpdateControlDateWhenControlSucceed(true);
        getContext().getActionEngine().runFullInternalAction(saveAction);
    }
}
