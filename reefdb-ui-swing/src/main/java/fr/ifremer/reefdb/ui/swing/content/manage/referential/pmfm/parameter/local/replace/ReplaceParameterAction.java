package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.ManageParametersUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractReplaceAction;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class ReplaceParameterAction extends AbstractReplaceAction<ParameterDTO, ReplaceParameterUIModel, ReplaceParameterUI, ReplaceParameterUIHandler> {

    /**
     * <p>Constructor for ReplaceParameterAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local.replace.ReplaceParameterUIHandler} object.
     */
    public ReplaceParameterAction(ReplaceParameterUIHandler handler) {
        super(handler);
    }

    /** {@inheritDoc} */
    @Override
    protected String getReferentialLabel() {
        return t("reefdb.property.pmfm.parameter");
    }

    /** {@inheritDoc} */
    @Override
    protected boolean prepareReplaceReferential(ReferentialService service, ParameterDTO source, ParameterDTO target) {

        if (service.isParameterUsedInProgram(source.getCode())) {
            String decoratedSource = decorate(source);
            if (getContext().getDialogHelper().showConfirmDialog(getReplaceUI(), t("reefdb.replaceReferential.used.programStrategy.message", decoratedSource, decoratedSource),
                    getReplaceUI().getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return false;
            }
            setDelete(false);
        }

        if (service.isParameterUsedInRules(source.getCode())) {
            String decoratedSource = decorate(source);
            if (getContext().getDialogHelper().showConfirmDialog(getReplaceUI(), t("reefdb.replaceReferential.used.controlRules.message", decoratedSource, decoratedSource),
                    getReplaceUI().getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return false;
            }
            setDelete(false);
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    protected void replaceReferential(ReferentialService service, ParameterDTO source, ParameterDTO target, boolean delete) {

        service.replaceParameter(source, target, delete);
    }

    /** {@inheritDoc} */
    @Override
    protected void resetCaches() {
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        final ManageParametersUI ui = (ManageParametersUI) getContext().getMainUI().getHandler().getCurrentBody();
        // reload table
        if (ui != null) {
            SwingUtilities.invokeLater(() -> getActionEngine().runAction(ui.getManageParametersLocalUI().getMenuUI().getSearchButton()));
        }

    }
}
