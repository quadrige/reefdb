package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.dao.technical.Images;
import fr.ifremer.quadrige3.core.dao.technical.enumeration.EnumValue;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 18/06/2019.
 */
public enum PhotoViewType implements EnumValue {

    VIEW_DIAPO("DIAPO", n("reefdb.photo.type.diaporama"), Images.ImageType.DIAPO),
    VIEW_THUMBNAIL("THUMBNAIL", n("reefdb.photo.type.thumbnail"), Images.ImageType.THUMBNAIL);

    private final String code;
    private final String i18nLabel;
    private final Images.ImageType imageType;


    PhotoViewType(String code, String i18nLabel, Images.ImageType imageType) {
        this.code = code;
        this.i18nLabel = i18nLabel;
        this.imageType = imageType;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return t(i18nLabel);
    }

    public Images.ImageType getImageType() {
        return imageType;
    }
}
