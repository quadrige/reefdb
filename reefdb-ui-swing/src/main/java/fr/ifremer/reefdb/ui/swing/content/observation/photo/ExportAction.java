package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Files;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Import action.
 */
public class ExportAction extends AbstractReefDbAction<PhotosTabUIModel, PhotosTabUI, PhotosTabUIHandler> {

    private File destDir;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public ExportAction(PhotosTabUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || getModel().getSelectedRows().isEmpty()) {
            return false;
        }

        destDir = chooseDirectory(t("reefdb.action.photo.export.chooseDirectory.title"),
                t("reefdb.action.photo.export.chooseDirectory.buttonLabel"));

        return destDir != null;

    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        for (PhotosTableRowModel photo : getModel().getSelectedRows().stream().filter(PhotosTableRowModel::isFileExists).collect(Collectors.toList())) {
            Path fileSrc = Paths.get(photo.getFullPath());
            if (!java.nio.file.Files.isRegularFile(fileSrc)) {
                throw new ReefDbBusinessException(t("quadrige3.error.file.not.exists"));
            }
            Path fileDest = destDir.toPath().resolve(fileSrc.getFileName());
            Files.copyFile(fileSrc, fileDest);
        }

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        String text = t("reefdb.action.photo.export.done",
                destDir.getAbsoluteFile().toURI(),
                destDir.getAbsolutePath()
                );

        JEditorPane editorPane = new JEditorPane("text/html", text);
        editorPane.setEditable(false);
        editorPane.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                ApplicationUIUtil.openLink(url);
            }
        });

        getContext().getDialogHelper().showOptionDialog(
                getUI(),
                editorPane,
                t("reefdb.action.photo.export.title"),
                JOptionPane.INFORMATION_MESSAGE,
                JOptionPane.DEFAULT_OPTION
        );

        super.postSuccessAction();
    }
}
