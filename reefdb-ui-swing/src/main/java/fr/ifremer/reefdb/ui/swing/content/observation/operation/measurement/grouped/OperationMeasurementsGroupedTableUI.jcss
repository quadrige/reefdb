/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
#tableauBasPanel {
	border: {BorderFactory.createTitledBorder("")};
	_panelType: {ReefDbUI.EDITION_PANEL_TYPE};
}

#fullScreenToggleButton {
    text: "reefdb.table.view.fullScreen";
    toggledText: {t("reefdb.table.view.unFullScreen")};
    actionIcon: fullScreen;
    toggledIcon: {SwingUtil.createActionIcon("unFullScreen")};
    toolTipText: "reefdb.table.view.fullScreen.tip";
    toggledToolTipText: {t("reefdb.table.view.unFullScreen.tip")};
}

#addButton {
	actionIcon: add;
	text: "reefdb.common.new";
	toolTipText: "reefdb.samplingOperation.measurement.grouped.new.tip";
	enabled: {model.getSurvey().isEditable() && !model.getPmfms().isEmpty()};
}

#deleteButton {
	actionIcon: delete;
	text: "reefdb.common.delete";
	toolTipText: "reefdb.samplingOperation.measurement.grouped.delete.tip";
	enabled: {model.getSurvey().isEditable() && !model.getSelectedRows().isEmpty() && !model.getPmfms().isEmpty()};
}

#duplicateButton {
	actionIcon: copy;
	text: "reefdb.action.duplicate.label";
	toolTipText: "reefdb.action.duplicate.measurement.tip";
	enabled: {model.getSurvey().isEditable() && !model.getSelectedRows().isEmpty() && (model.getSelectedRows().size() == 1) && !model.getPmfms().isEmpty()};
}

#initDataGridButton {
	actionIcon: edit;
	text: "reefdb.common.init";
	toolTipText: "reefdb.samplingOperation.measurement.grouped.init.tip";
	enabled: {model.getSurvey().isEditable() && model.isPitTransitionGridInitializationEnabled() && !model.getPmfms().isEmpty()};
}

#multiEditButton {
	actionIcon: edit;
	text: "reefdb.common.edit";
	toolTipText: "reefdb.measurement.grouped.multiEdit.tip";
	enabled: {model.getSurvey().isEditable() && !model.getSelectedRows().isEmpty() && (model.getSelectedRows().size() >= 2) && !model.getPmfms().isEmpty()};
}

#tableBlockLayer {
    blockingColor: {handler.getConfig().getColorBlockingLayer()};
    block:{model.isLoading()};
}