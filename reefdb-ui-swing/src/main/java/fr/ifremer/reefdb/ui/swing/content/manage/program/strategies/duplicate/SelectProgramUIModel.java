package fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.duplicate;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * <p>SelectProgramUIModel class.</p>
 *
 */
public class SelectProgramUIModel extends AbstractEmptyUIModel<SelectProgramUIModel> {

    private ProgramDTO sourceProgram;
    /** Constant <code>PROPERTY_SOURCE_PROGRAM="sourceProgram"</code> */
    public static final String PROPERTY_SOURCE_PROGRAM = "sourceProgram";

    private StrategyDTO sourceStrategy;
    /** Constant <code>PROPERTY_SOURCE_STRATEGY="sourceStrategy"</code> */
    public static final String PROPERTY_SOURCE_STRATEGY = "sourceStrategy";

    private ProgramDTO targetProgram;
    /** Constant <code>PROPERTY_TARGET_PROGRAM="targetProgram"</code> */
    public static final String PROPERTY_TARGET_PROGRAM = "targetProgram";

    /**
     * <p>Getter for the field <code>sourceProgram</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public ProgramDTO getSourceProgram() {
        return sourceProgram;
    }

    /**
     * <p>Setter for the field <code>sourceProgram</code>.</p>
     *
     * @param sourceProgram a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void setSourceProgram(ProgramDTO sourceProgram) {
        this.sourceProgram = sourceProgram;
        firePropertyChange(PROPERTY_SOURCE_PROGRAM, null, sourceProgram);
    }

    /**
     * <p>Getter for the field <code>sourceStrategy</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO} object.
     */
    public StrategyDTO getSourceStrategy() {
        return sourceStrategy;
    }

    /**
     * <p>Setter for the field <code>sourceStrategy</code>.</p>
     *
     * @param sourceStrategy a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO} object.
     */
    public void setSourceStrategy(StrategyDTO sourceStrategy) {
        this.sourceStrategy = sourceStrategy;
        firePropertyChange(PROPERTY_SOURCE_STRATEGY, null, sourceStrategy);
    }

    /**
     * <p>Getter for the field <code>targetProgram</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public ProgramDTO getTargetProgram() {
        return targetProgram;
    }

    /**
     * <p>Setter for the field <code>targetProgram</code>.</p>
     *
     * @param targetProgram a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void setTargetProgram(ProgramDTO targetProgram) {
        this.targetProgram = targetProgram;
        firePropertyChange(PROPERTY_TARGET_PROGRAM, null, targetProgram);
    }
}
