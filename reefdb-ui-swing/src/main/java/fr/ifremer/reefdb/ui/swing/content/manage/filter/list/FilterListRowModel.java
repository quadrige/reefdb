package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Row model.
 */
public class FilterListRowModel extends AbstractReefDbRowUIModel<FilterDTO, FilterListRowModel> implements FilterDTO, ErrorAware {

    private static final Binder<FilterDTO, FilterListRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(FilterDTO.class, FilterListRowModel.class);

    private static final Binder<FilterListRowModel, FilterDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(FilterListRowModel.class, FilterDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for FilterListRowModel.</p>
     */
    public FilterListRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected FilterDTO newBean() {
        return ReefDbBeanFactory.newFilterDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getFilterTypeId() {
        return delegateObject.getFilterTypeId();
    }

    /** {@inheritDoc} */
    @Override
    public void setFilterTypeId(Integer filterTypeId) {
        delegateObject.setFilterTypeId(filterTypeId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFilterLoaded() {
        return delegateObject.isFilterLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setFilterLoaded(boolean filterLoaded) {
        delegateObject.setFilterLoaded(filterLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public List<? extends QuadrigeBean> getElements() {
        return delegateObject.getElements();
    }

    /** {@inheritDoc} */
    @Override
    public void setElements(List<? extends QuadrigeBean> elements) {
        delegateObject.setElements(elements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }



}
