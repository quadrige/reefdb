package fr.ifremer.reefdb.ui.swing.content.home;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant la recherche des obervations.
 */
public class SearchAction extends AbstractCheckModelAction<HomeUIModel, HomeUI, HomeUIHandler> {

    private static final Log log = LogFactory.getLog(SearchAction.class);
    private List<SurveyDTO> surveys;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final HomeUIHandler handler) {
        super(handler, false);
        setActionDescription(t("reefdb.action.search.tip"));
    }

    /** {@inheritDoc} */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().setModify(modelModify);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getModel().isValid();
    }

    @Override
    public boolean prepareAction() throws Exception {
        if (getContext().isActionInProgress(this.getClass())) {
            log.warn("Action already running, skip.");
            return false;
        }
        boolean prepared = super.prepareAction();
        if (prepared) {
            getContext().setActionInProgress(this.getClass(), true);
        }
        return prepared;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Tell surveys table handler a load has begun
        getUI().getSurveysTable().getHandler().beforeLoad();

        // Les parametres de recherche
        final SurveyFilterDTO surveyFilter = ReefDbBeanFactory.newSurveyFilterDTO();

        surveyFilter.setCampaignId(getModel().getCampaignId());
        surveyFilter.setProgramCode(getModel().getProgramCode());
        surveyFilter.setLocationId(getModel().getLocationId());
        surveyFilter.setDate1(getModel().getStartDate());
        surveyFilter.setDate2(getModel().getEndDate());
        surveyFilter.setSearchDateId(getModel().getSearchDateId());
        surveyFilter.setComment(getModel().getComment());
        surveyFilter.setStateId(getModel().getStateId());
        surveyFilter.setShareId(getModel().getShareId());
        getContext().setSurveyFilter(surveyFilter);

        // Chargement des observations
        surveys = getContext().getObservationService().getSurveys(surveyFilter);
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        return ImmutableList.of(
                getModel().getSurveysTableUIModel(),
                getModel().getOperationsTableUIModel()
        );
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getUI().getSurveysTable().getSurveyTable().clearSelection();

        getModel().getSurveysTableUIModel().setBeans(surveys);
        // The load is done
        getUI().getSurveysTable().getHandler().afterLoad();

        if (getContext().getSelectedSurveyId() != null) {

            // Selection de la ligne du tableau
            getUI().getSurveysTable().getHandler().selectRowById(getContext().getSelectedSurveyId());
        }

    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        surveys = null;
        getContext().setActionInProgress(this.getClass(), false);
    }
}
