package fr.ifremer.reefdb.ui.swing.content.synchro.changes;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Modele pour le tableau de programmes.
 */
public class SynchroChangesRowModel extends AbstractReefDbRowUIModel<SynchroRowDTO, SynchroChangesRowModel> implements SynchroRowDTO {

    /**
     * Binder from.
     */
    private static final Binder<SynchroRowDTO, SynchroChangesRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(SynchroRowDTO.class, SynchroChangesRowModel.class);

    /**
     * Binder to.
     */
    private static final Binder<SynchroChangesRowModel, SynchroRowDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(SynchroChangesRowModel.class, SynchroRowDTO.class);

    /**
     * Constructor.
     */
    public SynchroChangesRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        setValid(true);
    }

    /**
     * <p>getName.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /**
     * <p>getOperationType.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOperationType() {
        return delegateObject.getOperationType();
    }

    /** {@inheritDoc} */
    public void setOperationType(String operationType) {
        delegateObject.setOperationType(operationType);
    }

    /**
     * <p>getId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return delegateObject.getId();
    }

    /** {@inheritDoc} */
    public void setId(Integer id) {
        delegateObject.setId(id);
    }

    /** {@inheritDoc} */
    @Override
    protected SynchroRowDTO newBean() {
        return ReefDbBeanFactory.newSynchroRowDTO();
    }

    /**
     * <p>getPkStr.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPkStr() {
        return delegateObject.getPkStr();
    }

    /** {@inheritDoc} */
    public void setPkStr(String pkStr) {
        delegateObject.setPkStr(pkStr);
    }

    /**
     * <p>getStrategy.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStrategy() {
        return delegateObject.getStrategy();
    }

    /** {@inheritDoc} */
    public void setStrategy(String strategy) {
        delegateObject.setStrategy(strategy);
    }
}
