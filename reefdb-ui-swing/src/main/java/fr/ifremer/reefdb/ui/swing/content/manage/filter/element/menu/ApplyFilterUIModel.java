package fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Model.
 */
public class ApplyFilterUIModel extends AbstractEmptyUIModel<ApplyFilterUIModel> {

    /**
     * Filtered elements.
     */
    private List<? extends QuadrigeBean> elements;
    /** Constant <code>PROPERTY_ELEMENTS="elements"</code> */
    public static final String PROPERTY_ELEMENTS = "elements";

    private FilterDTO filter;
    /** Constant <code>PROPERTY_FILTER="filter"</code> */
    public static final String PROPERTY_FILTER = "filter";
    
    private boolean adjusting;

    /**
     * Called by FilterUI validator (FilterUIModel-error-validation.xml)
     *
     * @return a boolean.
     */
    public boolean isElementsNotEmpty() {
        return CollectionUtils.isNotEmpty(elements);
    }

    /**
     * <p>Getter for the field <code>elements</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<? extends QuadrigeBean> getElements() {
        return elements;
    }

    /**
     * <p>Setter for the field <code>elements</code>.</p>
     *
     * @param elements a {@link java.util.List} object.
     */
    public void setElements(List<? extends QuadrigeBean> elements) {
        if (adjusting) {
            return;
        }
        this.elements = elements;
        firePropertyChange(PROPERTY_ELEMENTS, null, elements);
        firePropertyChange(PROPERTY_MODIFY, false, true);
    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public FilterDTO getFilter() {
        return filter;
    }

    /**
     * <p>Setter for the field <code>filter</code>.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public void setFilter(FilterDTO filter) {
        FilterDTO oldValue = getFilter();
        this.filter = filter;
        firePropertyChange(PROPERTY_FILTER, oldValue, filter);
    }
    
    /**
     * <p>Setter for the field <code>adjusting</code>.</p>
     *
     * @param adjusting a boolean.
     */
    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

}
