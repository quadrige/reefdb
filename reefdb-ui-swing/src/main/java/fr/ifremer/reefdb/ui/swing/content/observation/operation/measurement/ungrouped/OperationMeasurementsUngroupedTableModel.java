package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.ungrouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau du haut (Psfm) pour l onglet des mesures des prelevements.
 */
public class OperationMeasurementsUngroupedTableModel extends AbstractReefDbTableModel<OperationMeasurementsUngroupedRowModel> {

    /**
     * Identifiant pour la colonne mnemonique.
     */
    public static final ReefDbColumnIdentifier<OperationMeasurementsUngroupedRowModel> NAME = ReefDbColumnIdentifier.newId(
            OperationMeasurementsUngroupedRowModel.PROPERTY_NAME,
            n("reefdb.property.mnemonic"),
            n("reefdb.samplingOperation.measurement.mnemonic.tip"),
            String.class, true);

    /**
     * Identifiant pour la colonne analyst.
     */
    public static final ReefDbColumnIdentifier<OperationMeasurementsUngroupedRowModel> ANALYST = ReefDbColumnIdentifier.newId(
            OperationMeasurementsUngroupedRowModel.PROPERTY_ANALYST,
            n("reefdb.property.analyst"),
            n("reefdb.measurement.analyst.tip"),
            DepartmentDTO.class);

    private boolean readOnly;

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public OperationMeasurementsUngroupedTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        this.readOnly = false;
    }

    /**
     * <p>Setter for the field <code>readOnly</code>.</p>
     *
     * @param readOnly a boolean.
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationMeasurementsUngroupedRowModel createNewRow() {
        return new OperationMeasurementsUngroupedRowModel(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReefDbColumnIdentifier<OperationMeasurementsUngroupedRowModel> getFirstColumnEditing() {
        return NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationMeasurementsUngroupedTableUIModel getTableUIModel() {
        return (OperationMeasurementsUngroupedTableUIModel) super.getTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStateContext() {
        if (getTableUIModel().getSurvey() != null && getTableUIModel().getSurvey().getProgram() != null) {

            return SurveyDTO.PROPERTY_SAMPLING_OPERATIONS + '_'
                    + SamplingOperationDTO.PROPERTY_PMFMS + '_'
                    + getTableUIModel().getSurvey().getProgram().getCode();
        }

        return super.getStateContext();
    }
}
