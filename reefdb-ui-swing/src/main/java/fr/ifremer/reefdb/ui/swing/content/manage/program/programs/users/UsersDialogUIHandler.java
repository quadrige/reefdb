package fr.ifremer.reefdb.ui.swing.content.manage.program.programs.users;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler.
 */
public class UsersDialogUIHandler extends AbstractReefDbUIHandler<UsersDialogUIModel, UsersDialogUI> implements Cancelable {

    /**
     * Constant <code>DOUBLE_LIST="doubleList"</code>
     */
    public static final String DOUBLE_LIST = "doubleList";
    /**
     * Constant <code>LIST="list"</code>
     */
    public static final String LIST = "list";

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(UsersDialogUI ui) {
        super.beforeInit(ui);

        UsersDialogUIModel model = new UsersDialogUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(final UsersDialogUI ui) {
        initUI(ui);

        getUI().getUsersList().setCellRenderer(newListCellRender(PersonDTO.class));

        getModel().addPropertyChangeListener(UsersDialogUIModel.PROPERTY_PROGRAM, evt -> {

            if (getModel().getProgram() == null) {
                return;
            }

            Assert.notNull(getModel().getPrivilege(), "must set the privilege property before program");

            ProgramsTableRowModel program = getModel().getProgram();
            Collection<PersonDTO> users = null;
            switch (getModel().getPrivilege()) {
                case MANAGER:
                    users = program.getManagerPersons();
                    getModel().setCurrentUserIsManager(users.stream().anyMatch(user -> user.getId() == SecurityContextHelper.getQuadrigeUserId()));
                    getUI().setTitle(t("reefdb.program.program.managers.title", decorate(program)));
                    break;
                case RECORDER:
                    users = program.getRecorderPersons();
                    getUI().setTitle(t("reefdb.program.program.recorderPersons.title", decorate(program)));
                    break;
                case FULL_VIEWER:
                    users = program.getFullViewerPersons();
                    getUI().setTitle(t("reefdb.program.program.fullViewerPersons.title", decorate(program)));
                    break;
                case VIEWER:
                    users = program.getViewerPersons();
                    getUI().setTitle(t("reefdb.program.program.viewerPersons.title", decorate(program)));
                    break;
                case VALIDATOR:
                    users = program.getValidatorPersons();
                    getUI().setTitle(t("reefdb.program.program.validatorPersons.title", decorate(program)));
                    break;
            }

            if (program.isEditable()) {
                getUI().getListPanelLayout().setSelected(DOUBLE_LIST);

                // update filterUI
                getUI().getFilterElementUserUI().getFilterDoubleList().getModel().setSelected(Lists.newArrayList(users));
                getUI().getFilterElementUserUI().getHandler().enable();

                if (!program.isLocal()) {
                    // if program is national, user menu must search only national users
                    getUI().getFilterElementUserUI().getHandler().forceLocal(false);
                }

            } else {
                getUI().getListPanelLayout().setSelected(LIST);

                getUI().getUsersList().setListData(users.toArray(new PersonDTO[0]));
            }

        });

    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // set privilege to user
        if (getModel().getProgram().isEditable()) {

            List<PersonDTO> users = getUI().getFilterElementUserUI().getModel().getElements();

            switch (getModel().getPrivilege()) {

                case MANAGER:
                    // Mantis #0027643: add confirmation to remove current user from manager list
                    // Mantis #59066: only if it was in initial list
                    if (getModel().isCurrentUserIsManager() && ReefDbBeans.findByProperty(users, PersonDTO.PROPERTY_ID, SecurityContextHelper.getQuadrigeUserId()) == null) {
                        if (getContext().getDialogHelper().showConfirmDialog(getUI(),
                            t("reefdb.program.program.managers.removeCurrent.message", decorate(getModel().getProgram())),
                            t("reefdb.program.program.managers.title", decorate(getModel().getProgram())),
                            JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                            closeDialog();
                            return;
                        }
                    }
                    getModel().getProgram().setManagerPersons(users);
                    break;
                case RECORDER:
                    getModel().getProgram().setRecorderPersons(users);
                    break;
                case FULL_VIEWER:
                    getModel().getProgram().setFullViewerPersons(users);
                    break;
                case VIEWER:
                    getModel().getProgram().setViewerPersons(users);
                    break;
                case VALIDATOR:
                    getModel().getProgram().setValidatorPersons(users);
                    break;
            }
        }

        // close the dialog box
        closeDialog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {
        closeDialog();
    }
}
