package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.MeasurementsFilter;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.ungrouped.SurveyMeasurementsUngroupedTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour l'onglet observation mesures.
 */
public class SurveyMeasurementsTabUIHandler extends AbstractReefDbUIHandler<SurveyMeasurementsTabUIModel, SurveyMeasurementsTabUI> implements TabHandler {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SurveyMeasurementsTabUIHandler.class);

    private boolean firstLoad = true;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SurveyMeasurementsTabUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SurveyMeasurementsTabUIModel model = new SurveyMeasurementsTabUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final SurveyMeasurementsTabUI ui) {
        initUI(ui);

        // init comboboxes
        initComboBox();

        // affect models
        getModel().setUngroupedTableUIModel(getUI().getUngroupedTable().getModel());
        getModel().setGroupedTableUIModel(getUI().getGroupedTable().getModel());

        // init image
        SwingUtil.setComponentWidth(getUI().getLeftImage(), ui.getMenuPanel().getPreferredSize().width * 9 / 10);
        getUI().getLeftImage().setScaled(true);

        // Initialiser listeners
        initListeners();

        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    private void initComboBox() {

        // Initialisation des groupe de taxons
        initBeanFilterableComboBox(
                getUI().getSelectionGroupeTaxonCombo(),
                null,
                null, DecoratorService.NAME);

        getUI().getSelectionGroupeTaxonCombo().setActionEnabled(getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP));
        getUI().getSelectionGroupeTaxonCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updateTaxonGroupComboBox(getModel().getTaxon(), true);
        });

        // Intialisation des taxons
        initBeanFilterableComboBox(
                getUI().getSelectionTaxonCombo(),
                null,
                null);

        getUI().getSelectionTaxonCombo().setActionEnabled(getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON));
        getUI().getSelectionTaxonCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updateTaxonComboBox(getModel().getTaxonGroup(), true);
        });

        ReefDbUIs.forceComponentSize(getUI().getSelectionGroupeTaxonCombo());
        ReefDbUIs.forceComponentSize(getUI().getSelectionTaxonCombo());
    }

    /**
     * Initialiser listeners de l ecran.
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(SurveyMeasurementsTabUIModel.PROPERTY_OBSERVATION_MODEL, evt -> load(getModel().getObservationModel()));

        // add model listener on taxon group to filter taxons
        getModel().addPropertyChangeListener(SurveyMeasurementsTabUIModel.PROPERTY_TAXON_GROUP, evt -> {
            if (getModel().isAdjusting()) return;
            getModel().setAdjusting(true);
            updateTaxonComboBox(evt.getNewValue() == null ? null : (TaxonGroupDTO) evt.getNewValue(), false);
            getModel().setAdjusting(false);
        });

        getModel().addPropertyChangeListener(SurveyMeasurementsTabUIModel.PROPERTY_TAXON, evt -> {
            if (getModel().isAdjusting()) return;
            getModel().setAdjusting(true);
            updateTaxonGroupComboBox(evt.getNewValue() == null ? null : (TaxonDTO) evt.getNewValue(), false);
            getModel().setAdjusting(false);
        });

        // Add listeners on PROPERTY_ROWS_IN_ERROR to catch modifications and revalidate
        getModel().getUngroupedTableUIModel().addPropertyChangeListener(SurveyMeasurementsUngroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });
        getModel().getGroupedTableUIModel().addPropertyChangeListener(SurveyMeasurementsGroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });

        // Add listener on loading property and propagate to children
        getModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_LOADING, evt -> {
            getModel().getUngroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
            getModel().getGroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
        });

        // Listen tables models
        listenModelModify(getModel().getUngroupedTableUIModel());
        listenModelModify(getModel().getGroupedTableUIModel());

    }

    private void updateTaxonGroupComboBox(TaxonDTO taxon, boolean forceNoFilter) {

        getUI().getSelectionGroupeTaxonCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP));

        List<TaxonGroupDTO> taxonGroups = getModel().getObservationUIHandler().getAvailableTaxonGroups(taxon, forceNoFilter);

        getUI().getSelectionGroupeTaxonCombo().setData(taxonGroups);

        if (CollectionUtils.isEmpty(taxonGroups) && getModel().getTaxonGroup() != null) {
            getModel().setTaxonGroup(null);
            // Don't auto-select unique value (Mantis #49551)
//        } else if (taxonGroups.size() == 1) {
//            getModel().setTaxonGroup(taxonGroups.get(0));
        }

    }

    private void updateTaxonComboBox(TaxonGroupDTO taxonGroup, boolean forceNoFilter) {

        getUI().getSelectionTaxonCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON));

        List<TaxonDTO> taxons = getModel().getObservationUIHandler().getAvailableTaxons(taxonGroup, forceNoFilter);

        getUI().getSelectionTaxonCombo().setData(taxons);

        if (CollectionUtils.isEmpty(taxons) && getModel().getTaxon() != null) {
            getModel().setTaxon(null);
            // Don't auto-select unique value (Mantis #49551)
//        } else if (taxons.size() == 1) {
//            getModel().setTaxon(taxons.get(0));
        }

    }

    /**
     * Load observation.
     *
     * @param survey Observation
     */
    private void load(final ObservationUIModel survey) {

        if (survey == null || survey.getId() == null) {
            return;
        }

        if (firstLoad) {
            firstLoad = false;

            // prepare combo boxes
            updateTaxonGroupComboBox(null, false);
            updateTaxonComboBox(null, false);

            // auto-select filters if unique (Mantis #49551)
            if (CollectionUtils.size(getUI().getSelectionGroupeTaxonCombo().getData()) == 1) {
                getUI().getSelectionGroupeTaxonCombo().setSelectedItem(getUI().getSelectionGroupeTaxonCombo().getData().get(0));
            }
            if (CollectionUtils.size(getUI().getSelectionTaxonCombo().getData()) == 1) {
                getUI().getSelectionTaxonCombo().setSelectedItem(getUI().getSelectionTaxonCombo().getData().get(0));
            }
        }

        // Mantis #0027878 ignore pmfm for depth values in measurements tab
        final List<Integer> pmfmIdsToIgnore = ImmutableList.of(getConfig().getDepthValuesPmfmId());
        List<PmfmStrategyDTO> pmfmStrategies = ReefDbBeans.filterCollection(survey.getPmfmStrategies(),
                input -> input != null && input.isSurvey() && !pmfmIdsToIgnore.contains(input.getPmfm().getId()));

        // Load ungrouped data (up table)
        {

            // load pmfms for ungrouped measurements
            List<PmfmDTO> pmfms = Lists.newArrayList();
            for (PmfmStrategyDTO pmfmStrategy : pmfmStrategies) {
                if (!pmfmStrategy.isGrouping()) {
                    pmfms.add(pmfmStrategy.getPmfm());
                }
            }

            // Load other Pmfms in survey
            ReefDbBeans.fillListsEachOther(survey.getPmfms(), pmfms);

            // filter out pmfms under moratorium
            filterPmfmsUnderMoratorium(pmfms, survey);

            // Set model properties
            getModel().getUngroupedTableUIModel().setPmfms(pmfms);

            getModel().getUngroupedTableUIModel().setSurvey(survey);
        }

        // Load grouped data (down table)
        {

            List<PmfmDTO> pmfms = Lists.newArrayList();
            // find pmfms having taxon unique constraint in strategy
            List<PmfmDTO> uniquePmfms = Lists.newArrayList();
            for (PmfmStrategyDTO pmfmStrategy : pmfmStrategies) {
                if (pmfmStrategy.isGrouping()) {
                    pmfms.add(pmfmStrategy.getPmfm());
                    if (pmfmStrategy.isUnique()) {
                        uniquePmfms.add(pmfmStrategy.getPmfm());
                    }
                }
            }

            // Load other Pmfms in survey
            ReefDbBeans.fillListsEachOther(survey.getIndividualPmfms(), pmfms);

            // filter out pmfms under moratorium
            filterPmfmsUnderMoratorium(pmfms, survey);

            // Set model properties
            getModel().getGroupedTableUIModel().setPmfms(pmfms);
            getModel().getGroupedTableUIModel().setUniquePmfms(uniquePmfms);

            getModel().getGroupedTableUIModel().setSurvey(survey);
        }

        // execute doSearch to simulate loading measurements data
        doSearch();

    }

    private void filterPmfmsUnderMoratorium(List<PmfmDTO> pmfms, ObservationUIModel survey) {
        if (CollectionUtils.isEmpty(survey.getPmfmsUnderMoratorium()))
            return;

        pmfms.removeIf(pmfm -> survey.getPmfmsUnderMoratorium().stream().anyMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(pmfm, moratoriumPmfm)));
    }

    /**
     * <p>clearSearch.</p>
     */
    public void clearSearch() {

        // Suppression choix du groupe de taxons
        getModel().setTaxonGroup(null);

        // Suppression choix du taxon
        getModel().setTaxon(null);

    }

    /**
     * <p>doSearch.</p>
     */
    public void doSearch() {

        MeasurementsFilter measurementFilter = new MeasurementsFilter();
        measurementFilter.setTaxonGroup(getModel().getTaxonGroup());
        measurementFilter.setTaxon(getModel().getTaxon());
        getModel().getGroupedTableUIModel().setMeasurementFilter(measurementFilter);
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<SurveyMeasurementsTabUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void onShowTab(int currentIndex, int newIndex) {

    }

    /** {@inheritDoc} */
    @Override
    public boolean onRemoveTab() {
        return false;
    }

    public void save() {

        try {

            // Disable filter
            getModel().getGroupedTableUIModel().setMeasurementFilter(new MeasurementsFilter());

            // save ungrouped measurements
            getUI().getUngroupedTable().getHandler().save();

            // save grouped measurements
            getUI().getGroupedTable().getHandler().save();

        } finally {

            doSearch();
        }

    }
}
