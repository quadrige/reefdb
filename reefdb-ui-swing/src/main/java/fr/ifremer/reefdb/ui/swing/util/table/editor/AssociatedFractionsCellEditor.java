package fr.ifremer.reefdb.ui.swing.util.table.editor;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.associatedFractions.AssociatedFractionsUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import org.jdesktop.swingx.JXTable;

import java.awt.Dimension;

/**
 * <p>AssociatedFractionsCellEditor class.</p>
 *
 */
public class AssociatedFractionsCellEditor extends ButtonCellEditor {

	private final JXTable table;

	private final ReefDbUI parentUI;
	
	private final boolean isEditable;
	
	/**
	 * <p>Constructor for AssociatedFractionsCellEditor.</p>
	 *
	 * @param table a {@link org.jdesktop.swingx.JXTable} object.
	 * @param parentUI a {@link fr.ifremer.reefdb.ui.swing.util.ReefDbUI} object.
	 * @param isEditable a boolean.
	 */
	public AssociatedFractionsCellEditor(
			final JXTable table,
			final ReefDbUI parentUI,
			final boolean isEditable) {
		this.table = table;
		this.parentUI = parentUI;
		this.isEditable = isEditable;
	}

    /** {@inheritDoc} */
    @Override
    public void onButtonCellAction(final int rowIndex, final int column) {
    	
    	final AbstractReefDbTableModel<?> tableModel = (AbstractReefDbTableModel<?>) table.getModel();

        final int rowModelIndex = table.convertRowIndexToModel(rowIndex);
        final MatrixDTO rowModel = (MatrixDTO) tableModel.getEntry(rowModelIndex);
        
        // open associated fractions screen
		final AssociatedFractionsUI associatedFractionsUI = new AssociatedFractionsUI(parentUI);
        associatedFractionsUI.getModel().setEditable(isEditable);
        associatedFractionsUI.getModel().setMatrix(rowModel);
        parentUI.getHandler().openDialog(associatedFractionsUI, new Dimension(640, 480));
    }
}
