package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.privileges;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;


/**
 * <p>PrivilegesDialogUIModel class.</p>
 *
 */
public class PrivilegesDialogUIModel extends AbstractReefDbBeanUIModel<QuadrigeBean, PrivilegesDialogUIModel> {

    private PersonDTO user;
    /** Constant <code>PROPERTY_USER="user"</code> */
    public static final String PROPERTY_USER = "user";
    private boolean editable;
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";

    /**
     * <p>Constructor for PrivilegesDialogUIModel.</p>
     */
    protected PrivilegesDialogUIModel() {
        super(null, null);
    }

    /** {@inheritDoc} */
    @Override
    protected QuadrigeBean newBean() {
        return null;
    }

    /**
     * <p>Getter for the field <code>user</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    public PersonDTO getUser() {
        return user;
    }

    /**
     * <p>Setter for the field <code>user</code>.</p>
     *
     * @param user a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    public void setUser(PersonDTO user) {
        this.user = user;
        firePropertyChange(PROPERTY_USER, null, user);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }
}
