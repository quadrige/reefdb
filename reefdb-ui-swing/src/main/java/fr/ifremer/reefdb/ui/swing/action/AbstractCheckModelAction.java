package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Constructor.
 *
 * @param <M>  Model
 * @param <UI> UI
 * @param <H>  Handler
 */
public abstract class AbstractCheckModelAction<M extends AbstractBeanUIModel, UI extends ReefDbUI<M, ?>, H extends AbstractUIHandler<M, UI>>
        extends AbstractReefDbAction<M, UI, H> {

    /**
     * Constructor.
     *
     * @param handler  Handler
     * @param hideBody HideBody
     */
    protected AbstractCheckModelAction(final H handler, final boolean hideBody) {
        super(handler, hideBody);
    }

    /**
     * Save action class.
     *
     * @return Save action
     */
    protected abstract Class<? extends AbstractReefDbSaveAction> getSaveActionClass();

    /**
     * Model modify.
     *
     * @return True if model is modify, False otherwise
     */
    protected abstract boolean isModelModify();

    /**
     * Model modify
     *
     * @param modelModify a boolean.
     */
    protected abstract void setModelModify(boolean modelModify);

    /**
     * Model valid
     *
     * @return True if model is valid, false otherwise
     */
    protected abstract boolean isModelValid();

    /**
     * Return the handler used for the SaveAction
     *
     * @return a {@link org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler} object.
     */
    protected abstract AbstractApplicationUIHandler<?, ?> getSaveHandler();

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        boolean canContinue = super.prepareAction();
        // Model is modify
        if (canContinue && isModelModify()) {

            // Ask save modifications or not
            final int result = askSaveBeforeLeaving(t("reefdb.action.common.askSaveBeforeLeaving.message"));
            if (result == JOptionPane.YES_OPTION) {

                // Model is valid
                if (isModelValid()) {

                    // Save action
                    AbstractReefDbSaveAction saveAction = getActionFactory().createLogicAction(getSaveHandler(), getSaveActionClass());

                    if (saveAction.prepareAction()) {
                        if (this instanceof AbstractCheckBeforeChangeScreenAction) {
                            saveAction.setFromChangeScreenAction(true);
                        }
                        saveAction.setFromCheckModelAction(true);

                        getActionEngine().runInternalAction(saveAction);

                        // Continue
                        canContinue = true;
                    } else {
                        canContinue = false;
                    }
                } else {

                    // Ask user for remove error
                    displayErrorMessage(t("reefdb.action.save.errors.title"), t("reefdb.action.save.errors.remove"));

                    // Stop saving
                    canContinue = false;
                }
            } else if (result == JOptionPane.NO_OPTION) {

                // Continue
                canContinue = true;
            } else {

                // Stop closing
                canContinue = false;
            }
        }
        return canContinue;
    }

    // Override this method to gather other models to set as not modified at the end of action
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        return new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // Set each model as not modified
        getOtherModelsToModify().forEach(model -> model.setModify(false));

        //if action is done successfully, either model has been saved or changes have been canceled
        //so we set the model modify to false
        setModelModify(false);
    }
}
