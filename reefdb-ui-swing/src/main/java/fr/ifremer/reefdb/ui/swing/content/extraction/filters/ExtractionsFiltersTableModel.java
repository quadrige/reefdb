package fr.ifremer.reefdb.ui.swing.content.extraction.filters;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * extractions table model
 */
public class ExtractionsFiltersTableModel extends AbstractReefDbTableModel<ExtractionsFiltersRowModel> {

    /** Constant <code>FILTER_TYPE</code> */
    public static final ReefDbColumnIdentifier<ExtractionsFiltersRowModel> FILTER_TYPE = ReefDbColumnIdentifier.newId(
            ExtractionsFiltersRowModel.PROPERTY_FILTER_TYPE,
            n("reefdb.extraction.filters.type.short"),
            n("reefdb.extraction.filters.type.tip"),
            FilterTypeDTO.class);

    /** Constant <code>FILTER</code> */
    public static final ReefDbColumnIdentifier<ExtractionsFiltersRowModel> FILTER = ReefDbColumnIdentifier.newId(
            ExtractionsFiltersRowModel.PROPERTY_FILTER,
            n("reefdb.extraction.filters.elements.short"),
            n("reefdb.extraction.filters.elements.tip"),
            FilterDTO.class);

    /**
     * <p>Constructor for ExtractionsFiltersTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    public ExtractionsFiltersTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionsFiltersRowModel createNewRow() {
        return new ExtractionsFiltersRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<ExtractionsFiltersRowModel> getFirstColumnEditing() {
        return null;
    }

}
