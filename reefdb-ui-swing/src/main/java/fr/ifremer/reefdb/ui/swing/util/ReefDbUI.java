package fr.ifremer.reefdb.ui.swing.util;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.ApplicationUI;

/**
 * Contract to place on each generated jaxx ui.
 *
 * @param <M> type of UI model
 * @param <H> type of handler
 */
public interface ReefDbUI<M, H extends AbstractUIHandler<M, ?>> extends ApplicationUI<M, H> {

    /** Constant <code>CONTEXT_PANEL_TYPE="context"</code> */
    String CONTEXT_PANEL_TYPE = "context";
    /** Constant <code>SELECTION_PANEL_TYPE="selection"</code> */
    String SELECTION_PANEL_TYPE = "selection";
    /** Constant <code>EDITION_PANEL_TYPE="edition"</code> */
    String EDITION_PANEL_TYPE = "edition";

    /** {@inheritDoc} */
    @Override
    H getHandler();

}
