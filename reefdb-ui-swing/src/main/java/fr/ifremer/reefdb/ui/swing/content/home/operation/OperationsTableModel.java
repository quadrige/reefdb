package fr.ifremer.reefdb.ui.swing.content.home.operation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.PositioningSystemDTO;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau de prelevements pour l'ecran d accueil.
 */
public class OperationsTableModel extends AbstractReefDbTableModel<OperationsTableRowModel> {

    /**
     * Identifiant pour la colonne mnemonique.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_NAME,
            n("reefdb.property.mnemonic"),
            n("reefdb.property.mnemonic"),
            String.class,
            true);
    /**
     * Identifiant pour la colonne engin.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> SAMPLING_EQUIPMENT = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SAMPLING_EQUIPMENT,
            n("reefdb.property.samplingEquipment"),
            n("reefdb.property.samplingEquipment"),
            SamplingEquipmentDTO.class,
            true);
    /**
     * Identifiant pour la colonne heure.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> TIME = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_TIME,
            n("reefdb.property.time"),
            n("reefdb.property.time"),
            Integer.class);
    /**
     * Identifiant pour la colonne taille.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> SIZE = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SIZE,
            n("reefdb.property.size"),
            n("reefdb.property.size"),
            Double.class);
    /**
     * Identifiant pour la colonne unite taille.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> SIZE_UNIT = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SIZE_UNIT,
            n("reefdb.property.unit"),
            n("reefdb.property.unit"),
            UnitDTO.class);
    /**
     * Identifiant pour la colonne commentaire.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.comment"),
            n("reefdb.property.comment"),
            String.class);
    /**
     * Identifiant pour la colonne service preleveur.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> SAMPLING_DEPARTMENT = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_SAMPLING_DEPARTMENT,
            n("reefdb.property.department.sampler"),
            n("reefdb.home.samplingOperation.department.sampler.tip"),
            DepartmentDTO.class, true);
    /**
     * Identifiant pour la colonne analyst.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> ANALYST = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_ANALYST,
            n("reefdb.property.analyst"),
            n("reefdb.home.samplingOperation.department.analyst.tip"),
            DepartmentDTO.class);
    /**
     * Identifiant pour la colonne immersion.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> DEPTH = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_DEPTH,
            n("reefdb.property.depth.precise"),
            n("reefdb.home.samplingOperation.depth.precise.tip"),
            Double.class);

    /**
     * Identifiant pour la colonne immersion min.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> MIN_DEPTH = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_MIN_DEPTH,
            n("reefdb.property.depth.precise.min"),
            n("reefdb.home.samplingOperation.depth.precise.min.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne immersion.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> MAX_DEPTH = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_MAX_DEPTH,
            n("reefdb.property.depth.precise.max"),
            n("reefdb.home.samplingOperation.depth.precise.max.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne coordonnees (latitude).
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> LATITUDE = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_LATITUDE,
            n("reefdb.property.latitude.real"),
            n("reefdb.home.samplingOperation.latitude.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne coordonnees (longitude).
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> LONGITUDE = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_LONGITUDE,
            n("reefdb.property.longitude.real"),
            n("reefdb.home.samplingOperation.longitude.tip"),
            Double.class);
    /**
     * Identifiant pour la colonne positionnement.
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> POSITIONING = ReefDbColumnIdentifier.newId(
            OperationsTableRowModel.PROPERTY_POSITIONING,
            n("reefdb.property.positionning.name"),
            n("reefdb.home.samplingOperation.positionning.name.tip"),
            PositioningSystemDTO.class);
    /**
     * Constant <code>POSITIONING_PRECISION</code>
     */
    public static final ReefDbColumnIdentifier<OperationsTableRowModel> POSITIONING_PRECISION = ReefDbColumnIdentifier.newReadOnlyId(
            OperationsTableRowModel.PROPERTY_POSITIONING_PRECISION,
            n("reefdb.property.positionning.precision"),
            n("reefdb.home.samplingOperation.positionning.precision.tip"),
            String.class);
    private boolean readOnly;
    private boolean nameInModel;

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public OperationsTableModel(final SwingTableColumnModel columnModel, boolean createNewRow) {
        super(columnModel, createNewRow, false);
        readOnly = false;
        try {
            columnModel.getColumnIndex(NAME);
            nameInModel = true;
        } catch (IllegalArgumentException e) {
            nameInModel = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationsTableRowModel createNewRow() {
        return new OperationsTableRowModel(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReefDbColumnIdentifier<OperationsTableRowModel> getFirstColumnEditing() {
        return nameInModel ? NAME : SAMPLING_EQUIPMENT;
    }

    /**
     * <p>Setter for the field <code>readOnly</code>.</p>
     *
     * @param readOnly a boolean.
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractOperationsTableUIModel getTableUIModel() {
        return (AbstractOperationsTableUIModel) super.getTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStateContext() {
        if (getTableUIModel().getSurvey() != null && getTableUIModel().getSurvey().getProgram() != null) {

            return SurveyDTO.PROPERTY_SAMPLING_OPERATIONS + '_'
                    + SamplingOperationDTO.PROPERTY_PMFMS + '_'
                    + getTableUIModel().getSurvey().getProgram().getCode();

        }

        return super.getStateContext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<OperationsTableRowModel> propertyName) {

        boolean editable = true;
        // Positioning is allowed when coordinate is set
        if (POSITIONING == propertyName) {
            OperationsTableRowModel rowModel = getEntry(rowIndex);
            editable = rowModel.getLatitude() != null || rowModel.getLongitude() != null;
        }

        return editable && super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

    @Override
    public boolean isCreateNewRow() {
        return super.isCreateNewRow() && !this.readOnly;
    }
}
