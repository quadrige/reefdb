package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des Matrices au niveau local
 */
public class ManageMatricesMenuUIModel extends DefaultReferentialMenuUIModel {

    /** Constant <code>PROPERTY_MATRIX="matrix"</code> */
    public static final String PROPERTY_MATRIX = "matrix";
    private MatrixDTO matrix;

    /**
     * <p>getMatrixId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getMatrixId() {
        return getMatrix() == null ? null : getMatrix().getId();
    }

    /**
     * <p>Getter for the field <code>matrix</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     */
    public MatrixDTO getMatrix() {
        return matrix;
    }

    /**
     * <p>Setter for the field <code>matrix</code>.</p>
     *
     * @param matrix a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     */
    public void setMatrix(MatrixDTO matrix) {
        this.matrix = matrix;
        firePropertyChange(PROPERTY_MATRIX, null, matrix);
    }

}
