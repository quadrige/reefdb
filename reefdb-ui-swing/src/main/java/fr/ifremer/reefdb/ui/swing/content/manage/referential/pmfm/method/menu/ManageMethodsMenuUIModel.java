package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des Methods au niveau local
 */
public class ManageMethodsMenuUIModel extends DefaultReferentialMenuUIModel {

    /** Constant <code>PROPERTY_METHOD="method"</code> */
    public static final String PROPERTY_METHOD = "method";
    private MethodDTO method;

    /**
     * <p>getMethodId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getMethodId() {
        return getMethod() == null ? null : getMethod().getId();
    }

    /**
     * <p>Getter for the field <code>method</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO} object.
     */
    public MethodDTO getMethod() {
        return method;
    }

    /**
     * <p>Setter for the field <code>method</code>.</p>
     *
     * @param method a {@link fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO} object.
     */
    public void setMethod(MethodDTO method) {
        this.method = method;
        firePropertyChange(PROPERTY_METHOD, null, method);
    }

}
