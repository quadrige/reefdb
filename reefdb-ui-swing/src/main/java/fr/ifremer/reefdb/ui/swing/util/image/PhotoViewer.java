package fr.ifremer.reefdb.ui.swing.util.image;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Images;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Image Panel component that show one or more PhotoDTO in diaporama or thumbnail mode
 * <p/>
 * Created by Ludovic on 30/07/2015.
 */
public class PhotoViewer<P extends PhotoDTO> extends JPanel implements ChangeListener {

    private final static Log log = LogFactory.getLog(PhotoViewer.class);

    /**
     * Constant <code>EVENT_PHOTO_CLICKED="photoClicked"</code>
     */
    public static final String EVENT_PHOTO_CLICKED = "photoClicked";
    /**
     * Constant <code>PROPERTY_DEFAULT_THUMBNAILS_COLUMNS=6</code>
     */
    private static final int PROPERTY_DEFAULT_THUMBNAILS_COLUMNS = 6;

    private final JPanel imageGrid;
    private final GridLayout imageGridLayout;
    private final JScrollPane imageScroll;
    private final Map<P, BackgroundPanel> photoMap;
    private Images.ImageType type;
    private JSlider zoomSlider;
    private double ratio = BackgroundPanel.NO_SCALE;

    /**
     * <p>Constructor for PhotoViewer.</p>
     */
    public PhotoViewer() {
        setLayout(new BorderLayout());
        setBorder(new EmptyBorder(5, 5, 5, 5));
        photoMap = new LinkedHashMap<>();
        imageGridLayout = new GridLayout();
        imageGrid = new JPanel(imageGridLayout);
        imageScroll = new JScrollPane(imageGrid);
        add(imageScroll, BorderLayout.CENTER);
    }

    public void setType(Images.ImageType type) {
        this.type = type;

    }

    public void setPhoto(P photo, Images.ImageType type) {
        setPhotos(photo != null ? ImmutableList.of(photo) : null, type);
    }

    /**
     * <p>setPhotos.</p>
     *
     * @param photos a {@link Collection} object.
     */
    public void setPhotos(Collection<P> photos, Images.ImageType type) {

        Assert.notNull(type);
        setType(type);
        photoMap.clear();

        try {
            if (photos == null) {
                return;
            }

            for (P photo : photos) {

                if (photo == null) {
                    continue;
                }

                if (StringUtils.isBlank(photo.getFullPath())) {
                    log.warn("invalid photo object (no path)");
                    continue;
                }

                BufferedImage image = loadPhoto(photo, type);
                BackgroundPanel imagePanel = new BackgroundPanel(image, true);
                photoMap.put(photo, imagePanel);
            }

        } finally {
            updateImageGrid();
        }
    }

    /**
     * <p>setSelected.</p>
     *
     * @param photo a P object.
     */
    public void setSelected(P photo) {
        setSelected(ImmutableList.of(photo));
    }

    /**
     * <p>setSelected.</p>
     *
     * @param photos a {@link Collection} object.
     */
    public void setSelected(Collection<P> photos) {

        // reset selected status
        photoMap.values().forEach(backgroundPanel -> backgroundPanel.setSelected(false));

        if (photos == null) return;

        photos.stream().map(photoMap::get).filter(Objects::nonNull).forEach(backgroundPanel -> {
            backgroundPanel.setSelected(true);
            if (!SwingUtilities.isRectangleContainingRectangle(imageScroll.getViewport().getViewRect(), backgroundPanel.getBounds()))
                backgroundPanel.scrollRectToVisible(imageScroll.getViewport().getVisibleRect());
            backgroundPanel.revalidate();
        });
    }

    private BufferedImage loadPhoto(P photo, Images.ImageType type) {
        BufferedImage image = Images.loadImage(new File(photo.getFullPath()), type, true);
        if (image == null && type.getMaxSize() != null) {
            // if photo unavailable, draw a custom image (Mantis #48020)
            image = new BufferedImage(type.getMaxSize(), type.getMaxSize(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D graphics = image.createGraphics();
            // draw light gray background
            graphics.setColor(new Color(240, 240, 240));
            graphics.fillRect(0, 0, type.getMaxSize(), type.getMaxSize());
            // draw texts
            graphics.setColor(Color.RED.brighter());
            FontMetrics fm = graphics.getFontMetrics();
            List<String> texts = new ArrayList<>();
            texts.addAll(adjustText(fm, t("reefdb.photo.unavailable"), type.getMaxSize()));
            texts.addAll(adjustText(fm, t("reefdb.photo.unavailable.tip"), type.getMaxSize()));
            float textHeight = fm.getHeight();
            float textTotalHeight = textHeight * texts.size();
            // center in canvas
            for (int i = 0; i < texts.size(); i++) {
                String text = texts.get(i);
                graphics.drawString(text,
                        (float) type.getMaxSize() / 2 - ((float) fm.stringWidth(text)) / 2,
                        (float) type.getMaxSize() / 2 - textTotalHeight / 2 + fm.getAscent() + textHeight * i
                );
            }
        }
        return image;
    }

    private List<String> adjustText(FontMetrics fontMetrics, String text, int maxWidth) {
        if (text == null)
            text = "";
        List<String> result = new ArrayList<>();

        // separate each word by space
        String[] words = text.split("\\s+");
        if (words.length == 1) {
            // single word
            return ImmutableList.of(text);
        }
        // build line with the first word
        StringBuilder line = new StringBuilder(words[0]);
        int i = 0;
        while (i < words.length - 1) {
            // test the length of current line with the next word
            if (fontMetrics.stringWidth(line + " " + words[i + 1]) < maxWidth) {
                if (line.length() > 0) {
                    // restore the space on each words except the first one
                    line.append(" ");
                }
                // append the word to the current line
                line.append(words[++i]);
            } else {
                // this line is complete, append it to result and create a new one
                result.add(line.toString());
                line = new StringBuilder();
                if (fontMetrics.stringWidth(words[i + 1]) > maxWidth) {
                    // if the next word is too large, append it to result directly (it will be cropped)
                    result.add(words[++i]);
                }
            }
        }
        if (line.length() > 0) {
            // append the remaining line
            result.add(line.toString());
        }
        return result;
    }

    private void updateImageGrid() {

        imageGrid.removeAll();

        if (type == Images.ImageType.THUMBNAIL) {

            imageGridLayout.setColumns(PROPERTY_DEFAULT_THUMBNAILS_COLUMNS);
            imageGridLayout.setRows(0);

        } else {

            imageGridLayout.setColumns(1);
            imageGridLayout.setRows(1);

        }

        photoMap.values().forEach(backgroundPanel -> {
            initBackgroundPanel(backgroundPanel);
            imageGrid.add(backgroundPanel);
        });

        revalidate();
    }

    private void initBackgroundPanel(BackgroundPanel image) {

        // Add mouse listener
        image.addMouseListener(mouseAdapter);
        image.addMouseMotionListener(mouseAdapter);
        image.addMouseWheelListener(mouseAdapter);
        image.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        if (type == Images.ImageType.BASE) {
            createZoomSlider();
        } else if (zoomSlider != null) {
            remove(zoomSlider);
        }
    }

    private final MouseAdapter mouseAdapter = new MouseAdapter() {

        private Point origin;

        @Override
        public void mouseClicked(MouseEvent e) {
            // fire event on click
            if (e.getComponent() instanceof BackgroundPanel) {
                for (P photo : photoMap.keySet()) {
                    if (e.getComponent() == photoMap.get(photo)) {
                        firePropertyChange(EVENT_PHOTO_CLICKED, null, photo);
                        return;
                    }
                }
            }

        }

        @Override
        public void mousePressed(MouseEvent e) {
            origin = new Point(e.getPoint());
            e.getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            e.getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL)
                zoom(e.getWheelRotation());
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (origin != null) {
                JComponent component = (JComponent) e.getSource();
                int deltaX = origin.x - e.getX();
                int deltaY = origin.y - e.getY();

                Rectangle view = imageScroll.getViewport().getViewRect();
                view.x += deltaX;
                view.y += deltaY;

                component.scrollRectToVisible(view);
            }
        }

    };

    /**
     * /** {@inheritDoc}
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() != zoomSlider) return;
        if (zoomSlider.getValueIsAdjusting()) return;
        int zoom = ((JSlider) e.getSource()).getValue();
        ratio = zoom / 100.0;

        Arrays.stream(imageGrid.getComponents()).filter(component -> component instanceof BackgroundPanel)
                .map(component -> (BackgroundPanel) component)
                .forEach(backgroundPanel -> backgroundPanel.setRatio(ratio));

        revalidate();
    }

    private void createZoomSlider() {
        zoomSlider = new JSlider(JSlider.HORIZONTAL, 10, 100, 100);
        zoomSlider.setMajorTickSpacing(10);
        zoomSlider.setPaintTicks(true);
        zoomSlider.setPaintLabels(true);
        zoomSlider.setSnapToTicks(true);
        zoomSlider.addChangeListener(this);
        add(zoomSlider, BorderLayout.PAGE_END);
    }

    private void zoom(int amount) {
        if (zoomSlider == null || amount == 0) return;

        zoomSlider.setValue(zoomSlider.getValue() + (10 * (amount < 0 ? 1 : -1)));

    }
}
