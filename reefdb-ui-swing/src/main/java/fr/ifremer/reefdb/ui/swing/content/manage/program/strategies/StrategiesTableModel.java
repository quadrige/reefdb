package fr.ifremer.reefdb.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class StrategiesTableModel extends AbstractReefDbTableModel<StrategiesTableRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final ReefDbColumnIdentifier<StrategiesTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		StrategiesTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.program.strategy.name.tip"),
            String.class,
			true);
    
    /**
     * Identifiant pour la colonne description.
     */
    public static final ReefDbColumnIdentifier<StrategiesTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
    		StrategiesTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.description"),
            n("reefdb.program.strategy.description.tip"),
            String.class,
			true);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public StrategiesTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public StrategiesTableRowModel createNewRow() {
		return new StrategiesTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<StrategiesTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
