package fr.ifremer.reefdb.ui.swing.content;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.content.AbstractMainUIHandler;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.ui.swing.ReefDbApplication;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.URL;

/**
 * <p>ReefDbMainUIHandler class.</p>
 *
 * @since 0.1
 */
public class ReefDbMainUIHandler extends AbstractMainUIHandler<ReefDbUIContext, ReefDbMainUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReefDbMainUIHandler.class);

    @Override
    public ReefDbUIContext getContext() {
        return (ReefDbUIContext) super.getContext();
    }

    @Override
    public ReefDbConfiguration getConfig() {
        return (ReefDbConfiguration) super.getConfig();
    }

    //------------------------------------------------------------------------//
    //-- AbstractReefDbUIHandler methods                                     --//
    //------------------------------------------------------------------------//
    /** {@inheritDoc} */
    @Override
    public void beforeInit(ReefDbMainUI ui) {
        super.beforeInit(ui);

        // override context value
        ui.setContextValue(getContext());
        ui.createModel();
        ui.setContextValue(SwingUtil.createActionIcon("observation"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ReefDbMainUI ui) {

        super.afterInit(ui);

        ui.getMenuFile().add(ui.getMenuFileExit());

        // Don't add language menu until translation is done (Mantis #48927)
//        ui.getMenuHelp().add(ui.getMenuChangeLocale());

    }

    /**
     * <p>reloadUI.</p>
     */
    public void reloadUI() {

        //close ui
        onCloseUI();

        // restart ui
        ReefDbApplication.reloadUI(getContext());
    }

    /**
     * <p>gotoSite.</p>
     */
    public void gotoSite() {
        ReefDbConfiguration config = getConfig();

        URL siteURL = config.getSiteUrl();

        if (LOG.isDebugEnabled()) {
            LOG.debug("goto " + siteURL);
        }
        ReefDbUIs.openLink(siteURL);
    }

    /**
     * <p>showHelp.</p>
     */
    public void showHelp() {
        getContext().showHelp(ui, ui.getBroker(), null);
    }


}
