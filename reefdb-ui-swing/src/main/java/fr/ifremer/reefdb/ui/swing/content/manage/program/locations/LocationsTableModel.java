package fr.ifremer.reefdb.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class LocationsTableModel extends AbstractReefDbTableModel<LocationsTableRowModel> {

	/**
	 * Identifiant pour la colonne code = location id
	 */
    public static final ReefDbColumnIdentifier<LocationsTableRowModel> CODE = ReefDbColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_ID,
            n("reefdb.property.code"),
            n("reefdb.program.location.code.tip"),
            Integer.class);

	/**
	 * Identifiant pour la colonne label.
	 */
    public static final ReefDbColumnIdentifier<LocationsTableRowModel> LABEL = ReefDbColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_LABEL,
            n("reefdb.property.label"),
            n("reefdb.program.location.label.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne name.
     */
    public static final ReefDbColumnIdentifier<LocationsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_NAME,
            n("reefdb.program.location.name.short"),
            n("reefdb.program.location.name.tip"),
            String.class);

    /**
     * Identifiant pour la colonne date debut.
     */
    public static final ReefDbColumnIdentifier<LocationsTableRowModel> START_DATE = ReefDbColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_START_DATE,
            n("reefdb.program.location.startDate.short"),
            n("reefdb.program.location.startDate.tip"),
			LocalDate.class);

    /**
     * Identifiant pour la colonne date fin.
     */
    public static final ReefDbColumnIdentifier<LocationsTableRowModel> END_DATE = ReefDbColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_END_DATE,
            n("reefdb.program.location.endDate.short"),
            n("reefdb.program.location.endDate.tip"),
			LocalDate.class);

    /**
     * Identifiant pour la colonne preleveur.
     */
    public static final ReefDbColumnIdentifier<LocationsTableRowModel> PRELEVEUR = ReefDbColumnIdentifier.newId(
    		LocationsTableRowModel.PROPERTY_DEPARTMENT,
            n("reefdb.program.location.department"),
            n("reefdb.program.location.department.tip"),
            DepartmentDTO.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public LocationsTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public LocationsTableRowModel createNewRow() {
		return new LocationsTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<LocationsTableRowModel> getFirstColumnEditing() {
		return CODE;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<LocationsTableRowModel> propertyName) {

		if (propertyName == PRELEVEUR) {
			LocationsTableRowModel rowModel = getEntry(rowIndex);
			return rowModel.getStartDate() != null && rowModel.getEndDate() != null;
		}

		return super.isCellEditable(rowIndex, columnIndex, propertyName);
	}
}
