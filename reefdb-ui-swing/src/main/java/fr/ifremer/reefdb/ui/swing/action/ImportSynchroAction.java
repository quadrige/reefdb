package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.synchro.vo.SynchroProgressionStatus;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroDirection;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIContext;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIHandler;
import fr.ifremer.quadrige3.ui.swing.synchro.action.AbstractSynchroAction;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroCheckAction;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroDirectAction;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroStartAction;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;
import fr.ifremer.reefdb.ui.swing.content.synchro.program.ProgramSelectUI;
import org.apache.commons.collections4.CollectionUtils;

import java.awt.Dimension;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Main import synchro action
 * <p>
 * Created by Ludovic on 18/05/2015.
 */
public class ImportSynchroAction extends AbstractReefDbMainUIAction {

    private AbstractSynchroAction nextSynchroAction;
    private boolean referentialOnly = false;
    private boolean silentIfNoUpdate = false;
    private boolean silentIfUpdate = false;

    /**
     * <p>Constructor for ImportSynchroAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    public ImportSynchroAction(ReefDbMainUIHandler handler) {
        super(handler, false);
    }

    private SynchroUIContext getSynchroUIContext() {
        return getContext().getSynchroContext();
    }

    private SynchroUIHandler getSynchroHandler() {
        return getContext().getSynchroHandler();
    }

    /**
     * <p>Setter for the field <code>referentialOnly</code>.</p>
     *
     * @param referentialOnly a boolean.
     */
    public void setReferentialOnly(boolean referentialOnly) {
        this.referentialOnly = referentialOnly;
    }

    /**
     * <p>Setter for the field <code>silentIfNoUpdate</code>.</p>
     *
     * @param silentIfNoUpdate a boolean.
     */
    public void setSilentIfNoUpdate(boolean silentIfNoUpdate) {
        this.silentIfNoUpdate = silentIfNoUpdate;
    }

    public void setSilentIfUpdate(boolean silentIfUpdate) {
        this.silentIfUpdate = silentIfUpdate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {

        if (!getContext().isSynchroEnabled() && getConfig().isSynchronizationUsingServer()) {
            getContext().getErrorHelper().showWarningDialog(t("reefdb.synchro.unavailable"));
            return false;
        }

        // Local user could not import with data
        if (!referentialOnly && getContext().isAuthenticatedAsLocalUser()) {
            getContext().getErrorHelper().showWarningDialog(t("reefdb.error.authenticate.accessDenied"));
            return false;
        }

        return super.prepareAction();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {
        // test actual progression
        getSynchroUIContext().getProgressionModel().clear();
        getSynchroUIContext().setDirection(SynchroDirection.IMPORT);
        getSynchroUIContext().loadImportContext();
        boolean isSynchronizationUsingServer = getConfig().isSynchronizationUsingServer();

        // If last synchronization has failedAction
        if (getSynchroUIContext().getStatus() == SynchroProgressionStatus.FAILED) {
            getSynchroHandler().report(t("quadrige3.synchro.report.failed"));
            return;
        }

        // If synchronisation file is already here, apply it
        if (isSynchronizationUsingServer && getSynchroUIContext().getStatus() == SynchroProgressionStatus.SUCCESS && getSynchroUIContext().getWorkingDirectory() != null) {
            // already downloaded
            getSynchroHandler().showValidApplyPopup();
            return;
        }

        // If last status was RUNNING
        // AND there is a jobId (mantis #24916)
        if (isSynchronizationUsingServer
                && getSynchroUIContext().isRunningStatus()) {
            if (getSynchroUIContext().getImportJobId() != null) {
                nextSynchroAction = getContext().getActionFactory().createNonBlockingUIAction(getSynchroHandler(), ImportSynchroStartAction.class);
                return;
            } else {
                // This should never append, but reset some attributes anyway (NOT a complete reset, to avoid Direction reset...)
                getSynchroUIContext().setImportJobId(null);
                getSynchroUIContext().setStatus(SynchroProgressionStatus.NOT_STARTED);
            }
        }

        // auto select referential AND data updates (only if not referential only)
        getSynchroUIContext().setImportReferential(true);
        getSynchroUIContext().setImportData(!referentialOnly);

        // Ask user to select program to import
        if (!referentialOnly) {

            // Get from server if user has no access rights, only import referential
            if (isSynchronizationUsingServer
                    && !getContext().getProgramStrategyService().hasRemoteReadOnProgram(getContext().getAuthenticationInfo())) {
                referentialOnly = true;
            } else {

                // Ask for programs
                Set<String> programCodes = getSynchroUIContext().getImportDataProgramCodes();
                ProgramSelectUI programSelectUI = new ProgramSelectUI(getUI(), null, StatusFilter.NATIONAL_ACTIVE, programCodes, false, true);

                handler.openDialog(programSelectUI, t("reefdb.action.synchro.import.programCodes.title"), new Dimension(800, 400));

                List<ProgramDTO> programs = programSelectUI.getModel().getSelectedPrograms();

                // If no programs selected (or user cancelled): only import referential
                if (CollectionUtils.isEmpty(programs)) {
//                    getSynchroUIContext().setImportData(false);
//                    referentialOnly = true;
                    // If user cancel the program selection, then cancel synchro (Mantis #46164)
                    return;
                }

                // Transform selected programs into a code list
                else {
                    programCodes = Sets.newHashSet(ReefDbBeans.<String, ProgramDTO>collectProperties(programs, ProgramDTO.PROPERTY_CODE));
                    getSynchroUIContext().setImportDataProgramCodes(programCodes);
                }

                // get photo option and set it in context
                getSynchroUIContext().setImportPhoto(programSelectUI.getModel().isEnablePhoto());

            }
        }

        getSynchroUIContext().saveImportContext();

        // if user is admin/local admin or validator, ask dates (if enable in config)
        if (!referentialOnly
                && getConfig().isSynchronizationPeriodSelectionEnable()
                && (SecurityContextHelper.hasMinimumAuthority(QuadrigeUserAuthority.LOCAL_ADMIN)
                || SecurityContextHelper.hasMinimumAuthority(QuadrigeUserAuthority.ADMIN))) {

            // Show dates period popup
            getSynchroHandler().showDateChoicePopup();
            return;
        }

        // If using synchro from server
        if (isSynchronizationUsingServer) {
            // launch synchro
            ImportSynchroCheckAction checkAction = getContext().getActionFactory().createNonBlockingUIAction(getSynchroHandler(), ImportSynchroCheckAction.class);
            checkAction.setCheckReferentialOnly(referentialOnly);
            checkAction.setSilentIfNoUpdate(silentIfNoUpdate);
            checkAction.setSilentIfUpdate(silentIfUpdate);
            nextSynchroAction = checkAction;
        }

        // run apply direct synchro action
        else {

            ImportSynchroDirectAction importAction = getActionFactory().createLogicAction(getSynchroHandler(), ImportSynchroDirectAction.class);
            if (importAction.prepareAction()) {
                getActionEngine().runInternalAction(importAction);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getSynchroUIContext().saveImportContext();

        if (nextSynchroAction != null && getConfig().isSynchronizationUsingServer()) {
            nextSynchroAction.execute();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void releaseAction() {
        super.releaseAction();
        referentialOnly = false;
        silentIfNoUpdate = false;
        silentIfUpdate = false;
        nextSynchroAction = null;
    }
}
