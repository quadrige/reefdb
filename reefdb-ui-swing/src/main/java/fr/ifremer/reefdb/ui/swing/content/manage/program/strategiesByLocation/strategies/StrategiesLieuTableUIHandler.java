package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.strategies;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.StatusRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.SortOrder;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class StrategiesLieuTableUIHandler extends AbstractReefDbTableUIHandler<StrategiesLieuTableRowModel, StrategiesLieuTableUIModel, StrategiesLieuTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(StrategiesLieuTableUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<StrategiesLieuTableRowModel> getTableModel() {
        return (StrategiesLieuTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTableau();
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final StrategiesLieuTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final StrategiesLieuTableUIModel model = new StrategiesLieuTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(StrategiesLieuTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initialiserTableau();
    }

    /**
     * Chargement des strategies suivant le lieu.
     *
     * @param lieu Le lieu
     */
    public void load(final LocationDTO lieu) {

        // Load les strategies dans le model
        getModel().setBeans(getContext().getProgramStrategyService().getStrategyUsageByLocationId(lieu.getId()));

        // Initialiser le titre du panel
        getUI().setBorder(BorderFactory.createTitledBorder(
                t("reefdb.program.strategies.strategy.title", lieu.getName())));
    }

    /**
     * Initialisation de le tableau.
     */
    private void initialiserTableau() {

        // Colonne programme
        final TableColumnExt colonneProgramme = addColumn(
                null,
                newTableCellRender(ProgramDTO.class, DecoratorService.CODE),
                StrategiesLieuTableModel.PROGRAM);
        colonneProgramme.setSortable(true);
        colonneProgramme.setMinWidth(200);

        // Colonne libelle
        final TableColumnExt colonneLibelle = addColumn(
                StrategiesLieuTableModel.NAME);
        colonneLibelle.setSortable(true);
        colonneLibelle.setMinWidth(200);

        // Colonne date debut
        final TableColumnExt colonneStartDate = addLocalDatePickerColumnToModel(
                StrategiesLieuTableModel.START_DATE, getConfig().getDateFormat(), false);
        colonneStartDate.setSortable(true);
        colonneStartDate.setMinWidth(100);

        // Colonne date fin
        final TableColumnExt colonneEndDate = addLocalDatePickerColumnToModel(
                StrategiesLieuTableModel.END_DATE, getConfig().getDateFormat(), false);
        colonneEndDate.setSortable(true);
        colonneEndDate.setMinWidth(100);

        // Colonne local 
        final TableColumnExt colonneLocal = addColumn(StrategiesLieuTableModel.STATUS);
        colonneLocal.setCellRenderer(new StatusRenderer());
        colonneLocal.setMinWidth(50);
        colonneLocal.setMaxWidth(50);

        // Modele de la table
        final StrategiesLieuTableModel tableModel = new StrategiesLieuTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Le tableau n est pas editable
        getTable().setEditable(false);

        // Initialisation de la table
        initTable(getTable(), true);

        // Tri par defaut
        getTable().setSortOrder(StrategiesLieuTableModel.PROGRAM, SortOrder.ASCENDING);

        // Les colonnes obligatoire sont toujours presentes
        colonneLocal.setHideable(false);
        colonneProgramme.setHideable(false);
//		colonneEtat.setHideable(false);
        colonneLibelle.setHideable(false);
        colonneStartDate.setHideable(false);
        colonneEndDate.setHideable(false);
    }
}
