package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.GroupingTypeDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.SortOrder;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des observations.
 */
public class ExtractionsTableUIHandler extends
        AbstractReefDbTableUIHandler<ExtractionsRowModel, ExtractionsTableUIModel, ExtractionsTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ExtractionsTableUIHandler.class);

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{
                ExtractionsRowModel.PROPERTY_DIRTY,
                ExtractionsRowModel.PROPERTY_ERRORS
        };
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ExtractionsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ExtractionsTableUIModel model = new ExtractionsTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ExtractionsTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Initialisation du tableau
        initTable();

        // Initialisation de la combobox Extraction
        ui.getExtractSinpButton().setEnabled(getContext().isSynchroEnabled());
//        ui.getExtractPampaButton().setEnabled(getContext().isSynchroEnabled());
        initActionComboBox(getUI().getExtractCombo());

        // button border
        getUI().getExtractCombo().setBorder(
                BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), ui.getExtractCombo().getBorder())
        );

        // Initialisation des listeners
        initListeners();
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // name
        TableColumnExt nameCol = addColumn(ExtractionsTableModel.NAME);
        nameCol.setSortable(true);

        // grouping type
        TableColumnExt groupingTypeCol = addFilterableComboDataColumnToModel(
                ExtractionsTableModel.GROUPING_TYPE, getContext().getReferentialService().getGroupingTypes(), false);

        ExtractionsTableModel tableModel = new ExtractionsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable());

        getTable().setVisibleRowCount(8);

        // Tri par defaut
        getTable().setSortOrder(ExtractionsTableModel.NAME, SortOrder.ASCENDING);

        // border
        addEditionPanelBorder();

    }

    private void initListeners() {

        // Listener on selection
        getModel().addPropertyChangeListener(ExtractionsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // set selected extraction in main model
            getModel().getExtractionUIModel().setSelectedExtraction(getModel().getSingleSelectedRow());
        });

    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(ExtractionsRowModel row) {
        return super.isRowValid(row) && isExtractionValid(row);
    }

    private boolean isExtractionValid(ExtractionsRowModel row) {

        row.getErrors().clear();

        if (!row.isFiltersValid()) {
            ReefDbBeans.addError(row, t("reefdb.extraction.list.filter.invalid"), ExtractionsRowModel.PROPERTY_NAME);
        }

        // search for name duplicates
        if (StringUtils.isNoneBlank(row.getName()) && row.isDirty()) {
            boolean duplicateFound = false;

            for (ExtractionsRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    ReefDbBeans.addError(row, t("reefdb.error.alreadyExists.label.ui", row.getName()), ExtractionsRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                List<ExtractionDTO> allExtractions = getContext().getExtractionService().getAllExtractions();
                if (CollectionUtils.isNotEmpty(allExtractions)) {
                    for (ExtractionDTO extraction : allExtractions) {
                        if (!extraction.getId().equals(row.getId()) && row.getName().equalsIgnoreCase(extraction.getName())) {
                            ReefDbBeans.addError(row, t("reefdb.error.alreadyExists.label.db", row.getName()), ExtractionsRowModel.PROPERTY_NAME);
                            break;
                        }
                    }
                }
            }
        }

        return row.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, ExtractionsRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
        recomputeRowValidState(row);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ExtractionsRowModel> getTableModel() {
        return (ExtractionsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getExtractionsTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<ExtractionsRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        // should be only one row
        if (addedRows.size() == 1) {
            ExtractionsRowModel row = addedRows.get(0);

            // default order item type
            row.setGroupingType(ReefDbBeans.findByProperty(
                    getContext().getReferentialService().getGroupingTypes(),
                    GroupingTypeDTO.PROPERTY_CODE,
                    getConfig().getExtractionDefaultOrderItemTypeCode()
            ));

            // Ajouter le focus sur la cellule de la ligne cree
            setFocusOnCell(row);
        }
    }

}
