package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.menu.PmfmMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.table.PmfmTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.table.PmfmTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Quadruplets au niveau local
 */
public class PmfmsLocalUIHandler extends AbstractReefDbTableUIHandler<PmfmTableRowModel, PmfmsLocalUIModel, PmfmsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PmfmsLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final PmfmsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final PmfmsLocalUIModel model = new PmfmsLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final PmfmsLocalUI ui) {
        initUI(ui);

        // hide context filter panel
        ui.getPmfmsLocalMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getPmfmsLocalMenuUI().getHandler().forceLocal(true);

        // hide name panel
        ui.getPmfmsLocalMenuUI().getNamePanel().setVisible(false);

        // listen to search results
        ui.getPmfmsLocalMenuUI().getModel().addPropertyChangeListener(PmfmMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<PmfmDTO>) evt.getNewValue()));

        // Initialisation du tableau
        initTable();

        getUI().getManageQuadrupletsLocalTableDeleteBouton().setEnabled(false);
        getUI().getManageQuadrupletsLocalTableReplaceBouton().setEnabled(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        TableColumnExt idCol = addColumn(PmfmTableModel.PMFM_ID);
        idCol.setSortable(true);
        idCol.setEditable(false);

        // parameter
        final TableColumnExt parameterCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.PARAMETER,
                getContext().getReferentialService().getParameters(StatusFilter.ACTIVE), false);
        parameterCol.setSortable(true);

        // support
        final TableColumnExt supportCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.SUPPORT,
                getContext().getReferentialService().getMatrices(StatusFilter.ACTIVE), false);
        supportCol.setSortable(true);

        // fraction
        final TableColumnExt fractionCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.FRACTION,
                getContext().getReferentialService().getFractions(StatusFilter.ACTIVE), false);
        fractionCol.setSortable(true);

        // method
        final TableColumnExt methodCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.METHOD,
                getContext().getReferentialService().getMethods(StatusFilter.ACTIVE), false);
        methodCol.setSortable(true);

        // unit
        final TableColumnExt unitCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.ACTIVE),
                false);
        unitCol.setSortable(true);


        // Comment, creation and update dates
        addCommentColumn(PmfmTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(PmfmTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(PmfmTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);


        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL), false);
        statusCol.setSortable(true);
        fixDefaultColumnWidth(statusCol);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
                new AssociatedQualitativeValueCellEditor(getTable(), getUI(), true),
                new AssociatedQualitativeValueCellRenderer(),
                PmfmTableModel.QUALITATIVE_VALUES);
        associatedQualitativeValueCol.setSortable(true);
        fixColumnWidth(associatedQualitativeValueCol, 120);

//        // threshold
//        final TableColumnExt stepCol = addColumn(columnModel, PmfmTableModel.THRESHOLD);
//        stepCol.setSortable(true);
//        stepCol.setEditable(true);
//
//        // max decimal number
//        final TableColumnExt maxDecimalNumberCol = addColumn(columnModel, PmfmTableModel.MAX_DECIMAL_NUMBER);
//        maxDecimalNumberCol.setSortable(true);
//        maxDecimalNumberCol.setEditable(true);
//
//        // significant digits number
//        final TableColumnExt significantDigitsNumberCol = addColumn(columnModel, PmfmTableModel.SIGNIFICANT_DIGITS_NUMBER);
//        significantDigitsNumberCol.setSortable(true);
//        significantDigitsNumberCol.setEditable(true);

        PmfmTableModel tableModel = new PmfmTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfms.local"), PmfmTableModel.QUALITATIVE_VALUES);

        // Initialisation du tableau
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        idCol.setVisible(false);
//        stepCol.setVisible(false);
//        maxDecimalNumberCol.setVisible(false);
//        significantDigitsNumberCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<PmfmTableRowModel> getTableModel() {
        return (PmfmTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getPmfmsLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<PmfmTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            PmfmTableRowModel row = addedRows.get(0);
            // Set default status
            row.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(row);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, PmfmTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(PmfmTableRowModel row) {
        row.getErrors().clear();

        return super.isRowValid(row) && isUnique(row);
    }

    private boolean isUnique(PmfmTableRowModel row) {

        if (row.getParameter() != null && row.getMatrix() != null && row.getFraction() != null && row.getMethod() != null && row.getUnit() != null) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (PmfmTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getParameter().equals(otherRow.getParameter())
                        && row.getMatrix().equals(otherRow.getMatrix())
                        && row.getFraction().equals(otherRow.getFraction())
                        && row.getMethod().equals(otherRow.getMethod())
                        && row.getUnit().equals(otherRow.getUnit())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential",
                                    t("reefdb.property.pmfm"), decorate(row), t("reefdb.property.referential.local")),
                            PmfmDTO.PROPERTY_PARAMETER,
                            PmfmDTO.PROPERTY_MATRIX,
                            PmfmDTO.PROPERTY_FRACTION,
                            PmfmDTO.PROPERTY_METHOD,
                            PmfmDTO.PROPERTY_UNIT);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<PmfmDTO> existingPmfms =
                        getContext().getReferentialService().searchPmfms(StatusFilter.ALL,
                                row.getParameter().getCode(),
                                row.getMatrix().getId(),
                                row.getFraction().getId(),
                                row.getMethod().getId(),
                                row.getUnit().getId(),
                                null, null);
                if (CollectionUtils.isNotEmpty(existingPmfms)) {
                    for (PmfmDTO pmfm : existingPmfms) {
                        if (!pmfm.getId().equals(row.getId())
                                && row.getUnit().equals(pmfm.getUnit())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.pmfm"), decorate(row),
                                            ReefDbBeans.isLocalStatus(pmfm.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    PmfmDTO.PROPERTY_PARAMETER,
                                    PmfmDTO.PROPERTY_MATRIX,
                                    PmfmDTO.PROPERTY_FRACTION,
                                    PmfmDTO.PROPERTY_METHOD,
                                    PmfmDTO.PROPERTY_UNIT);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();
    }

}
