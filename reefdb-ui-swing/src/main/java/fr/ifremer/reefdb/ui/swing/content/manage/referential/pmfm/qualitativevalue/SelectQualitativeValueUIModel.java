package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

import java.util.List;

/**
 * @author peck7 on 24/10/2017.
 */
public class SelectQualitativeValueUIModel extends AbstractEmptyUIModel<SelectQualitativeValueUIModel> {

    public static final String PROPERTY_AVAILABLE_LIST = "availableList";
    private List<QualitativeValueDTO> availableList;
    public static final String PROPERTY_SELECTED_LIST = "selectedList";
    private List<QualitativeValueDTO> selectedList;
    private boolean editable;
    public static final String PROPERTY_EDITABLE = "editable";

    public List<QualitativeValueDTO> getAvailableList() {
        return availableList;
    }

    public void setAvailableList(List<QualitativeValueDTO> availableList) {
        this.availableList = availableList;
        firePropertyChange(PROPERTY_AVAILABLE_LIST, null, availableList);
    }

    public List<QualitativeValueDTO> getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(List<QualitativeValueDTO> selectedList) {
        this.selectedList = selectedList;
        firePropertyChange(PROPERTY_SELECTED_LIST, null, selectedList);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }
}
