package fr.ifremer.reefdb.ui.swing.util.validator;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.validator.NuitonValidatorModel;
import org.nuiton.validator.xwork2.XWork2NuitonValidator;
import org.nuiton.validator.xwork2.XWork2NuitonValidatorProvider;

/**
 * specific validator provider (not used)
 * Created by Ludovic on 08/06/2015.
 */
public class ReefDbValidatorProvider extends XWork2NuitonValidatorProvider {

    /**
     * <p>Constructor for ReefDbValidatorProvider.</p>
     */
    public ReefDbValidatorProvider() {
    }

    /** {@inheritDoc} */
    @Override
    public <O> XWork2NuitonValidator<O> newValidator(NuitonValidatorModel<O> model) {
        return new ReefDbValidator<>(model);
    }
}
