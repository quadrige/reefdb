package fr.ifremer.reefdb.ui.swing.util.table.renderer;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.quadrige3.ui.swing.table.renderer.CheckBoxRenderer;
import fr.ifremer.reefdb.dto.ReefDbBeans;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import java.awt.Component;

/**
 * <p>StatusRenderer class.</p>
 * todo move to quadrige3 ui swing common
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class StatusRenderer extends CheckBoxRenderer {

    /** {@inheritDoc} */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        JCheckBox render = (JCheckBox) super.getTableCellRendererComponent(table, false, isSelected, hasFocus, row, column);

        if (value instanceof StatusDTO) {
            render.setSelected(ReefDbBeans.isLocalStatus((StatusDTO) value));
        }
        return render;
    }

}
