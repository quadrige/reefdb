package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.ManageUsersUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local.replace.ReplaceUserUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local.replace.ReplaceUserUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table.UserTableUIModel;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 */
public class OpenReplaceUserAction extends AbstractReefDbAction<UserTableUIModel, ManageUsersLocalUI, ManageUsersLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceUserAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local.ManageUsersLocalUIHandler} object.
     */
    public OpenReplaceUserAction(ManageUsersLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<PersonDTO, ReplaceUserUIModel, ReplaceUserUI> openAction =
                new AbstractOpenReplaceUIAction<PersonDTO, ReplaceUserUIModel, ReplaceUserUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.user");
                    }

                    @Override
                    protected ReplaceUserUIModel createNewModel() {
                        return new ReplaceUserUIModel();
                    }

                    @Override
                    protected ReplaceUserUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceUserUI(ctx);
                    }

                    @Override
                    protected List<PersonDTO> getReferentialList(ReferentialService referentialService) {
                        // TODO it return active and non-active users, shall it return only active ?
                        return Lists.newArrayList(referentialService.getUsersInSameDepartment(getSelectedSource()));
                    }

                    @Override
                    protected PersonDTO getSelectedSource() {
                        List<PersonDTO> selectedBeans = OpenReplaceUserAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected PersonDTO getSelectedTarget() {
                        ManageUsersUI ui = OpenReplaceUserAction.this.getUI().getParentContainer(ManageUsersUI.class);
                        List<PersonDTO> selectedBeans = ui.getManageUsersNationalUI().getModel().getSelectedBeans();
                        PersonDTO targetPerson = selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                        if (targetPerson != null && getSelectedSource() != null && Objects.equals(targetPerson.getDepartment(), getSelectedSource().getDepartment())) {
                            return targetPerson;
                        }
                        return null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
