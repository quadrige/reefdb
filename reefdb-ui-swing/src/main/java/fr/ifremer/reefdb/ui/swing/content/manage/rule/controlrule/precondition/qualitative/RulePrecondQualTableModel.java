package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.qualitative;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePrecondQualTableModel extends AbstractReefDbTableModel<RulePrecondQualRowModel> {

    public static final ReefDbColumnIdentifier<RulePrecondQualRowModel> NAME = ReefDbColumnIdentifier.newId(
            QualitativeValueDTO.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class
    );

    /**
     * <p>Constructor for AbstractReefDbTableModel.</p>
     *
     * @param columnModel column model
     */
    public RulePrecondQualTableModel(SwingTableColumnModel columnModel) {
        super(columnModel, false, false);
    }

    @Override
    public ReefDbColumnIdentifier<RulePrecondQualRowModel> getFirstColumnEditing() {
        return NAME;
    }

    @Override
    public RulePrecondQualRowModel createNewRow() {
        return new RulePrecondQualRowModel();
    }
}
