package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.ungrouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SwingUtilities;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour le tableau du haut (Psfm) pour l onglet des mesures de l'observation.
 */
public class SurveyMeasurementsUngroupedTableUIHandler
        extends AbstractReefDbTableUIHandler<SurveyMeasurementsUngroupedRowModel, SurveyMeasurementsUngroupedTableUIModel, SurveyMeasurementsUngroupedTableUI> {

    private static final int ROW_COUNT = 5;

    // editor for analyst column
    private ExtendedComboBoxCellEditor<DepartmentDTO> departmentCellEditor;

    /**
     * <p>Constructor for SurveyMeasurementsUngroupedTableUIHandler.</p>
     */
    public SurveyMeasurementsUngroupedTableUIHandler() {
        super(SurveyMeasurementsUngroupedRowModel.PROPERTY_PMFMS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyMeasurementsUngroupedTableModel getTableModel() {
        return (SurveyMeasurementsUngroupedTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final SurveyMeasurementsUngroupedTableUI ui) {
        super.beforeInit(ui);
        // create model and register to the JAXX context
        ui.setContextValue(new SurveyMeasurementsUngroupedTableUIModel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final SurveyMeasurementsUngroupedTableUI ui) {

        // Initialiser l UI
        initUI(ui);
        createDepartmentCellEditor();
        updateDepartmentCellEditor(false);

        initTable();
        SwingUtil.setLayerUI(ui.getTableauHautScrollPane(), ui.getTableBlockLayer());

        // Initialiser listeners
        initListeners();

        getTable().setVisibleRowCount(ROW_COUNT);
    }

    private void createDepartmentCellEditor() {

        departmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        departmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateDepartmentCellEditor(true);
        });

    }

    private void updateDepartmentCellEditor(boolean forceNoFilter) {

        departmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        departmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    /**
     * Initialiser les listeners
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(SurveyMeasurementsUngroupedTableUIModel.PROPERTY_SURVEY, evt -> loadSurvey());

    }

    /**
     * Load survey.
     */
    private void loadSurvey() {

        SwingUtilities.invokeLater(() -> {

            // Uninstall save state listener
            uninstallSaveTableStateListener();

            addPmfmColumns(
                    getModel().getPmfms(),
                    SurveyDTO.PROPERTY_PMFMS,
                    DecoratorService.NAME_WITH_UNIT); // insert at the end

            boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfms());

            // tell the table model is editable or not
            getTableModel().setReadOnly(!getModel().getSurvey().isEditable());

            getModel().setRows(null);

            if (notEmpty) {

                // affect row
                SurveyMeasurementsUngroupedRowModel row = getModel().addNewRow(getModel().getSurvey());

                // set analyst from first non null measurement
                Optional<MeasurementDTO> measurementFound = getModel().getSurvey().getMeasurements().stream().filter(measurement -> measurement.getAnalyst() != null).findFirst();
                if (measurementFound.isPresent()) {
                    row.setAnalyst(measurementFound.get().getAnalyst());
                } else {
                    row.setAnalyst(getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
                            getModel().getSurvey(),
                            getModel().getPmfms()
                    ));
                }

            }

            recomputeRowsValidState();

            // restore table from swing session
            restoreTableState();

            // hide analyst if no pmfm
//            forceColumnVisibleAtLastPosition(SurveyMeasurementsUngroupedTableModel.ANALYST, notEmpty);
            // Don't force position (Mantis #49537)
            forceColumnVisible(SurveyMeasurementsUngroupedTableModel.ANALYST, notEmpty);

            // Install save state listener
            installSaveTableStateListener();

            getModel().fireMeasurementsLoaded();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, SurveyMeasurementsUngroupedRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // no need to tell the table is modified if no pmfms (measurement) changes
        if (SurveyMeasurementsUngroupedRowModel.PROPERTY_PMFMS.equals(propertyName) && oldValue == newValue) {
            return;
        }

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        getModel().getSurvey().setDirty(true);

    }

    public void save() {

        if (getModel().getRowCount() == 0)
            return;

        if (getModel().getRowCount() > 1)
            throw new ReefDbTechnicalException("Should be only 1 row of survey ungrouped measurements");

        SurveyMeasurementsUngroupedRowModel row = getModel().getRows().get(0);

        // update all measurements
        row.getMeasurements().forEach(measurement -> measurement.setAnalyst(row.getAnalyst()));

        // save modifications to parent model
        getModel().getSurvey().setMeasurements(row.getMeasurements());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(SurveyMeasurementsUngroupedRowModel row) {
        return super.isRowValid(row) && isMeasurementRowValid(row);
    }

    private boolean isMeasurementRowValid(SurveyMeasurementsUngroupedRowModel row) {

        if (row.getAnalyst() == null &&
            row.getMeasurements().stream()
                .filter(measurement -> getModel().getSurvey().getPmfmsUnderMoratorium().stream().noneMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(measurement.getPmfm(), moratoriumPmfm)))
                .anyMatch(measurement -> !ReefDbBeans.isMeasurementEmpty(measurement))
        ) {
            ReefDbBeans.addError(row,
                    t("reefdb.validator.error.analyst.required"),
                    SurveyMeasurementsUngroupedRowModel.PROPERTY_ANALYST);
            return false;
        }
        return true;
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Add analyst column (Mantis #42619)
        TableColumnExt analystColumn = addColumn(
                departmentCellEditor,
                newTableCellRender(SurveyMeasurementsUngroupedTableModel.ANALYST),
                SurveyMeasurementsUngroupedTableModel.ANALYST
        );
        analystColumn.setMinWidth(100);

        // Modele de la table
        SurveyMeasurementsUngroupedTableModel tableModel = new SurveyMeasurementsUngroupedTableModel(getTable().getColumnModel());
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable(), true);

        // border
        addEditionPanelBorder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getSurveyUngroupedMeasurementTable();
    }
}
