package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.taxonsDialog;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Handler.
 */
public class TaxonsDialogUIHandler extends AbstractReefDbUIHandler<TaxonsDialogUIModel, TaxonsDialogUI>  implements Cancelable {

    /** Constant <code>FILTER="filter"</code> */
    public static final String FILTER = "filter";
    /** Constant <code>TABLE="table"</code> */
    public static final String TABLE = "table";

    /** {@inheritDoc} */
    @Override
    public void beforeInit(TaxonsDialogUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new TaxonsDialogUIModel());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(TaxonsDialogUI ui) {
        initUI(ui);

        getModel().addPropertyChangeListener(evt -> {

            if (TaxonsDialogUIModel.PROPERTY_TAXON_GROUP.equals(evt.getPropertyName())) {

                List<TaxonDTO> taxons = getModel().getTaxonGroup() != null ? new ArrayList<>(getModel().getTaxonGroup().getTaxons()) : null;
                getUI().getTaxonsTable().getModel().setBeans(taxons);
                getUI().getTaxonsFilter().getHandler().loadSelectedElements(taxons);

            } else if (TaxonsDialogUIModel.PROPERTY_TAXONS.equals(evt.getPropertyName())) {

                getUI().getTaxonsTable().getModel().setBeans(getModel().getTaxons());
                getUI().getTaxonsFilter().getHandler().loadSelectedElements(getModel().getTaxons());

            } else if (TaxonsDialogUIModel.PROPERTY_EDITABLE.equals(evt.getPropertyName())) {

                getUI().getListPanelLayout().setSelected(getModel().isEditable() ? FILTER : TABLE);
            }
        });

        // hide menu
        ui.getTaxonsTable().getTaxonsNationalMenuUI().setVisible(false);
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // set selected taxon in taxon group
        if (getModel().isEditable() && getModel().getTaxonGroup() != null) {
            getModel().getTaxonGroup().setTaxons(new ArrayList<>(getUI().getTaxonsFilter().getModel().getElements()));
        }

        // close the dialog box
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
