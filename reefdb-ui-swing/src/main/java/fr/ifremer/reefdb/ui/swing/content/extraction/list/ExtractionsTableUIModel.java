package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model pour le tableau des observations.
 */
public class ExtractionsTableUIModel extends AbstractReefDbTableUIModel<ExtractionDTO, ExtractionsRowModel, ExtractionsTableUIModel> {

    private ExtractionUIModel extractionUIModel;

    private boolean filtersLoading;
    /** Constant <code>PROPERTY_FILTERS_LOADING="filtersLoading"</code> */
    public static final String PROPERTY_FILTERS_LOADING = "filtersLoading";

    /**
     * Constructor.
     */
    public ExtractionsTableUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>extractionUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIModel} object.
     */
    public ExtractionUIModel getExtractionUIModel() {
        return extractionUIModel;
    }

    /**
     * <p>Setter for the field <code>extractionUIModel</code>.</p>
     *
     * @param extractionUIModel a {@link fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIModel} object.
     */
    public void setExtractionUIModel(ExtractionUIModel extractionUIModel) {
        this.extractionUIModel = extractionUIModel;
    }

    /**
     * <p>isFiltersLoading.</p>
     *
     * @return a boolean.
     */
    public boolean isFiltersLoading() {
        return filtersLoading;
    }

    /**
     * <p>Setter for the field <code>filtersLoading</code>.</p>
     *
     * @param filtersLoading a boolean.
     */
    public void setFiltersLoading(boolean filtersLoading) {
        this.filtersLoading = filtersLoading;
        firePropertyChange(PROPERTY_FILTERS_LOADING, null, filtersLoading);
    }

}
