package fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;

/**
 * <p>ManageFiltersListTableUIRowModel class.</p>
 *
 * @author Antoine
 */
public class ManageFiltersListTableUIRowModel extends AbstractReefDbRowUIModel<QuadrigeBean, ManageFiltersListTableUIRowModel> {

    /** Constant <code>PROPERTY_TYPE="type"</code> */
    public static final String PROPERTY_TYPE = "type";
    /** Constant <code>PROPERTY_FILTER="filter"</code> */
    public static final String PROPERTY_FILTER = "filter";
    public String type;
    private FilterDTO filter;
    private Integer filterTypeId;

    /**
     * <p>Constructor for ManageFiltersListTableUIRowModel.</p>
     */
    public ManageFiltersListTableUIRowModel() {
        super(null, null);
    }

    /** {@inheritDoc} */
    @Override
    protected QuadrigeBean newBean() {
        return null;
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getType() {
        return type;
    }

    /**
     * <p>Setter for the field <code>type</code>.</p>
     *
     * @param type a {@link java.lang.String} object.
     */
    public void setType(String type) {
        this.type = type;

    }

    /**
     * <p>Getter for the field <code>filter</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public FilterDTO getFilter() {
        return filter;
    }

    /**
     * <p>Setter for the field <code>filter</code>.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    public void setFilter(FilterDTO filter) {
        this.filter = filter;
    }

    /**
     * <p>Getter for the field <code>filterTypeId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFilterTypeId() {
        return filterTypeId;
    }

    /**
     * <p>Setter for the field <code>filterTypeId</code>.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setFilterTypeId(final Integer id) {
        this.filterTypeId = id;
    }
}
