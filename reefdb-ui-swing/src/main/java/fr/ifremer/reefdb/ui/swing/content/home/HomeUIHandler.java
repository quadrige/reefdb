package fr.ifremer.reefdb.ui.swing.content.home;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.enums.SearchDateValues;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel;
import fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.util.List;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Home handler.
 */
public class HomeUIHandler extends AbstractReefDbUIHandler<HomeUIModel, HomeUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(HomeUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final HomeUI ui) {
        super.beforeInit(ui);

        // Ajout du model pour la page d acceuil
        ui.setContextValue(new HomeUIModel());

        ui.setContextValue(SwingUtil.createActionIcon("home"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(HomeUI ui) {
        initUI(ui);
        ui.getCampaignLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getProgramLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getLocationLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getStateLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getShareLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getCommentLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getDatesLabel().setForeground(getConfig().getColorThematicLabel());

        // Save models
        getModel().setSurveysTableUIModel(getUI().getSurveysTable().getModel());
        getModel().setOperationsTableUIModel(getUI().getOperationsTable().getModel());

        // Initialisation des combobox
        initComboBox();
        applyContext();

        // Initialisation des listeners
        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

        // init image
        SwingUtil.setComponentWidth(getUI().getLeftImage(), ui.getHomeMenu().getPreferredSize().width * 9 / 10);
        getUI().getLeftImage().setScaled(true);

        // Chargement des donnees de recherche, sauvegardees
        runLastSearch();
    }

    /**
     * Initialisation des composants de gauche
     */
    private void initComboBox() {

        // Initialisation des contextes
        initBeanFilterableComboBox(
                getUI().getContextCombobox(),
                getContext().getContextService().getAllContexts(),
                getContext().getSelectedContext());

        // Intialisation des campagnes
        initBeanFilterableComboBox(
                getUI().getCampaignCombo(),
                null,
                null);

        // Listener on remove filter on campaigns
        getUI().getCampaignCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            getUI().getCampaignCombo().setActionEnabled(false);
            getModel().setForceNoCampaignFilter(true);
            applyContext();
        });

        // Initailisation des programmes
        initBeanFilterableComboBox(
                getUI().getProgramCombo(),
                null, //getContext().getObservationService().getAvailablePrograms(null, null, null, false),
                null);

        getUI().getProgramCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            getUI().getProgramCombo().setActionEnabled(false);
            getModel().setForceNoProgramFilter(true);
            applyContext();
        });

        // Initialisation des lieux
        initBeanFilterableComboBox(
                getUI().getLocationCombo(),
                null, //getContext().getObservationService().getAvailableLocations(false),
                null);

        getUI().getLocationCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            getUI().getLocationCombo().setActionEnabled(false);
            getModel().setForceNoLocationFilter(true);
            applyContext();
        });

        // Initialisation de la recherche des dates
        initBeanFilterableComboBox(
                getUI().getSearchDateCombo(),
                getContext().getSystemService().getSearchDates(),
                null);

        // On désactive les champs date étant donne qu'il faut en premier selectionner la contrainte
        getUI().getStartDateEditor().setEnabled(false);
        getUI().getEndDateEditor().setEnabled(false);
        getUI().getAndLabel().setVisible(false);
        getUI().getEndDateEditor().setVisible(false);

        // Initialisation de la recherche par controle et validation
        initBeanFilterableComboBox(
                getUI().getStateCombo(),
                getContext().getSystemService().getStates(),
                null);

        // Initialisation de la recherche par controle et validation
        initBeanFilterableComboBox(
                getUI().getShareCombo(),
                getContext().getSystemService().getAllSynchronizationStatus(false) /*mantis 26500*/,
                null);

        // Modification des largeurs des combobox
        ReefDbUIs.forceComponentSize(getUI().getContextCombobox());
        ReefDbUIs.forceComponentSize(getUI().getCampaignCombo());
        ReefDbUIs.forceComponentSize(getUI().getProgramCombo());
        ReefDbUIs.forceComponentSize(getUI().getLocationCombo());
        ReefDbUIs.forceComponentSize(getUI().getStateCombo());
        ReefDbUIs.forceComponentSize(getUI().getShareCombo());
        ReefDbUIs.forceComponentSize(getUI().getSearchDateCombo(), 82);
        ReefDbUIs.forceComponentSize(getUI().getStartDateEditor(), 120);
        ReefDbUIs.forceComponentSize(getUI().getEndDateEditor(), 120);
    }

    /**
     * Initialiser les listeners.
     */
    private void initListeners() {

        // Listener sur la selection du programme
        getModel().addPropertyChangeListener(HomeUIModel.PROPERTY_PROGRAM, evt -> {
            // update locations
            updateLocations();
            getUI().getLocationCombo().requestFocus();
        });

        getModel().addPropertyChangeListener(HomeUIModel.PROPERTY_LOCATION, evt -> {
            // update programs
            updatePrograms();
            getUI().getStateCombo().requestFocus();
        });

        // Listener sur la selection du type de recherche sur les dates
        getModel().addPropertyChangeListener(HomeUIModel.PROPERTY_SEARCH_DATE, evt -> {

            if (getModel().getSearchDateId() != null) {

                // Si on fait une recherche sur les dates on active les champs de selection de date.
                getUI().getStartDateEditor().setEnabled(true);
                getUI().getEndDateEditor().setEnabled(true);

                // Traitement suivant la valeur sélectionnée
                final SearchDateValues searchDateValue = SearchDateValues.values()[getModel().getSearchDateId()];
                switch (searchDateValue) {
                    case BETWEEN:

                        // Les champs dateFin et label ET visible
                        getUI().getAndLabel().setVisible(true);
                        getUI().getEndDateEditor().setVisible(true);
                        break;

                    default:

                        // Les champs date 1 et label ET invisible
                        getUI().getEndDateEditor().setVisible(false);
                        getUI().getEndDateEditor().setLocalDate(null);
                        getUI().getAndLabel().setVisible(false);
                        break;
                }
            } else {

                //Si on efface le critère de recherche alors on efface les dates
                getUI().getStartDateEditor().setLocalDate(null);
                getUI().getStartDateEditor().setEnabled(false);
                getUI().getEndDateEditor().setLocalDate(null);
                getUI().getEndDateEditor().setEnabled(false);
                getUI().getEndDateEditor().setVisible(false);
                getUI().getAndLabel().setVisible(false);
            }
        });

        // Change context value
        getModel().addPropertyChangeListener(HomeUIModel.PROPERTY_CONTEXT, evt -> {

            getContext().setSelectedContext(getModel().getContext());
            applyContext();
        });

        getModel().getOperationsTableUIModel().addPropertyChangeListener(OperationsTableUIModel.PROPERTY_LOADING, evt -> {

            if (evt.getNewValue() instanceof Boolean) {
                getModel().getSurveysTableUIModel().setSamplingOperationsLoading((Boolean) evt.getNewValue());
                if (Boolean.FALSE.equals(evt.getNewValue())) {
                    checkOperationValid(null);
                }
            }
        });

        // Listen modify property and set dirty to the selected survey
        listenModelModify(getModel().getSurveysTableUIModel());
        getModel().getOperationsTableUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (modify != null) {
                getModel().setModify(modify);
                // Apply dirty state on selected survey in model, not from selected row (Mantis #46659)
                if (modify && getModel().getSelectedSurvey() != null) {
                    getModel().getSelectedSurvey().setDirty(true);
                }
            }
        });

        // Listen to survey selection
        getModel().getSurveysTableUIModel().addPropertyChangeListener(SurveysTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // Check previous survey valid state (Mantis #46659)
            // Prevent operation validation if loading (Mantis #59887)
            if (evt.getOldValue() instanceof SurveysTableRowModel && !getUI().getOperationsTable().getHandler().isLoading()) {
                SurveysTableRowModel previousSelectedSurvey = (SurveysTableRowModel) evt.getOldValue();
                getUI().getOperationsTable().getHandler().stopCellEditing();
                checkOperationValid(previousSelectedSurvey);
                getUI().getSurveysTable().getHandler().recomputeRowValidState(previousSelectedSurvey);
                getUI().getSurveysTable().getHandler().forceRevalidateModel();
            }

            // Get selected survey
            final SurveysTableRowModel survey = (SurveysTableRowModel) evt.getNewValue();

            // is survey editable ?
            getModel().getSurveysTableUIModel().setSelectedSurveyEditable(survey != null && survey.isEditable());

            // save observation id in context
            getContext().setSelectedSurveyId(survey == null ? null : survey.getId());

            // set selected survey in main model
            getModel().setSelectedSurvey(survey);

        });

        getModel().addPropertyChangeListener(HomeUIModel.PROPERTY_SELECTED_SURVEY, evt -> {

            // load operations from new selected survey
            getUI().getOperationsTable().getHandler().loadOperations(getModel().getSelectedSurvey());
        });

        // Listen to valid state
        getModel().getSurveysTableUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());
        getModel().getOperationsTableUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> {
            if (!getUI().getOperationsTable().getHandler().isLoading()) {
                checkOperationValid(null);
            }
        });

    }

    private void checkOperationValid(SurveysTableRowModel survey) {
        survey = Optional.ofNullable(survey).orElse(getModel().getSelectedSurvey());

        // Prevent operation validation if loading (Mantis #59887)
        if (survey != null && survey == getModel().getOperationsTableUIModel().getSurvey() && !getUI().getOperationsTable().getHandler().isLoading()) {

            // recompute operations valid
            getUI().getOperationsTable().getHandler().recomputeRowsValidState();

            // set operations valid state on current selected survey
            survey.setOperationsValid(getModel().getOperationsTableUIModel().isValid());
            if (!getUI().getSurveysTable().getSurveyTable().isEditing()) {
                getUI().getSurveysTable().getHandler().recomputeRowValidState(survey);
            }
            getValidator().doValidate();
            getUI().getSurveysTable().getHandler().forceRevalidateModel();
            forceRevalidateModel();
        }
    }

    private void updatePrograms() {

        if (getModel().isAdjusting()) {
            return;
        }
        getModel().setAdjusting(true);

        ProgramDTO oldProgram = getModel().getProgram();
        getModel().setProgram(null);

        List<ProgramDTO> programs = getContext().getObservationService().getAvailablePrograms(
                null,
                getModel().getLocationId(),
                null,
                getModel().isForceNoProgramFilter(),
                false);

        getUI().getProgramCombo().setData(programs);

        if (oldProgram != null && programs.contains(oldProgram)) {
            getModel().setProgram(oldProgram);
        } else if (programs.size() == 1) {
            getModel().setProgram(programs.get(0));
        }

        getModel().setAdjusting(false);
    }

    private void updateCampaigns() {

        if (getModel().isAdjusting()) {
            return;
        }
        getModel().setAdjusting(true);

        CampaignDTO oldCampaign = getModel().getCampaign();
        getModel().setCampaign(null);

        List<CampaignDTO> campaigns = getContext().getObservationService().getAvailableCampaigns(null, getModel().isForceNoCampaignFilter());

        getUI().getCampaignCombo().setData(campaigns);

        if (oldCampaign != null && campaigns.contains(oldCampaign)) {
            getModel().setCampaign(oldCampaign);
        } else if (campaigns.size() == 1) {
            getModel().setCampaign(campaigns.get(0));
        }

        getModel().setAdjusting(false);
    }

    private void updateLocations() {

        if (getModel().isAdjusting()) {
            return;
        }
        getModel().setAdjusting(true);

        LocationDTO oldLocation = getModel().getLocation();
        getModel().setLocation(null);

        List<LocationDTO> locations = getContext().getObservationService().getAvailableLocations(
                null,
                getModel().getProgramCode(),
                getModel().isForceNoLocationFilter(),
            false);

        getUI().getLocationCombo().setData(locations);

        if (oldLocation != null && locations.contains(oldLocation)) {
            getModel().setLocation(oldLocation);
        }

        getModel().setAdjusting(false);
    }

    /**
     * Change current context.
     */
    private void applyContext() {

        getModel().setProgramsFiltered(getContext().getDataContext().isContextFiltered(FilterTypeValues.PROGRAM));
        getModel().setCampaignsFiltered(getContext().getDataContext().isContextFiltered(FilterTypeValues.CAMPAIGN));
        getModel().setLocationsFiltered(getContext().getDataContext().isContextFiltered(FilterTypeValues.LOCATION));

        // reload combos
        updatePrograms();
        updateCampaigns();
        updateLocations();
    }

    /**
     * Chargement de la recherche sauvegardee.
     */
    private void runLastSearch() {

        // Les parametres sauvegardes
        final SurveyFilterDTO surveyFilter = getContext().getSurveyFilter();
        if (surveyFilter != null) {

            if (surveyFilter.getCampaignId() != null) {
                getUI().getCampaignCombo().getData().stream()
                        .filter(campaign -> campaign.getId().equals(surveyFilter.getCampaignId()))
                        .findFirst().ifPresent(campaign -> getModel().setCampaign(campaign));
            }
            if (surveyFilter.getProgramCode() != null) {
                getUI().getProgramCombo().getData().stream()
                        .filter(program -> program.getCode().equals(surveyFilter.getProgramCode()))
                        .findFirst().ifPresent(program -> getModel().setProgram(program));
            }
            if (surveyFilter.getLocationId() != null) {
                getUI().getLocationCombo().getData().stream()
                        .filter(location -> location.getId().equals(surveyFilter.getLocationId()))
                        .findFirst().ifPresent(location -> getModel().setLocation(location));
            }
            if (surveyFilter.getStateId() != null) {
                getUI().getStateCombo().getData().stream()
                        .filter(state -> state.getId().equals(surveyFilter.getStateId()))
                        .findFirst().ifPresent(state -> getModel().setState(state));
            }
            if (surveyFilter.getShareId() != null) {
                getUI().getShareCombo().getData().stream()
                        .filter(synchronizationStatus -> synchronizationStatus.getId().equals(surveyFilter.getShareId()))
                        .findFirst().ifPresent(synchronizationStatus -> getModel().setShare(synchronizationStatus));
            }
            if (surveyFilter.getSearchDateId() != null) {
                getUI().getSearchDateCombo().getData().stream()
                        .filter(searchDate -> searchDate.getId().equals(surveyFilter.getSearchDateId()))
                        .findFirst().ifPresent(searchDate -> getModel().setSearchDate(searchDate));
            }

            getModel().setStartDate(surveyFilter.getDate1());
            getModel().setEndDate(surveyFilter.getDate2());
            getModel().setComment(surveyFilter.getComment());

            // do search
            AbstractApplicationAction searchAction = getContext().getActionFactory().createLogicAction(HomeUIHandler.this, SearchAction.class);
            SwingUtilities.invokeLater(() -> getContext().getActionEngine().runAction(searchAction));
        }

    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<HomeUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }
}
