package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.associatedMatrices;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.HashSet;
import java.util.List;

/**
 * Controlleur pour la gestion des lieux au niveau national
 */
public class AssociatedMatricesUIHandler extends AbstractReefDbTableUIHandler<MatricesTableRowModel, AssociatedMatricesUIModel, AssociatedMatricesUI>  implements Cancelable {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(AssociatedMatricesUIHandler.class);

    /** Constant <code>DOUBLE_LIST="doubleList"</code> */
    public static final String DOUBLE_LIST = "doubleList";
    /** Constant <code>TABLE="table"</code> */
    public static final String TABLE = "table";

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AssociatedMatricesUI ui) {
        super.beforeInit(ui);

        AssociatedMatricesUIModel matricesUIModel = new AssociatedMatricesUIModel();
        ui.setContextValue(matricesUIModel);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(AssociatedMatricesUI associatedMatricesUI) {
        initUI(associatedMatricesUI);

        // Initialisation du tableau
        initTable();

        initBeanList(getUI().getAssociatedMatricesDoubleList(), getContext().getReferentialService().getMatrices(StatusFilter.ACTIVE), null);

        getModel().addPropertyChangeListener(evt -> {

            if (AssociatedMatricesUIModel.PROPERTY_FRACTION.equals(evt.getPropertyName())) {

                List<MatrixDTO> matrices = getModel().getFraction() != null ? Lists.newArrayList(getModel().getFraction().getMatrixes()) : null;
                getUI().getAssociatedMatricesDoubleList().getModel().setSelected(matrices);
                getModel().setBeans(matrices);

            } else if (AssociatedMatricesUIModel.PROPERTY_EDITABLE.equals(evt.getPropertyName())) {

                if (getModel().isEditable()) {
                    getUI().getListPanelLayout().setSelected(DOUBLE_LIST);
                } else {
                    getUI().getListPanelLayout().setSelected(TABLE);
                }
            }
        });
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(MatricesTableModel.NAME);
        mnemonicCol.setSortable(true);

        // description
        final TableColumnExt descriptionCol = addColumn(MatricesTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MatricesTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);
        fixDefaultColumnWidth(statusCol);

        MatricesTableModel tableModel = new MatricesTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable(), true);

        getTable().setEditable(false);

    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<MatricesTableRowModel> getTableModel() {
        return (MatricesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getAssociatedMatricesTable();
    }


    /**
     * <p>valid.</p>
     */
    public void valid() {
        if (getModel().isEditable() && getModel().getFraction() != null) {
            getModel().getFraction().setMatrixes(new HashSet<>(getUI().getAssociatedMatricesDoubleList().getModel().getSelected()));
        }

        // close the dialog box
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
