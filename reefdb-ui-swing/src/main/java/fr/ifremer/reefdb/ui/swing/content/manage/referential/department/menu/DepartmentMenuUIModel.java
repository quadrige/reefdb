/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.AbstractReferentialMenuUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * <p>DepartmentMenuUIModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class DepartmentMenuUIModel extends AbstractReferentialMenuUIModel<DepartmentCriteriaDTO, DepartmentMenuUIModel> implements DepartmentCriteriaDTO {

    private static final Binder<DepartmentMenuUIModel, DepartmentCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(DepartmentMenuUIModel.class, DepartmentCriteriaDTO.class);

    private static final Binder<DepartmentCriteriaDTO, DepartmentMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(DepartmentCriteriaDTO.class, DepartmentMenuUIModel.class);

    /**
     * <p>Constructor for DepartmentMenuUIModel.</p>
     */
    public DepartmentMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected DepartmentCriteriaDTO newBean() {
        return ReefDbBeanFactory.newDepartmentCriteriaDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return delegateObject.getCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setCode(String code) {
        delegateObject.setCode(code);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getParentDepartment() {
        return delegateObject.getParentDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentDepartment(DepartmentDTO parentDepartment) {
        delegateObject.setParentDepartment(parentDepartment);
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setCode(null);
        setParentDepartment(null);
    }
}
