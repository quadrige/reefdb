package fr.ifremer.reefdb.ui.swing.content.observation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedRowModel;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Save action.
 */
public class SaveAction extends AbstractReefDbSaveAction<ObservationUIModel, ObservationUI, ObservationUIHandler> {

    private ControlRuleMessagesBean controlMessages;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ObservationUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        // check if survey is not dirty
        if (ReefDbBeans.isSurveyValidated(getModel())) {
            getContext().getDialogHelper().showErrorDialog(t("reefdb.action.save.observation.alreadySynchronized"), t("reefdb.action.save.observation"));
            return false;
        }

        // Check survey date within campaign dates
        if (getModel().getCampaign() != null) {
            if (getModel().getDate().isBefore(getModel().getCampaign().getStartDate())
                    || (getModel().getCampaign().getEndDate() != null && getModel().getDate().isAfter(getModel().getCampaign().getEndDate()))) {
                // The save is invalid if the survey is outside campaign dates
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.action.save.observation.notInCampaign", ReefDbBeans.toString(getModel()), ReefDbBeans.toString(getModel().getCampaign())),
                        t("reefdb.action.save.observations"));
                return false;
            }
        }

        // check for individual measurements with taxon (or taxon group) and without measurement value
        if (isSurveyHasEmptyTaxonMeasurements()) {
            getContext().getDialogHelper().showErrorDialog(t("reefdb.action.save.observation.emptyTaxonMeasurement"), t("reefdb.action.save.observation"));
            return false;
        }

        // Save partial models from tabs
        getUI().getSurveyDetailsTabUI().getHandler().save();
        getUI().getPhotosTabUI().getHandler().save();

        // Save and control measurements tabs (Mantis #51725)
        getUI().getSurveyMeasurementsTabUI().getHandler().save();
        getUI().getOperationMeasurementsTabUI().getHandler().save();

        return true;
    }

    private boolean isSurveyHasEmptyTaxonMeasurements() {

        // check survey individual measurement rows
        for (SurveyMeasurementsGroupedRowModel row : getModel().getSurveyMeasurementsTabUIModel().getGroupedTableUIModel().getRows()) {
            if (row.getTaxonGroup() != null || row.getTaxon() != null) {
                boolean rowWithoutMeasurements = true;
                for (MeasurementDTO measurement : row.getMeasurements()) {
                    if (!ReefDbBeans.isMeasurementEmpty(measurement)) {
                        rowWithoutMeasurements = false;
                        break;
                    }
                }
                if (rowWithoutMeasurements) return true;
            }
        }

        // check operations individual measurement rows
        for (OperationMeasurementsGroupedRowModel row : getModel().getOperationMeasurementsTabUIModel().getGroupedTableUIModel().getRows()) {
            if (row.getTaxonGroup() != null || row.getTaxon() != null) {
                boolean rowWithoutMeasurements = true;
                for (MeasurementDTO measurement : row.getMeasurements()) {
                    if (!ReefDbBeans.isMeasurementEmpty(measurement)) {
                        rowWithoutMeasurements = false;
                        break;
                    }
                }
                if (rowWithoutMeasurements) return true;
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Save observation
        getContext().getObservationService().saveSurvey(getModel());

        // Control observation
        controlMessages = getContext().getControlRuleService().controlSurvey(getModel(),
                false /*Do not set control date, if succeed */,
                true /*But reset control date if KO */);

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getHandler().refreshModels();

        // show error messages
        showControlResult(controlMessages, false);

        // Postpone model modify state (Mantis #50915)
        SwingUtilities.invokeLater(() -> getModel().setModify(false));

    }

}
