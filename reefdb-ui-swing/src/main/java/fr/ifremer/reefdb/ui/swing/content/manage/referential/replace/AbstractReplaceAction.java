package fr.ifremer.reefdb.ui.swing.content.manage.referential.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.CodeOnly;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Objects;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @param <E>  type of bean implementing StatusDTO
 * @param <M>
 * @param <UI>
 * @param <H>
 * @since 3.6
 */
public abstract class AbstractReplaceAction<
        E extends BaseReferentialDTO,
        M extends AbstractReplaceUIModel<E>,
        UI extends ReefDbUI<M, ?>,
        H extends AbstractReefDbUIHandler<M, UI>> extends AbstractReefDbAction<M, UI, H> {

    static {
        n("reefdb.replaceTemporaryAndDelete.done");
        n("reefdb.replaceTemporary.done");
    }

    /**
     * Logger.
     */
    private static final Log log =
            LogFactory.getLog(AbstractReplaceAction.class);

    /**
     * <p>getReferentialLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    protected abstract String getReferentialLabel();

    private E source;

    private E target;

    private boolean delete;

    private boolean replaced;

    /**
     * <p>prepareReplaceReferential.</p>
     *
     * @param service a {@link fr.ifremer.reefdb.service.referential.ReferentialService} object.
     * @param source a E object.
     * @param target a E object.
     * @return a boolean.
     */
    protected abstract boolean prepareReplaceReferential(ReferentialService service, E source, E target);

    /**
     * <p>replaceReferential.</p>
     *
     * @param service a {@link fr.ifremer.reefdb.service.referential.ReferentialService} object.
     * @param source a E object.
     * @param target a E object.
     * @param delete a boolean.
     */
    protected abstract void replaceReferential(ReferentialService service, E source, E target, boolean delete);

    /**
     * <p>resetCaches.</p>
     */
    protected abstract void resetCaches();

    /**
     * <p>Constructor for AbstractReplaceAction.</p>
     *
     * @param handler a H object.
     */
    protected AbstractReplaceAction(H handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        replaced = false;
        boolean doAction = super.prepareAction();

        if (doAction) {

            M model = getModel();
            doAction = model.isValid();

            if (doAction) {
                source = model.getSelectedSource();
                target = model.getSelectedTarget();
                delete = model.isDelete();
            }

            if (Objects.equals(source, target)) {
                displayErrorMessage(getReplaceUI().getTitle(), t("reefdb.replaceReferential.error.sameReferential"));
                doAction = false;
            }

            // except for some DTO, test if source and target have ids.
            if (!(source instanceof CodeOnly)) {

                if (source.getId() == null) {
                    displayErrorMessage(getReplaceUI().getTitle(), t("reefdb.replaceReferential.error.source.invalid"));
                    doAction = false;
                }

                if (target.getId() == null) {
                    displayErrorMessage(getReplaceUI().getTitle(), t("reefdb.replaceReferential.error.target.invalid"));
                    doAction = false;
                }
            }
        }

        return doAction;
    }

    /**
     * <p>Getter for the field <code>source</code>.</p>
     *
     * @return a E object.
     */
    public E getSource() {
        return source;
    }

    /**
     * <p>Getter for the field <code>target</code>.</p>
     *
     * @return a E object.
     */
    public E getTarget() {
        return target;
    }

    /**
     * <p>isDelete.</p>
     *
     * @return a boolean.
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * <p>Setter for the field <code>delete</code>.</p>
     *
     * @param delete a boolean.
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    /**
     * <p>getReplaceUI.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractReplaceUI} object.
     */
    protected AbstractReplaceUI getReplaceUI() {
        return (AbstractReplaceUI) getUI();
    }

    /** {@inheritDoc} */
    @Override
    protected String decorate(Object object) {
        return super.decorate(object, getModel().getDecoratorContext());
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        Assert.notNull(source);
        Assert.notNull(target);

        if (!prepareReplaceReferential(getContext().getReferentialService(), source, target)) {
            return;
        }

        if (log.isInfoEnabled()) {
            log.info(String.format("Will replace %s temporary: %s by: %s",
                    getReferentialLabel(), decorate(source), decorate(target)));
        }

        replaceReferential(getContext().getReferentialService(), source, target, delete);
        replaced = true;

        // reset cache
        resetCaches();

    }

    /** {@inheritDoc} */
    @Override
    public void releaseAction() {
        source = target = null;
        delete = replaced = false;
        super.releaseAction();
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getUI().getHandler().onCloseUI();

        if (replaced) {
            displayInfoMessage(getReplaceUI().getTitle(),
                    t(delete ? "reefdb.replaceTemporaryAndDelete.done" : "reefdb.replaceTemporary.done", getReferentialLabel(), decorate(source), decorate(target)));
        }
    }

}
