package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.initGrid;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.annotation.Nonnull;
import javax.swing.JComponent;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>InitGridUIHandler class.</p>
 */
public class InitGridUIHandler extends AbstractReefDbUIHandler<InitGridUIModel, InitGridUI> implements Cancelable {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(InitGridUI ui) {
        super.beforeInit(ui);

        InitGridUIModel model = new InitGridUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(InitGridUI ui) {
        initUI(ui);

        initBeanFilterableComboBox(ui.getSamplingOperationComboBox(), new ArrayList<>(), null);

        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    private void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            if (InitGridUIModel.PROPERTY_SAMPLING_OPERATIONS.equals(evt.getPropertyName())) {
                // load sampling operations
                getUI().getSamplingOperationComboBox().setData(getModel().getSamplingOperations());

            } else if (InitGridUIModel.PROPERTY_SAMPLING_OPERATION.equals(evt.getPropertyName())) {

                if (getModel().getSamplingOperation() != null) {

                    // build existing transitions values map
                    buildCurrentMap(ImmutableList.of(getModel().getSamplingOperation()));

                    // Try to find the last transition gap and set it
                    getModel().setTransition(findLastTransition());

                    // find origin and length value
                    MeasurementDTO originMeasurement = ReefDbBeans.getMeasurementByPmfmId(getModel().getSamplingOperation(), getModel().getOriginPmfmId());
                    if (!ReefDbBeans.isMeasurementEmpty(originMeasurement) && getModel().getOriginUnit() != null && getModel().getTransitionUnit() != null) {
                        getModel().setOrigin(ReefDbBeans.convertLengthValue(
                            originMeasurement.getNumericalValue(),
                            getModel().getOriginUnit().getId(),
                            getModel().getTransitionUnit().getId()
                        ).intValue());
                    } else {
                        getModel().setOrigin(getConfig().getPitOriginDefaultValue());
                    }

                    MeasurementDTO lengthMeasurement = ReefDbBeans.getMeasurementByPmfmId(getModel().getSamplingOperation(), getModel().getLengthPmfmId());
                    if (!ReefDbBeans.isMeasurementEmpty(lengthMeasurement) && getModel().getLengthUnit() != null && getModel().getTransitionUnit() != null) {
                        getModel().setLength(ReefDbBeans.convertLengthValue(
                            lengthMeasurement.getNumericalValue(),
                            getModel().getLengthUnit().getId(),
                            getModel().getTransitionUnit().getId()
                        ).intValue());
                    } else {
                        getModel().setLength(getConfig().getPitTransectLengthDefaultValue());
                    }

                } else {

                    // reset
                    getModel().setOrigin(null);
                    getModel().setTransition(null);
                    getModel().setLength(null);
                }

            } else if (InitGridUIModel.PROPERTY_ALL_SAMPLING_OPERATIONS.equals(evt.getPropertyName())) {

                if (getModel().isAllSamplingOperations()) {

                    // try to get a unique value
                    if (CollectionUtils.isNotEmpty(getModel().getSamplingOperations())) {

                        // build existing transitions values map
                        buildCurrentMap(getModel().getSamplingOperations());

                        // Try to find the last transition gap and set it
                        getModel().setTransition(findLastTransition());

                        // find origin and length value
                        Integer origin = null;
                        boolean sameOrigin = true;
                        Integer length = null;
                        boolean sameLength = true;

                        for (SamplingOperationDTO samplingOperation : getModel().getSamplingOperations()) {
                            MeasurementDTO originMeasurement = ReefDbBeans.getMeasurementByPmfmId(samplingOperation, getModel().getOriginPmfmId());
                            if (!ReefDbBeans.isMeasurementEmpty(originMeasurement)) {
                                if (origin == null) {
                                    origin = originMeasurement.getNumericalValue().intValue();
                                } else if (origin != originMeasurement.getNumericalValue().intValue()) {
                                    sameOrigin = false;
                                }
                            }
                            MeasurementDTO lengthMeasurement = ReefDbBeans.getMeasurementByPmfmId(samplingOperation, getModel().getLengthPmfmId());
                            if (!ReefDbBeans.isMeasurementEmpty(lengthMeasurement)) {
                                if (length == null) {
                                    length = lengthMeasurement.getNumericalValue().intValue();
                                } else if (length != lengthMeasurement.getNumericalValue().intValue()) {
                                    sameLength = false;
                                }
                            }
                        }

                        if (sameOrigin && origin != null && getModel().getOriginUnit() != null && getModel().getTransitionUnit() != null) {
                            getModel().setOrigin(ReefDbBeans.convertLengthValue(
                                BigDecimal.valueOf(origin),
                                getModel().getOriginUnit().getId(),
                                getModel().getTransitionUnit().getId()
                            ).intValue());
                        } else {
                            getModel().setOrigin(getConfig().getPitOriginDefaultValue());
                        }
                        if (sameLength && length != null && getModel().getLengthUnit() != null && getModel().getTransitionUnit() != null) {
                            getModel().setLength(ReefDbBeans.convertLengthValue(
                                BigDecimal.valueOf(length),
                                getModel().getLengthUnit().getId(),
                                getModel().getTransitionUnit().getId()
                            ).intValue());
                        } else {
                            getModel().setLength(getConfig().getPitTransectLengthDefaultValue());
                        }

                    } else {

                        // reset
                        getModel().setOrigin(null);
                        getModel().setTransition(null);
                        getModel().setLength(null);
                    }

                } else {

                    // fire again on selected sampling operation
                    getModel().firePropertyChanged(InitGridUIModel.PROPERTY_SAMPLING_OPERATION, null, getModel().getSamplingOperation());
                }

            } else if (InitGridUIModel.PROPERTY_TRANSITION_UNIT.equals(evt.getPropertyName())) {
                // set unit text from transition unit
                getUI().getOriginUnit().setText(getModel().getTransitionUnit().getSymbol());
                getUI().getTransitionUnit().setText(getModel().getTransitionUnit().getSymbol());
                getUI().getLengthUnit().setText(getModel().getTransitionUnit().getSymbol());

            }
        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getSamplingOperationComboBox();
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // Valid the values
        if (!getModel().areValuesValid()) {
            if (getModel().isContiguousSamplingOperations()) {
                getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.validator.error.samplingOperation.measurement.grouped.init.contiguous",
                        getModel().getSamplingOperations().size(),
                        getModel().getOrigin(), getModel().getTransition(), getModel().getLength())
                );
            } else {
                getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.validator.error.samplingOperation.measurement.grouped.init",
                        getModel().getOrigin(), getModel().getTransition(), getModel().getLength())
                );
            }
            return;
        }

        buildResultMap();

        if (getModel().isValid()) {
            closeDialog();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<InitGridUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * Build current transition values
     *
     * @param samplingOperations selected sampling operations
     */
    private void buildCurrentMap(@Nonnull List<SamplingOperationDTO> samplingOperations) {
        getModel().getCurrentMap().clear();
        samplingOperations.forEach(samplingOperation -> getModel().getCurrentMap().putAll(samplingOperation, getExistingTransitionValues(samplingOperation)));

        // Try to find contiguous sampling operations
        if (getModel().getCurrentMap().keySet().size() > 1) {
            // if all values are unique, these can be contiguous
            getModel().setContiguousSamplingOperations(
                getModel().getCurrentMap().values().stream().allMatch(new HashSet<>()::add)
            );
        }
    }

    // find the gap of the last sampling operation input
    private Integer findLastTransition() {
        return getLastTransition((List<Integer>) getModel().getCurrentMap().get(
            getModel().isAllSamplingOperations() ? getModel().getLastSamplingOperation() : getModel().getSamplingOperation()
        ));
    }

    // Calculate the gap between the 2 last transition values
    private Integer getLastTransition(List<Integer> transitions) {
        if (CollectionUtils.size(transitions) < 2)
            return null;
        return transitions.get(transitions.size() - 1) - transitions.get(transitions.size() - 2);
    }

    private List<Integer> getExistingTransitionValues(SamplingOperationDTO samplingOperation) {
        return ReefDbBeans.getIndividualMeasurementsByPmfmId(samplingOperation, getModel().getTransitionPmfmId()).stream()
            .filter(measurement -> measurement.getNumericalValue() != null)
            .map(measurement -> measurement.getNumericalValue().intValue())
            .sorted()
            .collect(Collectors.toList());
    }

    /**
     * Build result map of all transition values for each sampling operation, except those already in current map
     */
    private void buildResultMap() {
        getModel().getResultMap().clear();

        List<SamplingOperationDTO> samplingOperations = getModel().isAllSamplingOperations()
            ? getModel().getSamplingOperations()
            : Collections.singletonList(getModel().getSamplingOperation());

        int origin = getModel().getOrigin();
        int lengthBySamplingOperation = getModel().isContiguousSamplingOperations()
            ? getModel().getLength() / getModel().getSamplingOperations().size()
            : getModel().getLength();
        int length = lengthBySamplingOperation;

        for (SamplingOperationDTO samplingOperation : samplingOperations) {
            for (int value = origin; value <= length; value += getModel().getTransition()) {
                if (!getModel().getCurrentMap().containsEntry(samplingOperation, value)) {
                    getModel().getResultMap().put(samplingOperation, value);
                }
            }
            if (getModel().isContiguousSamplingOperations()) {
                origin += lengthBySamplingOperation;
                length += lengthBySamplingOperation;
            }
        }

    }
}
