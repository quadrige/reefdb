package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * @author peck7 on 08/01/2019.
 */
public abstract class AbstractMeasurementsGroupedTableModel<R extends AbstractMeasurementsGroupedRowModel<MeasurementDTO, ?>>
    extends AbstractReefDbTableModel<R> {

    /**
     * Identifiant pour la colonne analyst.
     */
    public static final ReefDbColumnIdentifier<? extends AbstractMeasurementsGroupedRowModel<MeasurementDTO, ?>> ANALYST = ReefDbColumnIdentifier.newId(
        AbstractMeasurementsGroupedRowModel.PROPERTY_ANALYST,
        n("reefdb.property.analyst"),
        n("reefdb.measurement.analyst.tip"),
        DepartmentDTO.class);

    public static final ReefDbColumnIdentifier<? extends AbstractMeasurementsGroupedRowModel<MeasurementDTO, ?>> INDIVIDUAL_ID = ReefDbColumnIdentifier.newReadOnlyId(
        AbstractMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_ID,
        n("reefdb.property.individualId"),
        n("reefdb.property.individualId"),
        Integer.class);

    /**
     * <p>Constructor for AbstractReefDbTableModel.</p>
     *
     * @param columnModel a {@link TableColumnModelExt} object.
     */
    public AbstractMeasurementsGroupedTableModel(TableColumnModelExt columnModel, boolean createNewRow) {
        super(columnModel, createNewRow, false);
    }

    public abstract ReefDbColumnIdentifier<R> getPmfmInsertPosition();
}
