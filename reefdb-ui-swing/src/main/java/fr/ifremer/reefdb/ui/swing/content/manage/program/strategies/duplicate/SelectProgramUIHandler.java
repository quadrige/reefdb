package fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.duplicate;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JComponent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>SelectProgramUIHandler class.</p>
 *
 */
public class SelectProgramUIHandler extends AbstractReefDbUIHandler<SelectProgramUIModel, SelectProgramUI> implements Cancelable {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SelectProgramUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SelectProgramUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SelectProgramUIModel model = new SelectProgramUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final SelectProgramUI ui) {
        initUI(ui);

        // listener on default close action = cancel
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                getModel().setTargetProgram(null);
            }
        });

        getModel().addPropertyChangeListener(evt -> {
            if (SelectProgramUIModel.PROPERTY_SOURCE_PROGRAM.equals(evt.getPropertyName())) {
                initBeanFilterableComboBox(getUI().getProgramCombo(), getAllowedPrograms(), getModel().getSourceProgram());
                updateMessageLabel();
            } else if (SelectProgramUIModel.PROPERTY_SOURCE_STRATEGY.equals(evt.getPropertyName())) {
                updateMessageLabel();
            }
        });

    }

    private List<ProgramDTO> getAllowedPrograms() {

        Integer userId = getContext().getDataContext().getRecorderPersonId();
        if (userId == null) {
            return null; // not connected
        }

        // return programs where current user is manager,
        // (or local programs)
        // LP 03/02/2015 : return only local programs when source program is local (see Mantis #28343)
        return getContext().getProgramStrategyService().getManagedProgramsByUserAndStatus(userId,
                ReefDbBeans.isLocalStatus(getModel().getSourceProgram().getStatus())? StatusFilter.LOCAL : StatusFilter.ALL);
    }

    private void updateMessageLabel() {
        if (getModel().getSourceProgram() != null && getModel().getSourceStrategy() != null) {
            getUI().getMessageLabel().setText(t("reefdb.action.duplicate.strategy.selectProgram.message",
                    decorate(getModel().getSourceStrategy()), decorate(getModel().getSourceProgram())));
        }
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setTargetProgram(null);
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getProgramCombo();
    }

}
