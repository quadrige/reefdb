package fr.ifremer.reefdb.ui.swing.content.manage.referential.location.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.referential.GroupingTypeDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.util.ArrayList;
import java.util.List;

/**
 * Controlleur du menu pour la gestion des lieux
 */
public class LocationMenuUIHandler extends ReferentialMenuUIHandler<LocationMenuUIModel, LocationMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(LocationMenuUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final LocationMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final LocationMenuUIModel model = new LocationMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final LocationMenuUI ui) {
        super.afterInit(ui);

        // Initialiser les combobox
        initComboBox();

        getUI().getGroupingTypeCombo().addPropertyChangeListener(BeanFilterableComboBox.PROPERTY_SELECTED_ITEM, evt -> {
            GroupingTypeDTO item = (GroupingTypeDTO) evt.getNewValue();
            if (item != null) {
                getUI().getGroupingCombo().setData(item.getGrouping());
            }
            getUI().getGroupingCombo().setSelectedItem(null);
        });

    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getLocalCombo().setEnabled(enabled);
        getUI().getGroupingTypeCombo().setEnabled(enabled);
        getUI().getGroupingCombo().setEnabled(enabled);
        getUI().getProgramCombo().setEnabled(enabled);
        getUI().getLabelEditor().setEnabled(enabled);
        getUI().getNameEditor().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllLocationFilter();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /** {@inheritDoc} */
    @Override
    public JComponent getLocalFilterPanel() {
        return getUI().getLocalPanel();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Combo local
        initBeanFilterableComboBox(
                getUI().getLocalCombo(),
                getContext().getSystemService().getBooleanValues(),
                null);

        initBeanFilterableComboBox(
                getUI().getGroupingTypeCombo(),
                getContext().getReferentialService().getGroupingTypes(),
                null);

        initBeanFilterableComboBox(
                getUI().getGroupingCombo(),
                new ArrayList<>(),
                null);

        initBeanFilterableComboBox(
                getUI().getProgramCombo(),
                getContext().getProgramStrategyService().getReadablePrograms(),
                null,
                DecoratorService.CODE);

        // Dimension forcee sur la combobox
        ReefDbUIs.forceComponentSize(getUI().getLocalCombo());
        ReefDbUIs.forceComponentSize(getUI().getGroupingTypeCombo());
        ReefDbUIs.forceComponentSize(getUI().getGroupingCombo());
        ReefDbUIs.forceComponentSize(getUI().getProgramCombo());

    }

}
