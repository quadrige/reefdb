package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.action.AbstractChangeScreenAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import org.nuiton.jaxx.application.swing.action.AbstractApplicationAction;

/**
 * Abstract cloase action.
 *
 * @param <M> Model
 * @param <UI> UI
 * @param <H> Handler
 */
public abstract class AbstractCheckBeforeChangeScreenAction<M extends AbstractBeanUIModel, UI extends ReefDbUI<M, ?>, H extends AbstractUIHandler<M, UI>>
		extends AbstractCheckModelAction<M, UI, H> {
    
    /**
     * Constructor.
     *
     * @param handler Handler
     * @param hideBody HideBody
     */
    protected AbstractCheckBeforeChangeScreenAction(final H handler, boolean hideBody) {
        super(handler, hideBody);
    }
        
    /**
     * Go to action class.
     *
     * @return Action class
     */
    protected abstract Class<? extends AbstractChangeScreenAction> getGotoActionClass();
    
	/** {@inheritDoc} */
	@Override
	public void doAction() throws Exception {

		// Goto action class
        AbstractApplicationAction action = getActionFactory().createLogicAction(
                getUI().getParentContainer(ReefDbMainUI.class).getHandler(), getGotoActionClass());
        setActionDescription(action.getActionDescription());
        getActionEngine().runInternalAction(action);
	}
}
