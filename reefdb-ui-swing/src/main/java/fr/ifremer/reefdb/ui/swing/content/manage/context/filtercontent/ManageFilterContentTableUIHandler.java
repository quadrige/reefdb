package fr.ifremer.reefdb.ui.swing.content.manage.context.filtercontent;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>ManageFilterContentTableUIHandler class.</p>
 *
 */
public class ManageFilterContentTableUIHandler extends
        AbstractReefDbTableUIHandler<ManageFilterContentTableUIRowModel, ManageFilterContentTableUIModel, ManageFilterContentTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageFilterContentTableUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageFilterContentTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageFilterContentTableUIModel model = new ManageFilterContentTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageFilterContentTableUI ui) {

        // Initialize the screen
        initUI(ui);

        // Initialisatize the table
        initializeTable();

    }

    /**
     * Initialisation du tableau.
     */
    private void initializeTable() {

        // label
        TableColumnExt labelCol = addColumn(ManageFilterContentTableUITableModel.LABEL);
        labelCol.setSortable(true);
        labelCol.setEditable(false);

        ManageFilterContentTableUITableModel tableModel = new ManageFilterContentTableUITableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable(), true);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ManageFilterContentTableUIRowModel> getTableModel() {
        return (ManageFilterContentTableUITableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageFilterContentTable();
    }

    /**
     * <p>loadFilterElements.</p>
     *
     * @param filterId a {@link java.lang.Integer} object.
     */
    public void loadFilterElements(final Integer filterId) {
        FilterDTO filter = getContext().getContextService().getFilter(filterId);

        if (filter != null) {
            getUI().setEnabled(true);
            loadFilterContent(filter);
        }
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
        loadFilterContent(null);
    }

    private void loadFilterContent(FilterDTO filter) {

        // load filter elements
        getContext().getContextService().loadFilteredElements(filter);

        List<String> contentLabels = getFilterContentLabel(filter);

        List<ManageFilterContentTableUIRowModel> rows = new ArrayList<>(contentLabels.size());
        for (String label : contentLabels) {
            ManageFilterContentTableUIRowModel row = getTableModel().createNewRow();
            row.setValid(true);
            row.setLabel(label);
            rows.add(row);
        }
        getModel().setRows(rows);
    }

    private List<String> getFilterContentLabel(FilterDTO filter) {
        ArrayList<String> list = new ArrayList<>();
        if (filter != null && filter.getElements() != null) {
            for (QuadrigeBean bean : filter.getElements()) {
                String context = null;
                if (bean instanceof TaxonDTO) {
                    context = DecoratorService.WITH_CITATION_AND_REFERENT;
                }
                Decorator decorator = getContext().getDecoratorService().getDecorator(bean, context);
                list.add(decorator != null ? decorator.toString(bean) : bean.toString());
            }
        }
        return list;
    }

}
