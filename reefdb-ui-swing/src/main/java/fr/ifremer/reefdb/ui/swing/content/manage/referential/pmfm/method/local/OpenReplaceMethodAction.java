package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.ManageMethodsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.local.replace.ReplaceMethodUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.local.replace.ReplaceMethodUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceMethodAction extends AbstractReefDbAction<ManageMethodsLocalUIModel, ManageMethodsLocalUI, ManageMethodsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceMethodAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.local.ManageMethodsLocalUIHandler} object.
     */
    public OpenReplaceMethodAction(ManageMethodsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<MethodDTO, ReplaceMethodUIModel, ReplaceMethodUI> openAction =
                new AbstractOpenReplaceUIAction<MethodDTO, ReplaceMethodUIModel, ReplaceMethodUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.pmfm.method");
                    }

                    @Override
                    protected ReplaceMethodUIModel createNewModel() {
                        return new ReplaceMethodUIModel();
                    }

                    @Override
                    protected ReplaceMethodUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceMethodUI(ctx);
                    }

                    @Override
                    protected List<MethodDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getMethods(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected MethodDTO getSelectedSource() {
                        List<MethodDTO> selectedBeans = OpenReplaceMethodAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected MethodDTO getSelectedTarget() {
                        ManageMethodsUI ui = OpenReplaceMethodAction.this.getUI().getParentContainer(ManageMethodsUI.class);
                        List<MethodDTO> selectedBeans = ui.getManageMethodsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
