package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.ReferentialAnalysisInstrumentsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractReplaceAction;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class ReplaceAnalysisInstrumentAction extends AbstractReplaceAction<AnalysisInstrumentDTO, ReplaceAnalysisInstrumentUIModel, ReplaceAnalysisInstrumentUI, ReplaceAnalysisInstrumentUIHandler> {

    /**
     * <p>Constructor for ReplaceAnalysisInstrumentAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local.replace.ReplaceAnalysisInstrumentUIHandler} object.
     */
    public ReplaceAnalysisInstrumentAction(ReplaceAnalysisInstrumentUIHandler handler) {
        super(handler);
    }

    /** {@inheritDoc} */
    @Override
    protected String getReferentialLabel() {
        return t("reefdb.property.analysisInstrument");
    }

    /** {@inheritDoc} */
    @Override
    protected boolean prepareReplaceReferential(ReferentialService service, AnalysisInstrumentDTO source, AnalysisInstrumentDTO target) {

        if (getContext().getReferentialService().isAnalysisInstrumentUsedInProgram(source.getId())) {
            String decoratedSource = decorate(source);
            if (getContext().getDialogHelper().showConfirmDialog(getReplaceUI(), t("reefdb.replaceReferential.used.programStrategy.message", decoratedSource, decoratedSource),
                    getReplaceUI().getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return false;
            }
            setDelete(false);
        }

        if (getContext().getReferentialService().isAnalysisInstrumentUsedInValidatedData(source.getId())) {
            String decoratedSource = decorate(source);
            if (getContext().getDialogHelper().showConfirmDialog(getReplaceUI(), t("reefdb.replaceReferential.used.validatedData.message", decoratedSource, decoratedSource),
                    getReplaceUI().getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return false;
            }
            setDelete(false);
        }

        return true;
    }

    /** {@inheritDoc} */
    @Override
    protected void replaceReferential(ReferentialService service, AnalysisInstrumentDTO source, AnalysisInstrumentDTO target, boolean delete) {

        service.replaceAnalysisInstrument(source, target, delete);
    }

    /** {@inheritDoc} */
    @Override
    protected void resetCaches() {
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        final ReferentialAnalysisInstrumentsUI ui = (ReferentialAnalysisInstrumentsUI) getContext().getMainUI().getHandler().getCurrentBody();
        // reload table
        if (ui != null) {
            SwingUtilities.invokeLater(() -> getActionEngine().runAction(ui.getReferentialAnalysisInstrumentsLocalUI().getMenuUI().getSearchButton()));
        }

    }
}
