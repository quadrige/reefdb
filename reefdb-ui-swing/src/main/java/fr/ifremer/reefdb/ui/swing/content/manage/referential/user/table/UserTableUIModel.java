package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model pour le tableau des Users au niveau national
 */
public class UserTableUIModel extends AbstractReefDbTableUIModel<PersonDTO, UserRowModel, UserTableUIModel> {

    private boolean isAdmin = false;
    private int userId = -1;

    /**
     * Constructor.
     */
    public UserTableUIModel() {
        super();
    }

    /**
     * <p>isAdmin.</p>
     *
     * @return true if authenticated user is admin
     */
    public boolean isAdmin() {
        return isAdmin;
    }

    /**
     * <p>Setter for the field <code>isAdmin</code>.</p>
     *
     * @param isAdmin a boolean.
     */
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * <p>Getter for the field <code>userId</code>.</p>
     *
     * @return a int.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * <p>Setter for the field <code>userId</code>.</p>
     *
     * @param userId a int.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * <p>isUserItself.</p>
     *
     * @param user a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table.UserRowModel} object.
     * @return true if user if authenticated user himself
     */
    public boolean isUserItself(UserRowModel user) {
        return user.getId() != null && user.getId().equals(getUserId());
    }
}
