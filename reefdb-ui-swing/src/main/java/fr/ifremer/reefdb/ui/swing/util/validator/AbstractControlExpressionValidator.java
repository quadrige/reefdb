package fr.ifremer.reefdb.ui.swing.util.validator;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

import java.util.List;

/**
 * Created by Ludovic on 31/03/2016.
 */
public abstract class AbstractControlExpressionValidator extends FieldValidatorSupport {

    private static final String NONE_LEVEL = "none";
    private static final String ALL_LEVEL = "all";
    private static final String ERROR_ONLY = "error";
    private static final String WARNING_ONLY = "warning";
    protected String level = ALL_LEVEL;
    protected String controlLevel = ALL_LEVEL;

    /**
     * <p>Setter for the field <code>level</code>.</p>
     *
     * @param level a {@link java.lang.String} object.
     */
    public void setLevel(String level) {

        if (!NONE_LEVEL.equalsIgnoreCase(level)
                && !ALL_LEVEL.equalsIgnoreCase(level)
                && !ERROR_ONLY.equalsIgnoreCase(level)
                && !WARNING_ONLY.equalsIgnoreCase(level)) {
            throw new IllegalArgumentException("level must be one of " + NONE_LEVEL + "|" + ALL_LEVEL + "|" + ERROR_ONLY + "|" + WARNING_ONLY);
        }

        this.level = level;
    }

    /**
     * <p>Setter for the field <code>controlLevel</code>.</p>
     *
     * @param controlLevel a {@link java.lang.String} object.
     */
    public void setControlLevel(String controlLevel) {

        if (!NONE_LEVEL.equalsIgnoreCase(controlLevel)
                && !ALL_LEVEL.equalsIgnoreCase(controlLevel)
                && !ERROR_ONLY.equalsIgnoreCase(controlLevel)
                && !WARNING_ONLY.equalsIgnoreCase(controlLevel)) {
            throw new IllegalArgumentException("control level must be one of " + NONE_LEVEL + "|" + ALL_LEVEL + "|" + ERROR_ONLY + "|" + WARNING_ONLY);
        }

        this.controlLevel = controlLevel;
    }

    boolean isErrorActive() {
        return ALL_LEVEL.equalsIgnoreCase(this.level) || ERROR_ONLY.equalsIgnoreCase(this.level);
    }

    boolean isWarningActive() {
        return ALL_LEVEL.equalsIgnoreCase(this.level) || WARNING_ONLY.equalsIgnoreCase(this.level);
    }

    boolean isControlErrorActive() {
        return ALL_LEVEL.equalsIgnoreCase(this.controlLevel) || ERROR_ONLY.equalsIgnoreCase(this.controlLevel);
    }

    boolean isControlWarningActive() {
        return ALL_LEVEL.equalsIgnoreCase(this.controlLevel) || WARNING_ONLY.equalsIgnoreCase(this.controlLevel);
    }

    /**
     * add a message on field if not already
     * @param message
     */
    void addFieldErrorMessage(String message) {
        List<String> errors = getValidatorContext().getFieldErrors().get(getFieldName());
        if (errors == null || !errors.contains(message)) {
            getValidatorContext().addFieldError(getFieldName(), message);
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getMessage(Object object) {
        return "should not see me !";
    }

}
