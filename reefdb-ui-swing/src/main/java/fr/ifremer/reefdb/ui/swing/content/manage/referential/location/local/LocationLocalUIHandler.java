package fr.ifremer.reefdb.ui.swing.content.manage.referential.location.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.component.coordinate.CoordinateEditor;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.location.LocationCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.location.menu.LocationMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.location.table.LocationTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.location.table.LocationTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des lieux au niveau local
 */
public class LocationLocalUIHandler extends AbstractReefDbTableUIHandler<LocationTableRowModel, LocationLocalUIModel, LocationLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(LocationLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final LocationLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final LocationLocalUIModel model = new LocationLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(final LocationLocalUI ui) {
        initUI(ui);

        // hide context filter
        ui.getLocationLocalMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getLocationLocalMenuUI().getHandler().forceLocal(true);

        // listen to search results
        ui.getLocationLocalMenuUI().getModel().addPropertyChangeListener(LocationMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<LocationDTO>) evt.getNewValue()));

        // Initialisation du tableau
        initTable();

        getUI().getGererLieuxLocalTableSupprimerBouton().setEnabled(false);
        getUI().getGererLieuxLocalTableRemplacerBouton().setEnabled(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Identifiant
        TableColumnExt identifiantCol = addColumn(LocationTableModel.LABEL);
        identifiantCol.setSortable(true);
        fixColumnWidth(identifiantCol, 100);

        // Libelle
        TableColumnExt libelleCol = addColumn(LocationTableModel.NAME);
        libelleCol.setSortable(true);

        // Bathymetrie
        TableColumnExt bathymetrieCol = addColumn(LocationTableModel.BATHYMETRIE);
        bathymetrieCol.setSortable(true);
        fixColumnWidth(bathymetrieCol, 100);

        // Latitude Min
        TableColumnExt latitudeMinCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MIN,
                LocationTableModel.LATITUDE_MIN);
        latitudeMinCol.setSortable(true);
        latitudeMinCol.setPreferredWidth(100);

        // Longitude Min
        TableColumnExt longitudeMinCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MIN,
                LocationTableModel.LONGITUDE_MIN);
        longitudeMinCol.setSortable(true);
        longitudeMinCol.setPreferredWidth(100);

        // Commentaire
        final TableColumnExt commentaireCol = addCommentColumn(LocationTableModel.COMMENT);
        commentaireCol.setSortable(false);

        TableColumnExt creationDateCol = addDatePickerColumnToModel(LocationTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(LocationTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // Port de Rattachement
        TableColumnExt portRattachementCol = addFilterableComboDataColumnToModel(
                LocationTableModel.HARBOUR,
                getContext().getReferentialService().getHarbours(StatusFilter.ACTIVE), false);
        portRattachementCol.setSortable(true);

        // Delta UT Hivers
        TableColumnExt deltaUTHiverCol = addColumn(LocationTableModel.DELTA_UT_HIVER);
        deltaUTHiverCol.setSortable(true);

        // Changement d heure
//        final TableColumnExt changementHeureCol = addBooleanColumnToModel(columnModel, LocationTableModel.DAYLIGHT_SAVING_TIME, table);
//        changementHeureCol.setSortable(true);

        // Positionnement
        TableColumnExt positionnementCol = addFilterableComboDataColumnToModel(
                LocationTableModel.POSITIONING_NAME,
                getContext().getReferentialService().getPositioningSystems(), false);

        positionnementCol.setSortable(true);
        positionnementCol.setPreferredWidth(200);

        // Positionnement precision
        TableColumnExt positioningPrecisionCol = addColumn(
                LocationTableModel.POSITIONING_PRECISION);
        positioningPrecisionCol.setEditable(false);
        positioningPrecisionCol.setPreferredWidth(100);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
            LocationTableModel.STATUS,
            getContext().getReferentialService().getStatus(StatusFilter.LOCAL_ACTIVE),
            false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        LocationTableModel tableModel = new LocationTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.locations.local"));

        // Initialisation du tableau
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        portRattachementCol.setVisible(false);
        deltaUTHiverCol.setVisible(false);
//		ficheLieuCol.setVisible(false);
//        changementHeureCol.setVisible(false);
        positionnementCol.setVisible(false);
        positioningPrecisionCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<LocationTableRowModel> getTableModel() {
        return (LocationTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getLocationLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<LocationTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            LocationTableRowModel row = addedRows.get(0);

            // Set default status
            row.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));

            setFocusOnCell(row);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, LocationTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(LocationTableRowModel row) {
        row.getErrors().clear();
        return !row.isEditable() || (super.isRowValid(row) && isUnique(row));
    }

    private boolean isUnique(LocationTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (LocationTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.location"), row.getName(), t("reefdb.property.referential.local")),
                            LocationTableRowModel.PROPERTY_NAME
                    );
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                LocationCriteriaDTO locationCriteria = ReefDbBeanFactory.newLocationCriteriaDTO();
                locationCriteria.setName(row.getName());
                locationCriteria.setStrictName(true);
                List<LocationDTO> existingLocations = getContext().getReferentialService().searchLocations(locationCriteria);
                if (CollectionUtils.isNotEmpty(existingLocations)) {
                    for (LocationDTO location : existingLocations) {
                        if (!location.getId().equals(row.getId())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.location"), row.getName(), ReefDbBeans.isLocalStatus(location.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    LocationTableRowModel.PROPERTY_NAME
                            );
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();
    }

}
