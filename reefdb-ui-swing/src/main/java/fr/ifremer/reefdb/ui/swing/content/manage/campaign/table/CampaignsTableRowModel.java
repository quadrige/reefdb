package fr.ifremer.reefdb.ui.swing.content.manage.campaign.table;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

/**
 * Modele pour le tableau de programmes.
 */
public class CampaignsTableRowModel extends AbstractReefDbRowUIModel<CampaignDTO, CampaignsTableRowModel> implements CampaignDTO {

    private static final Binder<CampaignDTO, CampaignsTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(CampaignDTO.class, CampaignsTableRowModel.class);
    private static final Binder<CampaignsTableRowModel, CampaignDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(CampaignsTableRowModel.class, CampaignDTO.class);

    /**
     * Constructor.
     */
    public CampaignsTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    @Override
    public boolean isEditable() {
        return super.isEditable() && !isReadOnly();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CampaignDTO newBean() {
        return ReefDbBeanFactory.newCampaignDTO();
    }

    @Override
    public String getName() {
        return delegateObject.getName();
    }

    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    @Override
    public LocalDate getStartDate() {
        return delegateObject.getStartDate();
    }

    @Override
    public void setStartDate(LocalDate startDate) {
        delegateObject.setStartDate(startDate);
    }

    @Override
    public LocalDate getEndDate() {
        return delegateObject.getEndDate();
    }

    @Override
    public void setEndDate(LocalDate endDate) {
        delegateObject.setEndDate(endDate);
    }

    @Override
    public String getSismerLink() {
        return delegateObject.getSismerLink();
    }

    @Override
    public void setSismerLink(String sismerLink) {
        delegateObject.setSismerLink(sismerLink);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    @Override
    public PersonDTO getManager() {
        return delegateObject.getManager();
    }

    @Override
    public void setManager(PersonDTO manager) {
        delegateObject.setManager(manager);
    }

    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    @Override
    public DepartmentDTO getRecorderDepartment() {
        return delegateObject.getRecorderDepartment();
    }

    @Override
    public void setRecorderDepartment(DepartmentDTO recorderDepartment) {
        delegateObject.setRecorderDepartment(recorderDepartment);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
