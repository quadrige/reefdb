package fr.ifremer.reefdb.ui.swing.content.home;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.SearchDateDTO;
import fr.ifremer.reefdb.dto.StateDTO;
import fr.ifremer.reefdb.dto.SynchronizationStatusDTO;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel;
import fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableUIModel;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Home model.
 */
public class HomeUIModel extends AbstractEmptyUIModel<HomeUIModel> {

    public static final String PROPERTY_CONTEXT = "context";
    public static final String PROPERTY_PROGRAM = "program";
    public static final String PROPERTY_PROGRAMS_FILTERED = "programsFiltered";
    public static final String PROPERTY_LOCATION = "location";
    public static final String PROPERTY_LOCATIONS_FILTERED = "locationsFiltered";
    public static final String PROPERTY_CAMPAIGN = "campaign";
    public static final String PROPERTY_CAMPAIGNS_FILTERED = "campaignsFiltered";
    public static final String PROPERTY_STATE = "state";
    public static final String PROPERTY_SHARE = "share";
    public static final String PROPERTY_SEARCH_DATE = "searchDate";
    public static final String PROPERTY_COMMENT = "comment";
    public static final String PROPERTY_START_DATE = "startDate";
    public static final String PROPERTY_END_DATE = "endDate";
    public static final String PROPERTY_SELECTED_SURVEY = "selectedSurvey";
    public static final String STATE_CONTEXT_PREFIX = "surveys_";

    private ContextDTO context;
    private ProgramDTO program;
    private boolean programsFiltered;
    private boolean forceNoProgramFilter;
    private CampaignDTO campaign;
    private boolean campaignsFiltered;
    private boolean forceNoCampaignFilter;
    private LocationDTO location;
    private boolean locationsFiltered;
    private boolean forceNoLocationFilter;
    private StateDTO state;
    private SynchronizationStatusDTO share;
    private SearchDateDTO searchDate;
    private String comment;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean adjusting;
    /**
     * Observation table model.
     */
    private SurveysTableUIModel surveysTableUIModel;
    /**
     * Prelevement table model.
     */
    private OperationsTableUIModel operationsTableUIModel;
    private SurveysTableRowModel selectedSurvey;

    @Override
    public boolean isModify() {
        return super.isModify() && getSurveysTableUIModel().isModify();
    }

    /**
     * Check if there is real error (if not, means control is valid, then the save is enabled) (Mantis #50538)
     *
     * @return true if already valid or there is only control errors
     */
    public boolean isControlValid() {
        if (isValid())
            return true;
        if (getSurveysTableUIModel() == null)
            return false;

        boolean controlValid = true;
        for (SurveyDTO survey : getSurveysTableUIModel().getBeans()) {
            if (ReefDbBeans.isSurveyValidated(survey))
                continue;
            if (!ReefDbBeans.getErrors(survey, false).isEmpty()) {
                controlValid = false;
                break;
            } else {
                for (SamplingOperationDTO operation : survey.getSamplingOperations()) {
                    if (!ReefDbBeans.getErrors(operation, false).isEmpty()) {
                        controlValid = false;
                        break;
                    }
                }
            }
        }
        return controlValid;
    }

    /**
     * <p>isAdjusting.</p>
     *
     * @return a boolean.
     */
    public boolean isAdjusting() {
        return adjusting;
    }

    /**
     * <p>Setter for the field <code>adjusting</code>.</p>
     *
     * @param adjusting a boolean.
     */
    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    /**
     * <p>Getter for the field <code>context</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public ContextDTO getContext() {
        return context;
    }

    /**
     * <p>Setter for the field <code>context</code>.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public void setContext(ContextDTO context) {
        ContextDTO oldContext = getContext();
        this.context = context;
        firePropertyChange(PROPERTY_CONTEXT, oldContext, context);
    }

    /**
     * <p>Getter for the field <code>program</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public ProgramDTO getProgram() {
        return program;
    }

    /**
     * <p>Setter for the field <code>program</code>.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void setProgram(ProgramDTO program) {
        ProgramDTO oldValue = getProgram();
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
    }

    /**
     * <p>getProgramCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProgramCode() {
        return getProgram() == null ? null : getProgram().getCode();
    }

    public String getStateContext() {
        return Optional.ofNullable(getProgramCode()).map(STATE_CONTEXT_PREFIX::concat).orElse(AbstractTableModel.DEFAULT_STATE_CONTEXT);
    }

    public CampaignDTO getCampaign() {
        return campaign;
    }

    public Integer getCampaignId() {
        return getCampaign() == null ? null : getCampaign().getId();
    }

    public void setCampaign(CampaignDTO campaign) {
        CampaignDTO oldCampaign = getCampaign();
        this.campaign = campaign;
        firePropertyChange(PROPERTY_CAMPAIGN, oldCampaign, campaign);
    }

    /**
     * <p>Getter for the field <code>comment</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getComment() {
        return comment;
    }

    /**
     * <p>Setter for the field <code>comment</code>.</p>
     *
     * @param comment a {@link java.lang.String} object.
     */
    public void setComment(String comment) {
        String oldValue = getComment();
        this.comment = StringUtils.isEmpty(comment) ? null : comment;
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    /**
     * <p>isProgramsFiltered.</p>
     *
     * @return a boolean.
     */
    public boolean isProgramsFiltered() {
        return programsFiltered;
    }

    /**
     * <p>Setter for the field <code>programsFiltered</code>.</p>
     *
     * @param programsFiltered a boolean.
     */
    public void setProgramsFiltered(boolean programsFiltered) {
        boolean oldValue = isProgramsFiltered();
        this.programsFiltered = programsFiltered;
        firePropertyChange(PROPERTY_PROGRAMS_FILTERED, oldValue, programsFiltered);
    }

    /**
     * <p>isForceNoProgramFilter.</p>
     *
     * @return a boolean.
     */
    public boolean isForceNoProgramFilter() {
        return forceNoProgramFilter;
    }

    /**
     * <p>Setter for the field <code>forceNoProgramFilter</code>.</p>
     *
     * @param forceNoProgramFilter a boolean.
     */
    public void setForceNoProgramFilter(boolean forceNoProgramFilter) {
        this.forceNoProgramFilter = forceNoProgramFilter;
    }

    /**
     * <p>isCampaignsFiltered.</p>
     *
     * @return a boolean.
     */
    public boolean isCampaignsFiltered() {
        return campaignsFiltered;
    }

    /**
     * <p>Setter for the field <code>campaignsFiltered</code>.</p>
     *
     * @param campaignsFiltered a boolean.
     */
    public void setCampaignsFiltered(boolean campaignsFiltered) {
        boolean oldValue = isCampaignsFiltered();
        this.campaignsFiltered = campaignsFiltered;
        firePropertyChange(PROPERTY_CAMPAIGNS_FILTERED, oldValue, campaignsFiltered);
    }

    /**
     * <p>isForceNoCampaignFilter.</p>
     *
     * @return a boolean.
     */
    public boolean isForceNoCampaignFilter() {
        return forceNoCampaignFilter;
    }

    /**
     * <p>Setter for the field <code>forceNoCampaignFilter</code>.</p>
     *
     * @param forceNoCampaignFilter a boolean.
     */
    public void setForceNoCampaignFilter(boolean forceNoCampaignFilter) {
        this.forceNoCampaignFilter = forceNoCampaignFilter;
    }

    /**
     * <p>Getter for the field <code>searchDate</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.SearchDateDTO} object.
     */
    public SearchDateDTO getSearchDate() {
        return searchDate;
    }

    /**
     * <p>Setter for the field <code>searchDate</code>.</p>
     *
     * @param searchDate a {@link fr.ifremer.reefdb.dto.SearchDateDTO} object.
     */
    public void setSearchDate(SearchDateDTO searchDate) {
        SearchDateDTO oldSearchDate = getSearchDate();
        this.searchDate = searchDate;
        firePropertyChange(PROPERTY_SEARCH_DATE, oldSearchDate, searchDate);
    }

    /**
     * <p>getSearchDateId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSearchDateId() {
        return getSearchDate() == null ? null : getSearchDate().getId();
    }

    /**
     * <p>Getter for the field <code>startDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * <p>Setter for the field <code>startDate</code>.</p>
     *
     * @param startDate a {@link java.util.Date} object.
     */
    public void setStartDate(LocalDate startDate) {
        LocalDate oldDate = getStartDate();
        this.startDate = startDate;
        firePropertyChange(PROPERTY_START_DATE, oldDate, startDate);
    }

    /**
     * <p>Getter for the field <code>endDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * <p>Setter for the field <code>endDate</code>.</p>
     *
     * @param endDate a {@link java.util.Date} object.
     */
    public void setEndDate(LocalDate endDate) {
        LocalDate oldDate = getEndDate();
        this.endDate = endDate;
        firePropertyChange(PROPERTY_END_DATE, oldDate, endDate);
    }

    /**
     * <p>Getter for the field <code>location</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.LocationDTO} object.
     */
    public LocationDTO getLocation() {
        return location;
    }

    /**
     * <p>Setter for the field <code>location</code>.</p>
     *
     * @param location a {@link fr.ifremer.reefdb.dto.referential.LocationDTO} object.
     */
    public void setLocation(LocationDTO location) {
        LocationDTO oldLocation = getLocation();
        this.location = location;
        firePropertyChange(PROPERTY_LOCATION, oldLocation, location);
    }

    /**
     * <p>getLocationId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getLocationId() {
        return getLocation() == null ? null : getLocation().getId();
    }

    /**
     * <p>isLocationsFiltered.</p>
     *
     * @return a boolean.
     */
    public boolean isLocationsFiltered() {
        return locationsFiltered;
    }

    /**
     * <p>Setter for the field <code>locationsFiltered</code>.</p>
     *
     * @param locationsFiltered a boolean.
     */
    public void setLocationsFiltered(boolean locationsFiltered) {
        boolean oldValue = isLocationsFiltered();
        this.locationsFiltered = locationsFiltered;
        firePropertyChange(PROPERTY_LOCATIONS_FILTERED, oldValue, locationsFiltered);
    }

    /**
     * <p>isForceNoLocationFilter.</p>
     *
     * @return a boolean.
     */
    public boolean isForceNoLocationFilter() {
        return forceNoLocationFilter;
    }

    /**
     * <p>Setter for the field <code>forceNoLocationFilter</code>.</p>
     *
     * @param forceNoLocationFilter a boolean.
     */
    public void setForceNoLocationFilter(boolean forceNoLocationFilter) {
        this.forceNoLocationFilter = forceNoLocationFilter;
    }

    /**
     * <p>Getter for the field <code>state</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.StateDTO} object.
     */
    public StateDTO getState() {
        return state;
    }

    /**
     * <p>Setter for the field <code>state</code>.</p>
     *
     * @param state a {@link fr.ifremer.reefdb.dto.StateDTO} object.
     */
    public void setState(StateDTO state) {
        StateDTO oldState = getState();
        this.state = state;
        firePropertyChange(PROPERTY_STATE, oldState, state);
    }

    /**
     * <p>getStateId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getStateId() {
        return getState() == null ? null : getState().getId();
    }

    /**
     * <p>Getter for the field <code>share</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.SynchronizationStatusDTO} object.
     */
    public SynchronizationStatusDTO getShare() {
        return share;
    }

    /**
     * <p>Setter for the field <code>share</code>.</p>
     *
     * @param share a {@link fr.ifremer.reefdb.dto.SynchronizationStatusDTO} object.
     */
    public void setShare(SynchronizationStatusDTO share) {
        SynchronizationStatusDTO oldShare = getShare();
        this.share = share;
        firePropertyChange(PROPERTY_SHARE, oldShare, share);
    }

    /**
     * <p>getShareId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getShareId() {
        return getShare() == null ? null : getShare().getId();
    }

    /**
     * <p>Getter for the field <code>surveysTableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableUIModel} object.
     */
    public SurveysTableUIModel getSurveysTableUIModel() {
        return surveysTableUIModel;
    }

    /**
     * <p>Setter for the field <code>surveysTableUIModel</code>.</p>
     *
     * @param surveysTableUIModel a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableUIModel} object.
     */
    public void setSurveysTableUIModel(SurveysTableUIModel surveysTableUIModel) {
        this.surveysTableUIModel = surveysTableUIModel;
        surveysTableUIModel.setMainUIModel(this);
    }

    /**
     * <p>Getter for the field <code>operationsTableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableUIModel} object.
     */
    public OperationsTableUIModel getOperationsTableUIModel() {
        return operationsTableUIModel;
    }

    /**
     * <p>Setter for the field <code>operationsTableUIModel</code>.</p>
     *
     * @param operationsTableUIModel a {@link fr.ifremer.reefdb.ui.swing.content.home.operation.OperationsTableUIModel} object.
     */
    public void setOperationsTableUIModel(OperationsTableUIModel operationsTableUIModel) {
        this.operationsTableUIModel = operationsTableUIModel;
        operationsTableUIModel.setMainUIModel(this);
    }

    /**
     * <p>Getter for the field <code>selectedSurvey</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public SurveysTableRowModel getSelectedSurvey() {
        return selectedSurvey;
    }

    /**
     * <p>Setter for the field <code>selectedSurvey</code>.</p>
     *
     * @param selectedSurvey a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public void setSelectedSurvey(SurveysTableRowModel selectedSurvey) {
        this.selectedSurvey = selectedSurvey;
        firePropertyChange(PROPERTY_SELECTED_SURVEY, null, selectedSurvey);
    }

}
