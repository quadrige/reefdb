package fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ManageFiltersListTableUITableModel extends AbstractReefDbTableModel<ManageFiltersListTableUIRowModel> {

	/** Constant <code>TYPE</code> */
	public static final ReefDbColumnIdentifier<ManageFiltersListTableUIRowModel> TYPE = ReefDbColumnIdentifier.newId(
			ManageFiltersListTableUIRowModel.PROPERTY_TYPE,
			n("reefdb.context.filtersList.label"),
			n("reefdb.context.filtersList.label.tip"),
			String.class);

	/** Constant <code>FILTER</code> */
	public static final ReefDbColumnIdentifier<ManageFiltersListTableUIRowModel> FILTER = ReefDbColumnIdentifier.newId(
			ManageFiltersListTableUIRowModel.PROPERTY_FILTER,
			n("reefdb.context.filtersList.name"),
			n("reefdb.context.filtersList.name.tip"),
			FilterDTO.class);
	
	/**
	 * <p>Constructor for ManageFiltersListTableUITableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	public ManageFiltersListTableUITableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ManageFiltersListTableUIRowModel createNewRow() {
		return new ManageFiltersListTableUIRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ManageFiltersListTableUIRowModel> getFirstColumnEditing() {
		return FILTER;
	}
}
