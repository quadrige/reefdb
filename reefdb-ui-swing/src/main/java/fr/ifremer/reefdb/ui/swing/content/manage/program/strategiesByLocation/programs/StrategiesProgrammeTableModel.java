package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.programs;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.time.LocalDate;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class StrategiesProgrammeTableModel extends AbstractReefDbTableModel<StrategiesProgrammeTableRowModel> {

    /**
     * Identifiant pour la colonne local.
     */
    public static final ReefDbColumnIdentifier<StrategiesProgrammeTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.local"),
            n("reefdb.program.strategies.program.local.tip"),
            Boolean.class);

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final ReefDbColumnIdentifier<StrategiesProgrammeTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_NAME,
            n("reefdb.program.strategies.program.name.short"),
            n("reefdb.program.strategies.program.name.tip"),
            String.class);

	/**
	 * Identifiant pour la colonne date debut.
	 */
    public static final ReefDbColumnIdentifier<StrategiesProgrammeTableRowModel> START_DATE = ReefDbColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_START_DATE,
            n("reefdb.program.strategies.program.startDate.short"),
            n("reefdb.program.strategies.program.startDate.tip"),
			LocalDate.class);

	/**
	 * Identifiant pour la colonne date fin.
	 */
    public static final ReefDbColumnIdentifier<StrategiesProgrammeTableRowModel> END_DATE = ReefDbColumnIdentifier.newId(
    		StrategiesProgrammeTableRowModel.PROPERTY_END_DATE,
            n("reefdb.program.strategies.program.endDate.short"),
            n("reefdb.program.strategies.program.endDate.tip"),
			LocalDate.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public StrategiesProgrammeTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public StrategiesProgrammeTableRowModel createNewRow() {
		return new StrategiesProgrammeTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<StrategiesProgrammeTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
