package fr.ifremer.reefdb.ui.swing;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.awt.visitor.BuildTreeVisitor;
import jaxx.runtime.awt.visitor.ComponentTreeNode;
import jaxx.runtime.awt.visitor.GetCompopentAtPointVisitor;
import jaxx.runtime.swing.help.JAXXHelpBroker;
import jaxx.runtime.swing.help.JAXXHelpUI;
import jaxx.runtime.swing.help.JAXXHelpUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractButton;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

/**
 * Help broker.
 *
 * @since 1.1
 */
public class ReefDbHelpBroker extends JAXXHelpBroker {

    /**
     * Logger
     */
    private static final Log LOG = LogFactory.getLog(ReefDbHelpBroker.class);

    /**
     * <p>Constructor for ReefDbHelpBroker.</p>
     *
     * @param defaultID a {@link java.lang.String} object.
     */
    public ReefDbHelpBroker(String defaultID) {
        super("reefdb", "help",
                defaultID,
                (JAXXHelpUIHandler) ReefDbUIContext.getInstance());
    }

    /** {@inheritDoc} */
    @Override
    public void prepareUI(JAXXObject c) {

        Assert.notNull(c, "parameter c can not be null!");

        // l'ui doit avoir un boutton showHelp
        AbstractButton help = getShowHelpButton(c);

        if (help != null) {

            // attach context to button
            if (LOG.isDebugEnabled()) {
                LOG.debug("attach context to showhelp button " + c);
            }
            help.putClientProperty(JAXX_CONTEXT_ENTRY, c);

            // add tracking action
            ActionListener listener = getShowHelpAction();
            if (LOG.isDebugEnabled()) {
                LOG.debug("adding tracking action " + listener);
            }
            help.addActionListener(listener);

            if (LOG.isDebugEnabled()) {
                LOG.debug("done for " + c);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public String findHelpId(Component comp) {

        if (comp == null) {
            comp = ReefDbUIContext.getInstance().getMainUI();
        }
        JAXXHelpUI<?> parentContainer = SwingUtil.getParent(comp, JAXXHelpUI.class);

        String result;
        if (parentContainer != null && this != parentContainer.getBroker()) {

            JAXXHelpBroker broker = parentContainer.getBroker();
            result = broker.findHelpId(comp);
        } else {
            result = super.findHelpId(comp);
        }

        if (result == null) {
            result = "reefdb.index.help";
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Component getDeppestComponent(Component mouseComponent, MouseEvent event) {
        ComponentTreeNode tree = BuildTreeVisitor.buildTree(mouseComponent);

        Point point = event.getPoint();

        Component component = GetCompopentAtPointVisitor.get(tree, point);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Component at (" + point + "): " + component);
        }
        return component;
    }

}
