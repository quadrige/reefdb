package fr.ifremer.reefdb.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.ui.swing.action.AbstractChangeScreenAction;
import fr.ifremer.reefdb.ui.swing.ReefDbScreen;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;

/**
 * Action permettant de visualiser l ecran des stratégies et lieux associés.
 */
public class ShowStrategiesAction extends AbstractChangeScreenAction {

	/**
	 * Constructor.
	 *
	 * @param handler Controleur
	 */
	public ShowStrategiesAction(final ReefDbMainUIHandler handler) {
		super(handler, true, ReefDbScreen.STRATEGY_LOCATION);
	}
}
