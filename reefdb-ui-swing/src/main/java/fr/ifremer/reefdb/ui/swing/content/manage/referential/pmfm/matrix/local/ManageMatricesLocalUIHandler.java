package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.menu.ManageMatricesMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedFractionsCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedFractionCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Matrices au niveau local
 */
public class ManageMatricesLocalUIHandler extends AbstractReefDbTableUIHandler<MatricesTableRowModel, ManageMatricesLocalUIModel, ManageMatricesLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageMatricesLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageMatricesLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageMatricesLocalUIModel model = new ManageMatricesLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageMatricesLocalUI ui) {
        initUI(ui);

        // Initialisation du tableau
        initTable();

        getUI().getManageMatricesLocalTableDeleteBouton().setEnabled(false);
        getUI().getManageMatricesLocalTableReplaceBouton().setEnabled(false);

        ManageMatricesMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(true);

        menuUIModel.addPropertyChangeListener(ManageMatricesMenuUIModel.PROPERTY_RESULTS, evt -> loadMatrices((List<MatrixDTO>) evt.getNewValue()));

    }

    private void loadMatrices(List<MatrixDTO> matrices) {

        getUI().getManageMatricesLocalTableNouveauBouton().setEnabled(true);

        getModel().setBeans(matrices);
        getModel().setModify(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(MatricesTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(true);

        // description
        final TableColumnExt descriptionCol = addColumn(MatricesTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(true);

        // Comment, creation and update dates
        addCommentColumn(MatricesTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(MatricesTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(MatricesTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MatricesTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);
        fixDefaultColumnWidth(statusCol);

        // associated fractions
        final TableColumnExt associatedFractionsCol = addColumn(
                new AssociatedFractionsCellEditor(getTable(), getUI(), true),
                new AssociatedFractionCellRenderer(),
                MatricesTableModel.ASSOCIATED_FRACTIONS);
        associatedFractionsCol.setSortable(true);

        MatricesTableModel tableModel = new MatricesTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.matrices.local"), MatricesTableModel.ASSOCIATED_FRACTIONS);

        // Initialisation du tableau
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        associatedFractionsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<MatricesTableRowModel> getTableModel() {
        return (MatricesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageMatricesLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<MatricesTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            MatricesTableRowModel rowModel = addedRows.get(0);
            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(rowModel);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, MatricesTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(MatricesTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && row.sizeFractions() > 0 && isUnique(row);
    }

    private boolean isUnique(MatricesTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (MatricesTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.pmfm.matrix"), row.getName(), t("reefdb.property.referential.local")),
                            MatricesTableRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<MatrixDTO> existingMatrices = getContext().getReferentialService().searchMatrices(StatusFilter.ALL, null, null);
                if (CollectionUtils.isNotEmpty(existingMatrices)) {
                    for (MatrixDTO matrix : existingMatrices) {
                        if (!matrix.getId().equals(row.getId())
                                && row.getName().equalsIgnoreCase(matrix.getName())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.pmfm.matrix"), row.getName(), ReefDbBeans.isLocalStatus(matrix.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    MatricesTableRowModel.PROPERTY_NAME);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();

    }

}
