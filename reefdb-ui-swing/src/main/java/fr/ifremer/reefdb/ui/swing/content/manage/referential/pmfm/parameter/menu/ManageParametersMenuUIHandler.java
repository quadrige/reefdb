package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Controlleur du menu pour la gestion des Parameters au niveau local
 */
public class ManageParametersMenuUIHandler extends AbstractReefDbUIHandler<ManageParametersMenuUIModel, ManageParametersMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageParametersMenuUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageParametersMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageParametersMenuUIModel model = new ManageParametersMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageParametersMenuUI ui) {
        initUI(ui);

        // listen to model changes on 'local' to adapt combo box content
        getModel().addPropertyChangeListener(ManageParametersMenuUIModel.PROPERTY_LOCAL, evt -> {
            getUI().getStatusCombo().setData(getContext().getReferentialService().getStatus(getModel().getStatusFilter()));
            reloadComboBox();
        });


        // Initialiser les combobox
        initComboBox();
    }

	/**
	 * Initialisation des combobox
	 */
	private void initComboBox() {

        List<ParameterDTO> parameters = getContext().getReferentialService().getParameters(getModel().getStatusFilter());

		initBeanFilterableComboBox(
				getUI().getLabelCombo(),
                parameters,
				null,
                DecoratorService.NAME);
		
		initBeanFilterableComboBox(
				getUI().getCodeCombo(),
                parameters,
				null);
		
		initBeanFilterableComboBox(getUI().getStatusCombo(),
				getContext().getReferentialService().getStatus(getModel().getStatusFilter()),
				null);
		
        initBeanFilterableComboBox(getUI().getParameterGroupCombo(), 
				getContext().getReferentialService().getParameterGroup(StatusFilter.ACTIVE),
				null);

        ReefDbUIs.forceComponentSize(getUI().getLabelCombo());
        ReefDbUIs.forceComponentSize(getUI().getCodeCombo());
        ReefDbUIs.forceComponentSize(getUI().getStatusCombo());
        ReefDbUIs.forceComponentSize(getUI().getParameterGroupCombo());
	}

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        List<ParameterDTO> parameters = getContext().getReferentialService().getParameters(getModel().getStatusFilter());
        getUI().getLabelCombo().setData(parameters);
        getUI().getCodeCombo().setData(parameters);
    }
}
