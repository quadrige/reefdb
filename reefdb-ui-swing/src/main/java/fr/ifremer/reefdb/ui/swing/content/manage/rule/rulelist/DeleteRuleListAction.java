package fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de supprimer une observation dans le tableau des observations de l ecran d accueil.
 */
public class DeleteRuleListAction extends AbstractReefDbAction<RuleListUIModel, RuleListUI, RuleListUIHandler> {
    
    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteRuleListAction(final RuleListUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
    public boolean prepareAction() throws Exception {
        return super.prepareAction() && checkReadOnlyRuleLists() && askBeforeDelete(
                t("reefdb.action.delete.confirm.title"),
                t("reefdb.rule.ruleList.delete.message"));
    }

    private boolean checkReadOnlyRuleLists() {
        // return true if at least one rule list is read only
        if (getModel().getSelectedRows().stream().anyMatch(RuleListDTO::isReadOnly)) {
            getContext().getDialogHelper().showWarningDialog(
                    t("reefdb.rule.ruleList.delete.readOnly.message"),
                    t("quadrige3.error.business.warning")
            );
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
	@Override
	public void doAction() {
        getContext().getRuleListService().deleteRuleLists(
                getContext().getAuthenticationInfo(),
                Lists.newArrayList(getModel().getSelectedRows()));
        getModel().deleteSelectedRows();
	}

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        // reload combobox
        RulesUI parentUI = (RulesUI) ReefDbUIs.getParentUI(getUI());
        parentUI.getRulesMenuUI().getHandler().reloadComboBox();
    }
}
