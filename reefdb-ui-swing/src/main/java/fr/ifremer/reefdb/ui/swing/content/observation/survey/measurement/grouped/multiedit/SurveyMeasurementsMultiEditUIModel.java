package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.multiedit;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsMultiEditUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.SurveyMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import java.util.Set;

public class SurveyMeasurementsMultiEditUIModel
    extends AbstractMeasurementsMultiEditUIModel<MeasurementDTO, SurveyMeasurementsGroupedRowModel, SurveyMeasurementsMultiEditUIModel> {

    @Override
    public Set<ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel>> getIdentifiersToCheck() {
        return ImmutableSet.of(
            SurveyMeasurementsGroupedTableModel.TAXON_GROUP,
            SurveyMeasurementsGroupedTableModel.TAXON,
            SurveyMeasurementsGroupedTableModel.INPUT_TAXON_NAME,
            (ReefDbColumnIdentifier<SurveyMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.ANALYST
        );
    }
}
