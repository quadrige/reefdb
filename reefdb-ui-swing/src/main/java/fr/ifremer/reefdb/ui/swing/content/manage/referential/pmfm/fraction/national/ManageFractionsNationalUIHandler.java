package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.menu.ManageFractionsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedMatricesCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedSupportsCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Fractions au niveau national
 */
public class ManageFractionsNationalUIHandler extends
        AbstractReefDbTableUIHandler<FractionsTableRowModel, ManageFractionsNationalUIModel, ManageFractionsNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageFractionsNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageFractionsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageFractionsNationalUIModel model = new ManageFractionsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ManageFractionsNationalUI ui) {
        initUI(ui);

        ManageFractionsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(false);

        menuUIModel.addPropertyChangeListener(ManageFractionsMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<FractionDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(FractionsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(FractionsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(FractionsTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(FractionsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(FractionsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                FractionsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.NATIONAL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // associated matrices
        final TableColumnExt associatedSupportsCol = addColumn(
                new AssociatedMatricesCellEditor(getTable(), getUI(), false),
                new AssociatedSupportsCellRenderer(),
                FractionsTableModel.ASSOCIATED_SUPPORTS);
        associatedSupportsCol.setSortable(true);

        FractionsTableModel tableModel = new FractionsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        associatedSupportsCol.setVisible(false);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.fractions.local"), FractionsTableModel.ASSOCIATED_SUPPORTS);

        // Initialisation du tableau
        initTable(getTable(), true);

        // Les colonnes optionnelles sont invisibles
        descriptionCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<FractionsTableRowModel> getTableModel() {
        return (FractionsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageFractionsNationalTable();
    }
}
