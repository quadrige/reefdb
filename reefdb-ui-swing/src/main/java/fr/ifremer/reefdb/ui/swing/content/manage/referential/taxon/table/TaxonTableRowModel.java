package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.referential.CitationDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.TaxonomicLevelDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>TaxonTableRowModel class.</p>
 *
 * @author Antoine
 */
public class TaxonTableRowModel extends AbstractReefDbRowUIModel<TaxonDTO, TaxonTableRowModel> implements TaxonDTO, ErrorAware {

    private static final Binder<TaxonDTO, TaxonTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(TaxonDTO.class, TaxonTableRowModel.class);

    private static final Binder<TaxonTableRowModel, TaxonDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(TaxonTableRowModel.class, TaxonDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for TaxonTableRowModel.</p>
     */
    public TaxonTableRowModel() {
        this(false);
    }

    /**
     * <p>Constructor for TaxonTableRowModel.</p>
     *
     * @param readOnly a boolean.
     */
    public TaxonTableRowModel(boolean readOnly) {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
        setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEditable() {
        return !isReadOnly() && super.isEditable();
    }

    /** {@inheritDoc} */
    @Override
    protected TaxonDTO newBean() {
        return ReefDbBeanFactory.newTaxonDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTemporary() {
        return delegateObject.isTemporary();
    }

    /** {@inheritDoc} */
    @Override
    public void setTemporary(boolean temporary) {
        delegateObject.setTemporary(temporary);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isObsolete() {
        return delegateObject.isObsolete();
    }

    /** {@inheritDoc} */
    @Override
    public void setObsolete(boolean obsolete) {
        delegateObject.setObsolete(obsolete);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isVirtual() {
        return delegateObject.isVirtual();
    }

    /** {@inheritDoc} */
    @Override
    public void setVirtual(boolean virtual) {
        delegateObject.setVirtual(virtual);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isReferent() {
        return delegateObject.isReferent();
    }

    /** {@inheritDoc} */
    @Override
    public void setReferent(boolean referent) {
        delegateObject.setReferent(referent);
    }

    /** {@inheritDoc} */
    @Override
    public CitationDTO getCitation() {
        return delegateObject.getCitation();
    }

    /** {@inheritDoc} */
    @Override
    public void setCitation(CitationDTO citation) {
        delegateObject.setCitation(citation);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getReferenceTaxonId() {
        return delegateObject.getReferenceTaxonId();
    }

    /** {@inheritDoc} */
    @Override
    public void setReferenceTaxonId(Integer referenceTaxonId) {
        delegateObject.setReferenceTaxonId(referenceTaxonId);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getParentTaxonId() {
        return delegateObject.getParentTaxonId();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentTaxonId(Integer parentTaxonId) {
        delegateObject.setParentTaxonId(parentTaxonId);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupDTO getTaxonGroups(int index) {
        return delegateObject.getTaxonGroups(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTaxonGroupsEmpty() {
        return delegateObject.isTaxonGroupsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeTaxonGroups() {
        return delegateObject.sizeTaxonGroups();
    }

    /** {@inheritDoc} */
    @Override
    public void addTaxonGroups(TaxonGroupDTO taxonGroup) {
        delegateObject.addTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllTaxonGroups(Collection<TaxonGroupDTO> taxonGroup) {
        delegateObject.addAllTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeTaxonGroups(TaxonGroupDTO taxonGroup) {
        return delegateObject.removeTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllTaxonGroups(Collection<TaxonGroupDTO> taxonGroup) {
        return delegateObject.removeAllTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsTaxonGroups(TaxonGroupDTO taxonGroup) {
        return delegateObject.containsTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllTaxonGroups(Collection<TaxonGroupDTO> taxonGroup) {
        return delegateObject.containsAllTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonGroupDTO> getTaxonGroups() {
        return delegateObject.getTaxonGroups();
    }

    /** {@inheritDoc} */
    @Override
    public void setTaxonGroups(List<TaxonGroupDTO> taxonGroup) {
        delegateObject.setTaxonGroups(taxonGroup);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonDTO getParentTaxon() {
        return delegateObject.getParentTaxon();
    }

    /** {@inheritDoc} */
    @Override
    public void setParentTaxon(TaxonDTO parentTaxon) {
        delegateObject.setParentTaxon(parentTaxon);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonDTO getReferenceTaxon() {
        return delegateObject.getReferenceTaxon();
    }

    /** {@inheritDoc} */
    @Override
    public void setReferenceTaxon(TaxonDTO referenceTaxon) {
        delegateObject.setReferenceTaxon(referenceTaxon);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonDTO getCompositeTaxons(int index) {
        return delegateObject.getCompositeTaxons(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCompositeTaxonsEmpty() {
        return delegateObject.isCompositeTaxonsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeCompositeTaxons() {
        return delegateObject.sizeCompositeTaxons();
    }

    /** {@inheritDoc} */
    @Override
    public void addCompositeTaxons(TaxonDTO compositeTaxons) {
        delegateObject.addCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllCompositeTaxons(Collection<TaxonDTO> compositeTaxons) {
        delegateObject.addAllCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeCompositeTaxons(TaxonDTO compositeTaxons) {
        return delegateObject.removeCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllCompositeTaxons(Collection<TaxonDTO> compositeTaxons) {
        return delegateObject.removeAllCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsCompositeTaxons(TaxonDTO compositeTaxons) {
        return delegateObject.containsCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllCompositeTaxons(Collection<TaxonDTO> compositeTaxons) {
        return delegateObject.containsAllCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<TaxonDTO> getCompositeTaxons() {
        return delegateObject.getCompositeTaxons();
    }

    /** {@inheritDoc} */
    @Override
    public void setCompositeTaxons(Collection<TaxonDTO> compositeTaxons) {
        delegateObject.setCompositeTaxons(compositeTaxons);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonomicLevelDTO getLevel() {
        return delegateObject.getLevel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLevel(TaxonomicLevelDTO level) {
        delegateObject.setLevel(level);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public String getTaxRef() {
        return delegateObject.getTaxRef();
    }

    /** {@inheritDoc} */
    @Override
    public void setTaxRef(String taxRef) {
        delegateObject.setTaxRef(taxRef);
    }

    /** {@inheritDoc} */
    @Override
    public String getWormsRef() {
        return delegateObject.getWormsRef();
    }

    /** {@inheritDoc} */
    @Override
    public void setWormsRef(String wormsRef) {
        delegateObject.setWormsRef(wormsRef);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
