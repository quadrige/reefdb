package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de delete un fraction local
 */
public class DeleteFractionAction extends AbstractReefDbAction<ManageFractionsLocalUIModel, ManageFractionsLocalUI, ManageFractionsLocalUIHandler> {

    private Set<? extends FractionDTO> toDelete;
    private boolean deleteOk = false;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteFractionAction(final ManageFractionsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        toDelete = getModel().getSelectedRows();
        List<String> names = ReefDbBeans.collectProperties(toDelete, FractionDTO.PROPERTY_NAME);
        return super.prepareAction()
                && CollectionUtils.isNotEmpty(toDelete)
                && askBeforeDeleteMany(
                t("reefdb.action.delete.fraction.title"),
                t("reefdb.action.delete.fraction.message"),
                names);

    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // check usage
        List<String> used = Lists.newArrayList();
        for (FractionDTO fraction : toDelete) {
            if (fraction.getId() != null) {
                if (getContext().getReferentialService().isFractionUsedInReferential(fraction.getId())) {
                    used.add(fraction.getName());
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.referential.message"));
            return;
        }

        for (FractionDTO fraction : toDelete) {
            if (fraction.getId() != null) {
                if (getContext().getReferentialService().isFractionUsedInRules(fraction.getId())) {
                    used.add(fraction.getName());
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.rule.message"));
            return;
        }

        // apply delete
        getContext().getReferentialService().deleteFractions(ReefDbBeans.collectIds(toDelete));
        deleteOk = true;
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        if (deleteOk) {
            getModel().deleteSelectedRows();
            getUI().getMenuUI().getHandler().reloadComboBox();
        }

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        deleteOk = false;
        super.releaseAction();
    }
}
