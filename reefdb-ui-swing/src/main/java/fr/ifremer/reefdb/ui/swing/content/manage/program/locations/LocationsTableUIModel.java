package fr.ifremer.reefdb.ui.swing.content.manage.program.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Modele pour la zone des lieux.
 */
public class LocationsTableUIModel extends AbstractReefDbTableUIModel<AppliedStrategyDTO, LocationsTableRowModel, LocationsTableUIModel> {

    /** Constant <code>PROPERTY_PERIODS_ENABLED="periodsEnabled"</code> */
    public static final String PROPERTY_PERIODS_ENABLED = "periodsEnabled";
    /** Constant <code>PROPERTY_LOADED="loaded"</code> */
    public static final String PROPERTY_LOADED = "loaded";
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";
    private boolean periodsEnabled;
    private boolean loaded;
    private boolean editable;
    private ProgramsTableRowModel selectedProgram;

    private StrategiesTableRowModel selectedStrategy;

    /**
     * Constructor.
     */
    public LocationsTableUIModel() {
        super();
    }

    /**
     * <p>isPeriodsEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isPeriodsEnabled() {
        return periodsEnabled;
    }

    /**
     * <p>Setter for the field <code>periodsEnabled</code>.</p>
     *
     * @param periodsEnabled a boolean.
     */
    public void setPeriodsEnabled(boolean periodsEnabled) {
        this.periodsEnabled = periodsEnabled;
        firePropertyChange(PROPERTY_PERIODS_ENABLED, null, periodsEnabled);
    }

    /**
     * <p>isLoaded.</p>
     *
     * @return a boolean.
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * <p>Setter for the field <code>loaded</code>.</p>
     *
     * @param loaded a boolean.
     */
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
        firePropertyChange(PROPERTY_LOADED, null, loaded);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }

    /**
     * <p>Getter for the field <code>selectedProgram</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public ProgramsTableRowModel getSelectedProgram() {
        return selectedProgram;
    }

    /**
     * <p>Setter for the field <code>selectedProgram</code>.</p>
     *
     * @param selectedProgram a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public void setSelectedProgram(ProgramsTableRowModel selectedProgram) {
        this.selectedProgram = selectedProgram;
    }

    /**
     * <p>Getter for the field <code>selectedStrategy</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel} object.
     */
    public StrategiesTableRowModel getSelectedStrategy() {
        return selectedStrategy;
    }

    /**
     * <p>Setter for the field <code>selectedStrategy</code>.</p>
     *
     * @param selectedStrategy a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableRowModel} object.
     */
    public void setSelectedStrategy(StrategiesTableRowModel selectedStrategy) {
        this.selectedStrategy = selectedStrategy;
    }
}
