package fr.ifremer.reefdb.ui.swing.content.home.operation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

/**
 * Duplicate sampling operation action.
 * {@link Deprecated} Use fr.ifremer.reefdb.dto.ReefDbBeans#duplicate(fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO) outside an action
 */
@Deprecated
public class DuplicateOperationAction extends AbstractReefDbAction<OperationsTableUIModel, OperationsTableUI, OperationsTableUIHandler> {

	/**
	 * Row.
	 */
	private OperationsTableRowModel newRow = null;
	
	/**
	 * Constructor.
	 *
	 * @param handler the handler
	 */
	public DuplicateOperationAction(final OperationsTableUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public boolean prepareAction() throws Exception {
        return (super.prepareAction() && getModel().getSelectedRows().size() == 1);
    }

	/** {@inheritDoc} */
	@Override
	public void doAction() {
		
		// Selected operation
		final OperationsTableRowModel operationsTableRowModel = getModel().getSelectedRows().iterator().next();
		if (operationsTableRowModel != null) {
			
			// Duplicate operation
			final SamplingOperationDTO duplicateOperation = ReefDbBeans.duplicate(operationsTableRowModel.toBean());

			// Add duplicate operation to table
			newRow = getModel().addNewRow(duplicateOperation);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void postSuccessAction() {
		super.postSuccessAction();

        getModel().setModify(true);

		// Add focus on duplicate row
		getHandler().setFocusOnCell(newRow);
		getModel().getSurvey().setSamplingOperations(getModel().getBeans());
	}
}
