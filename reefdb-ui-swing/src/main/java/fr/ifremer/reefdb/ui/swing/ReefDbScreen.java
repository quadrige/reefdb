package fr.ifremer.reefdb.ui.swing;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.Screen;

/**
 * Enumeration of any internal screen of application.
 */
public class ReefDbScreen extends Screen {

    public ReefDbScreen(String screenName) {
        super(screenName);
    }

    /**
     * Onglet observation (accueil).
     */
    public static final ReefDbScreen OBSERVATION = new ReefDbScreen("OBSERVATION");

    /**
     * Onglet observation general.
     */
    public static final ReefDbScreen OBSERVATION_GENERAL = new ReefDbScreen("OBSERVATION_GENERAL");

    /**
     * Onglet observation mesures.
     */
    public static final ReefDbScreen OBSERVATION_MESURES = new ReefDbScreen("OBSERVATION_MESURES");

    /**
     * Onglet prelevements mesures.
     */
    public static final ReefDbScreen PRELEVEMENTS_MESURES = new ReefDbScreen("PRELEVEMENTS_MESURES");

    /**
     * Onglet phtos.
     */
    public static final ReefDbScreen PHOTOS = new ReefDbScreen("PHOTOS");

    /**
     * Configuration.
     */
    public static final ReefDbScreen CONFIG = new ReefDbScreen("CONFIG");

    /**
     * Configurations des contextes.
     */
    public static final ReefDbScreen CONTEXT = new ReefDbScreen("CONTEXT");

    /**
     * Configurations des groupes de taxons.
     */
    public static final ReefDbScreen TAXON_GROUP = new ReefDbScreen("TAXON_GROUP");

    /**
     * Configurations des filtres des lieux.
     */
    public static final ReefDbScreen FILTER_LOCATION = new ReefDbScreen("FILTER_LOCATION");

    /**
     * Configurations des filtres des programmes.
     */
    public static final ReefDbScreen FILTER_PROGRAM = new ReefDbScreen("FILTER_PROGRAM");

    /**
     * Configurations des filtres des campagnes.
     */
    public static final ReefDbScreen FILTER_CAMPAIGN = new ReefDbScreen("FILTER_CAMPAIGN");

    /**
     * Department filter.
     */
    public static final ReefDbScreen FILTER_DEPARTMENT = new ReefDbScreen("FILTER_DEPARTMENT");

    /**
     * Instrument filter.
     */
    public static final ReefDbScreen FILTER_ANALYSIS_INSTRUMENT = new ReefDbScreen("FILTER_ANALYSIS_INSTRUMENT");

    /**
     * Equipment filter.
     */
    public static final ReefDbScreen FILTER_SAMPLING_EQUIPMENT = new ReefDbScreen("FILTER_SAMPLING_EQUIPMENT");

    /**
     * Pmfm filter.
     */
    public static final ReefDbScreen FILTER_PMFM = new ReefDbScreen("FILTER_PMFM");

    /**
     * Taxon filter.
     */
    public static final ReefDbScreen FILTER_TAXON = new ReefDbScreen("FILTER_TAXON");

    /**
     * Taxon group filter.
     */
    public static final ReefDbScreen FILTER_TAXON_GROUP = new ReefDbScreen("FILTER_TAXON_GROUP");

    /**
     * User filter.
     */
    public static final ReefDbScreen FILTER_USER = new ReefDbScreen("FILTER_USER");

    /**
     * Configurations des lieux de surveillance.
     */
    public static final ReefDbScreen LOCATION = new ReefDbScreen("LOCATION");

    /**
     * Configurations des strategies et lieux.
     */
    public static final ReefDbScreen STRATEGY_LOCATION = new ReefDbScreen("STRATEGY_LOCATION");

    /**
     * Configurations des programmes.
     */
    public static final ReefDbScreen PROGRAM = new ReefDbScreen("PROGRAM");

    /**
     * Configurations des campagnes.
     */
    public static final ReefDbScreen CAMPAIGN = new ReefDbScreen("CAMPAIGN");

    /**
     * Configurations des regles.
     */
    public static final ReefDbScreen RULE_LIST = new ReefDbScreen("RULE_LIST");

    /**
     * Configurations des taxons.
     */
    public static final ReefDbScreen TAXON = new ReefDbScreen("TAXON");

    /**
     * Users management
     */
    public static final ReefDbScreen USER = new ReefDbScreen("USER");

    /**
     * Departments management
     */
    public static final ReefDbScreen DEPARTMENT = new ReefDbScreen("DEPARTMENT");

    /**
     * PMFM parameters management
     */
    public static final ReefDbScreen PARAMETER = new ReefDbScreen("PARAMETER");

    /**
     * PMFM methods management
     */
    public static final ReefDbScreen METHOD = new ReefDbScreen("METHOD");

    /**
     * PMFM fractions management
     */
    public static final ReefDbScreen FRACTION = new ReefDbScreen("FRACTION");

    /**
     * PMFM matrices management
     */
    public static final ReefDbScreen MATRIX = new ReefDbScreen("MATRIX");

    /**
     * PMFM quadruplets management
     */
    public static final ReefDbScreen PMFM = new ReefDbScreen("PMFM");

    /**
     * Referentiel des unites
     */
    public static final ReefDbScreen UNIT = new ReefDbScreen("UNIT");

    /**
     * Referentiel des engins de prelevement
     */
    public static final ReefDbScreen SAMPLING_EQUIPMENT = new ReefDbScreen("SAMPLING_EQUIPMENT");

    /**
     * Referential Analysis Instruments
     */
    public static final ReefDbScreen ANALYSIS_INSTRUMENT = new ReefDbScreen("ANALYSIS_INSTRUMENT");

    /**
     * Ecran d'extration des données
     */
    public static final ReefDbScreen EXTRACTION = new ReefDbScreen("EXTRACTION");

}
