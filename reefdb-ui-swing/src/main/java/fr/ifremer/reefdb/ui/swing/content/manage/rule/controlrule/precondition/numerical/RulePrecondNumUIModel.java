package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.numerical;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.control.NumericPreconditionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePrecondNumUIModel extends AbstractReefDbTableUIModel<NumericPreconditionDTO, RulePrecondNumRowModel, RulePrecondNumUIModel> {

    private PmfmDTO basePmfm;
    static final String PROPERTY_BASE_PMFM = "basePmfm";
    private PmfmDTO usedPmfm;
    static final String PROPERTY_USED_PMFM = "usedPmfm";
    private boolean adjusting;
    private boolean editable;
    public static final String PROPERTY_EDITABLE = "editable";

    PmfmDTO getBasePmfm() {
        return basePmfm;
    }

    public void setBasePmfm(PmfmDTO basePmfm) {
        this.basePmfm = basePmfm;
        firePropertyChange(PROPERTY_BASE_PMFM, null, basePmfm);
    }

    PmfmDTO getUsedPmfm() {
        return usedPmfm;
    }

    public void setUsedPmfm(PmfmDTO usedPmfm) {
        this.usedPmfm = usedPmfm;
        firePropertyChange(PROPERTY_USED_PMFM, null, usedPmfm);
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    public boolean isRulePreconditionValid() {
        return getRowCount() > 0 && getRows().stream().anyMatch(row -> row.getMin() != null && row.getMax() != null);
    }

    public void setRulePreconditionValid(boolean dummy) {
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }
}
