package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Delete action.
 */
public class DeleteFilterAction extends AbstractReefDbAction<FilterListUIModel, FilterListUI, FilterListUIHandler> {

    private List<? extends FilterDTO> filtersToDelete;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteFilterAction(final FilterListUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        if (!super.prepareAction() || getModel().getSelectedRows().isEmpty()) {
            return false;
        }

        if (askBeforeDelete(t("reefdb.action.delete.confirm.title"), t("reefdb.filter.filterList.delete.message"))) {

            // collect filter to delete
            filtersToDelete = Lists.newArrayList(getModel().getSelectedRows());

            // verify filter usage in context
            if (!getContext().getContextService().checkFiltersNotUsedInContext(filtersToDelete)) {
                getContext().getDialogHelper().showErrorDialog(t("reefdb.filter.filterList.delete.filterUsedInContext"));
                return false;
            }

            return true;
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {
        getContext().getContextService().deleteFilters(filtersToDelete);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        // remove beans from table
        getModel().deleteSelectedRows();

        getHandler().getParentUI().getHandler().clearFilterElements();

        getHandler().reloadComboBox();
    }

}
