package fr.ifremer.reefdb.ui.swing.content.manage.program.programs;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.program.ProgramPrivilegeIds;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.departments.DepartmentsDialogUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.users.UsersDialogUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.StatusRenderer;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class ProgramsTableUIHandler extends AbstractReefDbTableUIHandler<ProgramsTableRowModel, ProgramsTableUIModel, ProgramsTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ProgramsTableUIHandler.class);

    private StrategyAndLocationLoader strategyAndLocationLoader;
    private boolean newRowSelected;

    /**
     * <p>Constructor for ProgramsTableUIHandler.</p>
     */
    public ProgramsTableUIHandler() {
        super(ProgramsTableRowModel.PROPERTY_NAME,
            ProgramsTableRowModel.PROPERTY_COMMENT,
            ProgramsTableRowModel.PROPERTY_CODE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{ProgramsTableRowModel.PROPERTY_STRATEGIES, ProgramsTableRowModel.PROPERTY_STRATEGIES_LOADED,
            ProgramsTableRowModel.PROPERTY_LOCATIONS, ProgramsTableRowModel.PROPERTY_LOCATIONS_LOADED,
            ProgramsTableRowModel.PROPERTY_ERRORS};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<ProgramsTableRowModel> getTableModel() {
        return (ProgramsTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getProgramsTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ProgramsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ProgramsTableUIModel model = new ProgramsTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(ProgramsTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initTable();

        // Ajout des listeners
        initListeners();

        // Initilisation du bouton editer
        getUI().getEditCombobox().setMaximumRowCount(10);
        initActionComboBox(getUI().getEditCombobox());

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRowValid(final ProgramsTableRowModel row) {
        row.getErrors().clear();

        return !row.isEditable() || (super.isRowValid(row) && isProgramValid(row));
    }

    private boolean isProgramValid(ProgramsTableRowModel row) {

        // check if a national program has at least one manager
        if (!row.isLocal() && row.sizeManagerPersons() == 0) {
            ReefDbBeans.addError(row, t("reefdb.programs.validation.error.noManager"), ProgramsTableRowModel.PROPERTY_CODE);
        }

        // check if at least one location is set
        if (row.isLocationsLoaded() && row.isLocationsEmpty()) {
            ReefDbBeans.addError(row, t("reefdb.programs.validation.error.noLocationOnProg"), ProgramsTableRowModel.PROPERTY_CODE);
        }

        // check sub-models
        if (!row.isStrategiesValid() || !row.isLocationsValid()) {
            ReefDbBeans.addError(row, t("reefdb.program.tables.error"), ProgramsTableRowModel.PROPERTY_CODE);
        }

        return row.isErrorsEmpty();
    }

    /**
     * load programs in table
     *
     * @param programs programs to load
     */
    public void loadPrograms(Collection<ProgramDTO> programs) {

        getModel().setSingleSelectedRow(null);
        getModel().setBeans(programs);

        boolean isLocalAdmin = getContext().isAuthenticatedAsLocalAdmin();
        boolean isNationalAdmin = getContext().isAuthenticatedAsNationalAdmin();
        getModel().getRows().forEach(program -> {
            // a program is editable if save is enabled
            program.setEditable(getModel().isSaveEnabled());

            // determinate if current user is manager
            program.setCurrentUserIsManager(
                // user must be local admin (in all cases)
                isLocalAdmin && (
                    // for local program
                    program.isLocal() ||
                        // and national admin or manager of a national program
                        (isNationalAdmin ||
                            ReefDbBeans.isProgramManager(
                                program,
                                getContext().getDataContext().getRecorderPersonId(),
                                getContext().getDataContext().getRecorderDepartmentId()
                            )
                        )
                )
            );

        });

        autoSelectRow();
    }

    /**
     * Clear program table
     */
    public void clearTable() {

        getModel().setBeans(null);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<ProgramsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        // should be only one row
        if (addedRows.size() == 1) {
            final ProgramsTableRowModel row = addedRows.get(0);

            if (checkCodeOnNewProgramRow(row)) {

                row.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
                row.setCurrentUserIsManager(true);
                row.setNewCode(true);
                setFocusOnCell(row);

            } else {

                // the code is invalid, delete this row
                SwingUtilities.invokeLater(() -> {
                    getModel().deleteRow(row);
                    getModel().setSingleSelectedRow(null);
                });
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, ProgramsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
    }

    /**
     * Add a code on a new row, do nothing on an existing row, delete a new row without code
     *
     * @param selectedRow
     * @return true if new code or already existing code
     */
    private boolean checkCodeOnNewProgramRow(final ProgramsTableRowModel selectedRow) {

        // Restricted access to admin (Mantis #31020)
        if (!getContext().isAuthenticatedAsLocalAdmin()) {
            getContext().getDialogHelper().showErrorDialog(t("reefdb.error.authenticate.accessDenied"));
            return false;
        }

        // Ask user for a new code
        boolean edit = StringUtils.isNotBlank(selectedRow.getCode());
        String newCode = (String) getContext().getDialogHelper().showInputDialog(
            getUI(),
            t("reefdb.property.program.code"),
            edit ? t("reefdb.program.editCode.title") : t("reefdb.program.setCode.title"),
            null,
            selectedRow.getCode());

        if (StringUtils.isBlank(newCode)) {
            return false;
        }

        newCode = newCode.trim();
        // Check if the program code already exist in UI
        for (ProgramsTableRowModel programCheck : getModel().getRows()) {
            if (selectedRow == programCheck) continue;
            if (newCode.equalsIgnoreCase(programCheck.getCode())) {
                // Error messages
                getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.error.alreadyExists.referential", t("reefdb.property.program"), newCode,
                        ReefDbBeans.isLocalStatus(programCheck.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                    edit ? t("reefdb.program.editCode.title") : t("reefdb.program.setCode.title"));
                return false;
            }
        }

        ProgramDTO existingProgram = getContext().getProgramStrategyService().isProgramExists(newCode);
        if (existingProgram != null) {
            // Error messages
            getContext().getDialogHelper().showErrorDialog(
                t("reefdb.error.alreadyExists.referential", t("reefdb.property.program"), newCode,
                    ReefDbBeans.isLocalStatus(existingProgram.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                edit ? t("reefdb.program.editCode.title") : t("reefdb.program.setCode.title"));
            return false;
        }

        selectedRow.setCode(newCode);
        return true;
    }

    /**
     * <p>editProgramCode.</p>
     */
    public void editProgramCode() {
        ProgramsTableRowModel row = getModel().getSingleSelectedRow();
        if (row != null && row.isLocal() && row.isNewCode()) {
            if (checkCodeOnNewProgramRow(row)) {
                row.setDirty(true);
            }
        }
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(ProgramsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            final ProgramsUI adminProgrammeUI = getProgramsUI();
            final ProgramsTableRowModel selectedProgram = getModel().getSingleSelectedRow();

            // Si un seul element a ete selectionne
            if (selectedProgram != null && StringUtils.isNotBlank(selectedProgram.getCode())) {

                newRowSelected = true;

                // Suppression des informations du contexte
                getContext().setSelectedProgramCode(selectedProgram.getCode());

                if (strategyAndLocationLoader != null && !strategyAndLocationLoader.isDone()) {
                    strategyAndLocationLoader.cancel(true);
                }
                adminProgrammeUI.getStrategiesTableUI().getModel().setLoading(true);
                adminProgrammeUI.getLocationsTableUI().getModel().setLoading(true);
                strategyAndLocationLoader = new StrategyAndLocationLoader(selectedProgram);
                strategyAndLocationLoader.execute();

            }
        });

        getModel().addPropertyChangeListener(ProgramsTableUIModel.PROPERTY_SELECTED_ROWS, evt -> {

            if (getModel().getSelectedRows().size() == 1) {
                ProgramsTableRowModel selected = getModel().getSelectedRows().iterator().next();
                getUI().getEditCombobox().setEnabled(selected.getStatus() != null && !selected.isLocal());
            } else {
                getUI().getEditCombobox().setEnabled(false);
            }
        });

        // add a click listener on table to handle unselect strategy table
        getTable().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (!newRowSelected) {
                    getModel().firePropertyChanged(ProgramsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, null, null);
                }
                newRowSelected = false;
            }

        });

        // Add listener on saveEnabled property to disable change (Mantis #47532)
        getModel().addPropertyChangeListener(ProgramsTableUIModel.PROPERTY_SAVE_ENABLED, evt -> {
            getModel().getRows().forEach(programsTableRowModel -> programsTableRowModel.setEditable(getModel().isSaveEnabled()));
            getUI().getProgramsTable().invalidate();
            getUI().getProgramsTable().repaint();
            autoSelectRow();
        });

    }

    /**
     * <p>removeLocations.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     */
    public void removeLocations(List<Integer> locationIds) {
        ProgramsTableRowModel programModel = getModel().getSingleSelectedRow();
        if (programModel != null) {
            programModel.getLocations().removeIf(location -> locationIds.contains(location.getId()));
            recomputeRowValidState(programModel);
        }
    }

    public void showManagers() {
        showPersonDialog(ProgramPrivilegeIds.MANAGER);
    }

    public void showRecorderPersons() {
        showPersonDialog(ProgramPrivilegeIds.RECORDER);
    }

    public void showFullViewerPersons() {
        showPersonDialog(ProgramPrivilegeIds.FULL_VIEWER);
    }

    public void showViewerPersons() {
        showPersonDialog(ProgramPrivilegeIds.VIEWER);
    }

    public void showValidatorPersons() {
        showPersonDialog(ProgramPrivilegeIds.VALIDATOR);
    }

    protected void showPersonDialog(ProgramPrivilegeIds privilege) {

        if (getModel().getSingleSelectedRow() == null) {
            return;
        }

        UsersDialogUI usersDialogUI = new UsersDialogUI(getUI());
        usersDialogUI.getModel().setPrivilege(privilege);
        usersDialogUI.getModel().setProgram(getModel().getSingleSelectedRow());

        openDialog(usersDialogUI);

    }

    public void showRecorderDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.RECORDER);
    }

    public void showFullViewerDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.FULL_VIEWER);
    }

    public void showViewerDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.VIEWER);
    }

    public void showValidatorDepartments() {
        showDepartmentDialog(ProgramPrivilegeIds.VALIDATOR);
    }

    protected void showDepartmentDialog(ProgramPrivilegeIds privilege) {

        if (getModel().getSingleSelectedRow() == null) {
            return;
        }

        DepartmentsDialogUI departmentsDialogUI = new DepartmentsDialogUI(getUI());
        departmentsDialogUI.getModel().setPrivilege(privilege);
        departmentsDialogUI.getModel().setProgram(getModel().getSingleSelectedRow());

        openDialog(departmentsDialogUI);

    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Column code
        final TableColumnExt columnCode = addColumn(
            ProgramsTableModel.CODE);
        columnCode.setSortable(true);

        // Column libelle
        final TableColumnExt columnLibelle = addColumn(
            ProgramsTableModel.NAME);
        columnLibelle.setSortable(true);

        // Column description
        final TableColumnExt columnDescription = addCommentColumn(ProgramsTableModel.DESCRIPTION, ProgramsTableRowModel.PROPERTY_DESCRIPTION, true);
        columnDescription.setSortable(false);

        // Comment, creation and update dates
        addCommentColumn(ProgramsTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(ProgramsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(ProgramsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // Column local
        final TableColumnExt columnStatus = addColumn(null, new StatusRenderer(), ProgramsTableModel.STATUS);
        fixColumnWidth(columnStatus, ReefDbUIs.REEFDB_CHECKBOX_WIDTH);
        columnStatus.setSortable(true);

        // Modele de la table
        final ProgramsTableModel tableModel = new ProgramsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Columns non editable
        tableModel.setNoneEditableCols(ProgramsTableModel.CODE, ProgramsTableModel.STATUS);

        // Les columns obligatoire sont toujours presentes
        columnStatus.setHideable(false);
        columnCode.setHideable(false);
        columnLibelle.setHideable(false);
        columnDescription.setHideable(false);

        // Initialisation de la table
        initTable(getTable());

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(4);

        // Tri par defaut
        getTable().setSortOrder(ProgramsTableModel.CODE, SortOrder.ASCENDING);
    }

    /**
     * Selection d une ligne dans le tableau suivant l identifiant de la ligne.
     */
    public void autoSelectRow() {

        String previousSelectedProgramCode = getContext().getSelectedProgramCode();

        if (StringUtils.isBlank(previousSelectedProgramCode) && getModel().getRowCount() > 1) {
            return;
        }

        // force deselection first
        getModel().setSingleSelectedRow(null);

        ProgramsTableRowModel rowToSelect = null;
        if (getModel().getRowCount() == 1) {

            // unique row
            rowToSelect = getModel().getRows().get(0);

        } else {

            // find row with the previous selected program code
            for (final ProgramsTableRowModel row : getModel().getRows()) {
                if (row.getCode().equals(previousSelectedProgramCode)) {
                    rowToSelect = row;
                    break;
                }
            }
        }

        // Selected row
        if (rowToSelect != null) {
            ProgramsTableRowModel finalRowToSelect = rowToSelect;
            SwingUtilities.invokeLater(() -> {
                selectRow(finalRowToSelect);
                getModel().setSingleSelectedRow(finalRowToSelect);
            });
        }
    }

    /**
     * Save update (data in memory) on slave table in master table
     */
    public void keepModificationOnStrategiesTable() {

        if (getModel().getSingleSelectedRow() != null) {
            getModel().getSingleSelectedRow().setStrategies(getProgramsUI().getStrategiesTableUI().getHandler().getModel().getBeans());
            getModel().getSingleSelectedRow().setStrategiesLoaded(true);
        }

    }

    /**
     * <p>getProgramsUI.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI} object.
     */
    public ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<ProgramsTableUIModel> getValidator() {
        return getUI().getValidator();
    }

    private class StrategyAndLocationLoader extends SwingWorker<Object, Object> {

        private final ProgramsUI adminProgrammeUI = getProgramsUI();
        private final ProgramsTableRowModel selectedProgram;

        public StrategyAndLocationLoader(ProgramsTableRowModel selectedProgram) {
            this.selectedProgram = selectedProgram;
        }

        @Override
        protected Object doInBackground() {

            getContext().getProgramStrategyService().loadStrategiesAndLocations(selectedProgram);

            return null;
        }

        @Override
        protected void done() {
            if (isCancelled()) {
                return;
            }
            try {
                get();
            } catch (InterruptedException | ExecutionException e) {
                throw new ReefDbTechnicalException(e.getMessage(), e);
            }

            // Update strategies table
            adminProgrammeUI.getStrategiesTableUI().getHandler().load(selectedProgram);

            // Update applied strategies table
            adminProgrammeUI.getLocationsTableUI().getHandler().loadMonitoringLocationsFromProgram(selectedProgram);

            // Suppression des PSFMs
            adminProgrammeUI.getPmfmsTableUI().getHandler().clearTable();

            // revalidate program
            recomputeRowValidState(selectedProgram);
            adminProgrammeUI.getHandler().getValidator().doValidate();

        }
    }
}
