package fr.ifremer.reefdb.ui.swing.content.synchro.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.reefdb.dto.SearchDateDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;

import java.util.Date;
import java.util.List;

/**
 * Modele pour la zone des programmes.
 */
public class ProgramSelectUIModel extends AbstractEmptyUIModel<ProgramSelectUIModel> {

    /** Constant <code>PROPERTY_SEARCH_DATE="searchDate"</code> */
    public static final String PROPERTY_SEARCH_DATE = "searchDate";
    /** Constant <code>PROPERTY_START_DATE="startDate"</code> */
    public static final String PROPERTY_START_DATE = "startDate";
    /** Constant <code>PROPERTY_END_DATE="endDate"</code> */
    public static final String PROPERTY_END_DATE = "endDate";
    /** Constant <code>PROPERTY_DIRTY_ONLY="dirtyOnly"</code> */
    public static final String PROPERTY_DIRTY_ONLY = "dirtyOnly";
    public static final String PROPERTY_ENABLE_PHOTO = "enablePhoto";

    private List<ProgramDTO> selectedPrograms;
    private boolean dirtyOnly;
    private boolean enablePhoto;
    private SearchDateDTO searchDate;
    private Date startDate;
    private Date endDate;

    /**
     * <p>Getter for the field <code>selectedPrograms</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<ProgramDTO> getSelectedPrograms() {
        return selectedPrograms;
    }

    /**
     * <p>Setter for the field <code>selectedPrograms</code>.</p>
     *
     * @param selectedPrograms a {@link java.util.List} object.
     */
    public void setSelectedPrograms(List<ProgramDTO> selectedPrograms) {
        this.selectedPrograms = selectedPrograms;
    }

    /**
     * <p>isDirtyOnly.</p>
     *
     * @return a boolean.
     */
    public boolean isDirtyOnly() {
        return dirtyOnly;
    }

    /**
     * <p>Setter for the field <code>dirtyOnly</code>.</p>
     *
     * @param dirtyOnly a boolean.
     */
    public void setDirtyOnly(boolean dirtyOnly) {
        this.dirtyOnly = dirtyOnly;
        firePropertyChange(PROPERTY_DIRTY_ONLY, !dirtyOnly, dirtyOnly);
    }

    public boolean isEnablePhoto() {
        return enablePhoto;
    }

    public void setEnablePhoto(boolean enablePhoto) {
        this.enablePhoto = enablePhoto;
        firePropertyChange(PROPERTY_ENABLE_PHOTO, !enablePhoto, enablePhoto);
    }

    /**
     * <p>Getter for the field <code>searchDate</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.SearchDateDTO} object.
     */
    public SearchDateDTO getSearchDate() {
        return searchDate;
    }

    /**
     * <p>getSearchDateId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSearchDateId() {
        return getSearchDate() == null ? null : getSearchDate().getId();
    }

    /**
     * <p>Setter for the field <code>searchDate</code>.</p>
     *
     * @param searchDate a {@link fr.ifremer.reefdb.dto.SearchDateDTO} object.
     */
    public void setSearchDate(SearchDateDTO searchDate) {
        SearchDateDTO oldSearchDate = getSearchDate();
        this.searchDate = searchDate;
        firePropertyChange(PROPERTY_SEARCH_DATE, oldSearchDate, searchDate);
    }

    /**
     * <p>Getter for the field <code>startDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * <p>Setter for the field <code>startDate</code>.</p>
     *
     * @param startDate a {@link java.util.Date} object.
     */
    public void setStartDate(Date startDate) {
        Date oldDate = getStartDate();
        this.startDate = startDate;
        firePropertyChange(PROPERTY_START_DATE, oldDate, startDate);
    }

    /**
     * <p>Getter for the field <code>endDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * <p>Setter for the field <code>endDate</code>.</p>
     *
     * @param endDate a {@link java.util.Date} object.
     */
    public void setEndDate(Date endDate) {
        Date oldDate = getEndDate();
        this.endDate = endDate;
        firePropertyChange(PROPERTY_END_DATE, oldDate, endDate);
    }

}
