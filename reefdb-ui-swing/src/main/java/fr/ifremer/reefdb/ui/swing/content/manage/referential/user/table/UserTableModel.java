/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>UserTableModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class UserTableModel extends AbstractReefDbTableModel<UserRowModel> {

    /**
     * <p>Constructor for UserTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param createNewRowAllowed a boolean.
     */
    public UserTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
    }

    /** {@inheritDoc} */
    @Override
    public UserRowModel createNewRow() {
        return new UserRowModel();
    }

    /** Constant <code>REG_CODE</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> REG_CODE = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_REG_CODE,
            n("reefdb.property.user.regCode"),
            n("reefdb.property.user.regCode"),
            String.class);

    /** Constant <code>LASTNAME</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> LASTNAME = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_NAME,
            n("reefdb.property.user.lastname"),
            n("reefdb.property.user.lastname"),
            String.class,
            true);

    /** Constant <code>FIRSTNAME</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> FIRSTNAME = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_FIRST_NAME,
            n("reefdb.property.user.firstname"),
            n("reefdb.property.user.firstname"),
            String.class,
            true);

    /** Constant <code>DEPARTMENT</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> DEPARTMENT = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_DEPARTMENT,
            n("reefdb.property.department"),
            n("reefdb.property.department"),
            DepartmentDTO.class,
            true);

    /** Constant <code>INTRANET_LOGIN</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> INTRANET_LOGIN = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_INTRANET_LOGIN,
            n("reefdb.property.user.intranetLogin"),
            n("reefdb.property.user.intranetLogin"),
            String.class,
            true);

    /** Constant <code>EXTRANET_LOGIN</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> EXTRANET_LOGIN = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_EXTRANET_LOGIN,
            n("reefdb.property.user.extranetLogin"),
            n("reefdb.property.user.extranetLogin"),
            String.class);

    /** Constant <code>HAS_PASSWORD</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> HAS_PASSWORD = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_HAS_PASSWORD,
            n("reefdb.property.user.password"),
            n("reefdb.property.user.password"),
            Boolean.class);

    /** Constant <code>EMAIL</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> EMAIL = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_EMAIL,
            n("reefdb.property.email"),
            n("reefdb.property.email"),
            String.class);

    /** Constant <code>PHONE</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> PHONE = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_PHONE,
            n("reefdb.property.phone"),
            n("reefdb.property.phone"),
            String.class);

    /** Constant <code>ADDRESS</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> ADDRESS = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_ADDRESS,
            n("reefdb.property.address"),
            n("reefdb.property.address"),
            String.class);

    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> STATUS = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class,
            true);

    // PRIVILEGES
    /** Constant <code>PRIVILEGES</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> PRIVILEGES = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_PRIVILEGE_SIZE,
            n("reefdb.property.user.privileges"),
            n("reefdb.property.user.privileges"),
            Integer.class);

    /** Constant <code>ORGANISM</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> ORGANISM = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_ORGANISM,
            n("reefdb.property.department"),
            n("reefdb.property.department"),
            String.class);

    /** Constant <code>ADMIN_CENTER</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> ADMIN_CENTER = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_ADMIN_CENTER,
            n("reefdb.property.user.adminCenter"),
            n("reefdb.property.user.adminCenter"),
            String.class);

    /** Constant <code>SITE</code> */
    public static final ReefDbColumnIdentifier<UserRowModel> SITE = ReefDbColumnIdentifier.newId(
            UserRowModel.PROPERTY_SITE,
            n("reefdb.property.user.site"),
            n("reefdb.property.user.site"),
            String.class);


    public static final ReefDbColumnIdentifier<UserRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        UserRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<UserRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        UserRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<UserRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        UserRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);



    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<UserRowModel> getFirstColumnEditing() {
        return LASTNAME;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<UserRowModel> propertyName) {

        boolean editable = super.isCellEditable(rowIndex, columnIndex, propertyName);

        if (editable) {
            if (propertyName == PRIVILEGES) {

                // local privileges can be empty BUT editable
                UserRowModel row = getEntry(rowIndex);
                editable = row.getStatus() == null
                        || !StatusFilter.NATIONAL.toStatusCodes().contains(row.getStatus().getCode())
                        || row.sizePrivilege() > 0;

            } else if (propertyName == HAS_PASSWORD) {

                // password changes is allowed for admins and for the user itself
                UserRowModel row = getEntry(rowIndex);
                editable = (getTableUIModel().isAdmin() || getTableUIModel().isUserItself(row)) && StringUtils.isNotBlank(row.getIntranetLogin());
            }

        }

        return editable;

    }

    /** {@inheritDoc} */
    @Override
    public UserTableUIModel getTableUIModel() {
        return (UserTableUIModel) super.getTableUIModel();
    }
}
