package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des analysisInstruments
 */
public class AnalysisInstrumentsMenuUIModel extends DefaultReferentialMenuUIModel {

    /** Constant <code>PROPERTY_ANALYSIS_INSTRUMENT="analysisInstrument"</code> */
    public static final String PROPERTY_ANALYSIS_INSTRUMENT = "analysisInstrument";
    private AnalysisInstrumentDTO analysisInstrument;

    /**
     * <p>Getter for the field <code>analysisInstrument</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO} object.
     */
    public AnalysisInstrumentDTO getAnalysisInstrument() {
        return analysisInstrument;
    }

    /**
     * <p>Setter for the field <code>analysisInstrument</code>.</p>
     *
     * @param analysisInstrument a {@link fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO} object.
     */
    public void setAnalysisInstrument(AnalysisInstrumentDTO analysisInstrument) {
        this.analysisInstrument = analysisInstrument;
        firePropertyChange(PROPERTY_ANALYSIS_INSTRUMENT, null, analysisInstrument);
    }

    /**
     * <p>getAnalysisInstrumentId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getAnalysisInstrumentId() {
        return getAnalysisInstrument() != null ? getAnalysisInstrument().getId() : null;
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setAnalysisInstrument(null);
    }
}
