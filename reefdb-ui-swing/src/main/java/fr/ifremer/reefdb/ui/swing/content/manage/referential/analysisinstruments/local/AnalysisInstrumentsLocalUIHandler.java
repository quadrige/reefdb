package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.menu.AnalysisInstrumentsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.table.AnalysisInstrumentsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.table.AnalysisInstrumentsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des analysisInstruments en local
 */
public class AnalysisInstrumentsLocalUIHandler extends
        AbstractReefDbTableUIHandler<AnalysisInstrumentsTableRowModel, AnalysisInstrumentsLocalUIModel, AnalysisInstrumentsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(AnalysisInstrumentsLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final AnalysisInstrumentsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final AnalysisInstrumentsLocalUIModel model = new AnalysisInstrumentsLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(final AnalysisInstrumentsLocalUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // hide context filter panel
        ui.getMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getMenuUI().getHandler().forceLocal(true);

        // init listener on found analysis instruments
        ui.getMenuUI().getModel().addPropertyChangeListener(AnalysisInstrumentsMenuUIModel.PROPERTY_RESULTS, evt -> {
            if (evt.getNewValue() != null) {
                loadTable((List<AnalysisInstrumentDTO>) evt.getNewValue());
            }
        });

        // Initialisation du tableau
        initTable();

        getUI().getAnalysisInstrumentsLocalDeleteBouton().setEnabled(false);
        getUI().getAnalysisInstrumentsLocalReplaceBouton().setEnabled(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // mnemonic
        TableColumnExt mnemonicCol = addColumn(AnalysisInstrumentsTableModel.NAME);
        mnemonicCol.setSortable(true);

        // description
        TableColumnExt descriptionCol = addColumn(AnalysisInstrumentsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(AnalysisInstrumentsTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(AnalysisInstrumentsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(AnalysisInstrumentsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                AnalysisInstrumentsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        AnalysisInstrumentsTableModel tableModel = new AnalysisInstrumentsTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.analysisInstruments.local"));

        // Initialisation du tableau
        initTable(getTable());

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<AnalysisInstrumentsTableRowModel> getTableModel() {
        return (AnalysisInstrumentsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getAnalysisInstrumentsLocalTable();
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
        getModel().setBeans(null);
    }

    /**
     * <p>loadTable.</p>
     *
     * @param analysisInstruments a {@link java.util.List} object.
     */
    public void loadTable(List<AnalysisInstrumentDTO> analysisInstruments) {
        getModel().setBeans(analysisInstruments);
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<AnalysisInstrumentsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            AnalysisInstrumentsTableRowModel row = addedRows.get(0);

//            getModel().setModify(true);
            row.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));

            // Ajouter le focus sur la cellule de la ligne cree
            setFocusOnCell(row);

        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, AnalysisInstrumentsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(AnalysisInstrumentsTableRowModel row) {

        row.getErrors().clear();

        return super.isRowValid(row) && isUnique(row);
    }

    private boolean isUnique(AnalysisInstrumentsTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (AnalysisInstrumentsTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.analysisInstrument"), row.getName(), t("reefdb.property.referential.local")),
                            AnalysisInstrumentsTableRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<AnalysisInstrumentDTO> existingAnalysisInstrument = getContext().getReferentialService().searchAnalysisInstruments(StatusFilter.ALL, row.getName());
                if (CollectionUtils.isNotEmpty(existingAnalysisInstrument)) {
                    for (AnalysisInstrumentDTO analysisInstrument : existingAnalysisInstrument) {
                        if (!analysisInstrument.getId().equals(row.getId())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.analysisInstrument"), row.getName(), ReefDbBeans.isLocalStatus(analysisInstrument.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    AnalysisInstrumentsTableRowModel.PROPERTY_NAME
                            );
                            break;
                        }
                    }
                }
            }

        }

        return row.getErrors().isEmpty();

    }
}
