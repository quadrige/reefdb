package fr.ifremer.reefdb.ui.swing.content.manage.program.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.SaveAction;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Search action.
 */
public class SearchAction extends AbstractCheckModelAction<ProgramsMenuUIModel, ProgramsMenuUI, ProgramsMenuUIHandler> {

    private List<ProgramDTO> results;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final ProgramsMenuUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler().getProgramsUI().getHandler();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        final ProgramsUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        final ProgramsUIModel model = getLocalModel();
        if (model != null) {
            model.setModify(modelModify);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        final ProgramsUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        // Suppression des informations du contexte
        getContext().setSelectedProgramCode(null);
        getContext().setSelectedLocationId(null);

        return super.prepareAction();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        ProgramStrategyService service = getContext().getProgramStrategyService();
        int userId = SecurityContextHelper.getQuadrigeUserId();
        StatusFilter statusFilter = StatusFilter.toActiveLocalOrNational(getModel().getLocal());
        List<ProgramDTO> programs;

        if (getModel().isOnlyManagedPrograms()) {

            // get managed programs only
            programs = service.getManagedProgramsByUserAndStatus(userId, statusFilter);

        } else {

            // get readable programs only
            programs = service.getReadableProgramsByUserAndStatus(userId, statusFilter);

        }

        results = null;
        if (CollectionUtils.isNotEmpty(programs)) {
            if (getModel().getProgram() != null) {
                // filter by selected program in combo
                results = programs.stream().filter(program -> program.equals(getModel().getProgram())).collect(Collectors.toList());
            } else {
                results = programs;
            }
        }

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setResults(results);

        super.postSuccessAction();
    }

    private ProgramsUIModel getLocalModel() {
        ProgramsUI ui = getHandler().getProgramsUI();
        return ui != null ? ui.getModel() : null;
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        ProgramsUI ui = getHandler().getProgramsUI();
        if (ui != null) {
            return ImmutableList.of(
                ui.getProgramsTableUI().getModel(),
                ui.getLocationsTableUI().getModel(),
                ui.getPmfmsTableUI().getModel(),
                ui.getStrategiesTableUI().getModel()
            );
        }
        return super.getOtherModelsToModify();
    }
}
