package fr.ifremer.reefdb.ui.swing.util.table.editor;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue.QualitativeValueUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import org.jdesktop.swingx.JXTable;

import java.awt.Dimension;

/**
 * <p>AssociatedQualitativeValueCellEditor class.</p>
 *
 */
public class AssociatedQualitativeValueCellEditor extends ButtonCellEditor {

	private final JXTable table;
	
	private final ReefDbUI parentUI;
	
	private boolean editable;
	
	/**
	 * <p>Constructor for AssociatedQualitativeValueCellEditor.</p>
	 *
	 * @param table a {@link org.jdesktop.swingx.JXTable} object.
	 * @param parentUI a {@link fr.ifremer.reefdb.ui.swing.util.ReefDbUI} object.
	 * @param editable a boolean.
	 */
	public AssociatedQualitativeValueCellEditor(
			final JXTable table, 
			final ReefDbUI parentUI,
			final boolean editable) {
		this.table = table;
		this.parentUI = parentUI;
		this.editable = editable;
	}

    /** {@inheritDoc} */
    @Override
    public void onButtonCellAction(final int rowIndex, final int column) {
    	
    	final AbstractReefDbTableModel<?> tableModel = (AbstractReefDbTableModel<?>) table.getModel();

        final int rowModelIndex = table.convertRowIndexToModel(rowIndex);
        
        AbstractReefDbRowUIModel rowModel = tableModel.getEntry(rowModelIndex);
                
        // open associated qualitative value screen
		final QualitativeValueUI qualitativeValueUI = new QualitativeValueUI(parentUI);
		qualitativeValueUI.getModel().setEditable(editable && rowModel.isEditable());
		qualitativeValueUI.getModel().setParentRowModel(rowModel);
        parentUI.getHandler().openDialog(qualitativeValueUI, new Dimension(640, 480));
    }
}
