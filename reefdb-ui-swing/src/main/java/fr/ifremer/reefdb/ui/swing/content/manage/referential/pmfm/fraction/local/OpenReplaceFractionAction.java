package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.ManageFractionsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.local.replace.ReplaceFractionUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.local.replace.ReplaceFractionUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceFractionAction extends AbstractReefDbAction<ManageFractionsLocalUIModel, ManageFractionsLocalUI, ManageFractionsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceFractionAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.local.ManageFractionsLocalUIHandler} object.
     */
    public OpenReplaceFractionAction(ManageFractionsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<FractionDTO, ReplaceFractionUIModel, ReplaceFractionUI> openAction =
                new AbstractOpenReplaceUIAction<FractionDTO, ReplaceFractionUIModel, ReplaceFractionUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.pmfm.fraction");
                    }

                    @Override
                    protected ReplaceFractionUIModel createNewModel() {
                        return new ReplaceFractionUIModel();
                    }

                    @Override
                    protected ReplaceFractionUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceFractionUI(ctx);
                    }

                    @Override
                    protected List<FractionDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getFractions(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected FractionDTO getSelectedSource() {
                        List<FractionDTO> selectedBeans = OpenReplaceFractionAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected FractionDTO getSelectedTarget() {
                        ManageFractionsUI ui = OpenReplaceFractionAction.this.getUI().getParentContainer(ManageFractionsUI.class);
                        List<FractionDTO> selectedBeans = ui.getManageFractionsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
