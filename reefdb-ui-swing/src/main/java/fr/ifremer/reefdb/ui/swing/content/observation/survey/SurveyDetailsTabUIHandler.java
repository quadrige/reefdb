package fr.ifremer.reefdb.ui.swing.content.observation.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet observation general.
 */
public class SurveyDetailsTabUIHandler extends AbstractReefDbUIHandler<SurveyDetailsTabUIModel, SurveyDetailsTabUI> implements TabHandler {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SurveyDetailsTabUIHandler.class);

    /** Constant <code>OBSERVERS_DOUBLE_LIST="observersDoubleList"</code> */
    public static final String OBSERVERS_DOUBLE_LIST = "observersDoubleList";
    /** Constant <code>OBSERVERS_LIST="observersList"</code> */
    public static final String OBSERVERS_LIST = "observersList";

    private AbstractAction unfilterUsersAction;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SurveyDetailsTabUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SurveyDetailsTabUIModel model = new SurveyDetailsTabUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final SurveyDetailsTabUI ui) {
        initUI(ui);
        SwingUtil.setLayerUI(ui.getDetailsPanel(), ui.getSurveyDetailsBlockLayer());

        // Initialisation des combobox
        initComboBox();

        // Init double list
        initObserversLists();

        initListeners();

        // Listen validator
        listenValidatorValid(getValidator(), getModel());
    }

    private void initListeners() {

        // main listener on model
        getModel().addPropertyChangeListener(evt -> {

            if (SurveyDetailsTabUIModel.PROPERTY_OBSERVATION_MODEL.equals(evt.getPropertyName())) {
                // Listener on parent model
                load(getModel().getObservationModel());

            } else if (SurveyDetailsTabUIModel.PROPERTY_OBSERVERS.equals(evt.getPropertyName())) {

                if (getModel().isAdjusting()) return;

                // Reload selected observers
                List<PersonDTO> observers = new ArrayList<>(getModel().getObservers());
//                    getModel().setSelectedObservers(observers);
                getUI().getObserversDoubleList().getHandler().setSelected(observers);
                getUI().getObserversList().setListData(observers.toArray(new PersonDTO[observers.size()]));

            } else if (SurveyDetailsTabUIModel.PROPERTY_PROGRAM.equals(evt.getPropertyName())) {
                // reload locations combo
                updateLocations(false);

            } else if (SurveyDetailsTabUIModel.PROPERTY_LOCATION.equals(evt.getPropertyName())) {
                // update coordinate
                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MIN_LATITUDE_EDITOR_NUMBER_VALUE);
                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MAX_LATITUDE_EDITOR_NUMBER_VALUE);
                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MIN_LONGITUDE_EDITOR_NUMBER_VALUE);
                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_LOCATION_MAX_LONGITUDE_EDITOR_NUMBER_VALUE);
                // reload program combo
                updatePrograms(false);

            } else if (SurveyDetailsTabUIModel.PROPERTY_DATE.equals(evt.getPropertyName())) {
                // reload program combo
                updatePrograms(false);
                updateCampaigns(false);

            } else if (SurveyDetailsTabUIModel.PROPERTY_COORDINATE.equals(evt.getPropertyName())) {
                // update coordinate
                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_SURVEY_LATITUDE_EDITOR_NUMBER_VALUE);
                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_SURVEY_LONGITUDE_EDITOR_NUMBER_VALUE);


            } else if (SurveyDetailsTabUIModel.PROPERTY_LATITUDE.equals(evt.getPropertyName())
                    || SurveyDetailsTabUIModel.PROPERTY_LONGITUDE.equals(evt.getPropertyName())) {
                // update positioning when both latitude and longitude are null
                if (getModel().getLatitude() == null && getModel().getLongitude() == null) {
                    getModel().setPositioning(null);
                }

//            } else if (SurveyDetailsTabUIModel.PROPERTY_TIME.equals(evt.getPropertyName())) {
                // update time editor
//                getUI().applyDataBinding(SurveyDetailsTabUI.BINDING_TIME_EDITOR_DATE);

            } else if (SurveyDetailsTabUIModel.PROPERTY_POSITIONING.equals(evt.getPropertyName())) {
                // update positioning precision label
                getUI().getSelectionPrecisionLabel().setText(getModel().getPositioningPrecision());

            } else if (SurveyDetailsTabUIModel.PROPERTY_RECORDER_DEPARTMENT.equals(evt.getPropertyName())) {
                // update department
                getUI().getInputSaisisseur().setText(decorate(getModel().getRecorderDepartment()));

            } else if (SurveyDetailsTabUIModel.PROPERTY_EDITABLE.equals(evt.getPropertyName())) {
                // select list only if model is not editable
                if (getModel().isEditable()) {
                    getUI().getObserversPanelLayout().setSelected(OBSERVERS_DOUBLE_LIST);
                } else {
                    getUI().getObserversPanelLayout().setSelected(OBSERVERS_LIST);
                }

            }
        });

    }

    /**
     * Load observation.
     *
     * @param survey to load
     */
    private void load(final SurveyDTO survey) {

        // Selected observation
        getModel().setBean(survey);

        // Model not modified
        getModel().setModify(false);
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Intialisation des campagnes
        initBeanFilterableComboBox(
                getUI().getCampaignCombo(),
                null,
                null);
        getUI().getCampaignCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updateCampaigns(true);
        });
        updateCampaigns(false);

        // Initailisation des programmes
        initBeanFilterableComboBox(
                getUI().getProgramCombo(),
                null, //getContext().getObservationService().getAvailablePrograms(null, null, null, false),
                null);
        getUI().getProgramCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updatePrograms(true);
        });
        updatePrograms(false);

        // Initialisation des lieux
        initBeanFilterableComboBox(
                getUI().getLocationCombo(),
                null, //getContext().getObservationService().getAvailableLocations(false),
                null);
        getUI().getLocationCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updateLocations(true);
        });
        updateLocations(false);

        // Initialisation des Positionnements
        initBeanFilterableComboBox(
                getUI().getSelectionPositionnementCombo(),
                getContext().getReferentialService().getPositioningSystems(),
                null);

        // Initialisation des Profondeurs
        initBeanFilterableComboBox(
                getUI().getProfondeurEditor(),
                getContext().getReferentialService().getDepths(),
                null);

    }

    @SuppressWarnings("unchecked")
    private void initObserversLists() {

        // init list
        getUI().getObserversList().setCellRenderer(newListCellRender(PersonDTO.class));

        // init double list
        initBeanList(
                getUI().getObserversDoubleList(),
                new ArrayList<>(),
                new ArrayList<>(),
                300);

        // add unfilter action
        unfilterUsersAction = new AbstractAction("", SwingUtil.createActionIcon("unfilter")){
            @Override
            public void actionPerformed(ActionEvent e) {
                updateUsers(true);
            }
        };
        JButton unfilterUsersButton = new JButton();
        unfilterUsersButton.setAction(unfilterUsersAction);
        unfilterUsersButton.setToolTipText(t("reefdb.common.unfilter"));
        getUI().getObserversDoubleList().getFilterFieldLabel().setVisible(false);
        getUI().getObserversDoubleList().getToolbarLeft().add(unfilterUsersButton, 0);
        updateUsers(false);
    }

    private void updatePrograms(boolean forceNoFilter) {

        if (getModel().isAdjusting() || !getModel().isEditable()) {
            return;
        }
        getModel().setAdjusting(true);

        getUI().getProgramCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.PROGRAM));

        List<ProgramDTO> programs = getContext().getObservationService().getAvailablePrograms(
                null,  // no campaign filter getModel().getCampaignId(),
                getModel().getLocationId(),
                getModel().getDate(),
                forceNoFilter,
                true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        // Mantis #0029591 : Prevent set null when model is loading
        if (!getModel().isLoading() && getModel().getProgram() != null && !programs.contains(getModel().getProgram())) {
            getModel().setProgram(null);
        }
        getUI().getProgramCombo().setData(programs);

        getModel().setAdjusting(false);
    }

    private void updateCampaigns(boolean forceNoFilter) {

        if (getModel().isAdjusting() || !getModel().isEditable()) {
            return;
        }
        getModel().setAdjusting(true);

        getUI().getCampaignCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.CAMPAIGN));

        List<CampaignDTO> campaigns = getContext().getObservationService().getAvailableCampaigns(getModel().getDate(), forceNoFilter);

        // Mantis #0029591 : Prevent set null when model is loading
        if (!getModel().isLoading() && getModel().getCampaign() != null && !campaigns.contains(getModel().getCampaign())) {
            getModel().setCampaign(null);
        }
        getUI().getCampaignCombo().setData(campaigns);

        getModel().setAdjusting(false);
    }

    private void updateLocations(boolean forceNoFilter) {

        if (getModel().isAdjusting() || !getModel().isEditable()) {
            return;
        }
        getModel().setAdjusting(true);

        getUI().getLocationCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.LOCATION));

        List<LocationDTO> locations = getContext().getObservationService().getAvailableLocations(
                null, // pas de filtrage sur les campagne getModel().getCampaignId(),
                getModel().getProgramCode(),
                forceNoFilter, true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        // Mantis #0029591 : Prevent set null when model is loading
        if (!getModel().isLoading() && getModel().getLocation() != null && !locations.contains(getModel().getLocation())) {
            getModel().setLocation(null);
        }
        getUI().getLocationCombo().setData(locations);

        getModel().setAdjusting(false);
    }

    private void updateUsers(boolean forceNoFilter) {

        if (getModel().isAdjusting()) {
            return;
        }
        getModel().setAdjusting(true);

        unfilterUsersAction.setEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.USER));

        List<PersonDTO> users = getContext().getObservationService().getAvailableUsers(forceNoFilter);

        getUI().getObserversDoubleList().getModel().setUniverse(users);
        getUI().getObserversDoubleList().getHandler().sortData();

        getModel().setAdjusting(false);
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<SurveyDetailsTabUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void onShowTab(int currentIndex, int newIndex) {

    }

    /** {@inheritDoc} */
    @Override
    public boolean onRemoveTab() {
        return false;
    }

    /**
     * <p>save.</p>
     */
    public void save() {

        getModel().setAdjusting(true);

        // save observers to parent model
        getModel().setObservers(getModel().getSelectedObservers());

        getModel().setAdjusting(false);
    }
}
