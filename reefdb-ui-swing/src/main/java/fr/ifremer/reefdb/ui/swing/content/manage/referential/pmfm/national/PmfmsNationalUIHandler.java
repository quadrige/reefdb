package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.menu.PmfmMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.table.PmfmTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.table.PmfmTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Quadruplets au niveau national
 */
public class PmfmsNationalUIHandler extends
        AbstractReefDbTableUIHandler<PmfmTableRowModel, PmfmsNationalUIModel, PmfmsNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PmfmsNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(PmfmsNationalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        PmfmsNationalUIModel model = new PmfmsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(PmfmsNationalUI ui) {
        initUI(ui);
        
        // hide 'apply filter'
        ui.getPmfmsNationalMenuUI().getHandler().enableContextFilter(false);

        // force national
        ui.getPmfmsNationalMenuUI().getHandler().forceLocal(false);

        // listen to search results
        ui.getPmfmsNationalMenuUI().getModel().addPropertyChangeListener(PmfmMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<PmfmDTO>) evt.getNewValue()));
        
        // listen to 'apply filter' results
        ui.getPmfmsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            // load only national referential (Mantis #29668)
            getModel().setBeans(ReefDbBeans.filterNationalReferential((List<PmfmDTO>) evt.getNewValue()));
        });

        initTable();

    }

    private void initTable() {

        TableColumnExt idCol = addColumn(PmfmTableModel.PMFM_ID);
        idCol.setSortable(true);
        idCol.setEditable(false);

        // name
        TableColumnExt nameCol = addColumn(PmfmTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // parameter
        TableColumnExt parameterCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.PARAMETER,
                getContext().getReferentialService().getParameters(StatusFilter.NATIONAL_ACTIVE),
                false);
        parameterCol.setSortable(true);
        parameterCol.setEditable(false);

        // support
        final TableColumnExt supportCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.SUPPORT,
                getContext().getReferentialService().getMatrices(StatusFilter.NATIONAL_ACTIVE),
                false);
        supportCol.setSortable(true);
        supportCol.setEditable(false);

        // fraction
        final TableColumnExt fractionCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.FRACTION,
                getContext().getReferentialService().getFractions(StatusFilter.NATIONAL_ACTIVE),
                false);
        fractionCol.setSortable(true);
        fractionCol.setEditable(false);

        // method
        final TableColumnExt methodCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.METHOD,
                getContext().getReferentialService().getMethods(StatusFilter.NATIONAL_ACTIVE),
                false);
        methodCol.setSortable(true);
        methodCol.setEditable(false);

        // unit
        final TableColumnExt unitCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.NATIONAL_ACTIVE),
                false);
        unitCol.setSortable(true);
        unitCol.setEditable(false);

//		// threshold
//		final TableColumnExt stepCol = addColumn(columnModel, PmfmTableModel.THRESHOLD);
//		stepCol.setSortable(true);
//		stepCol.setEditable(false);
//
//		// max decimal number
//		final TableColumnExt maxDecimalNumberCol = addColumn(columnModel, PmfmTableModel.MAX_DECIMAL_NUMBER);
//		maxDecimalNumberCol.setSortable(true);
//		maxDecimalNumberCol.setEditable(false);
//
//		// significant digits number
//		final TableColumnExt significantDigitsNumberCol = addColumn(columnModel, PmfmTableModel.SIGNIFICANT_DIGITS_NUMBER);
//		significantDigitsNumberCol.setSortable(true);
//		significantDigitsNumberCol.setEditable(false);


        // Comment, creation and update dates
        addCommentColumn(PmfmTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(PmfmTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(PmfmTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                PmfmTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.NATIONAL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
                new AssociatedQualitativeValueCellEditor(getTable(), getUI(), false),
                new AssociatedQualitativeValueCellRenderer(),
                PmfmTableModel.QUALITATIVE_VALUES);
        associatedQualitativeValueCol.setSortable(true);
        fixColumnWidth(associatedQualitativeValueCol, 120);

        PmfmTableModel tableModel = new PmfmTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfms.national"), PmfmTableModel.QUALITATIVE_VALUES);

        // Initialisation du tableau
        initTable(getTable(), true);

        // Les colonnes optionnelles sont invisibles
        idCol.setVisible(false);
//		stepCol.setVisible(false);
//		maxDecimalNumberCol.setVisible(false);
//		significantDigitsNumberCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<PmfmTableRowModel> getTableModel() {
        return (PmfmTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getPmfmsNationalTable();
    }

}
