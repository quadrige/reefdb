package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.ManageParametersUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local.replace.ReplaceParameterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local.replace.ReplaceParameterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceParameterAction extends AbstractReefDbAction<ManageParametersLocalUIModel, ManageParametersLocalUI, ManageParametersLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceParameterAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local.ManageParametersLocalUIHandler} object.
     */
    public OpenReplaceParameterAction(ManageParametersLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<ParameterDTO, ReplaceParameterUIModel, ReplaceParameterUI> openAction =
                new AbstractOpenReplaceUIAction<ParameterDTO, ReplaceParameterUIModel, ReplaceParameterUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.pmfm.parameter");
                    }

                    @Override
                    protected ReplaceParameterUIModel createNewModel() {
                        ReplaceParameterUIModel model = new ReplaceParameterUIModel();
                        model.setDecoratorContext(DecoratorService.CODE_NAME);
                        return model;
                    }

                    @Override
                    protected ReplaceParameterUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceParameterUI(ctx);
                    }

                    @Override
                    protected List<ParameterDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getParameters(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected ParameterDTO getSelectedSource() {
                        List<ParameterDTO> selectedBeans = OpenReplaceParameterAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected ParameterDTO getSelectedTarget() {
                        ManageParametersUI ui = OpenReplaceParameterAction.this.getUI().getParentContainer(ManageParametersUI.class);
                        List<ParameterDTO> selectedBeans = ui.getManageParametersNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
