package fr.ifremer.reefdb.ui.swing.content.manage.campaign;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.synchro.vo.SynchroProgressionStatus;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroCheckAction;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.menu.CampaignsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet prelevements mesures.
 */
public class CampaignsUIHandler extends AbstractReefDbUIHandler<CampaignsUIModel, CampaignsUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(CampaignsUIHandler.class);
    private ImportSynchroCheckAction importSynchroCheckAction;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final CampaignsUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final CampaignsUIModel model = new CampaignsUIModel();
        ui.setContextValue(model);

        ui.setContextValue(SwingUtil.createActionIcon("campaign"));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final CampaignsUI ui) {
        initUI(ui);

        ui.getMenuUI().getHandler().enableContextFilter(false);

        // Save models
        getModel().setCampaignsTableUIModel(ui.getCampaignsTableUI().getModel());

        // init image
        SwingUtil.setComponentWidth(ui.getLeftImage(), ui.getMenu().getPreferredSize().width * 9 / 10);
        ui.getLeftImage().setScaled(true);

        initListeners();

        ui.applyDataBinding(CampaignsUI.BINDING_SAVE_BUTTON_ENABLED);

        // check referential update (Mantis #47231)
        SwingUtilities.invokeLater(this::checkForReferentialUpdates);
    }

    @SuppressWarnings("unchecked")
    private void initListeners() {

        // listen to menu results
        getUI().getMenuUI().getModel().addPropertyChangeListener(CampaignsMenuUIModel.PROPERTY_RESULTS, evt -> {

            // affect campaigns
            getModel().getCampaignsTableUIModel().setBeans((Collection<CampaignDTO>) evt.getNewValue());

            getModel().getCampaignsTableUIModel().getRows().forEach(campaign -> {

                // set rows editable if save is allowed
                campaign.setEditable(getModel().isSaveEnabled());

                // set row read-only if user or recorder department is not the same as the current user
                campaign.setReadOnly(!(campaign.getManager().getId().equals(getContext().getDataContext().getRecorderPersonId())
                        || campaign.getRecorderDepartment().getId().equals(getContext().getDataContext().getRecorderDepartmentId())));

            });

        });

        // Listen valid state
        getModel().getCampaignsTableUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());
        listenModelModify(getModel().getCampaignsTableUIModel());

        // Add listener on saveEnabled property to disable change (Mantis #48002)
        getModel().addPropertyChangeListener(CampaignsUIModel.PROPERTY_SAVE_ENABLED, evt -> getModel().getCampaignsTableUIModel().setSaveEnabled(getModel().isSaveEnabled()));

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<CampaignsUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private void checkForReferentialUpdates() {
        if (getContext().isNextImportSynchroCheckActionPrevented()) {
            getModel().setSaveEnabled(true);
            return;
        }
        if (getContext().getSynchroContext().isRunningStatus()) {
            return;
        }
        getImportSynchroCheckAction().execute();
        if (LOG.isDebugEnabled())
            LOG.debug("checkForReferentialUpdates executed");
    }

    private ImportSynchroCheckAction getImportSynchroCheckAction() {
        if (importSynchroCheckAction == null) {
            importSynchroCheckAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(), ImportSynchroCheckAction.class);
            importSynchroCheckAction.setCheckReferentialOnly(true);
            importSynchroCheckAction.setUseOptimisticCheck(true);
        }
        if (!importSynchroCheckAction.isConsumerSet()) {
            importSynchroCheckAction.setConsumer(synchroUIContext -> {

                if (LOG.isDebugEnabled())
                    LOG.debug("check result: " + synchroUIContext.isImportReferential());

                // If error occurs (eg. connection problem) set screen read-only (Mantis #48002)
                if (synchroUIContext.getStatus() == SynchroProgressionStatus.FAILED) {
                    getModel().setSaveEnabled(false);
                    getContext().getDialogHelper().showWarningDialog(t("reefdb.error.synchro.serverNotYetAvailable"));
                } else {
                    getModel().setSaveEnabled(true);
                    // get result
                    if (synchroUIContext.isImportReferential()) {
                        UpdateCampaignsAction updateCampaignsAction = getContext().getActionFactory().createLogicAction(this, UpdateCampaignsAction.class);
                        getContext().getActionEngine().runAction(updateCampaignsAction);
                    }
                }

                // Reset correctly the synchro context and widget (Mantis #48832)
                getContext().getSynchroHandler().report(t("quadrige3.synchro.report.idle"), false);
                getContext().getSynchroContext().resetImportContext();
                getContext().getSynchroContext().saveImportContext(true, true);

            });
        }
        return importSynchroCheckAction;
    }
}
