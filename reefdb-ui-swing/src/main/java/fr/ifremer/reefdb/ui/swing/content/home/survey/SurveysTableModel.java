package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.StateDTO;
import fr.ifremer.reefdb.dto.SynchronizationStatusDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.DepthDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.PositioningSystemDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.time.LocalDate;
import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>SurveysTableModel class.</p>
 *
 * @author Antoine
 */
public class SurveysTableModel extends AbstractReefDbTableModel<SurveysTableRowModel> {

    /** Constant <code>SYNCHRONIZATION_STATUS</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> SYNCHRONIZATION_STATUS = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_SYNCHRONIZATION_STATUS,
            n("reefdb.property.synchronizationStatus"),
            n("reefdb.property.synchronizationStatus"),
            SynchronizationStatusDTO.class);

    /** Constant <code>STATE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> STATE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_STATE,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StateDTO.class);

    /** Constant <code>LOCATION</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> LOCATION = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LOCATION,
            n("reefdb.property.location"),
            n("reefdb.property.location"),
            LocationDTO.class,
            true);

    /** Constant <code>DATE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> DATE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_DATE,
            n("reefdb.property.date"),
            n("reefdb.property.date"),
            LocalDate.class,
            true);

    /** Constant <code>DEPTH</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> DEPTH = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_DEPTH,
            n("reefdb.property.depth"),
            n("reefdb.property.depth"),
            DepthDTO.class);

    public static final ReefDbColumnIdentifier<SurveysTableRowModel> DEPTH_ANALYST = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_DEPTH_ANALYST,
            n("reefdb.property.analyst"),
            n("reefdb.property.analyst"),
            DepartmentDTO.class);

    /** Constant <code>PRECISE_DEPTH</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> PRECISE_DEPTH = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_PRECISE_DEPTH,
            n("reefdb.property.depth.precise"),
            n("reefdb.property.depth.precise"),
            Double.class);

    /** Constant <code>PROGRAM</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> PROGRAM = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_PROGRAM,
            n("reefdb.property.program"),
            n("reefdb.property.program"),
            ProgramDTO.class,
            true);

	public static final ReefDbColumnIdentifier<SurveysTableRowModel> CAMPAIGN = ReefDbColumnIdentifier.newId(
			SurveysTableRowModel.PROPERTY_CAMPAIGN,
			n("reefdb.property.campaign"),
			n("reefdb.property.campaign"),
			CampaignDTO.class);

    /** Constant <code>COMMENT</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.comment"),
            n("reefdb.property.comment"),
            String.class);

    /** Constant <code>LATITUDE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> LATITUDE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LATITUDE,
            n("reefdb.survey.coordinates.latitude"),
            n("reefdb.survey.coordinates.latitude"),
            Double.class);

    /** Constant <code>LONGITUDE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> LONGITUDE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_LONGITUDE,
            n("reefdb.survey.coordinates.longitude"),
            n("reefdb.survey.coordinates.longitude"),
            Double.class);

    /** Constant <code>POSITIONING</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> POSITIONING = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_POSITIONING,
            n("reefdb.property.positionning.name"),
            n("reefdb.property.positionning.name"),
            PositioningSystemDTO.class);

    /** Constant <code>POSITIONING_PRECISION</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> POSITIONING_PRECISION = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_POSITIONING_PRECISION,
            n("reefdb.property.positionning.precision"),
            n("reefdb.property.positionning.precision"),
            String.class);

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_NAME,
            n("reefdb.property.mnemonic"),
            n("reefdb.property.mnemonic"),
            String.class);

    /** Constant <code>DEPARTMENT</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> DEPARTMENT = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_RECORDER_DEPARTMENT,
            n("reefdb.property.department.recorder"),
            n("reefdb.property.department.recorder"),
            DepartmentDTO.class);

    /** Constant <code>TIME</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> TIME = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_TIME,
            n("reefdb.property.survey.time"),
            n("reefdb.property.survey.time"),
            Integer.class);

    /** Constant <code>UPDATE_DATE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_UPDATE_DATE,
            n("reefdb.property.date.modification"),
            n("reefdb.property.date.modification"),
            Date.class);

    /** Constant <code>CONTROL_DATE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> CONTROL_DATE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_CONTROL_DATE,
            n("reefdb.property.date.control"),
            n("reefdb.property.date.control"),
            Date.class);

    /** Constant <code>VALIDATION_DATE</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> VALIDATION_DATE = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_VALIDATION_DATE,
            n("reefdb.property.date.validation"),
            n("reefdb.property.date.validation"),
            Date.class);

    /** Constant <code>VALIDATION_COMMENT</code> */
    public static final ReefDbColumnIdentifier<SurveysTableRowModel> VALIDATION_COMMENT = ReefDbColumnIdentifier.newId(
            SurveysTableRowModel.PROPERTY_VALIDATION_COMMENT,
            n("reefdb.property.comment.validation"),
            n("reefdb.property.comment.validation"),
            String.class);

    /**
     * <p>Constructor for SurveysTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    public SurveysTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public SurveysTableRowModel createNewRow() {
        return new SurveysTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<SurveysTableRowModel> getFirstColumnEditing() {
        return LOCATION;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<SurveysTableRowModel> propertyName) {

        // validation comments are always editable
        if (VALIDATION_COMMENT == propertyName) return true;

        boolean editable = true;
        // Positioning is allowed when coordinate is set
        if (POSITIONING == propertyName) {
            SurveysTableRowModel rowModel = getEntry(rowIndex);
            editable = rowModel.getLatitude() != null || rowModel.getLongitude() != null;
        }

        return editable && super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

    @Override
    public SurveysTableUIModel getTableUIModel() {
        return (SurveysTableUIModel) super.getTableUIModel();
    }

    @Override
    public String getStateContext() {

        if (getTableUIModel().getMainUIModel() != null) {
            // Use program code in filter to set state context (Mantis #58778)
            return getTableUIModel().getMainUIModel().getStateContext();
        }

        return super.getStateContext();
    }
}
