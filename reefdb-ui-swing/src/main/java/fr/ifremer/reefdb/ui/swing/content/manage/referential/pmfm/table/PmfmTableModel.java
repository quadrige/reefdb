package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.*;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class PmfmTableModel extends AbstractReefDbTableModel<PmfmTableRowModel> {

    public static final ReefDbColumnIdentifier<PmfmTableRowModel> PMFM_ID = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmTableRowModel.PROPERTY_ID,
            n("reefdb.property.pmfm.id"),
            n("reefdb.property.pmfm.id"),
            Integer.class);

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> NAME = ReefDbColumnIdentifier.newPmfmNameId(
            n("reefdb.property.name"),
            n("reefdb.property.name"));

    /** Constant <code>PARAMETER</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> PARAMETER = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_PARAMETER,
            n("reefdb.property.pmfm.parameter"),
            n("reefdb.property.pmfm.parameter"),
            ParameterDTO.class,
            true);

    /** Constant <code>SUPPORT</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> SUPPORT = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_MATRIX,
            n("reefdb.property.pmfm.matrix"),
            n("reefdb.property.pmfm.matrix"),
            MatrixDTO.class,
            true);

    /** Constant <code>FRACTION</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> FRACTION = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_FRACTION,
            n("reefdb.property.pmfm.fraction"),
            n("reefdb.property.pmfm.fraction"),
            FractionDTO.class,
            true);

    /** Constant <code>METHOD</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> METHOD = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_METHOD,
            n("reefdb.property.pmfm.method"),
            n("reefdb.property.pmfm.method"),
            MethodDTO.class,
            true);

    /** Constant <code>UNIT</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> UNIT = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_UNIT,
            n("reefdb.property.pmfm.unit"),
            n("reefdb.property.pmfm.unit"),
            UnitDTO.class,
            true);

    // qualitative values
    /** Constant <code>QUALITATIVE_VALUES</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> QUALITATIVE_VALUES = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_QUALITATIVE_VALUES,
            n("reefdb.property.pmfm.parameter.associatedQualitativeValue"),
            n("reefdb.property.pmfm.parameter.associatedQualitativeValue"),
            QualitativeValueDTO.class,
            DecoratorService.COLLECTION_SIZE);

    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<PmfmTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            PmfmTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class,
            true);

    // TODO : threshold, maxDecimal and SignificativeNb are not used in this table... remove ?

//	public static final ReefDbColumnIdentifier<PmfmTableRowModel> THRESHOLD = ReefDbColumnIdentifier.newId(
//			PmfmTableRowModel.PROPERTY_THRESHOLD,
//			n("reefdb.property.pmfm.threshold"),
//			n("reefdb.property.pmfm.threshold"),
//			Double.class);
//	public static final ReefDbColumnIdentifier<PmfmTableRowModel> MAX_DECIMAL_NUMBER = ReefDbColumnIdentifier.newId(
//			PmfmTableRowModel.PROPERTY_MAX_DECIMAL_NB,
//			n("reefdb.property.pmfm.maxDecimalNumber"),
//			n("reefdb.property.pmfm.maxDecimalNumber"),
//			Integer.class);
//	public static final ReefDbColumnIdentifier<PmfmTableRowModel> SIGNIFICANT_DIGITS_NUMBER = ReefDbColumnIdentifier.newId(
//			PmfmTableRowModel.PROPERTY_SIGNIFICATIVE_FIGURES_NB,
//			n("reefdb.property.pmfm.significantDigitsNumber"),
//			n("reefdb.property.pmfm.significantDigitsNumber"),
//			Integer.class);


    public static final ReefDbColumnIdentifier<PmfmTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        PmfmTableRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<PmfmTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        PmfmTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<PmfmTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        PmfmTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);


    /**
     * <p>Constructor for PmfmTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    public PmfmTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmTableRowModel createNewRow() {
        return new PmfmTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<PmfmTableRowModel> getFirstColumnEditing() {
        return PARAMETER;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<PmfmTableRowModel> propertyName) {
        if (propertyName.equals(QUALITATIVE_VALUES)) {
            PmfmTableRowModel row = getEntry(rowIndex);
            return super.isCellEditable(rowIndex, columnIndex, propertyName) && row.getParameter() != null && row.getParameter().isQualitative();
        } else {
            return super.isCellEditable(rowIndex, columnIndex, propertyName);
        }
    }

}
