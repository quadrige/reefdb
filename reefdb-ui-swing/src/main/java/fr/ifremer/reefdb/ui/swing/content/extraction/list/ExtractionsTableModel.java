package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.GroupingTypeDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * extractions table model
 */
public class ExtractionsTableModel extends AbstractReefDbTableModel<ExtractionsRowModel> {

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<ExtractionsRowModel> NAME = ReefDbColumnIdentifier.newId(
            ExtractionsRowModel.PROPERTY_NAME,
            n("reefdb.extraction.list.name.short"),
            n("reefdb.extraction.list.name.tip"),
            String.class,
            true);

    /** Constant <code>GROUPING_TYPE</code> */
    public static final ReefDbColumnIdentifier<ExtractionsRowModel> GROUPING_TYPE = ReefDbColumnIdentifier.newId(
            ExtractionsRowModel.PROPERTY_GROUPING_TYPE,
            n("reefdb.extraction.list.groupingType.short"),
            n("reefdb.extraction.list.groupingType.tip"),
            GroupingTypeDTO.class,
            true);

    /**
     * <p>Constructor for ExtractionsTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    public ExtractionsTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionsRowModel createNewRow() {
        return new ExtractionsRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<ExtractionsRowModel> getFirstColumnEditing() {
        return NAME;
    }

}
