package fr.ifremer.reefdb.ui.swing.content.manage.campaign.menu;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.SearchDateValues;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.CampaignsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;

import javax.swing.JComponent;
import java.util.List;

/**
 * Controlleur du menu de l'onglet prelevements mesures.
 */
public class CampaignsMenuUIHandler extends ReferentialMenuUIHandler<CampaignsMenuUIModel, CampaignsMenuUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final CampaignsMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final CampaignsMenuUIModel model = new CampaignsMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final CampaignsMenuUI ui) {
        super.afterInit(ui);

        // Init the combobox
        initComboBox();

        initListeners();
    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getNameEditor().setEnabled(enabled);
        getUI().getStartDateCombo().setEnabled(enabled);
        getUI().getEndDateCombo().setEnabled(enabled);
        getUI().getStartDate1Editor().setEnabled(getModel().getSearchStartDate() != null);
        getUI().getStartDate2Editor().setEnabled(getModel().getSearchStartDate() != null);
        getUI().getEndDate1Editor().setEnabled(getModel().getSearchEndDate() != null);
        getUI().getEndDate2Editor().setEnabled(getModel().getSearchEndDate() != null);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllCampaignFilter();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    @Override
    public JComponent getLocalFilterPanel() {
        return null;
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Init date search criteria
        initBeanFilterableComboBox(
                getUI().getStartDateCombo(),
                getContext().getSystemService().getSearchDates(),
                null);
        initBeanFilterableComboBox(
                getUI().getEndDateCombo(),
                getContext().getSystemService().getSearchDates(),
                null);

        // disable date editors
        getUI().getStartDate1Editor().setEnabled(false);
        getUI().getStartDate2Editor().setEnabled(false);
        getUI().getStartDate2Editor().setVisible(false);
        getUI().getAndLabel1().setVisible(false);
        getUI().getEndDate1Editor().setEnabled(false);
        getUI().getEndDate2Editor().setEnabled(false);
        getUI().getEndDate2Editor().setVisible(false);
        getUI().getAndLabel2().setVisible(false);


        // Minimal size for components
        ReefDbUIs.forceComponentSize(getUI().getNameEditor());
        ReefDbUIs.forceComponentSize(getUI().getStartDateCombo(), 82);
        ReefDbUIs.forceComponentSize(getUI().getEndDateCombo(), 82);
        ReefDbUIs.forceComponentSize(getUI().getStartDate1Editor(), 120);
        ReefDbUIs.forceComponentSize(getUI().getStartDate2Editor(), 120);
        ReefDbUIs.forceComponentSize(getUI().getEndDate1Editor(), 120);
        ReefDbUIs.forceComponentSize(getUI().getEndDate2Editor(), 120);

    }


    private void initListeners() {

        getModel().addPropertyChangeListener(CampaignsMenuUIModel.PROPERTY_SEARCH_START_DATE, evt -> {

            if (getModel().getSearchStartDate() != null) {

                // enabled date editors
                getUI().getStartDate1Editor().setEnabled(true);
                getUI().getStartDate2Editor().setEnabled(true);

                SearchDateValues searchDateValue = SearchDateValues.values()[getModel().getSearchStartDate().getId()];
                if (searchDateValue == SearchDateValues.BETWEEN) {

                    // show 'and' part
                    getUI().getAndLabel1().setVisible(true);
                    getUI().getStartDate2Editor().setVisible(true);

                } else {

                    // hide 'and' part
                    getUI().getAndLabel1().setVisible(false);
                    getUI().getStartDate2Editor().setVisible(false);
                    getUI().getStartDate2Editor().setDate(null);
                }

            } else {

                // disable date editors
                getUI().getStartDate1Editor().setDate(null);
                getUI().getStartDate1Editor().setEnabled(false);
                getUI().getStartDate2Editor().setDate(null);
                getUI().getStartDate2Editor().setEnabled(false);
                getUI().getStartDate2Editor().setVisible(false);
                getUI().getAndLabel1().setVisible(false);
            }
        });

        getModel().addPropertyChangeListener(CampaignsMenuUIModel.PROPERTY_SEARCH_END_DATE, evt -> {

            if (getModel().getSearchEndDate() != null) {

                // enabled date editors
                getUI().getEndDate1Editor().setEnabled(true);
                getUI().getEndDate2Editor().setEnabled(true);

                SearchDateValues searchDateValue = SearchDateValues.values()[getModel().getSearchEndDate().getId()];
                if (searchDateValue == SearchDateValues.BETWEEN) {

                    // show 'and' part
                    getUI().getAndLabel2().setVisible(true);
                    getUI().getEndDate2Editor().setVisible(true);

                } else {

                    // hide 'and' part
                    getUI().getAndLabel2().setVisible(false);
                    getUI().getEndDate2Editor().setVisible(false);
                    getUI().getEndDate2Editor().setDate(null);
                }

            } else {

                // disable date editors
                getUI().getEndDate1Editor().setDate(null);
                getUI().getEndDate1Editor().setEnabled(false);
                getUI().getEndDate2Editor().setDate(null);
                getUI().getEndDate2Editor().setEnabled(false);
                getUI().getEndDate2Editor().setVisible(false);
                getUI().getAndLabel2().setVisible(false);
            }
        });

    }

    public CampaignsUI getCampaignsUI() {
        return getUI().getParentContainer(CampaignsUI.class);
    }
}
