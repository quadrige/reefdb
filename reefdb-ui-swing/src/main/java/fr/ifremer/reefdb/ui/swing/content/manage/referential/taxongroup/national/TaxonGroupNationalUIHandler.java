package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.menu.TaxonGroupMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.table.TaxonGroupTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.table.TaxonGroupTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedTaxonCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedTaxonCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des groupeTaxons au niveau national
 */
public class TaxonGroupNationalUIHandler extends
    AbstractReefDbTableUIHandler<TaxonGroupTableRowModel, TaxonGroupNationalUIModel, TaxonGroupNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(TaxonGroupNationalUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(TaxonGroupNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        TaxonGroupNationalUIModel model = new TaxonGroupNationalUIModel();
        ui.setContextValue(model);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(TaxonGroupNationalUI ui) {
        initUI(ui);

        // force national
        ui.getTaxonGroupNationalMenuUI().getHandler().forceLocal(false);

        // listen to search results
        ui.getTaxonGroupNationalMenuUI().getModel().addPropertyChangeListener(TaxonGroupMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<TaxonGroupDTO>) evt.getNewValue()));

        // listen to 'apply filter' results
        ui.getTaxonGroupNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            // load only national referential (Mantis #29668)
            getModel().setBeans(ReefDbBeans.filterNationalReferential((List<TaxonGroupDTO>) evt.getNewValue()));
        });

        initTable();

    }

    private void initTable() {

        // parent
        TableColumnExt parentCol = addFilterableComboDataColumnToModel(TaxonGroupTableModel.PARENT, getContext().getReferentialService().getTaxonGroups(), true);
        parentCol.setSortable(true);
        parentCol.setEditable(false);

        // label
        TableColumnExt labelCol = addColumn(TaxonGroupTableModel.LABEL);
        labelCol.setSortable(true);
        labelCol.setEditable(false);

        // name
        TableColumnExt nameCol = addColumn(TaxonGroupTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(TaxonGroupTableModel.STATUS, getContext().getReferentialService().getStatus(StatusFilter.ALL), false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);

        // type
        TableColumnExt typeCol = addColumn(TaxonGroupTableModel.TYPE);
        typeCol.setSortable(true);
        typeCol.setEditable(false);

        // comment
        TableColumnExt commentCol = addCommentColumn(TaxonGroupTableModel.COMMENT, false);
        commentCol.setSortable(false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(TaxonGroupTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(TaxonGroupTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // taxons
        TableColumnExt taxonsCol = addColumn(new AssociatedTaxonCellEditor(getTable(), getUI(), false), new AssociatedTaxonCellRenderer(), TaxonGroupTableModel.TAXONS);
        taxonsCol.setSortable(true);

        TaxonGroupTableModel tableModel = new TaxonGroupTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.taxonGroups.national"), TaxonGroupTableModel.TAXONS);

        // Initialisation du tableau
        initTable(getTable(), true);

        // optionnal columns are hidden
        typeCol.setVisible(false);
        commentCol.setVisible(false);
        taxonsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(5);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<TaxonGroupTableRowModel> getTableModel() {
        return (TaxonGroupTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getTaxonGroupNationalTable();
    }

}
