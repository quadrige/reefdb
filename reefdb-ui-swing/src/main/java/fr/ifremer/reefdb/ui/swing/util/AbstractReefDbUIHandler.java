package fr.ifremer.reefdb.ui.swing.util;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.beans.AbstractBean;

import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import java.io.Serializable;

import static org.nuiton.i18n.I18n.t;

/**
 * Contract of any UI handler.
 *
 * @param <M>
 * @param <UI>
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 * @since 1.0
 */
public abstract class AbstractReefDbUIHandler<M extends AbstractBean, UI extends ReefDbUI<M, ?>> extends AbstractUIHandler<M, UI> {

    /** {@inheritDoc} */
    @Override
    public ReefDbUIContext getContext() {
        return (ReefDbUIContext) super.getContext();
    }

    /**
     * get the screen title
     *
     * @return the screen title
     */
    public String getTitle() {
        return t("reefdb.screen." + getUI().getClass().getSimpleName() + ".title");
    }

    /**
     * <p>getConfig.</p>
     *
     * @return a {@link fr.ifremer.reefdb.config.ReefDbConfiguration} object.
     */
    public ReefDbConfiguration getConfig() {
        return getContext().getConfiguration();
    }

    // ------------------------------------------------------------------------//
    // -- Init methods --//
    // ------------------------------------------------------------------------//
    /** {@inheritDoc} */
    @Override
    protected void initUIComponent(Object component) {
        super.initUIComponent(component);

        if (component instanceof JPanel) {
            initPanel((JPanel) component);
        }
    }

    private void initPanel(JPanel panel) {

        // set background color
        String panelType = (String) panel.getClientProperty("panelType");
        if (StringUtils.isNotBlank(panelType)) {

            Color backgroundColor = null;
            switch (panelType) {
                case ReefDbUI.CONTEXT_PANEL_TYPE:
                    backgroundColor = getConfig().getColorContextPanelBackground();
                    break;
                case ReefDbUI.SELECTION_PANEL_TYPE:
                    backgroundColor = getConfig().getColorSelectionPanelBackground();
                    break;
                case ReefDbUI.EDITION_PANEL_TYPE:
                    backgroundColor = getConfig().getColorEditionPanelBackground();
                    break;
            }

            if (backgroundColor != null) {
                ReefDbUIs.setComponentTreeBackground(panel, backgroundColor);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void initTextField(JTextField jTextField) {
        super.initTextField(jTextField);

        Boolean computed = (Boolean) jTextField.getClientProperty("computed");
        if (computed != null && computed) {
            Font font = jTextField.getFont().deriveFont(Font.ITALIC);
            jTextField.setFont(font);
            jTextField.setEditable(false);
            jTextField.setEnabled(false);
            jTextField.setDisabledTextColor(getConfig().getColorComputedWeights());
        }

    }

    @Override
    public String decorate(Serializable object, String context) {
        String result = "";
        if (object != null) {
            Decorator decorator = getDecorator(object.getClass(), context);
            if (decorator != null)
                // decorate with this decorator
                result = getDecorator(object.getClass(), context).toString(object);
            else if (object instanceof QuadrigeBean)
                // try decorate a DaliBean
                result = ReefDbBeans.toString((QuadrigeBean) object);
        }
        return result;
    }
}
