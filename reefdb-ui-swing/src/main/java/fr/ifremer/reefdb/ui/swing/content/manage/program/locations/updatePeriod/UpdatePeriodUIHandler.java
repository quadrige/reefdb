package fr.ifremer.reefdb.ui.swing.content.manage.program.locations.updatePeriod;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JComponent;
import java.time.LocalDate;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour l'écran principal.
 */
public class UpdatePeriodUIHandler extends AbstractReefDbUIHandler<UpdatePeriodUIModel, UpdatePeriodUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(UpdatePeriodUI ui) {
        super.beforeInit(ui);

        UpdatePeriodUIModel model = new UpdatePeriodUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(UpdatePeriodUI ui) {
        initUI(ui);

        // Chargement de la combo
        initBeanFilterableComboBox(
                getUI().getDepartmentCombo(),
                getContext().getReferentialService().getDepartments(),
                null);

        getUI().getDepartmentCombo().setShowReset(true);
        getUI().getDepartmentCombo().setShowDecorator(false);

    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // Recuperation des saisies
        final LocalDate startDate = getUI().getStartDateEditor().getLocalDate();
        final LocalDate endDate = getUI().getEndDateEditor().getLocalDate();
        final DepartmentDTO preleveur = (DepartmentDTO) getUI().getDepartmentCombo().getSelectedItem();

        if (startDate == null ^ endDate == null) {

            // La date de debut doit être renseignee
            getContext().getDialogHelper().showErrorDialog(getUI(),
                    t("reefdb.program.location.periods.error.date.message"),
                    t("reefdb.program.location.periods.error.titre"));

        } else if (startDate != null && endDate != null && endDate.isBefore(startDate)) {

            // la date de fin ne pas etre antérieur au début
            getContext().getDialogHelper().showErrorDialog(getUI(),
                    t("reefdb.program.location.periods.error.date.before.message"),
                    t("reefdb.program.location.periods.error.titre"));

        } else {

            // Ajout des elements dans les lieux selectionnes
            for (final AppliedStrategyDTO lieu : getModel().getTableModel().getSelectedRows()) {
                lieu.setStartDate(startDate);
                lieu.setEndDate(endDate);
                lieu.setDepartment(preleveur);
            }

            // Quitter la dialogue
            closeDialog();
        }
    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getStartDateEditor();
    }

    /**
     * <p>forceIsLocal.</p>
     *
     * @param local a {@link java.lang.Boolean} object.
     */
    public void forceIsLocal(Boolean local) {
        StatusFilter statusFilter = StatusFilter.toLocalOrNational(local);
        getUI().getDepartmentCombo().setData(getContext().getReferentialService().getDepartments(statusFilter));
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
