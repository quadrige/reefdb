package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.ungrouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

import java.util.List;

/**
 * Modele pour le tableau du haut (Psfm) pour l onglet des mesures des prelevements.
 */
public class OperationMeasurementsUngroupedTableUIModel extends AbstractReefDbTableUIModel<SamplingOperationDTO, OperationMeasurementsUngroupedRowModel, OperationMeasurementsUngroupedTableUIModel> {

    public static final String PROPERTY_SURVEY = "survey";
    public static final String PROPERTY_SAMPLING_FILTER = "samplingFilter";
    public static final String EVENT_MEASUREMENTS_LOADED = "measurementsLoaded";

    private ObservationUIModel survey;
    private SamplingOperationDTO samplingFilter;
    private Integer calculatedLengthStartPositionPmfmId;

    /**
     * Constructor.
     */
    public OperationMeasurementsUngroupedTableUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>survey</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public ObservationUIModel getSurvey() {
        return survey;
    }

    /**
     * <p>Setter for the field <code>survey</code>.</p>
     *
     * @param survey a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public void setSurvey(ObservationUIModel survey) {
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, null, survey);
    }

    /**
     * <p>getSamplingOperations.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SamplingOperationDTO> getSamplingOperations() {
        return survey == null ? null : (List<SamplingOperationDTO>) survey.getSamplingOperations();
    }

    /**
     * <p>Getter for the field <code>samplingFilter</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    public SamplingOperationDTO getSamplingFilter() {
        return samplingFilter;
    }

    /**
     * <p>Setter for the field <code>samplingFilter</code>.</p>
     *
     * @param samplingFilter a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    public void setSamplingFilter(SamplingOperationDTO samplingFilter) {
        Object oldFilter = getSamplingFilter();
        this.samplingFilter = samplingFilter;
        firePropertyChange(PROPERTY_SAMPLING_FILTER, oldFilter, samplingFilter);
    }

    /**
     * <p>getAllMeasurements.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<MeasurementDTO> getAllMeasurements() {
        List<MeasurementDTO> measurements = Lists.newArrayList();
        if (getSamplingOperations() != null) {
            for (SamplingOperationDTO samplingOperation : getSamplingOperations()) {
                measurements.addAll(samplingOperation.getMeasurements());
            }
        }
        return measurements;
    }

    public Integer getCalculatedLengthStartPositionPmfmId() {
        return calculatedLengthStartPositionPmfmId;
    }

    public void setCalculatedLengthStartPositionPmfmId(Integer calculatedLengthStartPositionPmfmId) {
        this.calculatedLengthStartPositionPmfmId = calculatedLengthStartPositionPmfmId;
    }

    public void fireMeasurementsLoaded() {
        firePropertyChange(EVENT_MEASUREMENTS_LOADED, null, null);
    }
}
