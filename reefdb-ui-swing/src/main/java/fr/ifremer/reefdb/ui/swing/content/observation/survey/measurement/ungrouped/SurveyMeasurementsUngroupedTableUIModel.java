package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.ungrouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Modele pour le tableau du haut (Psfm) pour l onglet des mesures de l'observation.
 */
public class SurveyMeasurementsUngroupedTableUIModel extends AbstractReefDbTableUIModel<SurveyDTO, SurveyMeasurementsUngroupedRowModel, SurveyMeasurementsUngroupedTableUIModel> {

    public static final String PROPERTY_SURVEY = "survey";
    public static final String EVENT_MEASUREMENTS_LOADED = "measurementsLoaded";

    private ObservationUIModel survey;

    /**
     * Constructor.
     */
    public SurveyMeasurementsUngroupedTableUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>survey</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public ObservationUIModel getSurvey() {
        return survey;
    }

    /**
     * <p>Setter for the field <code>survey</code>.</p>
     *
     * @param survey a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public void setSurvey(ObservationUIModel survey) {
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, null, survey);
    }

    public void fireMeasurementsLoaded() {
        firePropertyChange(EVENT_MEASUREMENTS_LOADED, null, null);
    }
}
