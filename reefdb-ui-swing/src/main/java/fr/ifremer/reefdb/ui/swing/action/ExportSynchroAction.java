package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.common.synchro.service.SynchroResult;
import fr.ifremer.quadrige3.core.dao.technical.ZipUtils;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.core.service.http.HttpNotFoundException;
import fr.ifremer.quadrige3.synchro.meta.data.DataSynchroTables;
import fr.ifremer.quadrige3.synchro.service.client.SynchroClientService;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRejectedRowResolver;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRestClientService;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroClientExportResult;
import fr.ifremer.quadrige3.synchro.service.data.DataSynchroContext;
import fr.ifremer.quadrige3.synchro.vo.SynchroProgressionStatus;
import fr.ifremer.quadrige3.ui.swing.ApplicationUI;
import fr.ifremer.quadrige3.ui.swing.action.AbstractReloadCurrentScreenAction;
import fr.ifremer.quadrige3.ui.swing.model.ProgressionUIModel;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroDirection;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIContext;
import fr.ifremer.quadrige3.ui.swing.synchro.SynchroUIHandler;
import fr.ifremer.quadrige3.ui.swing.synchro.action.*;
import fr.ifremer.quadrige3.ui.swing.synchro.resolver.SynchroRejectedRowUIResolver;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;
import fr.ifremer.reefdb.ui.swing.content.synchro.program.ProgramSelectUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;

import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.io.File;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ExportSynchroAction class.</p>
 *
 * @author ludovic.pecquot@e-is.pro
 */
public class ExportSynchroAction extends AbstractReloadCurrentScreenAction {

    private static final Log log = LogFactory.getLog(ExportSynchroAction.class);

    private Set<String> programCodes;
    private int userId;
    private File dbDirToExport;
    private boolean serverJobRunning = false;
    private boolean serverFailed = false;
    private boolean hasData = false;

    /**
     * <p>Constructor for ExportSynchroAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    public ExportSynchroAction(ReefDbMainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("reefdb.action.synchro.export.title"));
    }

    @Override
    public ReefDbUIContext getContext() {
        return (ReefDbUIContext) super.getContext();
    }

    private SynchroUIContext getSynchroUIContext() {
        return getContext().getSynchroContext();
    }

    private SynchroUIHandler getSynchroHandler() {
        return getContext().getSynchroHandler();
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        super.prepareAction();

        if (!getContext().isSynchroEnabled() && getConfig().isSynchronizationUsingServer()) {
            getContext().getErrorHelper().showWarningDialog(t("reefdb.synchro.unavailable"));
            return false;
        }

        // Check if user has access right (not local user, or has some programs)
        if (getContext().isAuthenticatedAsLocalUser()
                || !getContext().getProgramStrategyService().hasRemoteWriteOnProgram(getContext().getAuthenticationInfo())) {
            getContext().getErrorHelper().showWarningDialog(t("reefdb.synchro.export.accessDenied"));
            return false;
        }

        // load actual export context
        getSynchroUIContext().setDirection(SynchroDirection.EXPORT);
        getSynchroUIContext().loadExportContext();
        userId = SecurityContextHelper.getQuadrigeUserId();

        // Ask user to select program to import
        programCodes = getSynchroUIContext().getExportDataProgramCodes();

        // Hide photo option (Mantis #48536)
        ProgramSelectUI programSelectUI = new ProgramSelectUI((ApplicationUI) getUI(), userId, StatusFilter.NATIONAL_ACTIVE, programCodes, false, false);

        // Force photo option to true (Mantis #48536)
        programSelectUI.getModel().setEnablePhoto(true);

        handler.openDialog(programSelectUI, t("reefdb.action.synchro.export.dataProgramCodes.title"), new Dimension(800, 400));

        List<ProgramDTO> programs = programSelectUI.getModel().getSelectedPrograms();

        // If no programs selected (or user cancelled): exit
        if (CollectionUtils.isEmpty(programs)) {
            return false;
        }

        // Get selected programs as code list
        programCodes = new LinkedHashSet<>(ReefDbBeans.collectProperties(programs, ProgramDTO.PROPERTY_CODE));

        // get photo option and set it in context
        getSynchroUIContext().setExportPhoto(programSelectUI.getModel().isEnablePhoto());

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doActionBeforeReload() throws Exception {
        dbDirToExport = null;
        serverJobRunning = false;
        serverFailed = false;
        hasData = false;

        // DO restore serverJobStarted
        // serverJobStarted = ...

        SynchroClientService synchroService = ReefDbServiceLocator.instance().getSynchroClientService();

        getSynchroUIContext().setExportDataProgramCodes(programCodes);
        getSynchroUIContext().saveExportContext();

        createProgressionUIModel(100);
        getSynchroUIContext().setStatus(SynchroProgressionStatus.RUNNING);
        getSynchroUIContext().saveExportContext();

        // build temp database and export local to temp
        getProgressionUIModel().setMessage(t("quadrige3.synchro.progress.export"));

        if (!getConfig().isSynchronizationUsingServer()) {

            SynchroRejectedRowResolver rejectResolver = new SynchroRejectedRowUIResolver(getContext().getDialogHelper(), false /*export*/);
            // do it in one go with direct synchronization
            SynchroClientExportResult exportResult = synchroService.exportToServerDatabase(userId,
                    programCodes,
                    rejectResolver,
                    getProgressionUIModel(),
                    100);
            hasData = exportResult.getDataResult().getTotalTreated() > 0;
            if (!hasData) {
                showNoDataMessage();
            }

        } else {
            // Do a referential import if need (see mantis #25665)
            doImportReferential();

            // After potential user privilege updates, check write access (Mantis #39506)
            if (!checkWriteAccess()) return;

            // export directory is set by the synchronization service
            SynchroClientExportResult exportResult = synchroService.exportDataToTempDb(userId,
                    programCodes,
                    getSynchroUIContext().isExportPhoto(),
                    getProgressionUIModel(),
                    100);
            DataSynchroContext context = exportResult.getDataContext();

            // If no data to export, stop here
            hasData = context.getResult().getTotalTreated() > 0;
            if (!hasData) {
                showNoDataMessage();
                return;
            }

            // check photo volume
            int nbPhoto = context.getResult().getNbRows(DataSynchroTables.PHOTO.name());
            int threshold = getConfig().getSynchroPhotoMaxNumberThreshold();
            if (threshold > 0 && nbPhoto > threshold) {
                if (getContext().getDialogHelper().showConfirmDialog(
                        t("quadrige3.synchro.export.photo.overThreshold.message", nbPhoto),
                        t("reefdb.action.synchro.export.title"),
                        JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)
                    return;
            }

            // compress the database
            dbDirToExport = exportResult.getTempDbExportDirectory();
            getProgressionUIModel().setMessage(t("quadrige3.synchro.progress.compress"));
            String filename = dbDirToExport.getName() + ".zip";
            File zipFile = new File(dbDirToExport.getParent(), filename);
            ZipUtils.compressFilesInPath(dbDirToExport.toPath(), zipFile.toPath(), getProgressionUIModel(), false, true);

            getSynchroUIContext().setExportFileName(zipFile.getName());
            getSynchroUIContext().saveExportContext();

            // delegate progression model from SynchroUIContext
            getProgressionUIModel().setTotal(100);
            setProgressionUIModel(getSynchroUIContext().getProgressionModel());

            // send it to synchro server
            ExportSynchroUploadAction uploadAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(),
                    ExportSynchroUploadAction.class);
            uploadAction.executeAndWait();

            boolean needDownloadResultAndFinish = false;
            int retryCounter = 0;
            while (true) {
                try {
                    // Start export job on server
                    if (!serverJobRunning) {
                        getProgressionUIModel().clear();
                        ExportSynchroStartAction startAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(),
                                ExportSynchroStartAction.class);
                        startAction.executeAndWait();
                        serverJobRunning = true;

                        Thread.sleep(getConfig().getSynchronizationRefreshTimeout()); // wait
                    }

                    // Get job status  on server (if job still alive)
                    if (serverJobRunning) {
                        if (getSynchroUIContext().isRunningStatus()) {
                            try {
                                ExportSynchroGetStatusAction getStatusAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(),
                                        ExportSynchroGetStatusAction.class);
                                getStatusAction.executeAndWait();
                                needDownloadResultAndFinish = true;
                            } catch (HttpNotFoundException e) {
                                // Job may has finished (we don't really known): continue as a successful status
                                getSynchroUIContext().setStatus(SynchroProgressionStatus.SUCCESS);
                                needDownloadResultAndFinish = true;
                            }

                            // if SynchroException, do not try to reconnect - fix mantis #39388
                            catch (SynchroException se) {
                                serverJobRunning = false;
                                serverFailed = true;
                                throw se; // TODO Dont throw here because the result file has to be downloaded from server
                                // needDownloadResultAndFinish = true;
                            }
                        } else {
                            needDownloadResultAndFinish = true;
                        }
                    }

                    // Finish export
                    if (needDownloadResultAndFinish) {
                        try {
                            finishExportThenCleanFiles(exportResult, getProgressionUIModel());
                            break; // stop loop
                        }

                        // Server delete the result file = failed
                        catch (HttpNotFoundException e) {
                            getSynchroUIContext().setStatus(SynchroProgressionStatus.FAILED);
                            serverFailed = true;
                            throw new SynchroException(t("reefdb.action.synchro.export.failed.serverJobStarted.log", e.getMessage()), e); // stop
                        }
                    }
                } catch(QuadrigeTechnicalException e) {
                    retryCounter++;
                    log.debug(String.format("Error during export: %s", e.getMessage()), e);
                    getProgressionUIModel().setMessage(t("reefdb.action.synchro.export.retry.progression", retryCounter, getConfig().getSynchronizationMaxRetryCount()));

                    // Retry many times, or ask user to retry (mantis #35441)
                    if (retryCounter < getConfig().getSynchronizationMaxRetryCount()) {
                        Thread.sleep(getConfig().getSynchronizationRetryTimeout()); // Wait then loop (= retry)
                    } else {
                        int result = getContext().getDialogHelper().showOptionDialog(null,
                                serverJobRunning
                                        ? t("reefdb.action.synchro.export.retry.ask.serverJobStarted",
                                        getConfig().getAdminEmail())
                                        : t("reefdb.action.synchro.export.retry.ask"),
                                t("reefdb.action.synchro.export.retry.title"), JOptionPane.ERROR_MESSAGE, JOptionPane.YES_NO_OPTION);

                        // No = stop
                        if (result == JOptionPane.NO_OPTION) throw e;

                        // Or try again
                        retryCounter = 0; // reset the counter
                        needDownloadResultAndFinish = true; // loop (= retry)
                    }
                }
            }

        }

        // Skip screen reloading if No updates
        setSkipScreenReload(!hasData);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        // If server failed (but no technical error during action)
        if (serverFailed) {
            postFailedAction(null);
            return;
        }

        super.postSuccessAction();
        if (log.isInfoEnabled()) {
            log.info("Synchronization export success");
        }

        getSynchroUIContext().resetExportContext();
        getSynchroUIContext().saveExportContext();

        // do not display if no data found
        if (!hasData) {
            getContext().getSynchroHandler().report(t("reefdb.action.synchro.export.noData"), false);
        } else {
            getContext().getSynchroHandler().report(t("reefdb.action.synchro.export.success"));
        }

    }

    /** {@inheritDoc} */
    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);

        if (error != null) {
            log.error(t("reefdb.action.synchro.export.failed.log", error.getMessage()));
        } else if (getSynchroUIContext().getProgressionModel() != null
                && getSynchroUIContext().getProgressionModel().getMessage() != null) {

            String serverMessage = getSynchroUIContext().getProgressionModel().getMessage();
            log.error(t("reefdb.action.synchro.export.failed.server.log", serverMessage));
        } else {
            log.error(t("reefdb.action.synchro.export.failed"));
        }

        getSynchroUIContext().resetExportContext();
        getSynchroUIContext().saveExportContext();

        if (serverJobRunning) {
            // warn user
            getContext().getSynchroHandler().report(t("reefdb.action.synchro.export.failed.serverJobStarted",
                    getConfig().getAdminEmail()));

            // Log to file
            ReefDbServiceLocator.instance().getSynchroHistoryService().saveExportError(
                    userId,
                    getSynchroUIContext().getSynchroExportContext(),
                    t("reefdb.action.synchro.export.failed.serverJobStarted.history",
                            error != null ? error.getMessage() : ""));
        } else {
            getContext().getSynchroHandler().report(t("reefdb.action.synchro.export.failed"));
        }
    }

    /* -- Internal methods -- */

    private void finishExportThenCleanFiles(SynchroClientExportResult exportResult,
                                            ProgressionUIModel mainProgressionModel) throws Exception {

        // Save server status and message
        SynchroProgressionStatus serverStatus = getSynchroUIContext().getStatus();
        serverFailed = serverStatus != SynchroProgressionStatus.SUCCESS; // server hasn't committed exported rows
        String serverErrorMessage = serverFailed ? getSynchroUIContext().getProgressionModel().getMessage() : null;

        // download the result file
        boolean hasRejectMessages = false;
        SynchroRestClientService restService = ReefDbServiceLocator.instance().getSynchroRestClientService();
        SynchroResult serverResult = restService.downloadExportResult(
                getContext().getAuthenticationInfo(),
                getModel().getSynchroContext().getSynchroExportContext(),
                getProgressionUIModel());
        boolean resultFileExists = serverResult != null;

        // restore action progression model
        setProgressionUIModel(mainProgressionModel);

        // exploit the result file: finish export
        if (resultFileExists) {
            exportResult.setServerResult(serverResult);

            mainProgressionModel.setMessage(t("quadrige3.synchro.progress.finishExport"));

            SynchroRejectedRowResolver rejectResolver = new SynchroRejectedRowUIResolver(getContext().getDialogHelper(), false /*export*/);
            SynchroClientService synchroService = ReefDbServiceLocator.instance().getSynchroClientService();

            try {
                hasRejectMessages = synchroService.finishExportData(userId, exportResult, rejectResolver, serverFailed, false /* do not revert PKs inside this method */);
            } catch(QuadrigeTechnicalException e) {
                // Conversion to SynchroException is need, to avoid a retry
                throw new SynchroException(t("quadrige3.error.synchro.export.finish"), e);
            }

            // get Pks to revert, then apply revert
            DataSynchroContext context = exportResult.getDataContext();
            Multimap<String, String> pksToRevert = context.getResult().getSourceMissingReverts();
            revertPks(pksToRevert);
        }

        // cleanFiles files (local and server)
        cleanFiles();

        // Server failed BEFORE finish: restore server status and message
        if (serverFailed) {

            // Restore the server status and message (could have been overridden by download action)
            getSynchroUIContext().getProgressionModel().setMessage(serverErrorMessage);
            getSynchroUIContext().setStatus(serverStatus);

            if (!resultFileExists || !hasRejectMessages) {
                // If nothing was displayed yet, throw the exception (will display an error dialog)
                throw new SynchroException(t("quadrige3.error.synchro.status", serverErrorMessage));
            }
        }
    }

    private void revertPks(Multimap<String, String> pksToRevert) throws Exception {
        if (pksToRevert == null || pksToRevert.isEmpty()) {
            return;
        }
        getSynchroUIContext().resetImportContext();
        getSynchroUIContext().setImportData(true);
        getSynchroUIContext().setImportReferential(false);
        // always enable photo on this import (Mantis #48536 & #48479)
        getSynchroUIContext().setImportPhoto(true);
        getSynchroUIContext().setImportDataPkIncludes(pksToRevert);
        getSynchroUIContext().setDirection(SynchroDirection.IMPORT);
        getSynchroUIContext().setImportDataForceEditedRowOverride(true);

        // send it to synchro server
        ImportSynchroStartAction reimportAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(),
                ImportSynchroStartAction.class);

        // execute and wait for it
        reimportAction.executeAndWait();

        // download
        ImportSynchroDownloadAction action = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(),
                ImportSynchroDownloadAction.class);

        // execute and wait for it
        action.executeAndWait();

        // Import Temp DB
        ImportSynchroApplyAction applyImportAction = getContext().getActionFactory().createLogicAction(getHandler(), ImportSynchroApplyAction.class);
        applyImportAction.prepareAction();
        applyImportAction.setSilent(true);
        getContext().getActionEngine().runInternalAction(applyImportAction);
    }

    private void cleanFiles() throws Exception {

        // clean local files
        ApplicationIOUtil.forceDeleteOnExit(dbDirToExport, t("quadrige3.error.delete.directory", dbDirToExport.getAbsolutePath()));
        File fileToDelete = new File(dbDirToExport.getParent(), dbDirToExport.getName() + ".zip");
        ApplicationIOUtil.deleteFile(fileToDelete, t("quadrige3.error.delete.directory", fileToDelete.getAbsolutePath()));

        fileToDelete = new File(dbDirToExport.getParent(), dbDirToExport.getName() + ".json");
        if (fileToDelete.exists()) {
            ApplicationIOUtil.deleteFile(fileToDelete, t("quadrige3.error.delete.directory", fileToDelete.getAbsolutePath()));
        }

        // send acknowledge
        ExportSynchroAckAction ackAction = getContext().getActionFactory().createNonBlockingUIAction(getSynchroHandler(),
                ExportSynchroAckAction.class);
        ackAction.executeAndWait();
    }

    private void showNoDataMessage() {
        getContext().getDialogHelper().showMessageDialog(t("reefdb.action.synchro.export.noData"),
                t("reefdb.action.synchro.export.result.title"));
    }

    /**
     * Do a import of referential data
     */
    private void doImportReferential() {

        ImportReferentialSynchroAtOnceAction importAction = getContext().getActionFactory().createLogicAction(getHandler(), ImportReferentialSynchroAtOnceAction.class);
        getContext().getActionEngine().runInternalAction(importAction);

        // Restore previous running status (fix mantis #39390)
        getSynchroUIContext().setStatus(SynchroProgressionStatus.RUNNING);
    }

    private boolean checkWriteAccess() {

        List<ProgramDTO> programs = getContext().getProgramStrategyService().getRemoteWritableProgramsByUser(getContext().getAuthenticationInfo());
        Set<String> allowedProgramCodes = programs.stream().map(ProgramDTO::getCode).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(programs) || !allowedProgramCodes.containsAll(programCodes)) {
            getContext().getDialogHelper().showWarningDialog(
                    t("reefdb.synchro.export.accessDenied.program.topMessage"),
                    ReefDbUIs.getHtmlString(CollectionUtils.removeAll(programCodes, allowedProgramCodes)),
                    t("reefdb.synchro.export.accessDenied.program.bottomMessage"),
                    t("reefdb.action.synchro.export.title")
            );
            return false;
        }
        return true;
    }
}
