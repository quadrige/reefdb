package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class AnalysisInstrumentsTableModel extends AbstractReefDbTableModel<AnalysisInstrumentsTableRowModel> {

    /**
     * <p>Constructor for AnalysisInstrumentsTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param allowCreateNewRow a boolean.
     */
    public AnalysisInstrumentsTableModel(final TableColumnModelExt columnModel, boolean allowCreateNewRow) {
        super(columnModel, allowCreateNewRow, false);
    }

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            AnalysisInstrumentsTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class, true);

    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            AnalysisInstrumentsTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class, true);

    /** Constant <code>DESCRIPTION</code> */
    public static final ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            AnalysisInstrumentsTableRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.property.description"),
            String.class);

    public static final ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
        AnalysisInstrumentsTableRowModel.PROPERTY_COMMENT,
        n("reefdb.property.comment"),
        n("reefdb.property.comment"),
        String.class,
        false);

    public static final ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        AnalysisInstrumentsTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        AnalysisInstrumentsTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);


    /** {@inheritDoc} */
    @Override
    public AnalysisInstrumentsTableRowModel createNewRow() {
        return new AnalysisInstrumentsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<AnalysisInstrumentsTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
