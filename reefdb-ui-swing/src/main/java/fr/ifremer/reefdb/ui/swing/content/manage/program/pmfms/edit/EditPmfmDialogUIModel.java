package fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.edit;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.PmfmsTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

/**
 * Created by Ludovic on 19/10/2015.
 */
public class EditPmfmDialogUIModel extends AbstractReefDbBeanUIModel<PmfmStrategyDTO, EditPmfmDialogUIModel> implements PmfmStrategyDTO {

    private static final Binder<PmfmStrategyDTO, EditPmfmDialogUIModel> fromBeanBinder = BinderFactory.newBinder(PmfmStrategyDTO.class, EditPmfmDialogUIModel.class);

    private static final Binder<EditPmfmDialogUIModel, PmfmStrategyDTO> toBeanBinder = BinderFactory.newBinder(EditPmfmDialogUIModel.class, PmfmStrategyDTO.class);

    private PmfmsTableUIModel tableModel;
    /** Constant <code>PROPERTY_TABLE_MODEL="tableModel"</code> */
    public static final String PROPERTY_TABLE_MODEL = "tableModel";

    /**
     * <p>Constructor for EditPmfmDialogUIModel.</p>
     */
    protected EditPmfmDialogUIModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    /**
     * <p>Getter for the field <code>tableModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.PmfmsTableUIModel} object.
     */
    public PmfmsTableUIModel getTableModel() {
        return tableModel;
    }

    /**
     * <p>Setter for the field <code>tableModel</code>.</p>
     *
     * @param tableModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.PmfmsTableUIModel} object.
     */
    public void setTableModel(PmfmsTableUIModel tableModel) {
        this.tableModel = tableModel;
        firePropertyChange(PROPERTY_TABLE_MODEL, null, tableModel);
    }

    /** {@inheritDoc} */
    @Override
    protected PmfmStrategyDTO newBean() {
        return ReefDbBeanFactory.newPmfmStrategyDTO();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isSurvey() {
        return delegateObject.isSurvey();
    }

    /** {@inheritDoc} */
    @Override
    public void setSurvey(boolean survey) {
        delegateObject.setSurvey(survey);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isSampling() {
        return delegateObject.isSampling();
    }

    /** {@inheritDoc} */
    @Override
    public void setSampling(boolean sampling) {
        delegateObject.setSampling(sampling);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isGrouping() {
        return delegateObject.isGrouping();
    }

    /** {@inheritDoc} */
    @Override
    public void setGrouping(boolean grouping) {
        delegateObject.setGrouping(grouping);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUnique() {
        return delegateObject.isUnique();
    }

    /** {@inheritDoc} */
    @Override
    public void setUnique(boolean unique) {
        delegateObject.setUnique(unique);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getRankOrder() {
        return delegateObject.getRankOrder();
    }

    /** {@inheritDoc} */
    @Override
    public void setRankOrder(Integer rankOrder) {
        delegateObject.setRankOrder(rankOrder);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getAnalysisDepartment() {
        return delegateObject.getAnalysisDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setAnalysisDepartment(DepartmentDTO analysisDepartment) {
        delegateObject.setAnalysisDepartment(analysisDepartment);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmDTO getPmfm() {
        return delegateObject.getPmfm();
    }

    /** {@inheritDoc} */
    @Override
    public void setPmfm(PmfmDTO pmfm) {
        delegateObject.setPmfm(pmfm);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }


    @Override
    public QualitativeValueDTO getQualitativeValues(int index) {
        return delegateObject.getQualitativeValues(0);
    }

    @Override
    public boolean isQualitativeValuesEmpty() {
        return delegateObject.isQualitativeValuesEmpty();
    }

    @Override
    public int sizeQualitativeValues() {
        return delegateObject.sizeQualitativeValues();
    }

    @Override
    public void addQualitativeValues(QualitativeValueDTO qualitativeValues) {
        delegateObject.addQualitativeValues(qualitativeValues);
    }

    @Override
    public void addAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        delegateObject.addAllQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean removeQualitativeValues(QualitativeValueDTO qualitativeValues) {
        return delegateObject.removeQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean removeAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        return delegateObject.removeAllQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean containsQualitativeValues(QualitativeValueDTO qualitativeValues) {
        return delegateObject.containsQualitativeValues(qualitativeValues);
    }

    @Override
    public boolean containsAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        return delegateObject.containsAllQualitativeValues(qualitativeValues);
    }

    @Override
    public List<QualitativeValueDTO> getQualitativeValues() {
        return delegateObject.getQualitativeValues();
    }

    @Override
    public void setQualitativeValues(List<QualitativeValueDTO> qualitativeValues) {
        delegateObject.setQualitativeValues(qualitativeValues);
    }
}
