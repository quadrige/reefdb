package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class LieuxProgrammeTableModel extends AbstractReefDbTableModel<LieuxProgrammeTableRowModel> {

	/**
	 * Identifiant pour la colonne code = location id
	 */
    public static final ReefDbColumnIdentifier<LieuxProgrammeTableRowModel> CODE = ReefDbColumnIdentifier.newId(
    		LieuxProgrammeTableRowModel.PROPERTY_ID,
            n("reefdb.property.code"),
            n("reefdb.program.strategies.location.code.tip"),
            Integer.class);

	/**
	 * Identifiant pour la colonne label
	 */
    public static final ReefDbColumnIdentifier<LieuxProgrammeTableRowModel> LABEL = ReefDbColumnIdentifier.newId(
    		LieuxProgrammeTableRowModel.PROPERTY_LABEL,
            n("reefdb.property.label"),
            n("reefdb.program.strategies.location.label.tip"),
            String.class);
    
    /**
     * Identifiant pour la colonne comment.
     */
    public static final ReefDbColumnIdentifier<LieuxProgrammeTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		LieuxProgrammeTableRowModel.PROPERTY_NAME,
            n("reefdb.program.strategies.location.name.short"),
            n("reefdb.program.strategies.location.name.tip"),
            String.class);

	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public LieuxProgrammeTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public LieuxProgrammeTableRowModel createNewRow() {
		return new LieuxProgrammeTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<LieuxProgrammeTableRowModel> getFirstColumnEditing() {
		return CODE;
	}
}
