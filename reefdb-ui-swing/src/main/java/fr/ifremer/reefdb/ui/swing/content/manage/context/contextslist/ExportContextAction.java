package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;


/**
 * <p>ExportContextAction class.</p>
 *
 */
public class ExportContextAction extends AbstractReefDbAction<ManageContextsListTableUIModel, ManageContextsListTableUI, ManageContextsListTableUIHandler> {

    private static final String EXPORT_FILE_FORMAT = "reefdb-contexts-%s.dat";
    
    private List<ContextDTO> contextsToExport;
    private File targetDirectory;
    private File exportFile;
    
	/**
	 * <p>Constructor for ExportContextAction.</p>
	 *
	 * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIHandler} object.
	 */
	public ExportContextAction(ManageContextsListTableUIHandler handler) {
		super(handler, false);
	}
    
    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        contextsToExport = null;
        targetDirectory = null;
        exportFile = null;
        
        if (super.prepareAction()) {
            // Choose directory
            targetDirectory = chooseDirectory(
                    t("reefdb.action.context.export.title"),
                    t("reefdb.action.common.chooseDirectory.buttonLabel"));

            contextsToExport = Lists.newArrayList();
            for (ManageContextsListTableUIRowModel row : getModel().getSelectedRows()) {
                ContextDTO context = row.toBean();
                // copy filters
//                context.setFilters(row.getFilters());
                // load filtered elements
                for (FilterDTO f : context.getFilters()) {
                    if (!f.isFilterLoaded()) {
                        getContext().getContextService().loadFilteredElements(f);
                    }
                }
                contextsToExport.add(context);
            }
        }
        return (targetDirectory != null && CollectionUtils.isNotEmpty(contextsToExport));
    }

	/** {@inheritDoc} */
	@Override
	public void doAction() {
        // create new filename
        String fileName = String.format(EXPORT_FILE_FORMAT, Times.getFileSuffix());
        exportFile = new File(targetDirectory, fileName);

        // Export filters
        getContext().getContextService().exportContexts(contextsToExport, exportFile);
	}
    
    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        // display success message
        displayInfoMessage(t("reefdb.common.success"), t("reefdb.action.context.export.success", exportFile.getAbsolutePath()));
    }
}
