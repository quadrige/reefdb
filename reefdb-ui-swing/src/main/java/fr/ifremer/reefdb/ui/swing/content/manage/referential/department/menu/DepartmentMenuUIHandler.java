package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.*;
import java.util.List;

/**
 * Controlleur du menu pour la gestion des lieux au niveau National
 */
public class DepartmentMenuUIHandler extends ReferentialMenuUIHandler<DepartmentMenuUIModel, DepartmentMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(DepartmentMenuUIHandler.class);
    
    /** {@inheritDoc} */
    @Override
    public void beforeInit(final DepartmentMenuUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        final DepartmentMenuUIModel model = new DepartmentMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final DepartmentMenuUI ui) {
        super.afterInit(ui);

        // listen to model changes on 'local' to adapt combo box content
        getModel().addPropertyChangeListener(DepartmentMenuUIModel.PROPERTY_LOCAL, evt -> {
            getUI().getStatusCombo().setData(getContext().getReferentialService().getStatus(getModel().getStatusFilter()));
            getUI().getParentCombo().setData(getContext().getReferentialService().getDepartments(getModel().isLocal() ? StatusFilter.ALL : StatusFilter.NATIONAL));
        });
        
        // Initialiser les combobox
        initComboBox();

    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getLocalCombo().setEnabled(enabled);
        getUI().getCodeEditor().setEnabled(enabled);
        getUI().getNameEditor().setEnabled(enabled);
        getUI().getParentCombo().setEnabled(enabled);
        getUI().getStatusCombo().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllDepartmentFilters();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /** {@inheritDoc} */
    @Override
    public JComponent getLocalFilterPanel() {
        return getUI().getLocalPanel();
    }

    /**
	 * Initialisation des combobox
	 */
	private void initComboBox() {
		
        // Combo local
		initBeanFilterableComboBox(
				getUI().getLocalCombo(), 
				getContext().getSystemService().getBooleanValues(),
				null);
		
		initBeanFilterableComboBox(
				getUI().getParentCombo(), 
				getContext().getReferentialService().getDepartments(StatusFilter.ALL),
				null);
		
		initBeanFilterableComboBox(getUI().getStatusCombo(), 
				getContext().getReferentialService().getStatus(getModel().getStatusFilter()),
				null);
		
				
		// Dimension forcee sur la combobox
        ReefDbUIs.forceComponentSize(getUI().getLocalCombo());
        ReefDbUIs.forceComponentSize(getUI().getParentCombo());
        ReefDbUIs.forceComponentSize(getUI().getStatusCombo());
        
	}

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getUI().getParentCombo().setData(null);
        getUI().getParentCombo().setData(getContext().getReferentialService().getDepartments(getModel().isLocal() ? StatusFilter.ALL : StatusFilter.NATIONAL));
    }

}
