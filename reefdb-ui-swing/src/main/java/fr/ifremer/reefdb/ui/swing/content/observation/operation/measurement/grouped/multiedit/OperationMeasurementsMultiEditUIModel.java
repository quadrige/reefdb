package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.multiedit;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsMultiEditUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import java.util.Set;

public class OperationMeasurementsMultiEditUIModel
    extends AbstractMeasurementsMultiEditUIModel<MeasurementDTO, OperationMeasurementsGroupedRowModel, OperationMeasurementsMultiEditUIModel> {

    @Override
    public Set<ReefDbColumnIdentifier<OperationMeasurementsGroupedRowModel>> getIdentifiersToCheck() {
        return ImmutableSet.of(
            OperationMeasurementsGroupedTableModel.SAMPLING,
            OperationMeasurementsGroupedTableModel.TAXON_GROUP,
            OperationMeasurementsGroupedTableModel.TAXON,
            OperationMeasurementsGroupedTableModel.INPUT_TAXON_NAME,
            (ReefDbColumnIdentifier<OperationMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.ANALYST
        );
    }
}
