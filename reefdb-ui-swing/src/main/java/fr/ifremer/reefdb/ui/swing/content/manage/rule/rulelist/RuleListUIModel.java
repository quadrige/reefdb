package fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model pour le tableau des listes de regles
 */
public class RuleListUIModel extends AbstractReefDbTableUIModel<RuleListDTO, RuleListRowModel, RuleListUIModel> {

	private RulesUIModel parentModel;

	private boolean saveEnabled;
	public static final String PROPERTY_SAVE_ENABLED = "saveEnabled";

	/**
	 * Constructor.
	 */
	public RuleListUIModel() {
		super();
	}

	/**
	 * <p>Getter for the field <code>parentModel</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel} object.
	 */
	public RulesUIModel getParentModel() {
		return parentModel;
	}

	/**
	 * <p>Setter for the field <code>parentModel</code>.</p>
	 *
	 * @param parentModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel} object.
	 */
	public void setParentModel(RulesUIModel parentModel) {
		this.parentModel = parentModel;
	}

	public boolean isSaveEnabled() {
		return saveEnabled;
	}

	public void setSaveEnabled(boolean saveEnabled) {
		this.saveEnabled = saveEnabled;
		firePropertyChange(PROPERTY_SAVE_ENABLED, null, saveEnabled);
	}

}
