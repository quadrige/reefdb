package fr.ifremer.reefdb.ui.swing.content.manage.rule;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.exception.SaveForbiddenException;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbRemoteSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.menu.SearchAction;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Save rule list action
 */
public class SaveAction extends AbstractReefDbRemoteSaveAction<RulesUIModel, RulesUI, RulesUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final RulesUIHandler handler) {
        super(handler, false);
    }

    @Override
    protected void doSave() {

        getContext().getRuleListService().saveRuleLists(getContext().getAuthenticationInfo(), getModel().getRuleListUIModel().getRows());

    }

    @Override
    protected void reload() {

        // If control rules in table, force newCode state to false (Mantis #46446)
        if (getModel().getControlRuleUIModel().getRowCount() > 0) {
            getModel().getControlRuleUIModel().getRows().forEach(controlRuleRowModel -> controlRuleRowModel.setNewCode(false));
        }

        // reload ComboBox
        getUI().getRulesMenuUI().getHandler().reloadComboBox();

        getActionEngine().runInternalAction(getUI().getRulesMenuUI().getHandler(), SearchAction.class);

    }

    @Override
    protected void onSaveForbiddenException(SaveForbiddenException exception) {

        if (CollectionUtils.isNotEmpty(exception.getObjectIds())) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.save.rules.forbidden.topMessage"),
                    ApplicationUIUtil.getHtmlString(exception.getObjectIds()),
                    t("reefdb.action.save.rules.forbidden.bottomMessage"),
                    t("reefdb.action.save.errors.title"));
        } else {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.save.rules.forbidden.message"),
                    t("reefdb.action.save.errors.title"));
        }

    }

    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        return ImmutableList.of(
                getModel().getRuleListUIModel(),
                getModel().getProgramsUIModel(),
                getModel().getDepartmentsUIModel(),
                getModel().getControlRuleUIModel(),
                getModel().getPmfmUIModel()
        );
    }

}
