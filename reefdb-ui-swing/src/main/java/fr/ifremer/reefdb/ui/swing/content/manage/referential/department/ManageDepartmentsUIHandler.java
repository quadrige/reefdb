package fr.ifremer.reefdb.ui.swing.content.manage.referential.department;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Controlleur pour la gestion des Departments
 */
public class ManageDepartmentsUIHandler extends AbstractReefDbUIHandler<ManageDepartmentsUIModel, ManageDepartmentsUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageDepartmentsUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageDepartmentsUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ui.setContextValue(new ManageDepartmentsUIModel());
        ui.setContextValue(SwingUtil.createActionIcon("config"));

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ManageDepartmentsUI ui) {
        initUI(ui);

        // Initialiser les parametres des ecrans Observation et prelevemnts
        getContext().clearObservationPrelevementsIds();

        // Listen modify & valid
        getModel().setLocalUIModel(getUI().getManageDepartmentsLocalUI().getModel());
        // listener local model
        listenModelModify(getModel().getLocalUIModel());
        getModel().getLocalUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<ManageDepartmentsUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;

    }

    /**
     * <p>reloadComboBoxes.</p>
     */
    public void reloadComboBoxes() {
        getUI().getManageDepartmentsNationalUI().getDepartmentMenuUI().getHandler().reloadComboBox();
        getUI().getManageDepartmentsLocalUI().getHandler().reloadComboBox();
        getUI().getManageDepartmentsLocalUI().getDepartmentMenuUI().getHandler().reloadComboBox();
    }
}
