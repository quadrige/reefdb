package fr.ifremer.reefdb.ui.swing.util.coordinate.horizontal;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Controlleur pour l'onglet observation general.
 */
public class CoordinateHorizontalUIHandler extends AbstractReefDbUIHandler<CoordinateHorizontalUIModel, CoordinateHorizontalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(CoordinateHorizontalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final CoordinateHorizontalUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        final CoordinateHorizontalUIModel model = new CoordinateHorizontalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final CoordinateHorizontalUI ui) {
        initUI(ui);
    }

    /**
     * Enabled input.
     *
     * @param enabled true if components should be enabled, false otherwise
     */
    public void setEnabled(final boolean enabled) {
    	getUI().getInputLatitudeMin().setEnabled(enabled);
    	getUI().getInputLatitudeMin().setShowReset(enabled);
    	getUI().getInputLatitudeMax().setEnabled(enabled);
    	getUI().getInputLatitudeMax().setShowReset(enabled);
    	getUI().getInputLongitudeMin().setEnabled(enabled);
    	getUI().getInputLongitudeMin().setShowReset(enabled);
    	getUI().getInputLongitudeMax().setEnabled(enabled);
    	getUI().getInputLongitudeMax().setShowReset(enabled);
    }
}
