package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class SamplingEquipmentsTableModel extends AbstractReefDbTableModel<SamplingEquipmentsTableRowModel> {

	
	/**
	 * <p>Constructor for SamplingEquipmentsTableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 * @param createNewRowAllowed a boolean.
	 */
	public SamplingEquipmentsTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
		super(columnModel, createNewRowAllowed, false);
	}
	
	/** Constant <code>NAME</code> */
	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
			SamplingEquipmentsTableRowModel.PROPERTY_NAME,
			n("reefdb.property.name"),
			n("reefdb.property.name"),
			String.class,
			true);
	
	/** Constant <code>STATUS</code> */
	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
			SamplingEquipmentsTableRowModel.PROPERTY_STATUS,
			n("reefdb.property.status"),
			n("reefdb.property.status"),
			StatusDTO.class,
			true);
	
	/** Constant <code>SIZE</code> */
	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> SIZE = ReefDbColumnIdentifier.newId(
			SamplingEquipmentsTableRowModel.PROPERTY_SIZE,
			n("reefdb.property.samplingEquipment.size"),
			n("reefdb.property.samplingEquipment.size.tip"),
			Double.class);
	
	/** Constant <code>UNIT</code> */
	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> UNIT = ReefDbColumnIdentifier.newId(
			SamplingEquipmentsTableRowModel.PROPERTY_UNIT,
			n("reefdb.property.pmfm.unit"),
			n("reefdb.property.pmfm.unit"),
			UnitDTO.class);
	
	/** Constant <code>DESCRIPTION</code> */
	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
			SamplingEquipmentsTableRowModel.PROPERTY_DESCRIPTION,
			n("reefdb.property.description"),
			n("reefdb.property.description"),
			String.class);


	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
		SamplingEquipmentsTableRowModel.PROPERTY_COMMENT,
		n("reefdb.property.comment"),
		n("reefdb.property.comment"),
		String.class,
		false);

	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
		SamplingEquipmentsTableRowModel.PROPERTY_CREATION_DATE,
		n("reefdb.property.date.creation"),
		n("reefdb.property.date.creation"),
		Date.class);

	public static final ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
		SamplingEquipmentsTableRowModel.PROPERTY_UPDATE_DATE,
		n("reefdb.property.date.modification"),
		n("reefdb.property.date.modification"),
		Date.class);



	/** {@inheritDoc} */
	@Override
	public SamplingEquipmentsTableRowModel createNewRow() {
		return new SamplingEquipmentsTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<SamplingEquipmentsTableRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
