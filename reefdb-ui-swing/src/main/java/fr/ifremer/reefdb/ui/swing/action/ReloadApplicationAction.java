package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.action.CloseApplicationAction;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.ui.swing.ReefDbApplication;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To reload ReefDb application.
 *
 * @since 1.0
 */
public class ReloadApplicationAction extends CloseApplicationAction {

    /**
     * <p>Constructor for ReloadApplicationAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    public ReloadApplicationAction(ReefDbMainUIHandler handler) {
        super(handler);
        setActionDescription(t("reefdb.main.action.reloadReefDb"));
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        // Clean caches 
        if (getContext().isPersistenceLoaded()) {
            ReefDbServiceLocator.instance().getCacheService().clearAllCaches();
        }

        setExitCode(ReefDbApplication.UPDATE_EXIT_CODE);
        super.doAction();
    }

}
