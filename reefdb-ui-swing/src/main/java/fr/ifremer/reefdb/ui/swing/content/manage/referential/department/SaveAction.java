package fr.ifremer.reefdb.ui.swing.content.manage.referential.department;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;

import java.util.List;

/**
 * Action fermer une observation.
 */
public class SaveAction extends AbstractReefDbSaveAction<ManageDepartmentsUIModel, ManageDepartmentsUI, ManageDepartmentsUIHandler> {

    private List<DepartmentDTO> departments;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ManageDepartmentsUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        departments = getModel().getLocalUIModel().getBeans();

        // if the screen can be saved and the screen is valid
        return getModel().isModify() && getModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {
        getContext().getReferentialService().saveDepartments(departments);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        // reload departments with new ids
        getModel().getLocalUIModel().setBeans(departments);

        getHandler().reloadComboBoxes();

        getModel().setModify(false);

        super.postSuccessAction();
    }

}
