package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>ParameterTableRowModel class.</p>
 *
 * @author Antoine
 */
public class ParameterTableRowModel extends AbstractReefDbRowUIModel<ParameterDTO, ParameterTableRowModel> implements ParameterDTO, ErrorAware {

    private static final Binder<ParameterDTO, ParameterTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(ParameterDTO.class, ParameterTableRowModel.class);

    private static final Binder<ParameterTableRowModel, ParameterDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ParameterTableRowModel.class, ParameterDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for ParameterTableRowModel.</p>
     */
    public ParameterTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected ParameterDTO newBean() {
        return ReefDbBeanFactory.newParameterDTO();
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /**
     * <p>isLocal.</p>
     *
     * @return a boolean.
     */
    public boolean isLocal() {
        return ReefDbBeans.isLocalStatus(getStatus());
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String mnemonique) {
        delegateObject.setName(mnemonique);
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return delegateObject.getCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setCode(String code) {
        delegateObject.setCode(code);
    }

    /** {@inheritDoc} */
    @Override
    public QualitativeValueDTO getQualitativeValues(int index) {
        return delegateObject.getQualitativeValues(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isQualitativeValuesEmpty() {
        return delegateObject.isQualitativeValuesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeQualitativeValues() {
        return delegateObject.sizeQualitativeValues();
    }

    /** {@inheritDoc} */
    @Override
    public void addQualitativeValues(QualitativeValueDTO QualitativeValues) {
        delegateObject.addQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllQualitativeValues(Collection<QualitativeValueDTO> QualitativeValues) {
        delegateObject.addAllQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeQualitativeValues(QualitativeValueDTO QualitativeValues) {
        return delegateObject.removeQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllQualitativeValues(Collection<QualitativeValueDTO> QualitativeValues) {
        return delegateObject.removeAllQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsQualitativeValues(QualitativeValueDTO QualitativeValues) {
        return delegateObject.containsQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllQualitativeValues(Collection<QualitativeValueDTO> QualitativeValues) {
        return delegateObject.containsAllQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualitativeValueDTO> getQualitativeValues() {
        return delegateObject.getQualitativeValues();
    }

    /** {@inheritDoc} */
    @Override
    public void setQualitativeValues(List<QualitativeValueDTO> QualitativeValues) {
        delegateObject.setQualitativeValues(QualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public ParameterGroupDTO getParameterGroup() {
        return delegateObject.getParameterGroup();
    }

    /** {@inheritDoc} */
    @Override
    public void setParameterGroup(ParameterGroupDTO parameterGroup) {
        delegateObject.setParameterGroup(parameterGroup);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCalculated() {
        return delegateObject.isCalculated();
    }

    /** {@inheritDoc} */
    @Override
    public void setCalculated(boolean isCalculated) {
        delegateObject.setCalculated(isCalculated);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isQualitative() {
        return delegateObject.isQualitative();
    }

    /** {@inheritDoc} */
    @Override
    public void setQualitative(boolean isQualitative) {
        delegateObject.setQualitative(isQualitative);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTaxonomic() {
        return delegateObject.isTaxonomic();
    }

    /** {@inheritDoc} */
    @Override
    public void setTaxonomic(boolean isTaxonomic) {
        delegateObject.setTaxonomic(isTaxonomic);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isNewCode() {
        return delegateObject.isNewCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setNewCode(boolean newCode) {
        delegateObject.setNewCode(newCode);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
