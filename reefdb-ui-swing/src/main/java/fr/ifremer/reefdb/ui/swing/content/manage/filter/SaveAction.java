package fr.ifremer.reefdb.ui.swing.content.manage.filter;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;

import java.util.List;

/**
 * Save action.
 */
public class SaveAction extends AbstractReefDbSaveAction<FilterUIModel, FilterUI, FilterUIHandler> {

    private List<? extends FilterDTO> filtersToSave;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final FilterUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || !getModel().isModify() || !getModel().isValid()) {
            return false;
        }

        filtersToSave = getUI().getFilterListUI().getModel().getRows();
        return !filtersToSave.isEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Save Filters by generic method
        getContext().getContextService().saveFilters(filtersToSave);
    }

    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        return ImmutableList.of(
                getModel().getFilterListUIModel(),
                getModel().getFilterElementUIModel()
        );
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getUI().getFilterListUI().getHandler().reloadComboBox();

        super.postSuccessAction();

    }
}
