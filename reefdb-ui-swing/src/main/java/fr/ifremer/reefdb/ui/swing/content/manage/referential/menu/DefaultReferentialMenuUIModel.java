package fr.ifremer.reefdb.ui.swing.content.manage.referential.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.FilterCriteriaDTO;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Created by Ludovic on 23/03/2016.
 */
public class DefaultReferentialMenuUIModel extends AbstractReferentialMenuUIModel<FilterCriteriaDTO, DefaultReferentialMenuUIModel> {

    private static final Binder<DefaultReferentialMenuUIModel, FilterCriteriaDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(DefaultReferentialMenuUIModel.class, FilterCriteriaDTO.class);

    private static final Binder<FilterCriteriaDTO, DefaultReferentialMenuUIModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(FilterCriteriaDTO.class, DefaultReferentialMenuUIModel.class);

    /**
     * <p>Constructor for DefaultReferentialMenuUIModel.</p>
     */
    public DefaultReferentialMenuUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected FilterCriteriaDTO newBean() {
        return ReefDbBeanFactory.newFilterCriteriaDTO();
    }

}
