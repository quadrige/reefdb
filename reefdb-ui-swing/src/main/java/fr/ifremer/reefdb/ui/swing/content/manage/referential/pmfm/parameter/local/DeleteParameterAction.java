package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de delete un parameter local
 */
public class DeleteParameterAction extends AbstractReefDbAction<ManageParametersLocalUIModel, ManageParametersLocalUI, ManageParametersLocalUIHandler> {

    private List<String> toDelete;
    private boolean deleteOk = false;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteParameterAction(final ManageParametersLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        toDelete = ReefDbBeans.collectProperties(getModel().getSelectedRows(), ParameterDTO.PROPERTY_CODE);
        return super.prepareAction()
                && CollectionUtils.isNotEmpty(toDelete)
                && askBeforeDeleteMany(
                t("reefdb.action.delete.parameter.title"),
                t("reefdb.action.delete.parameter.message"),
                toDelete);

    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {
        // check usage
        List<String> used = Lists.newArrayList();
        for (String parameterCode : toDelete) {
            if (StringUtils.isNotBlank(parameterCode)) {
                if (getContext().getReferentialService().isParameterUsedInReferential(parameterCode)) {
                    used.add(parameterCode);
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.referential.message"));
            return;
        }

        for (String parameterCode : toDelete) {
            if (StringUtils.isNotBlank(parameterCode)) {
                if (getContext().getReferentialService().isParameterUsedInRules(parameterCode)) {
                    used.add(parameterCode);
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.rule.message"));
            return;
        }

        // apply delete
        getContext().getReferentialService().deleteParameters(toDelete);
        deleteOk = true;

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        if (deleteOk) {
            getModel().deleteSelectedRows();
            getUI().getMenuUI().getHandler().reloadComboBox();
        }

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        deleteOk = false;
        super.releaseAction();
    }
}
