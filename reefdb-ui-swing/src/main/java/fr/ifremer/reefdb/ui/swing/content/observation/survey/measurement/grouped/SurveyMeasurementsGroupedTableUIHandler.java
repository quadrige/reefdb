package fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.ColumnIdentifier;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedTableUIHandler;
import fr.ifremer.reefdb.ui.swing.content.observation.survey.measurement.grouped.multiedit.SurveyMeasurementsMultiEditUI;
import jaxx.runtime.SwingUtil;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import java.util.ArrayList;
import java.util.List;

/**
 * Controleur pour les mesures de l'observation.
 */
public class SurveyMeasurementsGroupedTableUIHandler
        extends AbstractMeasurementsGroupedTableUIHandler<SurveyMeasurementsGroupedRowModel, SurveyMeasurementsGroupedTableUIModel, SurveyMeasurementsGroupedTableUI> {

    // initial border of full screen toggle button
    private Border initialToggleButtonBorder;

    /**
     * <p>Constructor for SurveyMeasurementsGroupedTableUIHandler.</p>
     */
    public SurveyMeasurementsGroupedTableUIHandler() {
        super(SurveyMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP,
                SurveyMeasurementsGroupedRowModel.PROPERTY_TAXON,
                SurveyMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS,
                SurveyMeasurementsGroupedRowModel.PROPERTY_COMMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractMeasurementsGroupedTableModel<SurveyMeasurementsGroupedRowModel> getTableModel() {
        return (SurveyMeasurementsGroupedTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getSurveyGroupedMeasurementTable();
    }

    @Override
    protected SurveyMeasurementsGroupedTableUIModel createNewModel() {
        return new SurveyMeasurementsGroupedTableUIModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final SurveyMeasurementsGroupedTableUI ui) {

        super.afterInit(ui);

        // Les boutons sont desactives
        getUI().getDeleteButton().setEnabled(false);
        getUI().getDuplicateButton().setEnabled(false);
        SwingUtil.setLayerUI(ui.getTableauBasScrollPane(), ui.getTableBlockLayer());

        // Keep initial border
        initialToggleButtonBorder = getUI().getFullScreenToggleButton().getBorder();

    }

    @Override
    public void toggleFullScreen(JPanel panel, JToggleButton toggleButton) {
        super.toggleFullScreen(panel, toggleButton);

        // Set highlight border when selected (Mantis #48592)
        toggleButton.setBorder(
                toggleButton.isSelected()
                        ? BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), initialToggleButtonBorder)
                        : initialToggleButtonBorder
        );

    }

    @Override
    protected void filterMeasurements() {

        getTable().getColumnExt(SurveyMeasurementsGroupedTableModel.TAXON_GROUP).setEditable(getModel().getTaxonGroupFilter() == null);
        getTable().getColumnExt(SurveyMeasurementsGroupedTableModel.TAXON).setEditable(getModel().getTaxonFilter() == null);

        if (getModel().getTaxonGroupFilter() == null && getModel().getTaxonFilter() == null) {
            // remove filter
            getTable().setRowFilter(null);
        } else {
            // add filter
            getTable().setRowFilter(new RowFilter<SurveyMeasurementsGroupedTableModel, Integer>() {
                @Override
                public boolean include(Entry<? extends SurveyMeasurementsGroupedTableModel, ? extends Integer> entry) {
                    return (getModel().getTaxonGroupFilter() == null
                            || getModel().getTaxonGroupFilter().equals(entry.getValue(getTable().getColumnExt(SurveyMeasurementsGroupedTableModel.TAXON_GROUP).getModelIndex())))
                            && (getModel().getTaxonFilter() == null
                            || getModel().getTaxonFilter().equals(entry.getValue(getTable().getColumnExt(SurveyMeasurementsGroupedTableModel.TAXON).getModelIndex())));
                }
            });
        }
    }

    @Override
    protected List<? extends MeasurementAware> getMeasurementAwareModels() {
        List<ObservationUIModel> result = new ArrayList<>();
        result.add(getModel().getSurvey());
        return result;
    }

    @Override
    protected SurveyMeasurementsGroupedRowModel createNewRow(boolean readOnly, MeasurementAware parentBean) {
        return new SurveyMeasurementsGroupedRowModel(readOnly);
    }

    @Override
    protected MeasurementAware getMeasurementAwareModelForRow(SurveyMeasurementsGroupedRowModel row) {
        return getModel().getSurvey();
    }

    /**
     * Initialisation du tableau.
     */
    @Override
    protected void initTable() {

        if (getConfig().isDebugMode()) {
            TableColumnExt indivIdCol = addColumn((ColumnIdentifier<SurveyMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.INDIVIDUAL_ID);
            indivIdCol.setSortable(true);
            setDefaultColumnMinWidth(indivIdCol);
        }

        // Colonne groupe taxon
        final TableColumnExt colTaxonGroup = addColumn(
                taxonGroupCellEditor,
                newTableCellRender(SurveyMeasurementsGroupedTableModel.TAXON_GROUP),
                SurveyMeasurementsGroupedTableModel.TAXON_GROUP);
        colTaxonGroup.setSortable(true);
        setDefaultColumnMinWidth(colTaxonGroup);

        // Colonne taxon
        final TableColumnExt colTaxon = addColumn(
                taxonCellEditor,
                newTableCellRender(SurveyMeasurementsGroupedTableModel.TAXON),
                SurveyMeasurementsGroupedTableModel.TAXON);
        colTaxon.setSortable(true);
        setDefaultColumnMinWidth(colTaxon);

        // Colonne taxon saisi
        final TableColumnExt colInputTaxon = addColumn(SurveyMeasurementsGroupedTableModel.INPUT_TAXON_NAME);
        colInputTaxon.setSortable(true);
        colInputTaxon.setEditable(false);
        setDefaultColumnMinWidth(colInputTaxon);

        // Colonne analyste
        final TableColumnExt colAnalyst = addColumn(
                departmentCellEditor,
                newTableCellRender(AbstractMeasurementsGroupedTableModel.ANALYST),
                (ColumnIdentifier<SurveyMeasurementsGroupedRowModel>) AbstractMeasurementsGroupedTableModel.ANALYST);
        colAnalyst.setSortable(true);
        setDefaultColumnMinWidth(colAnalyst);

        // Colonne commentaire
        final TableColumnExt colComment = addCommentColumn(SurveyMeasurementsGroupedTableModel.COMMENT);

        // Modele de la table
        final SurveyMeasurementsGroupedTableModel tableModel = new SurveyMeasurementsGroupedTableModel(getTable().getColumnModel(), true);
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable());

        colComment.setVisible(false);
        colAnalyst.setVisible(false);
        colInputTaxon.setVisible(false);

        // border
        addEditionPanelBorder();
    }

    public void editSelectedMeasurements() {

        editSelectedMeasurements(new SurveyMeasurementsMultiEditUI(getUI()));

    }
}
