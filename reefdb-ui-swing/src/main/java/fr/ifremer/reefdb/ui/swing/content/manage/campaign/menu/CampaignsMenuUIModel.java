package fr.ifremer.reefdb.ui.swing.content.manage.campaign.menu;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.SearchDateDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

import java.util.Date;

public class CampaignsMenuUIModel extends DefaultReferentialMenuUIModel {

    public static final String PROPERTY_SEARCH_START_DATE = "searchStartDate";
    public static final String PROPERTY_SEARCH_END_DATE = "searchEndDate";

    private SearchDateDTO searchStartDate;
    private SearchDateDTO searchEndDate;

    private Date startDate1;
    private Date startDate2;
    private Date endDate1;
    private Date endDate2;

    public SearchDateDTO getSearchStartDate() {
        return searchStartDate;
    }

    public void setSearchStartDate(SearchDateDTO searchStartDate) {
        this.searchStartDate = searchStartDate;
        firePropertyChange(PROPERTY_SEARCH_START_DATE, null, searchStartDate);
    }

    public SearchDateDTO getSearchEndDate() {
        return searchEndDate;
    }

    public void setSearchEndDate(SearchDateDTO searchEndDate) {
        this.searchEndDate = searchEndDate;
        firePropertyChange(PROPERTY_SEARCH_END_DATE, null, searchEndDate);
    }

    public Date getStartDate1() {
        return startDate1;
    }

    public void setStartDate1(Date startDate1) {
        this.startDate1 = startDate1;
    }

    public Date getStartDate2() {
        return startDate2;
    }

    public void setStartDate2(Date startDate2) {
        this.startDate2 = startDate2;
    }

    public Date getEndDate1() {
        return endDate1;
    }

    public void setEndDate1(Date endDate1) {
        this.endDate1 = endDate1;
    }

    public Date getEndDate2() {
        return endDate2;
    }

    public void setEndDate2(Date endDate2) {
        this.endDate2 = endDate2;
    }

    /** {@inheritDoc} */
    @Override
    public void clear() {
        super.clear();
        setName(null);
        setSearchStartDate(null);
        setSearchEndDate(null);
        setStartDate1(null);
        setStartDate2(null);
        setEndDate1(null);
        setEndDate2(null);
    }
}
