package fr.ifremer.reefdb.ui.swing.content.manage.rule.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.util.List;

/**
 * Controlleur du menu de l'onglet prelevements mesures.
 */
public class RulesMenuUIHandler extends ReferentialMenuUIHandler<RulesMenuUIModel, RulesMenuUI> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final RulesMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final RulesMenuUIModel model = new RulesMenuUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final RulesMenuUI ui) {
        initUI(ui);

        initComboBox();
        initListeners();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        initBeanFilterableComboBox(
            getUI().getRuleListComboBox(),
            getContext().getRuleListService().getRuleLists(),
            null);

        initBeanFilterableComboBox(
            getUI().getProgramComboBox(),
            getContext().getProgramStrategyService().getWritableProgramsByUserAndStatus(
                SecurityContextHelper.getQuadrigeUserId(),
                StatusFilter.ACTIVE
            ),
            null
        );

        ReefDbUIs.forceComponentSize(getUI().getRuleListComboBox());
        ReefDbUIs.forceComponentSize(getUI().getProgramComboBox());
    }

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getModel().setLoading(true);
        BeanFilterableComboBox<RuleListDTO> cb = getUI().getRuleListComboBox();
        cb.setData(null);
        List<RuleListDTO> ruleLists = getContext().getRuleListService().getRuleLists();
        cb.setData(ruleLists);

        // clear selected item if not present in list (Mantis #50537)
        if (cb.getSelectedItem() instanceof RuleListDTO && !ruleLists.contains(cb.getSelectedItem())) {
            cb.setSelectedItem(null);
        }

        getModel().setLoading(false);
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(RulesMenuUIModel.PROPERTY_RULE_LIST, evt -> {
            if (getModel().isLoading()) return;
            if (getModel().getRuleList() != null) {
                getModel().setProgram(null);
                SwingUtilities.invokeLater(() -> getUI().getSearchButton().getAction().actionPerformed(null));
            }
        });
    }

    @Override
    public void enableSearch(boolean enabled) {
    }

    @Override
    public List<FilterDTO> getFilters() {
        return null;
    }

    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return null;
    }

    @Override
    public JComponent getLocalFilterPanel() {
        return null;
    }

}
