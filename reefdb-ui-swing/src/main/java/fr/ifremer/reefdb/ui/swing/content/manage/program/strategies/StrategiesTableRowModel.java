package fr.ifremer.reefdb.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.List;

/**
 * Modele pour le tableau de programmes.
 */
public class StrategiesTableRowModel extends AbstractReefDbRowUIModel<StrategyDTO, StrategiesTableRowModel> implements StrategyDTO {

    private static final Binder<StrategyDTO, StrategiesTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(StrategyDTO.class, StrategiesTableRowModel.class);

    private static final Binder<StrategiesTableRowModel, StrategyDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(StrategiesTableRowModel.class, StrategyDTO.class);

    /**
     * Constructor.
     */
    public StrategiesTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected StrategyDTO newBean() {
        return ReefDbBeanFactory.newStrategyDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isAppliedStrategiesLoaded() {
        return delegateObject.isAppliedStrategiesLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setAppliedStrategiesLoaded(boolean appliedStrategiesLoaded) {
        delegateObject.setAppliedStrategiesLoaded(appliedStrategiesLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmStrategiesLoaded() {
        return delegateObject.isPmfmStrategiesLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setPmfmStrategiesLoaded(boolean pmfmStrategyLoaded) {
        delegateObject.setPmfmStrategiesLoaded(pmfmStrategyLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public AppliedStrategyDTO getAppliedStrategies(int index) {
        return delegateObject.getAppliedStrategies(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isAppliedStrategiesEmpty() {
        return delegateObject.isAppliedStrategiesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeAppliedStrategies() {
        return delegateObject.sizeAppliedStrategies();
    }

    /** {@inheritDoc} */
    @Override
    public void addAppliedStrategies(AppliedStrategyDTO appliedStrategy) {
        delegateObject.addAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllAppliedStrategies(Collection<AppliedStrategyDTO> appliedStrategy) {
        delegateObject.addAllAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAppliedStrategies(AppliedStrategyDTO appliedStrategy) {
        return delegateObject.removeAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllAppliedStrategies(Collection<AppliedStrategyDTO> appliedStrategy) {
        return delegateObject.removeAllAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAppliedStrategies(AppliedStrategyDTO appliedStrategy) {
        return delegateObject.containsAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllAppliedStrategies(Collection<AppliedStrategyDTO> appliedStrategy) {
        return delegateObject.containsAllAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public List<AppliedStrategyDTO> getAppliedStrategies() {
        return delegateObject.getAppliedStrategies();
    }

    /** {@inheritDoc} */
    @Override
    public void setAppliedStrategies(List<AppliedStrategyDTO> appliedStrategy) {
        delegateObject.setAppliedStrategies(appliedStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmStrategyDTO getPmfmStrategies(int index) {
        return delegateObject.getPmfmStrategies(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmStrategiesEmpty() {
        return delegateObject.isPmfmStrategiesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizePmfmStrategies() {
        return delegateObject.sizePmfmStrategies();
    }

    /** {@inheritDoc} */
    @Override
    public void addPmfmStrategies(PmfmStrategyDTO pmfmStrategy) {
        delegateObject.addPmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllPmfmStrategies(Collection<PmfmStrategyDTO> pmfmStrategy) {
        delegateObject.addAllPmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removePmfmStrategies(PmfmStrategyDTO pmfmStrategy) {
        return delegateObject.removePmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllPmfmStrategies(Collection<PmfmStrategyDTO> pmfmStrategy) {
        return delegateObject.removeAllPmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsPmfmStrategies(PmfmStrategyDTO pmfmStrategy) {
        return delegateObject.containsPmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllPmfmStrategies(Collection<PmfmStrategyDTO> pmfmStrategy) {
        return delegateObject.containsAllPmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmStrategyDTO> getPmfmStrategies() {
        return delegateObject.getPmfmStrategies();
    }

    /** {@inheritDoc} */
    @Override
    public void setPmfmStrategies(List<PmfmStrategyDTO> pmfmStrategy) {
        delegateObject.setPmfmStrategies(pmfmStrategy);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

}
