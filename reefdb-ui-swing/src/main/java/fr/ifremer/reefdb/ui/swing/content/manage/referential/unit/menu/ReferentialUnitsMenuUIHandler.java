package fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Controlleur du menu pour la gestion des Units au niveau local
 */
public class ReferentialUnitsMenuUIHandler extends AbstractReefDbUIHandler<ReferentialUnitsMenuUIModel, ReferentialUnitsMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReferentialUnitsMenuUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ReferentialUnitsMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ReferentialUnitsMenuUIModel model = new ReferentialUnitsMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ReferentialUnitsMenuUI ui) {
        initUI(ui);

        // listen to model changes on 'local' to adapt combo box content
        getModel().addPropertyChangeListener(ReferentialUnitsMenuUIModel.PROPERTY_LOCAL, evt -> {
            getUI().getStatusCombo().setData(getContext().getReferentialService().getStatus(getModel().getStatusFilter()));
            reloadComboBox();
        });

        // Initialiser les combobox
        initComboBox();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        initBeanFilterableComboBox(getUI().getLabelCombo(),
                getContext().getReferentialService().getUnits(getModel().getStatusFilter()),
                null);

        initBeanFilterableComboBox(getUI().getStatusCombo(),
                getContext().getReferentialService().getStatus(getModel().getStatusFilter()),
                null);

        ReefDbUIs.forceComponentSize(getUI().getLabelCombo());
        ReefDbUIs.forceComponentSize(getUI().getStatusCombo());
    }

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getUI().getLabelCombo().setData(getContext().getReferentialService().getUnits(getModel().getStatusFilter()));
    }
}
