package fr.ifremer.reefdb.ui.swing.content.manage.rule.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * Action permettant d effacer la recherche sur les regles
 */
public class ClearAction extends AbstractCheckModelAction<RulesMenuUIModel, RulesMenuUI, RulesMenuUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public ClearAction(final RulesMenuUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        // Suppression choix de regle
        getModel().clear();

        RulesUI rulesUI = getUI().getParentContainer(RulesUI.class);
        rulesUI.getRuleListUI().getHandler().clearTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        final RulesUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        final RulesUIModel model = getLocalModel();
        if (model != null) {
            model.setModify(modelModify);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        final RulesUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        final RulesUI rulesUI = getUI().getParentContainer(RulesUI.class);
        return rulesUI.getHandler();
    }

    private RulesUIModel getLocalModel() {
        final RulesUI ui = getUI().getParentContainer(RulesUI.class);
        if (ui != null) {
            return ui.getModel();
        }
        return null;
    }

}
