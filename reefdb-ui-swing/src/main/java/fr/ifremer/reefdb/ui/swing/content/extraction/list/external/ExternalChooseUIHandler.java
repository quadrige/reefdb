package fr.ifremer.reefdb.ui.swing.content.extraction.list.external;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.*;

/**
 * <p>ExternalChooseUIHandler class.</p>
 *
 */
public class ExternalChooseUIHandler extends AbstractReefDbUIHandler<ExternalChooseUIModel, ExternalChooseUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ExternalChooseUI ui) {
        super.beforeInit(ui);

        ExternalChooseUIModel model = new ExternalChooseUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ExternalChooseUI ui) {
        initUI(ui);

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getEmailField();
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        if (getModel().isValid()) {
            closeDialog();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        stopListenValidatorValid(getValidator());
        getModel().setValid(false);
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<ExternalChooseUIModel> getValidator() {
        return getUI().getValidator();
    }
}
