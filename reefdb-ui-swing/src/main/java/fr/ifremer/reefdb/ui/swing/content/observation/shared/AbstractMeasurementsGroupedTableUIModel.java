package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleGroupDTO;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIHandler;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author peck7 on 08/01/2019.
 */
public abstract class AbstractMeasurementsGroupedTableUIModel<
    B extends MeasurementDTO,
    R extends AbstractMeasurementsGroupedRowModel<B, R>,
    M extends AbstractMeasurementsGroupedTableUIModel<B, R, M>>
    extends AbstractReefDbTableUIModel<B, R, M> {

    public static final String PROPERTY_SURVEY = "survey";
    public static final String PROPERTY_MEASUREMENT_FILTER = "measurementFilter";
    public static final String EVENT_INDIVIDUAL_MEASUREMENTS_LOADED = "individualMeasurementsLoaded";

    // the survey model
    private ObservationUIModel survey;
    private ObservationUIHandler observationHandler;
    // pmfms with unique constraint
    private List<PmfmDTO> uniquePmfms;
    // the measurement filter
    private MeasurementsFilter measurementFilter;
    // map holding preconditions per pmfm id
    private Multimap<Integer, PreconditionRuleDTO> preconditionRulesByPmfmIdMap;
    // map holding groups per pmfm id
    private Multimap<ControlRuleDTO, Map.Entry<RuleGroupDTO, ? extends ReefDbColumnIdentifier<R>>> identifiersByGroupedRuleMap;
    private boolean adjusting;

    /**
     * <p>Getter for the field <code>survey</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public ObservationUIModel getSurvey() {
        return survey;
    }

    /**
     * <p>Setter for the field <code>survey</code>.</p>
     *
     * @param survey a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public void setSurvey(ObservationUIModel survey) {
        this.survey = survey;
        firePropertyChange(PROPERTY_SURVEY, null, survey);
    }

    public ObservationUIHandler getObservationUIHandler() {
        return observationHandler;
    }

    public void setObservationHandler(ObservationUIHandler observationHandler) {
        this.observationHandler = observationHandler;
    }

    /**
     * <p>Getter for the field <code>uniquePmfms</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<PmfmDTO> getUniquePmfms() {
        return uniquePmfms;
    }

    /**
     * <p>Setter for the field <code>uniquePmfms</code>.</p>
     *
     * @param uniquePmfms a {@link java.util.List} object.
     */
    public void setUniquePmfms(List<PmfmDTO> uniquePmfms) {
        this.uniquePmfms = uniquePmfms;
    }

    /**
     * <p>Setter for the field <code>measurementFilter</code>.</p>
     *
     * @param measurementFilter a {@link MeasurementsFilter} object.
     */
    public void setMeasurementFilter(MeasurementsFilter measurementFilter) {
        this.measurementFilter = measurementFilter;
        firePropertyChange(PROPERTY_MEASUREMENT_FILTER, null, measurementFilter);
    }

    /**
     * <p>getTaxonGroupFilter.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    public TaxonGroupDTO getTaxonGroupFilter() {
        return measurementFilter == null ? null : measurementFilter.getTaxonGroup();
    }

    /**
     * <p>getTaxonFilter.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    public TaxonDTO getTaxonFilter() {
        return measurementFilter == null ? null : measurementFilter.getTaxon();
    }

    /**
     * <p>getSamplingFilter.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    public SamplingOperationDTO getSamplingFilter() {
        return measurementFilter == null ? null : measurementFilter.getSamplingOperation();
    }

    public Multimap<Integer, PreconditionRuleDTO> getPreconditionRulesByPmfmIdMap() {
        if (preconditionRulesByPmfmIdMap == null) preconditionRulesByPmfmIdMap = ArrayListMultimap.create();
        return preconditionRulesByPmfmIdMap;
    }

    public void addPreconditionRuleByPmfmId(Integer pmfmId, PreconditionRuleDTO precondition) {
        getPreconditionRulesByPmfmIdMap().put(pmfmId, precondition);
    }

    public boolean isPmfmIdHasPreconditions(int pmfmId) {
        return getPreconditionRulesByPmfmIdMap().containsKey(pmfmId);
    }

    public Collection<PreconditionRuleDTO> getPreconditionRulesByPmfmId(int pmfmId) {
        return getPreconditionRulesByPmfmIdMap().get(pmfmId);
    }

    public Multimap<ControlRuleDTO, Map.Entry<RuleGroupDTO, ? extends ReefDbColumnIdentifier<R>>> getIdentifiersByGroupedRuleMap() {
        if (identifiersByGroupedRuleMap == null) identifiersByGroupedRuleMap = ArrayListMultimap.create();
        return identifiersByGroupedRuleMap;
    }

    public ReefDbColumnIdentifier<R> getIdentifierByGroup(RuleGroupDTO group) {
        for (Map.Entry<RuleGroupDTO, ? extends ReefDbColumnIdentifier<R>> identifierByGroup : getIdentifiersByGroupedRuleMap().values()) {
            if (identifierByGroup.getKey().equals(group))
                return identifierByGroup.getValue();
        }
        return null;
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    public void fireMeasurementsLoaded() {
        firePropertyChange(EVENT_INDIVIDUAL_MEASUREMENTS_LOADED, null, null);
    }
}
