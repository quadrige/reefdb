package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationAware;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.AbstractMeasurementsGroupedRowModel;

import java.util.List;
import java.util.Objects;

/**
 * <p>OperationMeasurementsGroupedRowModel class.</p>
 */
public class OperationMeasurementsGroupedRowModel
    extends AbstractMeasurementsGroupedRowModel<MeasurementDTO, OperationMeasurementsGroupedRowModel>
        implements SamplingOperationAware {

    /**
     * Constant <code>PROPERTY_SAMPLING_OPERATION="samplingOperation"</code>
     */
    public static final String PROPERTY_SAMPLING_OPERATION = "samplingOperation";

    private SamplingOperationDTO samplingOperation;

    /**
     * Constructor.
     *
     * @param readOnly a boolean.
     */
    public OperationMeasurementsGroupedRowModel(boolean readOnly) {
        super(readOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SamplingOperationDTO getSamplingOperation() {
        return samplingOperation;
    }

    /**
     * <p>Setter for the field <code>samplingOperation</code>.</p>
     *
     * @param samplingOperation a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    public void setSamplingOperation(SamplingOperationDTO samplingOperation) {
        SamplingOperationDTO oldValue = this.samplingOperation;
        this.samplingOperation = samplingOperation;
        firePropertyChange(PROPERTY_SAMPLING_OPERATION, oldValue, samplingOperation);
    }

    @Override
    public boolean isSameRow(AbstractMeasurementsGroupedRowModel<?, ?> that) {
        if (!(that instanceof OperationMeasurementsGroupedRowModel)) return false;
        return Objects.equals(this.getSamplingOperation(), ((OperationMeasurementsGroupedRowModel) that).getSamplingOperation()) && super.isSameRow(that);
    }

    @Override
    public List<String> getDefaultProperties() {
        return Lists.newArrayList(PROPERTY_SAMPLING_OPERATION, PROPERTY_TAXON_GROUP, PROPERTY_TAXON);
    }

    @Override
    public String rowWithMeasurementsHashCode() {
        return String.valueOf(getSamplingOperation() != null ? getSamplingOperation().getName() : null) + '|' + super.rowWithMeasurementsHashCode();
    }
}
