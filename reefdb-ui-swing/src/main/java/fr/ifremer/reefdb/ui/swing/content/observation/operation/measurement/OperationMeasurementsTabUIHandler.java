package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableUI;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.ungrouped.OperationMeasurementsUngroupedRowModel;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.ungrouped.OperationMeasurementsUngroupedTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.observation.shared.MeasurementsFilter;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.PmfmTableColumn;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbPmfmColumnIdentifier;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet prelevements mesures.
 */
public class OperationMeasurementsTabUIHandler extends AbstractReefDbUIHandler<OperationMeasurementsTabUIModel, OperationMeasurementsTabUI> implements TabHandler {

    private boolean firstLoad = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(OperationMeasurementsTabUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final OperationMeasurementsTabUIModel model = new OperationMeasurementsTabUIModel();
        ui.setContextValue(model);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(OperationMeasurementsTabUI ui) {
        initUI(ui);

        // init comboboxes
        initComboBox();

        getModel().setUngroupedTableUIModel(getUI().getUngroupedTable().getModel());
        getModel().setGroupedTableUIModel(getUI().getGroupedTable().getModel());

        // init image
        SwingUtil.setComponentWidth(getUI().getLeftImage(), ui.getMenuPanel().getPreferredSize().width * 9 / 10);
        getUI().getLeftImage().setScaled(true);

        // Initialiser listeners
        initListeners();

        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Initialisation des prelevements
        initBeanFilterableComboBox(
            getUI().getSelectionPrelevementsCombo(),
            getModel().getSamplingOperations(),
            getModel().getSampling(),
            DecoratorService.CONCAT);

        // Initialisation des groupe de taxons
        initBeanFilterableComboBox(
            getUI().getSelectionGroupeTaxonCombo(),
            null,
            null, DecoratorService.NAME);

        getUI().getSelectionGroupeTaxonCombo().setActionEnabled(getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP));
        getUI().getSelectionGroupeTaxonCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updateTaxonGroupComboBox(getModel().getTaxon(), true);
        });

        // Intialisation des taxons
        initBeanFilterableComboBox(
            getUI().getSelectionTaxonCombo(),
            null,
            null);

        getUI().getSelectionTaxonCombo().setActionEnabled(getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON));
        getUI().getSelectionTaxonCombo().setActionListener(e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            updateTaxonComboBox(getModel().getTaxonGroup(), true);
        });

        ReefDbUIs.forceComponentSize(getUI().getSelectionPrelevementsCombo());
        ReefDbUIs.forceComponentSize(getUI().getSelectionGroupeTaxonCombo());
        ReefDbUIs.forceComponentSize(getUI().getSelectionTaxonCombo());
    }

    /**
     * Initialiser listeners de l ecran.
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_OBSERVATION_MODEL, evt -> load(getModel().getObservationModel()));

        // add model listener on selected sampling operation
        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_SAMPLING, evt -> {
            if (getModel().isAdjusting()) return;
            // reset context value
            getContext().setSelectedSamplingOperationId(getModel().getSampling() != null ? getModel().getSampling().getId() : null);
        });

        // add model listener on taxon group to filter taxons
        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_TAXON_GROUP, evt -> {
            if (getModel().isAdjusting()) return;
            getModel().setAdjusting(true);
            updateTaxonComboBox(evt.getNewValue() == null ? null : (TaxonGroupDTO) evt.getNewValue(), false);
            getModel().setAdjusting(false);
        });
        getModel().addPropertyChangeListener(OperationMeasurementsTabUIModel.PROPERTY_TAXON, evt -> {
            if (getModel().isAdjusting()) return;
            getModel().setAdjusting(true);
            updateTaxonGroupComboBox(evt.getNewValue() == null ? null : (TaxonDTO) evt.getNewValue(), false);
            getModel().setAdjusting(false);
        });

        PropertyChangeListener loadListener = new PropertyChangeListener() {

            Map<String, Boolean> eventMap = new HashMap<>(ImmutableMap.of(
                OperationMeasurementsUngroupedTableUIModel.EVENT_MEASUREMENTS_LOADED, false,
                OperationMeasurementsGroupedTableUIModel.EVENT_INDIVIDUAL_MEASUREMENTS_LOADED, false
            ));

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (isEventComplete(evt.getPropertyName())) {
                    // detect if the triplet of PMFM for transition length
                    detectPmfmForTransitionCalculator();
                    detectPitPmfmForGridInitialization();
                    resetEventMap();
                }
            }

            private boolean isEventComplete(String propertyName) {
                if (eventMap.containsKey(propertyName))
                    eventMap.put(propertyName, true);
                return eventMap.values().stream().allMatch(aBoolean -> aBoolean);
            }

            private void resetEventMap() {
                eventMap.keySet().forEach(key -> eventMap.put(key, false));
            }

        };
        // add listeners on table models
        getModel().getUngroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsUngroupedTableUIModel.EVENT_MEASUREMENTS_LOADED, loadListener);
        getModel().getGroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsGroupedTableUIModel.EVENT_INDIVIDUAL_MEASUREMENTS_LOADED, loadListener);

        // Add listeners on PROPERTY_ROWS_IN_ERROR to catch modifications and revalidate
        getModel().getUngroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsUngroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });
        getModel().getGroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsGroupedTableUIModel.PROPERTY_ROWS_IN_ERROR, evt -> {
            // re validate
            getValidator().doValidate();
        });

        // Add listener on loading property and propagate to children
        getModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_LOADING, evt -> {
            getModel().getUngroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
            getModel().getGroupedTableUIModel().setLoading((Boolean) evt.getNewValue());
        });

        // Listen tables models
        listenModelModify(getModel().getUngroupedTableUIModel());
        listenModelModify(getModel().getGroupedTableUIModel());

    }

    private void updateTaxonGroupComboBox(TaxonDTO taxon, boolean forceNoFilter) {

        getUI().getSelectionGroupeTaxonCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP));

        List<TaxonGroupDTO> taxonGroups = getModel().getObservationUIHandler().getAvailableTaxonGroups(taxon, forceNoFilter);

        getUI().getSelectionGroupeTaxonCombo().setData(taxonGroups);

        if (CollectionUtils.isEmpty(taxonGroups) && getModel().getTaxonGroup() != null) {
            getModel().setTaxonGroup(null);
            // Don't auto-select unique value (Mantis #49551)
//        } else if (taxonGroups.size() == 1) {
//            getModel().setTaxonGroup(taxonGroups.get(0));
        }

    }

    private void updateTaxonComboBox(TaxonGroupDTO taxonGroup, boolean forceNoFilter) {

        getUI().getSelectionTaxonCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON));

        List<TaxonDTO> taxons = getModel().getObservationUIHandler().getAvailableTaxons(taxonGroup, forceNoFilter);

        getUI().getSelectionTaxonCombo().setData(taxons);

        if (CollectionUtils.isEmpty(taxons) && getModel().getTaxon() != null) {
            getModel().setTaxon(null);
            // Don't auto-select unique value (Mantis #49551)
//        } else if (taxons.size() == 1) {
//            getModel().setTaxon(taxons.get(0));
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingValidator<OperationMeasurementsTabUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * Load observation.
     *
     * @param survey Observation
     */
    private void load(final ObservationUIModel survey) {

        if (survey == null || survey.getId() == null) {
            return;
        }

        if (firstLoad) {
            firstLoad = false;
            // prepare combo boxes
            updateTaxonGroupComboBox(null, false);
            updateTaxonComboBox(null, false);

            // auto-select filters if unique (Mantis #49551)
            if (CollectionUtils.size(getUI().getSelectionGroupeTaxonCombo().getData()) == 1) {
                getUI().getSelectionGroupeTaxonCombo().setSelectedItem(getUI().getSelectionGroupeTaxonCombo().getData().get(0));
            }
            if (CollectionUtils.size(getUI().getSelectionTaxonCombo().getData()) == 1) {
                getUI().getSelectionTaxonCombo().setSelectedItem(getUI().getSelectionTaxonCombo().getData().get(0));
            }
        }

        getModel().setAdjusting(true);

        List<PmfmStrategyDTO> pmfmStrategies = ReefDbBeans.filterCollection(survey.getPmfmStrategies(), input -> input != null && input.isSampling());

        // list available samplings
        getUI().getSelectionPrelevementsCombo().setData(getModel().getSamplingOperations());

        // Load ungrouped data (up table)
        {

            // load pmfms for ungrouped measurements
            List<PmfmDTO> pmfms = Lists.newArrayList();
            for (PmfmStrategyDTO pmfmStrategy : pmfmStrategies) {
                if (!pmfmStrategy.isGrouping()) {
                    pmfms.add(pmfmStrategy.getPmfm());
                }
            }

            // Load other Pmfms in sampling operations
            if (CollectionUtils.isNotEmpty(getModel().getSamplingOperations())) {
                // populate pmfms from strategy to sampling operation and vice versa
                for (SamplingOperationDTO samplingOperation : getModel().getSamplingOperations()) {
                    ReefDbBeans.fillListsEachOther(samplingOperation.getPmfms(), pmfms);
                }
            }

            // filter out pmfms under moratorium
            filterPmfmsUnderMoratorium(pmfms, survey);

            // Set model properties
            getModel().getUngroupedTableUIModel().setPmfms(pmfms);

            getModel().getUngroupedTableUIModel().setSurvey(survey);
        }

        // Load grouped data (down table)
        {

            List<PmfmDTO> individualPmfms = Lists.newArrayList();
            // find pmfms having taxon unique constraint in strategy
            List<PmfmDTO> uniquePmfms = Lists.newArrayList();
            for (PmfmStrategyDTO pmfmStrategy : pmfmStrategies) {
                if (pmfmStrategy.isGrouping()) {
                    individualPmfms.add(pmfmStrategy.getPmfm());
                    if (pmfmStrategy.isUnique()) {
                        uniquePmfms.add(pmfmStrategy.getPmfm());
                    }
                }
            }

            // Load other Pmfms in sampling operations
            if (CollectionUtils.isNotEmpty(getModel().getSamplingOperations())) {
                // populate pmfms from strategy to sampling operation and vice versa
                for (SamplingOperationDTO samplingOperation : getModel().getSamplingOperations()) {
                    ReefDbBeans.fillListsEachOther(samplingOperation.getIndividualPmfms(), individualPmfms);
                }
            }

            // filter out pmfms under moratorium
            filterPmfmsUnderMoratorium(individualPmfms, survey);

            // Set model properties
            getModel().getGroupedTableUIModel().setPmfms(individualPmfms);
            getModel().getGroupedTableUIModel().setUniquePmfms(uniquePmfms);

            getModel().getGroupedTableUIModel().setSurvey(survey);
        }

        //update selected sampling
        if (getContext().getSelectedSamplingOperationId() != null && getModel().getSamplingOperations() != null) {
            for (SamplingOperationDTO samplingOperation : getModel().getSamplingOperations()) {
                if (getContext().getSelectedSamplingOperationId().equals(samplingOperation.getId())) {
                    getModel().setSampling(samplingOperation);
                    break;
                }
            }
        }

        // execute doSearch to simulate loading measurements data
        doSearch();

        getModel().setAdjusting(false);
    }

    private void filterPmfmsUnderMoratorium(List<PmfmDTO> pmfms, ObservationUIModel survey) {
        if (CollectionUtils.isEmpty(survey.getPmfmsUnderMoratorium()))
            return;

        pmfms.removeIf(pmfm -> survey.getPmfmsUnderMoratorium().stream().anyMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(pmfm, moratoriumPmfm)));
    }

    /**
     * <p>clearSearch.</p>
     */
    void clearSearch() {

        // Suppression choix du groupe de taxons
        getModel().setTaxonGroup(null);

        // Suppression choix du taxon
        getModel().setTaxon(null);

        // Suppression choix prelevement
        getModel().setSampling(null);

    }

    /**
     * <p>doSearch.</p>
     */
    void doSearch() {

        // set taxon group and taxon filter
        getModel().getUngroupedTableUIModel().setSamplingFilter(getModel().getSampling());

        MeasurementsFilter measurementFilter = new MeasurementsFilter();
        measurementFilter.setTaxonGroup(getModel().getTaxonGroup());
        measurementFilter.setTaxon(getModel().getTaxon());
        measurementFilter.setSamplingOperation(getModel().getSampling());
        getModel().getGroupedTableUIModel().setMeasurementFilter(measurementFilter);
    }

    private void detectPmfmForTransitionCalculator() {

        if (CollectionUtils.isEmpty(getModel().getUngroupedTableUIModel().getPmfmColumns())
            || CollectionUtils.isEmpty(getModel().getGroupedTableUIModel().getPmfmColumns())) {
            // both tables must have pmfm columns
            return;
        }

        List<Integer[]> transitionLengthPmfmTriplets = getConfig().getCalculatedTransitionLengthPmfmTriplets();
        for (Integer[] transitionLengthPmfmTriplet : transitionLengthPmfmTriplets) {

            int transitionLengthPmfmId = transitionLengthPmfmTriplet[0];
            int startPositionPmfmId = transitionLengthPmfmTriplet[1];
            int endPositionPmfmId = transitionLengthPmfmTriplet[2];

            // find the 3 columns
            PmfmTableColumn startPositionColumn = null;
            PmfmTableColumn endPositionColumn = null;
            PmfmTableColumn transitionLengthColumn = null;

            for (PmfmTableColumn column : getModel().getUngroupedTableUIModel().getPmfmColumns()) {
                if (column.getPmfmId() == startPositionPmfmId && column.isNumerical()) {
                    startPositionColumn = column;
                    break;
                }
            }
            for (PmfmTableColumn column : getModel().getGroupedTableUIModel().getPmfmColumns()) {
                if (column.getPmfmId() == transitionLengthPmfmId && column.isNumerical()) {
                    transitionLengthColumn = column;
                } else if (column.getPmfmId() == endPositionPmfmId && column.isNumerical()) {
                    endPositionColumn = column;
                }
            }

            if (startPositionColumn == null || endPositionColumn == null || transitionLengthColumn == null) {
                // one of the 3 column not found
                continue;
            }

            // set pmfm ids in models
            getModel().getUngroupedTableUIModel().setCalculatedLengthStartPositionPmfmId(startPositionPmfmId);
            getModel().getGroupedTableUIModel().setCalculatedLengthEndPositionPmfmId(endPositionPmfmId);
            getModel().getGroupedTableUIModel().setCalculatedLengthTransitionPmfmId(transitionLengthPmfmId);

            // set the transition length column non editable
            transitionLengthColumn.setEditable(false);
            transitionLengthColumn.getPmfmIdentifier().setNotMandatory();
            SwingTable groupedTable = getUI().getGroupedTable().getOperationGroupedMeasurementTable();
            groupedTable.setSortable(false);
            groupedTable.getRowSorter().setSortKeys(null);

            // initialize map of start positions
            // FIXME: je sais pas pourquoi mais les SamplingOperationDTO dans le modèle et ceux des mesures ne sont pas les mêmes objects, du coup je passe passe par le Name
            Map<String, BigDecimal> map = Maps.newHashMap();
            for (OperationMeasurementsUngroupedRowModel ungroupedRow : getModel().getUngroupedTableUIModel().getRows()) {
                map.put(ungroupedRow.getName(), (BigDecimal) startPositionColumn.getPmfmIdentifier().getValue(ungroupedRow));
            }

            // Remove previous calculator
            if (transitionLengthCalculator != null) {
                getModel().getGroupedTableUIModel().removePropertyChangeListener(OperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, transitionLengthCalculator);
            }

            // create calculator
            transitionLengthCalculator = new TransitionLengthCalculator(map,
                startPositionColumn.getPmfmIdentifier(),
                endPositionColumn.getPmfmIdentifier(),
                transitionLengthColumn.getPmfmIdentifier());

            startPositionColumn.getPmfmIdentifier().addPropertyChangeListener(transitionLengthCalculator);
            endPositionColumn.getPmfmIdentifier().addPropertyChangeListener(transitionLengthCalculator);

            // add also property change listener on sampling operation column (when a sampling operation in a row has changed) (Mantis #0027395)
            getModel().getGroupedTableUIModel().addPropertyChangeListener(OperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION, transitionLengthCalculator);

        }
    }

    private void detectPitPmfmForGridInitialization() {

        try {

            if (CollectionUtils.isEmpty(getModel().getGroupedTableUIModel().getPmfmColumns())) {
                // grouped table must have pmfm columns
                return;
            }

            List<Integer> transitionPmfmIds = getConfig().getPitTransitionPmfmIds();
            List<Integer> originPmfmIds = getConfig().getPitOriginPmfmIds();
            List<Integer> transectLengthPmfmIds = getConfig().getPitTransectLengthPmfmIds();

            List<Integer> allPmfmIds = CollectionUtils.union(
                CollectionUtils.emptyIfNull(getModel().getUngroupedTableUIModel().getPmfms()),
                CollectionUtils.emptyIfNull(getModel().getGroupedTableUIModel().getPmfms())
            ).stream().map(QuadrigeBean::getId).collect(Collectors.toList());

            // Find the transition pmfm id
            List<Integer> presentTransitionPmfmIds = ListUtils.retainAll(allPmfmIds, transitionPmfmIds);
            if (presentTransitionPmfmIds.size() > 1) {
                throw new ReefDbTechnicalException("More than one PIT Transition pmfm found");
            }

            // Mantis #38345 The grid initialization is enabled even if the origin and transect length identifiers are missing
            if (presentTransitionPmfmIds.isEmpty()) {
                // if the transition id is unknown, stop here
                return;
            }
            int transitionPmfmId = presentTransitionPmfmIds.get(0);

            // find the transition column
            PmfmTableColumn transitionLengthColumn = getModel().getGroupedTableUIModel().getPmfmColumns().stream()
                .filter(column -> column.getPmfmId() == transitionPmfmId)
                .filter(PmfmTableColumn::isNumerical)
                .findFirst().orElse(null);

            if (transitionLengthColumn == null) {
                // transition column not found, stop here
                return;
            }

            // Find the origin pmfm id
            List<Integer> presentOriginPmfmIds = ListUtils.retainAll(allPmfmIds, originPmfmIds);
            if (presentOriginPmfmIds.size() > 1) {
                throw new ReefDbTechnicalException("More than one PIT Origin pmfm found");
            }

            // Find the transect length pmfm id
            List<Integer> presentTransectLengthPmfmIds = ListUtils.retainAll(allPmfmIds, transectLengthPmfmIds);
            if (presentTransectLengthPmfmIds.size() > 1) {
                throw new ReefDbTechnicalException("More than one PIT Transect Length pmfm found");
            }

            getModel().getGroupedTableUIModel().setPitTransectOriginPmfmId(presentOriginPmfmIds.size() == 1 ? presentOriginPmfmIds.get(0) : null);
            getModel().getGroupedTableUIModel().setPitTransectLengthPmfmId(presentTransectLengthPmfmIds.size() == 1 ? presentTransectLengthPmfmIds.get(0) : null);
            getModel().getGroupedTableUIModel().setPitTransitionLengthPmfmId(transitionPmfmId);

        } finally {

            getUI().getGroupedTable().applyDataBinding(OperationMeasurementsGroupedTableUI.BINDING_INIT_DATA_GRID_BUTTON_ENABLED);

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onShowTab(int currentIndex, int newIndex) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onRemoveTab() {
        return false;
    }

    private TransitionLengthCalculator transitionLengthCalculator;

    private class TransitionLengthCalculator implements PropertyChangeListener {

        private final Map<String, BigDecimal> startPositionMap;
        private final ReefDbPmfmColumnIdentifier<AbstractReefDbRowUIModel> startPositionColumnIdentifier;
        private final ReefDbPmfmColumnIdentifier<AbstractReefDbRowUIModel> endPositionColumnIdentifier;
        private final ReefDbPmfmColumnIdentifier<AbstractReefDbRowUIModel> transitionLengthColumnIdentifier;

        TransitionLengthCalculator(Map<String, BigDecimal> startPositionMap,
                                   ReefDbPmfmColumnIdentifier<AbstractReefDbRowUIModel> startPositionColumnIdentifier,
                                   ReefDbPmfmColumnIdentifier<AbstractReefDbRowUIModel> endPositionColumnIdentifier,
                                   ReefDbPmfmColumnIdentifier<AbstractReefDbRowUIModel> transitionLengthColumnIdentifier) {
            this.startPositionMap = startPositionMap;
            this.startPositionColumnIdentifier = startPositionColumnIdentifier;
            this.endPositionColumnIdentifier = endPositionColumnIdentifier;
            this.transitionLengthColumnIdentifier = transitionLengthColumnIdentifier;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            if (evt instanceof ReefDbPmfmColumnIdentifier.PmfmChangeEvent) {
                ReefDbPmfmColumnIdentifier.PmfmChangeEvent event = (ReefDbPmfmColumnIdentifier.PmfmChangeEvent) evt;

                ReefDbPmfmColumnIdentifier sourceIdentifier = (ReefDbPmfmColumnIdentifier) event.getSource();
                SamplingOperationDTO samplingOperation = event.getSamplingOperation();
                BigDecimal startPositionValue = (BigDecimal) event.getNewValue();

                // if startPosition identifier
                if (Objects.equals(sourceIdentifier, startPositionColumnIdentifier)) {

                    // update startPosition value
                    String samplingName = samplingOperation == null ? null : samplingOperation.getName();
                    if (StringUtils.isNotBlank(samplingName)) {
                        startPositionMap.put(samplingName, startPositionValue);
                        calculate(samplingName);
                    }
                }

                // if endPosition identifier
                if (Objects.equals(sourceIdentifier, endPositionColumnIdentifier)) {

                    String samplingName = samplingOperation == null ? null : samplingOperation.getName();
                    if (StringUtils.isNotBlank(samplingName)) {
                        calculate(samplingName);
                    }
                }

            } else if (OperationMeasurementsGroupedTableUIModel.PROPERTY_SAMPLING_OPERATION.equals(evt.getPropertyName())) {

                // event coming from sampling operation column
                SamplingOperationDTO samplingOperation = (SamplingOperationDTO) evt.getNewValue();
                String samplingName = samplingOperation == null ? null : samplingOperation.getName();
                if (StringUtils.isNotBlank(samplingName)) {
                    calculate(samplingName);
                }

            }
        }

        private void calculate(final String samplingOperationName) {

            BigDecimal startPosition = startPositionMap.get(samplingOperationName);

            List<OperationMeasurementsGroupedRowModel> individualMeasurements = ReefDbBeans.filterCollection(getModel().getGroupedTableUIModel().getRows(), input -> Objects.equals(input.getSamplingOperation() == null ? null : input.getSamplingOperation().getName(), samplingOperationName));

            individualMeasurements.sort(Comparator.comparingInt(o -> o.getIndividualId() == null ? 0 : o.getIndividualId()));

            BigDecimal lastEndPosition = null;
            for (OperationMeasurementsGroupedRowModel rowModel : individualMeasurements) {

                if (startPosition == null) {
                    // reset values
                    transitionLengthColumnIdentifier.setValue(rowModel, null);
                } else {
                    BigDecimal endPosition = (BigDecimal) endPositionColumnIdentifier.getValue(rowModel);
                    if (endPosition == null) {
                        transitionLengthColumnIdentifier.setValue(rowModel, null);
                    } else {
                        if (lastEndPosition == null) {
                            transitionLengthColumnIdentifier.setValue(rowModel, endPosition.subtract(startPosition));
                        } else {
                            transitionLengthColumnIdentifier.setValue(rowModel, endPosition.subtract(lastEndPosition));
                        }
                        lastEndPosition = endPosition;
                    }
                }
            }

            // refresh table
            getUI().getGroupedTable().getOperationGroupedMeasurementTable().repaint();
        }
    }

    public void save() {

        try {

            // Disable filter
            getModel().getUngroupedTableUIModel().setSamplingFilter(null);
            getModel().getGroupedTableUIModel().setMeasurementFilter(new MeasurementsFilter());

            // save ungrouped measurements
            getUI().getUngroupedTable().getHandler().save();

            // save grouped measurements
            getUI().getGroupedTable().getHandler().save();

        } finally {

            doSearch();
        }
    }

}
