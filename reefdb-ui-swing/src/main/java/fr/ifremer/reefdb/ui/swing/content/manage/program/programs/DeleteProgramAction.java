package fr.ifremer.reefdb.ui.swing.content.manage.program.programs;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de supprimer un programmes.
 */
public class DeleteProgramAction extends AbstractReefDbAction<ProgramsTableUIModel, ProgramsTableUI, ProgramsTableUIHandler> {

    List<String> programCodes;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteProgramAction(final ProgramsTableUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            return false;
        }

        programCodes = Lists.newArrayList();

        // Vérification de la présence que de programme locaux
        for (ProgramDTO program : getModel().getSelectedRows()) {
            programCodes.add(program.getCode());
            if (!ReefDbBeans.isLocalStatus(program.getStatus())) {
                displayErrorMessage(
                        t("reefdb.action.delete.program.title"),
                        t("reefdb.action.delete.program.error.national.message"));
                return false;
            }
        }

        // Demande de confirmation avant la suppression
        return askBeforeDelete(t("reefdb.action.delete.program.title"), t("reefdb.action.delete.program.message"));
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // check program usage in data
        for (String programCode : programCodes) {

            Long surveyCount = getContext().getObservationService().countSurveysWithProgramAndLocations(programCode, null);
            if (surveyCount > 0) {
                getContext().getDialogHelper().showErrorDialog(
                        surveyCount == 1
                                ? t("reefdb.action.delete.program.used.data.message", programCode)
                                : t("reefdb.action.delete.program.used.data.many.message", surveyCount, programCode),
                        t("reefdb.action.delete.program.title"));
                return;

            }
        }

        // check usage in other tables
        ProgramStrategyService service = getContext().getProgramStrategyService();
        List<String> used = Lists.newArrayList();
        for (String programCode : programCodes) {
            if (service.isProgramUsedByRuleList(programCode)) {
                used.add(programCode);
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.rule.message"));
            return;

        }
        for (String programCode : programCodes) {
            if (service.isProgramUsedByFilter(programCode)) {
                used.add(programCode);
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.filter.message"));
            return;

        }
        for (String programCode : programCodes) {
            if (service.isProgramUsedByExtraction(programCode)) {
                used.add(programCode);
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.referential.used.extraction.message"));
            return;

        }

        // Suppression des programmes
        service.deletePrograms(programCodes);

        // Suppression des lignes
        getModel().deleteSelectedRows();

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        final ProgramsUI adminProgrammeUI = getHandler().getProgramsUI();

        // reset other tables
        adminProgrammeUI.getStrategiesTableUI().getHandler().clearTable();
        adminProgrammeUI.getLocationsTableUI().getHandler().clearTable();
        adminProgrammeUI.getPmfmsTableUI().getHandler().clearTable();

        // Reload list without deleted data
        adminProgrammeUI.getMenuUI().getProgramMnemonicCombo().setData(getContext().getProgramStrategyService().getWritablePrograms());
        adminProgrammeUI.getMenuUI().getProgramMnemonicCombo().setSelectedItem(null);
        adminProgrammeUI.getMenuUI().getProgramCodeCombo().setData(getContext().getProgramStrategyService().getWritablePrograms());
        adminProgrammeUI.getMenuUI().getProgramCodeCombo().setSelectedItem(null);
    }
}
