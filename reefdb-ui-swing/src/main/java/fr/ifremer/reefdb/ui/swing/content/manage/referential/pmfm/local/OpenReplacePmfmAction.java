package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.ManagePmfmsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.local.replace.ReplacePmfmUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.local.replace.ReplacePmfmUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplacePmfmAction extends AbstractReefDbAction<PmfmsLocalUIModel, PmfmsLocalUI, PmfmsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplacePmfmAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.local.PmfmsLocalUIHandler} object.
     */
    public OpenReplacePmfmAction(PmfmsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<PmfmDTO, ReplacePmfmUIModel, ReplacePmfmUI> openAction =
                new AbstractOpenReplaceUIAction<PmfmDTO, ReplacePmfmUIModel, ReplacePmfmUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.pmfm");
                    }

                    @Override
                    protected ReplacePmfmUIModel createNewModel() {
                        return new ReplacePmfmUIModel();
                    }

                    @Override
                    protected ReplacePmfmUI createUI(JAXXInitialContext ctx) {
                        return new ReplacePmfmUI(ctx);
                    }

                    @Override
                    protected List<PmfmDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getPmfms(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected PmfmDTO getSelectedSource() {
                        List<PmfmDTO> selectedBeans = OpenReplacePmfmAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected PmfmDTO getSelectedTarget() {
                        ManagePmfmsUI ui = OpenReplacePmfmAction.this.getUI().getParentContainer(ManagePmfmsUI.class);
                        List<PmfmDTO> selectedBeans = ui.getPmfmsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
