package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;


/**
 * Duplicate action.
 */
public class DuplicateFilterAction extends AbstractReefDbAction<FilterListUIModel, FilterListUI, FilterListUIHandler> {
    
    private FilterDTO duplicatedFilter;

	/**
	 * Constructor.
	 *
	 * @param handler Controller
	 */
	public DuplicateFilterAction(final FilterListUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() {
		FilterListRowModel filterToDuplicate = getModel().getSingleSelectedRow();
		if (filterToDuplicate != null) {
			duplicatedFilter = getContext().getContextService().duplicateFilter(filterToDuplicate.toBean());
		}
	}

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        FilterListRowModel row = getModel().addNewRow(duplicatedFilter);
        getHandler().setFocusOnCell(row);
    }
}
