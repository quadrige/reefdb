<!--
  #%L
  Reef DB :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JPanel id="tableauBasPanel" decorator='help' layout="{new BorderLayout()}"
				implements='fr.ifremer.reefdb.ui.swing.util.ReefDbUI&lt;SurveyMeasurementsGroupedTableUIModel, SurveyMeasurementsGroupedTableUIHandler&gt;'>
	<import>
		fr.ifremer.reefdb.ui.swing.ReefDbHelpBroker
		fr.ifremer.reefdb.ui.swing.ReefDbUIContext
		fr.ifremer.reefdb.ui.swing.util.ReefDbUI
		fr.ifremer.quadrige3.ui.swing.ApplicationUI
		fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
		fr.ifremer.quadrige3.ui.swing.plaf.WaitBlockingLayerUI

		java.awt.FlowLayout
		javax.swing.Box
		javax.swing.BoxLayout
		java.awt.BorderLayout

		fr.ifremer.quadrige3.ui.swing.table.SwingTable
		fr.ifremer.quadrige3.ui.swing.component.ToggleButton

		static org.nuiton.i18n.I18n.*
	</import>

	<SurveyMeasurementsGroupedTableUIModel id='model' initializer='getContextValue(SurveyMeasurementsGroupedTableUIModel.class)'/>
	<ReefDbHelpBroker id='broker' constructorParams='"reefdb.home.help"'/>
	<WaitBlockingLayerUI id='tableBlockLayer'/>

	<script><![CDATA[
        public SurveyMeasurementsGroupedTableUI(ApplicationUI parentUI) {
            ApplicationUIUtil.setParentUI(this, parentUI);
        }
	]]></script>

	<JPanel layout="{new BorderLayout()}">
		<JScrollPane id="tableauBasScrollPane" decorator='boxed' border="{null}">
			<SwingTable id="surveyGroupedMeasurementTable"/>
		</JScrollPane>
	</JPanel>

	<JPanel id='tableauBasPanelButtons' layout='{new BorderLayout()}' constraints="BorderLayout.PAGE_END">
		<JPanel layout='{new FlowLayout()}' constraints='BorderLayout.LINE_START'>
			<JButton id='newButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="model.insertNewRowAfterSelected()"/>
			<JButton id='duplicateButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="handler.duplicateSelectedRow()"/>
			<JButton id='multiEditButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="handler.editSelectedMeasurements()"/>
			<JButton id='deleteButton' alignmentX='{Component.CENTER_ALIGNMENT}' onActionPerformed="handler.removeIndividualMeasurements()"/>
		</JPanel>
		<JPanel constraints="BorderLayout.LINE_END">
			<ToggleButton id="fullScreenToggleButton" onActionPerformed="handler.toggleFullScreen(tableauBasPanel, fullScreenToggleButton)"/>
		</JPanel>
	</JPanel>

</JPanel>