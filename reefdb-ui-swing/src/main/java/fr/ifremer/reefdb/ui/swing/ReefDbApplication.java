package fr.ifremer.reefdb.ui.swing;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfiguration;
import fr.ifremer.quadrige3.ui.swing.Application;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIContext;
import fr.ifremer.quadrige3.ui.swing.plaf.FileChooserUI;
import fr.ifremer.quadrige3.ui.swing.plaf.WiderSynthComboBoxUI;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.ui.swing.action.StartAction;
import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUI;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.util.PaintUtils;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.UIResource;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Arrays;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbApplication class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class ReefDbApplication extends Application {

    /* Logger */
    private static final Log LOG = LogFactory.getLog(ReefDbApplication.class);

    private ReefDbUIContext context;
    private ReefDbConfiguration config;

    /**
     * <p>main.</p>
     *
     * @param args a {@link java.lang.String} object.
     */
    public static void main(String... args) {

        ReefDbApplication application = new ReefDbApplication();
        application.start(args);
    }

    @Override
    protected boolean init(String... args) {

        if (LOG.isInfoEnabled()) {
            LOG.info("Starting ReefDb with arguments: " + Arrays.toString(args));
        }

        // Could override config file path (useful for dev)
        String configFile = "reefdb.config";
        if (System.getProperty(configFile) != null) {
            configFile = System.getProperty(configFile);
            configFile = configFile.replaceAll("\\\\", "/");
        }

        // Create configuration
        config = new ReefDbConfiguration(configFile, args);
        ReefDbConfiguration.setInstance(config);

        // Create application context
        context = ReefDbUIContext.newContext(config);

        initLookAndFeel(context);

        // prepare context (mainly init configs, i18n)
        context.init("reefdb");

        return true;
    }

    @Override
    protected void show() {

        // Start this context
        startUI(context);
    }

    @Override
    public void restartUI() {

        initLookAndFeel(context);
        startUI(context);
    }

    @Override
    protected QuadrigeCoreConfiguration getConfig() {
        return config;
    }

    @Override
    protected ApplicationUIContext getContext() {
        return context;
    }

    private static void initLookAndFeel(ReefDbUIContext context) {
        /*
         * Override default color : http://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/_nimbusDefaults.html
         */

        // background
        UIManager.put("control", Color.WHITE);
        UIManager.put("background", Color.WHITE);
        UIManager.put("nimbusSelection", context.getConfiguration().getColorSelectedRow());
        UIManager.put("nimbusDisabledText", Color.BLACK);

        // table
        UIManager.put("Table[Disabled+Selected].textBackground", context.getConfiguration().getColorSelectedRow());
        UIManager.put("Table[Enabled+Selected].textBackground", context.getConfiguration().getColorSelectedRow());
        UIResource cellBorder = new BorderUIResource.CompoundBorderUIResource(
                new BorderUIResource.LineBorderUIResource(context.getConfiguration().getColorSelectedCell()),
                new BorderUIResource.EmptyBorderUIResource(1, 4, 1, 4));
        UIManager.put("Table.focusCellHighlightBorder", cellBorder);

        // list
        UIManager.put("List.focusCellHighlightBorder", cellBorder);

        // default selection
        UIManager.put("nimbusSelectionBackground", context.getConfiguration().getColorSelectedRow());
        UIManager.put("nimbusSelectedText", PaintUtils.computeForeground(context.getConfiguration().getColorSelectedRow()));

        // default label
        UIManager.put("TitledBorder.titleColor", context.getConfiguration().getColorThematicLabel());
        UIManager.put("thematicLabelColor", context.getConfiguration().getColorThematicLabel());

        // inactive components
        UIManager.put("TextArea[Disabled].textForeground", Color.BLACK);
        UIManager.put("TextArea[Disabled+NotInScrollPane].textForeground", Color.BLACK);
        UIManager.put("ComboBox:\"ComboBox.textField\"[Disabled].textForeground", Color.BLACK);
        UIManager.put("TextField.inactiveForeground", Color.BLACK);
        UIManager.put("FormattedTextField.inactiveForeground", Color.BLACK);

        // Prepare ui look&feel and load ui properties
        try {
            SwingUtil.initNimbusLoookAndFeel();

        } catch (Exception e) {
            // could not find nimbus look-and-feel
            if (LOG.isWarnEnabled()) {
                LOG.warn("Failed to init nimbus look and feel", e);
            }
        }

        // Add specific UI for ComboBox
        UIManager.put("ComboBoxUI", WiderSynthComboBoxUI.class.getCanonicalName());

        // Override other L&F defaults
        UIManager.getLookAndFeelDefaults().put("control", Color.WHITE);
        UIManager.getLookAndFeelDefaults().put("background", Color.WHITE);

        // Set default button order (Mantis #43543)
        UIManager.getDefaults().put("OptionPane.isYesLast", true);
        UIManager.put("FileChooserUI", FileChooserUI.class.getCanonicalName());
    }

    /**
     * <p>startUI.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.ui.swing.ReefDbUIContext} object.
     */
    public static void startUI(final ReefDbUIContext context) {

        // control screen resolution
        int recommendedWidth = context.getConfiguration().getUIRecommendedWidth();
        int recommendedHeight = context.getConfiguration().getUIRecommendedHeight();
        if (recommendedWidth > 0 && recommendedHeight > 0) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if (screenSize.getWidth() < recommendedWidth || screenSize.getHeight() < recommendedHeight) {
                context.getErrorHelper().showWarningDialog(t("reefdb.screen.resolution.warning", recommendedWidth, recommendedHeight));
            }
        }

        final ReefDbMainUI mainUI = new ReefDbMainUI(context);

        context.addMessageNotifier(mainUI.getHandler());
        context.getSwingSession().add(mainUI, true);

        SwingUtilities.invokeLater(() -> mainUI.setVisible(true));

        StartAction uiAction = context.getActionFactory().createLogicAction(mainUI.getHandler(), StartAction.class);
        context.getActionEngine().runAction(uiAction);
    }

    /**
     * <p>reloadUI.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.ui.swing.ReefDbUIContext} object.
     */
    public static void reloadUI(final ReefDbUIContext context) {

        initLookAndFeel(context);
        startUI(context);

    }


}
