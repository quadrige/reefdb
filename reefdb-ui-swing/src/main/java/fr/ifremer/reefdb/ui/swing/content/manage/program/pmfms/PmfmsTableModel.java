package fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.*;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.Optional;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class PmfmsTableModel extends AbstractReefDbTableModel<PmfmsTableRowModel> {

    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> PMFM_ID = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_ID,
            n("reefdb.property.pmfm.id"),
            n("reefdb.property.pmfm.id"),
            Integer.class);

    /**
     * Identifiant pour la colonne libelle parametre.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> PMFM_NAME = ReefDbColumnIdentifier.newPmfmNameId(
            PmfmsTableRowModel.PROPERTY_PMFM,
            n("reefdb.property.pmfm.name"),
            n("reefdb.program.pmfm.name.tip"));

    /**
     * Identifiant pour la colonne code parametre.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> PARAMETER_CODE = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_PARAMETER,
            n("reefdb.property.pmfm.parameter.code"),
            n("reefdb.program.pmfm.parameter.code.tip"),
            ParameterDTO.class);

    /**
     * Identifiant pour la colonne support.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> MATRIX = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_MATRIX,
            n("reefdb.property.pmfm.matrix"),
            n("reefdb.program.pmfm.matrix"),
            MatrixDTO.class);

    /**
     * Identifiant pour la colonne fraction.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> FRACTION = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_FRACTION,
            n("reefdb.property.pmfm.fraction"),
            n("reefdb.program.pmfm.fraction.tip"),
            FractionDTO.class);

    /**
     * Identifiant pour la colonne methode.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> METHOD = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_METHOD,
            n("reefdb.property.pmfm.method"),
            n("reefdb.program.pmfm.method.tip"),
            MethodDTO.class);

    /**
     * Identifiant pour la colonne unite.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> UNIT = ReefDbColumnIdentifier.newReadOnlyId(
            PmfmsTableRowModel.PROPERTY_PMFM + '.' + PmfmDTO.PROPERTY_UNIT,
            n("reefdb.property.pmfm.unit"),
            n("reefdb.program.pmfm.unit.tip"),
            UnitDTO.class);

    /**
     * Identifiant pour la colonne analyste.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> ANALYSTE = ReefDbColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_ANALYSIS_DEPARTMENT,
            n("reefdb.property.analyst"),
            n("reefdb.program.pmfm.analyst.tip"),
            DepartmentDTO.class);

    /**
     * Identifiant pour la colonne passage.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> SURVEY = ReefDbColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_SURVEY,
            n("reefdb.program.pmfm.survey.short"),
            n("reefdb.program.pmfm.survey.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne prelevement.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> SAMPLING = ReefDbColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_SAMPLING,
            n("reefdb.program.pmfm.samplingOperation.short"),
            n("reefdb.program.pmfm.samplingOperation.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne regroupement.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> GROUPING = ReefDbColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_GROUPING,
            n("reefdb.program.pmfm.grouping.short"),
            n("reefdb.program.pmfm.grouping.tip"),
            Boolean.class);

    /**
     * Identifiant pour la colonne unicite.
     */
    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> UNIQUE = ReefDbColumnIdentifier.newId(
            PmfmsTableRowModel.PROPERTY_UNIQUE,
            n("reefdb.program.pmfm.unique.short"),
            n("reefdb.program.pmfm.unique.tip"),
            Boolean.class);

    public static final ReefDbColumnIdentifier<PmfmsTableRowModel> QUALITATIVE_VALUES = ReefDbColumnIdentifier.newId(
        PmfmsTableRowModel.PROPERTY_QUALITATIVE_VALUES,
        n("reefdb.property.pmfm.parameter.associatedQualitativeValue"),
        n("reefdb.property.pmfm.parameter.associatedQualitativeValue"),
        QualitativeValueDTO.class,
        DecoratorService.COLLECTION_SIZE);

    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public PmfmsTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, false, false);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmsTableRowModel createNewRow() {
        return new PmfmsTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<PmfmsTableRowModel> getFirstColumnEditing() {
        return ANALYSTE;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, ColumnIdentifier<PmfmsTableRowModel> propertyName) {
        if (propertyName.equals(QUALITATIVE_VALUES)) {
            PmfmsTableRowModel row = getEntry(rowIndex);
            return Optional.ofNullable(row.getPmfm()).map(PmfmDTO::getParameter).map(ParameterDTO::isQualitative).orElse(false);
        } else {
            return super.isCellEditable(rowIndex, columnIndex, propertyName);
        }
    }
}
