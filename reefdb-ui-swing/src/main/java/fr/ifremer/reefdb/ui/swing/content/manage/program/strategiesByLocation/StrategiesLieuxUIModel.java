package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.programs.StrategiesProgrammeTableUIModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * Modele pour l administration de contextes.
 */
public class StrategiesLieuxUIModel extends AbstractEmptyUIModel<StrategiesLieuxUIModel> {
    
    private StrategiesProgrammeTableUIModel tableUIModel;

    /**
     * <p>Getter for the field <code>tableUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.programs.StrategiesProgrammeTableUIModel} object.
     */
    public StrategiesProgrammeTableUIModel getTableUIModel() {
        return tableUIModel;
    }

    /**
     * <p>Setter for the field <code>tableUIModel</code>.</p>
     *
     * @param tableUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.programs.StrategiesProgrammeTableUIModel} object.
     */
    public void setTableUIModel(StrategiesProgrammeTableUIModel tableUIModel) {
        this.tableUIModel = tableUIModel;
    }
}
