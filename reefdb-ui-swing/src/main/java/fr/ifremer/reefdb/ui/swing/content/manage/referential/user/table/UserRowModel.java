/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>UserRowModel class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class UserRowModel extends AbstractReefDbRowUIModel<PersonDTO, UserRowModel> implements PersonDTO, ErrorAware {

    private static final Binder<PersonDTO, UserRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(PersonDTO.class, UserRowModel.class);

    private static final Binder<UserRowModel, PersonDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(UserRowModel.class, PersonDTO.class);

    /** Constant <code>PROPERTY_PRIVILEGE_SIZE="privilegesSize"</code> */
    public static final String PROPERTY_PRIVILEGE_SIZE = "privilegesSize";

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for UserRowModel.</p>
     */
    public UserRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = Lists.newArrayList();
    }

    /** {@inheritDoc} */
    @Override
    protected PersonDTO newBean() {
        return ReefDbBeanFactory.newPersonDTO();
    }

    /**
     * <p>getPrivilegesSize.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPrivilegesSize() {
        return sizePrivilege();
    }

    /**
     * <p>setPrivilegesSize.</p>
     *
     * @param privilegesSize a {@link java.lang.Integer} object.
     */
    public void setPrivilegesSize(Integer privilegesSize) {
        // nothing to do here
    }

    /** {@inheritDoc} */
    @Override
    public String getFirstName() {
        return delegateObject.getFirstName();
    }

    /** {@inheritDoc} */
    @Override
    public void setFirstName(String firstname) {
        delegateObject.setFirstName(firstname);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getDepartment() {
        return delegateObject.getDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepartment(DepartmentDTO department) {
        delegateObject.setDepartment(department);
    }

    /** {@inheritDoc} */
    @Override
    public String getIntranetLogin() {
        return delegateObject.getIntranetLogin();
    }

    /** {@inheritDoc} */
    @Override
    public void setIntranetLogin(String intranetLogin) {
        delegateObject.setIntranetLogin(intranetLogin);
    }

    /** {@inheritDoc} */
    @Override
    public String getExtranetLogin() {
        return delegateObject.getExtranetLogin();
    }

    /** {@inheritDoc} */
    @Override
    public void setExtranetLogin(String extranetLogin) {
        delegateObject.setExtranetLogin(extranetLogin);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isHasPassword() {
        return delegateObject.isHasPassword();
    }

    /** {@inheritDoc} */
    @Override
    public void setHasPassword(boolean hasPassword) {
        delegateObject.setHasPassword(hasPassword);
    }

    /** {@inheritDoc} */
    @Override
    public String getNewPassword() {
        return delegateObject.getNewPassword();
    }

    /** {@inheritDoc} */
    @Override
    public void setNewPassword(String newPassword) {
        delegateObject.setNewPassword(newPassword);
    }

    /** {@inheritDoc} */
    @Override
    public String getEmail() {
        return delegateObject.getEmail();
    }

    /** {@inheritDoc} */
    @Override
    public void setEmail(String email) {
        delegateObject.setEmail(email);
    }

    /** {@inheritDoc} */
    @Override
    public String getAddress() {
        return delegateObject.getAddress();
    }

    /** {@inheritDoc} */
    @Override
    public void setAddress(String address) {
        delegateObject.setAddress(address);
    }

    /** {@inheritDoc} */
    @Override
    public String getPhone() {
        return delegateObject.getPhone();
    }

    /** {@inheritDoc} */
    @Override
    public void setPhone(String phone) {
        delegateObject.setPhone(phone);
    }

    /** {@inheritDoc} */
    @Override
    public String getOrganism() {
        return delegateObject.getOrganism();
    }

    /** {@inheritDoc} */
    @Override
    public void setOrganism(String organism) {
        delegateObject.setOrganism(organism);
    }

    /** {@inheritDoc} */
    @Override
    public String getAdminCenter() {
        return delegateObject.getAdminCenter();
    }

    /** {@inheritDoc} */
    @Override
    public void setAdminCenter(String adminCenter) {
        delegateObject.setAdminCenter(adminCenter);
    }

    /** {@inheritDoc} */
    @Override
    public String getRegCode() {
        return delegateObject.getRegCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setRegCode(String regCode) {
        delegateObject.setRegCode(regCode);
    }

    /** {@inheritDoc} */
    @Override
    public String getSite() {
        return delegateObject.getSite();
    }

    /** {@inheritDoc} */
    @Override
    public void setSite(String site) {
        delegateObject.setSite(site);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public PrivilegeDTO getPrivilege(int index) {
        return delegateObject.getPrivilege(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPrivilegeEmpty() {
        return delegateObject.isPrivilegeEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizePrivilege() {
        return delegateObject.sizePrivilege();
    }

    /** {@inheritDoc} */
    @Override
    public void addPrivilege(PrivilegeDTO privilege) {
        delegateObject.addPrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllPrivilege(Collection<PrivilegeDTO> privilege) {
        delegateObject.addAllPrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removePrivilege(PrivilegeDTO privilege) {
        return delegateObject.removePrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllPrivilege(Collection<PrivilegeDTO> privilege) {
        return delegateObject.removeAllPrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsPrivilege(PrivilegeDTO privilege) {
        return delegateObject.containsPrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllPrivilege(Collection<PrivilegeDTO> privilege) {
        return delegateObject.containsAllPrivilege(privilege);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PrivilegeDTO> getPrivilege() {
        return delegateObject.getPrivilege();
    }

    /** {@inheritDoc} */
    @Override
    public void setPrivilege(Collection<PrivilegeDTO> privilege) {
        Integer oldValue = sizePrivilege();
        delegateObject.setPrivilege(privilege);
        firePropertyChange(PROPERTY_PRIVILEGE_SIZE, oldValue, sizePrivilege());
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
