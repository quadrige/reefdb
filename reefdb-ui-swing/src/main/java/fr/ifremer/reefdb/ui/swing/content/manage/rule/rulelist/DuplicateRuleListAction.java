package fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import static org.nuiton.i18n.I18n.t;

/**
 * Rule list duplicate action
 */
public class DuplicateRuleListAction extends AbstractReefDbAction<RuleListUIModel, RuleListUI, RuleListUIHandler> {

    private String newCode;
    private boolean duplicateControlRules;
    private RuleListRowModel newRow;

	/**
	 * Constructor.
	 *
	 * @param handler the handler
	 */
	public DuplicateRuleListAction(RuleListUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public boolean prepareAction() throws Exception {
		if (!super.prepareAction() || getModel().getSelectedRows().size() != 1) {
            return false;
        }

        String title = t("reefdb.action.duplicate.rule.title");
        String message = t("reefdb.action.duplicate.rule.message");
        JTextField codeField = new JTextField();

        JCheckBox checkBox = new JCheckBox(t("reefdb.action.duplicate.rule.controlRules"));
        Object[] messageObjects = {message, codeField, checkBox};

        int result = getContext().getDialogHelper().showOptionDialog(getUI(), messageObjects, title, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.NO_OPTION || result == JOptionPane.CLOSED_OPTION) {
            return false;
        }

        duplicateControlRules = checkBox.isSelected();
        newCode = codeField.getText();

        // check code duplicates
        return getHandler().checkCodeDuplicates(newCode);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() {

        RuleListRowModel selectedRuleList = getModel().getSingleSelectedRow();
        if (selectedRuleList != null) {

            RuleListDTO duplicatedRuleList = getContext().getRuleListService().duplicateRuleList(selectedRuleList.toBean(), newCode, duplicateControlRules);

            newRow = getModel().addNewRow(duplicatedRuleList);

            newRow.setDirty(true);
            newRow.setLocalEditable(false);
        }
	}

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        getModel().setModify(true);

        getHandler().setFocusOnCell(newRow);
    }
}
