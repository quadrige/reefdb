package fr.ifremer.reefdb.ui.swing.content.manage.program.strategies;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import javax.swing.SwingWorker;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class StrategiesTableUIHandler extends AbstractReefDbTableUIHandler<StrategiesTableRowModel, StrategiesTableUIModel, StrategiesTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(StrategiesTableUIHandler.class);

    private AppliedStrategyLoader appliedStrategyLoader;
    private PmfmStrategyLoader pmfmStrategyLoader;

    /**
     * <p>Constructor for StrategiesTableUIHandler.</p>
     */
    public StrategiesTableUIHandler() {
        super(StrategiesTableRowModel.PROPERTY_NAME,
                StrategiesTableRowModel.PROPERTY_COMMENT);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<StrategiesTableRowModel> getTableModel() {
        return (StrategiesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getStrategiesTable();
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final StrategiesTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final StrategiesTableUIModel model = new StrategiesTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(StrategiesTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initTable();
        SwingUtil.setLayerUI(ui.getTableScrollPane(), ui.getTableBlockLayer());

        // Ajout des listeners
        initListeners();

    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(final StrategiesTableRowModel row) {
        row.getErrors().clear();
        return (!row.isEditable() || super.isRowValid(row)) && isStrategyValid(row);
    }

    private boolean isStrategyValid(StrategiesTableRowModel row) {

        // no need to check duplicates if number of rows < 2
        if (getModel().getRowCount() >= 2) {

            for (StrategiesTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equals(otherRow.getName())) {
                    // duplicate found
                    ReefDbBeans.addError(row, t("reefdb.program.strategy.name.duplicates"), StrategiesTableRowModel.PROPERTY_NAME);
                }
            }
        }

        if (row.isAppliedStrategiesLoaded()) {

            Map<Integer, AppliedStrategyDTO> validAppliedStrategies = Maps.newHashMap();

            // check at least 1 applied strategy has date range
            boolean isStrategyApplied = false;
            if (!row.isAppliedStrategiesEmpty()) {
                for (AppliedStrategyDTO appliedStrategy : row.getAppliedStrategies()) {
                    if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null) {
                        isStrategyApplied = true;
                        validAppliedStrategies.put(appliedStrategy.getId(), appliedStrategy);
                    }
                }
            }
            if (!isStrategyApplied) {
                ReefDbBeans.addError(row, t("reefdb.programs.validation.error.noAppliedPeriod"), StrategiesTableRowModel.PROPERTY_NAME);
            } else {

                // check there is no crossing strategies
                Multimap<Integer, AppliedStrategyDTO> otherAppliedStrategiesByLocationId = ArrayListMultimap.create();
                for (StrategiesTableRowModel otherRow : getModel().getRows()) {

                    if (otherRow == row) continue;

                    // build map of other applied strategies not in this row
                    for (AppliedStrategyDTO appliedStrategy : otherRow.getAppliedStrategies()) {
                        if (validAppliedStrategies.keySet().contains(appliedStrategy.getId()) && appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null) {
                            otherAppliedStrategiesByLocationId.put(appliedStrategy.getId(), appliedStrategy);
                        }
                    }
                }

                // exploit map to found overlapping
                for (Integer appliedStrategyId : validAppliedStrategies.keySet()) {
                    boolean overlappingFound = false;
                    AppliedStrategyDTO appliedStrategy = validAppliedStrategies.get(appliedStrategyId);
                    Collection<AppliedStrategyDTO> otherAppliedStrategies = otherAppliedStrategiesByLocationId.get(appliedStrategyId);
                    for (AppliedStrategyDTO otherAppliedStrategy : otherAppliedStrategies) {

                        if (appliedStrategy.getStartDate().compareTo(otherAppliedStrategy.getEndDate()) <= 0
                                && otherAppliedStrategy.getStartDate().compareTo(appliedStrategy.getEndDate()) <= 0) {

                            // overlapping found
                            overlappingFound = true;
                            break;
                        }

                    }

                    if (overlappingFound) {
                        ReefDbBeans.addError(row, t("reefdb.programs.validation.error.overlappingPeriods"), StrategiesTableRowModel.PROPERTY_NAME);
                        break;
                    }
                }

                // Check other applied period errors
                for (AppliedStrategyDTO appliedStrategy : row.getAppliedStrategies()) {
                    if (!appliedStrategy.isErrorsEmpty()) {
                        ReefDbBeans.addError(row, t("reefdb.programs.validation.error.appliedStrategies"), StrategiesTableRowModel.PROPERTY_NAME);
                    }
                }

            }
        }

        // check pmfm integrity
        if (row.isPmfmStrategiesLoaded()) {

            for (PmfmStrategyDTO pmfmStrategy: row.getPmfmStrategies()) {
                if (!pmfmStrategy.isSurvey() && !pmfmStrategy.isSampling()) {
                    ReefDbBeans.addError(row, t("reefdb.program.tables.error"), StrategiesTableRowModel.PROPERTY_NAME);
                }

            }
        }

        return row.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, StrategiesTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        saveToProgram();
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{StrategiesTableRowModel.PROPERTY_APPLIED_STRATEGIES,
                StrategiesTableRowModel.PROPERTY_APPLIED_STRATEGIES_LOADED,
                StrategiesTableRowModel.PROPERTY_PMFM_STRATEGIES,
                StrategiesTableRowModel.PROPERTY_PMFM_STRATEGIES_LOADED,
                StrategiesTableRowModel.PROPERTY_ERRORS};
    }

    /**
     * Load strategies from program
     *
     * @param selectedProgram a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel} object.
     */
    public void load(ProgramsTableRowModel selectedProgram) {

        getModel().setProgramStatus(selectedProgram.getStatus());
        getModel().setBeans(selectedProgram.getStrategies());
        getModel().setEditable(selectedProgram.isEditable());
        getModel().setLoading(false);
        getModel().setLoaded(true);
        recomputeRowsValidState();
    }

    /**
     * Clear table
     */
    public void clearTable() {

        getModel().setBeans(null);
        getModel().setLoaded(false);
    }

    private ProgramsUI getProgramsUI() {
        return getUI().getParentContainer(ProgramsUI.class);
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(StrategiesTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            final ProgramsUI adminProgrammeUI = getProgramsUI();

            // Get selected program and strategies
            final ProgramsTableRowModel program = adminProgrammeUI.getProgramsTableUI().getModel().getSingleSelectedRow();
            final StrategiesTableRowModel strategy = getModel().getSingleSelectedRow();

            // If only once selected
            if (program != null && strategy != null) {

                // load applied strategies
                if (appliedStrategyLoader != null && !appliedStrategyLoader.isDone()) {
                    appliedStrategyLoader.cancel(true);
                }
                adminProgrammeUI.getLocationsTableUI().getModel().setLoading(true);
                appliedStrategyLoader = new AppliedStrategyLoader(program, strategy);
                appliedStrategyLoader.execute();

                // load pmfms strategies
                if (pmfmStrategyLoader != null && !pmfmStrategyLoader.isDone()) {
                    pmfmStrategyLoader.cancel(true);
                }
                adminProgrammeUI.getPmfmsTableUI().getModel().setLoading(true);
                pmfmStrategyLoader = new PmfmStrategyLoader(program, strategy);
                pmfmStrategyLoader.execute();

            } else {

                // Suppression des PSFMs
                adminProgrammeUI.getPmfmsTableUI().getHandler().clearTable();
            }
        });
    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Colonne libelle
        final TableColumnExt colonneLibelle = addColumn(
                StrategiesTableModel.NAME);
        colonneLibelle.setSortable(true);

        // Colonne description
        final TableColumnExt colonneDescription = addCommentColumn(StrategiesTableModel.COMMENT);
        colonneDescription.setSortable(false);

        // Modele de la table
        final StrategiesTableModel tableModel = new StrategiesTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Les colonnes obligatoire sont toujours presentes
        colonneLibelle.setHideable(false);
        colonneDescription.setHideable(false);

        // Initialisation de la table
        initTable(getTable());

        // Number rows visible
        getTable().setVisibleRowCount(4);

        // Tri par defaut
        getTable().setSortOrder(StrategiesTableModel.NAME, SortOrder.ASCENDING);
    }

    /**
     * Save update (data in memory) on slave table in master table
     */
    public void keepModificationOnPmfmsTable() {

        getModel().getSingleSelectedRow().setPmfmStrategies(getProgramsUI().getPmfmsTableUI().getModel().getBeans());
        getModel().getSingleSelectedRow().setPmfmStrategiesLoaded(true);

        // tell also applied strategies tables to refresh rows validity
        getProgramsUI().getLocationsTableUI().getHandler().recomputeRowsValidState();
    }

    /**
     * <p>keepModificationOnLocationsTable.</p>
     */
    public void keepModificationOnLocationsTable() {

        if (getModel().getSingleSelectedRow() != null) {
            getModel().getSingleSelectedRow().setAppliedStrategies(getProgramsUI().getLocationsTableUI().getModel().getBeans());
            getModel().getSingleSelectedRow().setAppliedStrategiesLoaded(true);
        }
    }

    /**
     * <p>saveToProgram.</p>
     */
    public void saveToProgram() {

        if (getModel().isLoading()) return;

        // programs contains strategies, strategies contains pmfm
        // save modification on master object
        getProgramsUI().getProgramsTableUI().getHandler().keepModificationOnStrategiesTable();

        // force model modify
        recomputeRowsValidState();
        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);

    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<StrategiesTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            StrategiesTableRowModel row = addedRows.get(0);

            getModel().setModify(true);

            setFocusOnCell(row);

            saveToProgram();
        }
    }

    /**
     * <p>removeStrategies.</p>
     */
    public void removeStrategies() {

        if (getModel().getSelectedRows().isEmpty()) {
            return;
        }

        if (!checkExistingSurveysInsideRemovedStrategy()) {

            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.program.location.period.surveyInsidePeriod"),
                    t("reefdb.program.strategy.delete.titre")
            );

            return;
        }

        // Demande de confirmation avant la suppression
        if (askBeforeDelete(t("reefdb.program.strategy.delete.titre"), t("reefdb.program.strategy.delete.message"))) {

            // Suppression des lignes
            getModel().deleteSelectedRows();

            // Suppression des psfms du tableau
            getProgramsUI().getPmfmsTableUI().getHandler().clearTable();

            saveToProgram();

            // Reselect the program to refresh location/applied strategies table
            getProgramsUI().getHandler().reselectProgram();

        }

    }

    /**
     * check if strategies to delete have data inside their applied periods (Mantis #45902)
     *
     * @return true if no data inside strategies, delete is allowed
     */
    private boolean checkExistingSurveysInsideRemovedStrategy() {

        getModel().setLoading(true);
        try {

            List<AppliedStrategyDTO> appliedStrategies = getModel().getSelectedBeans().stream()
                    .filter(strategyDTO -> strategyDTO.getId() != null)
                    .flatMap(strategyDTO -> strategyDTO.getAppliedStrategies().stream())
                    .filter(appliedStrategyDTO -> appliedStrategyDTO.getStartDate() != null && appliedStrategyDTO.getEndDate() != null)
                    .collect(Collectors.toList());

            if (appliedStrategies.isEmpty()) return true;

            String programCode = getProgramsUI().getProgramsTableUI().getModel().getSingleSelectedRow().getCode();

            return appliedStrategies.stream().noneMatch(appliedStrategy ->
                    getContext().getObservationService().countSurveysWithProgramLocationAndInsideDates(
                            programCode,
                            appliedStrategy.getAppliedStrategyId(),
                            appliedStrategy.getId(),
                            appliedStrategy.getStartDate(),
                            appliedStrategy.getEndDate()
                    ) > 0
            );

        } finally {
            getModel().setLoading(false);
        }
    }

    private class AppliedStrategyLoader extends SwingWorker<Object, Object> {

        private final ProgramsUI adminProgrammeUI = getProgramsUI();
        private final ProgramsTableRowModel selectedProgram;
        private final StrategiesTableRowModel selectedStrategy;

        public AppliedStrategyLoader(ProgramsTableRowModel selectedProgram, StrategiesTableRowModel selectedStrategy) {
            this.selectedProgram = selectedProgram;
            this.selectedStrategy = selectedStrategy;
        }

        @Override
        protected Object doInBackground() {

            // Make sure applied strategies are loaded into the selected strategy
            getContext().getProgramStrategyService().loadAppliedPeriods(selectedProgram, selectedStrategy);

            return null;
        }

        @Override
        protected void done() {
            if (isCancelled()) {
                return;
            }
            try {
                get();
            } catch (InterruptedException | ExecutionException e) {
                throw new ReefDbTechnicalException(e.getMessage(), e);
            }

            // loading locations
            adminProgrammeUI.getLocationsTableUI().getHandler().loadAppliedStrategiesFromStrategy(selectedProgram, selectedStrategy);

        }
    }

    private class PmfmStrategyLoader extends SwingWorker<Object, Object> {

        private final ProgramsUI adminProgrammeUI = getUI().getParentContainer(ProgramsUI.class);
        private final ProgramsTableRowModel selectedProgram;
        private final StrategiesTableRowModel selectedStrategy;

        public PmfmStrategyLoader(ProgramsTableRowModel selectedProgram, StrategiesTableRowModel selectedStrategy) {
            this.selectedStrategy = selectedStrategy;
            this.selectedProgram = selectedProgram;
        }

        @Override
        protected Object doInBackground() {

            // Make sure linked pmfm are loaded into the selected strategy
            getContext().getProgramStrategyService().loadPmfmStrategy(selectedStrategy);

            return null;
        }

        @Override
        protected void done() {
            if (isCancelled()) {
                return;
            }
            try {
                get();
            } catch (InterruptedException | ExecutionException e) {
                throw new ReefDbTechnicalException(e.getMessage(), e);
            }

            // loading pmfms
            adminProgrammeUI.getPmfmsTableUI().getHandler().load(selectedProgram, selectedStrategy);

        }
    }
}
