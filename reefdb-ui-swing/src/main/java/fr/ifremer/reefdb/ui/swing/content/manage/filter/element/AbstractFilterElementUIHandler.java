package fr.ifremer.reefdb.ui.swing.content.manage.filter.element;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * Controler.
 *
 * @param <E>
 * @param <UI>
 */
public abstract class AbstractFilterElementUIHandler<E extends BaseReferentialDTO, UI extends FilterElementUI<E>, MENU extends ReferentialMenuUI>
        extends AbstractReefDbUIHandler<FilterElementUIModel, UI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(AbstractFilterElementUIHandler.class);

    private MENU referentialMenuUI;

    /**
     * method to override to obtain specific filter element menu
     *
     * @return the referential menu
     */
    protected abstract MENU createNewReferentialMenuUI();

    /**
     * <p>Getter for the field <code>referentialMenuUI</code>.</p>
     *
     * @return a MENU object.
     */
    public final MENU getReferentialMenuUI() {

        if (referentialMenuUI == null) {
            referentialMenuUI = createNewReferentialMenuUI();
        }

        return referentialMenuUI;

    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final UI ui) {
        super.beforeInit(ui);

        // Create model and register to the JAXX context directly into ui.model
        ui.model = new FilterElementUIModel<>();
        ui.setContextValue(ui.model);
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(UI ui) {

        initUI(ui);

        // set color here because jaxx don't recognize 'handler'
        ui.getListBlockLayer().setBlockingColor(getConfig().getColorBlockingLayer());
        SwingUtil.setLayerUI(ui.getFilterElementPanel(), ui.getListBlockLayer());
        ui.applyDataBinding(FilterElementUI.BINDING_LIST_BLOCK_LAYER_BLOCK);

        // Init double list
        initBeanList(
                getUI().getFilterDoubleList(),
                new ArrayList<>(),
                new ArrayList<>(),
                ReefDbUIs.REEFDB_DOUBLE_LIST_SIZE);

        // Reaffect the model
        getUI().getFilterDoubleList().setBean(getModel());

        // init menu UI
        MENU menuUI = getReferentialMenuUI();
        Assert.isInstanceOf(Component.class, menuUI);
        getUI().getFilterElementMenuPanel().add("menuUI", (Component) menuUI);
        getUI().get$objectMap().put("menuUI", menuUI);

        // init listener on filtered elements
        menuUI.getHandler().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            List<E> elements = (List<E>) evt.getNewValue();

            // must filter referential list depending on 'local' property
            if (getReferentialMenuUI().getModel().isForceStatus()) {
                elements = ReefDbBeans.filterReferential(elements, getReferentialMenuUI().getModel().isLocal());
            }

            loadAvailableElements(elements);
        });

        // listen to found results
        menuUI.getModel().addPropertyChangeListener(DefaultReferentialMenuUIModel.PROPERTY_RESULTS, evt -> loadAvailableElements((List<E>) evt.getNewValue()));

        disable();
    }

    /**
     * <p>forceLocal.</p>
     *
     * @param local a {@link java.lang.Boolean} object.
     */
    public void forceLocal(Boolean local) {

        getReferentialMenuUI().getHandler().forceLocal(local);
    }

    /**
     * <p>enable.</p>
     */
    public void enable() {

        // Activate search
        getReferentialMenuUI().getHandler().enableSearch(true);

        // Activation de la double liste
        getUI().getFilterDoubleList().setEnabled(true);

    }

    /**
     * <p>disable.</p>
     */
    public void disable() {

        // De activate search components
        getReferentialMenuUI().getHandler().enableSearch(false);

        // Deactivate double list
        getUI().getFilterDoubleList().setEnabled(false);

    }

    /**
     * Load element.
     * TODO rename
     *
     * @param elements Element to load
     */
    @SuppressWarnings("unchecked")
    public void loadSelectedElements(final List<E> elements) {

        enable();

        getModel().setElements(elements);
        getModel().setAdjusting(true);
        getUI().getFilterDoubleList().getHandler().setSelected(elements != null ? new ArrayList<>(elements) : null);
        getModel().setAdjusting(false);
    }

    /**
     * Load elements.
     * TODO rename
     *
     * @param elements Element to load
     */
    @SuppressWarnings("unchecked")
    public void loadAvailableElements(final List<E> elements) {
        if (getModel().isAdjusting()) {
            return;
        }

        // affect new universe list
        getUI().getFilterDoubleList().getHandler().setUniverse(elements);

        // re affect select elements by invokeLater because of concurrent access to universe list
        getModel().setAdjusting(true);
        SwingUtilities.invokeLater(() -> {
            getUI().getFilterDoubleList().getHandler().setSelected(getModel().getElements() != null ? new ArrayList<>(getModel().getElements()) : null);
            getModel().setAdjusting(false);
        });
    }

    /**
     * Delete filters.
     */
    public void clear() {

        disable();

        // Clear menu model
        getReferentialMenuUI().getModel().clear();

        // Clear list on double list
        getUI().getFilterDoubleList().getHandler().setUniverse(null);
        getUI().getFilterDoubleList().getHandler().setSelected(null);
    }

}
