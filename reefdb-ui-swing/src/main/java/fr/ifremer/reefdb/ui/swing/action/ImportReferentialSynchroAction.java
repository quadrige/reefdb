package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.content.ReefDbMainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ImportReferentialSynchroAction class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class ImportReferentialSynchroAction extends ImportSynchroAction {

    /**
     * <p>Constructor for ImportReferentialSynchroAction.</p>
     *
     * @param handler a {@link ReefDbMainUIHandler} object.
     */
    public ImportReferentialSynchroAction(ReefDbMainUIHandler handler) {
        super(handler);
        setActionDescription(t("reefdb.action.synchro.import.referential.title"));
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        // referential ONLY
        setReferentialOnly(true);

        return super.prepareAction();
    }

}
