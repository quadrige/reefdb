package fr.ifremer.reefdb.ui.swing.content.manage.rule.department;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlDepartmentTableModel extends AbstractReefDbTableModel<ControlDepartmentTableRowModel> {

	/** Constant <code>CODE</code> */
	public static final ReefDbColumnIdentifier<ControlDepartmentTableRowModel> CODE = ReefDbColumnIdentifier.newId(
			ControlDepartmentTableRowModel.PROPERTY_CODE,
			n("reefdb.property.code"),
			n("reefdb.rule.department.code.tip"),
			String.class);
	
	/** Constant <code>NAME</code> */
	public static final ReefDbColumnIdentifier<ControlDepartmentTableRowModel> NAME = ReefDbColumnIdentifier.newId(
			ControlDepartmentTableRowModel.PROPERTY_NAME,
			n("reefdb.property.name"),
			n("reefdb.rule.department.name.tip"),
			String.class);
	
	/**
	 * <p>Constructor for ControlDepartmentTableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	ControlDepartmentTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ControlDepartmentTableRowModel createNewRow() {
		return new ControlDepartmentTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ControlDepartmentTableRowModel> getFirstColumnEditing() {
		return CODE;
	}
}
