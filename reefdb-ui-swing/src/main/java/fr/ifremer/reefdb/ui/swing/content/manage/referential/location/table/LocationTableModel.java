package fr.ifremer.reefdb.ui.swing.content.manage.referential.location.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.HarbourDTO;
import fr.ifremer.reefdb.dto.referential.PositioningSystemDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>LocationTableModel class.</p>
 *
 * @author Antoine
 */
public class LocationTableModel extends AbstractReefDbTableModel<LocationTableRowModel> {

    private final boolean readOnly;

    /** Constant <code>ID</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> ID = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_ID,
            n("reefdb.property.id"),
            n("reefdb.property.id.tip"),
            Integer.class);

    /** Constant <code>LABEL</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> LABEL = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_LABEL,
            n("reefdb.property.label"),
            n("reefdb.property.label"),
            String.class);

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class,
            true);

    /** Constant <code>BATHYMETRIE</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> BATHYMETRIE = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_BATHYMETRY,
            n("reefdb.property.location.bathymetry"),
            n("reefdb.property.location.bathymetry"),
            Double.class);

    /** Constant <code>LATITUDE_MIN</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> LATITUDE_MIN = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MIN_LATITUDE,
            n("reefdb.property.location.latitude.min"),
            n("reefdb.property.location.latitude.min"),
            Double.class,
            true);

    /** Constant <code>LONGITUDE_MIN</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> LONGITUDE_MIN = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MIN_LONGITUDE,
            n("reefdb.property.location.longitude.min"),
            n("reefdb.property.location.longitude.min"),
            Double.class,
            true);

    /** Constant <code>COMMENT</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.comment"),
            n("reefdb.property.comment"),
            String.class);

    /** Constant <code>HARBOUR</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> HARBOUR = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_HARBOUR,
            n("reefdb.property.location.harbour"),
            n("reefdb.property.location.harbour"),
            HarbourDTO.class);

    /** Constant <code>DELTA_UT_HIVER</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> DELTA_UT_HIVER = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_UT_FORMAT,
            n("reefdb.property.location.deltaUT"),
            n("reefdb.property.location.deltaUT"),
            Double.class);

    /** Constant <code>DAYLIGHT_SAVING_TIME</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> DAYLIGHT_SAVING_TIME = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_DAY_LIGHT_SAVING_TIME,
            n("reefdb.property.location.daylightSavingTime"),
            n("reefdb.property.location.daylightSavingTime"),
            Boolean.class);

    /** Constant <code>LATITUDE_MAX</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> LATITUDE_MAX = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MAX_LATITUDE,
            n("reefdb.property.location.latitude.max"),
            n("reefdb.property.location.latitude.max"),
            Double.class);

    /** Constant <code>LONGITUDE_MAX</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> LONGITUDE_MAX = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_MAX_LONGITUDE,
            n("reefdb.property.location.longitude.max"),
            n("reefdb.property.location.longitude.max"),
            Double.class);

    /** Constant <code>POSITIONING_NAME</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> POSITIONING_NAME = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_POSITIONING,
            n("reefdb.property.location.positioning.name"),
            n("reefdb.property.location.positioning.name"),
            PositioningSystemDTO.class,
            true);

    /** Constant <code>POSITIONING_PRECISION</code> */
    public static final ReefDbColumnIdentifier<LocationTableRowModel> POSITIONING_PRECISION = ReefDbColumnIdentifier.newId(
            LocationTableRowModel.PROPERTY_POSITIONING_PRECISION,
            n("reefdb.property.location.positioning.precision"),
            n("reefdb.property.location.positioning.precision"),
            String.class,
            true);

    public static final ReefDbColumnIdentifier<LocationTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        LocationTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<LocationTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        LocationTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);

    public static final ReefDbColumnIdentifier<LocationTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
        LocationTableRowModel.PROPERTY_STATUS,
        n("reefdb.property.status"),
        n("reefdb.property.status"),
        StatusDTO.class, true);
    
//	public static final ReefDbColumnIdentifier<GererLieuxNationalTableUIRowModel> POSITIONING_PRECISION = ReefDbColumnIdentifier.newId(
//			LocationTableRowModel.PROPERTY_POSITIONING,
//			n("reefdb.ui.swing.content.config.gerer.lieux.national.table.positionnementPrecision.short"),
//			n("reefdb.ui.swing.content.config.gerer.lieux.national.table.positionnementPrecision.tip"),
//			PositioningSystemDTO.class, DecoratorService.PRECISION);

//	public static final ReefDbColumnIdentifier<GererLieuxNationalTableUIRowModel> FICHE_LIEU = ReefDbColumnIdentifier.newId(
//			LocationTableRowModel.PROPERTY_FICHE_LIEU,
//			n("reefdb.ui.swing.content.config.gerer.lieux.national.table.ficheLieu.short"),
//			n("reefdb.ui.swing.content.config.gerer.lieux.national.table.ficheLieu.tip"),
//			String.class);

    /**
     * <p>Constructor for LocationTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param createNewRowAllowed a boolean.
     */
    public LocationTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
        this.readOnly = !createNewRowAllowed;
    }

    /** {@inheritDoc} */
    @Override
    public LocationTableRowModel createNewRow() {
        return new LocationTableRowModel(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<LocationTableRowModel> getFirstColumnEditing() {
        return LABEL;
    }
}
