package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * Duplicate survey Action
 */
public class DuplicateSurveyAction extends AbstractReefDbAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

	private SurveyDTO duplicatedObservation;

	private boolean fullDuplication;
    private boolean duplicateCoordinate;
    private boolean duplicateSurveyMeasurements;

	/**
	 * Constructor.
	 *
	 * @param handler the handler
	 */
	public DuplicateSurveyAction(SurveysTableUIHandler handler) {
		super(handler, false);
	}

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction() || getModel().getSelectedRows().size() != 1) {
            return false;
        }

        // Change message and buttons (Mantis #50011)
        String title = t("reefdb.home.survey.duplicate.title");
        JCheckBox duplicateCoordinateCheckBox = new JCheckBox(t("reefdb.home.survey.duplicate.coordinate"));
        JCheckBox duplicateSurveyMeasurementsCheckBox = new JCheckBox(t("reefdb.home.survey.duplicate.surveyMeasurements"));
        Object[] messageObjects = {/*message, */duplicateCoordinateCheckBox, duplicateSurveyMeasurementsCheckBox};

        Object[] options = {
                t("reefdb.home.survey.duplicate.option.full"),
                t("reefdb.home.survey.duplicate.option.simple"),
                t("reefdb.common.cancel")};
        int result = getContext().getDialogHelper().showOptionDialog(getUI(), messageObjects, title, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION, options, options[0]);

        if (result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION) {
            return false;
        }

        fullDuplication = result == 0;
        duplicateCoordinate = duplicateCoordinateCheckBox.isSelected();
        duplicateSurveyMeasurements = duplicateSurveyMeasurementsCheckBox.isSelected();

        return true;
    }

    /** {@inheritDoc} */
    @Override
	public void doAction() {
		
		// Selected observation
		final SurveysTableRowModel surveysTableRowModel = getModel().getSelectedRows().iterator().next();
		if (surveysTableRowModel != null) {
			
			// Duplicate observation
			duplicatedObservation = getContext().getObservationService().duplicateSurvey(surveysTableRowModel.toBean(), fullDuplication, duplicateCoordinate, duplicateSurveyMeasurements);
		}
	}

	/** {@inheritDoc} */
	@Override
	public void postSuccessAction() {
		super.postSuccessAction();

		if (duplicatedObservation != null) {

			// Add duplicate observation to table
			getModel().addNewRow(duplicatedObservation);

			getModel().setModify(true);
		}
	}

	@Override
	protected void releaseAction() {
		super.releaseAction();

		duplicatedObservation = null;
	}
}
