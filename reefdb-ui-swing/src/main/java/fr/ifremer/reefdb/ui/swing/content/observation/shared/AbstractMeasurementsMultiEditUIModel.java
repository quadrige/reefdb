package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Modele pour les mesures de l'observation.
 */
public abstract class AbstractMeasurementsMultiEditUIModel<
    B extends MeasurementDTO,
    R extends AbstractMeasurementsGroupedRowModel<B, R>,
    M extends AbstractMeasurementsMultiEditUIModel<B, R, M>>
    extends AbstractMeasurementsGroupedTableUIModel<B, R, M> {

    // rows selected by caller
    Set<R> rowsToEdit;

    // pmfm ids as read-only
    List<Integer> readOnlyPmfmIds;

    public Set<R> getRowsToEdit() {
        if (rowsToEdit == null)
            rowsToEdit = new HashSet<>();
        return rowsToEdit;
    }

    public void setRowsToEdit(Set<R> rowsToEdit) {
        this.rowsToEdit = rowsToEdit;
    }

    public List<Integer> getReadOnlyPmfmIds() {
        if (readOnlyPmfmIds == null)
            readOnlyPmfmIds = new ArrayList<>();
        return readOnlyPmfmIds;
    }

    public void setReadOnlyPmfmIds(List<Integer> readOnlyPmfmIds) {
        this.readOnlyPmfmIds = readOnlyPmfmIds;
    }

    public abstract Set<ReefDbColumnIdentifier<R>> getIdentifiersToCheck();

}
