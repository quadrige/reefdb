package fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.user.PrivilegeCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.renderer.ButtonCellRenderer;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.local.password.PasswordUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.menu.UserMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.privileges.PrivilegesDialogUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table.UserRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table.UserTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.user.table.UserTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.Icon;
import javax.swing.JTable;
import java.awt.Dimension;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Users au niveau local
 */
public class ManageUsersLocalUIHandler extends AbstractReefDbTableUIHandler<UserRowModel, UserTableUIModel, ManageUsersLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageUsersLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageUsersLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final UserTableUIModel model = new UserTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageUsersLocalUI ui) {
        initUI(ui);

        // hide context filter panel
        ui.getUserMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getUserMenuUI().getHandler().forceLocal(true);

        // listen to search result
        ui.getUserMenuUI().getModel().addPropertyChangeListener(UserMenuUIModel.PROPERTY_RESULTS, evt -> loadTable((List<PersonDTO>) evt.getNewValue()));

        // Initialisation du tableau
        initTable();

        // Set the table model admin mode
        if (SecurityContextHelper.getQuadrigeUser() != null) {
            getModel().setUserId(SecurityContextHelper.getQuadrigeUserId());
            getModel().setIsAdmin(SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.LOCAL_ADMIN));
        }

        getUI().getManageUsersLocalTableDeleteBouton().setEnabled(false);
        getUI().getManageUsersLocalTableReplaceBouton().setEnabled(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // lastname
        TableColumnExt lastnameCol = addColumn(UserTableModel.LASTNAME);
        lastnameCol.setSortable(true);

        // firstname
        TableColumnExt firstnameCol = addColumn(UserTableModel.FIRSTNAME);
        firstnameCol.setSortable(true);

        // department
        TableColumnExt depCol = addFilterableComboDataColumnToModel(
                UserTableModel.DEPARTMENT,
                getContext().getReferentialService().getDepartments(),
                true);
        depCol.setSortable(true);

        // email
        TableColumnExt emailCol = addColumn(UserTableModel.EMAIL);
        emailCol.setSortable(true);

        // phone
        TableColumnExt phoneCol = addColumn(UserTableModel.PHONE);
        phoneCol.setSortable(true);

        // address
        TableColumnExt addressCol = addColumn(UserTableModel.ADDRESS);
        addressCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(UserTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(UserTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(UserTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                UserTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);

        // privilege
        TableColumnExt privilegesCol = addColumn(
                new ButtonCellEditor() {
                    @Override
                    public void onButtonCellAction(int row, int column) {
                        // show popup + set beans
                        PrivilegesDialogUI dialog = new PrivilegesDialogUI(getContext());
                        int rowModelIndex = getTable().convertRowIndexToModel(row);
                        UserRowModel rowModel = getTableModel().getEntry(rowModelIndex);
                        dialog.getModel().setUser(rowModel);
                        dialog.getModel().setEditable(getModel().isAdmin());
                        openDialog(dialog, new Dimension(600, 200));
                    }

                },
                new ButtonCellRenderer<Integer>(),
                UserTableModel.PRIVILEGES);
        privilegesCol.setMaxWidth(100);
        privilegesCol.setWidth(100);
        privilegesCol.setSortable(false);

        /* non visible cols */

        // intranet login
        TableColumnExt intranetLoginCol = addColumn(UserTableModel.INTRANET_LOGIN);
        intranetLoginCol.setSortable(true);

        // TODO crypted password
        TableColumnExt passwordCol = addColumn(
                new ButtonCellEditor() {
                    @Override
                    public void onButtonCellAction(int row, int column) {
                        // open password UI
                        UserRowModel rowModel = getTableModel().getEntry(getTable().convertRowIndexToModel(row));
                        String newPassword = new PasswordUI(getContext()).open(rowModel.getIntranetLogin()).getNewPassword();
                        // null result means user hits cancel
                        if (newPassword != null) {
                            rowModel.setNewPassword(newPassword);
                            rowModel.setHasPassword(StringUtils.isNotBlank(newPassword));
                        }
                    }
                },
                new ButtonCellRenderer(true) {
                    @Override
                    protected Icon getIcon(JTable table, Object value, int row, int column) {

                        if (value instanceof Boolean) {
                            boolean hasPassword = (boolean) value;
                            return hasPassword ? SwingUtil.createActionIcon("lock") : SwingUtil.createActionIcon("unlock");
                        }

                        return null;
                    }
                },
                UserTableModel.HAS_PASSWORD);
        passwordCol.setSortable(false);

        UserTableModel tableModel = new UserTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.users.local"), UserTableModel.PRIVILEGES, UserTableModel.HAS_PASSWORD);

        // Initialisation du tableau
        initTable(getTable());

        // optional columns are hidden
        intranetLoginCol.setVisible(false);
        passwordCol.setVisible(false);
        addressCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(UserRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isUserValid(row);
    }

    private boolean isUserValid(UserRowModel row) {

        if (!row.isHasPassword() && CollectionUtils.isNotEmpty(row.getPrivilege())) {
            for (PrivilegeDTO privilege : row.getPrivilege()) {
                if (PrivilegeCode.LOCAL_ADMINISTRATOR.getValue().equals(privilege.getCode()) || PrivilegeCode.QUALIFIER.getValue().equals(privilege.getCode())) {
                    ReefDbBeans.addError(row, t("reefdb.user.password.mandatory"), UserRowModel.PROPERTY_HAS_PASSWORD);
                }
            }
        }

        if (StringUtils.isNotBlank(row.getIntranetLogin())) {

            // check login duplicates in table
            boolean duplicateFound = false;
            for (UserRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getIntranetLogin().equalsIgnoreCase(otherRow.getIntranetLogin())) {
                    ReefDbBeans.addError(row, t("reefdb.user.existing.login", row.getIntranetLogin()), UserRowModel.PROPERTY_INTRANET_LOGIN);
                    duplicateFound = true;
                    break;
                }
            }

            // check login duplicates in db
            if (!duplicateFound) {
                PersonCriteriaDTO criteria = ReefDbBeanFactory.newPersonCriteriaDTO();
                criteria.setLogin(row.getIntranetLogin());
                List<PersonDTO> result = getContext().getUserService().searchUser(criteria);
                if (CollectionUtils.isNotEmpty(result)) {
                    for (PersonDTO existingUser : result) {
                        if (!existingUser.getId().equals(row.getId())) {
                            ReefDbBeans.addError(row, t("reefdb.user.existing.login", row.getIntranetLogin()), UserRowModel.PROPERTY_INTRANET_LOGIN);
                            break;
                        }
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(row.getName()) && StringUtils.isNotBlank(row.getFirstName())) {

            // check names duplicates in table
            boolean duplicateFound = false;
            for (UserRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName()) && row.getFirstName().equalsIgnoreCase(otherRow.getFirstName())) {
                    ReefDbBeans.addWarning(row,
                            t("reefdb.user.existing.local", decorate(otherRow.toBean())),
                            UserRowModel.PROPERTY_NAME,
                            UserRowModel.PROPERTY_FIRST_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            // check duplinates names in db
            if (!duplicateFound) {
                PersonCriteriaDTO criteria = ReefDbBeanFactory.newPersonCriteriaDTO();
                criteria.setName(row.getName());
                criteria.setFirstName(row.getFirstName());
                criteria.setStrictName(true);
                List<PersonDTO> result = getContext().getUserService().searchUser(criteria);
                if (CollectionUtils.isNotEmpty(result)) {
                    for (PersonDTO existingUser : result) {
                        if (!existingUser.getId().equals(row.getId())) {
                            ReefDbBeans.addWarning(row,
                                    ReefDbBeans.isLocalStatus(existingUser.getStatus())
                                            ? t("reefdb.user.existing.local", decorate(existingUser))
                                            : t("reefdb.user.existing.national", decorate(existingUser)),
                                    UserRowModel.PROPERTY_NAME,
                                    UserRowModel.PROPERTY_FIRST_NAME);
                            break;
                        }
                    }
                }
            }

        }

        return row.getErrors().isEmpty();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<UserRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            UserRowModel rowModel = addedRows.get(0);
            getModel().setModify(true);

            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));

            // set current department of authenticated user
            rowModel.setDepartment(getContext().getReferentialService().getDepartmentById(getContext().getDataContext().getRecorderDepartmentId()));

            setFocusOnCell(rowModel);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, UserRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<UserRowModel> getTableModel() {
        return (UserTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageUsersLocalTable();
    }

    /**
     * <p>loadTable.</p>
     *
     * @param users a {@link java.util.List} object.
     */
    public void loadTable(List<PersonDTO> users) {
        getModel().setBeans(users);
    }
}
