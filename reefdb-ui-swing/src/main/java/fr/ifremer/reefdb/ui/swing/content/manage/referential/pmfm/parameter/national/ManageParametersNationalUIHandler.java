package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.menu.ManageParametersMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.table.ParameterTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.table.ParameterTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedQualitativeValueCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedQualitativeValueCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Parameters au niveau national
 */
public class ManageParametersNationalUIHandler extends
        AbstractReefDbTableUIHandler<ParameterTableRowModel, ManageParametersNationalUIModel, ManageParametersNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageParametersNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageParametersNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageParametersNationalUIModel model = new ManageParametersNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ManageParametersNationalUI ui) {
        initUI(ui);

        ManageParametersMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(false);

        //listen to results
        menuUIModel.addPropertyChangeListener(ManageParametersMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<ParameterDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // code
        final TableColumnExt codeCol = addColumn(ParameterTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(ParameterTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(ParameterTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // qualitative
        final TableColumnExt qualitativeCol = addBooleanColumnToModel(ParameterTableModel.IS_QUALITATIVE, getTable());
        qualitativeCol.setSortable(true);
        qualitativeCol.setEditable(false);

        // calculated
        final TableColumnExt calculatedCol = addBooleanColumnToModel(ParameterTableModel.IS_CALCULATED, getTable());
        calculatedCol.setSortable(true);
        calculatedCol.setEditable(false);

        // taxonomic
        final TableColumnExt taxonomicCol = addBooleanColumnToModel(ParameterTableModel.IS_TAXONOMIC, getTable());
        taxonomicCol.setSortable(true);
        taxonomicCol.setEditable(false);

        // Associated qualitative value
        final TableColumnExt associatedQualitativeValueCol = addColumn(
                new AssociatedQualitativeValueCellEditor(getTable(), getUI(), false),
                new AssociatedQualitativeValueCellRenderer(),
                ParameterTableModel.ASSOCIATED_QUALITATIVE_VALUE);
        associatedQualitativeValueCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(ParameterTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(ParameterTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(ParameterTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                ParameterTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.NATIONAL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // parameter group
        final TableColumnExt parameterGroupCol = addFilterableComboDataColumnToModel(
                ParameterTableModel.PARAMETER_GROUP,
                getContext().getReferentialService().getParameterGroup(StatusFilter.ACTIVE),
                false);
        parameterGroupCol.setSortable(true);
        parameterGroupCol.setEditable(false);

        ParameterTableModel tableModel = new ParameterTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.parameters.national", ParameterTableModel.ASSOCIATED_QUALITATIVE_VALUE));

        // Initialisation du tableau
        initTable(getTable(), true);

        // hidden columns
        parameterGroupCol.setVisible(false);
        calculatedCol.setVisible(false);
        taxonomicCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ParameterTableRowModel> getTableModel() {
        return (ParameterTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getTable();
    }

}
