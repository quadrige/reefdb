<!--
  #%L
  Reef DB :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JPanel decorator='help' implements='fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUI&lt;DepartmentMenuUIModel, DepartmentMenuUIHandler&gt;'>
  <import>
    fr.ifremer.reefdb.ui.swing.ReefDbHelpBroker
    fr.ifremer.reefdb.ui.swing.ReefDbUIContext
    fr.ifremer.reefdb.ui.swing.util.ReefDbUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI

    fr.ifremer.reefdb.dto.referential.DepartmentDTO
    fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO
    fr.ifremer.reefdb.dto.BooleanDTO

    java.awt.FlowLayout
    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.BorderLayout
    javax.swing.SwingConstants

    fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox

    static org.nuiton.i18n.I18n.*
  </import>

  <!--getContextValue est une méthode interne JAXX-->
  <DepartmentMenuUIModel id='model' initializer='getContextValue(DepartmentMenuUIModel.class)'/>

  <ReefDbHelpBroker id='broker' constructorParams='"reefdb.home.help"'/>

  <script><![CDATA[
		//Le parent est très utile pour intervenir sur les frères
		public DepartmentMenuUI(ApplicationUI parentUI) {
		    ApplicationUIUtil.setParentUI(this, parentUI);
		}
	]]></script>

  <JPanel id="menuPanel" layout='{new BoxLayout(menuPanel, BoxLayout.PAGE_AXIS)}'>

    <ApplyFilterUI id='applyFilterUI' constructorParams='this'/>

    <Table id="selectionPanel" fill="both">
      <row>
        <cell>
          <JPanel id='localPanel' layout='{new BorderLayout()}'>
            <JLabel id='localLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='localCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='BooleanDTO'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='codeLabel' constraints='BorderLayout.PAGE_START'/>
            <JTextField id='codeEditor' onKeyReleased='handler.setText(event, "code")'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='nameLabel' constraints='BorderLayout.PAGE_START'/>
            <JTextField id='nameEditor' onKeyReleased='handler.setText(event, "name")'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='parentLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='parentCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='DepartmentDTO'/>
          </JPanel>
        </cell>
      </row>
      <row>
        <cell>
          <JPanel layout='{new BorderLayout()}'>
            <JLabel id='statusLabel' constraints='BorderLayout.PAGE_START'/>
            <ExtendedComboBox id='statusCombo' constructorParams='this' constraints='BorderLayout.CENTER' genericType='StatusDTO'/>
          </JPanel>
        </cell>
      </row>
    </Table>

    <JPanel id='selectionButtonsPanel' layout='{new GridLayout(1,0)}'>
      <JButton id='clearButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='searchButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
    </JPanel>

  </JPanel>
</JPanel>