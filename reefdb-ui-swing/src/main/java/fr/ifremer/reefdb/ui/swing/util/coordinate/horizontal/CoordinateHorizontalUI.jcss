/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

CoordinateEditor {
	autoPopup: {handler.getConfig().isAutoPopupNumberEditor()};
	showPopupButton: {handler.getConfig().isShowNumberEditorButton()};
	bean: {model};
	showReset: true;
	_selectOnFocus: {true};
}

#latitudeLabel {
	text: "reefdb.property.latitude";
}

#latitudeMinLabel {
	text: "reefdb.property.min";
	labelFor: {inputLatitudeMin};
}

#inputLatitudeMin {
	property: minLatitude;
	numberValue: {model.getMinLatitude()};
	coordinateType: {CoordinateType.LATITUDE_MIN};
}

#latitudeMaxLabel {
	text: "reefdb.property.max";
	labelFor: {inputLatitudeMax};
}

#inputLatitudeMax {
	property: maxLatitude;
	numberValue: {model.getMaxLatitude()};
	coordinateType: {CoordinateType.LATITUDE_MAX};
}

#longitudeLabel {
	text: "reefdb.property.longitude";
}

#longitudeMinLabel {
	text: "reefdb.property.min";
	labelFor: {inputLongitudeMin};
}

#inputLongitudeMin {
	property: minLongitude;
	numberValue: {model.getMinLongitude()};
	coordinateType: {CoordinateType.LONGITUDE_MIN};
}

#longitudeMaxLabel {
	text: "reefdb.property.max";
	labelFor: {inputLongitudeMax};
}

#inputLongitudeMax {
	property: maxLongitude;
	numberValue: {model.getMaxLongitude()};
	coordinateType: {CoordinateType.LONGITUDE_MAX};
}
