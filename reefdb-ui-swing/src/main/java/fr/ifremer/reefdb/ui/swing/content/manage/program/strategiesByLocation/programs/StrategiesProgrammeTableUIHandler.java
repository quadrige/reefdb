package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.programs;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.StatusRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class StrategiesProgrammeTableUIHandler extends AbstractReefDbTableUIHandler<StrategiesProgrammeTableRowModel, StrategiesProgrammeTableUIModel, StrategiesProgrammeTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(StrategiesProgrammeTableUIHandler.class);

    /**
     * <p>Constructor for StrategiesProgrammeTableUIHandler.</p>
     */
    public StrategiesProgrammeTableUIHandler() {
        super(StrategiesProgrammeTableRowModel.PROPERTY_START_DATE,
                StrategiesProgrammeTableRowModel.PROPERTY_END_DATE);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<StrategiesProgrammeTableRowModel> getTableModel() {
        return (StrategiesProgrammeTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTableau();
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final StrategiesProgrammeTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final StrategiesProgrammeTableUIModel model = new StrategiesProgrammeTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(StrategiesProgrammeTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initialiserTableau();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(final StrategiesProgrammeTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isDatesValid(row);
    }

    private boolean isDatesValid(StrategiesProgrammeTableRowModel row) {

        // both start and end dates must be null or not null
        if (row.getStartDate() == null && row.getEndDate() != null) {
            ReefDbBeans.addError(row, t("reefdb.program.location.startDate.null"), StrategiesProgrammeTableRowModel.PROPERTY_START_DATE);

        } else if (row.getStartDate() != null && row.getEndDate() == null) {
            ReefDbBeans.addError(row, t("reefdb.program.location.endDate.null"), StrategiesProgrammeTableRowModel.PROPERTY_END_DATE);

        } else if (row.getStartDate() != null && row.getEndDate() != null) {
            // end date must be after start date
            if (row.getStartDate().isAfter(row.getEndDate())) {
                ReefDbBeans.addError(row, t("reefdb.program.location.endDate.before"), StrategiesProgrammeTableRowModel.PROPERTY_END_DATE);
            } else {

                // if dates valid, check surveys outside dates
                Long surveysCount = getContext().getObservationService().countSurveysWithProgramLocationAndOutsideDates(row, getModel().getLocationId());
                if (surveysCount > 0) {
                    ReefDbBeans.addError(row, t("reefdb.program.location.period.surveyOutsidePeriod"),
                            StrategiesProgrammeTableRowModel.PROPERTY_START_DATE, StrategiesProgrammeTableRowModel.PROPERTY_END_DATE);
                }
            }
        }

        return row.getErrors().isEmpty();
    }

    /**
     * Chargement des strategies.
     *
     * @param lieu Lieu selectionne
     * @param programme a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    public void load(final ProgramDTO programme, final LocationDTO lieu) {

        // Ajout de la liste des strategies dans le tableau
        getModel().setBeans(getContext().getProgramStrategyService().getStrategyUsageByProgramAndLocationId(
                programme.getCode(), lieu.getId()));

        getModel().setLocationId(lieu.getId());
        getModel().setModify(false);
        getModel().setValid(true);

        // Initialiser le titre du panel
        getUI().setBorder(BorderFactory.createTitledBorder(
                t("reefdb.program.strategies.program.titre",
                        programme.getCode(), lieu.getName())));
    }

    /**
     * Initialisation de le tableau.
     */
    private void initialiserTableau() {

        // Colonne libelle
        final TableColumnExt colonneLibelle = addColumn(
                StrategiesProgrammeTableModel.NAME);
        colonneLibelle.setSortable(true);
        colonneLibelle.setMinWidth(200);

        // Colonne date debut
        final TableColumnExt colonneStartDate = addLocalDatePickerColumnToModel(
                StrategiesProgrammeTableModel.START_DATE, getConfig().getDateFormat());
        colonneStartDate.setSortable(true);
        colonneStartDate.setMinWidth(100);

        // Colonne date fin
        final TableColumnExt colonneEndDate = addLocalDatePickerColumnToModel(
                StrategiesProgrammeTableModel.END_DATE, getConfig().getDateFormat());
        colonneEndDate.setSortable(true);
        colonneEndDate.setMinWidth(100);

        // Colonne local 
        final TableColumnExt colonneLocal = addColumn(StrategiesProgrammeTableModel.STATUS);
        colonneLocal.setCellRenderer(new StatusRenderer());
        colonneLocal.setMinWidth(50);
        colonneLocal.setMaxWidth(50);

        // Modele de la table
        final StrategiesProgrammeTableModel tableModel = new StrategiesProgrammeTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable(), true);

        // Les colonnes obligatoire sont toujours presentes
        colonneLocal.setHideable(false);
        colonneLibelle.setHideable(false);
        colonneStartDate.setHideable(false);
        colonneEndDate.setHideable(false);

        // Seul les colonnes startDate et endDate doivent être editables
        colonneLocal.setEditable(false);
        colonneLibelle.setEditable(false);
        colonneStartDate.setEditable(true);
        colonneEndDate.setEditable(true);
    }
}
