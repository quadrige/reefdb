package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.qualitative;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

import java.util.Collection;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePrecondQualUIModel extends AbstractReefDbTableUIModel<QualitativeValueDTO, RulePrecondQualRowModel, RulePrecondQualUIModel> {

    private PmfmDTO basePmfm;
    public static final String PROPERTY_BASE_PMFM = "basePmfm";
    private PmfmDTO usedPmfm;
    public static final String PROPERTY_USED_PMFM = "usedPmfm";
    private boolean adjusting;
    private boolean editable;
    public static final String PROPERTY_EDITABLE = "editable";

    private Multimap<QualitativeValueDTO, QualitativeValueDTO> qvMap;

    public PmfmDTO getBasePmfm() {
        return basePmfm;
    }

    public void setBasePmfm(PmfmDTO basePmfm) {
        this.basePmfm = basePmfm;
        firePropertyChange(PROPERTY_BASE_PMFM, null, basePmfm);
    }

    public PmfmDTO getUsedPmfm() {
        return usedPmfm;
    }

    public void setUsedPmfm(PmfmDTO usedPmfm) {
        this.usedPmfm = usedPmfm;
        firePropertyChange(PROPERTY_USED_PMFM, null, usedPmfm);
    }

    public void setQvMap(Multimap<QualitativeValueDTO, QualitativeValueDTO> qvMap) {
        this.qvMap = qvMap;
    }

    public Multimap<QualitativeValueDTO, QualitativeValueDTO> getQvMap() {
        if (qvMap == null) {
            qvMap = HashMultimap.create();
        }
        return qvMap;
    }

    public boolean isAdjusting() {
        return adjusting;
    }

    public void setAdjusting(boolean adjusting) {
        this.adjusting = adjusting;
    }

    // setter used by usedRuleQVDoubleList
    public void setSelectedValues(Collection<QualitativeValueDTO> values) {
        QualitativeValueDTO key = getSingleSelectedRow().toBean();
        getQvMap().removeAll(key);
        getQvMap().putAll(key, values);
        if (!isAdjusting()) {
            setModify(true);
            // force validation
            firePropertyChange(PROPERTY_MODIFY, false, true);
        }
    }

    public boolean isRulePreconditionValid() {
        return !getQvMap().isEmpty() && !getQvMap().values().isEmpty();
    }

    public void setRulePreconditionValid(boolean dummy) {
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }
}
