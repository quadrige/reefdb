package fr.ifremer.reefdb.ui.swing.action;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.DialogHelper;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 27/06/2019.
 */
public abstract class AbstractUpdateReferentialAction<M extends AbstractBeanUIModel, UI extends ReefDbUI<M, ?>, H extends AbstractReefDbUIHandler<M, UI>>
        extends AbstractReefDbAction<M, UI, H> {

    private static final Log LOG = LogFactory.getLog(AbstractUpdateReferentialAction.class);

    /**
     * <p>Constructor for AbstractReefDbAction.</p>
     *
     * @param handler  a H object.
     * @param hideBody a boolean.
     */
    public AbstractUpdateReferentialAction(H handler, boolean hideBody) {
        super(handler, hideBody);
        setActionDescription(t("quadrige3.action.synchro.import.title"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Referential update have been proposed");
        }

        return getContext().getDialogHelper().showOptionDialog(null,
                ApplicationUIUtil.getHtmlString(t("reefdb.action.common.import.referential.beforeModify.confirm")),
                t("reefdb.action.common.import.referential.beforeModify.title"),
                JOptionPane.WARNING_MESSAGE,
                DialogHelper.CUSTOM_OPTION,
                t("reefdb.common.import"),
                t("reefdb.common.cancel")
        ) == JOptionPane.OK_OPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Referential update have been confirmed");
        }

        // Import referential
        doImportReferential();

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Referential update done.");
        }
    }
}
