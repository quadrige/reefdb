package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.associatedFractions;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction.table.FractionsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.HashSet;
import java.util.List;

/**
 * Handler.
 */
public class AssociatedFractionsUIHandler extends AbstractReefDbTableUIHandler<FractionsTableRowModel, AssociatedFractionsUIModel, AssociatedFractionsUI> implements Cancelable {

    private static final Log LOG = LogFactory.getLog(AssociatedFractionsUIHandler.class);

    /** Constant <code>DOUBLE_LIST="doubleList"</code> */
    public static final String DOUBLE_LIST = "doubleList";
    /** Constant <code>TABLE="table"</code> */
    public static final String TABLE = "table";

    /** {@inheritDoc} */
    @Override
    public void beforeInit(AssociatedFractionsUI ui) {
        super.beforeInit(ui);

        AssociatedFractionsUIModel model = new AssociatedFractionsUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(AssociatedFractionsUI associatedFractionsUI) {
        initUI(associatedFractionsUI);

        // Init table columns and properties
        initTable();

        initBeanList(getUI().getAssociatedFractionsDoubleList(), getContext().getReferentialService().getFractions(StatusFilter.ALL), null);

        getModel().addPropertyChangeListener(evt -> {

            if (AssociatedFractionsUIModel.PROPERTY_MATRIX.equals(evt.getPropertyName())) {

                List<FractionDTO> fractions = getModel().getMatrix() != null ? Lists.newArrayList(getModel().getMatrix().getFractions()) : null;
                getUI().getAssociatedFractionsDoubleList().getModel().setSelected(fractions);
                getModel().setBeans(fractions);

            } else if (AssociatedFractionsUIModel.PROPERTY_EDITABLE.equals(evt.getPropertyName())) {

                if (getModel().isEditable()) {
                    getUI().getListPanelLayout().setSelected(DOUBLE_LIST);
                } else {
                    getUI().getListPanelLayout().setSelected(TABLE);
                }
            }
        });
    }

    /**
     * Init the table
     */
    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(FractionsTableModel.NAME);
        mnemonicCol.setSortable(true);

        // Description
        final TableColumnExt descriptionCol = addColumn(FractionsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                FractionsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ALL),
                false);
        statusCol.setSortable(true);

        // model for table
        final FractionsTableModel tableModel = new FractionsTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable());

        getTable().setEditable(false);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<FractionsTableRowModel> getTableModel() {
        return (FractionsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getAssociatedFractionsTable();
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {
        if (getModel().isEditable() && getModel().getMatrix() != null) {
            getModel().getMatrix().setFractions(new HashSet<>(getUI().getAssociatedFractionsDoubleList().getModel().getSelected()));
        }

        // close the dialog box
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
