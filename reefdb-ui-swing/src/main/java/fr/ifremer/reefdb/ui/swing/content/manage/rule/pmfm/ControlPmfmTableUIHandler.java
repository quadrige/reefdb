package fr.ifremer.reefdb.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des PSFM.
 */
public class ControlPmfmTableUIHandler extends
        AbstractReefDbTableUIHandler<ControlPmfmRowModel, ControlPmfmTableUIModel, ControlPmfmTableUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ControlPmfmTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlPmfmTableUIModel model = new ControlPmfmTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ControlPmfmTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Initialisation du tableau
        initTable();
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // pmfm id
        TableColumnExt idCol = addColumn(ControlPmfmTableModel.PMFM_ID);
        idCol.setSortable(true);
        idCol.setEditable(false);

        // Libelle
        TableColumnExt nameCol = addColumn(ControlPmfmTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // parametre
        final TableColumnExt parameterCol = addColumn(ControlPmfmTableModel.PARAMETER);
        parameterCol.setSortable(true);
        parameterCol.setEditable(false);

        // support
        final TableColumnExt supportCol = addColumn(ControlPmfmTableModel.MATRIX);
        supportCol.setSortable(true);
        supportCol.setEditable(false);

        // fraction
        final TableColumnExt fractionCol = addColumn(ControlPmfmTableModel.FRACTION);
        fractionCol.setSortable(true);
        fractionCol.setEditable(false);

        // methode
        final TableColumnExt methodCol = addColumn(ControlPmfmTableModel.METHOD);
        methodCol.setSortable(true);
        methodCol.setEditable(false);

        // unit
        final TableColumnExt unitCol = addColumn(ControlPmfmTableModel.UNIT);
        unitCol.setSortable(true);
        unitCol.setEditable(false);

        // Le model
        ControlPmfmTableModel tableModel = new ControlPmfmTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable());
        idCol.setVisible(false);

        // Number rows visible
        getTable().setVisibleRowCount(3);
    }

    /**
     * <p>loadPmfms.</p>
     *
     * @param controlRule a {@link fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO} object.
     */
    public void loadPmfms(ControlRuleDTO controlRule) {

        if (controlRule == null) {

            // Empty table
            getModel().setBeans(null);

            // Set pmfm is not mandatory to avoid bad validation behaviour (Mantis #41992)
            getModel().setEditable(false);

        } else {

            // active table if pmfm is mandatory or preconditioned rule not fully set
            getModel().setEditable(ReefDbBeans.isPmfmMandatory(controlRule) /*&& isPreconditionRuleEmpty(controlRule)*/); // no, dont take care about preconditions (Mantis #46663)

            // disable table sorting for precondition rule
            if (ReefDbBeans.isPreconditionRule(controlRule)) {
                getTable().resetSortOrder();
                getTable().setSortable(false);
            } else {
                getTable().setSortable(true);
            }

            // Load table
            getModel().setBeans(controlRule.getRulePmfms());

        }

        recomputeRowsValidState(!getModel().isEditable());
        getModel().setModify(false);
    }

    private boolean isPreconditionRuleEmpty(ControlRuleDTO controlRule) {
        return !ReefDbBeans.isPreconditionRule(controlRule) || controlRule.isPreconditionsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, ControlPmfmRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        saveToParentModel();
    }

    private void saveToParentModel() {
        getModel().getParentModel().getControlRuleUIModel().getSingleSelectedRow().setRulePmfms(getModel().getBeans());

        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {

        // clear table
        getModel().setBeans(null);
        getModel().setEditable(false);
        getModel().setValid(true);

    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ControlPmfmRowModel> getTableModel() {
        return (ControlPmfmTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getControlPmfmTable();
    }

    /**
     * <p>openAddPmfmDialog.</p>
     */
    void openAddPmfmDialog() {

        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.PMFM.getFilterTypeId());
        dialog.setTitle(t("reefdb.filter.pmfm.addDialog.title"));
        List<PmfmDTO> pmfms = getModel().getBeans().stream().map(RulePmfmDTO::getPmfm)
                .sorted(getDecorator(PmfmDTO.class, DecoratorService.NAME_WITH_ID).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(pmfm -> pmfm.setReadOnly(true))
                .collect(Collectors.toList());

        dialog.getModel().setSelectedElements(pmfms);

        if (!getModel().getParentModel().getRuleListUIModel().getSingleSelectedRow().isLocal()) {
            // filter only national programs if rule list is national
            dialog.getHandler().getFilterElementUIHandler().forceLocal(false);
        }

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            List<PmfmDTO> newPmfms = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((PmfmDTO) element))
                    .filter(newPmfm -> getModel().getBeans().stream().noneMatch(rulePmfm -> rulePmfm.getPmfm().equals(newPmfm)))
                    .collect(Collectors.toList());

            if (!newPmfms.isEmpty()) {
                getModel().addBeans(newPmfms.stream().map(ReefDbBeans::pmfmToRulePmfm).collect(Collectors.toList()));
                getModel().setModify(true);
                saveToParentModel();
            }
        }
    }

    /**
     * <p>removePmfm.</p>
     */
    void removePmfm() {
        getModel().deleteSelectedRows();
        saveToParentModel();
        recomputeRowsValidState(false);
    }
}
