package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.ZipUtils;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.enums.ExtractionOutputType;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUI;
import fr.ifremer.reefdb.ui.swing.content.extraction.SaveAction;
import fr.ifremer.reefdb.ui.swing.content.extraction.list.external.ExternalChooseUI;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.util.DateUtil;

import java.awt.Dimension;
import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Abstract extract action
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public abstract class AbstractExternalExtractAction extends AbstractCheckModelAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    private static final Log LOG = LogFactory.getLog(AbstractExternalExtractAction.class);
    private static final int MAX_FILE_NAME_LENGTH = 255;

    private String fileName;
    private String email;

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    protected AbstractExternalExtractAction(ExtractionsTableUIHandler handler) {
        super(handler, true);
    }

    /**
     * <p>getOutputType.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.enums.ExtractionOutputType} object.
     */
    protected abstract ExtractionOutputType getOutputType();

    /** {@inheritDoc} */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getModel().getExtractionUIModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().getExtractionUIModel().setModify(modelModify);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getModel().getExtractionUIModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        final ExtractionUI extractionUI = getUI().getParentContainer(ExtractionUI.class);
        return extractionUI.getHandler();
    }

    private ExtractionDTO getSelectedExtraction() {
        return getModel().getSelectedRows().size() != 1 ? null : getModel().getSelectedRows().iterator().next();
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getSelectedExtraction() == null) {
            LOG.warn("no selected extraction");
            return false;
        }

        if (getOutputType() == null) {
            LOG.error("no ExtractionOutputType");
            return false;
        }
        if(!getOutputType().isExternal()){
            LOG.error("wrong ExtractionOutputType (should be external");
            return false;
        }

        String tempFileName = String.format("%s-%s-%s-%s",
                getConfig().getExtractionFilePrefix(),
                getOutputType().getLabel(),
                getSelectedExtraction().getName(),
                DateUtil.formatDate(new Date(), "yyyy-MM-dd-HHmmss"));

        // ask for email& file name confirmation
        ExternalChooseUI externalChooseUI = new ExternalChooseUI(getUI());
        externalChooseUI.getModel().setEmail(StringUtils.isEmpty(getSelectedExtraction().getUser().getEmail()) ? "" : getSelectedExtraction().getUser().getEmail().toLowerCase());
        externalChooseUI.getModel().setFileName(tempFileName);
        getHandler().openDialog(externalChooseUI, new Dimension(500, 150));

        if (!externalChooseUI.getModel().isValid()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("extraction aborted");
            }
            return false;
        }

        email = externalChooseUI.getModel().getEmail().toLowerCase().trim();
        String securedName = ReefDbBeans.toSecuredString(externalChooseUI.getModel().getFileName()).trim();
        fileName = String.format("%s#%s", email, securedName);

        // trim to fit 255 characters maximum
        if (fileName.length() > MAX_FILE_NAME_LENGTH) {
            fileName = String.format("%s#%s", email, securedName.substring(0, securedName.length() - MAX_FILE_NAME_LENGTH - fileName.length()));
        }
        Assert.isTrue(fileName.length() <= 255);

        createProgressionUIModel();

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() throws Exception {

        // create the extraction file in temp dir
        File tempDir = getConfig().getNewTmpDirectory("extraction");
        File extractionDir = new File(tempDir, fileName);
        File extractionFile = new File(extractionDir, fileName + "." + getConfig().getExtractionFileExtension());
        extractionDir.mkdirs();

        getContext().getExtractionPerformService().performExtraction(getSelectedExtraction(), getOutputType(), extractionFile, getProgressionUIModel());

        // create the metadata file
//        File metadataFile = new File(extractionDir, fileName + ".properties");
//        metadataFile.createNewFile();

        // zip files
        File zipFile = new File(tempDir, fileName + ".zip");
        ZipUtils.compressFilesInPath(extractionDir.toPath(), zipFile.toPath(), true);

        // export zip file
        getProgressionUIModel().setMessage(t("reefdb.extraction.external.upload.message"));

        ReefDbServiceLocator.instance().getExtractionRestClientService().uploadExtractionFile(
                getContext().getAuthenticationInfo(),
                zipFile, getOutputType().toString(),
                getProgressionUIModel());

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        String text = t("reefdb.action.external.extract.done",
                decorate(getSelectedExtraction()),
                getOutputType().getLabel(),
                email
                );

        getContext().getDialogHelper().showMessageDialog(text, t("reefdb.action.extract.title", getOutputType().getLabel()));

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        fileName = null;
        email = null;
        super.releaseAction();
    }
}
