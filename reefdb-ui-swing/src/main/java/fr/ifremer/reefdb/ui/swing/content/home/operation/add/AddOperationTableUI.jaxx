<!--
  #%L
  Reef DB :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JDialog id='addOperationDialog' layout='{new BorderLayout()}' implements='fr.ifremer.reefdb.ui.swing.util.ReefDbUI&lt;AddOperationTableUIModel, AddOperationTableUIHandler&gt;'>

  <import>
    fr.ifremer.reefdb.ui.swing.ReefDbUIContext
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    java.awt.FlowLayout
    java.text.NumberFormat

    fr.ifremer.quadrige3.ui.swing.component.number.NumberEditor
    fr.ifremer.quadrige3.ui.swing.table.SwingTable
  </import>

  <AddOperationTableUIModel id='model' initializer='getContextValue(AddOperationTableUIModel.class)'/>

  <script><![CDATA[
		public AddOperationTableUI(ReefDbUIContext context) {
			super(context.getExistingActionUI());
			ApplicationUIUtil.setApplicationContext(this, context);
		}
	]]></script>

  <JPanel layout="{new FlowLayout()}" constraints='BorderLayout.PAGE_START'>
    <JLabel id="nbOperationLabel"/>
    <NumberEditor id="nbOperationEditor" constructorParams='this'/>
    <JLabel id="operationNamePrefixLabel"/>
    <JTextField id="operationNamePrefixEditor" onKeyReleased='handler.setText(event, "operationNamePrefix")'/>
  </JPanel>

  <JScrollPane id="scrollPane" constraints='BorderLayout.CENTER'>
    <SwingTable id='addOperationTable'/>
  </JScrollPane>

  <JPanel layout="{new GridLayout(1,0)}" constraints='BorderLayout.PAGE_END'>
    <JButton id="cancelButton" onActionPerformed='handler.cancel()'/>
    <JButton id="validButton" onActionPerformed='handler.valid()'/>
  </JPanel>

</JDialog>