package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class TaxonGroupTableModel extends AbstractReefDbTableModel<TaxonGroupTableRowModel> {

    /** Constant <code>PARENT</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> PARENT = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_PARENT_TAXON_GROUP,
            n("reefdb.property.taxonGroup.parent"),
            n("reefdb.property.taxonGroup.parent"),
            TaxonGroupDTO.class);

    /** Constant <code>LABEL</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> LABEL = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_LABEL,
            n("reefdb.property.label"),
            n("reefdb.property.label"),
            String.class);

    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class,
            true);

    /** Constant <code>STATUS</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class,
            true);

    /** Constant <code>COMMENT</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.comment"),
            n("reefdb.property.comment"),
            String.class);

    /** Constant <code>TYPE</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> TYPE = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_TYPE,
            n("reefdb.property.taxonGroup.type"),
            n("reefdb.property.taxonGroup.type.tip"),
            String.class);

    /** Constant <code>TAXONS</code> */
    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> TAXONS = ReefDbColumnIdentifier.newId(
            TaxonGroupTableRowModel.PROPERTY_TAXONS,
            n("reefdb.property.taxonGroup.taxons"),
            n("reefdb.property.taxonGroup.taxons"),
            TaxonDTO.class,
            DecoratorService.COLLECTION_SIZE);


    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        TaxonGroupTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<TaxonGroupTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        TaxonGroupTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);



    final boolean readOnly;

    /**
     * <p>Constructor for TaxonGroupTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param readOnly a boolean.
     */
    public TaxonGroupTableModel(final TableColumnModelExt columnModel, boolean readOnly) {
        super(columnModel, !readOnly, false);
        this.readOnly = readOnly;
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupTableRowModel createNewRow() {
        return new TaxonGroupTableRowModel(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<TaxonGroupTableRowModel> propertyName) {

        if (propertyName == TAXONS && readOnly) {
            TaxonGroupTableRowModel row = getEntry(rowIndex);
            return row.sizeTaxons() > 0;
        }

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<TaxonGroupTableRowModel> getFirstColumnEditing() {
        return NAME;
    }
}
