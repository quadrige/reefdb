package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.menu.ManageMatricesMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.table.MatricesTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedFractionsCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedFractionCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Matrices au niveau national
 */
public class ManageMatricesNationalUIHandler extends
        AbstractReefDbTableUIHandler<MatricesTableRowModel, ManageMatricesNationalUIModel, ManageMatricesNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageMatricesNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ManageMatricesNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ManageMatricesNationalUIModel model = new ManageMatricesNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ManageMatricesNationalUI ui) {
        initUI(ui);

        ManageMatricesMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(false);

        menuUIModel.addPropertyChangeListener(ManageMatricesMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<MatrixDTO>) evt.getNewValue()));

        initTable();

    }

    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(MatricesTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(false);

        // description
        final TableColumnExt descriptionCol = addColumn(MatricesTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(false);

        // Comment, creation and update dates
        addCommentColumn(MatricesTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(MatricesTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(MatricesTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MatricesTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.NATIONAL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(false);
        fixDefaultColumnWidth(statusCol);

        // associated fractions
        final TableColumnExt associatedFractionsCol = addColumn(
                new AssociatedFractionsCellEditor(getTable(), getUI(), false),
                new AssociatedFractionCellRenderer(),
                MatricesTableModel.ASSOCIATED_FRACTIONS);
        associatedFractionsCol.setSortable(true);

        MatricesTableModel tableModel = new MatricesTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.matrices.national"), MatricesTableModel.ASSOCIATED_FRACTIONS);

        // Initialisation du tableau
        initTable(getTable(), true);

        // Les colonnes optionnelles sont invisibles
        associatedFractionsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<MatricesTableRowModel> getTableModel() {
        return (MatricesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageMatricesNationalTable();
    }
}
