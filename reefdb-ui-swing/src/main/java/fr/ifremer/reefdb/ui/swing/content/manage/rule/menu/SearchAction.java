package fr.ifremer.reefdb.ui.swing.content.manage.rule.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.SaveAction;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

/**
 * Search action.
 */
public class SearchAction extends AbstractCheckModelAction<RulesMenuUIModel, RulesMenuUI, RulesMenuUIHandler> {

    private List<RuleListDTO> ruleLists;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final RulesMenuUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        final RulesUI rulesUI = getUI().getParentContainer(RulesUI.class);
        return rulesUI.getHandler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        final RulesUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        final RulesUIModel model = getLocalModel();
        if (model != null) {
            model.setModify(modelModify);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        final RulesUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        // Recuperation de la liste des regles
        if (StringUtils.isNotBlank(getModel().getRuleListCode())) {
            ruleLists = Lists.newArrayList(getContext().getRuleListService().getRuleList(getModel().getRuleListCode()));
        } else if (StringUtils.isNotBlank(getModel().getProgramCode())) {
            ruleLists = Lists.newArrayList(getContext().getRuleListService().getRuleListsForProgram(getModel().getProgramCode()));
        } else {
            ruleLists = getContext().getRuleListService().getRuleLists();
        }

    }

    @Override
    public void postSuccessAction() {

        getModel().setResults(ruleLists);

        super.postSuccessAction();
    }

    private RulesUIModel getLocalModel() {
        final RulesUI ui = getUI().getParentContainer(RulesUI.class);
        if (ui != null) {
            return ui.getModel();
        }
        return null;
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        RulesUI ui = getUI().getParentContainer(RulesUI.class).getHandler().getUI();
        return ImmutableList.of(
            ui.getRuleListUI().getModel(),
            ui.getControlProgramTableUI().getModel(),
            ui.getControlDepartmentTableUI().getModel(),
            ui.getControlRuleTableUI().getModel(),
            ui.getControlPmfmTableUI().getModel()
        );
    }
}
