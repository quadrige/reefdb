package fr.ifremer.reefdb.ui.swing.content.manage.referential.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;

import java.util.List;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class AbstractReplaceUIModel<E extends BaseReferentialDTO> extends AbstractReefDbBeanUIModel<E, AbstractReplaceUIModel<E>> {

    private static final long serialVersionUID = 1L;

    /** Constant <code>PROPERTY_SOURCE_LIST="sourceList"</code> */
    public static final String PROPERTY_SOURCE_LIST = "sourceList";

    /** Constant <code>PROPERTY_TARGET_LIST="targetList"</code> */
    public static final String PROPERTY_TARGET_LIST = "targetList";

    /** Constant <code>PROPERTY_SELECTED_SOURCE="selectedSource"</code> */
    public static final String PROPERTY_SELECTED_SOURCE = "selectedSource";

    /** Constant <code>PROPERTY_SELECTED_TARGET="selectedTarget"</code> */
    public static final String PROPERTY_SELECTED_TARGET = "selectedTarget";

    /** Constant <code>PROPERTY_DELETE="delete"</code> */
    public static final String PROPERTY_DELETE = "delete";

    protected List<E> sourceList;

    protected List<E> targetList;

    protected E selectedSource;

    protected E selectedTarget;

    protected boolean delete = true;

    protected String decoratorContext = null;

    /**
     * <p>Constructor for AbstractReplaceUIModel.</p>
     */
    public AbstractReplaceUIModel() {
        super(null, null);
    }

    /**
     * <p>Getter for the field <code>sourceList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<E> getSourceList() {
        return sourceList;
    }

    /**
     * <p>Setter for the field <code>sourceList</code>.</p>
     *
     * @param sourceList a {@link java.util.List} object.
     */
    public void setSourceList(List<E> sourceList) {
        this.sourceList = sourceList;
        firePropertyChange(PROPERTY_SOURCE_LIST, null, sourceList);
    }

    /**
     * <p>Getter for the field <code>targetList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<E> getTargetList() {
        return targetList;
    }

    /**
     * <p>Setter for the field <code>targetList</code>.</p>
     *
     * @param targetList a {@link java.util.List} object.
     */
    public void setTargetList(List<E> targetList) {
        this.targetList = targetList;
        firePropertyChange(PROPERTY_TARGET_LIST, null, targetList);
    }

    /**
     * <p>Getter for the field <code>selectedTarget</code>.</p>
     *
     * @return a E object.
     */
    public E getSelectedTarget() {
        return selectedTarget;
    }

    /**
     * <p>Setter for the field <code>selectedTarget</code>.</p>
     *
     * @param selectedTarget a E object.
     */
    public void setSelectedTarget(E selectedTarget) {
        E oldvalue = getSelectedTarget();
        this.selectedTarget = selectedTarget;
        firePropertyChange(PROPERTY_SELECTED_TARGET, oldvalue, selectedTarget);
    }

    /**
     * <p>Getter for the field <code>selectedSource</code>.</p>
     *
     * @return a E object.
     */
    public E getSelectedSource() {
        return selectedSource;
    }

    /**
     * <p>Setter for the field <code>selectedSource</code>.</p>
     *
     * @param selectedSource a E object.
     */
    public void setSelectedSource(E selectedSource) {
        E oldvalue = getSelectedSource();
        this.selectedSource = selectedSource;
        firePropertyChange(PROPERTY_SELECTED_SOURCE, oldvalue, selectedSource);
    }

    /**
     * <p>isDelete.</p>
     *
     * @return a boolean.
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * <p>Setter for the field <code>delete</code>.</p>
     *
     * @param delete a boolean.
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    /**
     * <p>Getter for the field <code>decoratorContext</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDecoratorContext() {
        return decoratorContext;
    }

    /**
     * <p>Setter for the field <code>decoratorContext</code>.</p>
     *
     * @param decoratorContext a {@link java.lang.String} object.
     */
    public void setDecoratorContext(String decoratorContext) {
        this.decoratorContext = decoratorContext;
    }

    /** {@inheritDoc} */
    @Override
    protected E newBean() {
        return null;
    }
}
