package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Import action.
 */
public class ImportFilterAction extends AbstractReefDbAction<FilterListUIModel, FilterListUI, FilterListUIHandler> {

    private List<FilterDTO> importedFilters;

    private File importFile;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public ImportFilterAction(FilterListUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        importFile = null;
        if (super.prepareAction()) {
            // get importFile
            importFile = chooseFile(t("reefdb.action.filter.import.title"),
                    t("reefdb.action.common.chooseFile.buttonLabel"),
                    "^.*\\.dat", t("reefdb.common.file.dat"));

        }
        return importFile != null && importFile.exists();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {
        importedFilters = getContext().getContextService().importFilter(importFile, getHandler().getFilterTypeId());
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        getModel().addBeans(importedFilters);
        getHandler().reloadComboBox();
    }

}
