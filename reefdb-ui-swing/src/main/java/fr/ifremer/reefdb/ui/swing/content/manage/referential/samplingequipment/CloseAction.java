package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractCheckBeforeChangeScreenAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.action.GoToObservationAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local.SamplingEquipmentsLocalUIModel;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * Action close a configuration screen
 */
public class CloseAction extends AbstractCheckBeforeChangeScreenAction<ManageSamplingEquipmentsUIModel, ManageSamplingEquipmentsUI, ManageSamplingEquipmentsUIHandler> {

	/**
	 * Constructor.
	 *
	 * @param handler Controller
	 */
	public CloseAction(final ManageSamplingEquipmentsUIHandler handler) {
		super(handler, true);
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
		return SaveAction.class;
	}

	/** {@inheritDoc} */
	@Override
	protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
		return getHandler();
	}
	
	/** {@inheritDoc} */
	@Override
	protected Class<GoToObservationAction> getGotoActionClass() {
		return GoToObservationAction.class;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelModify() {
		return getLocalModel().isModify();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelValid() {
		return getLocalModel().isValid();
	}
	
	private SamplingEquipmentsLocalUIModel getLocalModel() {
		return getUI().getSamplingEquipmentsLocalUI().getModel();
	}
	
	/** {@inheritDoc} */
	@Override
	protected void setModelModify(boolean modelModify) {
		getLocalModel().setModify(modelModify);
	}
}
