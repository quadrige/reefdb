package fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.menu.ReferentialUnitsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.table.UnitTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.table.UnitTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des Units au niveau national
 */
public class ReferentialUnitsNationalUIHandler extends
        AbstractReefDbTableUIHandler<UnitTableRowModel, ReferentialUnitsNationalUIModel, ReferentialUnitsNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReferentialUnitsNationalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ReferentialUnitsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ReferentialUnitsNationalUIModel model = new ReferentialUnitsNationalUIModel();
        ui.setContextValue(model);

    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ReferentialUnitsNationalUI ui) {
        initUI(ui);

        ReferentialUnitsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(false);

        menuUIModel.addPropertyChangeListener(ReferentialUnitsMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((Collection<UnitDTO>) evt.getNewValue()));

        initTable();
    }

    private void initTable() {

        // mnemonic
        final TableColumnExt mnemonicCol = addColumn(
                UnitTableModel.NAME);
        mnemonicCol.setSortable(true);

        // Symbol
        final TableColumnExt symbolCol = addColumn(
                UnitTableModel.SYMBOL);
        symbolCol.setSortable(true);

        // Comment, creation and update dates
        addCommentColumn(UnitTableModel.COMMENT, false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(UnitTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(UnitTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                UnitTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.ACTIVE),
                false);
        statusCol.setSortable(true);

        final UnitTableModel tableModel = new UnitTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Colonnes non editable
        tableModel.setNoneEditableCols(
                UnitTableModel.NAME,
                UnitTableModel.SYMBOL,
                UnitTableModel.STATUS);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.units.national"));

        // Initialisation du tableau
        initTable(getTable(), true);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<UnitTableRowModel> getTableModel() {
        return (UnitTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getReferentialUnitsNationalTable();
    }

}
