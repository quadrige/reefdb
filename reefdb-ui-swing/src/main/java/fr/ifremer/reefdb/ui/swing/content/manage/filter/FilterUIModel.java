package fr.ifremer.reefdb.ui.swing.content.manage.filter;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.FilterElementUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListUIModel;

/**
 * Model.
 */
public class FilterUIModel extends AbstractEmptyUIModel<FilterUIModel> {

    private FilterListRowModel selectedFilter;

    private FilterListUIModel filterListUIModel;
    private FilterElementUIModel filterElementUIModel;

    /**
     * <p>Getter for the field <code>selectedFilter</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListRowModel} object.
     */
    public FilterListRowModel getSelectedFilter() {
        return selectedFilter;
    }

    /**
     * <p>Setter for the field <code>selectedFilter</code>.</p>
     *
     * @param selectedFilter a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListRowModel} object.
     */
    public void setSelectedFilter(FilterListRowModel selectedFilter) {
        this.selectedFilter = selectedFilter;
    }

    /**
     * <p>Getter for the field <code>filterListUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListUIModel} object.
     */
    public FilterListUIModel getFilterListUIModel() {
        return filterListUIModel;
    }

    /**
     * <p>Setter for the field <code>filterListUIModel</code>.</p>
     *
     * @param filterListUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListUIModel} object.
     */
    public void setFilterListUIModel(FilterListUIModel filterListUIModel) {
        this.filterListUIModel = filterListUIModel;
    }

    /**
     * <p>Getter for the field <code>filterElementUIModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.element.FilterElementUIModel} object.
     */
    public FilterElementUIModel getFilterElementUIModel() {
        return filterElementUIModel;
    }

    /**
     * <p>Setter for the field <code>filterElementUIModel</code>.</p>
     *
     * @param filterElementUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.filter.element.FilterElementUIModel} object.
     */
    public void setFilterElementUIModel(FilterElementUIModel filterElementUIModel) {
        this.filterElementUIModel = filterElementUIModel;
    }
}
