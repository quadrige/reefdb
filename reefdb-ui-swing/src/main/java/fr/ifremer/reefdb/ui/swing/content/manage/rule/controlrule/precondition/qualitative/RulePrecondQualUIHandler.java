package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.qualitative;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.AbstractTableModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import java.util.ArrayList;

/**
 * @author peck7 on 05/02/2018.
 */
public class RulePrecondQualUIHandler
        extends AbstractReefDbTableUIHandler<RulePrecondQualRowModel, RulePrecondQualUIModel, RulePrecondQualUI>
        implements Cancelable {

    private static final Log LOG = LogFactory.getLog(RulePrecondQualUIHandler.class);

    @Override
    public void beforeInit(RulePrecondQualUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(new RulePrecondQualUIModel());
    }

    @Override
    public void afterInit(RulePrecondQualUI rulePrecondQualUI) {
        initUI(rulePrecondQualUI);

        initTable();

        // adjust double list size (Mantis #51128)
        initBeanList(rulePrecondQualUI.getUsedRuleQVDoubleList(), null, new ArrayList<>(), 100);
        rulePrecondQualUI.getUsedRuleQVDoubleList().getHandler().addAdditionalControls();

        initListeners();

        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());
    }

    private void initTable() {

        TableColumnExt nameCol = addColumn(RulePrecondQualTableModel.NAME);
        nameCol.setSortable(true);

        RulePrecondQualTableModel tableModel = new RulePrecondQualTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        initTable(getTable(), true);

        getTable().setSortOrder(RulePrecondQualTableModel.NAME, SortOrder.ASCENDING);
        getTable().setHorizontalScrollEnabled(false);
        getTable().setEditable(false);
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {

                case RulePrecondQualUIModel.PROPERTY_BASE_PMFM:
                    initBasePmfm();
                    break;

                case RulePrecondQualUIModel.PROPERTY_USED_PMFM:
                    initUsedPmfm();
                    break;

                case RulePrecondQualUIModel.PROPERTY_SINGLE_ROW_SELECTED:
                    updateDoubleList();
                    break;
            }
        });
    }

    private void initBasePmfm() {
        PmfmDTO basePmfm = getModel().getBasePmfm();
        if (basePmfm == null) {
            LOG.error("base PMFMU should be set");
            return;
        }
        if (!basePmfm.getParameter().isQualitative()) {
            LOG.error("base PMFMU should be qualitative");
            return;
        }
        ((TitledBorder) getUI().getBaseRulePanel().getBorder()).setTitle(decorate(basePmfm, DecoratorService.NAME_WITH_UNIT));
        getUI().getBaseRulePanel().setToolTipText(decorate(basePmfm));

        // load real qualitative values
        getModel().setBeans(getContext().getReferentialService().getUniquePmfmFromPmfm(basePmfm).getQualitativeValues());

        // select first line
        if (getModel().getRowCount() > 0)
            SwingUtilities.invokeLater(() -> selectCell(0, null));
    }

    private void initUsedPmfm() {
        PmfmDTO usedPmfm = getModel().getUsedPmfm();
        if (usedPmfm == null) {
            LOG.error("used PMFMU should be set");
            return;
        }
        if (!usedPmfm.getParameter().isQualitative()) {
            LOG.error("used PMFMU should be numerical");
            return;
        }
        ((TitledBorder) getUI().getUsedRulePanel().getBorder()).setTitle(decorate(usedPmfm, DecoratorService.NAME_WITH_UNIT));
        getUI().getUsedRulePanel().setToolTipText(decorate(usedPmfm));

        // load real qualitative values
        getUI().getUsedRuleQVDoubleList().getHandler().setUniverse(
                getContext().getReferentialService().getUniquePmfmFromPmfm(usedPmfm).getQualitativeValues()
        );
    }

    private void updateDoubleList() {

        if (getModel().getSingleSelectedRow() != null) {
            getModel().setAdjusting(true);
            getUI().getUsedRuleQVDoubleList().getHandler().setSelected(
                    new ArrayList<>(
                            getModel().getQvMap().get(getModel().getSingleSelectedRow().toBean())
                    )
            );
            getModel().setAdjusting(false);
        }
    }

    @Override
    public SwingValidator<RulePrecondQualUIModel> getValidator() {
        return getUI().getValidator();
    }

    public void valid() {
        if (getModel().isValid()) {
            stopListenValidatorValid(getValidator());
            closeDialog();
        }
    }

    @Override
    public void cancel() {
        getModel().setModify(false);
//        getModel().setValid(false);
        stopListenValidatorValid(getValidator());
        closeDialog();
    }

    @Override
    public AbstractTableModel<RulePrecondQualRowModel> getTableModel() {
        return (RulePrecondQualTableModel) getTable().getModel();
    }

    @Override
    public SwingTable getTable() {
        return getUI().getBaseRuleQVTable();
    }
}
