package fr.ifremer.reefdb.ui.swing.content.manage.rule.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>ControlProgramTableRowModel class.</p>
 *
 * @author Antoine
 */
public class ControlProgramTableRowModel extends AbstractReefDbRowUIModel<ProgramDTO, ControlProgramTableRowModel> implements ProgramDTO {

    private static final Binder<ProgramDTO, ControlProgramTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(ProgramDTO.class, ControlProgramTableRowModel.class);

    private static final Binder<ControlProgramTableRowModel, ProgramDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(ControlProgramTableRowModel.class, ProgramDTO.class);

    /**
     * <p>Constructor for ControlProgramTableRowModel.</p>
     */
    public ControlProgramTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

    /** {@inheritDoc} */
    @Override
    protected ProgramDTO newBean() {
        return ReefDbBeanFactory.newProgramDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getCode() {
        return delegateObject.getCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setCode(String code) {
        delegateObject.setCode(code);
    }

    @Override
    public String getDescription() {
        return delegateObject.getDescription();
    }

    @Override
    public void setDescription(String description) {
        delegateObject.setDescription(description);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String Name) {
        delegateObject.setName(Name);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String Comment) {
        delegateObject.setComment(Comment);
    }

    /** {@inheritDoc} */
    @Override
    public StrategyDTO getStrategies(int index) {
        return delegateObject.getStrategies(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isStrategiesEmpty() {
        return delegateObject.isStrategiesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeStrategies() {
        return delegateObject.sizeStrategies();
    }

    /** {@inheritDoc} */
    @Override
    public void addStrategies(StrategyDTO strategy) {
        delegateObject.addStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllStrategies(Collection<StrategyDTO> strategy) {
        delegateObject.addAllStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeStrategies(StrategyDTO strategy) {
        return delegateObject.removeStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllStrategies(Collection<StrategyDTO> strategy) {
        return delegateObject.removeAllStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsStrategies(StrategyDTO strategy) {
        return delegateObject.containsStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllStrategies(Collection<StrategyDTO> strategy) {
        return delegateObject.containsAllStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<StrategyDTO> getStrategies() {
        return delegateObject.getStrategies();
    }

    /** {@inheritDoc} */
    @Override
    public void setStrategies(Collection<StrategyDTO> strategy) {
        delegateObject.setStrategies(strategy);
    }

    /** {@inheritDoc} */
    @Override
    public LocationDTO getLocations(int index) {
        return delegateObject.getLocations(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isLocationsEmpty() {
        return delegateObject.isLocationsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeLocations() {
        return delegateObject.sizeLocations();
    }

    /** {@inheritDoc} */
    @Override
    public void addLocations(LocationDTO location) {
        delegateObject.addLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllLocations(Collection<LocationDTO> location) {
        delegateObject.addAllLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeLocations(LocationDTO location) {
        return delegateObject.removeLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllLocations(Collection<LocationDTO> location) {
        return delegateObject.removeAllLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLocations(LocationDTO location) {
        return delegateObject.containsLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllLocations(Collection<LocationDTO> location) {
        return delegateObject.containsAllLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public List<LocationDTO> getLocations() {
        return delegateObject.getLocations();
    }

    /** {@inheritDoc} */
    @Override
    public void setLocations(List<LocationDTO> location) {
        delegateObject.setLocations(location);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isStrategiesLoaded() {
        return delegateObject.isStrategiesLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setStrategiesLoaded(boolean strategiesLoaded) {
        delegateObject.setStrategiesLoaded(strategiesLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isLocationsLoaded() {
        return delegateObject.isLocationsLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setLocationsLoaded(boolean locationsLoaded) {
        delegateObject.setLocationsLoaded(locationsLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getManagerPersons(int index) {
        return delegateObject.getManagerPersons(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isManagerPersonsEmpty() {
        return delegateObject.isManagerPersonsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeManagerPersons() {
        return delegateObject.sizeManagerPersons();
    }

    /** {@inheritDoc} */
    @Override
    public void addManagerPersons(PersonDTO managerPersons) {
        delegateObject.addManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllManagerPersons(Collection<PersonDTO> managerPersons) {
        delegateObject.addAllManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeManagerPersons(PersonDTO managerPersons) {
        return delegateObject.removeManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllManagerPersons(Collection<PersonDTO> managerPersons) {
        return delegateObject.removeAllManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsManagerPersons(PersonDTO managerPersons) {
        return delegateObject.containsManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllManagerPersons(Collection<PersonDTO> managerPersons) {
        return delegateObject.containsAllManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PersonDTO> getManagerPersons() {
        return delegateObject.getManagerPersons();
    }

    /** {@inheritDoc} */
    @Override
    public void setManagerPersons(Collection<PersonDTO> managerPersons) {
        delegateObject.setManagerPersons(managerPersons);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getRecorderPersons(int index) {
        return delegateObject.getRecorderPersons(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isRecorderPersonsEmpty() {
        return delegateObject.isRecorderPersonsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeRecorderPersons() {
        return delegateObject.sizeRecorderPersons();
    }

    /** {@inheritDoc} */
    @Override
    public void addRecorderPersons(PersonDTO recorderPersons) {
        delegateObject.addRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllRecorderPersons(Collection<PersonDTO> recorderPersons) {
        delegateObject.addAllRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeRecorderPersons(PersonDTO recorderPersons) {
        return delegateObject.removeRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllRecorderPersons(Collection<PersonDTO> recorderPersons) {
        return delegateObject.removeAllRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsRecorderPersons(PersonDTO recorderPersons) {
        return delegateObject.containsRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllRecorderPersons(Collection<PersonDTO> recorderPersons) {
        return delegateObject.containsAllRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PersonDTO> getRecorderPersons() {
        return delegateObject.getRecorderPersons();
    }

    /** {@inheritDoc} */
    @Override
    public void setRecorderPersons(Collection<PersonDTO> recorderPersons) {
        delegateObject.setRecorderPersons(recorderPersons);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getRecorderDepartments(int index) {
        return delegateObject.getRecorderDepartments(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isRecorderDepartmentsEmpty() {
        return delegateObject.isRecorderDepartmentsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeRecorderDepartments() {
        return delegateObject.sizeRecorderDepartments();
    }

    /** {@inheritDoc} */
    @Override
    public void addRecorderDepartments(DepartmentDTO recorderDepartments) {
        delegateObject.addRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllRecorderDepartments(Collection<DepartmentDTO> recorderDepartments) {
        delegateObject.addAllRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeRecorderDepartments(DepartmentDTO recorderDepartments) {
        return delegateObject.removeRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllRecorderDepartments(Collection<DepartmentDTO> recorderDepartments) {
        return delegateObject.removeAllRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsRecorderDepartments(DepartmentDTO recorderDepartments) {
        return delegateObject.containsRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllRecorderDepartments(Collection<DepartmentDTO> recorderDepartments) {
        return delegateObject.containsAllRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<DepartmentDTO> getRecorderDepartments() {
        return delegateObject.getRecorderDepartments();
    }

    /** {@inheritDoc} */
    @Override
    public void setRecorderDepartments(Collection<DepartmentDTO> recorderDepartments) {
        delegateObject.setRecorderDepartments(recorderDepartments);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isNewCode() {
        return delegateObject.isNewCode();
    }

    /** {@inheritDoc} */
    @Override
    public void setNewCode(boolean newCode) {
        delegateObject.setNewCode(newCode);
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


    @Override
    public DepartmentDTO getFullViewerDepartments(int index) {
        return delegateObject.getFullViewerDepartments(index);
    }

    @Override
    public boolean isFullViewerDepartmentsEmpty() {
        return delegateObject.isFullViewerDepartmentsEmpty();
    }

    @Override
    public int sizeFullViewerDepartments() {
        return delegateObject.sizeFullViewerDepartments();
    }

    @Override
    public void addFullViewerDepartments(DepartmentDTO fullViewerDepartments) {
        delegateObject.addFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public void addAllFullViewerDepartments(Collection<DepartmentDTO> fullViewerDepartments) {
        delegateObject.addAllFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public boolean removeFullViewerDepartments(DepartmentDTO fullViewerDepartments) {
        return delegateObject.removeFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public boolean removeAllFullViewerDepartments(Collection<DepartmentDTO> fullViewerDepartments) {
        return delegateObject.removeAllFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public boolean containsFullViewerDepartments(DepartmentDTO fullViewerDepartments) {
        return delegateObject.containsFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public boolean containsAllFullViewerDepartments(Collection<DepartmentDTO> fullViewerDepartments) {
        return delegateObject.containsAllFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public Collection<DepartmentDTO> getFullViewerDepartments() {
        return delegateObject.getFullViewerDepartments();
    }

    @Override
    public void setFullViewerDepartments(Collection<DepartmentDTO> fullViewerDepartments) {
        delegateObject.setFullViewerDepartments(fullViewerDepartments);
    }

    @Override
    public DepartmentDTO getViewerDepartments(int index) {
        return delegateObject.getViewerDepartments(index);
    }

    @Override
    public boolean isViewerDepartmentsEmpty() {
        return delegateObject.isViewerDepartmentsEmpty();
    }

    @Override
    public int sizeViewerDepartments() {
        return delegateObject.sizeViewerDepartments();
    }

    @Override
    public void addViewerDepartments(DepartmentDTO viewerDepartments) {
        delegateObject.addViewerDepartments(viewerDepartments);
    }

    @Override
    public void addAllViewerDepartments(Collection<DepartmentDTO> viewerDepartments) {
        delegateObject.addAllViewerDepartments(viewerDepartments);
    }

    @Override
    public boolean removeViewerDepartments(DepartmentDTO viewerDepartments) {
        return delegateObject.removeViewerDepartments(viewerDepartments);
    }

    @Override
    public boolean removeAllViewerDepartments(Collection<DepartmentDTO> viewerDepartments) {
        return delegateObject.removeAllViewerDepartments(viewerDepartments);
    }

    @Override
    public boolean containsViewerDepartments(DepartmentDTO viewerDepartments) {
        return delegateObject.containsViewerDepartments(viewerDepartments);
    }

    @Override
    public boolean containsAllViewerDepartments(Collection<DepartmentDTO> viewerDepartments) {
        return delegateObject.containsAllViewerDepartments(viewerDepartments);
    }

    @Override
    public Collection<DepartmentDTO> getViewerDepartments() {
        return delegateObject.getViewerDepartments();
    }

    @Override
    public void setViewerDepartments(Collection<DepartmentDTO> viewerDepartments) {
        delegateObject.setViewerDepartments(viewerDepartments);
    }

    @Override
    public DepartmentDTO getValidatorDepartments(int index) {
        return delegateObject.getValidatorDepartments(index);
    }

    @Override
    public boolean isValidatorDepartmentsEmpty() {
        return delegateObject.isValidatorDepartmentsEmpty();
    }

    @Override
    public int sizeValidatorDepartments() {
        return delegateObject.sizeValidatorDepartments();
    }

    @Override
    public void addValidatorDepartments(DepartmentDTO validatorDepartments) {
        delegateObject.addValidatorDepartments(validatorDepartments);
    }

    @Override
    public void addAllValidatorDepartments(Collection<DepartmentDTO> validatorDepartments) {
        delegateObject.addAllValidatorDepartments(validatorDepartments);
    }

    @Override
    public boolean removeValidatorDepartments(DepartmentDTO validatorDepartments) {
        return delegateObject.removeValidatorDepartments(validatorDepartments);
    }

    @Override
    public boolean removeAllValidatorDepartments(Collection<DepartmentDTO> validatorDepartments) {
        return delegateObject.removeAllValidatorDepartments(validatorDepartments);
    }

    @Override
    public boolean containsValidatorDepartments(DepartmentDTO validatorDepartments) {
        return delegateObject.containsValidatorDepartments(validatorDepartments);
    }

    @Override
    public boolean containsAllValidatorDepartments(Collection<DepartmentDTO> validatorDepartments) {
        return delegateObject.containsAllValidatorDepartments(validatorDepartments);
    }

    @Override
    public Collection<DepartmentDTO> getValidatorDepartments() {
        return delegateObject.getValidatorDepartments();
    }

    @Override
    public void setValidatorDepartments(Collection<DepartmentDTO> validatorDepartments) {
        delegateObject.setValidatorDepartments(validatorDepartments);
    }

    @Override
    public PersonDTO getFullViewerPersons(int index) {
        return delegateObject.getFullViewerPersons(index);
    }

    @Override
    public boolean isFullViewerPersonsEmpty() {
        return delegateObject.isFullViewerPersonsEmpty();
    }

    @Override
    public int sizeFullViewerPersons() {
        return delegateObject.sizeFullViewerPersons();
    }

    @Override
    public void addFullViewerPersons(PersonDTO fullViewerPersons) {
        delegateObject.addFullViewerPersons(fullViewerPersons);
    }

    @Override
    public void addAllFullViewerPersons(Collection<PersonDTO> fullViewerPersons) {
        delegateObject.addAllFullViewerPersons(fullViewerPersons);
    }

    @Override
    public boolean removeFullViewerPersons(PersonDTO fullViewerPersons) {
        return delegateObject.removeFullViewerPersons(fullViewerPersons);
    }

    @Override
    public boolean removeAllFullViewerPersons(Collection<PersonDTO> fullViewerPersons) {
        return delegateObject.removeAllFullViewerPersons(fullViewerPersons);
    }

    @Override
    public boolean containsFullViewerPersons(PersonDTO fullViewerPersons) {
        return delegateObject.containsFullViewerPersons(fullViewerPersons);
    }

    @Override
    public boolean containsAllFullViewerPersons(Collection<PersonDTO> fullViewerPersons) {
        return delegateObject.containsAllFullViewerPersons(fullViewerPersons);
    }

    @Override
    public Collection<PersonDTO> getFullViewerPersons() {
        return delegateObject.getFullViewerPersons();
    }

    @Override
    public void setFullViewerPersons(Collection<PersonDTO> fullViewerPersons) {
        delegateObject.setFullViewerPersons(fullViewerPersons);
    }

    @Override
    public PersonDTO getViewerPersons(int index) {
        return delegateObject.getViewerPersons(index);
    }

    @Override
    public boolean isViewerPersonsEmpty() {
        return delegateObject.isViewerPersonsEmpty();
    }

    @Override
    public int sizeViewerPersons() {
        return delegateObject.sizeViewerPersons();
    }

    @Override
    public void addViewerPersons(PersonDTO viewerPersons) {
        delegateObject.addViewerPersons(viewerPersons);
    }

    @Override
    public void addAllViewerPersons(Collection<PersonDTO> viewerPersons) {
        delegateObject.addAllViewerPersons(viewerPersons);
    }

    @Override
    public boolean removeViewerPersons(PersonDTO viewerPersons) {
        return delegateObject.removeViewerPersons(viewerPersons);
    }

    @Override
    public boolean removeAllViewerPersons(Collection<PersonDTO> viewerPersons) {
        return delegateObject.removeAllViewerPersons(viewerPersons);
    }

    @Override
    public boolean containsViewerPersons(PersonDTO viewerPersons) {
        return delegateObject.containsViewerPersons(viewerPersons);
    }

    @Override
    public boolean containsAllViewerPersons(Collection<PersonDTO> viewerPersons) {
        return delegateObject.containsAllViewerPersons(viewerPersons);
    }

    @Override
    public Collection<PersonDTO> getViewerPersons() {
        return delegateObject.getViewerPersons();
    }

    @Override
    public void setViewerPersons(Collection<PersonDTO> viewerPersons) {
        delegateObject.setViewerPersons(viewerPersons);
    }

    @Override
    public PersonDTO getValidatorPersons(int index) {
        return delegateObject.getValidatorPersons(index);
    }

    @Override
    public boolean isValidatorPersonsEmpty() {
        return delegateObject.isValidatorPersonsEmpty();
    }

    @Override
    public int sizeValidatorPersons() {
        return delegateObject.sizeValidatorPersons();
    }

    @Override
    public void addValidatorPersons(PersonDTO validatorPersons) {
        delegateObject.addValidatorPersons(validatorPersons);
    }

    @Override
    public void addAllValidatorPersons(Collection<PersonDTO> validatorPersons) {
        delegateObject.addAllValidatorPersons(validatorPersons);
    }

    @Override
    public boolean removeValidatorPersons(PersonDTO validatorPersons) {
        return delegateObject.removeValidatorPersons(validatorPersons);
    }

    @Override
    public boolean removeAllValidatorPersons(Collection<PersonDTO> validatorPersons) {
        return delegateObject.removeAllValidatorPersons(validatorPersons);
    }

    @Override
    public boolean containsValidatorPersons(PersonDTO validatorPersons) {
        return delegateObject.containsValidatorPersons(validatorPersons);
    }

    @Override
    public boolean containsAllValidatorPersons(Collection<PersonDTO> validatorPersons) {
        return delegateObject.containsAllValidatorPersons(validatorPersons);
    }

    @Override
    public Collection<PersonDTO> getValidatorPersons() {
        return delegateObject.getValidatorPersons();
    }

    @Override
    public void setValidatorPersons(Collection<PersonDTO> validatorPersons) {
        delegateObject.setValidatorPersons(validatorPersons);
    }

    @Override
    public MoratoriumDTO getMoratoriums(int index) {
        return delegateObject.getMoratoriums(index);
    }

    @Override
    public boolean isMoratoriumsEmpty() {
        return delegateObject.isMoratoriumsEmpty();
    }

    @Override
    public int sizeMoratoriums() {
        return delegateObject.sizeMoratoriums();
    }

    @Override
    public void addMoratoriums(MoratoriumDTO moratoriums) {
        delegateObject.addMoratoriums(moratoriums);
    }

    @Override
    public void addAllMoratoriums(Collection<MoratoriumDTO> moratoriums) {
        delegateObject.addAllMoratoriums(moratoriums);
    }

    @Override
    public boolean removeMoratoriums(MoratoriumDTO moratoriums) {
        return delegateObject.removeMoratoriums(moratoriums);
    }

    @Override
    public boolean removeAllMoratoriums(Collection<MoratoriumDTO> moratoriums) {
        return delegateObject.removeAllMoratoriums(moratoriums);
    }

    @Override
    public boolean containsMoratoriums(MoratoriumDTO moratoriums) {
        return delegateObject.containsMoratoriums(moratoriums);
    }

    @Override
    public boolean containsAllMoratoriums(Collection<MoratoriumDTO> moratoriums) {
        return delegateObject.containsAllMoratoriums(moratoriums);
    }

    @Override
    public List<MoratoriumDTO> getMoratoriums() {
        return delegateObject.getMoratoriums();
    }

    @Override
    public void setMoratoriums(List<MoratoriumDTO> moratoriums) {
        delegateObject.setMoratoriums(moratoriums);
    }

    @Override
    public DepartmentDTO getManagerDepartments(int index) {
        return delegateObject.getManagerDepartments(index);
    }

    @Override
    public boolean isManagerDepartmentsEmpty() {
        return delegateObject.isManagerDepartmentsEmpty();
    }

    @Override
    public int sizeManagerDepartments() {
        return delegateObject.sizeManagerDepartments();
    }

    @Override
    public void addManagerDepartments(DepartmentDTO managerDepartments) {
        delegateObject.addManagerDepartments(managerDepartments);
    }

    @Override
    public void addAllManagerDepartments(Collection<DepartmentDTO> managerDepartments) {
        delegateObject.addAllManagerDepartments(managerDepartments);
    }

    @Override
    public boolean removeManagerDepartments(DepartmentDTO managerDepartments) {
        return delegateObject.removeManagerDepartments(managerDepartments);
    }

    @Override
    public boolean removeAllManagerDepartments(Collection<DepartmentDTO> managerDepartments) {
        return delegateObject.removeAllManagerDepartments(managerDepartments);
    }

    @Override
    public boolean containsManagerDepartments(DepartmentDTO managerDepartments) {
        return delegateObject.containsManagerDepartments(managerDepartments);
    }

    @Override
    public boolean containsAllManagerDepartments(Collection<DepartmentDTO> managerDepartments) {
        return delegateObject.containsAllManagerDepartments(managerDepartments);
    }

    @Override
    public Collection<DepartmentDTO> getManagerDepartments() {
        return delegateObject.getManagerDepartments();
    }

    @Override
    public void setManagerDepartments(Collection<DepartmentDTO> managerDepartments) {
        delegateObject.setManagerDepartments(managerDepartments);
    }

}
