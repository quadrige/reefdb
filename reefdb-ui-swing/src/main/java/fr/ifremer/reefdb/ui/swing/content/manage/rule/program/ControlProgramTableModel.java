package fr.ifremer.reefdb.ui.swing.content.manage.rule.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlProgramTableModel extends AbstractReefDbTableModel<ControlProgramTableRowModel> {

	/** Constant <code>CODE</code> */
	public static final ReefDbColumnIdentifier<ControlProgramTableRowModel> CODE = ReefDbColumnIdentifier.newId(
			ControlProgramTableRowModel.PROPERTY_CODE,
			n("reefdb.property.code"),
			n("reefdb.rule.program.code.tip"),
			String.class);
	
	/** Constant <code>NAME</code> */
	public static final ReefDbColumnIdentifier<ControlProgramTableRowModel> NAME = ReefDbColumnIdentifier.newId(
			ControlProgramTableRowModel.PROPERTY_NAME,
			n("reefdb.property.name"),
			n("reefdb.rule.program.name.tip"),
			String.class);
	
	/** Constant <code>DESCRIPTION</code> */
	public static final ReefDbColumnIdentifier<ControlProgramTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
			ControlProgramTableRowModel.PROPERTY_COMMENT,
			n("reefdb.property.description"),
			n("reefdb.rule.program.description.tip"),
			String.class);
	
	
	/**
	 * <p>Constructor for ControlProgramTableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	ControlProgramTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ControlProgramTableRowModel createNewRow() {
		return new ControlProgramTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ControlProgramTableRowModel> getFirstColumnEditing() {
		return CODE;
	}
}
