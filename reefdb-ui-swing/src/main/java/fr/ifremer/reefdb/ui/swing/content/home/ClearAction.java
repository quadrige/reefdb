package fr.ifremer.reefdb.ui.swing.content.home;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

/**
 * Action permettant d effacer la recherche les obervations.
 */
public class ClearAction extends AbstractCheckModelAction<HomeUIModel, HomeUI, HomeUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public ClearAction(final HomeUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Delete filter model
        getModel().setCampaign(null);
        getModel().setProgram(null);
        getModel().setLocation(null);
        getModel().setSearchDate(null);
        getModel().setStartDate(null);
        getModel().setEndDate(null);
        getUI().getEndDateEditor().setEnabled(false);
        getUI().getEndDateEditor().setVisible(false);
        getUI().getAndLabel().setVisible(false);
        getModel().setState(null);
        getModel().setShare(null);
        getModel().setComment(null);

        // Clear result
        getModel().getSurveysTableUIModel().setBeans(null);

        getUI().getProgramCombo().requestFocus();
    }

    /** {@inheritDoc} */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().setModify(modelModify);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler();
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        return ImmutableList.of(
                getModel().getSurveysTableUIModel(),
                getModel().getOperationsTableUIModel()
        );
    }

}
