package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;

import static org.nuiton.i18n.I18n.n;

/**
 * Modele pour l onglet photo.
 */
public class PhotosTabUIModel extends AbstractReefDbTableUIModel<PhotoDTO, PhotosTableRowModel, PhotosTabUIModel> implements TabContentModel {

    /**
     * Constant <code>PROPERTY_OBSERVATION_MODEL="observationModel"</code>
     */
    public static final String PROPERTY_OBSERVATION_MODEL = "observationModel";
    /**
     * Constant <code>PROPERTY_PHOTO_INDEX="selectedPhoto"</code>
     */
    public static final String PROPERTY_PHOTO_INDEX = "selectedPhoto";
    private ObservationUIModel observationModel;
    /**
     * L'indice courant pour la photo.
     */
    private Integer photoIndex = null;
    private boolean modelAdjusting;

    /**
     * <p>Getter for the field <code>observationModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public ObservationUIModel getObservationModel() {
        return observationModel;
    }

    /**
     * <p>Setter for the field <code>observationModel</code>.</p>
     *
     * @param observationModel a {@link fr.ifremer.reefdb.ui.swing.content.observation.ObservationUIModel} object.
     */
    public void setObservationModel(ObservationUIModel observationModel) {
        this.observationModel = observationModel;
        firePropertyChange(PROPERTY_OBSERVATION_MODEL, null, observationModel);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEmpty() {
        return false; // getRowCount() == 0;
    }

    /** {@inheritDoc} */
    @Override
    public String getTitle() {
        return n("reefdb.photo.title");
    }

    /** {@inheritDoc} */
    @Override
    public String getIcon() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCloseable() {
        return false;
    }

    public PhotosTableRowModel getSelectedPhoto() {
        return getPhotoIndex() != null ? getRows().get(getPhotoIndex()) : null;
    }

    /**
     * <p>Getter for the field <code>photoIndex</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getPhotoIndex() {
        return photoIndex;
    }

    /**
     * <p>Setter for the field <code>photoIndex</code>.</p>
     *
     * @param photoIndex a {@link java.lang.Integer} object.
     */
    public void setPhotoIndex(Integer photoIndex) {
        Integer oldValue = getPhotoIndex();
        this.photoIndex = photoIndex != null
                ? Math.max(getFirstPhotoIndex(), Math.min(getLastPhotoIndex(), photoIndex))
                : null;
        firePropertyChange(PROPERTY_PHOTO_INDEX, oldValue, photoIndex);
    }

    /**
     * <p>getFirstPhotoIndex.</p>
     *
     * @return a int.
     */
    public int getFirstPhotoIndex() {
        return 0;
    }

    /**
     * <p>getLastPhotoIndex.</p>
     *
     * @return a int.
     */
    public int getLastPhotoIndex() {
        return getRowCount() - 1;
    }

    /**
     * <p>isModelAdjusting.</p>
     *
     * @return a boolean.
     */
    public boolean isModelAdjusting() {
        return modelAdjusting;
    }

    /**
     * <p>Setter for the field <code>modelAdjusting</code>.</p>
     *
     * @param modelAdjusting a boolean.
     */
    public void setModelAdjusting(boolean modelAdjusting) {
        this.modelAdjusting = modelAdjusting;
    }

    public boolean isExportEnabled() {
        return getSelectedRows().stream().anyMatch(PhotosTableRowModel::isFileExists);
    }

    public boolean isDownloadEnabled() {
        return getSelectedRows().stream().anyMatch(PhotosTableRowModel::isFileDownloadable);
    }
}
