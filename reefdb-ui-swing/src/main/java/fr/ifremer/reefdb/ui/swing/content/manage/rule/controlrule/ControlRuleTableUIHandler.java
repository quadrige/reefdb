package fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.dao.technical.decorator.DecoratorComparator;
import fr.ifremer.quadrige3.ui.swing.table.AbstractRowUIModel;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ButtonCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.FilterableComboBoxCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.PredicatedCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.renderer.ButtonCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.renderer.MultipleCellRenderer;
import fr.ifremer.quadrige3.ui.swing.table.renderer.PredicatedCellRenderer;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.dto.enums.ControlElementValues;
import fr.ifremer.reefdb.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.reefdb.dto.enums.ControlFunctionValues;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue.SelectQualitativeValueUI;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUI;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.numerical.RulePrecondNumUI;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.numerical.RulePrecondNumUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.qualitative.RulePrecondQualUI;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.controlrule.precondition.qualitative.RulePrecondQualUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Dimension;
import java.util.*;
import java.util.function.Predicate;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des regles
 */
public class ControlRuleTableUIHandler extends
        AbstractReefDbTableUIHandler<ControlRuleRowModel, ControlRuleTableUIModel, ControlRuleTableUI> {

    private FilterableComboBoxCellEditor<ControlFeatureDTO> controlFeatureEditor;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final ControlRuleTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlRuleTableUIModel model = new ControlRuleTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final ControlRuleTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Desactivation du bouton supprimer seulement
        getUI().getRemoveControlRuleButton().setEnabled(false);

        // Initialisation du tableau
        initTable();

        // Initialisation des listeners
        initListeners();

    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Code
        TableColumnExt codeCol = addColumn(ControlRuleTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // Fonction
        TableColumnExt functionCol = addFilterableComboDataColumnToModel(
                ControlRuleTableModel.FUNCTION,
                getContext().getSystemService().getFunctionsControlSystem(),
                false);
        functionCol.setSortable(true);
        functionCol.setPreferredWidth(200);

        // Element Controle
        TableColumnExt controlElementCol = addFilterableComboDataColumnToModel(
                ControlRuleTableModel.CONTROL_ELEMENT,
                getContext().getSystemService().getControlElements(),
                false);
        controlElementCol.setSortable(true);
        controlElementCol.setPreferredWidth(200);

        // Caracteristique Controle
        controlFeatureEditor = newFilterableComboBoxCellEditor(new ArrayList<>(), ControlFeatureDTO.class, false);
        TableColumnExt controlFeatureCol = addColumn(
                controlFeatureEditor,
                newTableCellRender(ControlFeatureDTO.class),
                ControlRuleTableModel.CONTROL_FEATURE);
        controlFeatureCol.setSortable(true);
        controlFeatureCol.setPreferredWidth(200);

        // Actif
        final TableColumnExt activeCol = addBooleanColumnToModel(ControlRuleTableModel.ACTIVE, getTable());
        activeCol.setSortable(true);

        // Bloquante
        final TableColumnExt blockingCol = addBooleanColumnToModel(ControlRuleTableModel.BLOCKING, getTable());
        blockingCol.setSortable(true);

        // Min
        TableColumnExt minColumn = addColumn(ControlRuleTableModel.MIN);
        minColumn.setSortable(true);

        // Max
        TableColumnExt maxColumn = addColumn(ControlRuleTableModel.MAX);
        maxColumn.setSortable(true);

        // specific renderer for min & max columns
        Map<Class, TableCellRenderer> minMaxRenderers = Maps.newHashMap();
        minMaxRenderers.put(Double.class, newNumberCellRenderer(10));
        minMaxRenderers.put(Date.class, newDateCellRenderer(getConfig().getDateFormat()));
        MultipleCellRenderer smartCellRenderer = new MultipleCellRenderer(minMaxRenderers);
        minColumn.setCellRenderer(smartCellRenderer);
        maxColumn.setCellRenderer(smartCellRenderer);

        // specific editors for min & max columns
        Map<Predicate<ControlRuleRowModel>, TableCellEditor> minMaxEditors = Maps.newHashMap();
        minMaxEditors.put(controlRule -> ControlFunctionValues.MIN_MAX_DATE.equals(controlRule.getFunction()), newDateCellEditor(getConfig().getDateFormat()));
        minMaxEditors.put(controlRule -> ControlFunctionValues.MIN_MAX.equals(controlRule.getFunction()), newNumberCellEditor(Double.class, true, ReefDbUI.SIGNED_HIGH_DECIMAL_DIGITS_PATTERN));
        PredicatedCellEditor<ControlRuleRowModel> minMaxCellEditor = new PredicatedCellEditor<>(minMaxEditors);
        minColumn.setCellEditor(minMaxCellEditor);
        maxColumn.setCellEditor(minMaxCellEditor);
        // TODO maybe choose Integer or Double (with precision) depending on witch feature is controlled

        // Valeurs Autorisees
        TableColumnExt allowedValuesCol = addColumn(ControlRuleTableModel.ALLOWED_VALUES);
        allowedValuesCol.setSortable(true);

        allowedValuesCol.setCellEditor(new PredicatedCellEditor<>(buildAllowedValuesEditors()));

        // the same predicate for renderers
        allowedValuesCol.setCellRenderer(new PredicatedCellRenderer<>(buildAllowedValuesRenderers()));

        ControlRuleTableModel tableModel = new ControlRuleTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable());

    }

    private Map<Predicate<ControlRuleRowModel>, TableCellEditor> buildAllowedValuesEditors() {
        Map<Predicate<ControlRuleRowModel>, TableCellEditor> allowedValuesEditors = Maps.newHashMap();

        // specific editor for allowed values if controlled attribute is a qualitative value
        allowedValuesEditors.put(ControlRuleRowModel.qualitativeValuePredicate, new ButtonCellEditor() {

            private String allowedValues;

            @Override
            public void onButtonCellAction(int row, int column) {
                int rowModelIndex = getTable().convertRowIndexToModel(row);
                ControlRuleRowModel rowModel = getTableModel().getEntry(rowModelIndex);
                allowedValues = rowModel.getAllowedValues();

                if (CollectionUtils.isNotEmpty(rowModel.getRulePmfms())) {

                    // build available qualitative values
                    Set<QualitativeValueDTO> availableList = Sets.newHashSet();
                    for (RulePmfmDTO rulePmfm : rowModel.getRulePmfms()) {
                        // the PmfmDTO list is not from referential but from RulePmfm, so we need to get the real pmfm
                        List<PmfmDTO> refPmfms = getContext().getReferentialService().searchPmfms(StatusFilter.ALL,
                                rulePmfm.getPmfm().getParameter().getCode(), // only parameter is mandatory
                                rulePmfm.getPmfm().getMatrix() != null ? rulePmfm.getPmfm().getMatrix().getId() : null,
                                rulePmfm.getPmfm().getFraction() != null ? rulePmfm.getPmfm().getFraction().getId() : null,
                                rulePmfm.getPmfm().getMethod() != null ? rulePmfm.getPmfm().getMethod().getId() : null,
                                rulePmfm.getPmfm().getUnit() != null ? rulePmfm.getPmfm().getUnit().getId() : null,
                                null,
                                null);
                        // gather all qualitative values
                        for (PmfmDTO refPmfm : refPmfms) {
                            availableList.addAll(refPmfm.getQualitativeValues());
                        }
                    }
                    // build selected
                    List<QualitativeValueDTO> selectedList = Lists.newArrayList();
                    if (StringUtils.isNotBlank(allowedValues)) {
                        Set<Integer> qvIds = ReefDbBeans.getIntegerSetFromString(allowedValues, getConfig().getValueSeparator());
                        selectedList.addAll(getContext().getReferentialService().getQualitativeValues(qvIds));
                    }

                    // open select qualitative values ui
                    SelectQualitativeValueUI selectQualitativeValueUI = new SelectQualitativeValueUI(getContext());
                    List<QualitativeValueDTO> orderedAvailableList = new ArrayList<>(availableList);
                    orderedAvailableList.sort(new DecoratorComparator<>(getDecorator(QualitativeValueDTO.class, null)));
                    selectQualitativeValueUI.getModel().setAvailableList(orderedAvailableList);
                    selectQualitativeValueUI.getModel().setSelectedList(selectedList);
                    selectQualitativeValueUI.getModel().setEditable(getModel().getParentModel().isEditable());
                    openDialog(selectQualitativeValueUI, new Dimension(640, 480));

                    if (selectQualitativeValueUI.getModel().isValid()) {
                        // if user validates, build allowed values from qualitative value ids
                        allowedValues = ReefDbBeans.joinIds(selectQualitativeValueUI.getModel().getSelectedList(), getConfig().getValueSeparator());
                    }
                    getTable().editingStopped(null);
                }
            }

            @Override
            public Object getCellEditorValue() {
                return allowedValues;
            }
        });

        // specific editor for preconditions (qualitative)
        allowedValuesEditors.put(ControlRuleRowModel.preconditionQualitativePredicate, new ButtonCellEditor() {
            @Override
            public void onButtonCellAction(int row, int column) {
                int rowModelIndex = getTable().convertRowIndexToModel(row);
                ControlRuleRowModel rowModel = getTableModel().getEntry(rowModelIndex);

                if (rowModel.sizeRulePmfms() != 2) return;

                RulePrecondQualUI rulePreconditionUI = new RulePrecondQualUI(getContext());
                RulePrecondQualUIModel model = rulePreconditionUI.getModel();
                model.setQvMap(getContext().getRuleListService().buildQualitativeValueMapFromPreconditions(rowModel.getPreconditions()));
                model.setBasePmfm(rowModel.getRulePmfms(0).getPmfm());
                model.setUsedPmfm(rowModel.getRulePmfms(1).getPmfm());
                model.setEditable(getModel().getParentModel().isEditable());

                openDialog(rulePreconditionUI, new Dimension(600, 800));

                if (model.isModify() && model.isValid()) {
                    getContext().getRuleListService().buildPreconditionsFromQualitativeValueMap(rowModel, model.getQvMap());
                    getModel().setModify(true);
                }

            }
        });

        // specific editor for preconditions (numerical)
        allowedValuesEditors.put(ControlRuleRowModel.preconditionNumericalPredicate, new ButtonCellEditor() {
            @Override
            public void onButtonCellAction(int row, int column) {
                int rowModelIndex = getTable().convertRowIndexToModel(row);
                ControlRuleRowModel rowModel = getTableModel().getEntry(rowModelIndex);

                if (rowModel.sizeRulePmfms() != 2) return;

                RulePrecondNumUI rulePreconditionUI = new RulePrecondNumUI(getContext());
                RulePrecondNumUIModel model = rulePreconditionUI.getModel();
                model.setBasePmfm(rowModel.getRulePmfms(0).getPmfm());
                model.setUsedPmfm(rowModel.getRulePmfms(1).getPmfm());
                model.setBeans(getContext().getRuleListService().buildNumericPreconditionListFromPreconditions(rowModel.getPreconditions()));
                model.setEditable(getModel().getParentModel().isEditable());

                openDialog(rulePreconditionUI, new Dimension(600, 800));

                if (model.isModify() && model.isValid()) {
                    getContext().getRuleListService().buildPreconditionsFromNumericPreconditionList(rowModel, model.getBeans());
                    getModel().setModify(true);
                }

            }
        });

        return allowedValuesEditors;
    }

    private Map<Predicate<ControlRuleRowModel>, TableCellRenderer> buildAllowedValuesRenderers() {

        Map<Predicate<ControlRuleRowModel>, TableCellRenderer> allowedValuesRenderers = Maps.newHashMap();

        // renderer for qualitative values
        allowedValuesRenderers.put(ControlRuleRowModel.qualitativeValuePredicate, new AllowedValuesCellRenderer("qualitative-value"));

        // renderer for preconditions (qualitative)
        allowedValuesRenderers.put(ControlRuleRowModel.preconditionQualitativePredicate, new ButtonCellRenderer(SwingUtil.createActionIcon("associated-qualitative-value"), true));

        // renderer for preconditions (numerical)
        allowedValuesRenderers.put(ControlRuleRowModel.preconditionNumericalPredicate, new ButtonCellRenderer(SwingUtil.createActionIcon("associated-numerical-value"), true));

        return allowedValuesRenderers;
    }

    private void initListeners() {

        // Table listener
        getModel().addPropertyChangeListener(ControlRuleTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            // Si un seul element a ete selectionne
            final ControlRuleDTO controlRule = getModel().getSingleSelectedRow();

            configureColumns(controlRule);

            // Chargement des PSFMControle
            loadPmfmTable(controlRule);

            // Chargement des commentaires/messages affiches
            loadControlRuleInformation(controlRule);
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{ControlRuleRowModel.PROPERTY_ERRORS, ControlRuleRowModel.PROPERTY_PMFM_VALID};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, ControlRuleRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        if (ControlRuleRowModel.PROPERTY_FUNCTION.equals(propertyName) || ControlRuleRowModel.PROPERTY_CONTROL_ELEMENT.equals(propertyName)) {
            configureColumns(row);
        }

        if (ControlRuleRowModel.PROPERTY_FUNCTION.equals(propertyName) && row.getFunction() != null) {
            // reset min max or allowed values
            if (!ControlFunctionValues.MIN_MAX.equals(row.getFunction()) && !ControlFunctionValues.MIN_MAX_DATE.equals(row.getFunction())) {
                row.setMin(null);
                row.setMax(null);
            }
            if (!ControlFunctionValues.IS_AMONG.equals(row.getFunction())) {
                row.setAllowedValues(null);
            }
            // reset preconditions
            if (!ReefDbBeans.isPreconditionRule(row)) {
                row.setPreconditions(null);
            }
            // reset groups
            if (!ReefDbBeans.isGroupedRule(row)) {
                row.setGroups(null);
            } else {
                // build the intern rules and groups
                getContext().getRuleListService().buildNotEmptyConditionalGroupsFromGroupedRule(row);
            }
            // reset rulePmfm ids (Mantis #45625)
            resetRulePmfmIds(row);

        }

        if (ControlRuleRowModel.PROPERTY_CONTROL_ELEMENT.equals(propertyName)) {
            row.setControlFeature(null);
        }

        if (ControlRuleRowModel.PROPERTY_CONTROL_FEATURE.equals(propertyName)) {

            // reset pmfm list if not mandatory
            if (!ReefDbBeans.isPmfmMandatory(row) && !ReefDbBeans.isPreconditionRule(row) && !ReefDbBeans.isGroupedRule(row)) {
                row.setRulePmfms(null);
            }

            // reload pmfms
            loadPmfmTable(row);
        }

        if (ControlRuleRowModel.PROPERTY_RULE_PMFMS.equals(propertyName) && ReefDbBeans.isPreconditionRule(row)) {
            // Clear preconditions when the pmfms are not valid (Mantis #46663)
            if (CollectionUtils.size(newValue) < 2) {
                row.getPreconditions().clear();
            }
        }

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        saveToParentModel();
    }

    private void resetRulePmfmIds(ControlRuleRowModel row) {
        if (row == null) return;
        row.getRulePmfms().forEach(rulePmfm -> rulePmfm.setId(null));
    }

    public void onPmfmModified(ControlRuleRowModel row) {

        // fix Mantis #45309
        if (row == null) return;

        if (ReefDbBeans.isGroupedRule(row)) {
            getContext().getRuleListService().buildNotEmptyConditionalGroupsFromGroupedRule(row);
        }

        recomputeRowValidState(row);
    }

    private void loadPmfmTable(ControlRuleDTO controlRule) {
        getRulesUI().getControlPmfmTableUI().getHandler().loadPmfms(controlRule);
    }

    private void saveToParentModel() {
        getModel().getParentModel().getSelectedRuleList().setControlRules(getModel().getBeans());
        recomputeRowsValidState(false);

        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<ControlRuleRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            ControlRuleRowModel row = addedRows.get(0);

            // create a new rule by service
            ControlRuleDTO newRule = getContext().getRuleListService().newControlRule(getModel().getParentModel().getSelectedRuleList());
            // convert it to the added row
            row.fromBean(newRule);

            newRule.setNewCode(true);
            getModel().setModify(true);
            setFocusOnCell(row);
        }
    }

    /**
     * <p>removeRules.</p>
     */
    void removeRules() {
        if (getContext().getDialogHelper().showConfirmDialog(
                t("reefdb.rule.controlRule.delete.message"),
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

            getModel().deleteSelectedRows();
            saveToParentModel();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(ControlRuleRowModel row) {
        return super.isRowValid(row) && isControlRuleValid(row);
    }

    private boolean isControlRuleValid(ControlRuleRowModel row) {

        row.getErrors().clear();

        // check code length (rule_cd : 40 characters max)
        if (StringUtils.length(row.getCode()) > 40) {
            ReefDbBeans.addError(row, t("reefdb.rule.controlRule.code.tooLong", 40), ControlRuleRowModel.PROPERTY_CODE);
        }

        // check mandatory PMFM
        row.setPmfmValid(true);
        if (ReefDbBeans.isPmfmMandatory(row)) {

            if (ReefDbBeans.isPreconditionRule(row)) {
                if (ControlFunctionValues.PRECONDITION_QUALITATIVE.equals(row.getFunction())) {
                    if (row.sizeRulePmfms() != 2 || !row.getRulePmfms(0).getPmfm().getParameter().isQualitative() || !row.getRulePmfms(1).getPmfm().getParameter().isQualitative()) {
                        row.setPmfmValid(false);
                        ReefDbBeans.addError(row, t("reefdb.rule.rulePrecondition.invalidQualitativePmfm"), ControlRuleRowModel.PROPERTY_CODE);
                    }
                } else if (ControlFunctionValues.PRECONDITION_NUMERICAL.equals(row.getFunction())) {
                    if (row.sizeRulePmfms() != 2 || !row.getRulePmfms(0).getPmfm().getParameter().isQualitative() || row.getRulePmfms(1).getPmfm().getParameter().isQualitative()) {
                        row.setPmfmValid(false);
                        ReefDbBeans.addError(row, t("reefdb.rule.rulePrecondition.invalidNumericalPmfm"), ControlRuleRowModel.PROPERTY_CODE);
                    }
                }
            } else {
                if (row.isRulePmfmsEmpty()) {
                    row.setPmfmValid(false);
                    ReefDbBeans.addError(row, t("reefdb.rule.controlRule.noPmfm"), ControlRuleRowModel.PROPERTY_CODE);
                }
            }
        }

        // check min max
        if (ControlFunctionValues.MIN_MAX.equals(row.getFunction())) {
            Number min = (row.getMin() instanceof Number) ? (Number) row.getMin() : null;
            Number max = (row.getMax() instanceof Number) ? (Number) row.getMax() : null;
            if (min == null && max == null) {
                ReefDbBeans.addError(row, t("reefdb.rule.controlRule.minMaxEmpty"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            } else if (min != null && max != null && min.doubleValue() > max.doubleValue()) {
                ReefDbBeans.addError(row, t("reefdb.rule.controlRule.minGreaterThanMax"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            }
        }

        // check min max date
        if (ControlFunctionValues.MIN_MAX_DATE.equals(row.getFunction())) {
            Date min = (row.getMin() instanceof Date) ? (Date) row.getMin() : null;
            Date max = (row.getMax() instanceof Date) ? (Date) row.getMax() : null;
            if (min == null && max == null) {
                ReefDbBeans.addError(row, t("reefdb.rule.controlRule.minMaxDateEmpty"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            } else if (min != null && max != null && min.after(max)) {
                ReefDbBeans.addError(row, t("reefdb.rule.controlRule.minDateGreaterThanMaxDate"),
                        ControlRuleRowModel.PROPERTY_MIN, ControlRuleRowModel.PROPERTY_MAX);
            }
        }

        // check allowed values separator
        if (ControlFunctionValues.IS_AMONG.equals(row.getFunction())) {
            if (StringUtils.isNotBlank(row.getAllowedValues())) {
                if (row.getAllowedValues().contains(" ") && !row.getAllowedValues().contains(getConfig().getValueSeparator())) {
                    // the value separator is not present in allowedValues
                    ReefDbBeans.addError(row,
                            t("reefdb.rule.controlRule.allowedValues.noSeparator", getConfig().getValueSeparator()),
                            ControlRuleRowModel.PROPERTY_ALLOWED_VALUES);
                }
            } else {
                // no allowed values
                ReefDbBeans.addError(row,
                        t("reefdb.rule.controlRule.allowedValues.empty"),
                        ControlRuleRowModel.PROPERTY_ALLOWED_VALUES);

            }
        }

        // check preconditions validity to avoid save action to be performed with empty preconditions (Mantis #41964)
        if (ReefDbBeans.isPreconditionRule(row) && CollectionUtils.isEmpty(row.getPreconditions()) && row.isPmfmValid()) {
            ReefDbBeans.addError(row,
                    t("reefdb.rule.controlRule.preconditions.empty"),
                    ControlRuleRowModel.PROPERTY_ALLOWED_VALUES);
        }

        return ReefDbBeans.hasNoBlockingError(row);
    }

    private void configureColumns(ControlRuleDTO rule) {
        if (rule == null) {
            return;
        }

        // configure control feature combo
        if (rule.getControlElement() != null) {
            List<ControlFeatureDTO> controlFeatures = getContext().getSystemService().getControlFeatures(rule.getControlElement());
            controlFeatureEditor.getCombo().setData(controlFeatures);
        } else {
            controlFeatureEditor.getCombo().setData(null);
        }

        // special case for precondition
        if (ReefDbBeans.isPreconditionRule(rule)) {
            rule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            rule.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        } else if (ReefDbBeans.isGroupedRule(rule)) {
            rule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            rule.setControlFeature(ControlFeatureMeasurementValues.PMFM.toControlFeatureDTO());
        }

        getTable().repaint();
    }

    /**
     * <p>loadControlRuleInformation.</p>
     *
     * @param controlRule a {@link fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO} object.
     */
    private void loadControlRuleInformation(final ControlRuleDTO controlRule) {

        final RulesUI rulesUI = getRulesUI();

        rulesUI.getModel().setLoading(true);

        if (controlRule != null) {
            rulesUI.getRuleMessageEditor().setEnabled(getModel().getParentModel().isEditable());
            rulesUI.getRuleDescriptionEditor().setEnabled(getModel().getParentModel().isEditable());
            rulesUI.getModel().setRuleMessage(controlRule.getMessage());
            rulesUI.getModel().setRuleDescription(controlRule.getDescription());
        } else {
            rulesUI.getRuleMessageEditor().setEnabled(false);
            rulesUI.getRuleDescriptionEditor().setEnabled(false);
            rulesUI.getModel().setRuleMessage(null);
            rulesUI.getModel().setRuleDescription(null);
        }
        rulesUI.getModel().setLoading(false);
    }

    /**
     * <p>clearControlRuleInformation.</p>
     */
    public void clearControlRuleInformation() {

        final RulesUI rulesUI = getRulesUI();
        rulesUI.getModel().setLoading(true);
        rulesUI.getRuleMessageEditor().setEnabled(false);
        rulesUI.getRuleDescriptionEditor().setEnabled(false);
        rulesUI.getModel().setRuleMessage(null);
        rulesUI.getModel().setRuleDescription(null);
        rulesUI.getModel().setLoading(false);
    }

    /**
     * <p>loadControlRules.</p>
     *
     * @param controlRules a {@link java.util.Collection} object.
     */
    public void loadControlRules(final Collection<ControlRuleDTO> controlRules) {

        // Stop table edition to avoid rendering exception (Mantis #49268)
        getTable().editingStopped(null);

        // Load les controlRule dans le model
        getModel().setBeans(controlRules);
        getModel().getRows().forEach(controlRuleRowModel -> controlRuleRowModel.setEditable(getModel().getParentModel().isEditable()));

        // Activation du bouton nouveau
        getUI().applyDataBinding(ControlRuleTableUI.BINDING_ADD_CONTROL_RULE_BUTTON_ENABLED);

        recomputeRowsValidState(false);
        getModel().setModify(false);
    }

    public boolean canDeleteSelectedRows() {
        return getModel().getSelectedRows().stream().allMatch(AbstractRowUIModel::isEditable);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {

        // Suppression des programmesControle dans le model
        getModel().setBeans(null);

        // Desactivation du bouton nouveau
        getUI().getAddControlRuleButton().setEnabled(false);
    }

    void editRuleCode() {
        ControlRuleRowModel row = getModel().getSingleSelectedRow();
        if (row != null && row.isNewCode()) {
            if (checkNewCode(row)) {
                getModel().getParentModel().getSelectedRuleList().setDirty(true);
            }
        }
    }

    private boolean checkNewCode(ControlRuleRowModel row) {

        boolean edit = StringUtils.isNotBlank(row.getCode());

        // ask for new code
        String newCode = (String) getContext().getDialogHelper().showInputDialog(
                getUI(),
                t("reefdb.rule.controlRule.setCode"),
                edit ? t("reefdb.rule.controlRule.editCode.title") : t("reefdb.rule.controlRule.setCode.title"),
                null,
                row.getCode());

        if (checkCodeDuplicates(newCode, row, edit)) {
            row.setCode(newCode);
            return true;
        }

        return false;
    }

    /**
     * <p>checkCodeDuplicates.</p>
     *
     * @param newCode    a {@link java.lang.String} object.
     * @param currentRow a {@link ControlRuleRowModel} object.
     * @param edit       a boolean.
     * @return a boolean.
     */
    private boolean checkCodeDuplicates(String newCode, ControlRuleRowModel currentRow, boolean edit) {

        if (StringUtils.isBlank(newCode)) {
            return false;
        }

        newCode = newCode.trim();

        // check if new code is already in UI
        for (ControlRuleRowModel otherRow : getModel().getRows()) {
            if (currentRow == otherRow) continue;
            if (newCode.equalsIgnoreCase(otherRow.getCode())) {
                getContext().getDialogHelper().showErrorDialog(
                        t("reefdb.error.alreadyExists.referential", t("reefdb.rule.controlRule.title"), newCode, t("reefdb.property.referential.national")),
                        edit ? t("reefdb.rule.controlRule.editCode.title") : t("reefdb.rule.controlRule.setCode.title")
                );
                return false;
            }
        }

        // check if new code is already in bdd
        if (getContext().getRuleListService().ruleCodeExists(newCode)) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.error.alreadyExists.referential", t("reefdb.rule.controlRule.title"), newCode, t("reefdb.property.referential.national")),
                    edit ? t("reefdb.rule.controlRule.editCode.title") : t("reefdb.rule.controlRule.setCode.title")
            );
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<ControlRuleRowModel> getTableModel() {
        return (ControlRuleTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getControlRuleTable();
    }

    /**
     * <p>getRulesUI.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUI} object.
     */
    private RulesUI getRulesUI() {
        return getUI().getParentContainer(RulesUI.class);
    }

    private class AllowedValuesCellRenderer extends ButtonCellRenderer<String> {

        AllowedValuesCellRenderer(String actionIconName) {
            super(SwingUtil.createActionIcon(actionIconName), false);
        }

        @Override
        protected String getText(JTable table, String value, int row, int column) {
            return value == null ? "0" : String.valueOf(new StringTokenizer(value, getConfig().getValueSeparator()).countTokens());
        }
    }
}
