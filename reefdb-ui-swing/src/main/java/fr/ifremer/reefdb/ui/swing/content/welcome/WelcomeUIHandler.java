package fr.ifremer.reefdb.ui.swing.content.welcome;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;

/**
 * Controller for Welcome scereen.
 */
public class WelcomeUIHandler extends AbstractReefDbUIHandler<ReefDbUIContext, WelcomeUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(WelcomeUIHandler.class);

    /**
     * Button font.
     */
    private static final Font BUTTON_FONT = new Font("", Font.BOLD, 40);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final WelcomeUI ui) {
        super.beforeInit(ui);

        // Ajout du model pour la page d acceuil
        ui.setContextValue(getContext());

        ui.setContextValue(SwingUtil.createActionIcon("home"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final WelcomeUI ui) {
        initUI(ui);

        // Change font on title
        getUI().getSaisieButton().setEnabled(getContext().isAuthenticated());
        getUI().getSaisieButton().setFont(BUTTON_FONT);
        URL imageURL = getClass().getResource("/image/plongeurs.jpg");
        if (imageURL != null) {

            // Load image for label
            ImageIcon image = new ImageIcon(imageURL);

            // Add image on label
            getUI().getSaisieButton().setIcon(image);
            getUI().getSaisieButton().setPreferredSize(new Dimension(image.getIconWidth(), image.getIconHeight()));

            getUI().getSaisieButton().setHorizontalTextPosition(SwingConstants.CENTER);
            getUI().getSaisieButton().setForeground(Color.WHITE);
        }

        getUI().getExtractionButton().setEnabled(getContext().isAuthenticated());
        getUI().getExtractionButton().setFont(BUTTON_FONT);
        imageURL = getClass().getResource("/image/poissons.jpg");
        if (imageURL != null) {

            // Load image for label
            ImageIcon image = new ImageIcon(imageURL);

            // Add image on label
            getUI().getExtractionButton().setIcon(image);
            getUI().getExtractionButton().setPreferredSize(new Dimension(image.getIconWidth(), image.getIconHeight()));

            getUI().getExtractionButton().setHorizontalTextPosition(SwingConstants.CENTER);
            getUI().getExtractionButton().setForeground(Color.WHITE);
        }

    }

}
