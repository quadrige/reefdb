package fr.ifremer.reefdb.ui.swing.content.manage.filter.taxongroup.element;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.menu.TaxonGroupMenuUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Controler.
 */
public class FilterElementTaxonGroupUIHandler extends AbstractFilterElementUIHandler<TaxonGroupDTO, FilterElementTaxonGroupUI, TaxonGroupMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(FilterElementTaxonGroupUIHandler.class);

    /** {@inheritDoc} */
    @Override
    protected TaxonGroupMenuUI createNewReferentialMenuUI() {
        return new TaxonGroupMenuUI(getUI());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(FilterElementTaxonGroupUI ui) {

        // Force bean type on double list
        getUI().getFilterDoubleList().setBeanType(TaxonGroupDTO.class);

        super.afterInit(ui);

        // hide code / name editors
        getReferentialMenuUI().getLabelEditor().getParent().setVisible(false);
        getReferentialMenuUI().getNameEditor().getParent().setVisible(false);

    }


}
