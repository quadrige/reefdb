package fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;
import fr.ifremer.quadrige3.ui.swing.component.bean.ExtendedComboBox;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.FilterableComboBoxCellEditor;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.ui.swing.content.manage.context.ManageContextsUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.SortOrder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ManageFiltersListTableUIHandler class.</p>
 */
public class ManageFiltersListTableUIHandler extends
        AbstractReefDbTableUIHandler<ManageFiltersListTableUIRowModel, ManageFiltersListTableUIModel, ManageFiltersListTableUI> {

    /**
     * {@inheritDoc}
     *
     * Logger.
     */
//    private static final Log LOG = LogFactory.getLog(ManageFiltersListTableUIHandler.class);
    @Override
    public void beforeInit(final ManageFiltersListTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageFiltersListTableUIModel model = new ManageFiltersListTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageFiltersListTableUI ui) {

        // Initialize the screen
        initUI(ui);

        // Initialisatize the table
        initializeTable();

        // Initialisatize the listeners
        initListeners();
    }

    /**
     * Initialisation du tableau.
     */
    private void initializeTable() {

        // type
        TableColumnExt typeCol = addColumn(ManageFiltersListTableUITableModel.TYPE);
        typeCol.setSortable(true);
        typeCol.setEditable(false);

        // Filter column
        final TableColumnExt filterCol = addFilterComboDataColumnToModel();
        filterCol.setEditable(true);

        ManageFiltersListTableUITableModel tableModel = new ManageFiltersListTableUITableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable(), true);

        getTable().setSortOrder(ManageFiltersListTableUITableModel.TYPE, SortOrder.ASCENDING);

        getTable().setVisibleRowCount(FilterTypeValues.values().length);
    }

    /**
     * Add caracteristique controle column into model.
     *
     * @return Column
     */
    private TableColumnExt addFilterComboDataColumnToModel() {

        // Column identifier
        final ReefDbColumnIdentifier<ManageFiltersListTableUIRowModel> identifier = ManageFiltersListTableUITableModel.FILTER;

        // Decorator name
        final String decoratorName = identifier.getDecoratorName();

        // Specific editor
        final FilterableComboBoxCellEditor<FilterDTO> editor = getFilterComboCellEditor(decoratorName);

        editor.addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingStopped(ChangeEvent e) {
                FilterComboCellEditor comboCellEditor = (FilterComboCellEditor) e.getSource();
                FilterDTO filter = (FilterDTO) comboCellEditor.getCellEditorValue();
                int filterTypeId = getModel().getSingleSelectedRow().getFilterTypeId();

                // ensure filterTypeId are equals
                if (filter != null) {
                    Assert.equals(filterTypeId, filter.getFilterTypeId());
                }

                replaceFilterType(filterTypeId, filter);
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
            }
        });

        // Add column to model
        return addColumn(editor, newTableCellRender(FilterDTO.class, decoratorName), identifier);
    }

    private void replaceFilterType(int filterTypeId, FilterDTO filter) {

        // remove old filter
        FilterDTO oldFilter = ReefDbBeans.findByProperty(getModel().getLocalContext().getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID, filterTypeId);
        if (oldFilter != null) {
            getModel().getLocalContext().removeFilters(oldFilter);
            getModel().setModify(true);
        }

        // add new filter
        if (filter != null) {
            getModel().getLocalContext().addFilters(filter);
            getModel().setModify(true);
        }
    }

    /**
     * Caracteristique Controle Editor.
     *
     * @param decoratorName Decorator name
     * @return Editor
     */
    private FilterComboCellEditor getFilterComboCellEditor(final String decoratorName) {

        // Combobox
        final ExtendedComboBox<FilterDTO> comboBox = new ExtendedComboBox<>(ui);
        comboBox.setFilterable(true);
        comboBox.setShowReset(false);
        comboBox.setShowDecorator(false);
        comboBox.setBeanType(FilterDTO.class);
        comboBox.setAutoFocus(false);

        // Decorator
        final Decorator<FilterDTO> decorator = getContext().getDecoratorService().getDecoratorByType(
                FilterDTO.class, decoratorName);

        // add listener to filter combo change
        comboBox.getCombobox().addItemListener(event -> {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                FilterDTO filter = null;
                if (event.getItem() instanceof FilterDTO) {
                    filter = (FilterDTO) event.getItem();
                }
                // updates element list
                getUI().getParentContainer(ManageContextsUI.class).getHandler().loadFilterElements(filter);

            } else if (event.getStateChange() == ItemEvent.DESELECTED) {

                getUI().getParentContainer(ManageContextsUI.class).getHandler().loadFilterElements(null);
            }
        });

        // Editor
        return new FilterComboCellEditor(getModel(), comboBox, decorator, getContext().getContextService());
    }

    private void initListeners() {

        getModel().addPropertyChangeListener(ManageFiltersListTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            if (getTable().getSelectedRow() > -1) {
                final ManageFiltersListTableUIRowModel rowModel = getModel().getSingleSelectedRow();
                getUI().getParentContainer(ManageContextsUI.class).getHandler().loadFilterElements(rowModel.getFilter());
            }
        });

    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ManageFiltersListTableUIRowModel> getTableModel() {
        return (ManageFiltersListTableUITableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageFiltersListTable();
    }

    /**
     * <p>disable.</p>
     */
    public void disable() {
        getTable().setEnabled(false);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {
        loadContext(null);
    }

    /**
     * <p>loadContext.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public void loadContext(final ContextDTO context) {
        // Load context
        getModel().setLocalContext(context);

        if (context != null) {
            getTable().setEnabled(true);
            List<ManageFiltersListTableUIRowModel> rows = new ArrayList<>(FilterTypeValues.values().length);

            for (FilterTypeValues contextFilter : FilterTypeValues.values()) {
                ManageFiltersListTableUIRowModel row = getTableModel().createNewRow();
                row.setValid(true);
                row.setType(contextFilter.getLabel());
                row.setFilterTypeId(contextFilter.getFilterTypeId());

                // find a filter in context
                if (CollectionUtils.isNotEmpty(context.getFilters())) {
                    row.setFilter(ReefDbBeans.findByProperty(context.getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID, contextFilter.getFilterTypeId()));
                }

                rows.add(row);
            }
            getModel().setRows(rows);
        } else {
            getModel().setRows(null);
        }
    }

}
