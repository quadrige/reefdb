package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local.replace;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractReplaceAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.ManageTaxonsUI;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 */
public class ReplaceTaxonAction extends AbstractReplaceAction<TaxonDTO, ReplaceTaxonUIModel, ReplaceTaxonUI, ReplaceTaxonUIHandler> {

    /**
     * <p>Constructor for ReplaceTaxonAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local.replace.ReplaceTaxonUIHandler} object.
     */
    public ReplaceTaxonAction(ReplaceTaxonUIHandler handler) {
        super(handler);
    }

    /** {@inheritDoc} */
    @Override
    protected String getReferentialLabel() {
        return t("reefdb.property.taxon");
    }

    /** {@inheritDoc} */
    @Override
    protected boolean prepareReplaceReferential(ReferentialService service, TaxonDTO source, TaxonDTO target) {

        if (service.isTaxonNameUsedInValidatedData(source.getId())) {
            String decoratedSource = decorate(source);
            if (getContext().getDialogHelper().showConfirmDialog(getReplaceUI(), t("reefdb.replaceReferential.used.validatedData.message", decoratedSource, decoratedSource),
                    getReplaceUI().getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return false;
            }
            setDelete(false);
        }

        return true;

    }

    /** {@inheritDoc} */
    @Override
    protected void replaceReferential(ReferentialService service, TaxonDTO source, TaxonDTO target, boolean delete) {

        service.replaceTaxon(source, target, delete);
    }

    /** {@inheritDoc} */
    @Override
    protected void resetCaches() {
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        final ManageTaxonsUI ui = (ManageTaxonsUI) getContext().getMainUI().getHandler().getCurrentBody();
        // reload table
        if (ui != null) {
            SwingUtilities.invokeLater(() -> getActionEngine().runAction(ui.getTaxonsLocalUI().getTaxonsLocalMenuUI().getSearchButton()));
        }

    }
}
