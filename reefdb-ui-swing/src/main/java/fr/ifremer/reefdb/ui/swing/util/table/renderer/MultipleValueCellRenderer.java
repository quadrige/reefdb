package fr.ifremer.reefdb.ui.swing.util.table.renderer;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2020 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 24/01/2020.
 */
public class MultipleValueCellRenderer implements TableCellRenderer {

    private final TableCellRenderer delegate;
    private final JLabel multiValueLabel;

    public MultipleValueCellRenderer(TableCellRenderer delegate) {
        this.delegate = delegate;
        multiValueLabel = createMultiValueLabel();
    }

    private JLabel createMultiValueLabel() {
        JLabel label = new JLabel(t("reefdb.measurement.grouped.multiEdit.multiValueLabel"));
        label.setOpaque(true);
        return label;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return value == null
            ? multiValueLabel
            : delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
