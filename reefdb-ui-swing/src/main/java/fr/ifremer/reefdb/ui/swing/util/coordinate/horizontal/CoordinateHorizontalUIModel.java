package fr.ifremer.reefdb.ui.swing.util.coordinate.horizontal;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;

/**
 * Model CoordinateUI.
 */
public class CoordinateHorizontalUIModel extends AbstractReefDbBeanUIModel<CoordinateDTO, CoordinateHorizontalUIModel> implements CoordinateDTO {
    
    private static final Binder<CoordinateHorizontalUIModel, CoordinateDTO> TO_BEAN_BINDER =
    		BinderFactory.newBinder(CoordinateHorizontalUIModel.class, CoordinateDTO.class);

    private static final Binder<CoordinateDTO, CoordinateHorizontalUIModel> FROM_BEAN_BINDER =
    		BinderFactory.newBinder(CoordinateDTO.class, CoordinateHorizontalUIModel.class);
    
    /**
     * Constructor.
     */
    public CoordinateHorizontalUIModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
    }

	/** {@inheritDoc} */
	@Override
	protected CoordinateDTO newBean() {
		return ReefDbBeanFactory.newCoordinateDTO();
	}

	/** {@inheritDoc} */
	@Override
	public Double getMaxLongitude() {
		return delegateObject.getMaxLongitude();
	}

	/** {@inheritDoc} */
	@Override
	public void setMaxLongitude(Double longitudeMax) {
		delegateObject.setMaxLongitude(longitudeMax);
	}

	/** {@inheritDoc} */
	@Override
	public Double getMinLongitude() {
		return delegateObject.getMinLongitude();
	}

	/** {@inheritDoc} */
	@Override
	public void setMinLongitude(Double longitudeMin) {
		delegateObject.setMinLongitude(longitudeMin);
	}

	/** {@inheritDoc} */
	@Override
	public Double getMaxLatitude() {
		return delegateObject.getMaxLatitude();
	}

	/** {@inheritDoc} */
	@Override
	public void setMaxLatitude(Double latitudeMax) {
		delegateObject.setMaxLatitude(latitudeMax);
	}

	/** {@inheritDoc} */
	@Override
	public Double getMinLatitude() {
		return delegateObject.getMinLatitude();
	}

	/** {@inheritDoc} */
	@Override
	public void setMinLatitude(Double latitudeMin) {
		delegateObject.setMinLatitude(latitudeMin);
	}
}
