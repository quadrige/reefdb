package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.locations;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.StrategiesLieuxUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.SortOrder;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des programmes.
 */
public class LieuxProgrammeTableUIHandler extends AbstractReefDbTableUIHandler<LieuxProgrammeTableRowModel, LieuxProgrammeTableUIModel, LieuxProgrammeTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(LieuxProgrammeTableUIHandler.class);

	/** {@inheritDoc} */
	@Override
	public AbstractReefDbTableModel<LieuxProgrammeTableRowModel> getTableModel() {
		return (LieuxProgrammeTableModel) getTable().getModel();
	}

	/** {@inheritDoc} */
	@Override
	public SwingTable getTable() {
		return getUI().getTableau();
	}

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final LieuxProgrammeTableUI ui) {
        super.beforeInit(ui);
        
        // create model and register to the JAXX context
        final LieuxProgrammeTableUIModel model = new LieuxProgrammeTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final LieuxProgrammeTableUI ui) {
    	
    	// Initialiser l UI
        initUI(ui);

        // Initialiser le tableau
        initialiserTableau();
        
        // Initialiser listeners
        initialiserListeners();
    }

    /**
     * Chargement des lieux.
     *
     * @param programme Le programme selectionne
     */
    public void load(final ProgramDTO programme) {
    	
    	// Sauvegarde du programme
    	getModel().setProgramme(programme);
    	
    	// Les lieux
    	final List<LocationDTO> lieux = getContext().getReferentialService().getLocations(null, programme.getCode(), false);
    	
    	// Chargement des lieux dans le model
    	getModel().setBeans(lieux);
        
        // Initialiser le titre du panel
        getUI().setBorder(BorderFactory.createTitledBorder(
        		t("reefdb.program.strategies.location.title", programme.getCode())));
        
        // Selectionner la premiere ligne du tableau
        selectCell(0, null);
        
        // Load les autres tableaux avec le premier lieu
        if (getContext().getSelectedLocationId() != null) {
            selectRowById(getContext().getSelectedLocationId());
            setFocusOnCell(getModel().getSingleSelectedRow());
        }
    }

    /**
     * Initialisation des listeners.
     */
    private void initialiserListeners() {

		// Listener sur le tableau
		getModel().addPropertyChangeListener(LocationsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> loadAutresTableaux(getModel().getSingleSelectedRow()));
    }
    
    /**
     * Load les autres tableaux.
     * @param lieu Le lieu selectionne
     */
    private void loadAutresTableaux(final LocationDTO lieu) {
		if (lieu != null) {
			
			// Chargement du tableau des strategies pour le lieu selectionne
			getUI().getParentContainer(StrategiesLieuxUI.class).getStrategiesLieuTableUI().getHandler().load(lieu);
			
			// Chargement du tableau des strategies pour un programme et un lieu
			getUI().getParentContainer(StrategiesLieuxUI.class).getStrategiesProgrammeTableUI().getHandler().load(
					getModel().getProgramme(), lieu);
		}
    }

    /**
     * Initialisation de le tableau.
     */
    private void initialiserTableau() {

        // Colonne label
        final TableColumnExt colonneLabel = addColumn(
                LieuxProgrammeTableModel.CODE);
        colonneLabel.setSortable(true);
        colonneLabel.setMinWidth(80);
        colonneLabel.setCellRenderer(newNumberCellRenderer(0));

        // Colonne name
        final TableColumnExt colonneName = addColumn(
                LieuxProgrammeTableModel.LABEL);
        colonneName.setSortable(true);
        colonneName.setMinWidth(80);

        // Colonne comment
        final TableColumnExt colonneComment = addColumn(
                LieuxProgrammeTableModel.NAME);
        colonneComment.setSortable(true);
        colonneComment.setMinWidth(100);
        
        // Modele de la table
        final LieuxProgrammeTableModel tableModel = new LieuxProgrammeTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable(), true);

        // Tri par defaut
        getTable().setSortOrder(LieuxProgrammeTableModel.CODE, SortOrder.ASCENDING);

		// Colonnes non editable
		tableModel.setNoneEditableCols();

		// Les colonnes obligatoire sont toujours presentes
		colonneLabel.setHideable(false);
		colonneLabel.setEditable(false);
		colonneName.setHideable(false);
		colonneName.setEditable(false);
		colonneComment.setHideable(false);
		colonneComment.setEditable(false);
    }
}
