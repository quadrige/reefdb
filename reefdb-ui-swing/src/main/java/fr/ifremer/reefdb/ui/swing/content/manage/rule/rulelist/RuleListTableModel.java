package fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class RuleListTableModel extends AbstractReefDbTableModel<RuleListRowModel> {

    /**
     * Constant <code>CODE</code>
     */
    static final ReefDbColumnIdentifier<RuleListRowModel> CODE = ReefDbColumnIdentifier.newId(
            RuleListRowModel.PROPERTY_CODE,
            n("reefdb.property.code"),
            n("reefdb.property.code"),
            String.class,
            true);

    /**
     * Constant <code>ACTIVE</code>
     */
    static final ReefDbColumnIdentifier<RuleListRowModel> ACTIVE = ReefDbColumnIdentifier.newId(
            RuleListRowModel.PROPERTY_ACTIVE,
            n("reefdb.rule.ruleList.active.short"),
            n("reefdb.rule.ruleList.active.tip"),
            Boolean.class,
            false);

    /**
     * Constant <code>START_MONTH</code>
     */
    static final ReefDbColumnIdentifier<RuleListRowModel> START_MONTH = ReefDbColumnIdentifier.newId(
            RuleListRowModel.PROPERTY_START_MONTH,
            n("reefdb.rule.ruleList.startMonth.short"),
            n("reefdb.rule.ruleList.startMonth.tip"),
        MonthDTO.class);

    /**
     * Constant <code>END_MONTH</code>
     */
    static final ReefDbColumnIdentifier<RuleListRowModel> END_MONTH = ReefDbColumnIdentifier.newId(
            RuleListRowModel.PROPERTY_END_MONTH,
            n("reefdb.rule.ruleList.endMonth.short"),
            n("reefdb.rule.ruleList.endMonth.tip"),
        MonthDTO.class);

    /**
     * Constant <code>DESCRIPTION</code>
     */
    public static final ReefDbColumnIdentifier<RuleListRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            RuleListRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.rule.ruleList.description.tip"),
            String.class,
            true);
    /**
     * Identifiant pour la colonne local.
     */
    public static final ReefDbColumnIdentifier<RuleListRowModel> LOCAL = ReefDbColumnIdentifier.newId(
            RuleListRowModel.PROPERTY_LOCAL,
            n("reefdb.property.referential.local"),
            n("reefdb.rule.ruleList.local.tip"),
            Boolean.class);


    public static final ReefDbColumnIdentifier<RuleListRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        RuleListRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<RuleListRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        RuleListRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);

    /**
     * <p>Constructor for RuleListTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     */
    RuleListTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, false, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListRowModel createNewRow() {
        return new RuleListRowModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReefDbColumnIdentifier<RuleListRowModel> getFirstColumnEditing() {
        return START_MONTH;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<RuleListRowModel> propertyName) {

        // national or local status is editable only on creation (not duplication)
        if (LOCAL == propertyName) {
            RuleListRowModel rowModel = getEntry(rowIndex);
            return rowModel.isLocalEditable();
        }

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }
}
