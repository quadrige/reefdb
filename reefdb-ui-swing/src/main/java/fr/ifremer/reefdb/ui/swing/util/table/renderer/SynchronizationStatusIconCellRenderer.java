package fr.ifremer.reefdb.ui.swing.util.table.renderer;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.ui.swing.table.renderer.IconCellRenderer;
import fr.ifremer.reefdb.dto.SynchronizationStatusDTO;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;

import javax.swing.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Renderer pour la gestion des icons pour la partage.
 */
public class SynchronizationStatusIconCellRenderer extends IconCellRenderer<SynchronizationStatusDTO> {
	
	/**
	 * Le contexte.
	 */
	private ReefDbUIContext contexte;
	
	/**
	 * Constructor.
	 *
	 * @param contexte a {@link fr.ifremer.reefdb.ui.swing.ReefDbUIContext} object.
	 */
	public SynchronizationStatusIconCellRenderer(final ReefDbUIContext contexte) {
		setContexte(contexte);
	}

    /** {@inheritDoc} */
    @Override
    protected Icon getIcon(SynchronizationStatusDTO object) {
        if (object == null) {
            return null;
        }
        return getContexte().getObjectStatusIcon(object.getIconName(), object.getIconName());
    }
    
    /** {@inheritDoc} */
    @Override
    protected String getToolTipText(SynchronizationStatusDTO object) {
        if (object == null) {
            return null;
        }
        return t("reefdb.property.status." + object.getIconName());
    }

	/** {@inheritDoc} */
	@Override
	protected String getText(SynchronizationStatusDTO object) {
      if (object == null) {
            return null;
        }
		return object.getName();
	}

	private ReefDbUIContext getContexte() {
		return contexte;
	}

	private void setContexte(ReefDbUIContext contexte) {
		this.contexte = contexte;
	}
}
