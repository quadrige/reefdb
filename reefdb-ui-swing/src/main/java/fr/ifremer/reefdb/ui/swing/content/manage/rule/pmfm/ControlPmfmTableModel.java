package fr.ifremer.reefdb.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.*;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class ControlPmfmTableModel extends AbstractReefDbTableModel<ControlPmfmRowModel> {

	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> PMFM_ID = ReefDbColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_ID,
			n("reefdb.property.pmfm.id"),
			n("reefdb.property.pmfm.id"),
			Integer.class);

	/** Constant <code>NAME</code> */
	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> NAME = ReefDbColumnIdentifier.newPmfmNameId(
			ControlPmfmRowModel.PROPERTY_PMFM,
			n("reefdb.property.name"),
			n("reefdb.property.name"));
	
	/** Constant <code>MATRIX</code> */
	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> MATRIX = ReefDbColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_MATRIX,
			n("reefdb.property.pmfm.matrix"),
			n("reefdb.property.pmfm.matrix"),
			MatrixDTO.class);
	
	/** Constant <code>FRACTION</code> */
	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> FRACTION = ReefDbColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_FRACTION,
			n("reefdb.property.pmfm.fraction"),
			n("reefdb.property.pmfm.fraction"),
			FractionDTO.class);
	
	/** Constant <code>METHOD</code> */
	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> METHOD = ReefDbColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_METHOD,
			n("reefdb.property.pmfm.method"),
			n("reefdb.property.pmfm.method"),
			MethodDTO.class);
	
	/** Constant <code>PARAMETER</code> */
	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> PARAMETER = ReefDbColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_PARAMETER,
			n("reefdb.property.pmfm.parameter"),
			n("reefdb.property.pmfm.parameter"),
			ParameterDTO.class);
    
	/** Constant <code>UNIT</code> */
	public static final ReefDbColumnIdentifier<ControlPmfmRowModel> UNIT = ReefDbColumnIdentifier.newReadOnlyId(
			ControlPmfmRowModel.PROPERTY_PMFM + "." + PmfmDTO.PROPERTY_UNIT,
			n("reefdb.property.pmfm.unit"),
			n("reefdb.property.pmfm.unit"),
			UnitDTO.class);

	/**
	 * <p>Constructor for ControlPmfmTableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	ControlPmfmTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, false, false);
	}

	/** {@inheritDoc} */
	@Override
	public ControlPmfmRowModel createNewRow() {
		return new ControlPmfmRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ControlPmfmRowModel> getFirstColumnEditing() {
		return null;
	}
}
