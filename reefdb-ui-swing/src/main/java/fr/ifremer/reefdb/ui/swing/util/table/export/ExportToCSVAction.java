package fr.ifremer.reefdb.ui.swing.util.table.export;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.escape.Escaper;
import com.google.common.escape.Escapers;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil;
import fr.ifremer.quadrige3.ui.swing.table.CheckTableColumn;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.service.extraction.ExtractionService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.nuiton.util.DateUtil;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.table.TableColumn;
import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * generic Action for CSV table extraction
 * <p/>
 * Created by Ludovic on 14/12/2015.
 */
public class ExportToCSVAction<M extends AbstractReefDbTableUIModel<?, ?, M>, UI extends ReefDbUI<M, ?>, H extends AbstractReefDbTableUIHandler<?, M, UI>>
        extends AbstractReefDbAction<M, UI, H> {

    public static final Log LOG = LogFactory.getLog(ExportToCSVAction.class);

    /**
     * the localized table name to display
     */
    private final String tableName;

    private Collection<ReefDbColumnIdentifier<? extends AbstractReefDbRowUIModel>> ignoredColumnIdentifiers;

    private File outputFile;

    private char separator = 0;

    private Escaper csvEscaper;

    /**
     * <p>Constructor for ExportToCSVAction.</p>
     *
     * @param handler a H object.
     */
    @SafeVarargs
    public ExportToCSVAction(H handler, String tableName, ReefDbColumnIdentifier<? extends AbstractReefDbRowUIModel>... identifiersToIgnore) {
        super(handler, false);
        setActionDescription(t("reefdb.action.extract.table.label"));
        Assert.notBlank(tableName);
        this.tableName = tableName;
        if (identifiersToIgnore != null) {
            getIgnoredColumnIdentifiers().addAll(Arrays.asList(identifiersToIgnore));
        }
    }

    private Collection<ReefDbColumnIdentifier<? extends AbstractReefDbRowUIModel>> getIgnoredColumnIdentifiers() {
        if (ignoredColumnIdentifiers == null) {
            ignoredColumnIdentifiers = new ArrayList<>();
        }
        return ignoredColumnIdentifiers;
    }

    /**
     * <p>Getter for the field <code>outputFile</code>.</p>
     *
     * @return a {@link java.io.File} object.
     */
    private File getOutputFile() {
        return outputFile;
    }

    /**
     * <p>getTable.</p>
     *
     * @return a {@link org.jdesktop.swingx.JXTable} object.
     */
    protected JXTable getTable() {
        return getHandler().getTable();
    }

    /**
     * Get the visible column list, except the CheckTableColumn if exists
     *
     * @return column list
     */
    protected List<TableColumn> getColumns() {
        return ReefDbBeans.filterCollection(getTable().getColumns(false),
                column -> column != null
                        && !(column instanceof CheckTableColumn)
                        && column.getIdentifier() instanceof ReefDbColumnIdentifier
                        && !getIgnoredColumnIdentifiers().contains(column.getIdentifier()));
    }

    /**
     * the CSV separator character
     *
     * @return CSV separator character
     */
    protected char getSeparator() {
        if (separator == 0) {
            separator = getConfig().getCsvSeparator().charAt(0);
        }
        return separator;
    }

    /**
     * Build and get an escaper that will remove all unwanted character like \r, \n or the CSV separator, from a string value
     *
     * @return a string cleaner
     */
    private Escaper getCsvEscaper() {
        if (csvEscaper == null) {
            csvEscaper = Escapers.builder().addEscape(CharUtils.CR, "").addEscape(CharUtils.LF, "").addEscape(getSeparator(), "").build();
        }
        return csvEscaper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        // prepare file name
        String date = DateUtil.formatDate(new Date(), "yyyy-MM-dd");
        String fileName = String.format("%s-%s-%s", getConfig().getExtractionFilePrefix(), tableName, date);
        String fileExtension = getConfig().getExtractionFileExtension();

        // ask user file where to export table
        outputFile = saveFile(
                fileName,
                fileExtension,
                t("reefdb.action.extract.table.choose.file.title", tableName),
                t("reefdb.action.extract.table.choose.file.buttonLabel"),
                "^.*\\." + fileExtension,
                t("reefdb.common.file." + fileExtension));

        return outputFile != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // create a writer
        try (BufferedWriter writer = java.nio.file.Files.newBufferedWriter(getOutputFile().toPath(), StandardCharsets.UTF_8)) {

            // first octet of file is the UTF8 BOM to allow encoding recognition (ex: Excel)
            writer.write(ExtractionService.UTF8_BOM);

            // write the header to file
            writeHeader(writer);

            // write lines to file
            writeLines(writer);

            // export done
            writer.flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        // generic message indicating where the output file is.
        String text = t("reefdb.action.extract.table.done", tableName, outputFile.getAbsoluteFile().toURI(), outputFile.getAbsolutePath());

        JEditorPane editorPane = new JEditorPane("text/html", text);
        editorPane.setEditable(false);
        editorPane.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                if (LOG.isInfoEnabled()) {
                    LOG.info("open url: " + url);
                }
                ApplicationUIUtil.openLink(url);
            }
        });

        getContext().getDialogHelper().showOptionDialog(
                null,
                editorPane,
                t("reefdb.action.extract.table.title", tableName),
                JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void releaseAction() {
        outputFile = null;
        super.releaseAction();
    }

    private void writeHeader(BufferedWriter writer) throws IOException {

        writer.write(getHeader());
        writer.newLine();

    }

    /**
     * Get the header string. Built from the identifiers of all columns.
     *
     * @return the header string
     */
    protected String getHeader() {

        return Joiner.on(getSeparator()).skipNulls().join(ReefDbBeans.transformCollection(getColumns(), column -> {
            ReefDbColumnIdentifier identifier = (ReefDbColumnIdentifier) column.getIdentifier();
            return t(identifier.getHeaderI18nKey());
        }));
    }

    /**
     * Write lines of the table into writer
     *
     * @param writer opened writer
     * @throws IOException if any
     */
    private void writeLines(BufferedWriter writer) throws IOException {

        int nbRows = getTable().getRowCount();
        // iterate over each line
        for (int rowIndex = 0; rowIndex < nbRows; rowIndex++) {

            // the row values to write
            List<String> rowValues = new ArrayList<>(getColumns().size());

            // iterate over columns
            for (TableColumn column : getColumns()) {

                // default value
                String stringValue = "";

                // get the object value from the table model
                Object value = getTable().getModel().getValueAt(rowIndex, column.getModelIndex());

                // try to stringify the object value
                if (value != null) {
                    stringValue = getStringValue(value, rowIndex, column);
                }

                // add escaped string value to list
                rowValues.add(getCsvEscaper().escape(stringValue));
            }

            // write line
            writer.write(Joiner.on(getSeparator()).join(rowValues));
            writer.newLine();
        }
    }

    /**
     * Get the String representation of the value
     *
     * @param value    the value to stringify
     * @param rowIndex the current row index
     * @param column   the current column
     * @return the string representing the value
     */
    private String getStringValue(Object value, int rowIndex, TableColumn column) {

        if (value instanceof QuadrigeBean && column.getCellRenderer() != null) {
            // use the specific renderer
            Component component = column.getCellRenderer().getTableCellRendererComponent(getTable(), value, false, false, rowIndex, column.getModelIndex());
            if (component instanceof JLabel) {
                return ((JLabel) component).getText();
            }
        }

        // default behavior
        return value.toString();
    }
}
