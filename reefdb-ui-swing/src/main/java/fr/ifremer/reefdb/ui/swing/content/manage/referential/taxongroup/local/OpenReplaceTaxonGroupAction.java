package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.ManageTaxonGroupUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.local.replace.ReplaceTaxonGroupUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.local.replace.ReplaceTaxonGroupUIModel;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 */
public class OpenReplaceTaxonGroupAction extends AbstractReefDbAction<TaxonGroupLocalUIModel, TaxonGroupLocalUI, TaxonGroupLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceTaxonGroupAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.taxongroup.local.TaxonGroupLocalUIHandler} object.
     */
    public OpenReplaceTaxonGroupAction(TaxonGroupLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<TaxonGroupDTO, ReplaceTaxonGroupUIModel, ReplaceTaxonGroupUI> openAction =
                new AbstractOpenReplaceUIAction<TaxonGroupDTO, ReplaceTaxonGroupUIModel, ReplaceTaxonGroupUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.taxonGroup");
                    }

                    @Override
                    protected ReplaceTaxonGroupUIModel createNewModel() {
                        return new ReplaceTaxonGroupUIModel();
                    }

                    @Override
                    protected ReplaceTaxonGroupUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceTaxonGroupUI(ctx);
                    }

                    @Override
                    protected List<TaxonGroupDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getTaxonGroups());
                    }

                    @Override
                    protected TaxonGroupDTO getSelectedSource() {
                        List<TaxonGroupDTO> selectedBeans = OpenReplaceTaxonGroupAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected TaxonGroupDTO getSelectedTarget() {
                        ManageTaxonGroupUI ui = OpenReplaceTaxonGroupAction.this.getUI().getParentContainer(ManageTaxonGroupUI.class);
                        List<TaxonGroupDTO> selectedBeans = ui.getTaxonGroupNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
