package fr.ifremer.reefdb.ui.swing.content.manage.program.strategiesByLocation.strategies;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

/**
 * Modele pour le tableau de programmes.
 */
public class StrategiesLieuTableRowModel extends AbstractReefDbRowUIModel<ProgStratDTO, StrategiesLieuTableRowModel> implements ProgStratDTO {

	/**
	 * Binder from.
	 */
    private static final Binder<ProgStratDTO, StrategiesLieuTableRowModel> FROM_BEAN_BINDER = 
    		BinderFactory.newBinder(ProgStratDTO.class, StrategiesLieuTableRowModel.class);
    
    /**
     * Binder to.
     */
    private static final Binder<StrategiesLieuTableRowModel, ProgStratDTO> TO_BEAN_BINDER = 
    		BinderFactory.newBinder(StrategiesLieuTableRowModel.class, ProgStratDTO.class);

	/**
	 * Constructor.
	 */
	public StrategiesLieuTableRowModel() {
		super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
	}

	/** {@inheritDoc} */
	@Override
	protected ProgStratDTO newBean() {
		return ReefDbBeanFactory.newProgStratDTO();
	}

	/** {@inheritDoc} */
	@Override
	public StatusDTO getStatus() {
		return delegateObject.getStatus();
	}

	/** {@inheritDoc} */
	@Override
	public void setStatus(StatusDTO status) {
		delegateObject.setStatus(status);
	}

	/** {@inheritDoc} */
	@Override
	public String getName() {
		return delegateObject.getName();
	}

	/** {@inheritDoc} */
	@Override
	public void setName(String name) {
		delegateObject.setName(name);
	}

	@Override
	public boolean isDirty() {
		return delegateObject.isDirty();
	}

	@Override
	public void setDirty(boolean dirty) {
		delegateObject.setDirty(dirty);
	}

	@Override
	public boolean isReadOnly() {
		return delegateObject.isReadOnly();
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		delegateObject.setReadOnly(readOnly);
	}

	/** {@inheritDoc} */
	@Override
	public LocalDate getStartDate() {
		return delegateObject.getStartDate();
	}

	/** {@inheritDoc} */
	@Override
	public void setStartDate(LocalDate startDate) {
		delegateObject.setStartDate(startDate);
	}

	/** {@inheritDoc} */
	@Override
	public LocalDate getEndDate() {
		return delegateObject.getEndDate();
	}

	/** {@inheritDoc} */
	@Override
	public void setEndDate(LocalDate endDate) {
		delegateObject.setEndDate(endDate);
	}

	/** {@inheritDoc} */
	@Override
	public Integer getAppliedStrategyId() {
		return delegateObject.getAppliedStrategyId();
	}

	/** {@inheritDoc} */
	@Override
	public void setAppliedStrategyId(Integer appliedStrategyId) {
		delegateObject.setAppliedStrategyId(appliedStrategyId);
	}

	/** {@inheritDoc} */
	@Override
	public ProgramDTO getProgram() {
		return delegateObject.getProgram();
	}

	/** {@inheritDoc} */
	@Override
	public void setProgram(ProgramDTO program) {
		delegateObject.setProgram(program);
	}

	/** {@inheritDoc} */
	@Override
	public DepartmentDTO getDepartment() {
		return delegateObject.getDepartment();
	}

	/** {@inheritDoc} */
	@Override
	public void setDepartment(DepartmentDTO department) {
		delegateObject.setDepartment(department);
	}

	@Override
	public ErrorDTO getErrors(int index) {
		return delegateObject.getErrors(index);
	}

	@Override
	public boolean isErrorsEmpty() {
		return delegateObject.isErrorsEmpty();
	}

	@Override
	public int sizeErrors() {
		return delegateObject.sizeErrors();
	}

	@Override
	public void addErrors(ErrorDTO errors) {
		delegateObject.addErrors(errors);
	}

	@Override
	public void addAllErrors(Collection<ErrorDTO> errors) {
		delegateObject.addAllErrors(errors);
	}

	@Override
	public boolean removeErrors(ErrorDTO errors) {
		return delegateObject.removeErrors(errors);
	}

	@Override
	public boolean removeAllErrors(Collection<ErrorDTO> errors) {
		return delegateObject.removeAllErrors(errors);
	}

	@Override
	public boolean containsErrors(ErrorDTO errors) {
		return delegateObject.containsErrors(errors);
	}

	@Override
	public boolean containsAllErrors(Collection<ErrorDTO> errors) {
		return delegateObject.containsAllErrors(errors);
	}

	@Override
	public Collection<ErrorDTO> getErrors() {
		return delegateObject.getErrors();
	}

	@Override
	public void setErrors(Collection<ErrorDTO> errors) {
		delegateObject.setErrors(errors);
	}

	@Override
	public Date getCreationDate() {
		return delegateObject.getCreationDate();
	}

	@Override
	public void setCreationDate(Date date) {
		delegateObject.setCreationDate(date);
	}

	@Override
	public Date getUpdateDate() {
		return delegateObject.getUpdateDate();
	}

	@Override
	public void setUpdateDate(Date date) {
		delegateObject.setUpdateDate(date);
	}


}
