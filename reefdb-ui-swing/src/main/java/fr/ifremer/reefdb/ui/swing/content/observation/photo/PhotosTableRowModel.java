package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.PhotoTypeDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Modele des lignes pour le tableau des photos.
 */
public class PhotosTableRowModel extends AbstractReefDbRowUIModel<PhotoDTO, PhotosTableRowModel> implements PhotoDTO, ErrorAware {

    private static final Binder<PhotoDTO, PhotosTableRowModel> FROM_BEAN_BINDER =
    		BinderFactory.newBinder(PhotoDTO.class, PhotosTableRowModel.class);
    
    private static final Binder<PhotosTableRowModel, PhotoDTO> TO_BEAN_BINDER =
    		BinderFactory.newBinder(PhotosTableRowModel.class, PhotoDTO.class);

	private final boolean readOnly;

	private final List<ErrorDTO> errors;

	/**
	 * Constructor.
	 *
	 * @param readOnly a boolean.
	 */
	public PhotosTableRowModel(boolean readOnly) {
		super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        this.readOnly = readOnly;
        errors = new ArrayList<>();
    }

    boolean isFileExists() {
		return getFullPath() != null && Files.isRegularFile(Paths.get(getFullPath()));
	}

	boolean isFileDownloadable() {
		return !isFileExists() && getRemoteId() != null;
	}

    /** {@inheritDoc} */
    @Override
    public boolean isEditable() {
        return !readOnly && super.isEditable();
    }

    /** {@inheritDoc} */
    @Override
	protected PhotoDTO newBean() {
		return ReefDbBeanFactory.newPhotoDTO();
	}

	/** {@inheritDoc} */
	@Override
	public String getName() {
		return delegateObject.getName();
	}

	/** {@inheritDoc} */
	@Override
	public void setName(String name) {
		delegateObject.setName(name);
	}

	/** {@inheritDoc} */
	@Override
	public String getCaption() {
		return delegateObject.getCaption();
	}

	/** {@inheritDoc} */
	@Override
	public void setCaption(String caption) {
		delegateObject.setCaption(caption);
	}

	/** {@inheritDoc} */
	@Override
	public Date getDate() {
		return delegateObject.getDate();
	}

	/** {@inheritDoc} */
	@Override
	public void setDate(Date date) {
		delegateObject.setDate(date);
	}

	/** {@inheritDoc} */
	@Override
	public String getDirection() {
		return delegateObject.getDirection();
	}

	/** {@inheritDoc} */
	@Override
	public void setDirection(String direction) {
		delegateObject.setDirection(direction);
	}

	/** {@inheritDoc} */
	@Override
	public String getPath() {
		return delegateObject.getPath();
	}

	/** {@inheritDoc} */
	@Override
	public void setPath(String path) {
		delegateObject.setPath(path);
	}

	/** {@inheritDoc} */
	@Override
	public String getFullPath() {
		return delegateObject.getFullPath();
	}

	/** {@inheritDoc} */
	@Override
	public void setFullPath(String tempPath) {
		delegateObject.setFullPath(tempPath);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isDirty() {
		return delegateObject.isDirty();
	}

	/** {@inheritDoc} */
	@Override
	public void setDirty(boolean dirty) {
		delegateObject.setDirty(dirty);
	}

	@Override
	public Integer getRemoteId() {
		return delegateObject.getRemoteId();
	}

	@Override
	public void setRemoteId(Integer remoteId) {
		delegateObject.setRemoteId(remoteId);
	}

	/** {@inheritDoc} */
	@Override
	public SamplingOperationDTO getSamplingOperation() {
		return delegateObject.getSamplingOperation();
	}

	/** {@inheritDoc} */
	@Override
	public void setSamplingOperation(SamplingOperationDTO samplingOperation) {
		delegateObject.setSamplingOperation(samplingOperation);
	}

	/** {@inheritDoc} */
	@Override
	public PhotoTypeDTO getPhotoType() {
		return delegateObject.getPhotoType();
	}

	/** {@inheritDoc} */
	@Override
	public void setPhotoType(PhotoTypeDTO photoType) {
		delegateObject.setPhotoType(photoType);
	}

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }
}
