package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.fraction;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;

import java.util.List;

/**
 * Action fermer une observation.
 */
public class SaveAction extends AbstractReefDbSaveAction<ManageFractionsUIModel, ManageFractionsUI, ManageFractionsUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public SaveAction(final ManageFractionsUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        // if the screen can be saved and the screen is valid
        return getModel().isModify() && getModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        List<? extends FractionDTO> toSave = getModel().getLocalUIModel().getRows();

        getContext().getReferentialService().saveFractions(toSave);
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setModify(false);
        getUI().getManageFractionsLocalUI().getMenuUI().getHandler().reloadComboBox();

        super.postSuccessAction();
    }

}
