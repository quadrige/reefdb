package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;

/**
 * <p>DuplicateContextAction class.</p>
 *
 */
public class DuplicateContextAction extends AbstractReefDbAction<ManageContextsListTableUIModel, ManageContextsListTableUI, ManageContextsListTableUIHandler> {
    
    private ContextDTO duplicatedContext;

	/**
	 * <p>Constructor for DuplicateContextAction.</p>
	 *
	 * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIHandler} object.
	 */
	public DuplicateContextAction(ManageContextsListTableUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() {

        ContextDTO contextToDuplicate = getModel().getSingleSelectedRow();
        duplicatedContext = getContext().getContextService().duplicateContext(contextToDuplicate);
	}

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        getModel().addBean(duplicatedContext);
        ManageContextsListTableUIRowModel lastRow = getModel().getRows().get(getModel().getRows().size()-1);
        getHandler().setFocusOnCell(lastRow);
    }
    
}
