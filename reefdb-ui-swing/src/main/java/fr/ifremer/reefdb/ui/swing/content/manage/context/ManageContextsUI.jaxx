<!--
  #%L
  Reef DB :: UI
  $Id:$
  $HeadURL:$
  %%
  Copyright (C) 2014 - 2015 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  #L%
  -->
<JPanel layout="{new BorderLayout()}"
        decorator='help' implements='fr.ifremer.reefdb.ui.swing.util.ReefDbUI&lt;ManageContextsUIModel, ManageContextsUIHandler&gt;'>
  <import>
    fr.ifremer.reefdb.ui.swing.ReefDbHelpBroker
    fr.ifremer.reefdb.ui.swing.ReefDbUIContext
    fr.ifremer.reefdb.ui.swing.util.ReefDbUI
    fr.ifremer.reefdb.ui.swing.content.ReefDbMainUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUI
    fr.ifremer.quadrige3.ui.swing.ApplicationUIUtil
    fr.ifremer.reefdb.ui.swing.content.manage.context.menu.ManageContextsListMenuUI
    fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUI
    fr.ifremer.reefdb.ui.swing.content.manage.context.filterslist.ManageFiltersListTableUI
    fr.ifremer.reefdb.ui.swing.content.manage.context.filtercontent.ManageFilterContentTableUI

    java.awt.FlowLayout
    javax.swing.Box
    javax.swing.BoxLayout
    java.awt.BorderLayout

    static org.nuiton.i18n.I18n.*
    static org.nuiton.i18n.I18n.t;
  </import>

  <!--getContextValue est une méthode interne JAXX-->
  <ManageContextsUIModel id='model' initializer='getContextValue(ManageContextsUIModel.class)'/>

  <ReefDbHelpBroker id='broker' constructorParams='"reefdb.home.help"'/>

  <BeanValidator id='validator' bean='model' uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='contextListModel' component='manageContextsListUI'/>
  </BeanValidator>

  <script><![CDATA[
		//Le parent est très utile pour intervenir sur les frères
		public ManageContextsUI(ReefDbMainUI parentUI) {
		    ApplicationUIUtil.setParentUI(this, parentUI);
		}
	]]></script>

  <ManageContextsListMenuUI id="manageContextsListMenuUI" constructorParams='this' constraints="BorderLayout.LINE_START"/>

  <JPanel layout="{new BorderLayout()}" constraints="BorderLayout.CENTER">

    <Table fill="both" constraints="BorderLayout.CENTER">
      <row>
        <cell weightx="1" weighty="0.2">
          <ManageContextsListTableUI id="manageContextsListUI" constructorParams='this'/>
        </cell>
      </row>
      <row>
        <cell weightx="1" weighty="0.8">
          <JPanel id="filters" layout="{new GridLayout(0,1)}">
            <ManageFiltersListTableUI id="manageFiltersListUI" constructorParams='this'/>
            <ManageFilterContentTableUI id="manageFilterContentUI" constructorParams='this'/>
          </JPanel>
        </cell>
      </row>
    </Table>

    <JPanel layout="{new GridLayout(1,0)}" constraints="BorderLayout.PAGE_END">
      <JButton id='closeButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
      <JButton id='saveButton' alignmentX='{Component.CENTER_ALIGNMENT}'/>
    </JPanel>

  </JPanel>

</JPanel>