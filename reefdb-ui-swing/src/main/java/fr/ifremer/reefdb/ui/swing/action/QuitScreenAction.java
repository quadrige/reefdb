package fr.ifremer.reefdb.ui.swing.action;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.AbstractUIHandler;
import fr.ifremer.quadrige3.ui.swing.action.AbstractChangeScreenAction;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * <p>QuitScreenAction class.</p>
 *
 */
public class QuitScreenAction<M extends AbstractBeanUIModel<?, M>, UI extends ReefDbUI<M, ?>, H extends AbstractUIHandler<M, UI>>
		extends AbstractCheckBeforeChangeScreenAction<M, UI, H> {

	private final Class<? extends AbstractReefDbSaveAction> saveActionClass;

	/**
	 * <p>Constructor for QuitScreenAction.</p>
	 *
	 * @param handler a H object.
	 * @param hideBody a boolean.
	 * @param saveActionClass a {@link java.lang.Class} object.
	 */
	public QuitScreenAction(H handler, boolean hideBody, Class<? extends AbstractReefDbSaveAction> saveActionClass) {
		super(handler, hideBody);
		this.saveActionClass= saveActionClass;
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
		return saveActionClass;
	}

	/** {@inheritDoc} */
	@Override
	protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
		return handler;
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractChangeScreenAction> getGotoActionClass() {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelModify() {
		return getModel().isModify();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelValid() {
		return getModel().isValid();
	}

	/** {@inheritDoc} */
	@Override
	protected void setModelModify(boolean modelModify) {
		getModel().setModify(modelModify);
	}

}
