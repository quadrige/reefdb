package fr.ifremer.reefdb.ui.swing.content.home.operation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.CoordinateAware;
import fr.ifremer.reefdb.dto.data.PositioningPrecisionAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Modele pour le tableau de prélevements pour la zone des prelevements de l'ecran d'accueil.
 */
public class OperationsTableRowModel extends AbstractReefDbRowUIModel<SamplingOperationDTO, OperationsTableRowModel> implements SamplingOperationDTO, PositioningPrecisionAware, CoordinateAware {

    /**
     * Binder from.
     */
    private static final Binder<SamplingOperationDTO, OperationsTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(SamplingOperationDTO.class, OperationsTableRowModel.class);

    /**
     * Binder to.
     */
    private static final Binder<OperationsTableRowModel, SamplingOperationDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(OperationsTableRowModel.class, SamplingOperationDTO.class);

    private final boolean readOnly;

    private DepartmentDTO analyst;
    public static final String PROPERTY_ANALYST = "analyst";

    /**
     * Constructor.
     *
     * @param readOnly a boolean.
     */
    public OperationsTableRowModel(boolean readOnly) {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        this.readOnly = readOnly;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEditable() {
        return !readOnly && super.isEditable();
    }

    /** {@inheritDoc} */
    @Override
    protected SamplingOperationDTO newBean() {
        return ReefDbBeanFactory.newSamplingOperationDTO();
    }

    public DepartmentDTO getAnalyst() {
        return analyst;
    }

    public void setAnalyst(DepartmentDTO analyst) {
        DepartmentDTO oldValue = getAnalyst();
        this.analyst = analyst;
        firePropertyChange(PROPERTY_ANALYST, oldValue, analyst);
    }

    /** {@inheritDoc} */
    @Override
    public void addMeasurements(MeasurementDTO measurements) {
        delegateObject.addMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public Double getSize() {
        return delegateObject.getSize();
    }

    /** {@inheritDoc} */
    @Override
    public void setSize(Double size) {
        delegateObject.setSize(size);
    }

    /** {@inheritDoc} */
    @Override
    public UnitDTO getSizeUnit() {
        return delegateObject.getSizeUnit();
    }

    /** {@inheritDoc} */
    @Override
    public void setSizeUnit(UnitDTO sizeUnit) {
        delegateObject.setSizeUnit(sizeUnit);
    }

    /** {@inheritDoc} */
    @Override
    public Double getDepth() {
        return delegateObject.getDepth();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepth(Double depth) {
        delegateObject.setDepth(depth);
    }

    /** {@inheritDoc} */
    @Override
    public Double getMinDepth() {
        return delegateObject.getMinDepth();
    }

    /** {@inheritDoc} */
    @Override
    public void setMinDepth(Double minDepth) {
        delegateObject.setMinDepth(minDepth);
    }

    /** {@inheritDoc} */
    @Override
    public Double getMaxDepth() {
        return delegateObject.getMaxDepth();
    }

    /** {@inheritDoc} */
    @Override
    public void setMaxDepth(Double maxDepth) {
        delegateObject.setMaxDepth(maxDepth);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getTime() {
        return delegateObject.getTime();
    }

    /** {@inheritDoc} */
    @Override
    public void setTime(Integer time) {
        delegateObject.setTime(time);
    }

    /** {@inheritDoc} */
    @Override
    public Integer getIndividualCount() {
        return delegateObject.getIndividualCount();
    }

    /** {@inheritDoc} */
    @Override
    public void setIndividualCount(Integer individualCount) {
        delegateObject.setIndividualCount(individualCount);
    }

    /** {@inheritDoc} */
    @Override
    public SamplingEquipmentDTO getSamplingEquipment() {
        return delegateObject.getSamplingEquipment();
    }

    /** {@inheritDoc} */
    @Override
    public void setSamplingEquipment(SamplingEquipmentDTO samplingEquipment) {
        delegateObject.setSamplingEquipment(samplingEquipment);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioning() {
        return delegateObject.getPositioning();
    }

    /** {@inheritDoc} */
    @Override
    public void setPositioning(PositioningSystemDTO positioning) {
        delegateObject.setPositioning(positioning);
    }

    /** {@inheritDoc} */
    @Override
    public CoordinateDTO getCoordinate() {
        return delegateObject.getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public void setCoordinate(CoordinateDTO coordinate) {
        delegateObject.setCoordinate(coordinate);
    }

    /** {@inheritDoc} */
    @Override
    public LevelDTO getDepthLevel() {
        return delegateObject.getDepthLevel();
    }

    /** {@inheritDoc} */
    @Override
    public void setDepthLevel(LevelDTO depthLevel) {
        delegateObject.setDepthLevel(depthLevel);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getSamplingDepartment() {
        return delegateObject.getSamplingDepartment();
    }

    /** {@inheritDoc} */
    @Override
    public void setSamplingDepartment(DepartmentDTO samplingDepartment) {
        delegateObject.setSamplingDepartment(samplingDepartment);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmDTO getPmfms(int index) {
        return delegateObject.getPmfms(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmsEmpty() {
        return delegateObject.isPmfmsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizePmfms() {
        return delegateObject.sizePmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void addPmfms(PmfmDTO pmfms) {
        delegateObject.addPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllPmfms(Collection<PmfmDTO> pmfms) {
        delegateObject.addAllPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removePmfms(PmfmDTO pmfms) {
        return delegateObject.removePmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllPmfms(Collection<PmfmDTO> pmfms) {
        return delegateObject.removeAllPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsPmfms(PmfmDTO pmfms) {
        return delegateObject.containsPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllPmfms(Collection<PmfmDTO> pmfms) {
        return delegateObject.containsAllPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmDTO> getPmfms() {
        return delegateObject.getPmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void setPmfms(List<PmfmDTO> pmfms) {
        delegateObject.setPmfms(pmfms);
    }

    /** {@inheritDoc} */
    @Override
    public PmfmDTO getIndividualPmfms(int index) {
        return delegateObject.getIndividualPmfms(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isIndividualPmfmsEmpty() {
        return delegateObject.isIndividualPmfmsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeIndividualPmfms() {
        return delegateObject.sizeIndividualPmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void addIndividualPmfms(PmfmDTO individualPmfms) {
        delegateObject.addIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllIndividualPmfms(Collection<PmfmDTO> individualPmfms) {
        delegateObject.addAllIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeIndividualPmfms(PmfmDTO individualPmfms) {
        return delegateObject.removeIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllIndividualPmfms(Collection<PmfmDTO> individualPmfms) {
        return delegateObject.removeAllIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsIndividualPmfms(PmfmDTO individualPmfms) {
        return delegateObject.containsIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllIndividualPmfms(Collection<PmfmDTO> individualPmfms) {
        return delegateObject.containsAllIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmDTO> getIndividualPmfms() {
        return delegateObject.getIndividualPmfms();
    }

    /** {@inheritDoc} */
    @Override
    public void setIndividualPmfms(List<PmfmDTO> individualPmfms) {
        delegateObject.setIndividualPmfms(individualPmfms);
    }

    /** {@inheritDoc} */
    @Override
    public MeasurementDTO getMeasurements(int index) {
        return delegateObject.getMeasurements(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMeasurementsEmpty() {
        return delegateObject.isMeasurementsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeMeasurements() {
        return delegateObject.sizeMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void addAllMeasurements(Collection<MeasurementDTO> measurements) {
        delegateObject.addAllMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeMeasurements(MeasurementDTO measurements) {
        return delegateObject.removeMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllMeasurements(Collection<MeasurementDTO> measurements) {
        return delegateObject.removeAllMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsMeasurements(MeasurementDTO measurements) {
        return delegateObject.containsMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllMeasurements(Collection<MeasurementDTO> measurements) {
        return delegateObject.containsAllMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public List<MeasurementDTO> getMeasurements() {
        return delegateObject.getMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void setMeasurements(List<MeasurementDTO> measurements) {
        delegateObject.setMeasurements(measurements);
    }

    /** {@inheritDoc} */
    @Override
    public MeasurementDTO getIndividualMeasurements(int index) {
        return delegateObject.getIndividualMeasurements(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isIndividualMeasurementsEmpty() {
        return delegateObject.isIndividualMeasurementsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeIndividualMeasurements() {
        return delegateObject.sizeIndividualMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void addIndividualMeasurements(MeasurementDTO individualMeasurements) {
        delegateObject.addIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllIndividualMeasurements(Collection<MeasurementDTO> individualMeasurements) {
        delegateObject.addAllIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeIndividualMeasurements(MeasurementDTO individualMeasurements) {
        return delegateObject.removeIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllIndividualMeasurements(Collection<MeasurementDTO> individualMeasurements) {
        return delegateObject.removeAllIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsIndividualMeasurements(MeasurementDTO individualMeasurements) {
        return delegateObject.containsIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllIndividualMeasurements(Collection<MeasurementDTO> individualMeasurements) {
        return delegateObject.containsAllIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public List<MeasurementDTO> getIndividualMeasurements() {
        return delegateObject.getIndividualMeasurements();
    }

    /** {@inheritDoc} */
    @Override
    public void setIndividualMeasurements(List<MeasurementDTO> individualMeasurements) {
        delegateObject.setIndividualMeasurements(individualMeasurements);
    }

    /** {@inheritDoc} */
    @Override
    public ErrorDTO getErrors(int index) {
        return delegateObject.getErrors(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isErrorsEmpty() {
        return delegateObject.isErrorsEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeErrors() {
        return delegateObject.sizeErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void addErrors(ErrorDTO errors) {
        delegateObject.addErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllErrors(Collection<ErrorDTO> errors) {
        delegateObject.addAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeErrors(ErrorDTO errors) {
        return delegateObject.removeErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.removeAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsErrors(ErrorDTO errors) {
        return delegateObject.containsErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllErrors(Collection<ErrorDTO> errors) {
        return delegateObject.containsAllErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return delegateObject.getErrors();
    }

    /** {@inheritDoc} */
    @Override
    public void setErrors(Collection<ErrorDTO> errors) {
        delegateObject.setErrors(errors);
    }

    /** {@inheritDoc} */
    @Override
    public Date getControlDate() {
        return delegateObject.getControlDate();
    }

    /** {@inheritDoc} */
    @Override
    public void setControlDate(Date controlDate) {
        delegateObject.setControlDate(controlDate);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMeasurementsLoaded() {
        return delegateObject.isMeasurementsLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setMeasurementsLoaded(boolean measurementsLoaded) {
        delegateObject.setMeasurementsLoaded(measurementsLoaded);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isIndividualMeasurementsLoaded() {
        return delegateObject.isIndividualMeasurementsLoaded();
    }

    /** {@inheritDoc} */
    @Override
    public void setIndividualMeasurementsLoaded(boolean individualMeasurementsLoaded) {
        delegateObject.setIndividualMeasurementsLoaded(individualMeasurementsLoaded);
    }

    @Override
    public boolean isActualPosition() {
        return delegateObject.isActualPosition();
    }

    @Override
    public void setActualPosition(boolean actualPosition) {
        delegateObject.setActualPosition(actualPosition);
    }

    /** {@inheritDoc} */
    @Override
    public String getPositioningPrecision() {
        return getPositioning() == null ? null : getPositioning().getPrecision();
    }

    /** {@inheritDoc} */
    @Override
    public Double getLatitude() {
        return getCoordinateToModify().getMinLatitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setLatitude(Double latitude) {
        Double oldValue = getLatitude();
        getCoordinateToModify().setMinLatitude(latitude);
        firePropertyChange(PROPERTY_LATITUDE, oldValue, latitude);
    }

    /** {@inheritDoc} */
    @Override
    public Double getLongitude() {
        return getCoordinateToModify().getMinLongitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setLongitude(Double longitude) {
        Double oldValue = getLongitude();
        getCoordinateToModify().setMinLongitude(longitude);
        firePropertyChange(PROPERTY_LONGITUDE, oldValue, longitude);
    }

    private CoordinateDTO getCoordinateToModify() {
        if (getCoordinate() == null) {
            setCoordinate(ReefDbBeanFactory.newCoordinateDTO());
        }
        return getCoordinate();
    }
}
