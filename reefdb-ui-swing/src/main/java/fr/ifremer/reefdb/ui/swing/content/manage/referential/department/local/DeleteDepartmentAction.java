package fr.ifremer.reefdb.ui.swing.content.manage.referential.department.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.ManageDepartmentsUI;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de delete un department local
 */
public class DeleteDepartmentAction extends AbstractReefDbAction<ManageDepartmentsLocalUIModel, ManageDepartmentsLocalUI, ManageDepartmentsLocalUIHandler> {

    private Set<? extends DepartmentDTO> toDelete;
    boolean deleteOk = false;

    /**
     * Constructor.
     *
     * @param handler Le controleur
     */
    public DeleteDepartmentAction(final ManageDepartmentsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {

        toDelete = getModel().getSelectedRows();

        return super.prepareAction()
                && CollectionUtils.isNotEmpty(toDelete)
                && askBeforeDelete(t("reefdb.action.delete.confirm.title"), t("reefdb.action.delete.department.message"));
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        List<String> used = Lists.newArrayList();

        // check usage in data
        for (DepartmentDTO department: toDelete) {
            if (department.getId() != null) {
                if (getContext().getReferentialService().isDepartmentUsedInData(department.getId())) {
                    used.add(decorate(department));
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.department.used.data.message"));
            return;
        }

        // check usage in rules
        for (DepartmentDTO department: toDelete) {
            if (department.getId() != null) {
                if (getContext().getReferentialService().isDepartmentUsedInRules(department.getId())) {
                    used.add(decorate(department));
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.department.used.rule.message"));
            return;
        }

        // check usage in program/strategy
        for (DepartmentDTO department: toDelete) {
            if (department.getId() != null) {
                if (getContext().getReferentialService().isDepartmentUsedInProgram(department.getId())) {
                    used.add(decorate(department));
                }
            }
        }
        if (!used.isEmpty()) {
            getContext().getDialogHelper().showErrorDialog(
                    t("reefdb.action.delete.referential.used.title"),
                    ReefDbUIs.getHtmlString(used),
                    t("reefdb.action.delete.department.used.strategy.message"));
            return;
        }

        // delete
        getContext().getReferentialService().deleteDepartments(ReefDbBeans.collectIds(toDelete));
        deleteOk = true;
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        if (deleteOk) {
            getModel().deleteSelectedRows();

            // reload
            ManageDepartmentsUI parentUI = getUI().getParentContainer(ManageDepartmentsUI.class);
            parentUI.getHandler().reloadComboBoxes();

        }

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        deleteOk = false;
        super.releaseAction();
    }
}
