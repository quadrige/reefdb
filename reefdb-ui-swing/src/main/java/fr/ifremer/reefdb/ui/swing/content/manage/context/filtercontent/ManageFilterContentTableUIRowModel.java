package fr.ifremer.reefdb.ui.swing.content.manage.context.filtercontent;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;

/**
 * <p>ManageFilterContentTableUIRowModel class.</p>
 *
 * @author Antoine
 */
public class ManageFilterContentTableUIRowModel extends AbstractReefDbRowUIModel<QuadrigeBean, ManageFilterContentTableUIRowModel> {

    private String label;
    /** Constant <code>PROPERTY_LABEL="label"</code> */
    public static final String PROPERTY_LABEL = "label";

    /**
     * <p>Constructor for ManageFilterContentTableUIRowModel.</p>
     */
    public ManageFilterContentTableUIRowModel() {
        super(null, null);
    }

    /** {@inheritDoc} */
    @Override
    protected QuadrigeBean newBean() {
        return null;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return label;
    }

    /**
     * <p>Setter for the field <code>label</code>.</p>
     *
     * @param label a {@link java.lang.String} object.
     */
    public void setLabel(String label) {
        this.label = label;
    }
}
