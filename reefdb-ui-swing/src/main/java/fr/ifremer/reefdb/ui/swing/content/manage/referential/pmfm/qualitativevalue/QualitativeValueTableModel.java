package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Model for qualitative value dialog table
 */
public class QualitativeValueTableModel extends AbstractReefDbTableModel<QualitativeValueTableRowModel> {

    /**
     * Column Mnemonic
     */
    public static final ReefDbColumnIdentifier<QualitativeValueTableRowModel> MNEMONIC = ReefDbColumnIdentifier.newId(
            QualitativeValueTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class, true);
    /**
     * Column Description
     */
    public static final ReefDbColumnIdentifier<QualitativeValueTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
            QualitativeValueTableRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.property.description"),
            String.class);
    /**
     * Column Status
     */
    public static final ReefDbColumnIdentifier<QualitativeValueTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
            QualitativeValueTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.status"),
            n("reefdb.property.status"),
            StatusDTO.class, true);

    /**
     * Constructor.
     *
     * @param columnModel model for columns
     */
    public QualitativeValueTableModel(final TableColumnModelExt columnModel) {
        super(columnModel, true, false);
    }

    /** {@inheritDoc} */
    @Override
    public QualitativeValueTableRowModel createNewRow() {
        return new QualitativeValueTableRowModel();
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<QualitativeValueTableRowModel> getFirstColumnEditing() {
        return MNEMONIC;
    }
}
