package fr.ifremer.reefdb.ui.swing.content.manage.program;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.PmfmsTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableUIModel;
import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * Modele pour l administration de contextes.
 */
public class ProgramsUIModel extends AbstractEmptyUIModel<ProgramsUIModel> {
    
    /**
     * Programmes UI model.
     */
    private ProgramsTableUIModel programsUIModel;
    
    /**
     * Strategies UI model.
     */
    private StrategiesTableUIModel strategiesUIModel;
    
    /**
     * Lieux UI model.
     */
    private LocationsTableUIModel locationsUIModel;
    
    /**
     * PSFMs UI model.
     */
    private PmfmsTableUIModel pmfmsUIModel;

	private boolean saveEnabled;
	public static final String PROPERTY_SAVE_ENABLED = "saveEnabled";

	/** {@inheritDoc} */
	@Override
	public void setModify(boolean modify) {

		if (!modify) {
			programsUIModel.setModify(false);
			strategiesUIModel.setModify(false);
			locationsUIModel.setModify(false);
			pmfmsUIModel.setModify(false);
		}

		super.setModify(modify);
	}

	/**
	 * <p>Getter for the field <code>programsUIModel</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableUIModel} object.
	 */
	public ProgramsTableUIModel getProgramsUIModel() {
		return programsUIModel;
	}

	/**
	 * <p>Setter for the field <code>programsUIModel</code>.</p>
	 *
	 * @param programsUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableUIModel} object.
	 */
	public void setProgramsUIModel(ProgramsTableUIModel programsUIModel) {
		this.programsUIModel = programsUIModel;
	}

	/**
	 * <p>Getter for the field <code>strategiesUIModel</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableUIModel} object.
	 */
	public StrategiesTableUIModel getStrategiesUIModel() {
		return strategiesUIModel;
	}

	/**
	 * <p>Setter for the field <code>strategiesUIModel</code>.</p>
	 *
	 * @param strategiesUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.strategies.StrategiesTableUIModel} object.
	 */
	public void setStrategiesUIModel(StrategiesTableUIModel strategiesUIModel) {
		this.strategiesUIModel = strategiesUIModel;
	}

	/**
	 * <p>Getter for the field <code>locationsUIModel</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel} object.
	 */
	public LocationsTableUIModel getLocationsUIModel() {
		return locationsUIModel;
	}

	/**
	 * <p>Setter for the field <code>locationsUIModel</code>.</p>
	 *
	 * @param locationsUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.locations.LocationsTableUIModel} object.
	 */
	public void setLocationsUIModel(LocationsTableUIModel locationsUIModel) {
		this.locationsUIModel = locationsUIModel;
	}

	/**
	 * <p>Getter for the field <code>pmfmsUIModel</code>.</p>
	 *
	 * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.PmfmsTableUIModel} object.
	 */
	public PmfmsTableUIModel getPmfmsUIModel() {
		return pmfmsUIModel;
	}

	/**
	 * <p>Setter for the field <code>pmfmsUIModel</code>.</p>
	 *
	 * @param pmfmsUIModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.PmfmsTableUIModel} object.
	 */
	public void setPmfmsUIModel(PmfmsTableUIModel pmfmsUIModel) {
		this.pmfmsUIModel = pmfmsUIModel;
	}

	public boolean isSaveEnabled() {
		return saveEnabled;
	}

	public void setSaveEnabled(boolean saveEnabled) {
		this.saveEnabled = saveEnabled;
		firePropertyChange(PROPERTY_SAVE_ENABLED, null, saveEnabled);
	}
}
