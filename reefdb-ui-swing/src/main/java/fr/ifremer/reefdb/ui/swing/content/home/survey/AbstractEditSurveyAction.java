package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractCheckBeforeChangeScreenAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUI;
import fr.ifremer.reefdb.ui.swing.content.home.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by Ludovic on 01/07/2015.
 */
public abstract class AbstractEditSurveyAction extends
        AbstractCheckBeforeChangeScreenAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

    /**
     * Constructor.
     *
     * @param handler  Handler
     * @param hideBody HideBody
     */
    protected AbstractEditSurveyAction(SurveysTableUIHandler handler, boolean hideBody) {
        super(handler, hideBody);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        boolean canContinue = super.prepareAction();
        getContext().setSelectedSamplingOperationId(null);
        if (canContinue && (getModel().isModify() || getModel().getMainUIModel().isModify())) {
            getContext().getDialogHelper().showWarningDialog(
                    t("reefdb.home.survey.error.message"),
                    t("reefdb.home.survey.error.title"));
            canContinue = false;
        }
        return canContinue;

    }

    /** {@inheritDoc} */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getModel().getMainUIModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().getMainUIModel().setModify(modelModify);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getModel().getMainUIModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        final HomeUI HomeUI = getUI().getParentContainer(HomeUI.class);
        return HomeUI.getHandler();
    }
}
