package fr.ifremer.reefdb.ui.swing.content.observation.shared;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.ui.core.dto.DirtyAware;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleGroupDTO;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementAware;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.enums.ControlElementValues;
import fr.ifremer.reefdb.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.grouped.OperationMeasurementsGroupedRowModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.PmfmTableColumn;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbPmfmColumnIdentifier;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author peck7 on 08/01/2019.
 */
public abstract class AbstractMeasurementsGroupedTableUIHandler<
    R extends AbstractMeasurementsGroupedRowModel<MeasurementDTO, R>,
    M extends AbstractMeasurementsGroupedTableUIModel<MeasurementDTO, R, M>,
    UI extends ReefDbUI<M, ?>>
    extends AbstractReefDbTableUIHandler<R, M, UI> {

    private static final Log LOG = LogFactory.getLog(AbstractMeasurementsGroupedTableUIHandler.class);

    // editor for taxon group column
    protected ExtendedComboBoxCellEditor<TaxonGroupDTO> taxonGroupCellEditor;
    // editor for taxon column
    protected ExtendedComboBoxCellEditor<TaxonDTO> taxonCellEditor;
    // editor for analyst column
    protected ExtendedComboBoxCellEditor<DepartmentDTO> departmentCellEditor;

    public AbstractMeasurementsGroupedTableUIHandler(String... properties) {
        super(properties);
    }

    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{
            AbstractMeasurementsGroupedRowModel.PROPERTY_INPUT_TAXON_ID,
            AbstractMeasurementsGroupedRowModel.PROPERTY_INPUT_TAXON_NAME
        };
    }

    @Override
    public void beforeInit(UI ui) {
        super.beforeInit(ui);
        ui.setContextValue(createNewModel());
    }

    protected abstract M createNewModel();

    protected abstract void initTable();

    @Override
    public void afterInit(UI ui) {

        initUI(ui);

        createTaxonGroupCellEditor();
        createTaxonCellEditor();
        createDepartmentCellEditor();

        initTable();

        initListeners();
    }

    @Override
    public abstract AbstractMeasurementsGroupedTableModel<R> getTableModel();

    private void createTaxonCellEditor() {

//        taxonCellEditor = newExtendedComboBoxCellEditor(null, SurveyMeasurementsGroupedTableModel.TAXON, false);
        taxonCellEditor = newExtendedComboBoxCellEditor(null, TaxonDTO.class, DecoratorService.WITH_CITATION_AND_REFERENT, false);

        taxonCellEditor.setAction("unfilter-taxon", "reefdb.common.unfilter.taxon", e -> {
            // unfilter taxons
            updateTaxonCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateTaxonCellEditor(R row, boolean forceNoFilter) {

        // Mantis #0027041 forceNoFilter==true remove link between TaxonGroup and Taxon, but keep context filter
        taxonCellEditor.getCombo().setActionEnabled(!forceNoFilter /*&& getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON)*/);

//        List<TaxonDTO> taxons = getContext().getObservationService().getAvailableTaxons(row == null ? null : row.getTaxonGroup(), forceNoFilter);
        List<TaxonDTO> taxons = getModel().getObservationUIHandler().getAvailableTaxons(row == null || forceNoFilter ? null : row.getTaxonGroup(), false);
        taxonCellEditor.getCombo().setData(taxons);

        // Mantis #0028101 : don't affect taxon in row, even there is no corresponding
//        if (row != null) {
//            if (taxons.isEmpty() && row.getTaxon() != null) {
//                row.setTaxon(null);
//            } else if (taxons.size() == 1) {
//                row.setTaxon(taxons.get(0));
//            }
//        }
    }

    private void createTaxonGroupCellEditor() {

        taxonGroupCellEditor = newExtendedComboBoxCellEditor(null, TaxonGroupDTO.class, false);

        taxonGroupCellEditor.setAction("unfilter-taxon", "reefdb.common.unfilter.taxon", e -> {
            // unfilter taxon groups
            updateTaxonGroupCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateTaxonGroupCellEditor(R row, boolean forceNoFilter) {

        // Mantis #0027041 forceNoFilter==true remove link between TaxonGroup and Taxon, but keep context filter
        taxonGroupCellEditor.getCombo().setActionEnabled(!forceNoFilter /*&& getContext().getDataContext().isContextFiltered(FilterTypeValues.TAXON_GROUP)*/);

//        List<TaxonGroupDTO> taxonGroups = getContext().getObservationService().getAvailableTaxonGroups(row == null ? null : row.getTaxon(), forceNoFilter);
        List<TaxonGroupDTO> taxonGroups = getModel().getObservationUIHandler().getAvailableTaxonGroups(row == null || forceNoFilter ? null : row.getTaxon(), false);

        taxonGroupCellEditor.getCombo().setData(taxonGroups);

        // Mantis #0028101 : don't affect taxon group in row, even there is no corresponding
//        if (row != null) {
//            if (taxonGroups.isEmpty() && row.getTaxonGroup() != null) {
//                row.setTaxonGroup(null);
//            } else if (taxonGroups.size() == 1) {
//                row.setTaxonGroup(taxonGroups.get(0));
//            }
//        }
    }

    private void createDepartmentCellEditor() {

        departmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        departmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateDepartmentCellEditor(true);
        });

    }

    private void updateDepartmentCellEditor(boolean forceNoFilter) {

        departmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
            && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        departmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    protected void resetCellEditors() {

        updateTaxonGroupCellEditor(null, false);
        updateTaxonCellEditor(null, false);
        updateDepartmentCellEditor(false);
    }

    protected void initListeners() {

        getModel().addPropertyChangeListener(evt -> {

            switch (evt.getPropertyName()) {

                case AbstractMeasurementsGroupedTableUIModel.PROPERTY_SURVEY:

                    loadMeasurements();
                    break;

                case AbstractMeasurementsGroupedTableUIModel.EVENT_INDIVIDUAL_MEASUREMENTS_LOADED:

                    detectPreconditionedPmfms();
                    detectGroupedIdentifiers();
                    break;

                case AbstractMeasurementsGroupedTableUIModel.PROPERTY_MEASUREMENT_FILTER:

                    filterMeasurements();
                    break;

                case AbstractMeasurementsGroupedTableUIModel.PROPERTY_SINGLE_ROW_SELECTED:

                    if (getModel().getSingleSelectedRow() != null && !getModel().getSingleSelectedRow().isEditable()) {
                        return;
                    }

                    // update taxonCellEditor
                    updateTaxonCellEditor(getModel().getSingleSelectedRow(), false);
                    updateTaxonGroupCellEditor(getModel().getSingleSelectedRow(), false);
                    updateDepartmentCellEditor(false);
                    updatePmfmCellEditors(getModel().getSingleSelectedRow(), null, false);
                    break;

            }
        });
    }

    protected abstract void filterMeasurements();

    /**
     * Load measurements
     */
    private void loadMeasurements() {

        SwingUtilities.invokeLater(() -> {

            // Uninstall save state listener
            uninstallSaveTableStateListener();

            // reset (and prepare combo boxes)
            resetCellEditors();

            // Build dynamic columns
            // TODO manage column position depending on criteria (parametrized list or specific attribute)
            // maybe split pmfms list in 2 separated list or move afterward
            ReefDbColumnIdentifier<R> insertPosition = getTableModel().getPmfmInsertPosition();
            addPmfmColumns(
                getModel().getPmfms(),
                AbstractMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS,
                DecoratorService.NAME_WITH_UNIT,
                insertPosition);

            boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfms());

            // Build rows
            getModel().setRows(buildRows(!getModel().getSurvey().isEditable()));

            recomputeRowsValidState();

            // Apply measurement filter
            filterMeasurements();

            // restore table from swing session
            restoreTableState();

            // hide analyst if no pmfm
//            forceColumnVisibleAtLastPosition(AbstractMeasurementsGroupedTableModel.ANALYST, notEmpty);
            // Don't force position (Mantis #49537)
            forceColumnVisible(AbstractMeasurementsGroupedTableModel.ANALYST, notEmpty);

            // set columns with errors visible (Mantis #40752)
            ensureColumnsWithErrorAreVisible(getModel().getRows());

            // Install save state listener
            installSaveTableStateListener();

            getModel().fireMeasurementsLoaded();
        });

    }

    protected List<R> buildRows(boolean readOnly) {

        List<R> rows = new ArrayList<>();

        List<? extends MeasurementAware> measurementAwareBeans = getMeasurementAwareModels();

        for (MeasurementAware bean : measurementAwareBeans) {

            List<MeasurementDTO> measurements = bean.getIndividualMeasurements().stream()
                // sort by individual id
                .sorted(Comparator.comparingInt(MeasurementDTO::getIndividualId))
                .collect(Collectors.toList());

            R row = null;
            for (final MeasurementDTO measurement : measurements) {
                // check previous row
                if (row != null) {

                    // if individual id differs = new row
                    if (!row.getIndividualId().equals(measurement.getIndividualId())) {
                        row = null;
                    }

                    // if taxon group or taxon differs, should update current row (see Mantis #0026647)
                    else if (!Objects.equals(row.getTaxonGroup(), measurement.getTaxonGroup())
                        || !Objects.equals(row.getTaxon(), measurement.getTaxon())
                        || !Objects.equals(row.getInputTaxonId(), measurement.getInputTaxonId())
                        || !Objects.equals(row.getInputTaxonName(), measurement.getInputTaxonName())) {

                        // update taxon group and taxon if previously empty
                        if (row.getTaxonGroup() == null) {
                            row.setTaxonGroup(measurement.getTaxonGroup());
                        } else if (measurement.getTaxonGroup() != null && !row.getTaxonGroup().equals(measurement.getTaxonGroup())) {
                            // the taxon group in measurement differs
                            LOG.error(String.format("taxon group in measurement (id=%s) differs with taxon group in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                        if (row.getTaxon() == null) {
                            row.setTaxon(measurement.getTaxon());
                        } else if (measurement.getTaxon() != null && !row.getTaxon().equals(measurement.getTaxon())) {
                            // the taxon in measurement differs
                            LOG.error(String.format("taxon in measurement (id=%s) differs with taxon in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                        if (row.getInputTaxonId() == null) {
                            row.setInputTaxonId(measurement.getInputTaxonId());
                        } else if (measurement.getInputTaxonId() != null && !row.getInputTaxonId().equals(measurement.getInputTaxonId())) {
                            // the input taxon Id in measurement differs
                            LOG.error(String.format("input taxon id in measurement (id=%s) differs with input taxon id in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                        if (StringUtils.isBlank(row.getInputTaxonName())) {
                            row.setInputTaxonName(measurement.getInputTaxonName());
                        } else if (measurement.getInputTaxonName() != null && !row.getInputTaxonName().equals(measurement.getInputTaxonName())) {
                            // the input taxon name in measurement differs
                            LOG.error(String.format("input taxon name in measurement (id=%s) differs with input taxon name in previous measurements with same individual id (=%s) !",
                                measurement.getId(), measurement.getIndividualId()));
                        }
                    }
                }

                // build a new row
                if (row == null) {
                    row = createNewRow(readOnly, bean);
                    row.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));
                    row.setIndividualId(measurement.getIndividualId());
                    row.setTaxonGroup(measurement.getTaxonGroup());
                    row.setTaxon(measurement.getTaxon());
                    row.setInputTaxonId(measurement.getInputTaxonId());
                    row.setInputTaxonName(measurement.getInputTaxonName());
                    row.setAnalyst(measurement.getAnalyst());
                    // Mantis #26724 or #29130: don't take only one comment but a concatenation of all measurements comments
//                    row.setComment(measurement.getComment());

                    row.setValid(true);

                    rows.add(row);

                    // add errors on new row if samplingOperations have some
                    List<ErrorDTO> errors = ReefDbBeans.filterCollection(bean.getErrors(),
                        input -> input != null
                            && ControlElementValues.MEASUREMENT.equals(input.getControlElementCode())
                            && Objects.equals(input.getIndividualId(), measurement.getIndividualId())
                    );
                    ReefDbBeans.addUniqueErrors(row, errors);
                }

                // add measurement on current row
                row.getIndividualMeasurements().add(measurement);

                // add errors on row if measurement have some
                ReefDbBeans.addUniqueErrors(row, measurement.getErrors());

            }

        }

        // Mantis #26724 or #29130: don't take only one comment but a concatenation of all measurements comments
        for (R row : rows) {

            // join and affect
            row.setComment(ReefDbBeans.getUnifiedCommentFromIndividualMeasurements(row));
        }

        return rows;
    }

    protected abstract List<? extends MeasurementAware> getMeasurementAwareModels();

    protected abstract MeasurementAware getMeasurementAwareModelForRow(R row);

    protected abstract R createNewRow(boolean readOnly, MeasurementAware parentBean);

    @Override
    protected void onRowsAdded(List<R> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            R newRow = addedRows.get(0);

            // If newRow has no pmfm and no measurement (a real new row)
            if (CollectionUtils.isEmpty(newRow.getIndividualPmfms()) && CollectionUtils.isEmpty(newRow.getIndividualMeasurements())) {

                // Affect individual pmfms from parent model
                newRow.setIndividualPmfms(new ArrayList<>(getModel().getPmfms()));

                // Create empty measurements
                ReefDbBeans.createEmptyMeasurements(newRow);

                // Set default value if filters are set
                initAddedRow(newRow);
            }

            // set default analyst from pmfm strategies (Mantis #42619)
            setDefaultAnalyst(newRow);

            // reset the cell editors
            resetCellEditors();

            getModel().setModify(true);

            // Ajouter le focus sur la cellule de la ligne cree
            setFocusOnCell(newRow);

        } else {

            // set default analyst from pmfm strategies for all new rows (ie. from grid initialization) (Mantis #49331)
            setDefaultAnalyst(addedRows);
        }
    }

    protected void setDefaultAnalyst(R row) {
        setDefaultAnalyst(Collections.singleton(row));
    }

    protected void setDefaultAnalyst(Collection<R> rows) {

        if (CollectionUtils.isEmpty(rows))
            return;

        DepartmentDTO analyst = getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
            getModel().getSurvey(),
            getModel().getPmfms()
        );

        rows.forEach(row -> row.setAnalyst(analyst));
    }

    protected void initAddedRow(R row) {
        if (getModel().getTaxonGroupFilter() != null) {
            row.setTaxonGroup(getModel().getTaxonGroupFilter());
        }
        if (getModel().getTaxonFilter() != null) {
            row.setTaxon(getModel().getTaxonFilter());
        }
    }

    @Override
    protected void onRowModified(int rowIndex, R row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // if a value of individual pmfm changes
        if (AbstractMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS.equals(propertyName)) {

            // no need to tell the table is modified if no pmfms value change
            if (oldValue == newValue) return;

            // if a value has been changed, update other editor with preconditions
            if (!getModel().isAdjusting())
                updatePmfmCellEditors(row, propertyIndex, newValue != null);
        }

        // update taxon when taxon group is updated
        if (AbstractMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP.equals(propertyName)) {
            // filter taxon
            updateTaxonCellEditor(row, false);

            // reset measurementId if no more taxon group or new taxon group
            if ((newValue == null ^ oldValue == null) && row.getTaxon() == null) {

                resetIndividualMeasurementIds(row);
            }
        }

        if (AbstractMeasurementsGroupedRowModel.PROPERTY_TAXON.equals(propertyName)) {
            // filter taxon
            updateTaxonGroupCellEditor(row, false);

            // reset measurementId if no more taxon or new taxon
            if ((newValue == null ^ oldValue == null) && row.getTaxonGroup() == null) {

                resetIndividualMeasurementIds(row);
            }

            // update input taxon
            TaxonDTO taxon = (TaxonDTO) newValue;
            row.setInputTaxonId(taxon != null ? taxon.getId() : null);
            row.setInputTaxonName(taxon != null ? taxon.getName() : null);
        }

        // fire modify event
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        // if value has changed, process save to bean
        if (oldValue != newValue) {

            // save modifications ot the row to bean
//            saveMeasurementsInModel(row);

            // also recompute valid state on all rows
            recomputeRowsValidState();
        }

    }

    protected void resetIndividualMeasurementIds(R row) {
        if (row != null && CollectionUtils.isNotEmpty(row.getIndividualMeasurements())) {
            row.getIndividualMeasurements().forEach(measurement -> measurement.setId(null));
        }
    }

    protected void setDirty(MeasurementAware bean) {
        if (bean instanceof DirtyAware) ((DirtyAware) bean).setDirty(true);
    }

    protected void updateMeasurementFromRow(MeasurementDTO measurement, R row) {
        measurement.setIndividualId(row.getIndividualId());
        measurement.setTaxonGroup(row.getTaxonGroup());
        measurement.setTaxon(row.getTaxon());
        measurement.setInputTaxonId(row.getInputTaxonId());
        measurement.setInputTaxonName(row.getInputTaxonName());
        measurement.setComment(row.getComment());
        measurement.setAnalyst(row.getAnalyst());
    }

    public void removeIndividualMeasurements() {

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Can't remove rows: no row selected");
            return;
        }

        if (askBeforeDelete(t("reefdb.action.delete.survey.measurement.titre"), t("reefdb.action.delete.survey.measurement.message"))) {

            // Get distinct models to update after rows removed
            Collection<MeasurementAware> modelsToUpdate = getModel().getSelectedRows().stream()
                .map(this::getMeasurementAwareModelForRow)
                .collect(Collectors.toSet());

            // Remove from table
            getModel().deleteSelectedRows();

            modelsToUpdate.forEach(this::setDirty);
            getModel().setModify(true);
            recomputeRowsValidState();
        }

    }

    public void duplicateSelectedRow() {

        if (getModel().getSelectedRows().size() == 1 && getModel().getSingleSelectedRow() != null) {

            R rowToDuplicate = getModel().getSingleSelectedRow();
            MeasurementAware bean = getMeasurementAwareModelForRow(rowToDuplicate);
            R row = createNewRow(false, bean);
            row.setTaxonGroup(rowToDuplicate.getTaxonGroup());
            row.setTaxon(rowToDuplicate.getTaxon());
            row.setInputTaxonId(rowToDuplicate.getInputTaxonId());
            row.setInputTaxonName(rowToDuplicate.getInputTaxonName());
            row.setComment(rowToDuplicate.getComment());
            row.setIndividualPmfms(rowToDuplicate.getIndividualPmfms());
            // add also analyst (MAntis #45054)
            row.setAnalyst(rowToDuplicate.getAnalyst());
            // duplicate measurements
            row.setIndividualMeasurements(ReefDbBeans.duplicate(rowToDuplicate.getIndividualMeasurements()));

            // Add duplicate measurement to table
            getModel().insertRowAfterSelected(row);

            setDirty(bean);
            recomputeRowsValidState();
            getModel().setModify(true);
            setFocusOnCell(row);
        }
    }

    /* validation section */

    /**
     * Call this method only before save to perform perfect duplicate check
     * It can be too long if many rows (see Mantis #50040)
     */
    @Override
    public void recomputeRowsValidState() {

        // recompute individual row valid state
        super.recomputeRowsValidState();

        // and check perfect duplicates
        hasNoTaxonPerfectDuplicates();
    }

    @Override
    protected boolean isRowValid(R row) {
        boolean valid = super.isRowValid(row);

        if (!valid && !row.isMandatoryValid()) {
            // check invalid mandatory errors
            new ArrayList<>(row.getInvalidMandatoryIdentifiers()).forEach(invalidIdentifier -> {
                if (row.getMultipleValuesOnIdentifier().contains(invalidIdentifier)) {
                    // if this identifier has multiple value, remove error
                    row.getErrors().removeIf(error -> error.getPropertyName().size() == 1 && error.getPropertyName().contains(invalidIdentifier.getPropertyName()));
                    row.getInvalidMandatoryIdentifiers().remove(invalidIdentifier);
                }
            });
            valid = row.isMandatoryValid();
        }

        boolean noUnicityDuplicates = hasNoUnicityDuplicates(row);
        boolean noPreconditionErrors = hasNoPreconditionErrors(row);
        boolean noGroupedRuleErrors = hasNoGroupedRuleErrors(row);
        boolean hasAnalyst = hasAnalyst(row);

        return valid && noUnicityDuplicates && noPreconditionErrors && noGroupedRuleErrors && hasAnalyst;
    }

    protected boolean hasAnalyst(R row) {
        if (!row.getMultipleValuesOnIdentifier().contains(AbstractMeasurementsGroupedTableModel.ANALYST) &&
            row.getAnalyst() == null &&
            row.getIndividualMeasurements().stream()
                .filter(measurement -> getModel().getSurvey().getPmfmsUnderMoratorium().stream().noneMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(measurement.getPmfm(), moratoriumPmfm)))
                .anyMatch(this::isMeasurementNotEmpty)
        ) {
            ReefDbBeans.addError(row,
                t("reefdb.validator.error.analyst.required"),
                AbstractMeasurementsGroupedRowModel.PROPERTY_ANALYST);
            return false;
        }
        return true;
    }

    protected boolean isMeasurementNotEmpty(MeasurementDTO measurement) {
        return !ReefDbBeans.isMeasurementEmpty(measurement);
    }

    /**
     * Check and set invalid rows which are perfect duplicates (Mantis #50040)
     */
    protected void hasNoTaxonPerfectDuplicates() {

        if (getModel().getRowCount() < 2) {
            // no need to check duplicates
            return;
        }

        Map<String, List<R>> duplicatedMap = getModel().getRows().stream().collect(Collectors.groupingBy(r -> r.rowWithMeasurementsHashCode()));
        duplicatedMap.entrySet().stream().filter(entry -> entry.getValue().size() > 1)
            .forEach(entry -> entry.getValue().forEach(duplicatedRow -> {
                ReefDbBeans.addWarning(duplicatedRow,
                    t("reefdb.measurement.grouped.duplicates"), duplicatedRow.getDefaultProperties().toArray(new String[0]));
                ReefDbBeans.getPmfmIdsOfNonEmptyIndividualMeasurements(duplicatedRow).forEach(pmfmId -> ReefDbBeans.addWarning(duplicatedRow,
                    t("reefdb.measurement.grouped.duplicates"),
                    pmfmId,
                    AbstractMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS));
            }));

    }

    protected boolean hasNoUnicityDuplicates(R row) {

        // TODO: LP 08/01/2020: try to use same algo as hasNoTaxonPerfectDuplicates but it's a bit more difficult due to valid state to return on each row
        // use rowWithMeasurementsHashCode(Collection<PmfmDTO> filterPmfms)

        if (getModel().getRowCount() < 2
            || (row.getTaxonGroup() == null && row.getTaxon() == null)
            || CollectionUtils.isEmpty(getModel().getUniquePmfms())) {
            // no need to check duplicates
            return true;
        }

        for (R otherRow : getModel().getRows()) {
            if (otherRow == row || (otherRow.getTaxonGroup() == null && otherRow.getTaxon() == null)) continue;

            if (row.isSameRow(otherRow)
                // TODO : ce bug n'a pas été trouvé !
//                    && row.getIndividualMeasurements().size() == otherRow.getIndividualMeasurements().size()
            ) {
                // if rows are equals, check measurement values with unique pmfms
                for (PmfmDTO uniquePmfm : getModel().getUniquePmfms()) {

                    // find the measurement with this pmfm in the row
                    MeasurementDTO measurement = ReefDbBeans.getIndividualMeasurementByPmfmId(row, uniquePmfm.getId());

                    if (measurement == null) {
                        continue;
                    }

                    // find the measurement with this pmfm in the other row
                    MeasurementDTO otherMeasurement = ReefDbBeans.getIndividualMeasurementByPmfmId(otherRow, uniquePmfm.getId());

                    if (otherMeasurement == null) {
                        continue;
                    }

                    if ((measurement.getPmfm().getParameter().isQualitative() && Objects.equals(measurement.getQualitativeValue(), otherMeasurement.getQualitativeValue()))
                        || (!measurement.getPmfm().getParameter().isQualitative() && Objects.equals(measurement.getNumericalValue(), otherMeasurement.getNumericalValue()))) {

                        // duplicate value found
                        List<String> properties = row.getDefaultProperties();
                        properties.add(AbstractMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS);
                        ReefDbBeans.addError(row,
                            t("reefdb.measurement.grouped.duplicates.taxonUnique", decorate(uniquePmfm, DecoratorService.NAME_WITH_UNIT)),
                            uniquePmfm.getId(), properties.toArray(new String[0]));
                        return false;

                    }
                }
            }
        }

        return true;
    }

    protected boolean hasNoPreconditionErrors(R row) {
        if (row.getPreconditionErrors().isEmpty()) return true;

        row.getErrors().addAll(row.getPreconditionErrors());
        return true;
    }

    /* grouped rules related methods */

    private void detectGroupedIdentifiers() {

        if (CollectionUtils.isEmpty(getModel().getSurvey().getGroupedRules())) return;

        for (ControlRuleDTO groupedRule : getModel().getSurvey().getGroupedRules()) {
            boolean allIdentifiersFound = true;
            List<Map.Entry<RuleGroupDTO, ? extends ReefDbColumnIdentifier<R>>> identifierByGroup = new ArrayList<>();
            for (RuleGroupDTO group : groupedRule.getGroups()) {
                ControlRuleDTO rule = group.getRule();
                if (ControlElementValues.MEASUREMENT.equals(rule.getControlElement())) {

                    if (ControlFeatureMeasurementValues.TAXON_GROUP.equals(rule.getControlFeature())) {
                        ReefDbColumnIdentifier<R> identifier = findColumnIdentifierByPropertyName(AbstractMeasurementsGroupedRowModel.PROPERTY_TAXON_GROUP);
                        if (identifier != null) {
                            identifierByGroup.add(Maps.immutableEntry(group, identifier));
                        } else {
                            allIdentifiersFound = false;
                            break;
                        }
                    } else if (ControlFeatureMeasurementValues.TAXON.equals(rule.getControlFeature())) {
                        ReefDbColumnIdentifier<R> identifier = findColumnIdentifierByPropertyName(AbstractMeasurementsGroupedRowModel.PROPERTY_TAXON);
                        if (identifier != null) {
                            identifierByGroup.add(Maps.immutableEntry(group, identifier));
                        } else {
                            allIdentifiersFound = false;
                            break;
                        }
                    } else if (ControlFeatureMeasurementValues.PMFM.equals(rule.getControlFeature())) {
                        for (PmfmDTO pmfm : rule.getRulePmfms().stream().map(RulePmfmDTO::getPmfm).collect(Collectors.toList())) {
                            PmfmTableColumn column = findPmfmColumnByPmfmId(getModel().getPmfmColumns(), pmfm.getId());
                            if (column != null) {
                                identifierByGroup.add(Maps.immutableEntry(group, column.getPmfmIdentifier()));
                            } else {
                                allIdentifiersFound = false;
                                break;
                            }
                        }
                    }
                }
            }
            if (allIdentifiersFound) {
                getModel().getIdentifiersByGroupedRuleMap().putAll(groupedRule, identifierByGroup);
            }
        }
    }

    protected boolean hasNoGroupedRuleErrors(R row) {
        if (getModel().getIdentifiersByGroupedRuleMap().isEmpty()) return true;
        boolean result = true;

        for (ControlRuleDTO groupedRule : getModel().getIdentifiersByGroupedRuleMap().keySet()) {

            boolean groupResult = false;
            for (Map.Entry<RuleGroupDTO, ? extends ReefDbColumnIdentifier<R>> identifierByGroup : getModel().getIdentifiersByGroupedRuleMap().get(groupedRule)) {
                RuleGroupDTO group = identifierByGroup.getKey();
                ReefDbColumnIdentifier<R> identifier = identifierByGroup.getValue();

                if (row.getMultipleValuesOnIdentifier().contains(identifier)
                    || (identifier instanceof ReefDbPmfmColumnIdentifier && row.getMultipleValuesOnPmfmIds().contains(((ReefDbPmfmColumnIdentifier) identifier).getPmfmId()))
                ) {
                    groupResult = true;
                    break;
                }

                // For now, assume logical operator is always OR
                Assert.isTrue(group.isIsOr());
                groupResult |= getContext().getControlRuleService().controlUniqueObject(group.getRule(), identifier.getValue(row));
            }

            if (!groupResult) {
                // build error message
                String message;
                List<String> columnNames = new ArrayList<>();
                Set<String> propertyNames = new HashSet<>();
                Set<String> pmfmPropertyNames = new HashSet<>();
                Set<Integer> pmfmIds = new HashSet<>();
                for (RuleGroupDTO group : groupedRule.getGroups()) {
                    if (ReefDbBeans.isPmfmMandatory(group.getRule())) {
                        // iterate pmfms
                        for (PmfmDTO pmfm : group.getRule().getRulePmfms().stream().map(RulePmfmDTO::getPmfm).collect(Collectors.toList())) {
                            ReefDbPmfmColumnIdentifier<R> pmfmIdentifier = findPmfmColumnByPmfmId(getModel().getPmfmColumns(), pmfm.getId()).getPmfmIdentifier();
                            columnNames.add(pmfmIdentifier.getHeaderLabel());
                            pmfmPropertyNames.add(pmfmIdentifier.getPropertyName());
                            pmfmIds.add(pmfm.getId());
                        }
                    } else {
                        ReefDbColumnIdentifier<R> identifier = getModel().getIdentifierByGroup(group);
                        columnNames.add(t(identifier.getHeaderI18nKey()));
                        propertyNames.add(identifier.getPropertyName());
                    }
                }
                if (StringUtils.isNotBlank(groupedRule.getMessage())) {
                    message = groupedRule.getMessage();
                } else {
                    message = t("reefdb.measurement.grouped.invalidGroupedRule", Joiner.on(',').join(columnNames));
                }

                // add error on normal column
                ReefDbBeans.addError(row, message, propertyNames.toArray(new String[0]));
                // add error on pmfm column
                if (!pmfmIds.isEmpty())
                    for (Integer pmfmId : pmfmIds)
                        ReefDbBeans.addError(row, message, pmfmId, pmfmPropertyNames.toArray(new String[0]));
            }

            // if a group result is false, the result is false (= has error)
            result &= groupResult;

        }

        return result;
    }

    /* Preconditions related methods */

    private void detectPreconditionedPmfms() {

        if (CollectionUtils.isEmpty(getModel().getSurvey().getPreconditionedRules())) return;

        for (ControlRuleDTO preconditionedRule : getModel().getSurvey().getPreconditionedRules()) {

            for (PreconditionRuleDTO precondition : preconditionedRule.getPreconditions()) {

                int basePmfmId = precondition.getBaseRule().getRulePmfms(0).getPmfm().getId();
                int usedPmfmId = precondition.getUsedRule().getRulePmfms(0).getPmfm().getId();

                getModel().addPreconditionRuleByPmfmId(basePmfmId, precondition);
                if (precondition.isBidirectional())
                    getModel().addPreconditionRuleByPmfmId(usedPmfmId, precondition);
            }
        }
    }

    /**
     * Update all editors on measurements if they have preconditions
     *
     * @param row               the actual selected row
     * @param firstPmfmId       the first pmfm Id to treat (can be null)
     * @param resetValueAllowed allow or not the target value to be reset
     */
    private void updatePmfmCellEditors(R row, Integer firstPmfmId, boolean resetValueAllowed) {

        if (row == null || CollectionUtils.isEmpty(row.getIndividualPmfms())) return;

        // Build list of pmfms to process (if firstPmfmId is specified, it will be threat first)
        List<PmfmDTO> allPmfms;
        if (firstPmfmId == null) {
            allPmfms = row.getIndividualPmfms();
        } else {
            allPmfms = new ArrayList<>(row.getIndividualPmfms());
            PmfmDTO firstPmfm = ReefDbBeans.findById(allPmfms, firstPmfmId);
            if (firstPmfm != null) {
                allPmfms.remove(firstPmfm);
                // add this pmfm at first position
                allPmfms.add(0, firstPmfm);
            }
        }

        // clear previous allowed values map
        row.getAllowedQualitativeValuesMap().clear();
        row.getPreconditionErrors().clear();
        List<Integer> allPmfmIds = allPmfms.stream().map(PmfmDTO::getId).collect(Collectors.toList());

        for (PmfmDTO pmfm : allPmfms) {

            // if there is a preconditioned rule for this pmfm
            if (getModel().isPmfmIdHasPreconditions(pmfm.getId())) {

                // get the measurement with this pmfm
                MeasurementDTO measurement = ReefDbBeans.getIndividualMeasurementByPmfmId(row, pmfm.getId());
                if (measurement == null) {
                    // create empty measurement if needed
                    measurement = ReefDbBeanFactory.newMeasurementDTO();
                    measurement.setPmfm(pmfm);
                    row.getIndividualMeasurements().add(measurement);
                }

                // Process allowed qualitative values
                getContext().getRuleListService().buildAllowedValuesByPmfmId(
                    measurement,
                    allPmfmIds,
                    getModel().getPreconditionRulesByPmfmId(pmfm.getId()),
                    row.getAllowedQualitativeValuesMap());

                // update combos of affected pmfms
                for (Integer targetPmfmId : row.getAllowedQualitativeValuesMap().getTargetIds()) {
                    updateQualitativePmfmCellEditors(row, targetPmfmId, resetValueAllowed);
                }

            }
        }

        // then update all pmfm editors a last time
        for (PmfmDTO pmfm : allPmfms) {
            if (pmfm.getParameter().isQualitative()) {
                updateQualitativePmfmCellEditors(row, pmfm.getId(), resetValueAllowed);
            }
        }

    }

    @SuppressWarnings("unchecked")
    private void updateQualitativePmfmCellEditors(R row, int targetPmfmId, boolean resetValueAllowed) {

        PmfmDTO targetPmfm = ReefDbBeans.findById(row.getIndividualPmfms(), targetPmfmId);
        PmfmTableColumn targetPmfmColumn = findPmfmColumnByPmfmId(getModel().getPmfmColumns(), targetPmfmId);

        // cellEditor must be a ExtendedComboBoxCellEditor
        if (!(targetPmfmColumn.getCellEditor() instanceof ExtendedComboBoxCellEditor)) return; // simply skip
        ExtendedComboBoxCellEditor<QualitativeValueDTO> comboBoxCellEditor = (ExtendedComboBoxCellEditor<QualitativeValueDTO>) targetPmfmColumn.getCellEditor();

        AllowedQualitativeValuesMap.AllowedValues allowedTargetIds = new AllowedQualitativeValuesMap.AllowedValues();

        Collection<Integer> sourcePmfmIds = row.getAllowedQualitativeValuesMap().getExistingSourcePmfmIds(targetPmfmId);
        Integer lastNumericalSourcePmfmId = null;
        for (Integer sourcePmfmId : sourcePmfmIds) {

            // get existing measurement for this source pmfm
            MeasurementDTO sourceMeasurement = ReefDbBeans.getIndividualMeasurementByPmfmId(row, sourcePmfmId);
            boolean sourceIsNumerical = false;

            if (sourceMeasurement != null) {
                AllowedQualitativeValuesMap.AllowedValues allowedValues = null;
                if (sourceMeasurement.getQualitativeValue() != null) {
                    allowedValues = row.getAllowedQualitativeValuesMap().getAllowedValues(targetPmfmId, sourcePmfmId, sourceMeasurement.getQualitativeValue().getId());
                } else if (sourceMeasurement.getNumericalValue() != null) {
                    allowedValues = row.getAllowedQualitativeValuesMap().getAllowedValues(targetPmfmId, sourcePmfmId, sourceMeasurement.getNumericalValue());
                    sourceIsNumerical = true;
                }
                if (allowedValues != null) {
                    allowedTargetIds.addOrRetain(allowedValues);
                    if (sourceIsNumerical) lastNumericalSourcePmfmId = sourcePmfmId;
                }
            }
        }

        // Compute allowed values
        List<QualitativeValueDTO> allowedQualitativeValues = ReefDbBeans.filterCollection(targetPmfm.getQualitativeValues(),
            qualitativeValue -> allowedTargetIds.isAllowed(qualitativeValue.getId()));
        comboBoxCellEditor.getCombo().setData(allowedQualitativeValues);

        // fix the target value
        QualitativeValueDTO actualValue = (QualitativeValueDTO) targetPmfmColumn.getPmfmIdentifier().getValue(row);
        if (resetValueAllowed) {
            if (allowedQualitativeValues.size() == 1) {
                if (actualValue == null) {
                    // force a 1:1 relation (only the first time)
                    getModel().setAdjusting(true);
                    targetPmfmColumn.getPmfmIdentifier().setValue(row, allowedQualitativeValues.get(0));
                    getModel().setAdjusting(false);
                }
            } else {
                if (actualValue != null && !allowedQualitativeValues.contains(actualValue) && lastNumericalSourcePmfmId == null) {
                    // reset a bad 1:n relation
                    getModel().setAdjusting(true);
                    targetPmfmColumn.getPmfmIdentifier().setValue(row, null);
                    getModel().setAdjusting(false);
                }
            }
        }

        // check integrity
        if (!allowedQualitativeValues.isEmpty() && actualValue != null && !allowedQualitativeValues.contains(actualValue) && lastNumericalSourcePmfmId != null) {
            PmfmDTO sourcePmfm = getContext().getReferentialService().getPmfm(lastNumericalSourcePmfmId);
            ErrorDTO error = ReefDbBeanFactory.newErrorDTO();
            error.setError(true);
            error.setMessage(t("reefdb.measurement.grouped.incoherentQualitativeAndNumericalValues",
                decorate(targetPmfm, DecoratorService.NAME_WITH_UNIT), decorate(sourcePmfm, DecoratorService.NAME_WITH_UNIT)));
            error.setPropertyName(ImmutableList.of(AbstractMeasurementsGroupedRowModel.PROPERTY_INDIVIDUAL_PMFMS));
            error.setPmfmId(lastNumericalSourcePmfmId);
            ReefDbBeans.addUniqueErrors(row.getPreconditionErrors(), ImmutableList.of(error));
            recomputeRowValidState(row);
        }
    }

    /**
     * Open the multiline edit dialog (Mantis #49615)
     *
     * @param dialog the dialog to open
     */
    protected void editSelectedMeasurements(JDialog dialog) {
        // save current table state to be able to restore it in dialog's table
        saveTableState();

        Assert.isInstanceOf(ReefDbUI.class, dialog);
        ReefDbUI<?, ?> multiEditUI = (ReefDbUI<?, ?>) dialog;
        Assert.isInstanceOf(AbstractMeasurementsMultiEditUIModel.class, multiEditUI.getModel());
        AbstractMeasurementsMultiEditUIModel<MeasurementDTO, R, ?> multiEditModel = (AbstractMeasurementsMultiEditUIModel<MeasurementDTO, R, ?>) multiEditUI.getModel();
        multiEditModel.setObservationHandler(getModel().getObservationUIHandler());
        multiEditModel.setRowsToEdit(getModel().getSelectedRows());
        multiEditModel.setPmfms(getModel().getPmfms());
        multiEditModel.setSurvey(getModel().getSurvey());

        // get parent component for dialog centering
        JComponent parent = getUI().getParentContainer(getTable(), JScrollPane.class);
        Insets borderInsets = parent.getBorder().getBorderInsets(parent);
        openDialog(dialog, new Dimension(parent.getWidth() + borderInsets.left + borderInsets.right, 160), true, parent);

        // if dialog is valid
        if (multiEditModel.isValid()) {

            // affect all non multiple column values to selected rows
            R multiEditRow = multiEditModel.getRows().get(0);

            for (R row : getModel().getSelectedRows()) {

                // affect column values
                for (ReefDbColumnIdentifier<R> identifier : multiEditModel.getIdentifiersToCheck()) {
                    // be sure that sampling operation value can not be changed
                    if (identifier.getPropertyName().equals(OperationMeasurementsGroupedRowModel.PROPERTY_SAMPLING_OPERATION))
                        continue;

                    // get values
                    Object multiValue = identifier.getValue(multiEditRow);
                    Object currentValue = identifier.getValue(row);
                    boolean isMultiple = multiEditRow.getMultipleValuesOnIdentifier().contains(identifier);
                    // affect value if a multiple value has been replaced, or modification has been made
                    if ((!isMultiple || multiValue != null) && !Objects.equals(multiValue, currentValue)) {
                        identifier.setValue(row, multiValue);
                        // special case for INPUT_TAXON_NAME
                        if (identifier.getPropertyName().equals(AbstractMeasurementsGroupedRowModel.PROPERTY_INPUT_TAXON_NAME)) {
                            // affect also INPUT_TAXON_ID
                            row.setInputTaxonId(multiEditRow.getInputTaxonId());
                        }
                    }
                }

                // for measurement columns
                for (PmfmDTO pmfm : getModel().getPmfms()) {
                    MeasurementDTO multiMeasurement = ReefDbBeans.getIndividualMeasurementByPmfmId(multiEditRow, pmfm.getId());
                    if (multiMeasurement == null) {
                        // can happened if user pass on on a cel without setting a value
                        multiMeasurement = ReefDbBeanFactory.newMeasurementDTO();
                    }
                    boolean isMultiple = multiEditRow.getMultipleValuesOnPmfmIds().contains(pmfm.getId());
                    // affect value if a multiple value has been replaced, or modification has been made
                    if (!isMultiple || !ReefDbBeans.isMeasurementEmpty(multiMeasurement)) {
                        MeasurementDTO measurement = ReefDbBeans.getIndividualMeasurementByPmfmId(row, pmfm.getId());
                        if (measurement == null) {
                            // create and add the measurement if not exists
                            measurement = ReefDbBeanFactory.newMeasurementDTO();
                            measurement.setPmfm(pmfm);
                            measurement.setIndividualId(row.getIndividualId());
                            row.getIndividualMeasurements().add(measurement);
                        }
                        // affect value (either numeric or qualitative)
                        if (!ReefDbBeans.measurementValuesEquals(multiMeasurement, measurement)) {
                            measurement.setNumericalValue(multiMeasurement.getNumericalValue());
                            measurement.setQualitativeValue(multiMeasurement.getQualitativeValue());
                        }
                    }
                }

            }

            // done
            getModel().setModify(true);
            getTable().repaint();
        }
    }

    /**
     * Save grouped measurements on parent model(s)
     * This replace inline save (see Mantis #51725)
     */
    public void save() {

        // Get all existing measurements in beans
        List<? extends MeasurementAware> beansToSave = getMeasurementAwareModels();
        List<MeasurementDTO> existingMeasurements = beansToSave.stream()
            .flatMap(bean -> bean.getIndividualMeasurements().stream())
            .collect(Collectors.toList());

        // get the minimum negative measurement id
        AtomicInteger minNegativeId = new AtomicInteger(
            Math.min(
                0,
                existingMeasurements.stream()
                    .filter(measurement -> measurement != null && measurement.getId() != null)
                    .mapToInt(MeasurementDTO::getId)
                    .min()
                    .orElse(0)
            )
        );

        Multimap<MeasurementAware, R> rowsByBean = ArrayListMultimap.create();

        // Get ordered rows by bean
        for (int i = 0; i < getTable().getRowCount(); i++) {
            R row = getTableModel().getEntry(getTable().convertRowIndexToModel(i));
            MeasurementAware bean = getMeasurementAwareModelForRow(row);
            if (bean == null) {
                throw new ReefDbTechnicalException("The parent bean is null for a grouped measurements row");
            }
            rowsByBean.put(bean, row);
        }

        rowsByBean.keySet().forEach(bean -> {

            AtomicInteger individualId = new AtomicInteger();
            List<MeasurementDTO> beanMeasurements = new ArrayList<>();

            // Iterate over each row
            rowsByBean.get(bean).forEach(row -> {

                // Test is this row is to save or not
                if (isRowToSave(row)) {

                    // Affect row individual ids according this order
                    row.setIndividualId(individualId.incrementAndGet());

                    // Iterate over row's measurements
                    row.getIndividualMeasurements().forEach(measurement -> {
                        if (measurement.getId() == null) {
                            // this measurement was not in bean, so add it with next negative id
                            measurement.setId(minNegativeId.decrementAndGet());
                        }
                        // update this measurement
                        updateMeasurementFromRow(measurement, row);
                        // Add to new list
                        beanMeasurements.add(measurement);
                    });

                }
            });

            // Affect new list of measurements
            bean.getIndividualMeasurements().clear();
            bean.getIndividualMeasurements().addAll(beanMeasurements);

            setDirty(bean);
            // Remove this bean from the list
            beansToSave.remove(bean);
        });

        // Clean remaining beans
        beansToSave.forEach(bean -> {
            // If a bean still in this map, it means there is no row of this bean, so remove all
            bean.getIndividualMeasurements().clear();
            setDirty(bean);
        });
    }

    /**
     * Determine if this row is to be saved
     *
     * @param row to test
     * @return true if to save (default)
     */
    protected boolean isRowToSave(R row) {
        return
            // The row must exists
            row != null
                // and contains at least 1 non empty individual measurement or a taxon group or a taxon
                && (row.hasTaxonInformation() || row.hasNonEmptyMeasurements());
    }
}
