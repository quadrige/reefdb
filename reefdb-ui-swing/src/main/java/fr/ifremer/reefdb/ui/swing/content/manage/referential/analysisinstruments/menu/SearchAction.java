package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.ReferentialAnalysisInstrumentsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

/**
 * Action permettant la recherche de analysisInstruments au niveau national
 */
public class SearchAction extends AbstractCheckModelAction<AnalysisInstrumentsMenuUIModel, AnalysisInstrumentsMenuUI, AnalysisInstrumentsMenuUIHandler> {

    private List<AnalysisInstrumentDTO> result;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final AnalysisInstrumentsMenuUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getParentUI() != null && getParentUI().getModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        if (getParentUI() == null) {
            return;
        }
        getParentUI().getModel().setModify(modelModify);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getParentUI() == null || getParentUI().getModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getParentUI() != null ? getParentUI().getHandler() : null;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        result = getContext().getReferentialService().searchAnalysisInstruments(
                getModel().getStatusFilter(),
                getModel().getAnalysisInstrumentId(),
                getModel().getStatusCode());

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setResults(result);

        super.postSuccessAction();
    }

    private ReferentialAnalysisInstrumentsUI getParentUI() {
        return getUI().getParentContainer(ReferentialAnalysisInstrumentsUI.class);
    }

}
