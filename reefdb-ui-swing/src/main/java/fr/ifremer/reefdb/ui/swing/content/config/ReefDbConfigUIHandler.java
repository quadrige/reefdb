package fr.ifremer.reefdb.ui.swing.content.config;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.config.QuadrigeConfigurationOption;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfigurationOption;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.ui.swing.table.editor.DatePatternCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.renderer.CheckBoxRenderer;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.config.ReefDbConfigurationOption;
import fr.ifremer.reefdb.config.ReefDbReadOnlyConfigurationOption;
import fr.ifremer.reefdb.config.ReefDbSecuredConfigurationOption;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.action.GoToPreviousScreenAction;
import fr.ifremer.reefdb.ui.swing.action.ReloadApplicationAction;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.config.ConfigUI;
import jaxx.runtime.swing.config.ConfigUIHelper;
import jaxx.runtime.swing.config.model.MainCallBackFinalizer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbConfigUIHandler class.</p>
 *
 * @since 1.0
 */
public class ReefDbConfigUIHandler extends AbstractReefDbUIHandler<ReefDbUIContext, ReefDbConfigUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReefDbConfigUIHandler.class);

    /**
     * Constant <code>CALLBACK_APPLICATION="application"</code>
     */
    public static final String CALLBACK_APPLICATION = "application";

    /**
     * Constant <code>CALLBACK_UI="ui"</code>
     */
    public static final String CALLBACK_UI = "ui";

//    public static final String CALLBACK_CACHE = "cache";

    @Override
    public void beforeInit(ReefDbConfigUI ui) {
        super.beforeInit(ui);

        ui.setContextValue(SwingUtil.createActionIcon("config"));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(ReefDbConfigUI ui) {

        initUI(ui);

        getContext().getSwingSession().addUnsavedRootComponentByName(ui.getName());

        ReefDbConfiguration config = getConfig();

        ConfigUIHelper helper = new ConfigUIHelper(config.getApplicationConfig(), config.getApplicationConfig(), config.getConfigFile());

        helper.registerCallBack(
                CALLBACK_UI, n("reefdb.config.action.reload.ui"),
                SwingUtil.createActionIcon("reload-ui"),
                this::reloadUI
        ).registerCallBack(
                CALLBACK_APPLICATION, n("reefdb.config.action.reload.application"),
                SwingUtil.createActionIcon("reload-application"),
                this::reloadApplication
        )/*.registerCallBack(
                CALLBACK_CACHE, n("reefdb.config.action.reload.cache"),
                SwingUtil.createActionIcon("reload-ui"),
                new Runnable() {

                    @Override
                    public void run() {
                        reloadPersistence();
                    }
                }
        )*/;

//        // Application category
//        helper.addCategory(n("reefdb.config.category.applications"),
//            n("reefdb.config.category.applications.description"), CALLBACK_CACHE);
//            //            .addOption(ReefDbConfigurationOption.CSV_SEPARATOR)
//            //            .setOptionEditor(new DefaultCellEditor(new JTextField()))
//            //            .setOptionShortLabel(t("reefdb.config.option.csv.separator.shortLabel"))
//
//        helper.addOption(ReefDbConfigurationOption.PREDOC_PERIOD_LENGTH)
//            .setOptionEditor(newNumberCellEditor(Integer.class, false, ReefDbUI.INT_2_DIGITS_PATTERN, 1, 24))
//            .setOptionShortLabel(t("reefdb.config.option.predocumentation.period.length.shortLabel"));

        // Data category
        List<QuadrigeUserAuthority> neededRoles = Lists.newArrayList(QuadrigeUserAuthority.LOCAL_ADMIN, QuadrigeUserAuthority.ADMIN);
        helper.addCategory(n("reefdb.config.category.data"),
                n("reefdb.config.category.data.description"),
                CALLBACK_APPLICATION)
                // Authentification URL :
                .addOption(new ReefDbSecuredConfigurationOption(QuadrigeCoreConfigurationOption.AUTHENTICATION_INTRANET_SITE_URL, neededRoles))
                .setOptionEditor(new DefaultCellEditor(new JTextField()))
                .setOptionShortLabel(t("reefdb.config.option.authentication.intranet.site.url.shortLabel"))
                .addOption(new ReefDbSecuredConfigurationOption(QuadrigeCoreConfigurationOption.AUTHENTICATION_EXTRANET_SITE_URL, neededRoles))
                .setOptionShortLabel(t("reefdb.config.option.authentication.extranet.site.url.shortLabel"))
                .addOption(new ReefDbSecuredConfigurationOption(QuadrigeConfigurationOption.SYNCHRONIZATION_SITE_URL, neededRoles))
                .setOptionShortLabel(t("reefdb.config.option.synchronization.site.url.shortLabel"))
                .addOption(new ReefDbReadOnlyConfigurationOption(ReefDbConfigurationOption.PMFM_ID_DEPTH_VALUES))
                .setOptionShortLabel(t("reefdb.config.option.pmfm.id.depthValues.shortLabel"))
                .addOption(new ReefDbReadOnlyConfigurationOption(ReefDbConfigurationOption.PMFM_IDS_TRANSITION_LENGTH))
                .setOptionShortLabel(t("reefdb.config.option.pmfm.ids.transitionLength.shortLabel"))
        ;

        // UI category
        helper.addCategory(n("reefdb.config.category.ui"),
                n("reefdb.config.category.ui.description"),
                CALLBACK_UI)
                .addOption(ReefDbConfigurationOption.COLOR_ALTERNATE_ROW)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.alternateRow.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_SELECTED_ROW)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.selectedRow.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_ROW_INVALID)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.rowInvalid.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_ROW_READ_ONLY)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.rowReadOnly.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_CELL_WITH_VALUE)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.cellWithValue.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_COMPUTED_WEIGHTS)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.computedWeights.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_BLOCKING_LAYER)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.blockingLayer.shortLabel"))
                .addOption(ReefDbConfigurationOption.COLOR_SELECTED_CELL)
                .setOptionShortLabel(t("reefdb.config.option.ui.color.selectedCell.shortLabel"))
                .addOption(ReefDbConfigurationOption.DATE_FORMAT).setOptionEditor(new DatePatternCellEditor())
                .setOptionShortLabel(t("reefdb.config.option.ui.dateFormat.shortLabel"))
                .addOption(ReefDbConfigurationOption.DATE_TIME_FORMAT).setOptionEditor(new DatePatternCellEditor())
                .setOptionShortLabel(t("reefdb.config.option.ui.dateTimeFormat.shortLabel"))
                .addOption(ReefDbConfigurationOption.TABLES_CHECKBOX).setOptionRenderer(new CheckBoxRenderer())
                .setOptionShortLabel(t("reefdb.config.option.ui.table.showCheckbox.shortLabel"));

        // Fonctional category
//        helper.addCategory(n("reefdb.config.category.fonctional"),
//                n("reefdb.config.category.fonctional.description"), CALLBACK_UI)
//                .addOption(ReefDbConfigurationOption.TABBED_PANE_OBSERVATION_MESURES_CHECKBOX).setOptionRenderer(new CheckBoxRenderer())
//                .setOptionShortLabel(t("reefdb.config.option.ui.tabbedPane.observation.mesures.showCheckbox"))
//                .addOption(ReefDbConfigurationOption.TABBED_PANE_PRELEVEMENTS_DETAILS_CHECKBOX).setOptionRenderer(new CheckBoxRenderer())
//                .setOptionShortLabel(t("reefdb.config.option.ui.tabbedPane.prelevements.details.showCheckbox"));

        // Technical category
        helper.addCategory(n("reefdb.config.category.technical"),
                n("reefdb.config.category.technical.description"), CALLBACK_APPLICATION)
                .addOption(ReefDbConfigurationOption.BASEDIR)
                .setOptionShortLabel(t("reefdb.config.option.basedir.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DATA_DIRECTORY)
                .setOptionShortLabel(t("reefdb.config.option.data.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.TMP_DIRECTORY)
                .setOptionShortLabel(t("reefdb.config.option.tmp.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.I18N_DIRECTORY)
                .setOptionShortLabel(t("reefdb.config.option.i18n.directory.shortLabel"))
                .addOption(ReefDbConfigurationOption.HELP_DIRECTORY)
                .setOptionShortLabel(t("reefdb.config.option.help.directory.shortLabel"))
                .addOption(QuadrigeCoreConfigurationOption.CONFIG_DIRECTORY)
                .setOptionShortLabel(t("reefdb.config.option.config.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.JDBC_URL)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.jdbc.url.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_BACKUP_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.backup.directory.shortLabel"))
                .addOption(ReefDbConfigurationOption.DB_BACKUP_EXTERNAL_DIRECTORY)
                .setOptionShortLabel(t("reefdb.config.option.persistence.db.backup.external.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_ATTACHMENT_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.attachment.directory.shortLabel"))
                .addOption(QuadrigeConfigurationOption.DB_CACHE_DIRECTORY)
                .setOptionShortLabel(t("quadrige3.config.option.persistence.db.cache.directory.shortLabel"))
                .addOption(ReefDbConfigurationOption.UI_CONFIG_FILE)
                .setOptionShortLabel(t("reefdb.config.option.ui.config.file.shortLabel"))
                .addOption(ReefDbConfigurationOption.SITE_URL)
                .setOptionShortLabel(t("reefdb.config.option.site.url.shortLabel"))
                .addOption(QuadrigeCoreConfigurationOption.UPDATE_APPLICATION_URL)
                .setOptionShortLabel(t("reefdb.config.option.update.application.url.shortLabel"))
                .addOption(QuadrigeCoreConfigurationOption.UPDATE_DATA_URL)
                .setOptionShortLabel(t("reefdb.config.option.update.data.url.shortLabel"));


        helper.setFinalizer(new MainCallBackFinalizer(CALLBACK_APPLICATION));

        helper.setCloseAction(() -> getContext().getActionEngine().runInternalAction(
                ReefDbConfigUIHandler.this,
                GoToPreviousScreenAction.class));
        ConfigUI configUI = helper.buildUI(
                getUI(),
                n("reefdb.config.category.data"));
//            n("reefdb.config.category.applications"));

        configUI.getHandler().setTopContainer(getUI());
        getUI().add(configUI, BorderLayout.CENTER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected JComponent getComponentToFocus() {
        return getUI();
    }

    /**
     * <p>reloadApplication.</p>
     */
    protected void reloadApplication() {
        ReloadApplicationAction action = getContext().getActionFactory().createLogicAction(this, ReloadApplicationAction.class);
        getContext().getActionEngine().runAction(action);
    }

    /**
     * <p>reloadUI.</p>
     */
    protected void reloadUI() {
        getContext().getMainUI().getHandler().reloadUI();
    }

//    protected void reloadPersistence() {
//        getContext().reloadDbCache(progressionModel);
//        reloadUI();
//    }

}
