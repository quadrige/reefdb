package fr.ifremer.reefdb.ui.swing.content.manage.program.programs;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class ProgramsTableModel extends AbstractReefDbTableModel<ProgramsTableRowModel> {

	/**
	 * Identifiant pour la colonne libelle.
	 */
    public static final ReefDbColumnIdentifier<ProgramsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
    		ProgramsTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.program.program.name.tip"),
            String.class,
            true);
    
    /**
     * Identifiant pour la colonne description.
     */
    public static final ReefDbColumnIdentifier<ProgramsTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
    		ProgramsTableRowModel.PROPERTY_DESCRIPTION,
            n("reefdb.property.description"),
            n("reefdb.program.program.description.tip"),
            String.class,
            true);

    /**
     * Identifiant pour la colonne code.
     */
    public static final ReefDbColumnIdentifier<ProgramsTableRowModel> CODE = ReefDbColumnIdentifier.newId(
    		ProgramsTableRowModel.PROPERTY_CODE,
            n("reefdb.property.code"),
            n("reefdb.program.program.code.tip"),
            String.class,
            true);

	/**
     * Identifiant pour la colonne local.
     */
    public static final ReefDbColumnIdentifier<ProgramsTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
    		ProgramsTableRowModel.PROPERTY_STATUS,
            n("reefdb.property.referential.local"),
            n("reefdb.program.program.local.tip"),
            Boolean.class);

	public static final ReefDbColumnIdentifier<ProgramsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
		ProgramsTableRowModel.PROPERTY_COMMENT,
		n("reefdb.property.comment"),
		n("reefdb.property.comment"),
		String.class,
		false);

	public static final ReefDbColumnIdentifier<ProgramsTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
		ProgramsTableRowModel.PROPERTY_CREATION_DATE,
		n("reefdb.property.date.creation"),
		n("reefdb.property.date.creation"),
		Date.class);

	public static final ReefDbColumnIdentifier<ProgramsTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
		ProgramsTableRowModel.PROPERTY_UPDATE_DATE,
		n("reefdb.property.date.modification"),
		n("reefdb.property.date.modification"),
		Date.class);



	/**
	 * Constructor.
	 *
	 * @param columnModel Le modele pour les colonnes
	 */
	public ProgramsTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, true, false);
    }

	/** {@inheritDoc} */
	@Override
	public ProgramsTableRowModel createNewRow() {
		return new ProgramsTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ProgramsTableRowModel> getFirstColumnEditing() {
		return NAME;
	}

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<ProgramsTableRowModel> propertyName) {

        if (DESCRIPTION == propertyName) {
			return true;
		}

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }
}
