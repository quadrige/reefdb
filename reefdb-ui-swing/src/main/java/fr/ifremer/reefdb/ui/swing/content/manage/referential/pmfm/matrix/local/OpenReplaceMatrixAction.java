package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.ManageMatricesUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.local.replace.ReplaceMatrixUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.local.replace.ReplaceMatrixUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceMatrixAction extends AbstractReefDbAction<ManageMatricesLocalUIModel, ManageMatricesLocalUI, ManageMatricesLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceMatrixAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.matrix.local.ManageMatricesLocalUIHandler} object.
     */
    public OpenReplaceMatrixAction(ManageMatricesLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<MatrixDTO, ReplaceMatrixUIModel, ReplaceMatrixUI> openAction =
                new AbstractOpenReplaceUIAction<MatrixDTO, ReplaceMatrixUIModel, ReplaceMatrixUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.pmfm.matrix");
                    }

                    @Override
                    protected ReplaceMatrixUIModel createNewModel() {
                        return new ReplaceMatrixUIModel();
                    }

                    @Override
                    protected ReplaceMatrixUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceMatrixUI(ctx);
                    }

                    @Override
                    protected List<MatrixDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getMatrices(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected MatrixDTO getSelectedSource() {
                        List<MatrixDTO> selectedBeans = OpenReplaceMatrixAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected MatrixDTO getSelectedTarget() {
                        ManageMatricesUI ui = OpenReplaceMatrixAction.this.getUI().getParentContainer(ManageMatricesUI.class);
                        List<MatrixDTO> selectedBeans = ui.getManageMatricesNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
