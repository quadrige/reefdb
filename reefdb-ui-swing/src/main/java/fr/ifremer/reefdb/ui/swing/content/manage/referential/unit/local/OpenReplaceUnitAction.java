package fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.ReferentialUnitsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.local.replace.ReplaceUnitUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.local.replace.ReplaceUnitUIModel;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 */
public class OpenReplaceUnitAction extends AbstractReefDbAction<ReferentialUnitsLocalUIModel, ReferentialUnitsLocalUI, ReferentialUnitsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceUnitAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.unit.local.ReferentialUnitsLocalUIHandler} object.
     */
    public OpenReplaceUnitAction(ReferentialUnitsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<UnitDTO, ReplaceUnitUIModel, ReplaceUnitUI> openAction =
                new AbstractOpenReplaceUIAction<UnitDTO, ReplaceUnitUIModel, ReplaceUnitUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.pmfm.unit");
                    }

                    @Override
                    protected ReplaceUnitUIModel createNewModel() {
                        ReplaceUnitUIModel model = new ReplaceUnitUIModel();
                        model.setDecoratorContext(DecoratorService.WITH_SYMBOL);
                        return model;
                    }

                    @Override
                    protected ReplaceUnitUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceUnitUI(ctx);
                    }

                    @Override
                    protected List<UnitDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getUnits(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected UnitDTO getSelectedSource() {
                        List<UnitDTO> selectedBeans = OpenReplaceUnitAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected UnitDTO getSelectedTarget() {
                        ReferentialUnitsUI ui = OpenReplaceUnitAction.this.getUI().getParentContainer(ReferentialUnitsUI.class);
                        List<UnitDTO> selectedBeans = ui.getReferentialUnitsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
