package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.ReferentialMenuUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.util.List;

/**
 * Controlleur du menu pour la gestion des Quadruplets au niveau National
 */
public class PmfmMenuUIHandler extends ReferentialMenuUIHandler<PmfmMenuUIModel, PmfmMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PmfmMenuUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final PmfmMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final PmfmMenuUIModel model = new PmfmMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final PmfmMenuUI ui) {
        super.afterInit(ui);

        // listen to model changes on 'local' to adapt combo box content
        getModel().addPropertyChangeListener(PmfmMenuUIModel.PROPERTY_LOCAL, evt -> {
            getUI().getStatusCombo().setData(getContext().getReferentialService().getStatus(getModel().getStatusFilter()));
            // reload these referential with correct status filter (Mantis #48500)
            StatusFilter referentialStatusFilter = getModel().isLocal() ? StatusFilter.ALL : StatusFilter.NATIONAL;
            getUI().getParametersCombo().setData(getContext().getReferentialService().getParameters(referentialStatusFilter));
            getUI().getMatricesCombo().setData(getContext().getReferentialService().getMatrices(referentialStatusFilter));
            getUI().getFractionsCombo().setData(getContext().getReferentialService().getFractions(referentialStatusFilter));
            getUI().getMethodsCombo().setData(getContext().getReferentialService().getMethods(referentialStatusFilter));
            getUI().getUnitsCombo().setData(getContext().getReferentialService().getUnits(referentialStatusFilter));
        });

        // Initialiser les combobox
        initComboBox();
    }

    /** {@inheritDoc} */
    @Override
    public void enableSearch(boolean enabled) {
        getUI().getLocalCombo().setEnabled(enabled);
        getUI().getNameEditor().setEnabled(enabled);
        getUI().getParametersCombo().setEnabled(enabled);
        getUI().getMatricesCombo().setEnabled(enabled);
        getUI().getFractionsCombo().setEnabled(enabled);
        getUI().getMethodsCombo().setEnabled(enabled);
        getUI().getUnitsCombo().setEnabled(enabled);
        getUI().getStatusCombo().setEnabled(enabled);
        getUI().getClearButton().setEnabled(enabled);
        getUI().getSearchButton().setEnabled(enabled);
        getApplyFilterUI().setEnabled(enabled);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getFilters() {
        return getContext().getContextService().getAllPmfmFilters();
    }

    /** {@inheritDoc} */
    @Override
    public ApplyFilterUI getApplyFilterUI() {
        return getUI().getApplyFilterUI();
    }

    /** {@inheritDoc} */
    @Override
    public JComponent getLocalFilterPanel() {
        return getUI().getLocalPanel();
    }

    /**
     * Initialisation des combobox
     */
    private void initComboBox() {

        // Combo local
        initBeanFilterableComboBox(
                getUI().getLocalCombo(),
                getContext().getSystemService().getBooleanValues(),
                null);

        initBeanFilterableComboBox(
                getUI().getParametersCombo(),
                getContext().getReferentialService().getParameters(getModel().getStatusFilter()),
                null,
                DecoratorService.CODE_NAME);

        initBeanFilterableComboBox(
                getUI().getMatricesCombo(),
                getContext().getReferentialService().getMatrices(getModel().getStatusFilter()),
                null);

        initBeanFilterableComboBox(
                getUI().getFractionsCombo(),
                getContext().getReferentialService().getFractions(getModel().getStatusFilter()),
                null);

        initBeanFilterableComboBox(
                getUI().getMethodsCombo(),
                getContext().getReferentialService().getMethods(getModel().getStatusFilter()),
                null);

        initBeanFilterableComboBox(
                getUI().getUnitsCombo(),
                getContext().getReferentialService().getUnits(getModel().getStatusFilter()),
                null,
                DecoratorService.WITH_SYMBOL);

        initBeanFilterableComboBox(getUI().getStatusCombo(),
                getContext().getReferentialService().getStatus(getModel().getStatusFilter()),
                null);

        ReefDbUIs.forceComponentSize(getUI().getNameEditor());
        ReefDbUIs.forceComponentSize(getUI().getLocalCombo());
        ReefDbUIs.forceComponentSize(getUI().getParametersCombo());
        ReefDbUIs.forceComponentSize(getUI().getMatricesCombo());
        ReefDbUIs.forceComponentSize(getUI().getFractionsCombo());
        ReefDbUIs.forceComponentSize(getUI().getMethodsCombo());
        ReefDbUIs.forceComponentSize(getUI().getUnitsCombo());
        ReefDbUIs.forceComponentSize(getUI().getStatusCombo());
    }

    private void reloadComboBox() {

    }

}
