package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.menu.ManageMethodsMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.table.MethodsTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.method.table.MethodsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des Methods au niveau local
 */
public class ManageMethodsLocalUIHandler extends AbstractReefDbTableUIHandler<MethodsTableRowModel, ManageMethodsLocalUIModel, ManageMethodsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageMethodsLocalUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageMethodsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageMethodsLocalUIModel model = new ManageMethodsLocalUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageMethodsLocalUI ui) {
        initUI(ui);

        // Initialisation du tableau
        initTable();

        ManageMethodsMenuUIModel menuUIModel = getUI().getMenuUI().getModel();
        menuUIModel.setLocal(true);

        menuUIModel.addPropertyChangeListener(ManageMethodsMenuUIModel.PROPERTY_RESULTS, evt -> loadMethods((List<MethodDTO>) evt.getNewValue()));
    }

    private void loadMethods(List<MethodDTO> methods) {

        getUI().getManageMethodsLocalTableNouveauBouton().setEnabled(true);

        getModel().setBeans(methods);
        getModel().setModify(false);
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // name
        final TableColumnExt mnemonicCol = addColumn(MethodsTableModel.NAME);
        mnemonicCol.setSortable(true);
        mnemonicCol.setEditable(true);

        // description
        final TableColumnExt descriptionCol = addColumn(MethodsTableModel.DESCRIPTION);
        descriptionCol.setSortable(true);
        descriptionCol.setEditable(true);

        // reference
        final TableColumnExt referenceCol = addColumn(MethodsTableModel.REFERENCE);
        referenceCol.setSortable(true);
        referenceCol.setEditable(true);

        // Comment, creation and update dates
        addCommentColumn(MethodsTableModel.COMMENT);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(MethodsTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(MethodsTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // status
        final TableColumnExt statusCol = addFilterableComboDataColumnToModel(
                MethodsTableModel.STATUS,
                getContext().getReferentialService().getStatus(StatusFilter.LOCAL),
                false);
        statusCol.setSortable(true);
        statusCol.setEditable(true);
        fixDefaultColumnWidth(statusCol);

        // number
//		final TableColumnExt numberCol = addColumn(columnModel, MethodsTableModel.NUMBER);
//		numberCol.setSortable(true);
//		numberCol.setEditable(true);

        // description packaging
        final TableColumnExt descriptionPackagingCol = addColumn(MethodsTableModel.DESCRIPTIONPACKAGING);
        descriptionPackagingCol.setSortable(true);
        descriptionPackagingCol.setEditable(true);

        // description preparation
        final TableColumnExt descriptionPreparationCol = addColumn(MethodsTableModel.DESCRIPTIONPREPARATION);
        descriptionPreparationCol.setSortable(true);
        descriptionPreparationCol.setEditable(true);

        // description
        final TableColumnExt descriptionPreservationCol = addColumn(MethodsTableModel.DESCRIPTIONPRESERVATION);
        descriptionPreservationCol.setSortable(true);
        descriptionPreservationCol.setEditable(true);

        MethodsTableModel tableModel = new MethodsTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.pmfm.methods.local"));

        // Initialisation du tableau
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        descriptionPackagingCol.setVisible(false);
        descriptionPreparationCol.setVisible(false);
        descriptionPreservationCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<MethodsTableRowModel> getTableModel() {
        return (MethodsTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getManageMethodsLocalTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<MethodsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            MethodsTableRowModel rowModel = addedRows.get(0);
            // Set default status
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(rowModel);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, MethodsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(MethodsTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isUnique(row);
    }

    private boolean isUnique(MethodsTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (MethodsTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential", t("reefdb.property.pmfm.method"), row.getName(), t("reefdb.property.referential.local")),
                            MethodsTableRowModel.PROPERTY_NAME);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                List<MethodDTO> existingMethods = getContext().getReferentialService().searchMethods(StatusFilter.ALL, null, null);
                if (CollectionUtils.isNotEmpty(existingMethods)) {
                    for (MethodDTO method : existingMethods) {
                        if (!method.getId().equals(row.getId())
                                && row.getName().equalsIgnoreCase(method.getName())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.pmfm.method"), row.getName(), ReefDbBeans.isLocalStatus(method.getStatus())
                                                    ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    MethodsTableRowModel.PROPERTY_NAME);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();

    }

}
