package fr.ifremer.reefdb.ui.swing.content.extraction;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Save action.
 */
public class SaveAction extends AbstractReefDbSaveAction<ExtractionUIModel, ExtractionUI, ExtractionUIHandler> {

    private Collection<? extends ExtractionDTO> extractionsToSave;

    /**
     * <p>Constructor for SaveAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUIHandler} object.
     */
    public SaveAction(final ExtractionUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }
        // Model is valid
        if (!getModel().isValid()) {

            // Remove error
            displayErrorMessage(t("reefdb.action.save.errors.title"), t("reefdb.action.save.errors.remove"));

            // Stop saving
            return false;
        }

        // Selected extractions
        if (extractionsToSave == null) {
            extractionsToSave = getUI().getExtractionsTable().getModel().getRows();
        }

        return !CollectionUtils.isEmpty(extractionsToSave);

    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Save observations
        getContext().getExtractionService().saveExtractions(extractionsToSave);
    }

    @Override
    protected List<AbstractBeanUIModel> getModelsToModify() {
        return ImmutableList.of(
                getModel().getExtractionsTableUIModel(),
                getModel().getExtractionsFiltersUIModel()
        );
    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getHandler().reloadComboBox();

        sendMessage(t("reefdb.action.save.extractions.success", extractionsToSave.size()));

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected void releaseAction() {
        super.releaseAction();

        extractionsToSave = null;
    }
}
