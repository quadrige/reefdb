package fr.ifremer.reefdb.ui.swing.content.observation.operation.measurement.ungrouped;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour le tableau du haut (Psfm) pour l onglet des mesures des prelevements.
 */
public class OperationMeasurementsUngroupedTableUIHandler extends AbstractReefDbTableUIHandler<OperationMeasurementsUngroupedRowModel, OperationMeasurementsUngroupedTableUIModel, OperationMeasurementsUngroupedTableUI> {

    private static final int ROW_COUNT = 5;

    // editor for analyst column
    private ExtendedComboBoxCellEditor<DepartmentDTO> departmentCellEditor;

    /**
     * <p>Constructor for OperationMeasurementsUngroupedTableUIHandler.</p>
     */
    public OperationMeasurementsUngroupedTableUIHandler() {
        super(OperationMeasurementsUngroupedRowModel.PROPERTY_PMFMS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationMeasurementsUngroupedTableModel getTableModel() {
        return (OperationMeasurementsUngroupedTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final OperationMeasurementsUngroupedTableUI ui) {
        super.beforeInit(ui);
        // create model and register to the JAXX context
        final OperationMeasurementsUngroupedTableUIModel model = new OperationMeasurementsUngroupedTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final OperationMeasurementsUngroupedTableUI ui) {

        // Initialiser l UI
        initUI(ui);
        createDepartmentCellEditor();
        updateDepartmentCellEditor(false);

        initTable();
        SwingUtil.setLayerUI(ui.getTableauHautScrollPane(), ui.getTableBlockLayer());

        // Initialiser listeners
        initListeners();

        getTable().setVisibleRowCount(ROW_COUNT);
    }

    private void createDepartmentCellEditor() {

        departmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        departmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateDepartmentCellEditor(true);
        });

    }

    private void updateDepartmentCellEditor(boolean forceNoFilter) {

        departmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
            && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        departmentCellEditor.getCombo().setData(getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    /**
     * Initialiser les listeners
     */
    private void initListeners() {

        getModel().addPropertyChangeListener(OperationMeasurementsUngroupedTableUIModel.PROPERTY_SURVEY, evt -> loadSamplingOperations());
        getModel().addPropertyChangeListener(OperationMeasurementsUngroupedTableUIModel.PROPERTY_SAMPLING_FILTER, evt -> filterSamplingOperations());

    }

    /**
     * Load prelevements.
     */
    private void loadSamplingOperations() {

        SwingUtilities.invokeLater(() -> {

            // Uninstall save state listener
            uninstallSaveTableStateListener();

            addPmfmColumns(
                getModel().getPmfms(),
                SamplingOperationDTO.PROPERTY_PMFMS,
                DecoratorService.NAME_WITH_UNIT); // insert at the end

            boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfms());

            // tell the table model is editable or not
            getTableModel().setReadOnly(!getModel().getSurvey().isEditable());

            getModel().setBeans(getModel().getSamplingOperations());
            if (notEmpty) {
                for (OperationMeasurementsUngroupedRowModel row : getModel().getRows()) {
                    // set analyst from first non null measurement
                    Optional<MeasurementDTO> measurementFound = row.getMeasurements().stream().filter(measurement -> measurement.getAnalyst() != null).findFirst();
                    if (measurementFound.isPresent()) {
                        row.setAnalyst(measurementFound.get().getAnalyst());
                    } else {
                        row.setAnalyst(getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
                            getModel().getSurvey(),
                            getModel().getPmfms()
                        ));
                    }
                }
            }
            recomputeRowsValidState();

            filterSamplingOperations();

            // restore table from swing session
            restoreTableState();

            // hide analyst if no pmfm
//            forceColumnVisibleAtLastPosition(OperationMeasurementsUngroupedTableModel.ANALYST, notEmpty);
            // Don't force position (Mantis #49537)
            forceColumnVisible(OperationMeasurementsUngroupedTableModel.ANALYST, notEmpty);

            // set columns with errors visible (Mantis #40752)
            ensureColumnsWithErrorAreVisible(getModel().getRows());

            // Fix scrollable height if no column (Mantis #53717)
            fixScrollableHeight();

            // Install save state listener
            installSaveTableStateListener();

            getModel().fireMeasurementsLoaded();
        });
    }

    private void filterSamplingOperations() {

        if (getModel().getSamplingFilter() == null) {
            getTable().setRowFilter(null);
        } else {
            // add filter
            getTable().setRowFilter(new RowFilter<OperationMeasurementsUngroupedTableModel, Integer>() {
                @Override
                public boolean include(Entry<? extends OperationMeasurementsUngroupedTableModel, ? extends Integer> entry) {
                    return getModel().getSamplingFilter() == null
                        || getModel().getSamplingFilter().getName().equals(entry.getValue(getTable().getColumnExt(OperationMeasurementsUngroupedTableModel.NAME).getModelIndex()));
                }
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, OperationMeasurementsUngroupedRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // no need to tell the table is modified if no pmfms (measurement) changes
        if (OperationMeasurementsUngroupedRowModel.PROPERTY_PMFMS.equals(propertyName) && oldValue == newValue) {
            return;
        }

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        row.setDirty(true);

    }

    public void save() {

        Map<Integer, OperationMeasurementsUngroupedRowModel> rowById = new HashMap<>();
        getModel().getRows().forEach(row -> rowById.put(row.getId(), row));

        getModel().getSamplingOperations().forEach(samplingOperation -> {
            OperationMeasurementsUngroupedRowModel row = rowById.get(samplingOperation.getId());
            if (row == null)
                throw new ReefDbTechnicalException("Unable to find the row for sampling operation id=" + samplingOperation.getId());

            // update all measurements
            row.getMeasurements().forEach(measurement -> measurement.setAnalyst(row.getAnalyst()));

            // affect measurements
            samplingOperation.setMeasurements(row.getMeasurements());
        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(OperationMeasurementsUngroupedRowModel row) {
        return super.isRowValid(row) && isMeasurementRowValid(row);
    }

    private boolean isMeasurementRowValid(OperationMeasurementsUngroupedRowModel row) {

        if (row.getAnalyst() == null &&
            row.getMeasurements().stream()
                .filter(measurement -> getModel().getSurvey().getPmfmsUnderMoratorium().stream().noneMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(measurement.getPmfm(), moratoriumPmfm)))
                .anyMatch(measurement -> !ReefDbBeans.isMeasurementEmpty(measurement))
        ) {
            ReefDbBeans.addError(row,
                t("reefdb.validator.error.analyst.required"),
                OperationMeasurementsUngroupedRowModel.PROPERTY_ANALYST);
            return false;
        }
        return true;
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Colonne mnemonique
        final TableColumnExt colMnemonic = addColumn(
            OperationMeasurementsUngroupedTableModel.NAME);
        colMnemonic.setSortable(true);
        colMnemonic.setEditable(false);
        setDefaultColumnMinWidth(colMnemonic);

        // Add analyst column (Mantis #42619)
        TableColumnExt analystColumn = addColumn(
            departmentCellEditor,
            newTableCellRender(OperationMeasurementsUngroupedTableModel.ANALYST),
            OperationMeasurementsUngroupedTableModel.ANALYST
        );
        analystColumn.setMinWidth(100);

        // Modele de la table
        final OperationMeasurementsUngroupedTableModel tableModel = new OperationMeasurementsUngroupedTableModel(getTable().getColumnModel());
        tableModel.setNoneEditableCols();
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable(), true);

        // border
        addEditionPanelBorder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getOperationUngroupedMeasurementTable();
    }
}
