package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.national;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.menu.ApplyFilterUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.menu.TaxonMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.table.TaxonTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.table.TaxonTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.editor.AssociatedTaxonCellEditor;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.AssociatedTaxonCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour la gestion des taxons au niveau national
 */
public class TaxonsNationalUIHandler extends AbstractReefDbTableUIHandler<TaxonTableRowModel, TaxonsNationalUIModel, TaxonsNationalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(TaxonsNationalUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(TaxonsNationalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        TaxonsNationalUIModel model = new TaxonsNationalUIModel();
        ui.setContextValue(model);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(TaxonsNationalUI ui) {
        initUI(ui);

        // force national
        ui.getTaxonsNationalMenuUI().getHandler().forceLocal(false);

        TaxonMenuUIModel nationalMenuModel = ui.getTaxonsNationalMenuUI().getModel();

        // set full search mode
        nationalMenuModel.setFullProperties(true);

        // listen to search results
        nationalMenuModel.addPropertyChangeListener(TaxonMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<TaxonDTO>) evt.getNewValue()));

        // listen to 'apply filter' results
        ui.getTaxonsNationalMenuUI().getApplyFilterUI().getModel().addPropertyChangeListener(ApplyFilterUIModel.PROPERTY_ELEMENTS, evt -> {

            // load only national referential (Mantis #29668)
            List<TaxonDTO> taxonsFromFilter = ReefDbBeans.filterNationalReferential((List<TaxonDTO>) evt.getNewValue());

            // taxonsFromFilter list miss some properties, must query them
            getContext().getReferentialService().fillTaxonsProperties(taxonsFromFilter);

            // then affect list to table
            getModel().setBeans(taxonsFromFilter);
        });

        initTable();

    }

    private void initTable() {

        // name
        TableColumnExt nameCol = addColumn(TaxonTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        // citation
        TableColumnExt citationCol = addFilterableComboDataColumnToModel(TaxonTableModel.CITATION, getContext().getReferentialService().getCitations(), false);
        citationCol.setSortable(true);
        citationCol.setEditable(false);

        // reference taxon
        TableColumnExt refTaxonCol = addFilterableComboDataColumnToModel(TaxonTableModel.REFERENCE_TAXON, getContext().getReferentialService().getTaxons(null), false);
        refTaxonCol.setSortable(true);
        refTaxonCol.setEditable(false);

        // parent taxon
        TableColumnExt parentTaxonCol = addFilterableComboDataColumnToModel(TaxonTableModel.PARENT, getContext().getReferentialService().getTaxons(null), false);
        parentTaxonCol.setSortable(true);
        parentTaxonCol.setEditable(false);

        // level
        TableColumnExt levelCol = addFilterableComboDataColumnToModel(TaxonTableModel.LEVEL, getContext().getReferentialService().getTaxonomicLevels(), false);
        levelCol.setSortable(true);
        levelCol.setEditable(false);

        // comment
        TableColumnExt commentCol = addCommentColumn(TaxonTableModel.COMMENT, false);
        commentCol.setSortable(false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(TaxonTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(TaxonTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // temporary
        TableColumnExt temporaryCol = addBooleanColumnToModel(TaxonTableModel.TEMPORARY, getTable());
        temporaryCol.setSortable(true);
        temporaryCol.setEditable(false);

        // obsolete
        TableColumnExt obsoleteCol = addBooleanColumnToModel(TaxonTableModel.OBSOLETE, getTable());
        obsoleteCol.setSortable(true);
        obsoleteCol.setEditable(false);

        // virtual
        TableColumnExt virtualCol = addBooleanColumnToModel(TaxonTableModel.VIRTUAL, getTable());
        virtualCol.setSortable(true);
        virtualCol.setEditable(false);

        // composites
        TableColumnExt compositesCol = addColumn(new AssociatedTaxonCellEditor(getTable(), getUI(), false), new AssociatedTaxonCellRenderer(), TaxonTableModel.COMPOSITES);
        compositesCol.setSortable(true);

        // taxref
        TableColumnExt taxRefCol = addColumn(TaxonTableModel.TAXREF);
        taxRefCol.setSortable(true);
        taxRefCol.setEditable(false);

        // worms
        TableColumnExt wormsCol = addColumn(TaxonTableModel.WORMS);
        wormsCol.setSortable(true);
        wormsCol.setEditable(false);

        TaxonTableModel tableModel = new TaxonTableModel(getTable().getColumnModel(), false);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.taxons.national"), TaxonTableModel.COMPOSITES);

        // Initialisation du tableau
        initTable(getTable(), true);

        // optional columns are hidden
        temporaryCol.setVisible(false);
        obsoleteCol.setVisible(false);
        virtualCol.setVisible(false);
        compositesCol.setVisible(false);
        taxRefCol.setVisible(false);
        wormsCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<TaxonTableRowModel> getTableModel() {
        return (TaxonTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getTaxonsNationalTable();
    }

}
