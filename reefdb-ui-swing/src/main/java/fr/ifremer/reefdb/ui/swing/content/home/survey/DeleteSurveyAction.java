package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant de supprimer une observation dans le tableau des observations de l ecran d accueil.
 */
public class DeleteSurveyAction extends AbstractReefDbAction<SurveysTableUIModel, SurveysTableUI, SurveysTableUIHandler> {

    /**
     * Loggeur.
     */
    private static final Log LOG = LogFactory.getLog(DeleteSurveyAction.class);

    private Collection<? extends SurveyDTO> surveysToDelete;

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    public DeleteSurveyAction(final SurveysTableUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucune observations de selectionne");
            return false;
        }

        surveysToDelete = getModel().getSelectedRows();

        // Check sampling operations exists
        boolean hasSamplingOperation = false;
        for (SurveyDTO survey : surveysToDelete) {

            if (ReefDbBeans.isSurveyValidated(survey)) {
                displayErrorMessage(t("reefdb.action.delete.survey.titre"), t("reefdb.action.delete.survey.error.isValidated"));
                return false;
            }

            if (!survey.isSamplingOperationsLoaded()) {
                getContext().getObservationService().loadSamplingOperationsFromSurvey(survey, false);
            }
            if (!survey.isSamplingOperationsEmpty()) {
                hasSamplingOperation = true;
            }
        }

        // Demande de confirmation avant la suppression
        if (hasSamplingOperation) {
            return askBeforeDelete(t("reefdb.action.delete.survey.titre"), t("reefdb.action.delete.survey.withSamplingOperation.message"));
        } else {
            return askBeforeDelete(t("reefdb.action.delete.survey.titre"), t("reefdb.action.delete.survey.message"));
        }
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // Selected observation ID
        final List<Integer> idObservationToDelete = ReefDbBeans.collectIds(surveysToDelete);

        // Remove observations service
        getContext().getObservationService().deleteSurveys(idObservationToDelete);

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().getMainUIModel().setSelectedSurvey(null);

        // Remove form model
        getModel().deleteSelectedRows();

        super.postSuccessAction();
    }
}
