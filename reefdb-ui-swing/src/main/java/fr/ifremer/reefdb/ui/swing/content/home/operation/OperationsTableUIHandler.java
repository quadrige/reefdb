package fr.ifremer.reefdb.ui.swing.content.home.operation;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.StringIterator;
import fr.ifremer.quadrige3.ui.swing.action.ActionFactory;
import fr.ifremer.quadrige3.ui.swing.component.coordinate.CoordinateEditor;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.LocalTimeCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUI;
import fr.ifremer.reefdb.ui.swing.content.home.operation.add.AddOperationTableUI;
import fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

import static org.nuiton.i18n.I18n.t;

/**
 * Controleur pour la zone des prelevements de l'ecran d'accueil
 */
public class OperationsTableUIHandler extends AbstractReefDbTableUIHandler<OperationsTableRowModel, OperationsTableUIModel, OperationsTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(OperationsTableUIHandler.class);
    private ThreadPoolExecutor executor;
    private ExtendedComboBoxCellEditor<SamplingEquipmentDTO> samplingEquipmentCellEditor;
    private ExtendedComboBoxCellEditor<DepartmentDTO> samplingDepartmentCellEditor;
    private ExtendedComboBoxCellEditor<DepartmentDTO> analystDepartmentCellEditor;

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationsTableModel getTableModel() {
        return (OperationsTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return getUI().getOperationTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final OperationsTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final OperationsTableUIModel model = new OperationsTableUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(OperationsTableUI ui) {

        // Initialiser l UI
        initUI(ui);

        createSamplingDepartmentCellEditor();
        createAnalystDepartmentCellEditor();
        createSamplingEquipmentCellEditor();

        iniTable();
        SwingUtil.setLayerUI(ui.getHomePrelevementsScrollPane(), ui.getTableBlockLayer());

        // Ajout des listeners
        initListeners();

        // Initilisation du bouton editer
        initActionComboBox(getUI().getPrelevementsPanelEditerCombobox());

        ui.applyDataBinding(OperationsTableUI.BINDING_PRELEVEMENTS_PANEL_DUPLIQUER_BOUTON_ENABLED);
        ui.applyDataBinding(OperationsTableUI.BINDING_PRELEVEMENTS_PANEL_EDITER_COMBOBOX_ENABLED);
        ui.applyDataBinding(OperationsTableUI.BINDING_PRELEVEMENTS_PANEL_SUPPRIMER_BOUTON_ENABLED);

        // button border
        getUI().getNextButton().setBorder(
                BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), ui.getNextButton().getBorder())
        );

        executor = ActionFactory.createSingleThreadExecutor(ActionFactory.ExecutionMode.LATEST);

    }

    @Override
    public void onCloseUI() {
        executor.shutdownNow();
        super.onCloseUI();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{
                OperationsTableRowModel.PROPERTY_ERRORS,
                OperationsTableRowModel.PROPERTY_DIRTY,
                OperationsTableRowModel.PROPERTY_MEASUREMENTS_LOADED
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(final OperationsTableRowModel row) {
        return (super.isRowValid(row) || !row.isEditable()) & isOperationValid(row);
    }

    private boolean isOperationValid(OperationsTableRowModel row) {

        // Name length
        if (StringUtils.length(row.getName()) > 50) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.mnemonic.too.long.50"), OperationsTableRowModel.PROPERTY_NAME);
        }

        // check name duplicates
        if (getModel().getRowCount() >= 2) {
            for (OperationsTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (StringUtils.isNotBlank(row.getName()) && row.getName().equals(otherRow.getName())) {
                    // duplicate found
                    ReefDbBeans.addError(row, t("reefdb.validator.error.samplingOperation.mnemonic.duplicate"), OperationsTableRowModel.PROPERTY_NAME);
                }
            }
        }

        // coordinates
        if (row.getLatitude() == null ^ row.getLongitude() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.coordinate.invalid"), OperationsTableRowModel.PROPERTY_LONGITUDE, OperationsTableRowModel.PROPERTY_LATITUDE);
        }

        // positioning
        if (row.getLatitude() != null && row.getLongitude() != null && row.getPositioning() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.positioning.required"), OperationsTableRowModel.PROPERTY_POSITIONING);
        }

        // analyst
        if (row.getAnalyst() == null &&
            row.getMeasurements().stream()
                .filter(measurement ->
                    CollectionUtils.emptyIfNull(getModel().getSurvey().getPmfmsUnderMoratorium()).stream()
                        .noneMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(measurement.getPmfm(), moratoriumPmfm)))
                .anyMatch(measurement -> !ReefDbBeans.isMeasurementEmpty(measurement))
        ) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.analyst.required"), OperationsTableRowModel.PROPERTY_ANALYST);
        }

        // length
        if (row.getSize() != null && row.getSizeUnit() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.sizeUnit.required"), OperationsTableRowModel.PROPERTY_SIZE_UNIT);
        }

        boolean hasNoError = ReefDbBeans.hasNoBlockingError(row);

        if (!hasNoError) {
            ensureColumnsWithErrorAreVisible(row);
        }

        return hasNoError;
    }

    /**
     * Initialisation des listeners.
     */
    private void initListeners() {

        // Listener sur le tableau
        getModel().addPropertyChangeListener(evt -> {

            if (OperationsTableUIModel.PROPERTY_SINGLE_ROW_SELECTED.equals(evt.getPropertyName())) {
                // Si un seul element a ete selectionne
                final SamplingOperationDTO prelevement = getModel().getSingleSelectedRow();
                if (prelevement != null) {

                    updateSamplingDepartmentCellEditor(false);
                    updateAnalystDepartmentCellEditor(false);
                    updateSamplingEquipmentCellEditor(false);

                    // Sauvegarde de l identifiant du prelevement
                    getContext().setSelectedSamplingOperationId(getModel().getSingleSelectedRow().getId());
                }

            } else if (OperationsTableUIModel.PROPERTY_SURVEY_EDITABLE.equals(evt.getPropertyName())) {

                // Enable table model if survey is editable
                getTableModel().setReadOnly(!getModel().isSurveyEditable());
            }
        });
    }

    /**
     * Charge les prelevements.
     *
     * @param survey a {@link fr.ifremer.reefdb.ui.swing.content.home.survey.SurveysTableRowModel} object.
     */
    public void loadOperations(final SurveysTableRowModel survey) {

        // before affect new survey, need to save previous one
        if (getTable().isEditing()) {
            getTable().getCellEditor().stopCellEditing();
        }

        // save actual table context before loading new survey
        saveTableState();

        getModel().setLoading(true);
        executor.execute(new SamplingOperationsLoader(survey));

    }

    /**
     * Initialisation de la table prelevements.
     */
    private void iniTable() {

        // Colonne mnemonique
        final TableColumnExt colName = addColumn(OperationsTableModel.NAME);
        colName.setSortable(true);
        colName.setMinWidth(100);

        // Colonne engin
        final TableColumnExt colSamplingEquipment = addColumn(
                samplingEquipmentCellEditor,
                newTableCellRender(OperationsTableModel.SAMPLING_EQUIPMENT),
                OperationsTableModel.SAMPLING_EQUIPMENT
        );
        colSamplingEquipment.setSortable(true);
        colSamplingEquipment.setMinWidth(100);

        // Colonne Heure
        final TableColumnExt colTime = addColumn(
                new LocalTimeCellEditor(),
                newTableCellRender(Integer.class, DecoratorService.TIME_IN_HOURS_MINUTES),
                OperationsTableModel.TIME);
        colTime.setSortable(true);
        colTime.setMinWidth(50);

        // Colonne commentaire
        addCommentColumn(OperationsTableModel.COMMENT);

        //------ Colonnes optionnelles

        // Colonne Taille
        final TableColumnExt colSize = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL7_PATTERN),
                newNumberCellRenderer(7),
                OperationsTableModel.SIZE);
        colSize.setSortable(true);
        colSize.setMinWidth(50);

        // Colonne Unite taille
        final TableColumnExt colSizeUnit = addExtendedComboDataColumnToModel(
                OperationsTableModel.SIZE_UNIT,
                getContext().getReferentialService().getUnits(StatusFilter.ACTIVE),
                false);
        colSizeUnit.setSortable(true);
        colSizeUnit.setMinWidth(100);

        // Colonne service préleveur
        final TableColumnExt colSamplingDepartment = addColumn(
                samplingDepartmentCellEditor,
                newTableCellRender(OperationsTableModel.SAMPLING_DEPARTMENT),
                OperationsTableModel.SAMPLING_DEPARTMENT
        );
        colSamplingDepartment.setSortable(true);
        colSamplingDepartment.setMinWidth(100);

        // Colonne immersion
        final TableColumnExt colDepth = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN),
                newNumberCellRenderer(2),
                OperationsTableModel.DEPTH);
        colDepth.setSortable(true);
        colDepth.setMinWidth(100);

        // Colonne immersion min
        final TableColumnExt colMinDepth = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN),
                newNumberCellRenderer(2),
                OperationsTableModel.MIN_DEPTH);
        colMinDepth.setSortable(true);
        colMinDepth.setMinWidth(100);

        // Colonne immersion
        final TableColumnExt colMaxDepth = addColumn(
                newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN),
                newNumberCellRenderer(2),
                OperationsTableModel.MAX_DEPTH);
        colMaxDepth.setSortable(true);
        colMaxDepth.setMinWidth(100);

        // Colonne Coordonnées réelles
        final TableColumnExt colLatitude = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MIN,
                OperationsTableModel.LATITUDE);
        colLatitude.setSortable(true);
        colLatitude.setMinWidth(100);

        final TableColumnExt colLongitude = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MIN,
                OperationsTableModel.LONGITUDE);
        colLongitude.setSortable(true);
        colLongitude.setMinWidth(100);

        // Colonne informations de positionnement
        final TableColumnExt colPositioning = addExtendedComboDataColumnToModel(
                OperationsTableModel.POSITIONING,
                getContext().getReferentialService().getPositioningSystems(),
                false);
        colPositioning.setSortable(true);
        colPositioning.setMinWidth(100);

        final TableColumnExt colPositioningPrecision = addColumn(
                OperationsTableModel.POSITIONING_PRECISION);
        colPositioningPrecision.setSortable(true);
        colPositioningPrecision.setMinWidth(100);
        colPositioningPrecision.setEditable(false);

        // Colonne service analyste
        final TableColumnExt colAnalystDepartment = addColumn(
            analystDepartmentCellEditor,
            newTableCellRender(OperationsTableModel.ANALYST),
            OperationsTableModel.ANALYST
        );
        colAnalystDepartment.setSortable(true);
        colAnalystDepartment.setMinWidth(100);

        // Modele de la table
        final OperationsTableModel tableModel = new OperationsTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Initialisation de la table
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        colSize.setVisible(false);
        colSizeUnit.setVisible(false);
        colSamplingDepartment.setVisible(false);
//        colonneNiveau.setVisible(false);
        colDepth.setVisible(false);
        colMinDepth.setVisible(false);
        colMaxDepth.setVisible(false);
        colLatitude.setVisible(false);
        colLongitude.setVisible(false);
        colPositioning.setVisible(false);
        colPositioningPrecision.setVisible(false);

        // Tri par defaut
        getTable().setSortOrder(OperationsTableModel.NAME, SortOrder.ASCENDING);

        // border
        addEditionPanelBorder();
    }

    private void createSamplingEquipmentCellEditor() {

        samplingEquipmentCellEditor = newExtendedComboBoxCellEditor(null, SamplingEquipmentDTO.class, false);

        samplingEquipmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateSamplingEquipmentCellEditor(true);
            getTable().requestFocus();
        });

    }

    private void updateSamplingEquipmentCellEditor(boolean forceNoFilter) {

        samplingEquipmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.SAMPLING_EQUIPMENT));

        samplingEquipmentCellEditor.getCombo().setData(
                getContext().getObservationService().getAvailableSamplingEquipments(forceNoFilter));
    }

    private void createSamplingDepartmentCellEditor() {

        samplingDepartmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        samplingDepartmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter department
            updateSamplingDepartmentCellEditor(true);
            getTable().requestFocus();
        });

    }

    private void updateSamplingDepartmentCellEditor(boolean forceNoFilter) {

        samplingDepartmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        samplingDepartmentCellEditor.getCombo().setData(
                getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    private void createAnalystDepartmentCellEditor() {

        analystDepartmentCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        analystDepartmentCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter department
            updateAnalystDepartmentCellEditor(true);
            getTable().requestFocus();
        });

    }

    private void updateAnalystDepartmentCellEditor(boolean forceNoFilter) {

        analystDepartmentCellEditor.getCombo().setActionEnabled(!forceNoFilter
                && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        analystDepartmentCellEditor.getCombo().setData(
                getContext().getObservationService().getAvailableDepartments(forceNoFilter));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, OperationsTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // Set row dirty first
        row.setDirty(true);

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        if (OperationsTableRowModel.PROPERTY_NAME.equals(propertyName)) {
            recomputeRowsValidState();

        } else if (OperationsTableRowModel.PROPERTY_PMFMS.equals(propertyName)) {
            // force measurements loaded to true to tell the service to save measurements
            // in case of duplicated sampling operation, this flag is set to false, so the measurements was not saved (Mantis #0027276)
            row.setMeasurementsLoaded(true);

            // Set also default analyst (Mantis #50218)
            if (row.getAnalyst() == null) {
                row.setAnalyst(
                    getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
                        getModel().getSurvey(),
                        getModel().getPmfms()
                    )
                );
            }
        }

        saveToSurvey();
    }

    /**
     * <p>addSamplingOperations.</p>
     */
    public void addSamplingOperations() {

        AddOperationTableUI dialog = new AddOperationTableUI(getContext());
        dialog.getHandler().load(this);
        openDialog(dialog, new Dimension(1000, 232));

        // Add new rows
        List<OperationsTableRowModel> newOperations = dialog.getModel().getRows();
        if (dialog.getModel().isValid() && CollectionUtils.isNotEmpty(newOperations)) {

            // Use label auto generation, now with specified label prefix (Mantis #47245)
            String labelPrefix = dialog.getModel().getOperationNamePrefix();
            StringIterator labelIterator = StringIterator.newStringIteratorByProperty(getModel().getRows(), SamplingOperationDTO.PROPERTY_NAME, labelPrefix);
            newOperations.forEach(newOperation -> newOperation.setName(labelIterator.next()));

            getModel().addRows(newOperations);
            getModel().setModify(true);

            recomputeRowsValidState();
            saveToSurvey();

            // force focus on first empty name (Mantis #40749)
            for (OperationsTableRowModel row : getModel().getRows()) {
                if (StringUtils.isBlank(row.getName())) {
                    getTable().setTerminateEditOnFocusLost(false); // Trick to prevent focus owner change (even if setFocusOnCell is invoked later)
                    setFocusOnCell(row);
                    break;
                }
            }
        }

    }

    /**
     * <p>removeSamplingOperations.</p>
     */
    public void removeSamplingOperations() {

        if (getModel().getSelectedRows().isEmpty()) {
            LOG.warn("Aucun prelevement de selectionne");
            return;
        }

        // Demande de confirmation avant la suppression
        boolean canContinue = askBeforeDelete(t("reefdb.action.delete.samplingOperation.title"), t("reefdb.action.delete.samplingOperation.message"));

        if (canContinue) {

            // check measurement
            boolean hasMeasurement = false;
            for (OperationsTableRowModel row : getModel().getSelectedRows()) {

                if (!row.isMeasurementsEmpty()) {
                    for (MeasurementDTO measurement : row.getMeasurements()) {
                        if (measurement.getQualitativeValue() != null || measurement.getNumericalValue() != null) {
                            hasMeasurement = true;
                            break;
                        }
                    }
                }
            }

            if (hasMeasurement) {
                canContinue = askBeforeDelete(t("reefdb.action.delete.samplingOperation.measurement.title"), t("reefdb.action.delete.samplingOperation.measurement.message"));
            }
        }

        if (canContinue) {

            // Suppression des lignes
            getModel().deleteSelectedRows();

            // Recompute valid state (Mantis #33741)
            recomputeRowsValidState();

            forceRevalidateModel();

            saveToSurvey();
        }
    }

    /**
     * <p>saveToSurvey.</p>
     */
    private void saveToSurvey() {

        // save analyst in every measurements (Mantis #42782)
        for (OperationsTableRowModel rowModel : getModel().getRows()) {
            if (!rowModel.isMeasurementsEmpty()) {
                rowModel.getMeasurements().forEach(measurement -> measurement.setAnalyst(rowModel.getAnalyst()));
            }
        }

        // save sampling operations in parent model
        getModel().getSurvey().setSamplingOperations(getModel().getBeans());
        getModel().getSurvey().setSamplingOperationsLoaded(true);

        // force model modify
        getModel().getMainUIModel().getSurveysTableUIModel().setModify(true);
        getModel().setModify(true);
        getModel().getMainUIModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<OperationsTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            OperationsTableRowModel rowModel = addedRows.get(0);

            rowModel.setDirty(true);

            // Add default name
            if (StringUtils.isBlank(rowModel.getName())) {
                rowModel.setName(StringIterator.newStringIteratorByProperty(getModel().getRows(), SamplingOperationDTO.PROPERTY_NAME).next());
            }

            // Add default service from observation, if any
            if (rowModel.getSamplingDepartment() == null) {
                rowModel.setSamplingDepartment(getContext().getProgramStrategyService().getDepartmentOfAppliedStrategyBySurvey(getModel().getSurvey()));
            }

            // create all empty measurements
            rowModel.addAllPmfms(getModel().getPmfms());
            for (PmfmDTO pmfm : getModel().getPmfms()) {
                MeasurementDTO measurement = ReefDbBeanFactory.newMeasurementDTO();
                measurement.setPmfm(pmfm);
                rowModel.addMeasurements(measurement);
            }

            // The analyst from strategy (Mantis #42619)
            if (rowModel.getAnalyst() == null) {
                rowModel.setAnalyst(
                        getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
                                getModel().getSurvey(),
                                rowModel.getPmfms()
                        ));
            }

            getModel().setModify(true);
            setFocusOnCell(rowModel);
        }
    }

    /**
     * <p>onNext.</p>
     */
    public void onNext() {

        HomeUI homeUI = getUI().getParentContainer(HomeUI.class);

        getContext().getActionEngine().runAction(homeUI.getSurveysTable().getObservationEditerGeneralBouton());
    }

    public boolean isLoading() {
        return getModel().isLoading() || executor.getActiveCount() > 0;
    }

    private class SamplingOperationsLoader extends SwingWorker<Object, Object> {

        List<PmfmDTO> pmfms;
        final SurveysTableRowModel survey;

        private SamplingOperationsLoader(SurveysTableRowModel survey) {
            this.survey = survey;
        }

        @Override
        protected Object doInBackground() {

            if (survey != null) {

                // load sampling operations, check loaded first to avoid unnecessary call to service
                if (!survey.isSamplingOperationsLoaded()) {
                    getContext().getObservationService().loadSamplingOperationsFromSurvey(survey, false /* don't load individual measurements */);
                }

                // Load pmfms under moratorium
                if (survey.getPmfmsUnderMoratorium() == null) {
                    survey.setPmfmsUnderMoratorium(getContext().getObservationService().getPmfmsUnderMoratorium(survey));
                }

                // Load pmfms from strategies
                pmfms = getContext().getProgramStrategyService().getPmfmsForSamplingOperationBySurvey(survey);

                // Load other Pmfms in sampling operations
                if (!survey.isSamplingOperationsEmpty()) {
                    // populate pmfms from strategy to sampling operation and vice versa
                    for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
                        ReefDbBeans.fillListsEachOther(samplingOperation.getPmfms(), pmfms);
                    }
                }

                // Filter out pmfms under moratorium
                if (CollectionUtils.isNotEmpty(survey.getPmfmsUnderMoratorium())) {
                    pmfms.removeIf(pmfm -> survey.getPmfmsUnderMoratorium().stream().anyMatch(moratoriumPmfm -> ReefDbBeans.isPmfmEquals(pmfm, moratoriumPmfm)));
                }
            }

            return null;
        }

        @Override
        protected void done() {
            try {
                if (isCancelled()) {
                    return;
                }
                try {
                    get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new ReefDbTechnicalException(e.getMessage(), e);
                }

                // Set model properties
                getModel().setPmfms(pmfms);

                // Initialiser la table des prelevements
                addPmfmColumns(
                        getModel().getPmfms(),
                        SamplingOperationDTO.PROPERTY_PMFMS,
                        DecoratorService.NAME_WITH_UNIT,
                        OperationsTableModel.TIME);

                boolean notEmpty = CollectionUtils.isNotEmpty(getModel().getPmfmColumns());

                // affect empty survey to prevent artifact (Mantis #60929)
                getModel().setSurvey(null);

                if (survey == null) {

                    // reset all
                    getModel().setBeans(null);

                } else if (survey.isSamplingOperationsEmpty()) {

                    // reset all
                    getModel().setBeans(null);
                    // affect new selected survey directly
                    getModel().setSurvey(survey);

                } else {

                    // Set model read only state before building rows (Mantis #61556)
                    getTableModel().setReadOnly(!survey.isEditable());

                    // Chargement du tableau avec les prelevements
                    getModel().setBeans(survey.getSamplingOperations());

                    if (notEmpty) {
                        for (OperationsTableRowModel row : getModel().getRows()) {
                            // set analyst from first non null measurement
                            Optional<MeasurementDTO> measurementFound = row.getMeasurements().stream().filter(measurement -> measurement.getAnalyst() != null).findFirst();
                            measurementFound.ifPresent(measurementDTO -> row.setAnalyst(measurementDTO.getAnalyst()));

                        }
                    }

                    // affect new selected survey before recompute valid state (Mantis #64133)
                    getModel().setSurvey(survey);
                    recomputeRowsValidState();

                }

                forceRevalidateModel();

                // restore table state from swing session
                restoreTableState();

                // hide analyst column if no pmfm (Mantis #42619)
//                forceColumnVisibleAtLastPosition(OperationsTableModel.ANALYST, notEmpty);
                // Don't force position (Mantis #49537)
                forceColumnVisible(OperationsTableModel.ANALYST, notEmpty);

                // set columns with errors visible
                ensureColumnsWithErrorAreVisible(getModel().getRows());

            } finally {
                getModel().setLoading(false);
            }
        }
    }
}
