package fr.ifremer.reefdb.ui.swing.content.synchro.changes;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model for synchro changes
 */
public class SynchroChangesUIModel extends AbstractReefDbTableUIModel<SynchroRowDTO, SynchroChangesRowModel, SynchroChangesUIModel> {

    /** Constant <code>PROPERTY_CHANGES="changes"</code> */
    public final static String PROPERTY_CHANGES = "changes";

    private SynchroChangesDTO changes;

    private boolean changesValidated;

    /**
     * <p>Setter for the field <code>changes</code>.</p>
     *
     * @param changes a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     */
    public void setChanges(SynchroChangesDTO changes) {
        firePropertyChanged(PROPERTY_CHANGES,
                this.changes,
                this.changes = changes);
    }

    /**
     * <p>Getter for the field <code>changes</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     */
    public SynchroChangesDTO getChanges() {
        return changes;
    }

    /**
     * <p>Setter for the field <code>changesValidated</code>.</p>
     *
     * @param changesValidated a boolean.
     */
    public void setChangesValidated(boolean changesValidated) {
        this.changesValidated = changesValidated;
    }

    /**
     * <p>isChangesValidated.</p>
     *
     * @return a boolean.
     */
    public boolean isChangesValidated() {
        return changesValidated;
    }
}
