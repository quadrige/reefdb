package fr.ifremer.reefdb.ui.swing.content.extraction;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.content.extraction.filters.ExtractionsFiltersUIModel;
import fr.ifremer.reefdb.ui.swing.content.extraction.list.ExtractionsRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.util.List;

/**
 * Extraction handler.
 */
public class ExtractionUIHandler extends AbstractReefDbUIHandler<ExtractionUIModel, ExtractionUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ExtractionUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ExtractionUI ui) {
        super.beforeInit(ui);

        // Ajout du model pour la page d acceuil
        ui.setContextValue(new ExtractionUIModel());
        ui.setContextValue(SwingUtil.createActionIcon("export"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ExtractionUI ui) {
        initUI(ui);
        ui.getSelectExtractionLabel().setForeground(getConfig().getColorThematicLabel());
        ui.getProgramLabel().setForeground(getConfig().getColorThematicLabel());

        // Save models
        getModel().setExtractionsTableUIModel(getUI().getExtractionsTable().getModel());
        getModel().setExtractionsFiltersUIModel(getUI().getFiltersTable().getModel());

        // Initialisation des combobox
        initComboBox();

        // Initialisation des listeners
        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

        // init image
        SwingUtil.setComponentWidth(getUI().getLeftImage(), ui.getExtractionMenu().getPreferredSize().width * 9 / 10);
        getUI().getLeftImage().setScaled(true);

    }

    /**
     * Initialisation des composants de gauche
     */
    private void initComboBox() {

        // Init context combo
        initBeanFilterableComboBox(
                getUI().getContextComboBox(),
                getContext().getContextService().getAllContexts(),
                getContext().getSelectedContext());

        // Init extractions combo
        initBeanFilterableComboBox(
                getUI().getSelectExtractionCombo(),
                getContext().getExtractionService().getAllExtractions(),
                null);

        // Init programs combo
        initBeanFilterableComboBox(
                getUI().getSelectProgramCombo(),
                getContext().getObservationService().getAvailablePrograms(null, null, null, false, false),
                null);

        // Modification des largeurs des combobox
        ReefDbUIs.forceComponentSize(getUI().getContextComboBox());
        ReefDbUIs.forceComponentSize(getUI().getSelectExtractionCombo());
        ReefDbUIs.forceComponentSize(getUI().getSelectProgramCombo());

        getUI().getSelectExtractionCombo().getComboBoxModel().addWillChangeSelectedItemListener(event -> {
            if (getModel().isLoading()) return;
            if (event.getNextSelectedItem() != null) SwingUtilities.invokeLater(() -> getUI().getSearchButton().getAction().actionPerformed(null));
        });
    }

    /**
     * <p>reloadComboBox.</p>
     */
    public void reloadComboBox() {
        getUI().getSelectExtractionCombo().setData(getContext().getExtractionService().getAllExtractions());
    }

    /**
     * Initialiser les listeners.
     */
    private void initListeners() {

        // Listen modify property and set dirty to the selected program
        listenModelModify(getModel().getExtractionsTableUIModel());
        getModel().getExtractionsFiltersUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (!getModel().getExtractionsFiltersUIModel().isLoading() && modify != null) {
                getModel().setModify(modify);
                if (modify && getModel().getExtractionsTableUIModel().getSingleSelectedRow() != null) {
                    getModel().getExtractionsTableUIModel().getSingleSelectedRow().setDirty(true);
                }
            }
        });

        // Listen when a extraction is selected by table
        getModel().addPropertyChangeListener(ExtractionUIModel.PROPERTY_SELECTED_EXTRACTION, evt -> getUI().getFiltersTable().getHandler().loadFilters());

        getModel().getExtractionsTableUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());
        getModel().getExtractionsFiltersUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> {

            Boolean valid = (Boolean) evt.getNewValue();
            ExtractionsRowModel extractionRow = getModel().getExtractionsTableUIModel().getSingleSelectedRow();
            if (extractionRow != null) {

                // set filters valid state on current selected extraction
                extractionRow.setFiltersValid(valid);
                getUI().getExtractionsTable().getHandler().recomputeRowValidState(extractionRow);
                getValidator().doValidate();
            }
        });

        // listener on program selection
        getModel().addPropertyChangeListener(ExtractionUIModel.PROPERTY_PROGRAM, evt -> {
            // TODO ?
        });

        // listener on extraction selection
        getModel().addPropertyChangeListener(ExtractionUIModel.PROPERTY_EXTRACTION, evt -> {
            // TODO ?
        });

        // propagate loading property
        getModel().getExtractionsFiltersUIModel().addPropertyChangeListener(ExtractionsFiltersUIModel.PROPERTY_LOADING, evt -> {

            if (evt.getNewValue() instanceof Boolean) {
                getModel().getExtractionsTableUIModel().setFiltersLoading((Boolean) evt.getNewValue());
            }
        });

        // Change context value
        getModel().addPropertyChangeListener(ExtractionUIModel.PROPERTY_CONTEXT, evt -> getContext().setSelectedContext(getModel().getContext()));

    }

    /**
     * Methode permettant le changement des extractions.
     *
     * @param extractions La liste des observations
     */
    public void loadExtractions(final List<ExtractionDTO> extractions) {

        getModel().getExtractionsTableUIModel().setBeans(extractions);

        // auto select unique row
        if (getModel().getExtractionsTableUIModel().getRowCount() == 1) {
            ExtractionsRowModel rowModel = getModel().getExtractionsTableUIModel().getRows().get(0);
            SwingUtilities.invokeLater(() -> {
                getUI().getExtractionsTable().getHandler().selectRow(rowModel);
                getModel().getExtractionsTableUIModel().setSingleSelectedRow(rowModel);
            });
        }
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<ExtractionUIModel> getValidator() {
        return getUI().getValidator();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }
}
