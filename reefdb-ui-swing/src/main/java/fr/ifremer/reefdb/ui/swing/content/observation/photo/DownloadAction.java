package fr.ifremer.reefdb.ui.swing.content.observation.photo;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.service.http.HttpNotFoundException;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Download action.
 */
public class DownloadAction extends AbstractReefDbAction<PhotosTabUIModel, PhotosTabUI, PhotosTabUIHandler> {

    private static final Log LOG = LogFactory.getLog(DownloadAction.class);
    private boolean allDownloaded;
    private Collection<PhotosTableRowModel> toDownload;

    /**
     * Constructor.
     *
     * @param handler Controller
     */
    public DownloadAction(PhotosTabUIHandler handler) {
        super(handler, false);
        setActionDescription(t("reefdb.photo.download.tip"));
    }

    public Collection<PhotosTableRowModel> getToDownload() {
        return Optional.ofNullable(toDownload).orElse(getModel().getSelectedRows());
    }

    public void setToDownload(PhotosTableRowModel toDownload) {
        this.toDownload = ImmutableList.of(toDownload);
    }

    public void setToDownload(Collection<PhotosTableRowModel> toDownload) {
        this.toDownload = toDownload;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        return super.prepareAction() && !getToDownload().isEmpty() && getToDownload().stream().anyMatch(PhotosTableRowModel::isFileDownloadable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        allDownloaded = true;
        createProgressionUIModel();
        List<PhotosTableRowModel> downloadablePhotos = getToDownload().stream().filter(PhotosTableRowModel::isFileDownloadable).collect(Collectors.toList());
        int nbPhotos = downloadablePhotos.size();
        int nPhoto = 0;

        for (PhotosTableRowModel photo: downloadablePhotos) {

            try {
                getProgressionUIModel().setMessage(String.format("%s / %s", ++nPhoto, nbPhotos));
                boolean downloaded = ReefDbServiceLocator.instance().getSynchroRestClientService().downloadPhoto(
                        getContext().getAuthenticationInfo(),
                        photo.getRemoteId(),
                        photo.getFullPath(),
                        getProgressionUIModel()
                );
                if (!downloaded) {
                    LOG.warn(String.format("the photo %s was not download, but without exception", photo.getName()));
                }
                allDownloaded &= downloaded;

            } catch (HttpNotFoundException e) {
                throw new ReefDbBusinessException(t("reefdb.photo.download.notFound", photo.getName()));
            }

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        if (!allDownloaded)
            getContext().getDialogHelper().showErrorDialog(t("reefdb.photo.download.error"));

        getHandler().updatePhotoViewerContent(false);
        getUI().invalidate();
        getUI().repaint();
        // update buttons states
        getUI().processDataBinding(PhotosTabUI.BINDING_EXPORT_PHOTO_BUTTON_ENABLED);
        getUI().processDataBinding(PhotosTabUI.BINDING_DOWNLOAD_PHOTO_BUTTON_ENABLED);

        super.postSuccessAction();
    }

}
