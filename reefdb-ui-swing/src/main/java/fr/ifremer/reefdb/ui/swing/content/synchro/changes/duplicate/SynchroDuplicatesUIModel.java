package fr.ifremer.reefdb.ui.swing.content.synchro.changes.duplicate;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.reefdb.ui.swing.content.synchro.changes.SynchroChangesRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model for synchro changes
 */
public class SynchroDuplicatesUIModel extends AbstractReefDbTableUIModel<SynchroRowDTO, SynchroChangesRowModel, SynchroDuplicatesUIModel> {

    /** Constant <code>PROPERTY_CHANGES="changes"</code> */
    public final static String PROPERTY_CHANGES = "changes";

    private SynchroChangesDTO changes;

    private SynchroTableDTO tableChange;

    private String tableNameFilter;

    private boolean changesValidated;

    /**
     * <p>Getter for the field <code>changes</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     */
    public SynchroChangesDTO getChanges() {
        return changes;
    }

    /**
     * <p>Setter for the field <code>changes</code>.</p>
     *
     * @param changes a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     */
    public void setChanges(SynchroChangesDTO changes) {
        firePropertyChange(PROPERTY_CHANGES, this.changes, this.changes = changes);
    }

    /**
     * <p>isChangesValidated.</p>
     *
     * @return a boolean.
     */
    public boolean isChangesValidated() {
        return changesValidated;
    }

    /**
     * <p>Setter for the field <code>changesValidated</code>.</p>
     *
     * @param changesValidated a boolean.
     */
    public void setChangesValidated(boolean changesValidated) {
        this.changesValidated = changesValidated;
    }

    /**
     * <p>Getter for the field <code>tableChange</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO} object.
     */
    public SynchroTableDTO getTableChange() {
        return tableChange;
    }

    /**
     * <p>Setter for the field <code>tableChange</code>.</p>
     *
     * @param tableChange a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO} object.
     */
    public void setTableChange(SynchroTableDTO tableChange) {
        this.tableChange = tableChange;
    }

    /**
     * <p>Getter for the field <code>tableNameFilter</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTableNameFilter() {
        return tableNameFilter;
    }

    /**
     * <p>Setter for the field <code>tableNameFilter</code>.</p>
     *
     * @param tableNameFilter a {@link java.lang.String} object.
     */
    public void setTableNameFilter(String tableNameFilter) {
        this.tableNameFilter = tableNameFilter;
    }
}
