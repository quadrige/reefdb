package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.menu.DefaultReferentialMenuUIModel;

/**
 * Modele du menu pour la gestion des Parameters au niveau local
 */
public class ManageParametersMenuUIModel extends DefaultReferentialMenuUIModel {

    private ParameterDTO parameter;
    private ParameterGroupDTO parameterGroup;

    /**
     * <p>Getter for the field <code>parameter</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO} object.
     */
    public ParameterDTO getParameter() {
        return parameter;
    }

    /**
     * <p>Setter for the field <code>parameter</code>.</p>
     *
     * @param parameter a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO} object.
     */
    public void setParameter(ParameterDTO parameter) {
        this.parameter = parameter;
    }

    /**
     * <p>getParameterCode.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getParameterCode() {
        return getParameter() == null ? null : getParameter().getCode();
    }

    /**
     * <p>Getter for the field <code>parameterGroup</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO} object.
     */
    public ParameterGroupDTO getParameterGroup() {
        return parameterGroup;
    }

    /**
     * <p>Setter for the field <code>parameterGroup</code>.</p>
     *
     * @param parameterGroup a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO} object.
     */
    public void setParameterGroup(ParameterGroupDTO parameterGroup) {
        this.parameterGroup = parameterGroup;
    }

}
