package fr.ifremer.reefdb.ui.swing.content.manage.referential.location.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.location.ManageLocationUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.location.local.replace.ReplaceLocationUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.location.local.replace.ReplaceLocationUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceLocationAction extends AbstractReefDbAction<LocationLocalUIModel, LocationLocalUI, LocationLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceLocationAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.location.local.LocationLocalUIHandler} object.
     */
    public OpenReplaceLocationAction(LocationLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<LocationDTO, ReplaceLocationUIModel, ReplaceLocationUI> openAction =
                new AbstractOpenReplaceUIAction<LocationDTO, ReplaceLocationUIModel, ReplaceLocationUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.location");
                    }

                    @Override
                    protected ReplaceLocationUIModel createNewModel() {
                        return new ReplaceLocationUIModel();
                    }

                    @Override
                    protected ReplaceLocationUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceLocationUI(ctx);
                    }

                    @Override
                    protected List<LocationDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getLocations(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected LocationDTO getSelectedSource() {
                        List<LocationDTO> selectedBeans = OpenReplaceLocationAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected LocationDTO getSelectedTarget() {
                        ManageLocationUI ui = OpenReplaceLocationAction.this.getUI().getParentContainer(ManageLocationUI.class);
                        List<LocationDTO> selectedBeans = ui.getManageLocationNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
