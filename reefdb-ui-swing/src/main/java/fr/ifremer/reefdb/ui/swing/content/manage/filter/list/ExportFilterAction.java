package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Export action.
 */
public class ExportFilterAction extends AbstractReefDbAction<FilterListUIModel, FilterListUI, FilterListUIHandler> {

    private static final String EXPORT_FILE_FORMAT = "reefdb-filters-%s-%s.dat";
    private FilterTypeValues contextFilter;
    private List<FilterDTO> filtersToExport;
    private File targetDirectory;
    private File exportFile;

    /**
     * Constructor.
     *
     * @param handler Controlleur
     */
    public ExportFilterAction(FilterListUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        filtersToExport = null;
        targetDirectory = null;
        exportFile = null;
        contextFilter = null;

        if (super.prepareAction()) {
            // Choose directory
            targetDirectory = chooseDirectory(
                    t("reefdb.action.filter.export.title"),
                    t("reefdb.action.common.chooseDirectory.buttonLabel"));

            filtersToExport = Lists.newArrayList();
            for (FilterListRowModel row : getModel().getSelectedRows()) {
                filtersToExport.add(row.toBean());
                if (contextFilter == null) {
                    contextFilter = FilterTypeValues.getFilterType(row.getFilterTypeId());
                }
            }
        }
        return (targetDirectory != null && CollectionUtils.isNotEmpty(filtersToExport));
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        // create new filename
        String fileName = String.format(EXPORT_FILE_FORMAT,
                contextFilter.toString().toLowerCase(),
                Times.getFileSuffix());
        exportFile = new File(targetDirectory, fileName);

        // Export filters
        getContext().getContextService().exportFilter(filtersToExport, exportFile);

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {
        // display success message
        displayInfoMessage(t("reefdb.common.success"), t("reefdb.action.filter.export.success", exportFile.getAbsolutePath()));
    }

}
