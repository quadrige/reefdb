package fr.ifremer.reefdb.ui.swing.content.manage.filter.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * Table Model.
 */
public class FilterListTableModel extends AbstractReefDbTableModel<FilterListRowModel> {

	/**
	 * Name column identifier.
	 */
    public static final ReefDbColumnIdentifier<FilterListRowModel> NAME = ReefDbColumnIdentifier.newId(
    		FilterListRowModel.PROPERTY_NAME,
            n("reefdb.filter.filterList.name"),
            n("reefdb.filter.filterList.name.tip"),
            String.class,
            true);

	/**
	 * <p>Constructor for FilterListTableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 */
	public FilterListTableModel(final TableColumnModelExt columnModel) {
		super(columnModel, true, false);
	}

	/** {@inheritDoc} */
	@Override
	public FilterListRowModel createNewRow() {
		return new FilterListRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<FilterListRowModel> getFirstColumnEditing() {
		return NAME;
	}
}
