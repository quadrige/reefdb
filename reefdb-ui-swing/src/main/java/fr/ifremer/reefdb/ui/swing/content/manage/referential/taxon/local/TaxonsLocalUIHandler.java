package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.taxon.TaxonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.menu.TaxonMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.table.TaxonTableModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.table.TaxonTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import java.util.List;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l administration des taxons au niveau local
 */
public class TaxonsLocalUIHandler extends AbstractReefDbTableUIHandler<TaxonTableRowModel, TaxonsLocalUIModel, TaxonsLocalUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(TaxonsLocalUIHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(final TaxonsLocalUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final TaxonsLocalUIModel model = new TaxonsLocalUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterInit(final TaxonsLocalUI ui) {
        initUI(ui);

        // hide context filter panel
        ui.getTaxonsLocalMenuUI().getHandler().enableContextFilter(false);

        // force local
        ui.getTaxonsLocalMenuUI().getHandler().forceLocal(true);

        TaxonMenuUIModel localMenuModel = ui.getTaxonsLocalMenuUI().getModel();

        // set full search mode
        localMenuModel.setFullProperties(true);

        // listen to search results
        localMenuModel.addPropertyChangeListener(TaxonMenuUIModel.PROPERTY_RESULTS, evt -> getModel().setBeans((List<TaxonDTO>) evt.getNewValue()));

        // Initialisation du tableau
        initTable();
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // name
        TableColumnExt nameCol = addColumn(TaxonTableModel.NAME);
        nameCol.setSortable(true);

        // parent taxon
        List<TaxonDTO> taxons = getContext().getReferentialService().getTaxons(null);
        getContext().getReferentialService().fillReferentTaxons(taxons);
        addColumn(
                newFilterableComboBoxCellEditor(taxons, TaxonDTO.class, DecoratorService.WITH_CITATION_AND_REFERENT, false),
                newTableCellRender(TaxonDTO.class, null, DecoratorService.WITH_CITATION_AND_REFERENT),
                TaxonTableModel.PARENT);

        // level
        TableColumnExt levelCol = addFilterableComboDataColumnToModel(
                TaxonTableModel.LEVEL,
                getContext().getReferentialService().getTaxonomicLevels(),
                false);

        // comment
        TableColumnExt commentCol = addCommentColumn(TaxonTableModel.COMMENT);
        commentCol.setSortable(false);
        TableColumnExt creationDateCol = addDatePickerColumnToModel(TaxonTableModel.CREATION_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(creationDateCol, 120);
        TableColumnExt updateDateCol = addDatePickerColumnToModel(TaxonTableModel.UPDATE_DATE, getConfig().getDateTimeFormat(), false);
        fixColumnWidth(updateDateCol, 120);

        // citation
        TableColumnExt citationCol = addFilterableComboDataColumnToModel(
                TaxonTableModel.CITATION,
                getContext().getReferentialService().getCitations(),
                false);

        // obsolete
        TableColumnExt obsoleteCol = addBooleanColumnToModel(TaxonTableModel.OBSOLETE, getTable());
        obsoleteCol.setSortable(false);

        TaxonTableModel tableModel = new TaxonTableModel(getTable().getColumnModel(), true);
        getTable().setModel(tableModel);

        // Add extraction action
        addExportToCSVAction(t("reefdb.property.taxons.local"));

        // Initialisation du tableau
        initTable(getTable());

        // optionnal columns are hidden
        citationCol.setVisible(false);

        creationDateCol.setVisible(false);
        updateDateCol.setVisible(false);

        getTable().setVisibleRowCount(5);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractReefDbTableModel<TaxonTableRowModel> getTableModel() {
        return (TaxonTableModel) getTable().getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SwingTable getTable() {
        return ui.getTaxonsLocalTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowsAdded(List<TaxonTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            TaxonTableRowModel rowModel = addedRows.get(0);
            // Set default status TODO taxonName n'a pas de status !!
            rowModel.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
            setFocusOnCell(rowModel);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onRowModified(int rowIndex, TaxonTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        row.setDirty(true);
        forceRevalidateModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isRowValid(TaxonTableRowModel row) {
        row.getErrors().clear();
        return super.isRowValid(row) && isUnique(row);
    }

    private boolean isUnique(TaxonTableRowModel row) {

        if (StringUtils.isNotBlank(row.getName())) {
            boolean duplicateFound = false;

            // check unicity in local table
            for (TaxonTableRowModel otherRow : getModel().getRows()) {
                if (row == otherRow) continue;
                if (row.getName().equalsIgnoreCase(otherRow.getName())
                        && Objects.equals(row.getCitation(), otherRow.getCitation())) {
                    // duplicate found in table
                    ReefDbBeans.addError(row,
                            t("reefdb.error.alreadyExists.referential",
                                    t("reefdb.property.taxon"), decorate(row, DecoratorService.WITH_CITATION), t("reefdb.property.referential.local")),
                            TaxonDTO.PROPERTY_NAME,
                            TaxonDTO.PROPERTY_CITATION);
                    duplicateFound = true;
                    break;
                }
            }

            if (!duplicateFound) {
                // check unicity in database
                TaxonCriteriaDTO taxonCriteria = ReefDbBeanFactory.newTaxonCriteriaDTO();
                taxonCriteria.setName(row.getName());
                taxonCriteria.setStrictName(true);
                List<TaxonDTO> existingTaxons = getContext().getReferentialService().searchTaxons(taxonCriteria);
                if (CollectionUtils.isNotEmpty(existingTaxons)) {
                    for (TaxonDTO taxon : existingTaxons) {
                        if (!taxon.getId().equals(row.getId())
                                && Objects.equals(row.getCitation(), taxon.getCitation())) {
                            // duplicate found in database
                            ReefDbBeans.addError(row,
                                    t("reefdb.error.alreadyExists.referential",
                                            t("reefdb.property.pmfm"), decorate(row, DecoratorService.WITH_CITATION),
                                            ReefDbBeans.isLocalStatus(taxon.getStatus()) ? t("reefdb.property.referential.local") : t("reefdb.property.referential.national")),
                                    TaxonDTO.PROPERTY_NAME,
                                    TaxonDTO.PROPERTY_CITATION);
                            break;
                        }
                    }
                }
            }
        }

        return row.getErrors().isEmpty();

    }

}
