package fr.ifremer.reefdb.ui.swing.content.extraction.list.external;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractEmptyUIModel;

/**
 * Created by Ludovic on 26/05/2015.
 */
public class ExternalChooseUIModel extends AbstractEmptyUIModel<ExternalChooseUIModel> {

    private String email;
    public static final String PROPERTY_EMAIL = "email";

    private String fileName;
    public static final String PROPERTY_FILE_NAME = "fileName";

    /**
     * <p>Getter for the field <code>email</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmail() {
        return email;
    }

    /**
     * <p>Setter for the field <code>email</code>.</p>
     *
     * @param email a {@link java.lang.String} object.
     */
    public void setEmail(String email) {
        String oldValue = this.email;
        this.email = email;
        firePropertyChange(PROPERTY_EMAIL, oldValue, email);
    }

    /**
     * <p>Getter for the field <code>fileName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter for the field <code>fileName</code>.</p>
     *
     * @param fileName a {@link java.lang.String} object.
     */
    public void setFileName(String fileName) {
        String oldValue = this.fileName;
        this.fileName = fileName;
        firePropertyChange(PROPERTY_FILE_NAME, oldValue, fileName);
    }
}
