package fr.ifremer.reefdb.ui.swing.util.table.editor;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Cell Editor abstrait specifique à l application.
 */
public abstract class AbstratReefDbCellEditor extends AbstractCellEditor implements TableCellEditor, FocusListener, AncestorListener {

	/**
	 * Initialiser l editeur.
	 */
	protected void init() {
		getTextField().addFocusListener(this);
		getTextField().addAncestorListener(this);
	}
	
	/**
	 * Le TextField.
	 *
	 * @return Le TextField
	 */
	public abstract JTextComponent getTextField();	

	// ------------------------- FocusListener

	/** {@inheritDoc} */
	@Override
	public void focusGained(FocusEvent e) {
        focusComponent();
	}

	/** {@inheritDoc} */
	@Override
	public void focusLost(FocusEvent e) {
		
	}

	// ------------------------- AncestorListener

    /** {@inheritDoc} */
    @Override
    public void ancestorAdded(AncestorEvent event) {
    	focusComponent();
    }

    /** {@inheritDoc} */
    @Override
    public void ancestorRemoved(AncestorEvent event) {
    	// Override
    }

    /** {@inheritDoc} */
    @Override
    public void ancestorMoved(AncestorEvent event) {
    	// Override
    }

    private void focusComponent() {
        SwingUtilities.invokeLater(() -> {
            getTextField().selectAll();
            getTextField().requestFocusInWindow();
        });
    }
}
