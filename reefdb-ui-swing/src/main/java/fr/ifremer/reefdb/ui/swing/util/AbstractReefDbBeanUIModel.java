package fr.ifremer.reefdb.ui.swing.util;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import org.nuiton.util.beans.Binder;

/**
 * Abstract UI model to edit a bean.
 *
 * @param <B> type of Bean
 * @param <M> type of model
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 * @since 0.1
 * <p/>
 * classes extending this must implement the E interface and delegate methods to delegateObject
 */
public abstract class AbstractReefDbBeanUIModel<B extends QuadrigeBean, M extends AbstractReefDbBeanUIModel<B, M>>
        extends AbstractBeanUIModel<B, M> {

    /**
     * <p>Constructor for AbstractReefDbBeanUIModel.</p>
     *
     * @param fromBeanBinder a {@link org.nuiton.util.beans.Binder} object.
     * @param toBeanBinder a {@link org.nuiton.util.beans.Binder} object.
     */
    protected AbstractReefDbBeanUIModel(Binder<B, M> fromBeanBinder, Binder<M, B> toBeanBinder) {
        super(fromBeanBinder, toBeanBinder);
    }

    /**
     * <p>isCreate.</p>
     *
     * @return a boolean.
     */
    public boolean isCreate() {
        return delegateObject.getId() == null;
    }

    /**
     * <p>getId.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return delegateObject.getId();
    }

    /**
     * <p>setId.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    public void setId(Integer id) {
        delegateObject.setId(id);
    }

}
