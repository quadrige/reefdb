package fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.context.ManageContextsUI;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>DeleteContextAction class.</p>
 *
 */
public class DeleteContextAction extends AbstractReefDbAction<ManageContextsListTableUIModel, ManageContextsListTableUI, ManageContextsListTableUIHandler> {

    private List<ContextDTO> contextsToDelete;
    private boolean activeContextIsDeleted;

    /**
     * <p>Constructor for DeleteContextAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.context.contextslist.ManageContextsListTableUIHandler} object.
     */
    public DeleteContextAction(final ManageContextsListTableUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public boolean prepareAction() throws Exception {
        boolean canContinue = super.prepareAction() && getModel().getSelectedRows().size() > 0;

        if (canContinue) {

            contextsToDelete = Lists.newArrayList();
            for (ManageContextsListTableUIRowModel selectedRow: getModel().getSelectedRows()) {
                contextsToDelete.add(selectedRow.toBean());
            }

            // detect if active context is selected
            activeContextIsDeleted = false;
            if (getContext().getSelectedContext() != null) {
                activeContextIsDeleted = contextsToDelete.contains(getContext().getSelectedContext());
            }

            if (activeContextIsDeleted) {
                canContinue = askBeforeDelete(t("reefdb.action.delete.confirm.title"), t("reefdb.action.delete.context.active.message"));
            } else {
                canContinue = askBeforeDelete(t("reefdb.action.delete.confirm.title"), t("reefdb.action.delete.context.message"));
            }
        }

        return canContinue;
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        getContext().getContextService().deleteContexts(contextsToDelete);

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().deleteSelectedRows();

        // if active context has been deleted, reset it
        if (activeContextIsDeleted) {
            getContext().setSelectedContext(null);
        }

        ManageContextsUI manageContextsUI = getHandler().getParentContainer(ManageContextsUI.class);
        if (manageContextsUI != null) {
            manageContextsUI.getManageContextsListMenuUI().getHandler().reloadComboBox();
        }

        super.postSuccessAction();
    }
}
