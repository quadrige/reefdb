package fr.ifremer.reefdb.ui.swing.content.manage.program.programs.departments;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import fr.ifremer.reefdb.ui.swing.content.manage.program.programs.ProgramsTableRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.Collection;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler.
 */
public class DepartmentsDialogUIHandler extends AbstractReefDbUIHandler<DepartmentsDialogUIModel, DepartmentsDialogUI> implements Cancelable {

    /**
     * Constant <code>DOUBLE_LIST="doubleList"</code>
     */
    public static final String DOUBLE_LIST = "doubleList";
    /**
     * Constant <code>LIST="list"</code>
     */
    public static final String LIST = "list";

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeInit(DepartmentsDialogUI ui) {
        super.beforeInit(ui);

        DepartmentsDialogUIModel model = new DepartmentsDialogUIModel();
        ui.setContextValue(model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void afterInit(final DepartmentsDialogUI ui) {
        initUI(ui);

        getUI().getDepartmentsList().setCellRenderer(newListCellRender(DepartmentDTO.class));

        getModel().addPropertyChangeListener(DepartmentsDialogUIModel.PROPERTY_PROGRAM, evt -> {

            if (getModel().getProgram() == null) {
                return;
            }

            Assert.notNull(getModel().getPrivilege(), "must set the privilege property before program");

            ProgramsTableRowModel program = getModel().getProgram();
            Collection<DepartmentDTO> departments = null;
            switch (getModel().getPrivilege()) {
                case MANAGER:
                    throw new ReefDbBusinessException("manager departments not handled");
                case RECORDER:
                    departments = program.getRecorderDepartments();
                    getUI().setTitle(t("reefdb.program.program.recorderDepartments.title", decorate(program)));
                    break;
                case FULL_VIEWER:
                    departments = program.getFullViewerDepartments();
                    getUI().setTitle(t("reefdb.program.program.fullViewerDepartments.title", decorate(program)));
                    break;
                case VIEWER:
                    departments = program.getViewerDepartments();
                    getUI().setTitle(t("reefdb.program.program.viewerDepartments.title", decorate(program)));
                    break;
                case VALIDATOR:
                    departments = program.getValidatorDepartments();
                    getUI().setTitle(t("reefdb.program.program.validatorDepartments.title", decorate(program)));
                    break;
            }

            if (program.isEditable()) {
                getUI().getListPanelLayout().setSelected(DOUBLE_LIST);

                // update filterUI
                getUI().getFilterElementDepartmentUI().getFilterDoubleList().getModel().setSelected(Lists.newArrayList(departments));
                getUI().getFilterElementDepartmentUI().getHandler().enable();

                if (!program.isLocal()) {
                    // if program is national, department menu must search only national departments
                    getUI().getFilterElementDepartmentUI().getHandler().forceLocal(false);
                }

            } else {
                getUI().getListPanelLayout().setSelected(LIST);

                getUI().getDepartmentsList().setListData(departments.toArray(new DepartmentDTO[0]));
            }

        });

    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        // set privilege to user
        if (getModel().getProgram().isEditable()) {

            List<DepartmentDTO> departments = getUI().getFilterElementDepartmentUI().getModel().getElements();
            switch (getModel().getPrivilege()) {

                case MANAGER:
                    throw new ReefDbBusinessException("manager departments not handled");
                case RECORDER:
                    getModel().getProgram().setRecorderDepartments(departments);
                    break;
                case FULL_VIEWER:
                    getModel().getProgram().setFullViewerDepartments(departments);
                    break;
                case VIEWER:
                    getModel().getProgram().setViewerDepartments(departments);
                    break;
                case VALIDATOR:
                    getModel().getProgram().setValidatorDepartments(departments);
                    break;
            }
        }

        // close the dialog box
        closeDialog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cancel() {
        closeDialog();
    }
}
