package fr.ifremer.reefdb.ui.swing.content.synchro.changes;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.i18n.I18n;

import javax.swing.JScrollPane;
import javax.swing.SortOrder;
import java.util.List;

/**
 * Controleur pour la zone des programmes.
 */
public class SynchroChangesUIHandler extends AbstractReefDbTableUIHandler<SynchroChangesRowModel, SynchroChangesUIModel, SynchroChangesUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SynchroChangesUIHandler.class);

    //private final TableColumnExt columnName

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SynchroChangesUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SynchroChangesUIModel model = new SynchroChangesUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(SynchroChangesUI ui) {

        // Init UI
        initUI(ui);

        // Init list
        initList();

        // Init table
        initTable();

        // load change log?
        //getModel().setBeans(programs);

        getTable().packAll();
    }

    @SuppressWarnings("unchecked")
    private void initList() {
        getUI().getEntityTypeList().setCellRenderer(newListCellRender(SynchroTableDTO.class, DecoratorService.WITH_COUNT));

        getModel().addPropertyChangeListener(SynchroChangesUIModel.PROPERTY_CHANGES, evt -> populateList());

        getUI().getEntityTypeList().addListSelectionListener(e -> {
            SynchroTableDTO selectedTable = getUI().getEntityTypeList().getSelectedValue();
            populateTable(selectedTable);
        });
    }

    /**
     * Initialisation de le tableau.
     */
    private void initTable() {

        // Operation type
        TableColumnExt operationTypeColumm = addColumn(
                null,
                new SynchroOperationTypeIconCellRenderer(getContext()),
                SynchroChangesTableModel.OPERATION_TYPE);
        operationTypeColumm.setSortable(true);
        operationTypeColumm.setMinWidth(30);
        operationTypeColumm.setMaxWidth(30);
        operationTypeColumm.setResizable(false);

        // Column name
        final TableColumnExt columnName = addColumn(
                SynchroChangesTableModel.NAME);
        columnName.setSortable(true);

        // Modele de la table
        final SynchroChangesTableModel tableModel = new SynchroChangesTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Les columns obligatoire sont toujours presentes
        columnName.setHideable(false);

        getTable().setEditable(false);

        // Initialisation de la table
        initTable(getTable());

        // Tri par defaut
        getTable().setSortOrder(SynchroChangesTableModel.NAME, SortOrder.ASCENDING);

        getUI().getTableParent().setVisible(false);
    }


    /**
     * <p>populateList.</p>
     */
    protected void populateList() {
        // Set tables from model, to the JList
        SynchroChangesDTO synchroChanges = getModel().getChanges();
        if (synchroChanges != null && CollectionUtils.isNotEmpty(synchroChanges.getTables())) {
            SynchroTableDTO[] tables = synchroChanges.getTables().toArray(new SynchroTableDTO[synchroChanges.getTables().size()]);
            getUI().getEntityTypeList().setListData(tables);
        }
        else {
            getUI().getEntityTypeList().setListData(new SynchroTableDTO[0]);
        }

        getModel().setValid(false);
        //getUI().updateUI();
        getTableParent().setVisible(false);
    }

    /**
     * <p>populateTable.</p>
     *
     * @param synchroTable a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO} object.
     */
    protected void populateTable(SynchroTableDTO synchroTable) {

        TableColumnExt nameColumn = getTable().getColumnExt(SynchroChangesTableModel.NAME);
        List<SynchroChangesRowModel> rowModels = Lists.newArrayList();

        if (synchroTable == null
                || CollectionUtils.isEmpty(synchroTable.getRows())) {
            getTableParent().setVisible(false);
            nameColumn.setTitle(I18n.t("reefdb.synchro.changes.name.short.default"));
        }
        else {

            getTableParent().setVisible(true);
            getUI().updateUI();
            nameColumn.setTitle(I18n.t("reefdb.synchro.changes.name.short", decorate(synchroTable)));

                        // Create a listener to update strategy, when select a row
//            PropertyChangeListener onRowSelectChangeListener = new PropertyChangeListener() {
//                @Override
//                public void propertyChange(PropertyChangeEvent evt) {
//                    // Mark row as resolve
//                    SynchroChangesRowModel rowModel = (SynchroChangesRowModel)evt.getSource();
//                    boolean isSelected = evt.getNewValue() != null && (boolean)evt.getNewValue();
//                    if (isSelected && rowModel.getStrategy() == null) {
//                        rowModel.setStrategy(RejectedRow.ResolveStrategy.UPDATE.name());
//                    }
//                    else if (!isSelected && rowModel.getStrategy() != null){
//                        rowModel.setStrategy(null);
//                    }
//                }
//            };

            // Create a listener to refresh entity list (row count changed)
//            PropertyChangeListener onRowStrategyChangeListener = new PropertyChangeListener() {
//                @Override
//                public void propertyChange(PropertyChangeEvent evt) {
//                    // Mark row as resolve
//                    SynchroChangesRowModel rowModel = (SynchroChangesRowModel)evt.getSource();
//                    String strategy = (String)evt.getNewValue();
//                    if (!Objects.equals(strategy, evt.getOldValue())) {
//                        boolean isValid = ReefDbBeans.hasRowWithStrategy(getModel().getChanges());
//                        getModel().setValid(isValid);
//
//                        // Refresh table list (row model has changed)
//                        //getUI().updateUI();
//                    }
//                }
//            };

//            boolean hasRowWithStrategy = false;
//            for (SynchroRowDTO row : synchroTable.getRows()) {
//                SynchroChangesRowModel rowModel = new SynchroChangesRowModel();
//                rowModel.setBean(row);
//                if (row.getStrategy() != null) {
//                    rowModel.setSelected(true);
//                    hasRowWithStrategy = true;
//                }
//                rowModels.add(rowModel);

                // Add listeners
//                rowModel.addPropertyChangeListener(SynchroChangesRowModel.PROPERTY_SELECTED, onRowSelectChangeListener);
//                rowModel.addPropertyChangeListener(SynchroChangesRowModel.PROPERTY_STRATEGY, onRowStrategyChangeListener);
//            }

            getModel().setBeans(synchroTable.getRows());
//            getModel().setValid(hasRowWithStrategy);

        }


//        getModel().setRows(rowModels);
    }



    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<SynchroChangesRowModel> getTableModel() {
        return (SynchroChangesTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getTable();
    }

    /**
     * <p>getTableParent.</p>
     *
     * @return a {@link javax.swing.JScrollPane} object.
     */
    public JScrollPane getTableParent() {
        return getUI().getTableParent();
    }

    /**
     * <p>cancel.</p>
     */
    public void cancel() {
        getModel().setChangesValidated(false);
        closeDialog();
    }

    /**
     * <p>confirm.</p>
     */
    public void confirm() {
        getModel().setChangesValidated(true);
        closeDialog();
    }

    /* -- Internal methods -- */
}
