package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonomicLevelDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Model.
 */
public class TaxonTableModel extends AbstractReefDbTableModel<TaxonTableRowModel> {

    private final boolean readOnly;

    /**
     * <p>Constructor for TaxonTableModel.</p>
     *
     * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
     * @param createNewRowAllowed a boolean.
     */
    public TaxonTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
        super(columnModel, createNewRowAllowed, false);
        readOnly = !createNewRowAllowed;
    }

    // name
    /** Constant <code>NAME</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.property.name"),
            String.class,
            true);

    // citation
    /** Constant <code>CITATION</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> CITATION = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_CITATION,
            n("reefdb.property.taxon.citation"),
            n("reefdb.property.taxon.citation"),
            BaseReferentialDTO.class);

    // reference taxon
    /** Constant <code>REFERENCE_TAXON</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> REFERENCE_TAXON = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_REFERENCE_TAXON,
            n("reefdb.property.taxon.reference"),
            n("reefdb.property.taxon.reference"),
            TaxonDTO.class,
            DecoratorService.WITH_CITATION);

    // parent taxon
    /** Constant <code>PARENT</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> PARENT = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_PARENT_TAXON,
            n("reefdb.property.taxon.parent"),
            n("reefdb.property.taxon.parent"),
            TaxonDTO.class,
            DecoratorService.WITH_CITATION,
            true);

    // level
    /** Constant <code>LEVEL</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> LEVEL = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_LEVEL,
            n("reefdb.property.taxon.level"),
            n("reefdb.property.taxon.level.tip"),
            TaxonomicLevelDTO.class,
            true);

    // comment
    /** Constant <code>COMMENT</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.comment"),
            n("reefdb.property.comment"),
            String.class);

    // temporary
    /** Constant <code>TEMPORARY</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> TEMPORARY = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_TEMPORARY,
            n("reefdb.property.temporary"),
            n("reefdb.property.temporary"),
            Boolean.class);

    // obsolete
    /** Constant <code>OBSOLETE</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> OBSOLETE = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_OBSOLETE,
            n("reefdb.property.obsolete"),
            n("reefdb.property.obsolete"),
            Boolean.class/*,
            true*/);

    // virtual
    /** Constant <code>VIRTUAL</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> VIRTUAL = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_VIRTUAL,
            n("reefdb.property.virtual"),
            n("reefdb.property.virtual"),
            Boolean.class);

    // composites
    /** Constant <code>COMPOSITES</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> COMPOSITES = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_COMPOSITE_TAXONS,
            n("reefdb.property.taxon.composites"),
            n("reefdb.property.taxon.composites"),
            TaxonDTO.class,
            DecoratorService.COLLECTION_SIZE);

    // taxref
    /** Constant <code>TAXREF</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> TAXREF = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_TAX_REF,
            n("reefdb.property.taxon.taxRef"),
            n("reefdb.property.taxon.taxRef"),
            String.class);

    // worms
    /** Constant <code>WORMS</code> */
    public static final ReefDbColumnIdentifier<TaxonTableRowModel> WORMS = ReefDbColumnIdentifier.newId(
            TaxonTableRowModel.PROPERTY_WORMS_REF,
            n("reefdb.property.taxon.worms"),
            n("reefdb.property.taxon.worms"),
            String.class);


    public static final ReefDbColumnIdentifier<TaxonTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        TaxonTableRowModel.PROPERTY_CREATION_DATE,
        n("reefdb.property.date.creation"),
        n("reefdb.property.date.creation"),
        Date.class);

    public static final ReefDbColumnIdentifier<TaxonTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        TaxonTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);


    /** {@inheritDoc} */
    @Override
    public TaxonTableRowModel createNewRow() {
        return new TaxonTableRowModel(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public ReefDbColumnIdentifier<TaxonTableRowModel> getFirstColumnEditing() {
        return NAME;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<TaxonTableRowModel> propertyName) {

        if (propertyName == COMPOSITES) {
            TaxonTableRowModel row = getEntry(rowIndex);
            return row.sizeCompositeTaxons() > 0;
        }

        return super.isCellEditable(rowIndex, columnIndex, propertyName);
    }

}
