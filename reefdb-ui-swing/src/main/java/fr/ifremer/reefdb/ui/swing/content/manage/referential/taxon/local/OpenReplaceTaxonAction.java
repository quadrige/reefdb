package fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.ManageTaxonsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local.replace.ReplaceTaxonUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local.replace.ReplaceTaxonUIModel;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 */
public class OpenReplaceTaxonAction extends AbstractReefDbAction<TaxonsLocalUIModel, TaxonsLocalUI, TaxonsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceTaxonAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.taxon.local.TaxonsLocalUIHandler} object.
     */
    public OpenReplaceTaxonAction(TaxonsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<TaxonDTO, ReplaceTaxonUIModel, ReplaceTaxonUI> openAction =
                new AbstractOpenReplaceUIAction<TaxonDTO, ReplaceTaxonUIModel, ReplaceTaxonUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.taxon");
                    }

                    @Override
                    protected ReplaceTaxonUIModel createNewModel() {
                        return new ReplaceTaxonUIModel();
                    }

                    @Override
                    protected ReplaceTaxonUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceTaxonUI(ctx);
                    }

                    @Override
                    protected List<TaxonDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getTaxons(null));
                    }

                    @Override
                    protected List<TaxonDTO> getSourceListFromReferentialList(List<TaxonDTO> list) {
                        return ReefDbBeans.filterLocalReferential(list);
                    }

                    @Override
                    protected TaxonDTO getSelectedSource() {
                        List<TaxonDTO> selectedBeans = OpenReplaceTaxonAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected TaxonDTO getSelectedTarget() {
                        ManageTaxonsUI ui = OpenReplaceTaxonAction.this.getUI().getParentContainer(ManageTaxonsUI.class);
                        List<TaxonDTO> selectedBeans = ui.getTaxonsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
