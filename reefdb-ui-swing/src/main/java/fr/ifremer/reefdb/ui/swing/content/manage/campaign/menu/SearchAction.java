package fr.ifremer.reefdb.ui.swing.content.manage.campaign.menu;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.CampaignsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.campaign.CampaignsUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

/**
 * Search action.
 */
public class SearchAction extends AbstractCheckModelAction<CampaignsMenuUIModel, CampaignsMenuUI, CampaignsMenuUIHandler> {

    private List<CampaignDTO> results;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final CampaignsMenuUIHandler handler) {
        super(handler, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler().getCampaignsUI().getHandler();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        final CampaignsUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        final CampaignsUIModel model = getLocalModel();
        if (model != null) {
            model.setModify(modelModify);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        final CampaignsUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() throws Exception {

        // Load program by code
        results = getContext().getCampaignService().findCampaignsByCriteria(
                getModel().getName(),
                getModel().getSearchStartDate(), getModel().getStartDate1(), getModel().getStartDate2(),
                getModel().getSearchEndDate(), getModel().getEndDate1(), getModel().getEndDate2()
        );

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        getModel().setResults(results);

        super.postSuccessAction();
    }

    private CampaignsUIModel getLocalModel() {
        CampaignsUI ui = getHandler().getCampaignsUI();
        return ui != null ? ui.getModel() : null;
    }

}
