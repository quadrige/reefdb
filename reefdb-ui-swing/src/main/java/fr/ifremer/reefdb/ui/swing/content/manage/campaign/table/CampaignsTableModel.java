package fr.ifremer.reefdb.ui.swing.content.manage.campaign.table;

/*
 * #%L
 * ReefDb :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTableColumnModel;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;

import java.time.LocalDate;
import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * Le modele pour le tableau des programmes.
 */
public class CampaignsTableModel extends AbstractReefDbTableModel<CampaignsTableRowModel> {

    /**
     * Identifiant pour la colonne libelle.
     */
    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> NAME = ReefDbColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_NAME,
            n("reefdb.property.name"),
            n("reefdb.campaign.name.tip"),
            String.class,
            true);

    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> START_DATE = ReefDbColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_START_DATE,
            n("reefdb.property.date.start"),
            n("reefdb.campaign.date.start.tip"),
            LocalDate.class,
            true);

    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> END_DATE = ReefDbColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_END_DATE,
            n("reefdb.property.date.end"),
            n("reefdb.campaign.date.end.tip"),
            LocalDate.class);

    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> SISMER_LINK = ReefDbColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_SISMER_LINK,
            n("reefdb.property.sismer.link"),
            n("reefdb.campaign.sismer.link.tip"),
            String.class);

    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> MANAGER = ReefDbColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_MANAGER,
            n("reefdb.property.manager"),
            n("reefdb.campaign.manager.tip"),
            PersonDTO.class,
            true);

    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
            CampaignsTableRowModel.PROPERTY_COMMENT,
            n("reefdb.property.comment"),
            n("reefdb.campaign.comment.tip"),
            String.class);
    
    public static final ReefDbColumnIdentifier<CampaignsTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
        CampaignsTableRowModel.PROPERTY_UPDATE_DATE,
        n("reefdb.property.date.modification"),
        n("reefdb.property.date.modification"),
        Date.class);



    /**
     * Constructor.
     *
     * @param columnModel Le modele pour les colonnes
     */
    public CampaignsTableModel(final SwingTableColumnModel columnModel) {
        super(columnModel, true, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CampaignsTableRowModel createNewRow() {
        return new CampaignsTableRowModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReefDbColumnIdentifier<CampaignsTableRowModel> getFirstColumnEditing() {
        return NAME;
    }

}
