package fr.ifremer.reefdb.ui.swing.content.manage.referential.location.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.CoordinateAreaAware;
import fr.ifremer.reefdb.dto.data.PositioningPrecisionAware;
import fr.ifremer.reefdb.dto.referential.GroupingDTO;
import fr.ifremer.reefdb.dto.referential.HarbourDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.PositioningSystemDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Row model.
 */
public class LocationTableRowModel extends AbstractReefDbRowUIModel<LocationDTO, LocationTableRowModel> implements LocationDTO, ErrorAware, CoordinateAreaAware, PositioningPrecisionAware {

    private static final Binder<LocationDTO, LocationTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(LocationDTO.class, LocationTableRowModel.class);

    private static final Binder<LocationTableRowModel, LocationDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(LocationTableRowModel.class, LocationDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for LocationTableRowModel.</p>
     *
     * @param readOnly a boolean.
     */
    public LocationTableRowModel(boolean readOnly) {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
        setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEditable() {
        return !isReadOnly() && super.isEditable();
    }

    /** {@inheritDoc} */
    @Override
    protected LocationDTO newBean() {
        return ReefDbBeanFactory.newLocationDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getLabel() {
        return delegateObject.getLabel();
    }

    /** {@inheritDoc} */
    @Override
    public void setLabel(String identifiant) {
        delegateObject.setLabel(identifiant);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String libelle) {
        delegateObject.setName(libelle);
    }

    /** {@inheritDoc} */
    @Override
    public GroupingDTO getGrouping() {
        return delegateObject.getGrouping();
    }

    /** {@inheritDoc} */
    @Override
    public void setGrouping(GroupingDTO grouping) {
        delegateObject.setGrouping(grouping);
    }

    /** {@inheritDoc} */
    @Override
    public HarbourDTO getHarbour() {
        return delegateObject.getHarbour();
    }

    /** {@inheritDoc} */
    @Override
    public void setHarbour(HarbourDTO harbour) {
        delegateObject.setHarbour(harbour);
    }

    /** {@inheritDoc} */
    @Override
    public Double getBathymetry() {
        return delegateObject.getBathymetry();
    }

    /** {@inheritDoc} */
    @Override
    public void setBathymetry(Double bathymetrie) {
        delegateObject.setBathymetry(bathymetrie);
    }

    /** {@inheritDoc} */
    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    /** {@inheritDoc} */
    @Override
    public void setComment(String commentaire) {
        delegateObject.setComment(commentaire);
    }

    /** {@inheritDoc} */
    @Override
    public Double getUtFormat() {
        return delegateObject.getUtFormat();
    }

    /** {@inheritDoc} */
    @Override
    public void setUtFormat(Double deltaUTHiver) {
        delegateObject.setUtFormat(deltaUTHiver);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean getDayLightSavingTime() {
        return delegateObject.getDayLightSavingTime();
    }

    /** {@inheritDoc} */
    @Override
    public void setDayLightSavingTime(Boolean appliquerChangementHeure) {
        delegateObject.setDayLightSavingTime(appliquerChangementHeure);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioning() {
        return delegateObject.getPositioning();
    }

    /** {@inheritDoc} */
    @Override
    public void setPositioning(PositioningSystemDTO positionnement) {
        delegateObject.setPositioning(positionnement);
    }

    /** {@inheritDoc} */
    @Override
    public CoordinateDTO getCoordinate() {
        return delegateObject.getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public void setCoordinate(CoordinateDTO coordonnee) {
        delegateObject.setCoordinate(coordonnee);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    /** {@inheritDoc} */
    @Override
    public Double getMinLatitude() {
        return getCoordinateToModify().getMinLatitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setMinLatitude(Double minLatitude) {
        Double oldValue = getMinLatitude();
        getCoordinateToModify().setMinLatitude(minLatitude);
        firePropertyChange(PROPERTY_MIN_LATITUDE, oldValue, minLatitude);
    }

    /** {@inheritDoc} */
    @Override
    public Double getMinLongitude() {
        return getCoordinateToModify().getMinLongitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setMinLongitude(Double minLongitude) {
        Double oldValue = getMinLongitude();
        getCoordinateToModify().setMinLongitude(minLongitude);
        firePropertyChange(PROPERTY_MIN_LONGITUDE, oldValue, minLongitude);
    }

    /** {@inheritDoc} */
    @Override
    public Double getMaxLatitude() {
        return getCoordinateToModify().getMaxLatitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setMaxLatitude(Double maxLatitude) {
        Double oldValue = getMaxLatitude();
        getCoordinateToModify().setMaxLatitude(maxLatitude);
        firePropertyChange(PROPERTY_MAX_LATITUDE, oldValue, maxLatitude);
    }

    /** {@inheritDoc} */
    @Override
    public Double getMaxLongitude() {
        return getCoordinateToModify().getMaxLongitude();
    }

    /** {@inheritDoc} */
    @Override
    public void setMaxLongitude(Double maxLongitude) {
        Double oldValue = getMaxLongitude();
        getCoordinateToModify().setMaxLongitude(maxLongitude);
        firePropertyChange(PROPERTY_MAX_LONGITUDE, oldValue, maxLongitude);
    }

    private CoordinateDTO getCoordinateToModify() {
        if (getCoordinate() == null) {
            setCoordinate(ReefDbBeanFactory.newCoordinateDTO());
        }
        return getCoordinate();
    }

    /** {@inheritDoc} */
    @Override
    public String getPositioningPrecision() {
        return getPositioning() == null ? null : getPositioning().getPrecision();
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
