package fr.ifremer.reefdb.ui.swing.content.manage.rule;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.synchro.vo.SynchroProgressionStatus;
import fr.ifremer.quadrige3.ui.swing.synchro.action.ImportSynchroCheckAction;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.menu.RulesMenuUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist.RuleListRowModel;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.rulelist.RuleListUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Controlleur pour l'onglet prelevements mesures.
 */
public class RulesUIHandler extends AbstractReefDbUIHandler<RulesUIModel, RulesUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(RulesUIHandler.class);
    private ImportSynchroCheckAction importSynchroCheckAction;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final RulesUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        ui.setContextValue(new RulesUIModel());
        ui.setContextValue(SwingUtil.createActionIcon("rule-list"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final RulesUI ui) {
        initUI(ui);

        // Initialiser les parametres des ecrans Observation et prelevemnts
        getContext().clearObservationPrelevementsIds();

        // set model editable if user is admin
        getModel().setUserIsAdmin(getContext().isAuthenticatedAsLocalAdmin());

        // Save models
        getModel().setRuleListUIModel(getUI().getRuleListUI().getModel());
        getModel().setProgramsUIModel(getUI().getControlProgramTableUI().getModel());
        getModel().setDepartmentsUIModel(getUI().getControlDepartmentTableUI().getModel());
        getModel().setControlRuleUIModel(getUI().getControlRuleTableUI().getModel());
        getModel().setPmfmUIModel(getUI().getControlPmfmTableUI().getModel());

        // Disable controls regarding editable property
        disableControls();

        // Listen modify property and set dirty to the selected program
        initListeners();

        // Register validator
        registerValidators(getValidator());
        listenValidatorValid(getValidator(), getModel());

        // Get managed programs
        getModel().setManagedProgramsAvailable(
            CollectionUtils.isNotEmpty(getContext().getProgramStrategyService().getManagedPrograms())
        );

        // check referential update (Mantis #46969)
        SwingUtilities.invokeLater(this::checkForReferentialUpdates);
    }

    private void initListeners() {

        // Listen search results
        getUI().getRulesMenuUI().getModel().addPropertyChangeListener(RulesMenuUIModel.PROPERTY_RESULTS, evt ->
            getUI().getRuleListUI().getHandler().loadRuleLists((List<RuleListDTO>) evt.getNewValue())
        );

        // Listen modify property and set dirty to the selected rule list
        listenModelModify(getModel().getRuleListUIModel());
        PropertyChangeListener modifyListener = evt -> {
            Boolean modify = (Boolean) evt.getNewValue();
            if (modify != null) {
                getModel().setModify(modify);

                RuleListRowModel selectedRuleList = getModel().getRuleListUIModel().getSingleSelectedRow();
                if (selectedRuleList != null) {
                    if (modify) {
                        selectedRuleList.setDirty(true);
                    }
                    getUI().getRuleListUI().getHandler().recomputeRowValidState(selectedRuleList);
                }
                getValidator().doValidate();

            }
        };
        getModel().getProgramsUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, modifyListener);
        getModel().getDepartmentsUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, modifyListener);
        getModel().getControlRuleUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, modifyListener);
        getModel().getPmfmUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, modifyListener);

        // listen pmfm changes and validate rule
        getModel().getPmfmUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, evt ->
                getUI().getControlRuleTableUI().getHandler().onPmfmModified(getModel().getControlRuleUIModel().getSingleSelectedRow()));

        // Listen valid state
        listenModelValid(getModel().getRuleListUIModel());

        PropertyChangeListener validListener = evt -> {
            Boolean valid = (Boolean) evt.getNewValue();
            if (valid != null) {
                getModel().setValid(valid);
                forceRevalidateModel();
            }
        };

        getModel().getProgramsUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, validListener);
        getModel().getDepartmentsUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, validListener);
        getModel().getControlRuleUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, validListener);
        getModel().getPmfmUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, validListener);

        getUI().addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                int h = getUI().getHeight()
                        - getUI().getTopPanel().getPreferredSize().height
                        - getUI().getBottomPanel().getPreferredSize().height - 30;
                h = Math.max(h, getUI().getRuleListUI().getPreferredSize().height);
                Dimension d = new Dimension(getUI().getControlRuleTableUI().getPreferredSize().width, h);
                getUI().getControlRuleTableUI().setPreferredSize(d);
                getUI().getControlRuleTableUI().invalidate();
            }
        });

        getModel().addPropertyChangeListener(RulesUIModel.PROPERTY_RULE_DESCRIPTION, evt -> {
            if (getModel().isLoading()) return;
            ControlRuleDTO selectedRule = getModel().getControlRuleUIModel().getSingleSelectedRow();
            selectedRule.setDescription(getModel().getRuleDescription());
        });
        getModel().addPropertyChangeListener(RulesUIModel.PROPERTY_RULE_MESSAGE, evt -> {
            if (getModel().isLoading()) return;
            ControlRuleDTO selectedRule = getModel().getControlRuleUIModel().getSingleSelectedRow();
            selectedRule.setMessage(getModel().getRuleMessage());
        });

        // Add listener on saveEnabled property to disable change (Mantis #48002)
        getModel().addPropertyChangeListener(RulesUIModel.PROPERTY_SAVE_ENABLED, evt -> {
            getModel().getRuleListUIModel().setSaveEnabled(getModel().isSaveEnabled());
        });

    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<RulesUIModel> getValidator() {
        return getUI().getValidator();
    }

    /**
     * <p>disableControls.</p>
     */
    private void disableControls() {

        //desactive tout les boutons et champ qui ne sont pas a remplir en premier
        getUI().getControlProgramTableUI().getAddProgramButton().setEnabled(false);
        getUI().getControlDepartmentTableUI().getAddDepartmentButton().setEnabled(false);
        getUI().getControlRuleTableUI().getAddControlRuleButton().setEnabled(false);
        getUI().getControlPmfmTableUI().getAddPmfmButton().setEnabled(false);
        getUI().getControlPmfmTableUI().getRemovePmfmButton().setEnabled(false);
        getUI().getRuleMessageEditor().setEnabled(false);
        getUI().getRuleDescriptionEditor().setEnabled(false);

        getUI().getRuleListUI().applyDataBinding(RuleListUI.BINDING_NEW_RULE_LIST_BUTTON_ENABLED);
        getUI().getRuleListUI().applyDataBinding(RuleListUI.BINDING_DUPLICATE_RULE_LIST_BUTTON_ENABLED);
        getUI().getRuleListUI().applyDataBinding(RuleListUI.BINDING_DELETE_RULE_LIST_BUTTON_ENABLED);

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private void checkForReferentialUpdates() {
        if (getContext().isNextImportSynchroCheckActionPrevented()) {
            getModel().setSaveEnabled(true);
            return;
        }
        if (getContext().getSynchroContext().isRunningStatus()) {
            return;
        }
        getImportSynchroCheckAction().execute();
        if (LOG.isDebugEnabled())
            LOG.debug("checkForReferentialUpdates executed");
    }

    private ImportSynchroCheckAction getImportSynchroCheckAction() {
        if (importSynchroCheckAction == null) {
            importSynchroCheckAction = getContext().getActionFactory().createNonBlockingUIAction(getContext().getSynchroHandler(), ImportSynchroCheckAction.class);
            importSynchroCheckAction.setCheckReferentialOnly(true);
            importSynchroCheckAction.setUseOptimisticCheck(true);
        }
        if (!importSynchroCheckAction.isConsumerSet()) {
            importSynchroCheckAction.setConsumer(synchroUIContext -> {

                if (LOG.isDebugEnabled())
                    LOG.debug("check result: " + synchroUIContext.isImportReferential());

                // If error occurs (eg. connection problem) set screen read-only (Mantis #48002)
                if (synchroUIContext.getStatus() == SynchroProgressionStatus.FAILED) {
                    getModel().setSaveEnabled(false);
                    getContext().getDialogHelper().showWarningDialog(t("reefdb.error.synchro.serverNotYetAvailable"));
                } else {
                    getModel().setSaveEnabled(true);
                    // get result
                    if (synchroUIContext.isImportReferential()) {
                        UpdateRulesAction updateRulesAction = getContext().getActionFactory().createLogicAction(this, UpdateRulesAction.class);
                        getContext().getActionEngine().runAction(updateRulesAction);
                    }
                }

                // Reset correctly the synchro context and widget (Mantis #48832)
                getContext().getSynchroHandler().report(t("quadrige3.synchro.report.idle"), false);
                getContext().getSynchroContext().resetImportContext();
                getContext().getSynchroContext().saveImportContext(true, true);

            });
        }
        return importSynchroCheckAction;
    }
}
