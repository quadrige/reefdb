package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.parameter.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.ReefDbColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import java.util.Date;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>ParameterTableModel class.</p>
 *
 * @author Antoine
 */
public class ParameterTableModel extends AbstractReefDbTableModel<ParameterTableRowModel> {

	
	/**
	 * <p>Constructor for ParameterTableModel.</p>
	 *
	 * @param columnModel a {@link org.jdesktop.swingx.table.TableColumnModelExt} object.
	 * @param createNewRowAllowed a boolean.
	 */
	public ParameterTableModel(final TableColumnModelExt columnModel, boolean createNewRowAllowed) {
		super(columnModel, createNewRowAllowed, false);
	}
	
	/** Constant <code>CODE</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> CODE = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_CODE,
			n("reefdb.property.code"),
			n("reefdb.property.code"),
			String.class,
			true);
	
	/** Constant <code>NAME</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> NAME = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_NAME,
			n("reefdb.property.name"),
			n("reefdb.property.name"),
			String.class,
			true);    
    
	/** Constant <code>DESCRIPTION</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> DESCRIPTION = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_DESCRIPTION,
			n("reefdb.property.description"),
			n("reefdb.property.description"),
			String.class);
	
	/** Constant <code>STATUS</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> STATUS = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_STATUS,
			n("reefdb.property.status"),
			n("reefdb.property.status"),
			StatusDTO.class,
			true);
	
	/** Constant <code>ASSOCIATED_QUALITATIVE_VALUE</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> ASSOCIATED_QUALITATIVE_VALUE = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_QUALITATIVE_VALUES,
			n("reefdb.property.pmfm.parameter.associatedQualitativeValue"),
			n("reefdb.property.pmfm.parameter.associatedQualitativeValue"),
			QualitativeValueDTO.class,
			DecoratorService.COLLECTION_SIZE);
	
	/** Constant <code>PARAMETER_GROUP</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> PARAMETER_GROUP = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_PARAMETER_GROUP,
			n("reefdb.property.pmfm.parameter.parameterGroup"),
			n("reefdb.property.pmfm.parameter.parameterGroup"),
			ParameterGroupDTO.class,
			true);
	
	/** Constant <code>IS_QUALITATIVE</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> IS_QUALITATIVE = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_QUALITATIVE,
			n("reefdb.property.pmfm.parameter.qualitative"),
			n("reefdb.property.pmfm.parameter.qualitative"),
			Boolean.class);
	
	/** Constant <code>IS_CALCULATED</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> IS_CALCULATED = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_CALCULATED,
			n("reefdb.property.pmfm.parameter.calculated"),
			n("reefdb.property.pmfm.parameter.calculated"),
			Boolean.class);
	
	/** Constant <code>IS_TAXONOMIC</code> */
	public static final ReefDbColumnIdentifier<ParameterTableRowModel> IS_TAXONOMIC = ReefDbColumnIdentifier.newId(
			ParameterTableRowModel.PROPERTY_TAXONOMIC,
			n("reefdb.property.pmfm.parameter.taxonomic"),
			n("reefdb.property.pmfm.parameter.taxonomic"),
			Boolean.class);


	public static final ReefDbColumnIdentifier<ParameterTableRowModel> COMMENT = ReefDbColumnIdentifier.newId(
		ParameterTableRowModel.PROPERTY_COMMENT,
		n("reefdb.property.comment"),
		n("reefdb.property.comment"),
		String.class,
		false);

	public static final ReefDbColumnIdentifier<ParameterTableRowModel> CREATION_DATE = ReefDbColumnIdentifier.newReadOnlyId(
		ParameterTableRowModel.PROPERTY_CREATION_DATE,
		n("reefdb.property.date.creation"),
		n("reefdb.property.date.creation"),
		Date.class);

	public static final ReefDbColumnIdentifier<ParameterTableRowModel> UPDATE_DATE = ReefDbColumnIdentifier.newReadOnlyId(
		ParameterTableRowModel.PROPERTY_UPDATE_DATE,
		n("reefdb.property.date.modification"),
		n("reefdb.property.date.modification"),
		Date.class);


	/** {@inheritDoc} */
	@Override
	public ParameterTableRowModel createNewRow() {
		return new ParameterTableRowModel();
	}

	/** {@inheritDoc} */
	@Override
	public ReefDbColumnIdentifier<ParameterTableRowModel> getFirstColumnEditing() {
		return CODE;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex, org.nuiton.jaxx.application.swing.table.ColumnIdentifier<ParameterTableRowModel> propertyName) {
		boolean editable = super.isCellEditable(rowIndex, columnIndex, propertyName);

		if (editable && ASSOCIATED_QUALITATIVE_VALUE.equals(propertyName)) {

			ParameterTableRowModel rowModel = getEntry(rowIndex);
			editable = rowModel.isQualitative();
		}

		return editable;
	}
}
