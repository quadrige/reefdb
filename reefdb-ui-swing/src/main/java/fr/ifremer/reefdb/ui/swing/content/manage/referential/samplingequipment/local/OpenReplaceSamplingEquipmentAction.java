package fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.ManageSamplingEquipmentsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local.replace.ReplaceSamplingEquipmentUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local.replace.ReplaceSamplingEquipmentUIModel;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceSamplingEquipmentAction extends AbstractReefDbAction<SamplingEquipmentsLocalUIModel, SamplingEquipmentsLocalUI, SamplingEquipmentsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceSamplingEquipmentAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.samplingequipment.local.SamplingEquipmentsLocalUIHandler} object.
     */
    public OpenReplaceSamplingEquipmentAction(SamplingEquipmentsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<SamplingEquipmentDTO, ReplaceSamplingEquipmentUIModel, ReplaceSamplingEquipmentUI> openAction =
                new AbstractOpenReplaceUIAction<SamplingEquipmentDTO, ReplaceSamplingEquipmentUIModel, ReplaceSamplingEquipmentUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.samplingEquipment");
                    }

                    @Override
                    protected ReplaceSamplingEquipmentUIModel createNewModel() {
                        return new ReplaceSamplingEquipmentUIModel();
                    }

                    @Override
                    protected ReplaceSamplingEquipmentUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceSamplingEquipmentUI(ctx);
                    }

                    @Override
                    protected List<SamplingEquipmentDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getSamplingEquipments(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected SamplingEquipmentDTO getSelectedSource() {
                        List<SamplingEquipmentDTO> selectedBeans = OpenReplaceSamplingEquipmentAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected SamplingEquipmentDTO getSelectedTarget() {
                        ManageSamplingEquipmentsUI ui = OpenReplaceSamplingEquipmentAction.this.getUI().getParentContainer(ManageSamplingEquipmentsUI.class);
                        List<SamplingEquipmentDTO> selectedBeans = ui.getSamplingEquipmentsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
