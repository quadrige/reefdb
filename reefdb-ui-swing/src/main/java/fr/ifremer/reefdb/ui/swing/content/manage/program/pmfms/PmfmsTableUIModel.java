package fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Modele pour la zone des programmes.
 */
public class PmfmsTableUIModel extends AbstractReefDbTableUIModel<PmfmStrategyDTO, PmfmsTableRowModel, PmfmsTableUIModel> {

    /** Constant <code>PROPERTY_LOADED="loaded"</code> */
    public static final String PROPERTY_LOADED = "loaded";
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";
    /** Constant <code>PROPERTY_UP_ALLOWED="upAllowed"</code> */
    public static final String PROPERTY_UP_ALLOWED = "upAllowed";
    /** Constant <code>PROPERTY_DOWN_ALLOWED="downAllowed"</code> */
    public static final String PROPERTY_DOWN_ALLOWED = "downAllowed";
    private boolean loaded;
    private boolean editable;
    private boolean upAllowed;
    private boolean downAllowed;

    /**
     * Constructor.
     */
    public PmfmsTableUIModel() {
        super();
    }

    /**
     * <p>isLoaded.</p>
     *
     * @return a boolean.
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * <p>Setter for the field <code>loaded</code>.</p>
     *
     * @param loaded a boolean.
     */
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
        firePropertyChange(PROPERTY_LOADED, null, loaded);
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, null, editable);
    }

    /**
     * <p>isUpAllowed.</p>
     *
     * @return a boolean.
     */
    public boolean isUpAllowed() {
        return upAllowed;
    }

    /**
     * <p>Setter for the field <code>upAllowed</code>.</p>
     *
     * @param upAllowed a boolean.
     */
    public void setUpAllowed(boolean upAllowed) {
        this.upAllowed = upAllowed;
        firePropertyChange(PROPERTY_UP_ALLOWED, null, upAllowed);
    }

    /**
     * <p>isDownAllowed.</p>
     *
     * @return a boolean.
     */
    public boolean isDownAllowed() {
        return downAllowed;
    }

    /**
     * <p>Setter for the field <code>downAllowed</code>.</p>
     *
     * @param downAllowed a boolean.
     */
    public void setDownAllowed(boolean downAllowed) {
        this.downAllowed = downAllowed;
        firePropertyChange(PROPERTY_DOWN_ALLOWED, null, downAllowed);
    }
}
