package fr.ifremer.reefdb.ui.swing.util.tab;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.tab.AbstractTabContainerUIHandler;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;

import static org.nuiton.i18n.I18n.t;

/**
 * UI containing a tab panel.
 * Use ReefDbTab as tab component
 *
 * @param <M>  type of the ui model
 * @param <UI> type of the screen ui
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public abstract class AbstractReefDbTabContainerUIHandler<M, UI extends ReefDbUI<M, ?>>
        extends AbstractTabContainerUIHandler<M, UI> {

    @Override
    public ReefDbConfiguration getConfig() {
        return (ReefDbConfiguration) super.getConfig();
    }

    @Override
    public ReefDbUIContext getContext() {
        return (ReefDbUIContext) super.getContext();
    }

    /**
     * get the screen title
     *
     * @return the screen title
     */
    public String getTitle() {
        return t("reefdb.screen." + getUI().getClass().getSimpleName() + ".title");
    }

}
