package fr.ifremer.reefdb.ui.swing.content.manage.rule.pmfm;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

/**
 * Model pour le tableau des controles sur les PSFM
 */
public class ControlPmfmTableUIModel extends AbstractReefDbTableUIModel<RulePmfmDTO, ControlPmfmRowModel, ControlPmfmTableUIModel> {

    private RulesUIModel parentModel;

    private boolean editable;
    /** Constant <code>PROPERTY_EDITABLE="editable"</code> */
    public static final String PROPERTY_EDITABLE = "editable";

    /**
     * Constructor.
     */
    public ControlPmfmTableUIModel() {
        super();
    }

    /**
     * <p>Getter for the field <code>parentModel</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel} object.
     */
    public RulesUIModel getParentModel() {
        return parentModel;
    }

    /**
     * <p>Setter for the field <code>parentModel</code>.</p>
     *
     * @param parentModel a {@link fr.ifremer.reefdb.ui.swing.content.manage.rule.RulesUIModel} object.
     */
    public void setParentModel(RulesUIModel parentModel) {
        this.parentModel = parentModel;
    }

    /**
     * <p>isEditable.</p>
     *
     * @return a boolean.
     */
    public boolean isEditable() {
        return editable && getParentModel().isEditable();
    }

    /**
     * <p>Setter for the field <code>editable</code>.</p>
     *
     * @param editable a boolean.
     */
    public void setEditable(boolean editable) {
        boolean oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }
}
