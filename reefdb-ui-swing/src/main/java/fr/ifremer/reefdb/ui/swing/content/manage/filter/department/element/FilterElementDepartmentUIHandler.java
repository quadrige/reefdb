package fr.ifremer.reefdb.ui.swing.content.manage.filter.department.element;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.department.menu.DepartmentMenuUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Controler.
 */
public class FilterElementDepartmentUIHandler extends AbstractFilterElementUIHandler<DepartmentDTO, FilterElementDepartmentUI, DepartmentMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(FilterElementDepartmentUIHandler.class);

    /** {@inheritDoc} */
    @Override
    protected DepartmentMenuUI createNewReferentialMenuUI() {
        return new DepartmentMenuUI(getUI());
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(FilterElementDepartmentUI ui) {

        // Force bean type on double list
        getUI().getFilterDoubleList().setBeanType(DepartmentDTO.class);

        super.afterInit(ui);

        // hide code / name
        getReferentialMenuUI().getCodeEditor().getParent().setVisible(false);
        getReferentialMenuUI().getNameEditor().getParent().setVisible(false);

    }

}
