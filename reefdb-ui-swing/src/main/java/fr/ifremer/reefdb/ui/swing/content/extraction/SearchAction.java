package fr.ifremer.reefdb.ui.swing.content.extraction;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action permettant la recherche des obervations.
 */
public class SearchAction extends AbstractCheckModelAction<ExtractionUIModel, ExtractionUI, ExtractionUIHandler> {

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final ExtractionUIHandler handler) {
        super(handler, false);
        setActionDescription(t("reefdb.action.search.tip"));
    }

    /** {@inheritDoc} */
    @Override
    protected Class<SaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getHandler();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().setModify(modelModify);
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        getUI().getExtractionsTable().getExtractionsTable().clearSelection();

        // Chargement des extractions
        getHandler().loadExtractions(getContext().getExtractionService().getExtractions(getModel().getExtractionId(), getModel().getProgramCode()));
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        return ImmutableList.of(
                getModel().getExtractionsTableUIModel(),
                getModel().getExtractionsFiltersUIModel()
        );
    }

}
