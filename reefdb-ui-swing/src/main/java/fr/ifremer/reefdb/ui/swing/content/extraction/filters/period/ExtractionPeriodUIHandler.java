package fr.ifremer.reefdb.ui.swing.content.extraction.filters.period;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import javax.swing.JComponent;
import java.awt.Component;
import java.time.LocalDate;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ExtractionPeriodUIHandler class.</p>
 *
 */
public class ExtractionPeriodUIHandler extends AbstractReefDbTableUIHandler<ExtractionPeriodRowModel, ExtractionPeriodUIModel, ExtractionPeriodUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(ExtractionPeriodUI ui) {
        super.beforeInit(ui);

        ExtractionPeriodUIModel model = new ExtractionPeriodUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(ExtractionPeriodUI ui) {
        initUI(ui);

        initTable();

        getModel().addPropertyChangeListener(ExtractionPeriodUIModel.PROPERTY_BEANS_LOADED, evt -> {
            // after periods loaded
            if (getModel().getRowCount() == 0) {
                // create a new row if empty, focus is requested by onRowsAdded
                getModel().addNewRow();
            } else {
                // focus on first row
                setFocusOnCell(getModel().getRows().get(0));
            }
        });
    }

    private void initTable() {

        TableColumnExt startDateCol = addLocalDatePickerColumnToModel(ExtractionPeriodTableModel.START_DATE, getConfig().getDateFormat());
        fixColumnWidth(startDateCol, 120);
        TableColumnExt endDateCol = addLocalDatePickerColumnToModel(ExtractionPeriodTableModel.END_DATE, getConfig().getDateFormat());
        fixColumnWidth(endDateCol, 120);

        ExtractionPeriodTableModel tableModel = new ExtractionPeriodTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        getTable().setVisibleRowCount(5);

        initTable(getTable());
    }

    /** {@inheritDoc} */
    @Override
    protected JComponent getComponentToFocus() {
        return getTable();
    }

    /** {@inheritDoc} */
    @Override
    public Component getNextComponentToFocus() {
        return getUI().getValidButton();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<ExtractionPeriodRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        if (addedRows.size() == 1) {
            ExtractionPeriodRowModel row = addedRows.get(0);
            setFocusOnCell(row);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, ExtractionPeriodRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {
        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);
        recomputeRowsValidState(false);
    }

    // remove selected rows
    void removeSelectedRows() {
        // stop cell editing first (Mantis #46661)
        getTable().removeEditor();
        getModel().deleteSelectedRows();
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(ExtractionPeriodRowModel row) {
        return /*super.isRowValid(row) &&*/ isPeriodValid(row);
    }

    private boolean isPeriodValid(ExtractionPeriodRowModel row) {

        row.getErrors().clear();

        final LocalDate startDate = row.getStartDate();
        final LocalDate endDate = row.getEndDate();

        if (startDate == null) {

            // both dates must be filled
            ReefDbBeans.addError(row, t("reefdb.extraction.period.empty.message"), ExtractionPeriodRowModel.PROPERTY_START_DATE);

        } else if (endDate == null) {

            // both dates must be filled
            ReefDbBeans.addError(row, t("reefdb.extraction.period.empty.message"), ExtractionPeriodRowModel.PROPERTY_END_DATE);

        } else if (endDate.isBefore(startDate)) {

            // end date should be after start date
            ReefDbBeans.addError(row, t("reefdb.extraction.period.invalid.message"), ExtractionPeriodRowModel.PROPERTY_END_DATE);

        }

        return row.getErrors().isEmpty();
    }

    /**
     * <p>valid.</p>
     */
    public void valid() {

        if (getModel().isValid()) {
            closeDialog();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        getModel().setValid(false);
        closeDialog();
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ExtractionPeriodRowModel> getTableModel() {
        return (ExtractionPeriodTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return getUI().getPeriodTable();
    }
}
