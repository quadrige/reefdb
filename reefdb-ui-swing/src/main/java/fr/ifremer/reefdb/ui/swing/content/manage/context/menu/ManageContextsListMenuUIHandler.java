package fr.ifremer.reefdb.ui.swing.content.manage.context.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.SwingUtilities;

/**
 * Controlleur du menu pour la gestion des lieux au niveau National
 */
public class ManageContextsListMenuUIHandler extends AbstractReefDbUIHandler<ManageContextsListMenuUIModel, ManageContextsListMenuUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ManageContextsListMenuUIHandler.class);

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ManageContextsListMenuUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ManageContextsListMenuUIModel model = new ManageContextsListMenuUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ManageContextsListMenuUI ui) {
        initUI(ui);

        // Initialize combobox
        initComboBox();
    }

    /**
     * Initialize  combobox
     */
    private void initComboBox() {

        initBeanFilterableComboBox(
                getUI().getContextsLabelsCombo(),
                getContext().getContextService().getAllContexts(),
                null);

        ReefDbUIs.forceComponentSize(getUI().getContextsLabelsCombo());

        getUI().getContextsLabelsCombo().getComboBoxModel().addWillChangeSelectedItemListener(event -> {
            if (getModel().isLoading()) return;
            if (event.getNextSelectedItem() != null) SwingUtilities.invokeLater(() -> getUI().getSearchButton().getAction().actionPerformed(null));
        });
    }

    /**
     * <p>reloadComboBox.</p>
     */
    @SuppressWarnings("unchecked")
    public void reloadComboBox() {
        BeanFilterableComboBox cb = getUI().getContextsLabelsCombo();
        cb.setData(null);
        cb.setData(getContext().getContextService().getAllContexts());
        if (cb.getCombobox().getSelectedIndex() != -1) {
            int lastSelectedIndex = cb.getCombobox().getSelectedIndex();
            cb.getCombobox().setSelectedIndex(-1);
            cb.getCombobox().setSelectedIndex(lastSelectedIndex);
        }

    }
}
