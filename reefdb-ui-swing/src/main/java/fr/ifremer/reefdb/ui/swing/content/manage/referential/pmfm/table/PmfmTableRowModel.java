package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.table;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>PmfmTableRowModel class.</p>
 *
 * @author Antoine
 */
public class PmfmTableRowModel extends AbstractReefDbRowUIModel<PmfmDTO, PmfmTableRowModel> implements PmfmDTO, ErrorAware {

    private static final Binder<PmfmDTO, PmfmTableRowModel> FROM_BEAN_BINDER =
            BinderFactory.newBinder(PmfmDTO.class, PmfmTableRowModel.class);
    
    private static final Binder<PmfmTableRowModel, PmfmDTO> TO_BEAN_BINDER =
            BinderFactory.newBinder(PmfmTableRowModel.class, PmfmDTO.class);

    private final List<ErrorDTO> errors;

    /**
     * <p>Constructor for PmfmTableRowModel.</p>
     */
    public PmfmTableRowModel() {
        super(FROM_BEAN_BINDER, TO_BEAN_BINDER);
        errors = new ArrayList<>();
    }

    /** {@inheritDoc} */
    @Override
    protected PmfmDTO newBean() {
        return ReefDbBeanFactory.newPmfmDTO();
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return delegateObject.getName();
    }

    /** {@inheritDoc} */
    @Override
    public void setName(String name) {
        delegateObject.setName(name);
    }

    /** {@inheritDoc} */
    @Override
    public StatusDTO getStatus() {
        return delegateObject.getStatus();
    }

    /** {@inheritDoc} */
    @Override
    public void setStatus(StatusDTO status) {
        delegateObject.setStatus(status);
    }

    /** {@inheritDoc} */
    @Override
    public ParameterDTO getParameter() {
        return delegateObject.getParameter();
    }

    /** {@inheritDoc} */
    @Override
    public void setParameter(ParameterDTO parameter) {
        delegateObject.setParameter(parameter);
    }

    /** {@inheritDoc} */
    @Override
    public MatrixDTO getMatrix() {
        return delegateObject.getMatrix();
    }

    /** {@inheritDoc} */
    @Override
    public void setMatrix(MatrixDTO Matrix) {
        delegateObject.setMatrix(Matrix);
    }

    @Override
    public String getComment() {
        return delegateObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        delegateObject.setComment(comment);
    }

    /** {@inheritDoc} */
    @Override
    public FractionDTO getFraction() {
        return delegateObject.getFraction();
    }

    /** {@inheritDoc} */
    @Override
    public void setFraction(FractionDTO fraction) {
        delegateObject.setFraction(fraction);
    }

    /** {@inheritDoc} */
    @Override
    public MethodDTO getMethod() {
        return delegateObject.getMethod();
    }

    /** {@inheritDoc} */
    @Override
    public void setMethod(MethodDTO method) {
        delegateObject.setMethod(method);
    }

    /** {@inheritDoc} */
    @Override
    public UnitDTO getUnit() {
        return delegateObject.getUnit();
    }

    /** {@inheritDoc} */
    @Override
    public void setUnit(UnitDTO unit) {
        delegateObject.setUnit(unit);
    }

    /** {@inheritDoc} */
    @Override
    public QualitativeValueDTO getQualitativeValues(int index) {
        return delegateObject.getQualitativeValues(index);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isQualitativeValuesEmpty() {
        return delegateObject.isQualitativeValuesEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public int sizeQualitativeValues() {
        return delegateObject.sizeQualitativeValues();
    }

    /** {@inheritDoc} */
    @Override
    public void addQualitativeValues(QualitativeValueDTO qualitativeValues) {
        delegateObject.addQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public void addAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        delegateObject.addAllQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeQualitativeValues(QualitativeValueDTO qualitativeValues) {
        return delegateObject.removeQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean removeAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        return delegateObject.removeAllQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsQualitativeValues(QualitativeValueDTO qualitativeValues) {
        return delegateObject.containsQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAllQualitativeValues(Collection<QualitativeValueDTO> qualitativeValues) {
        return delegateObject.containsAllQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualitativeValueDTO> getQualitativeValues() {
        return delegateObject.getQualitativeValues();
    }

    /** {@inheritDoc} */
    @Override
    public void setQualitativeValues(List<QualitativeValueDTO> qualitativeValues) {
        delegateObject.setQualitativeValues(qualitativeValues);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDirty() {
        return delegateObject.isDirty();
    }

    /** {@inheritDoc} */
    @Override
    public void setDirty(boolean dirty) {
        delegateObject.setDirty(dirty);
    }

    @Override
    public boolean isReadOnly() {
        return delegateObject.isReadOnly();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        delegateObject.setReadOnly(readOnly);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<ErrorDTO> getErrors() {
        return errors;
    }

    @Override
    public Date getCreationDate() {
        return delegateObject.getCreationDate();
    }

    @Override
    public void setCreationDate(Date date) {
        delegateObject.setCreationDate(date);
    }

    @Override
    public Date getUpdateDate() {
        return delegateObject.getUpdateDate();
    }

    @Override
    public void setUpdateDate(Date date) {
        delegateObject.setUpdateDate(date);
    }


}
