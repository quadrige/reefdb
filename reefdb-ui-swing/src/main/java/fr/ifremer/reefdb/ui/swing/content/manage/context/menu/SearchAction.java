package fr.ifremer.reefdb.ui.swing.content.manage.context.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.ui.swing.model.AbstractBeanUIModel;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.context.ManageContextsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.context.ManageContextsUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.context.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import java.util.List;

/**
 * <p>SearchAction class.</p>
 *
 */
public class SearchAction extends AbstractCheckModelAction<ManageContextsListMenuUIModel, ManageContextsListMenuUI, ManageContextsListMenuUIHandler> {

    private List<ContextDTO> results;

    /**
     * Constructor.
     *
     * @param handler Le controller
     */
    public SearchAction(final ManageContextsListMenuUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        Integer idLocalContext = null;
        if (getUI().getContextsLabelsCombo().getSelectedItem() instanceof ContextDTO) {
            final ContextDTO localContext = (ContextDTO) getUI().getContextsLabelsCombo().getSelectedItem();
            idLocalContext = localContext.getId();
        }

//        getUI().getParentContainer(ManageContextsUI.class).getHandler().loadContexts(idLocalContext);

        if (idLocalContext == null) {
            results = getContext().getContextService().getAllContexts();
        } else {
            results = ImmutableList.of(getContext().getContextService().getContext(idLocalContext));
        }

    }

    /** {@inheritDoc} */
    @Override
    public void postSuccessAction() {

        getModel().setResults(results);

        super.postSuccessAction();
    }

    /** {@inheritDoc} */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelModify() {
        return getParentModel() != null && getParentModel().isModify();
    }

    /** {@inheritDoc} */
    @Override
    protected void setModelModify(boolean modelModify) {
        if (getParentModel() != null) {
            getParentModel().setModify(modelModify);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isModelValid() {
        return getParentModel() == null || getParentModel().isValid();
    }

    /** {@inheritDoc} */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        return getParentUI().getHandler();
    }

    @Override
    protected List<AbstractBeanUIModel> getOtherModelsToModify() {
        if (getParentModel() != null) {
            return ImmutableList.of(getParentModel().getContextListModel());
        }
        return super.getOtherModelsToModify();
    }

    private ManageContextsUI getParentUI() {
        return getUI().getParentContainer(ManageContextsUI.class);
    }

    private ManageContextsUIModel getParentModel() {
        final ManageContextsUI ui = getParentUI();
        if (ui != null) {
            return ui.getModel();
        }
        return null;
    }

}
