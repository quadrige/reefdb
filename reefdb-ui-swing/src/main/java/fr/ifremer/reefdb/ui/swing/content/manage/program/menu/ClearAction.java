package fr.ifremer.reefdb.ui.swing.content.manage.program.menu;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.program.ProgramsUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.program.SaveAction;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

/**
 * Action permettant d effacer la recherche les mesures.
 */
public class ClearAction extends AbstractCheckModelAction<ProgramsMenuUIModel, ProgramsMenuUI, ProgramsMenuUIHandler> {

	/**
	 * Constructor.
	 *
	 * @param handler Controller
	 */
	public ClearAction(final ProgramsMenuUIHandler handler) {
		super(handler, false);
	}

	/** {@inheritDoc} */
	@Override
	public void doAction() {
		
		getModel().clear();
        // remove selected item directly because model binding can't works here
		getUI().getProgramMnemonicCombo().setSelectedItem(null);
		getUI().getProgramCodeCombo().setSelectedItem(null);

    	// Suppression des informations du contexte
    	getContext().setSelectedProgramCode(null);
    	getContext().setSelectedLocationId(null);
    	
    	// Suppression des donnees dans les tableaux
		ProgramsUI ui = getHandler().getProgramsUI();
		if (ui != null) {
			ui.getProgramsTableUI().getHandler().clearTable();
			ui.getStrategiesTableUI().getHandler().clearTable();
			ui.getLocationsTableUI().getHandler().clearTable();
			ui.getPmfmsTableUI().getHandler().clearTable();
		}
	}

	/** {@inheritDoc} */
	@Override
	protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
		return SaveAction.class;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelModify() {
        final ProgramsUIModel model = getLocalModel();
        return model != null && model.isModify();
    }

	/** {@inheritDoc} */
	@Override
	protected void setModelModify(boolean modelModify) {
		ProgramsUIModel model = getLocalModel();
		if (model != null) {
			model.setModify(modelModify);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isModelValid() {
        final ProgramsUIModel model = getLocalModel();
        return model == null || model.isValid();
    }

	/** {@inheritDoc} */
	@Override
	protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
		return getHandler().getProgramsUI().getHandler();
	}

	private ProgramsUIModel getLocalModel() {
		final ProgramsUI ui = getHandler().getProgramsUI();
		if (ui != null) {
			return ui.getModel();
		}
		return null;
	}

}
