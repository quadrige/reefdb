package fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbAction;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.ReferentialAnalysisInstrumentsUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local.replace.ReplaceAnalysisInstrumentUI;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local.replace.ReplaceAnalysisInstrumentUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.referential.replace.AbstractOpenReplaceUIAction;
import jaxx.runtime.context.JAXXInitialContext;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @since 3.6
 */
public class OpenReplaceAnalysisInstrumentAction extends AbstractReefDbAction<AnalysisInstrumentsLocalUIModel, AnalysisInstrumentsLocalUI, AnalysisInstrumentsLocalUIHandler> {

    /**
     * <p>Constructor for OpenReplaceAnalysisInstrumentAction.</p>
     *
     * @param handler a {@link fr.ifremer.reefdb.ui.swing.content.manage.referential.analysisinstruments.local.AnalysisInstrumentsLocalUIHandler} object.
     */
    public OpenReplaceAnalysisInstrumentAction(AnalysisInstrumentsLocalUIHandler handler) {
        super(handler, false);
    }

    /** {@inheritDoc} */
    @Override
    public void doAction() {

        AbstractOpenReplaceUIAction<AnalysisInstrumentDTO, ReplaceAnalysisInstrumentUIModel, ReplaceAnalysisInstrumentUI> openAction =
                new AbstractOpenReplaceUIAction<AnalysisInstrumentDTO, ReplaceAnalysisInstrumentUIModel, ReplaceAnalysisInstrumentUI>(getContext().getMainUI().getHandler()) {

                    @Override
                    protected String getEntityLabel() {
                        return t("reefdb.property.analysisInstrument");
                    }

                    @Override
                    protected ReplaceAnalysisInstrumentUIModel createNewModel() {
                        return new ReplaceAnalysisInstrumentUIModel();
                    }

                    @Override
                    protected ReplaceAnalysisInstrumentUI createUI(JAXXInitialContext ctx) {
                        return new ReplaceAnalysisInstrumentUI(ctx);
                    }

                    @Override
                    protected List<AnalysisInstrumentDTO> getReferentialList(ReferentialService referentialService) {
                        return Lists.newArrayList(referentialService.getAnalysisInstruments(StatusFilter.ACTIVE));
                    }

                    @Override
                    protected AnalysisInstrumentDTO getSelectedSource() {
                        List<AnalysisInstrumentDTO> selectedBeans = OpenReplaceAnalysisInstrumentAction.this.getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }

                    @Override
                    protected AnalysisInstrumentDTO getSelectedTarget() {
                        ReferentialAnalysisInstrumentsUI ui = OpenReplaceAnalysisInstrumentAction.this.getUI().getParentContainer(ReferentialAnalysisInstrumentsUI.class);
                        List<AnalysisInstrumentDTO> selectedBeans = ui.getReferentialAnalysisInstrumentsNationalUI().getModel().getSelectedBeans();
                        return selectedBeans.size() == 1 ? selectedBeans.get(0) : null;
                    }
                };

        getActionEngine().runFullInternalAction(openAction);
    }
}
