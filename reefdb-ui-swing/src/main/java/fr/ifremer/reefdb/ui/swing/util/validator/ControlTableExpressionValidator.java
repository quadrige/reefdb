package fr.ifremer.reefdb.ui.swing.util.validator;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ifremer.reefdb.dto.ErrorAware;
import fr.ifremer.reefdb.dto.ErrorDTO;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbRowUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;

import static org.nuiton.i18n.I18n.t;

/**
 * A customized FieldValidatorSupport that shows control messages from a AbstractReefDbTableUIModel with ErrorAware row model
 * <p/>
 * Created by Ludovic on 01/07/2015.
 */
public class ControlTableExpressionValidator extends AbstractControlExpressionValidator {

    /** {@inheritDoc} */
    @Override
    public void validate(Object object) throws ValidationException {

        Object objectValue = getFieldValue(getFieldName(), object);

        if (!(objectValue instanceof AbstractReefDbTableUIModel)) {
            return;
        }

        AbstractReefDbTableUIModel<? extends QuadrigeBean, ? extends AbstractReefDbRowUIModel, ?> model =
                (AbstractReefDbTableUIModel<? extends QuadrigeBean, ? extends AbstractReefDbRowUIModel, ?>) objectValue;

        for (AbstractReefDbRowUIModel row : model.getRows()) {
            if (row instanceof ErrorAware) {

                if (isErrorActive()) {
                    for (ErrorDTO error : ReefDbBeans.getErrors((ErrorAware) row, false)) {
                        addFieldErrorMessage(ReefDbUIs.removeHtmlTags(t("reefdb.validator.error", error.getMessage())));
                    }
                }

                if (isControlErrorActive()) {
                    for (ErrorDTO error : ReefDbBeans.getErrors((ErrorAware) row, true)) {
                        addFieldErrorMessage(ReefDbUIs.removeHtmlTags(t("reefdb.validator.error.control", error.getMessage())));
                    }
                }

                if (isWarningActive()) {
                    for (ErrorDTO warning : ReefDbBeans.getWarnings((ErrorAware) row, false)) {
                        addFieldErrorMessage(ReefDbUIs.removeHtmlTags(t("reefdb.validator.warning", warning.getMessage())));
                    }
                }

                if (isControlWarningActive()) {
                    for (ErrorDTO warning : ReefDbBeans.getWarnings((ErrorAware) row, true)) {
                        addFieldErrorMessage(ReefDbUIs.removeHtmlTags(t("reefdb.validator.warning.control", warning.getMessage())));
                    }
                }
            }
        }
    }

}

