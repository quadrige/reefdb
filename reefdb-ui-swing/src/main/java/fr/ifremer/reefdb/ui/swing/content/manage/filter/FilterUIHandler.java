package fr.ifremer.reefdb.ui.swing.content.manage.filter;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.ui.swing.action.QuitScreenAction;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.AbstractFilterElementUIHandler;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.element.FilterElementUIModel;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.list.FilterListRowModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIModel;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.SwingWorker;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller.
 */
public class FilterUIHandler extends AbstractReefDbUIHandler<FilterUIModel, FilterUI> implements CloseableUI {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(FilterUIHandler.class);

    private FilterElementsLoader filterElementsLoader;

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final FilterUI ui) {
        super.beforeInit(ui);

        // Create model and register to the JAXX context
        ui.setContextValue(new FilterUIModel());

        ui.setContextValue(SwingUtil.createActionIcon("config"));
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final FilterUI ui) {
        initUI(ui);

        // set client property here because overridden FilterUI don't add it
        ui.getFilterElementUI().putClientProperty("validatorLabel", t("reefdb.filter.selected.label"));

        // Clean observations id on context
        getContext().clearObservationPrelevementsIds();

        // List model
        getModel().setFilterListUIModel(getUI().getFilterListUI().getModel());
        listenModelModify(getModel().getFilterListUIModel());
        getModel().getFilterListUIModel().addPropertyChangeListener(AbstractReefDbBeanUIModel.PROPERTY_VALID, evt -> getValidator().doValidate());

        // Element model
        getModel().setFilterElementUIModel(getUI().getFilterElementUI().getModel());

        initListeners();

        getModel().setValid(true);
    }

    /** {@inheritDoc} */
    @Override
    public SwingValidator<FilterUIModel> getValidator() {
        return getUI().getValidator();
    }

    @SuppressWarnings("unchecked")
    private void initListeners() {

        getModel().getFilterListUIModel().addPropertyChangeListener(AbstractReefDbTableUIModel.PROPERTY_SINGLE_ROW_SELECTED, evt -> {

            if (filterElementsLoader != null && !filterElementsLoader.isDone()) {
                filterElementsLoader.cancel(true);
            }
            getModel().setSelectedFilter(null);
            getModel().getFilterElementUIModel().setLoading(true);
            filterElementsLoader = new FilterElementsLoader(getModel().getFilterListUIModel().getSingleSelectedRow());
            filterElementsLoader.execute();

        });

        getModel().getFilterElementUIModel().addPropertyChangeListener(FilterElementUIModel.PROPERTY_ELEMENTS, evt -> {
            if (getModel().getSelectedFilter() != null
                    && !getModel().getFilterElementUIModel().isLoading()
                    && !getModel().getFilterElementUIModel().isAdjusting()) {
                List<? extends QuadrigeBean> list = getUI().getFilterElementUI().getModel().getElements();
                getModel().getSelectedFilter().setElements(list != null
                        ? list.stream().filter(Objects::nonNull).collect(Collectors.toList())
                        : new ArrayList<>());
                getModel().getSelectedFilter().setFilterLoaded(true);
                getModel().getSelectedFilter().setDirty(true);
                getModel().setModify(true);
            }
            getUI().getFilterListUI().getHandler().recomputeRowsValidState();
            getValidator().doValidate();
        });

        listenValidatorValid(getValidator(), getModel());
        registerValidators(getValidator());

        // adapt filterElementUI height: use a scroll pane when UI height is too narrow (Mantis #46824)
        getUI().addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                int h = getUI().getHeight()
                        - getUI().getButtonsPanel().getPreferredSize().height
                        - getUI().getFilterListUI().getPreferredSize().height
                        - 10;
                h = Math.max(h, getUI().getFilterElementUI().getFilterElementMenuPanel().getPreferredSize().height);
                Dimension d = new Dimension(getUI().getFilterElementUI().getPreferredSize().width, h);
                getUI().getFilterElementUI().setPreferredSize(d);
                getUI().getFilterElementUI().invalidate();
            }
        });
    }

    private AbstractFilterElementUIHandler getFilterElementUIHandler() {
        return (AbstractFilterElementUIHandler) getUI().getFilterElementUI().getHandler();
    }

    /**
     * Delete filter.
     */
    public void clearFilterElements() {
        getFilterElementUIHandler().clear();
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public boolean quitUI() {
        try {
            QuitScreenAction action = new QuitScreenAction(this, false, SaveAction.class);
            if (action.prepareAction()) {
                return true;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }

    private class FilterElementsLoader extends SwingWorker<Object, Object> {

        private FilterListRowModel selectedFilter;

        public FilterElementsLoader(FilterListRowModel selectedFilter) {
            this.selectedFilter = selectedFilter;
        }

        @Override
        protected Object doInBackground() {

            // Load filter elements by service
            getContext().getContextService().loadFilteredElements(selectedFilter);

            return null;
        }

        @Override
        @SuppressWarnings("unchecked")
        protected void done() {
            try {
                if (isCancelled()) {
                    return;
                }
                try {
                    get();
                } catch (InterruptedException | ExecutionException e) {
                    throw new ReefDbTechnicalException(e.getMessage(), e);
                }

                getModel().getFilterElementUIModel().setLoading(true);

                getModel().setSelectedFilter(selectedFilter);

                // Load elements in FilterElementUI
                getFilterElementUIHandler().loadSelectedElements(selectedFilter != null
                        ? new ArrayList(selectedFilter.getElements())
                        : null);

                getValidator().doValidate();

            } finally {
                getModel().getFilterElementUIModel().setLoading(false);
            }

        }
    }
}
