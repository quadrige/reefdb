package fr.ifremer.reefdb.ui.swing.content.home.survey;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.component.coordinate.CoordinateEditor;
import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.quadrige3.ui.swing.table.editor.ExtendedComboBoxCellEditor;
import fr.ifremer.quadrige3.ui.swing.table.editor.LocalTimeCellEditor;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.ui.swing.ReefDbUIContext;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUI;
import fr.ifremer.reefdb.ui.swing.content.home.HomeUIHandler;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUI;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.StateIconCellRenderer;
import fr.ifremer.reefdb.ui.swing.util.table.renderer.SynchronizationStatusIconCellRenderer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.BorderFactory;
import javax.swing.SortOrder;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des observations.
 */
public class SurveysTableUIHandler extends
        AbstractReefDbTableUIHandler<SurveysTableRowModel, SurveysTableUIModel, SurveysTableUI> {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(SurveysTableUIHandler.class);

    private ExtendedComboBoxCellEditor<ProgramDTO> programCellEditor;
    private ExtendedComboBoxCellEditor<CampaignDTO> campaignCellEditor;
    private ExtendedComboBoxCellEditor<LocationDTO> locationCellEditor;
    private ExtendedComboBoxCellEditor<DepartmentDTO> analystCellEditor;
    private PropertyChangeListener authenticationListener;

    /** {@inheritDoc} */
    @Override
    protected String[] getRowPropertiesToIgnore() {
        return new String[]{
                SurveysTableRowModel.PROPERTY_SAMPLING_OPERATIONS,
                SurveysTableRowModel.PROPERTY_SAMPLING_OPERATIONS_LOADED,
                SurveysTableRowModel.PROPERTY_DIRTY,
                SurveysTableRowModel.PROPERTY_ERRORS,
                SurveysTableRowModel.PROPERTY_CONTROL_DATE,
                SurveysTableRowModel.PROPERTY_VALIDATION_DATE,
                SurveysTableRowModel.PROPERTY_QUALIFICATION_COMMENT,
                SurveysTableRowModel.PROPERTY_SYNCHRONIZATION_STATUS,
                SurveysTableRowModel.PROPERTY_COORDINATE
        };
    }

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final SurveysTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final SurveysTableUIModel model = new SurveysTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final SurveysTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Désactivation des boutons
        getUI().getObservationDupliquerBouton().setEnabled(false);
        getUI().getObservationEditerCombobox().setEnabled(false);
        getUI().getObservationChangerEtatCombobox().setEnabled(false);
        getUI().getObservationSupprimerBouton().setEnabled(false);

        createProgramCellEditor();
        createCampaignCellEditor();
        createLocationCellEditor();
        createAnalystCellEditor();

        // Initialisation du tableau
        initTable();

        // Initialisation de la combobox Editer
        initActionComboBox(getUI().getObservationEditerCombobox());

        // Intialisation de la combobox Changer etat
        initActionComboBox(getUI().getObservationChangerEtatCombobox());

        // Initialisation des listeners
        initListeners();

        // button border
        getUI().getNextButton().setBorder(
                BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, getConfig().getColorHighlightButtonBorder()), ui.getNextButton().getBorder())
        );
    }

    /** {@inheritDoc} */
    @Override
    protected boolean isRowValid(final SurveysTableRowModel row) {
        return (super.isRowValid(row) || !row.isEditable()) && isSurveyRowValid(row);
    }

    private boolean isSurveyRowValid(SurveysTableRowModel row) {

        // Name length
        if (StringUtils.length(row.getName()) > 50) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.mnemonic.too.long.50"), SurveysTableRowModel.PROPERTY_NAME);
        }

        // coordinates
        if (row.getLatitude() == null ^ row.getLongitude() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.coordinate.invalid"), SurveysTableRowModel.PROPERTY_LONGITUDE, SurveysTableRowModel.PROPERTY_LATITUDE);
        }

        // positioning
        if (row.getLatitude() != null && row.getLongitude() != null && row.getPositioning() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.positioning.required"), SurveysTableRowModel.PROPERTY_POSITIONING);
        }

        // dummy error if operations invalid
        if (!row.isOperationsValid()) {
            ReefDbBeans.addError(row, t("reefdb.home.survey.table.operationsInvalid"));
        }

        // depth analyst
        boolean hasDepthAnalystError = false;
        if (row.getDepth() != null && row.getDepthAnalyst() == null) {
            ReefDbBeans.addError(row, t("reefdb.validator.error.analyst.required"), SurveysTableRowModel.PROPERTY_DEPTH_ANALYST);
            hasDepthAnalystError = true;
        }

        boolean hasNoError = ReefDbBeans.hasNoBlockingError(row);

        if (!hasNoError) {
            ensureColumnsWithErrorAreVisible(row);
            if (hasDepthAnalystError)
                moveColumnAfter(SurveysTableModel.DEPTH_ANALYST, SurveysTableModel.DEPTH);
        }

        return hasNoError;
    }

    /**
     * Initialisation du tableau.
     */
    private void initTable() {

        // Program cell editor
        TableColumnExt programmeCol = addColumn(
                programCellEditor,
                newTableCellRender(SurveysTableModel.PROGRAM),
                SurveysTableModel.PROGRAM);
        programmeCol.setSortable(true);
        programmeCol.setPreferredWidth(200);

        // Campaign
        TableColumnExt campaignCol = addColumn(
                campaignCellEditor,
                newTableCellRender(SurveysTableModel.CAMPAIGN),
                SurveysTableModel.CAMPAIGN);
        campaignCol.setSortable(true);
        campaignCol.setPreferredWidth(200);

        // lieu
        TableColumnExt lieuCol = addColumn(
                locationCellEditor,
                newTableCellRender(SurveysTableModel.LOCATION),
                SurveysTableModel.LOCATION);
        lieuCol.setSortable(true);
        lieuCol.setPreferredWidth(200);

        // Date
        TableColumnExt dateCol = addLocalDatePickerColumnToModel(SurveysTableModel.DATE, getConfig().getDateFormat());
        dateCol.setSortable(true);
        dateCol.setPreferredWidth(50);

        // Profondeur
        TableColumnExt profondeurCol = addExtendedComboDataColumnToModel(
                SurveysTableModel.DEPTH,
                getContext().getReferentialService().getDepths(),
                false);
        profondeurCol.setSortable(true);
        profondeurCol.setPreferredWidth(100);

        // depth analyst column (Mantis #47991)
        TableColumnExt depthAnalystCol = addColumn(
                analystCellEditor,
                newTableCellRender(SurveysTableModel.DEPTH_ANALYST),
                SurveysTableModel.DEPTH_ANALYST);
        depthAnalystCol.setSortable(true);
        depthAnalystCol.setPreferredWidth(200);

        // Commentaire
        final TableColumnExt colonneCommentaire = addCommentColumn(SurveysTableModel.COMMENT);
        colonneCommentaire.setSortable(false);

        // Partage
        TableColumnExt partageCol = addColumn(
                null,
                new SynchronizationStatusIconCellRenderer(getContext()),
                SurveysTableModel.SYNCHRONIZATION_STATUS);
        partageCol.setSortable(true);
        partageCol.setPreferredWidth(20);

        // Etat
        TableColumnExt etatCol = addColumn(
                null,
                new StateIconCellRenderer(getContext()),
                SurveysTableModel.STATE);
        etatCol.setSortable(true);
//        addEtatObservationHighlighter(getTable(), SurveysTableModel.STATE);
        etatCol.setPreferredWidth(100);

        // Profondeur precise
        TableColumnExt profondeurPreciseCol = addColumn(SurveysTableModel.PRECISE_DEPTH);
        profondeurPreciseCol.setCellEditor(newNumberCellEditor(Double.class, false, ReefDbUI.DECIMAL2_PATTERN));
        profondeurPreciseCol.setCellRenderer(newNumberCellRenderer(2));
        profondeurPreciseCol.setSortable(true);
        profondeurPreciseCol.setPreferredWidth(100);

        // Latitude
        TableColumnExt latitudeCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LATITUDE_MIN,
                SurveysTableModel.LATITUDE);
        latitudeCol.setSortable(true);
        latitudeCol.setPreferredWidth(100);

        // longitude
        TableColumnExt longitudeCol = addCoordinateColumnToModel(
                CoordinateEditor.CoordinateType.LONGITUDE_MIN,
                SurveysTableModel.LONGITUDE);
        longitudeCol.setSortable(true);
        longitudeCol.setPreferredWidth(100);

        // positionnement
        TableColumnExt positionnementLabelCol = addExtendedComboDataColumnToModel(
                SurveysTableModel.POSITIONING,
                getContext().getReferentialService().getPositioningSystems(),
                false);
        positionnementLabelCol.setSortable(true);
        positionnementLabelCol.setPreferredWidth(100);

        // positionnement
        TableColumnExt positionnementPrecisionCol = addColumn(
                SurveysTableModel.POSITIONING_PRECISION);
        positionnementPrecisionCol.setSortable(true);
        positionnementPrecisionCol.setPreferredWidth(100);
        positionnementPrecisionCol.setEditable(false);

        // mnemonique
        TableColumnExt mnemoniqueCol = addColumn(SurveysTableModel.NAME);
        mnemoniqueCol.setSortable(true);
        mnemoniqueCol.setPreferredWidth(100);

        // Service Saisisseur
        TableColumnExt serviceSaisisseurCol = addColumn(
                null, //departmentCellEditor,
                newTableCellRender(SurveysTableModel.DEPARTMENT),
                SurveysTableModel.DEPARTMENT
        );
        serviceSaisisseurCol.setSortable(true);
        serviceSaisisseurCol.setPreferredWidth(200);
        serviceSaisisseurCol.setEditable(false);

        // Colonne Heure
        final TableColumnExt heureDuPassageCol = addColumn(
                new LocalTimeCellEditor(),
                newTableCellRender(Integer.class, DecoratorService.TIME_IN_HOURS_MINUTES),
                SurveysTableModel.TIME);
        heureDuPassageCol.setMaxWidth(100);
        heureDuPassageCol.setWidth(100);
        heureDuPassageCol.setSortable(true);

        // Date Modif
        TableColumnExt dateModifCol = addDatePickerColumnToModel(SurveysTableModel.UPDATE_DATE, getConfig().getDateFormat());
        dateModifCol.setSortable(true);
        dateModifCol.setPreferredWidth(50);

        // Date Control
        TableColumnExt dateControlCol = addDatePickerColumnToModel(SurveysTableModel.CONTROL_DATE, getConfig().getDateFormat());
        dateControlCol.setSortable(true);
        dateControlCol.setPreferredWidth(50);

        // Date Valid
        TableColumnExt dateValidCol = addDatePickerColumnToModel(SurveysTableModel.VALIDATION_DATE, getConfig().getDateFormat());
        dateValidCol.setSortable(true);
        dateValidCol.setPreferredWidth(50);

        // Commentaire Valid
        final TableColumnExt colonneCommentaireValid = addCommentColumn(
                SurveysTableModel.VALIDATION_COMMENT,
                SurveysTableRowModel.PROPERTY_VALIDATION_COMMENT,
                false);
        colonneCommentaireValid.setSortable(false);

        SurveysTableModel tableModel = new SurveysTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Colonnes non editable
        tableModel.setNoneEditableCols(
                SurveysTableModel.SYNCHRONIZATION_STATUS,
                SurveysTableModel.STATE,
                SurveysTableModel.UPDATE_DATE,
                SurveysTableModel.CONTROL_DATE,
                SurveysTableModel.VALIDATION_DATE);

        // Les colonnes obligatoire sont toujours presentes
        dateCol.setHideable(false);
        programmeCol.setHideable(false);

        // Initialisation du tableau
        initTable(getTable());

        // Les colonnes optionnelles sont invisibles
        serviceSaisisseurCol.setVisible(false);
        profondeurPreciseCol.setVisible(false);
        longitudeCol.setVisible(false);
        positionnementLabelCol.setVisible(false);
        positionnementPrecisionCol.setVisible(false);
        mnemoniqueCol.setVisible(false);
        heureDuPassageCol.setVisible(false);
        dateModifCol.setVisible(false);
        dateControlCol.setVisible(false);
        latitudeCol.setVisible(false);
        dateValidCol.setVisible(false);
        colonneCommentaireValid.setVisible(false);

        // Tri par defaut
        getTable().setSortOrder(SurveysTableModel.LOCATION, SortOrder.ASCENDING);

        // border
        addEditionPanelBorder();

    }

    private void createProgramCellEditor() {

        programCellEditor = newExtendedComboBoxCellEditor(null, ProgramDTO.class, false);

        programCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter programs
            updateProgramCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateProgramCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateProgramCellEditor(row, forceNoFilter, true);
    }

    private void updateProgramCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46727)
        if (forceNoFilter) row.setForceNoProgramFilter(true);
        if (row.isForceNoProgramFilter()) forceNoFilter = true;

        programCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.PROGRAM));

        List<ProgramDTO> availablePrograms =
                getContext().getObservationService().getAvailablePrograms(
                        null, // no campaign filter row.getCampaign() == null ? null : row.getCampaign().getId(),
                        row.getLocation() == null ? null : row.getLocation().getId(),
                        row.getDate(), forceNoFilter, true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        if (canResetValue && row.isEditable() && row.getProgram() != null && !availablePrograms.contains(row.getProgram())) {
            row.setProgram(null);
        }
        programCellEditor.getCombo().setData(availablePrograms);

    }

    private void createCampaignCellEditor() {

        campaignCellEditor = newExtendedComboBoxCellEditor(null, CampaignDTO.class, false);

        campaignCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter campaigns
            updateCampaignCellEditor(getModel().getSingleSelectedRow(), true);
            getTable().requestFocus();
        });

    }

    private void updateCampaignCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateCampaignCellEditor(row, forceNoFilter, true);
    }

    private void updateCampaignCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46655)
        if (forceNoFilter) row.setForceNoCampaignFilter(true);
        if (row.isForceNoCampaignFilter()) forceNoFilter = true;

        campaignCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.CAMPAIGN));

        List<CampaignDTO> availableCampaigns =
                getContext().getObservationService().getAvailableCampaigns(row.getDate(), forceNoFilter);

        // Avoid Campaign/Date incompatibility
        if (canResetValue && row.isEditable() && row.getCampaign() != null && !availableCampaigns.contains(row.getCampaign())) {
            row.setCampaign(null);
        }

        campaignCellEditor.getCombo().setData(availableCampaigns);

    }

    private void createLocationCellEditor() {

        locationCellEditor = newExtendedComboBoxCellEditor(null, LocationDTO.class, false);

        locationCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter location
            updateLocationCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateLocationCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateLocationCellEditor(row, forceNoFilter, true);
    }

    private void updateLocationCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46727)
        if (forceNoFilter) row.setForceNoLocationFilter(true);
        if (row.isForceNoLocationFilter()) forceNoFilter = true;

        locationCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.LOCATION));

        List<LocationDTO> availableLocations =
                getContext().getObservationService().getAvailableLocations(
                        null, // pas de filtrage sur les campagnes row.getCampaign() == null ? null : row.getCampaign().getId(),
                        row.getProgram() == null ? null : row.getProgram().getCode(),
                        forceNoFilter, true);

        // Mantis #0027340 & #0027282 : Avoid Program/Location/Date incompatibility
        if (canResetValue && row.isEditable() && row.getLocation() != null && !availableLocations.contains(row.getLocation())) {
            row.setLocation(null);
        }
        locationCellEditor.getCombo().setData(availableLocations);
    }

    private void createAnalystCellEditor() {

        analystCellEditor = newExtendedComboBoxCellEditor(null, DepartmentDTO.class, false);

        analystCellEditor.setAction("unfilter", "reefdb.common.unfilter", e -> {
            if (!askBefore(t("reefdb.common.unfilter"), t("reefdb.common.unfilter.confirmation"))) {
                return;
            }
            // unfilter departments
            updateAnalystCellEditor(getModel().getSingleSelectedRow(), true);
        });

    }

    private void updateAnalystCellEditor(SurveysTableRowModel row, boolean forceNoFilter) {

        updateAnalystCellEditor(row, forceNoFilter, true);
    }

    private void updateAnalystCellEditor(SurveysTableRowModel row, boolean forceNoFilter, boolean canResetValue) {

        // Keep the forceNoFilter on this row (Mantis #46727)
        if (forceNoFilter) row.setForceNoAnalystFilter(true);
        if (row.isForceNoAnalystFilter()) forceNoFilter = true;

        analystCellEditor.getCombo().setActionEnabled(!forceNoFilter && getContext().getDataContext().isContextFiltered(FilterTypeValues.DEPARTMENT));

        List<DepartmentDTO> availableAnalysts = getContext().getObservationService().getAvailableDepartments(forceNoFilter);

        analystCellEditor.getCombo().setData(availableAnalysts);
    }

    private void initListeners() {

        // listen to this JTable property
        getTable().addPropertyChangeListener("tableCellEditor", evt -> {
            if (evt.getNewValue() != null) {

                // a table edition begins
                final SurveysTableRowModel survey = getModel().getSingleSelectedRow();

                // update cell editors because its too long
                if (survey != null) {
                    updateProgramCellEditor(survey, false, false);
                    updateCampaignCellEditor(survey, false, false);
                    updateLocationCellEditor(survey, false, false);
                    updateAnalystCellEditor(survey, false, false);
                }

            }
        });

        // add special listener if the privileges of the authenticated user have changed (see Mantis #38841)
        authenticationListener = evt -> enablePrivilegedControls();
        // Change access to these function if privilege changed
        getContext().addPropertyChangeListener(ReefDbUIContext.PROPERTY_AUTHENTICATION_TOOLTIPTEXT, authenticationListener);
        getModel().addPropertyChangeListener(SurveysTableUIModel.PROPERTY_SELECTED_ROWS, authenticationListener);
    }

    private void enablePrivilegedControls() {
        // enable or not these controls depending on user privileges
        List<SurveyDTO> selectedSurveys = getModel().getSelectedBeans();
        Set<ProgramDTO> selectedPrograms = selectedSurveys.stream()
            .map(SurveyDTO::getProgram)
            .filter(Objects::nonNull)
            .collect(Collectors.toSet());
        int userId = getContext().getDataContext().getRecorderPersonId();
        int departmentId = getContext().getDataContext().getRecorderDepartmentId();

        boolean isManager = getContext().isAuthenticatedAsNationalAdmin() || selectedPrograms.stream().allMatch(program -> ReefDbBeans.isProgramManager(program, userId, departmentId));
        boolean isValidator = isManager || selectedPrograms.stream().allMatch(program -> ReefDbBeans.isProgramValidator(program, userId, departmentId));
        boolean isProgramRecorder = isManager || isValidator || selectedPrograms.stream().allMatch(program -> ReefDbBeans.isProgramRecorder(program, userId, departmentId));
        boolean isSurveyRecorder = isManager || isValidator || selectedSurveys.stream().allMatch(survey -> getContext().getDataContext().isSurveyEditable(survey));

        getUI().getObservationNouveauBouton().setEnabled(CollectionUtils.isNotEmpty(
            getContext().getObservationService().getAvailablePrograms(null, null, null, false, true)
        ));
        getUI().getObservationDupliquerBouton().setEnabled(getModel().getSelectedRows().size() == 1 && isProgramRecorder);
        getUI().getObservationSupprimerBouton().setEnabled(!getModel().getSelectedRows().isEmpty() && isSurveyRecorder);
        getUI().getObservationControlerBouton().setEnabled(isSurveyRecorder);
        getUI().getObservationValiderBouton().setEnabled(isValidator);
        getUI().getObservationDeValiderBouton().setEnabled(isValidator);
    }

    @Override
    public void onCloseUI() {

        // remove special listener to prevent memory leaks (Mantis #48757)
        getContext().removePropertyChangeListener(ReefDbUIContext.PROPERTY_AUTHENTICATION_TOOLTIPTEXT, authenticationListener);

        super.onCloseUI();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowModified(int rowIndex, SurveysTableRowModel row, String propertyName, Integer propertyIndex, Object oldValue, Object newValue) {

        // Set row dirty first
        row.setDirty(true);

        super.onRowModified(rowIndex, row, propertyName, propertyIndex, oldValue, newValue);

        // Pas de filtrage sur les campagnes
//        if (SurveysTableRowModel.PROPERTY_CAMPAIGN.equals(propertyName)) {
//
//            updateProgramCellEditor(row, false);
//            updateLocationCellEditor(row, false);
//
//        }

        // Listen to PROGRAM modification to load sampling operations table
        if (SurveysTableRowModel.PROPERTY_PROGRAM.equals(propertyName)) {

            updateLocationCellEditor(row, false);

            // use context variable to trigger PROPERTY_SELECTED_SURVEY event
            selectSurvey(row);
        }

        // Listen to LOCATION and DATE modification to load available programs and sampling operations table
        if (SurveysTableRowModel.PROPERTY_LOCATION.equals(propertyName) || SurveysTableRowModel.PROPERTY_DATE.equals(propertyName)) {

            updateProgramCellEditor(row, false);

            if (SurveysTableRowModel.PROPERTY_DATE.equals(propertyName)) {
                updateCampaignCellEditor(row, false);
            }

            // use context variable to trigger PROPERTY_SELECTED_SURVEY event
            selectSurvey(row);
        }

        // Listen to latitude or longitude to clear positioning when both are null
        if (SurveysTableRowModel.PROPERTY_LATITUDE.equals(propertyName) || SurveysTableRowModel.PROPERTY_LONGITUDE.equals(propertyName)) {

            if (row.getLatitude() == null && row.getLongitude() == null) {
                row.setPositioning(null);
                getTable().repaint();
            }
        }

        if (SurveysTableRowModel.PROPERTY_DEPTH.equals(propertyName)) {

            if (row.getDepth() == null) {
                // clear analyst
                row.setDepthAnalyst(null);

            } else {
                // set analyst visible and try to get default (Mantis #47991)
                getTable().getColumnExt(SurveysTableModel.DEPTH_ANALYST).setVisible(true);
                if (row.getDepthAnalyst() == null) {
                    PmfmDTO depthValuesPmfm = getContext().getReferentialService().getPmfm(getConfig().getDepthValuesPmfmId());
                    if (depthValuesPmfm != null) {
                        row.setDepthAnalyst(getContext().getProgramStrategyService().getAnalysisDepartmentOfAppliedStrategyBySurvey(
                                row,
                                Collections.singletonList(depthValuesPmfm)
                        ));
                    }
                }
            }
        }

        forceRevalidateModel();
    }

    private void selectSurvey(SurveysTableRowModel survey) {

        getModel().getMainUIModel().setSelectedSurvey(survey);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<SurveysTableRowModel> getTableModel() {
        return (SurveysTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getSurveyTable();
    }

    /** {@inheritDoc} */
    @Override
    protected void onRowsAdded(List<SurveysTableRowModel> addedRows) {
        super.onRowsAdded(addedRows);

        // should be only one row
        if (addedRows.size() == 1) {
            SurveysTableRowModel row = addedRows.get(0);

            // Ajout d'un mode de partage local pour une ligne ajoutee en local
            row.setSynchronizationStatus(getContext().getSystemService().getLocalShare());

            // Add actual department of current user
            Integer recorderDepId = getContext().getDataContext().getRecorderDepartmentId();
            if (recorderDepId != null) {
                row.setRecorderDepartment(getContext().getReferentialService().getDepartmentById(recorderDepId));
            }

            // Current user is recorder
            row.setCurrentUserIsRecorder(true);

            // Ajouter le focus sur la cellule de la ligne cree
            setFocusOnCell(row);
        }
    }

    /**
     * <p>onNext.</p>
     */
    public void onNext() {

        if (getTable().isEditing()) {
            getTable().getCellEditor().stopCellEditing();
        }

        getHomeUI().getOperationsTable().getHandler().addSamplingOperations();

    }

    public void beforeLoad() {
        uninstallSaveTableStateListener();
    }

    public void afterLoad() {
        restoreTableState();
        installSaveTableStateListener();

        // Determine if current user is allowed to edit survey
        getModel().getRows().forEach(row -> {
            boolean userIsRecorder = getContext().getDataContext().isSurveyEditable(row);
            row.setCurrentUserIsRecorder(userIsRecorder);
        });

    }

    public HomeUIHandler getHomeUIHandler() {
        return getHomeUI().getHandler();
    }

    private HomeUI getHomeUI() {
        return getParentContainer(HomeUI.class);
    }
}
