package fr.ifremer.reefdb.ui.swing.content.manage.rule.department;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.swing.table.SwingTable;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.ui.swing.content.manage.filter.select.SelectFilterUI;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbBeanUIModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableModel;
import fr.ifremer.reefdb.ui.swing.util.table.AbstractReefDbTableUIHandler;
import org.jdesktop.swingx.table.TableColumnExt;

import java.awt.Dimension;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Controller pour le tableau des services.
 */
public class ControlDepartmentTableUIHandler extends
        AbstractReefDbTableUIHandler<ControlDepartmentTableRowModel, ControlDepartmentTableUIModel, ControlDepartmentTableUI> {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(final ControlDepartmentTableUI ui) {
        super.beforeInit(ui);

        // create model and register to the JAXX context
        final ControlDepartmentTableUIModel model = new ControlDepartmentTableUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(final ControlDepartmentTableUI ui) {

        // Initialisation de l ecran
        initUI(ui);

        // Desactivation des boutons
        getUI().getAddDepartmentButton().setEnabled(false);
        getUI().getRemoveDepartmentButton().setEnabled(false);

        // Initialisation du tableau
        initialisationTableau();

    }

    /**
     * Initialisation du tableau.
     */
    private void initialisationTableau() {

        // Code
        TableColumnExt codeCol = addColumn(ControlDepartmentTableModel.CODE);
        codeCol.setSortable(true);
        codeCol.setEditable(false);

        // name
        TableColumnExt nameCol = addColumn(ControlDepartmentTableModel.NAME);
        nameCol.setSortable(true);
        nameCol.setEditable(false);

        ControlDepartmentTableModel tableModel = new ControlDepartmentTableModel(getTable().getColumnModel());
        getTable().setModel(tableModel);

        // Initialisation du tableau
        initTable(getTable());

        // Number rows visible
        getTable().setVisibleRowCount(3);
    }

    /**
     * <p>loadDepartments.</p>
     *
     * @param departments a {@link java.util.Collection} object.
     */
    public void loadDepartments(final Collection<DepartmentDTO> departments) {

        // Load les servicesControle dans le model
        getModel().setBeans(departments);
        getTable().setEditable(getModel().getParentModel().isEditable());

        // Activation du bouton nouveau
        getUI().applyDataBinding(ControlDepartmentTableUI.BINDING_ADD_DEPARTMENT_BUTTON_ENABLED);

        recomputeRowsValidState(false);
        getModel().setModify(false);
    }

    /**
     * <p>clearTable.</p>
     */
    public void clearTable() {

        // Suppression des servicesControle dans le model
        getModel().setBeans(null);

        // Desactivation du bouton nouveau
        getUI().getAddDepartmentButton().setEnabled(false);
    }

    /** {@inheritDoc} */
    @Override
    public AbstractReefDbTableModel<ControlDepartmentTableRowModel> getTableModel() {
        return (ControlDepartmentTableModel) getTable().getModel();
    }

    /** {@inheritDoc} */
    @Override
    public SwingTable getTable() {
        return ui.getControlDepartmentTable();
    }

    /**
     * <p>openAddDialog.</p>
     */
    void openAddDialog() {
        SelectFilterUI dialog = new SelectFilterUI(getContext(), FilterTypeValues.DEPARTMENT.getFilterTypeId());
        dialog.setTitle(t("reefdb.filter.department.addDialog.title"));
        List<DepartmentDTO> departments = getModel().getBeans().stream()
                .sorted(getDecorator(DepartmentDTO.class, null).getCurrentComparator())
                // set existing referential as read only to 'fix' these beans on double list
                .peek(department -> department.setReadOnly(true))
                .collect(Collectors.toList());

        dialog.getModel().setSelectedElements(departments);

        if (!getModel().getParentModel().getSelectedRuleList().isLocal()) {
            // filter only national programs if rule list is national
            dialog.getHandler().getFilterElementUIHandler().forceLocal(false);
        }

        openDialog(dialog, new Dimension(1024, 720));

        if (dialog.getModel().isValid()) {

            List<DepartmentDTO> newDepartments = dialog.getModel().getSelectedElements().stream()
                    .map(element -> ((DepartmentDTO) element))
                    .filter(newDepartment -> !getModel().getBeans().contains(newDepartment))
                    .collect(Collectors.toList());

            if (!newDepartments.isEmpty()) {
                getModel().addBeans(newDepartments);
                getModel().setModify(true);
                saveToParentModel();
            }
        }
    }

    /**
     * <p>removeDepartments.</p>
     */
    void removeDepartments() {
        getModel().deleteSelectedRows();
        saveToParentModel();
    }

    private void saveToParentModel() {
        getModel().getParentModel().getSelectedRuleList().setDepartments(getModel().getBeans());
        recomputeRowsValidState(false);

        getModel().firePropertyChanged(AbstractReefDbBeanUIModel.PROPERTY_MODIFY, null, true);

    }

}
