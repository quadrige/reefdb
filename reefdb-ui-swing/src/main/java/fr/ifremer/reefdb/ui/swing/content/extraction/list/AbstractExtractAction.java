package fr.ifremer.reefdb.ui.swing.content.extraction.list;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.enums.ExtractionOutputType;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import fr.ifremer.reefdb.ui.swing.action.AbstractCheckModelAction;
import fr.ifremer.reefdb.ui.swing.action.AbstractReefDbSaveAction;
import fr.ifremer.reefdb.ui.swing.content.extraction.ExtractionUI;
import fr.ifremer.reefdb.ui.swing.content.extraction.SaveAction;
import fr.ifremer.reefdb.ui.swing.util.ReefDbUIs;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.util.DateUtil;

import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Abstract extract action
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public abstract class AbstractExtractAction extends AbstractCheckModelAction<ExtractionsTableUIModel, ExtractionsTableUI, ExtractionsTableUIHandler> {

    private static final Log LOG = LogFactory.getLog(AbstractExtractAction.class);

    private File outputFile;

    /**
     * Constructor.
     *
     * @param handler Handler
     */
    protected AbstractExtractAction(ExtractionsTableUIHandler handler) {
        super(handler, true);
    }

    /**
     * <p>getOutputType.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.enums.ExtractionOutputType} object.
     */
    protected abstract ExtractionOutputType getOutputType();

    /**
     * <p>Getter for the field <code>outputFile</code>.</p>
     *
     * @return a {@link java.io.File} object.
     */
    public File getOutputFile() {
        return outputFile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<? extends AbstractReefDbSaveAction> getSaveActionClass() {
        return SaveAction.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelModify() {
        return getModel().getExtractionUIModel().isModify();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setModelModify(boolean modelModify) {
        getModel().getExtractionUIModel().setModify(modelModify);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isModelValid() {
        return getModel().getExtractionUIModel().isValid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractApplicationUIHandler<?, ?> getSaveHandler() {
        final ExtractionUI extractionUI = getUI().getParentContainer(ExtractionUI.class);
        return extractionUI.getHandler();
    }

    private ExtractionDTO getSelectedExtraction() {
        return getModel().getSelectedRows().size() != 1 ? null : getModel().getSelectedRows().iterator().next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean prepareAction() throws Exception {
        if (!super.prepareAction()) {
            return false;
        }

        if (getSelectedExtraction() == null) {
            LOG.warn("no selected extraction");
            return false;
        }

        if (getOutputType() == null) {
            LOG.error("no ExtractionOutputType");
            return false;
        }
        if (getOutputType().isExternal()) {
            LOG.error("wrong ExtractionOutputType (should be not external");
            return false;
        }

        // Check potential data under moratorium
        if (getContext().getProgramStrategyService().hasPotentialMoratoriums(
            ReefDbBeans.getFilterElementsIds(getSelectedExtraction(), ExtractionFilterValues.PROGRAM),
            ReefDbBeans.getExtractionPeriods(getSelectedExtraction())
        )) {
            getContext().getDialogHelper().showWarningDialog(t("reefdb.action.extract.potentialMoratorium"), t("reefdb.action.extract.title", getOutputType().getLabel()));
        }

        String date = DateUtil.formatDate(new Date(), "yyyy-MM-dd-HHmmss");
        String fileName = String.format("%s-%s-%s-%s",
                getConfig().getExtractionFilePrefix(),
                getOutputType().getLabel(),
                getSelectedExtraction().getName(),
                date);
        String fileExtension = getConfig().getExtractionFileExtension();

        createProgressionUIModel();

        // ask user file where to export db
        outputFile = saveFile(
                fileName,
                fileExtension,
                t("reefdb.extraction.choose.file.title"),
                t("reefdb.extraction.choose.file.buttonLabel"),
                "^.*\\." + fileExtension,
                t("reefdb.common.file." + fileExtension));

        if (outputFile == null) {
            return false;
        }

        // Try open a stream to verify file availability
        try {
            OutputStream out = FileUtils.openOutputStream(outputFile);
            IOUtils.closeQuietly(out);
        } catch (IOException e) {
            throw new ReefDbBusinessException(e.getLocalizedMessage());
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAction() {

        getContext().getExtractionPerformService().performExtraction(getSelectedExtraction(), getOutputType(), outputFile, getProgressionUIModel());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void postSuccessAction() {

        String text = t("reefdb.action.extract.done",
                decorate(getSelectedExtraction()),
                getOutputType().getLabel(),
                getOutputFile().getAbsoluteFile().toURI(),
                getOutputFile().getAbsolutePath());

        JEditorPane editorPane = new JEditorPane("text/html", text);
        editorPane.setEditable(false);
        editorPane.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                if (LOG.isInfoEnabled()) {
                    LOG.info("open url: " + url);
                }
                ReefDbUIs.openLink(url);
            }
        });

        getContext().getDialogHelper().showOptionDialog(
                getUI(),
                editorPane,
                t("reefdb.action.extract.title", getOutputType().getLabel()),
                JOptionPane.INFORMATION_MESSAGE,
                JOptionPane.DEFAULT_OPTION);

        super.postSuccessAction();
    }

    @Override
    public void postFailedAction(Throwable error) {

        // Delete empty file (Mantis #51225)
        if (getOutputFile() != null && getOutputFile().exists() && getOutputFile().length() == 0) {
            getOutputFile().delete();
        }

        super.postFailedAction(error);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void releaseAction() {
        outputFile = null;
        super.releaseAction();
    }
}
