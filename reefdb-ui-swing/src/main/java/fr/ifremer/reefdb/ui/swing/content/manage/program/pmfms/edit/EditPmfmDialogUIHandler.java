package fr.ifremer.reefdb.ui.swing.content.manage.program.pmfms.edit;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>EditPmfmDialogUIHandler class.</p>
 *
 */
public class EditPmfmDialogUIHandler extends AbstractReefDbUIHandler<EditPmfmDialogUIModel, EditPmfmDialogUI> implements Cancelable {

    /** {@inheritDoc} */
    @Override
    public void beforeInit(EditPmfmDialogUI ui) {
        super.beforeInit(ui);

        EditPmfmDialogUIModel model = new EditPmfmDialogUIModel();
        ui.setContextValue(model);
    }

    /** {@inheritDoc} */
    @Override
    public void afterInit(EditPmfmDialogUI editPmfmDialogUI) {
        initUI(ui);

        initBeanFilterableComboBox(
                getUI().getAnalysteCombo(),
                getContext().getReferentialService().getDepartments(),
                null);

        getModel().addPropertyChangeListener(EditPmfmDialogUIModel.PROPERTY_TABLE_MODEL, evt -> {

            // initialize ui with selected pmfm strategy
            PmfmStrategyDTO pmfmStrategy = getModel().getTableModel().getSingleSelectedRow() != null ? getModel().getTableModel().getSingleSelectedRow().toBean() : null;
            getModel().fromBean(pmfmStrategy);
        });
    }


    /**
     * <p>valid.</p>
     */
    public void valid() {

        // check analyst
        if (getModel().getAnalysisDepartment() == null) {
            getContext().getDialogHelper().showErrorDialog(
                    getUI(),
                    t("reefdb.program.pmfm.edit.error.analyst.message"),
                    t("reefdb.program.pmfm.edit.error.titre"));

            return;
        }

        // set properties to all selected pmfm strategies
        for (final PmfmStrategyDTO psfmProgStra : getModel().getTableModel().getSelectedRows()) {
            psfmProgStra.setAnalysisDepartment(getModel().getAnalysisDepartment());
            psfmProgStra.setSurvey(getModel().isSurvey());
            psfmProgStra.setSampling(getModel().isSampling());
            psfmProgStra.setGrouping(getModel().isGrouping());
            psfmProgStra.setUnique(getModel().isUnique());
        }

        // Quitter la dialogue
        closeDialog();
    }

    /**
     * <p>forceIsLocal.</p>
     *
     * @param local a {@link java.lang.Boolean} object.
     */
    public void forceIsLocal(Boolean local) {
        StatusFilter statusFilter = StatusFilter.toActiveLocalOrNational(local);
        getUI().getAnalysteCombo().setData(getContext().getReferentialService().getDepartments(statusFilter));
    }

    /** {@inheritDoc} */
    @Override
    public void cancel() {
        closeDialog();
    }
}
