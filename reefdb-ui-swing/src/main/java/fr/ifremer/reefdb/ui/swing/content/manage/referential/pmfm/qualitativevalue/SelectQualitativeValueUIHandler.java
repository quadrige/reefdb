package fr.ifremer.reefdb.ui.swing.content.manage.referential.pmfm.qualitativevalue;

/*-
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.ui.swing.util.AbstractReefDbUIHandler;
import org.nuiton.jaxx.application.swing.util.Cancelable;

import java.util.ArrayList;

/**
 * @author peck7 on 24/10/2017.
 */
public class SelectQualitativeValueUIHandler extends AbstractReefDbUIHandler<SelectQualitativeValueUIModel, SelectQualitativeValueUI> implements Cancelable {

    @Override
    public void beforeInit(SelectQualitativeValueUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(new SelectQualitativeValueUIModel());
    }

    @Override
    public void afterInit(SelectQualitativeValueUI ui) {

        initUI(ui);

        initBeanList(ui.getAssociatedQualitativeValuesDoubleList(), null,null);

        getModel().addPropertyChangeListener(SelectQualitativeValueUIModel.PROPERTY_AVAILABLE_LIST,
                evt -> getUI().getAssociatedQualitativeValuesDoubleList().getModel().setUniverse(getModel().getAvailableList()));

        getModel().addPropertyChangeListener(SelectQualitativeValueUIModel.PROPERTY_SELECTED_LIST,
                evt -> getUI().getAssociatedQualitativeValuesDoubleList().getModel().setSelected(getModel().getSelectedList()));

    }

    public void valid() {
        getModel().setSelectedList(new ArrayList<>(getUI().getAssociatedQualitativeValuesDoubleList().getModel().getSelected()));
        getModel().setValid(true);
        closeDialog();
    }

    @Override
    public void cancel() {
        getModel().setSelectedList(null);
        getModel().setValid(false);
        closeDialog();
    }
}
