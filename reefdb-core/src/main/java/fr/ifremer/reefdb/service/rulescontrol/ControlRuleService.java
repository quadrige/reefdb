package fr.ifremer.reefdb.service.rulescontrol;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Rules control service.
 *
 * @author peck7 on 04/07/2018.
 */
public interface ControlRuleService {

    /**
     * <p>controlSurveys.</p>
     *
     * @param surveys                      a {@link Collection} object.
     * @param updateControlDateWhenSucceed Should update control date, when control succeed for a survey ?
     * @param resetControlDateWhenFailed   Should reset to NULL the control date, when control failed for a survey ?
     * @param progressionModel             progression model
     * @return a {@link fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean} object.
     */
    @Transactional()
    ControlRuleMessagesBean controlSurveys(Collection<? extends SurveyDTO> surveys,
                                           boolean updateControlDateWhenSucceed,
                                           boolean resetControlDateWhenFailed,
                                           ProgressionCoreModel progressionModel);

    /**
     * <p>controlSurvey.</p>
     *
     * @param survey                       a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     * @param updateControlDateWhenSucceed Should update control date, when control succeed ?
     * @param resetControlDateWhenFailed   Should reset to NULL the control date, when control failed ?
     * @return a {@link fr.ifremer.reefdb.service.rulescontrol.ControlRuleMessagesBean} object.
     */
    @Transactional()
    ControlRuleMessagesBean controlSurvey(SurveyDTO survey,
                                          boolean updateControlDateWhenSucceed,
                                          boolean resetControlDateWhenFailed);

    /**
     * Control a object
     *
     * @param rule the rule
     * @param objectToControl the object to control against the rule
     * @return true is the rule accept the object
     */
    boolean controlUniqueObject(ControlRuleDTO rule, Object objectToControl);

}
