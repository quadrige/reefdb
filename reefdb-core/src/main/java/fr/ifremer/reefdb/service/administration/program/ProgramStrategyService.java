package fr.ifremer.reefdb.service.administration.program;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramService;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Les services pour la configuration.
 */
@Transactional(readOnly = true)
public interface ProgramStrategyService extends ProgramService {

    /**
     * Duplicate strategy.
     *
     * @param strategy             Strategy.
     * @param targetProgram a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     * @param skipLocalReferential a boolean.
     * @return Strategy
     */
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    StrategyDTO duplicateStrategy(StrategyDTO strategy, ProgramDTO targetProgram, boolean skipLocalReferential);


    List<ProgramDTO> getProgramsByCodes(List<String> programCodes);

    List<ProgramDTO> getReadableProgramsByUserAndStatus(int userId, StatusFilter statusFilter);

    /**
     * Retrieve programs by status (NATIONAL, LOCAL, ...), and with access rights for the given user
     *
     * @param userId       identifier of the user
     * @param statusFilter the filter to apply
     * @return list of programs found
     */
    List<ProgramDTO> getWritableProgramsByUserAndStatus(int userId, StatusFilter statusFilter);

    List<ProgramDTO> getReadableProgramsByCampaignId(Integer campaignId);

    List<ProgramDTO> getWritableProgramsByCampaignId(Integer campaignId);

    List<ProgramDTO> getReadablePrograms();

    List<ProgramDTO> getWritablePrograms();

    List<ProgramDTO> getReadableProgramsByLocationAndDate(Integer locationId, Date date);

    List<ProgramDTO> getWritableProgramsByLocationAndDate(Integer locationId, Date date);

    ProgramDTO getReadableProgramByCode(final String programCode);

    ProgramDTO getWritableProgramByCode(String programCode);

    /**
     * Retrieve active programs with manager rights for the current user
     *
     * @return list of programs found
     */
    List<ProgramDTO> getManagedPrograms();

    /**
     * Retrieve active programs with manager rights for the given user
     *
     * @param userId       identifier of the user
     * @return list of programs found
     */
    List<ProgramDTO> getManagedProgramsByUser(int userId);

    /**
     * Retrieve programs by status (NATIONAL, LOCAL, ...), and with manager rights for the given user
     *
     * @param userId       identifier of the user
     * @param statusFilter the filter to apply
     * @return list of programs found
     */
    List<ProgramDTO> getManagedProgramsByUserAndStatus(int userId, StatusFilter statusFilter);

    /**
     * <p>isProgramUsedByRuleList.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isProgramUsedByRuleList(String programCode);

    /**
     * <p>isProgramUsedByExtraction.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isProgramUsedByExtraction(String programCode);

    /**
     * <p>isProgramUsedByFilter.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isProgramUsedByFilter(String programCode);

    /**
     * Will load (if necessary) program's strategies and locations
     *
     * @param program a not null program
     */
    void loadStrategiesAndLocations(final ProgramDTO program);

    /**
     * Will load (if necessary) strategy's applied periods, AND add all other program's locations
     *
     * @param program  a not null program
     * @param strategy a not null strategy
     */
    void loadAppliedPeriods(ProgramDTO program, StrategyDTO strategy);

    /**
     * Get the list of all PMFM strategies for the survey
     *
     * @param survey a {@link SurveyDTO} object.
     * @return a {@link java.util.List} object.
     */
    List<PmfmStrategyDTO> getPmfmStrategiesBySurvey(SurveyDTO survey);

    /**
     * Get the first not null department from applied strategy
     *
     * @param survey a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     */
    DepartmentDTO getDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey);

    /**
     * Get analysis departments by pmfm id, for the given survey
     *
     * @param survey a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     * @return a {@link java.util.Map} object.
     */
    Map<Integer, DepartmentDTO> getAnalysisDepartmentsByPmfmMapBySurvey(SurveyDTO survey);

    /**
     * Get the first not null analysis department from applied strategy
     *
     * @param survey a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     */
    DepartmentDTO getAnalysisDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey);

    /**
     * Get the analysis department from applied strategy and pmfm
     *
     * @param survey a {@link SurveyDTO} object.
     * @param pmfms collection of pmfm
     * @return analyst department
     */
    DepartmentDTO getAnalysisDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey, Collection<PmfmDTO> pmfms);

    /**
     * Get the list of PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getPmfmsForSurvey(SurveyDTO survey);

    /**
     * Get the list of grouped PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getGroupedPmfmsForSurvey(SurveyDTO survey);

    /**
     * Get the list of PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getPmfmsForSamplingOperationBySurvey(SurveyDTO survey);

    /**
     * Get the list of grouped PMFMS used to populate the sampling operation table
     *
     * @param survey the survey from where the program, location and date are used to populate the list
     * @return a List of PmfmDTO
     */
    List<PmfmDTO> getGroupedPmfmsForSamplingOperationBySurvey(SurveyDTO survey);

    /**
     * Suppression des programmes.
     *
     * @param programCodes Identifiants des programmes
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void deletePrograms(List<String> programCodes);

    /**
     * Will load (if necessary) strategy's pmfm(s) and
     *
     * @param strategy not null
     */
    void loadPmfmStrategy(StrategyDTO strategy);

    /**
     * Save a list of local program
     *
     * @param programs a {@link java.util.List} object.
     * @throws fr.ifremer.reefdb.service.ReefDbTechnicalException if one program is not local (add argument authenticationInfo to save it remotely)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void savePrograms(List<? extends ProgramDTO> programs);

    /**
     * Save a list of . Remote program will be saved to remote server
     *
     * @param programs a {@link java.util.List} object.
     * @param authenticationInfo a {@link fr.ifremer.quadrige3.core.security.AuthenticationInfo} object.
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void savePrograms(AuthenticationInfo authenticationInfo, List<? extends ProgramDTO> programs);

    /**
     * <p>searchPrograms.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @param programName a {@link java.lang.String} object.
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return a {@link java.util.List} object.
     */
    List<ProgramDTO> searchPrograms(String programCode, String programName, StatusFilter statusFilter);

    /**
     * Recherche les strategies pour un lieu
     *
     * @param locationId Identifiant du lieu
     * @return La liste de strategies
     */
    List<ProgStratDTO> getStrategyUsageByLocationId(Integer locationId);

    /**
     * Recherche les strategies pour un programme et un lieu
     *
     * @param programCode Identifiant programme
     * @param locationId  Identifiant lieu
     * @return La liste de strategies
     */
    List<ProgStratDTO> getStrategyUsageByProgramAndLocationId(final String programCode, final Integer locationId);

    /**
     * <p>saveStrategiesByProgramAndLocation.</p>
     *
     * @param strategies a {@link java.util.List} object.
     * @param locationId a int.
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void saveStrategiesByProgramAndLocation(List<ProgStratDTO> strategies, int locationId);


    /**
     * Get list a program for user (with read access rights), from the remote synchro server
     *
     * @param authenticationInfo Authentication info, to connect to remote server
     * @return a {@link java.util.List} object.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    List<ProgramDTO> getRemoteReadableProgramsByUser(AuthenticationInfo authenticationInfo);

    /**
     * Get list a program for user (with read/write access rights), from the remote synchro server
     *
     * @param authenticationInfo Authentication info, to connect to remote server
     * @return a {@link java.util.List} object.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    List<ProgramDTO> getRemoteWritableProgramsByUser(AuthenticationInfo authenticationInfo);

    /**
     * Get if a user has some read access rights on at least on program, from the remote synchro server
     *
     * @param authenticationInfo authentication info, to connect to remote server
     * @return a boolean.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    boolean hasRemoteReadOnProgram(AuthenticationInfo authenticationInfo);

    /**
     * Get if a user has some write access rights on at least on program, from the remote synchro server
     *
     * @param authenticationInfo authentication info, to connect to remote server
     * @return a boolean.
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    boolean hasRemoteWriteOnProgram(AuthenticationInfo authenticationInfo);

    /**
     * Check if the program code exists in database, and return the found program
     * @param programCode the program code to find
     * @return the existing program or null if not found
     */
    ProgramDTO isProgramExists(String programCode);

    boolean hasPotentialMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters);

    List<MoratoriumDTO> getMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters);

}

	
