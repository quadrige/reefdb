package fr.ifremer.reefdb.service.rulescontrol;

/*
 * #%L
 * Reef DB :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Rules control bean.
 */
public class ControlRuleMessagesBean {

    /**
     * Error messages.
     */
    private List<String> errorMessages;

    /**
     * Warning messages.
     */
    private List<String> warningMessages;

    /**
     * Date when the control has been made
     */
    private Date controlDate;

    /**
     * Constructor.
     *
     * @param controlDate a {@link java.util.Date} object.
     */
    ControlRuleMessagesBean(Date controlDate) {
        errorMessages = new ArrayList<>();
        warningMessages = new ArrayList<>();
        this.controlDate = controlDate;
    }

    /**
     * <p>Getter for the field <code>controlDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getControlDate() {
        return controlDate;
    }

    /**
     * If exists error message.
     *
     * @return True or false
     */
    public boolean isErrorMessages() {
        return !errorMessages.isEmpty();
    }

    /**
     * If exists warning message.
     *
     * @return True or false
     */
    public boolean isWarningMessages() {
        return !warningMessages.isEmpty();
    }

    /**
     * <p>Getter for the field <code>errorMessages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    void addErrorMessage(String errorMessage) {
        errorMessages.add(errorMessage);
    }

    /**
     * <p>Getter for the field <code>warningMessages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getWarningMessages() {
        return warningMessages;
    }

    void addWarningMessage(String warningMessage) {
        warningMessages.add(warningMessage);
    }
}
