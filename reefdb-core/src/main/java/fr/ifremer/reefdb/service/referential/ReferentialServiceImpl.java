package fr.ifremer.reefdb.service.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbAnalysisInstrumentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.referential.ReefDbSamplingEquipmentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dao.referential.pmfm.*;
import fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonGroupDao;
import fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonNameDao;
import fr.ifremer.reefdb.dto.ParameterTypeDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.location.LocationCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.taxon.TaxonCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO;
import fr.ifremer.reefdb.dto.enums.ParameterTypeValues;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.*;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.administration.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implémentation des services pour les données de reference
 */
@Service("reefdbReferentialService")
public class ReferentialServiceImpl implements ReferentialService {

    private static final Log LOG = LogFactory.getLog(ReferentialServiceImpl.class);

    @Autowired
    protected CacheService cacheService;

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "reefDbReferentialDao")
    protected ReefDbReferentialDao referentialDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefDbMonitoringLocationDao")
    protected ReefDbMonitoringLocationDao monitoringLocationDao;

    @Resource(name = "reefDbTaxonGroupDao")
    protected ReefDbTaxonGroupDao taxonGroupDao;

    @Resource(name = "reefDbTaxonNameDao")
    protected ReefDbTaxonNameDao taxonNameDao;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource(name = "reefDbQualitativeValueDao")
    private ReefDbQualitativeValueDao qualitativeValueDao;

    @Resource(name = "reefDbParameterDao")
    protected ReefDbParameterDao parameterDao;

    @Resource(name = "reefDbMatrixDao")
    protected ReefDbMatrixDao matrixDao;

    @Resource(name = "reefDbFractionDao")
    protected ReefDbFractionDao fractionDao;

    @Resource(name = "reefDbMethodDao")
    protected ReefDbMethodDao methodDao;

    @Resource(name = "reefDbAnalysisInstrumentDao")
    protected ReefDbAnalysisInstrumentDao analysisInstrumentDao;

    @Resource(name = "reefDbSamplingEquipmentDao")
    protected ReefDbSamplingEquipmentDao samplingEquipmentDao;

    @Resource(name = "reefDbUnitDao")
    protected ReefDbUnitDao unitDao;

    @Resource(name = "reefDbUserService")
    protected UserService userService;

    private List<ParameterTypeDTO> parameterTypeList;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> getSamplingEquipments(StatusFilter statusFilter) {
        return new ArrayList<>(samplingEquipmentDao.getAllSamplingEquipments(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveSamplingEquipments(List<? extends SamplingEquipmentDTO> samplingEquipments) {
        samplingEquipmentDao.saveSamplingEquipments(samplingEquipments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSamplingEquipments(List<Integer> samplingEquipmentIds) {
        samplingEquipmentDao.deleteSamplingEquipments(samplingEquipmentIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSamplingEquipmentUsedInData(int samplingEquipmentId) {
        return samplingEquipmentDao.isSamplingEquipmentUsedInData(samplingEquipmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSamplingEquipmentUsedInValidatedData(int samplingEquipmentId) {
        return samplingEquipmentDao.isSamplingEquipmentUsedInValidatedData(samplingEquipmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceSamplingEquipment(SamplingEquipmentDTO source, SamplingEquipmentDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a sampling equipment with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a sampling equipment with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            samplingEquipmentDao.replaceTemporarySamplingEquipment(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UnitDTO> getUnits(StatusFilter statusFilter) {
        return new ArrayList<>(unitDao.getAllUnits(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveUnits(List<? extends UnitDTO> units) {
        unitDao.saveUnits(units);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUnits(List<Integer> unitIds) {
        unitDao.deleteUnits(unitIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnitUsedInReferential(int unitId) {
        return unitDao.isUnitUsedInReferential(unitId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnitUsedInData(int unitId) {
        return unitDao.isUnitUsedInData(unitId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnitUsedInValidatedData(int unitId) {
        return unitDao.isUnitUsedInValidatedData(unitId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceUnit(UnitDTO source, UnitDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a unit with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a unit with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            unitDao.replaceTemporaryUnit(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> getDepartments() {
        return new ArrayList<>(departmentDao.getAllDepartments(StatusFilter.ACTIVE.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> getDepartments(StatusFilter statusFilter) {
        return new ArrayList<>(departmentDao.getAllDepartments(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DepartmentDTO getDepartmentById(int departmentId) {
        return departmentDao.getDepartmentById(departmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveDepartments(List<DepartmentDTO> departments) {
        for (DepartmentDTO department : departments) {
            if (department.isDirty()) {
                departmentDao.saveTemporaryDepartment(department);
                department.setDirty(false);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteDepartments(List<Integer> departmentIds) {
        departmentDao.deleteTemporaryDepartments(departmentIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDepartmentUsedInProgram(int departmentId) {
        return departmentDao.isDepartmentUsedInProgram(departmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDepartmentUsedInRules(int departmentId) {
        return departmentDao.isDepartmentUsedInRules(departmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDepartmentUsedInData(int departmentId) {
        return departmentDao.isDepartmentUsedInData(departmentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDepartmentUsedInValidatedData(Integer id) {
        return departmentDao.isDepartmentUsedInValidatedData(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceDepartment(DepartmentDTO source, DepartmentDTO target, boolean delete) {

        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceDepId = source.getId();
        Integer targetDepId = target.getId();
        Assert.isTrue(sourceDepId < 0, String.format("Can't replace a department with a positive id %s", sourceDepId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a department with non local status %s", sourceDepId));

        if (!sourceDepId.equals(targetDepId)) {
            // replace occurrences
            departmentDao.replaceTemporaryDepartmentFks(sourceDepId, targetDepId, delete);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> getAnalysisInstruments(StatusFilter statusFilter) {
        return new ArrayList<>(analysisInstrumentDao.getAllAnalysisInstruments(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveAnalysisInstruments(List<? extends AnalysisInstrumentDTO> analysisInstruments) {
        analysisInstrumentDao.saveAnalysisInstruments(analysisInstruments);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteAnalysisInstruments(List<Integer> analysisInstrumentIds) {
        analysisInstrumentDao.deleteAnalysisInstruments(analysisInstrumentIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAnalysisInstrumentUsedInData(int analysisInstrumentId) {
        return analysisInstrumentDao.isAnalysisInstrumentUsedInData(analysisInstrumentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAnalysisInstrumentUsedInValidatedData(int analysisInstrumentId) {
        return analysisInstrumentDao.isAnalysisInstrumentUsedInValidatedData(analysisInstrumentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAnalysisInstrumentUsedInProgram(int analysisInstrumentId) {
        return analysisInstrumentDao.isAnalysisInstrumentUsedInProgram(analysisInstrumentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceAnalysisInstrument(AnalysisInstrumentDTO source, AnalysisInstrumentDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a analysis instrument with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a analysis instrument with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            analysisInstrumentDao.replaceTemporaryAnalysisInstrument(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<StatusDTO> getStatus(StatusFilter statusFilter) {
        return referentialDao.getAllStatus(statusFilter.toStatusCodes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getLocations(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(monitoringLocationDao.getAllLocations(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getActiveLocations() {
        return new ArrayList<>(monitoringLocationDao.getAllLocations(StatusFilter.ACTIVE.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocationDTO getLocation(int locationId) {
        return monitoringLocationDao.getLocationById(locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getLocations(Integer campaignId, String programCode, boolean activeOnly) {
        StatusFilter statusFilter = activeOnly ? StatusFilter.ACTIVE : StatusFilter.ALL;
        if (campaignId == null && programCode == null) {
            return getLocations(statusFilter);
        }
        return new ArrayList<>(monitoringLocationDao.getLocationsByCampaignAndProgram(campaignId, programCode, statusFilter));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveLocations(List<? extends LocationDTO> locations) {
        monitoringLocationDao.saveLocations(locations);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteLocations(List<Integer> locationIds) {
        monitoringLocationDao.deleteLocations(locationIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLocationUsedInProgram(int locationId) {
        return monitoringLocationDao.isLocationUsedInProgram(locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLocationUsedInData(int locationId) {
        return monitoringLocationDao.isLocationUsedInData(locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLocationUsedInValidatedData(int locationId) {
        return monitoringLocationDao.isLocationUsedInValidatedData(locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceLocation(LocationDTO source, LocationDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a monitoring location with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a monitoring location with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            monitoringLocationDao.replaceTemporaryLocation(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<HarbourDTO> getHarbours(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return monitoringLocationDao.getAllHarbours(statusFilter.toStatusCodes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getTaxonGroups() {
        return new ArrayList<>(taxonGroupDao.getAllTaxonGroups());
    }

    @Override
    public List<TaxonGroupDTO> getTaxonGroups(TaxonDTO taxon) {

        // Reverse the taxon name map
        Multimap<TaxonDTO, Integer> taxonGroupIdByTaxon =
                Multimaps.invertFrom(
                        taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now()),
                        ArrayListMultimap.create());

        return taxonGroupDao.getTaxonGroupsByIds(taxonGroupIdByTaxon.get(taxon));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxonGroupDTO getTaxonGroup(int taxonGroupId) {
        return taxonGroupDao.getTaxonGroupById(taxonGroupId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getFullTaxonGroups(List<TaxonGroupDTO> taxonGroups) {
        List<TaxonGroupDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(taxonGroups)) {
            for (TaxonGroupDTO taxonGroup : taxonGroups) {
                if (taxonGroup.getId() == null) {
                    // if happens, add it directly
                    result.add(taxonGroup);
                } else {

                    // reload this taxon group from cache (or get it from db)
                    result.add(taxonGroupDao.getTaxonGroupById(taxonGroup.getId()));
                }
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveTaxonGroups(List<? extends TaxonGroupDTO> taxonGroups) {
        taxonGroupDao.saveTaxonGroups(taxonGroups);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTaxonGroups(List<Integer> taxonGroupIds) {
        taxonGroupDao.deleteTaxonGroups(taxonGroupIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonGroupUsedInReferential(int taxonGroupId) {
        return taxonGroupDao.isTaxonGroupUsedInReferential(taxonGroupId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonGroupUsedInData(int taxonGroupId) {
        return taxonGroupDao.isTaxonGroupUsedInData(taxonGroupId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonGroupUsedInValidatedData(int taxonGroupId) {
        return taxonGroupDao.isTaxonGroupUsedInValidatedData(taxonGroupId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceTaxonGroup(TaxonGroupDTO source, TaxonGroupDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a taxon group with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a taxon group with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            taxonGroupDao.replaceTemporaryTaxonGroup(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getTaxons(Integer taxonGroupId) {
        if (taxonGroupId == null) {
            return new ArrayList<>(taxonNameDao.getAllTaxonNames());
        } else {
            return new ArrayList<>(taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now()).get(taxonGroupId));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxonDTO getTaxon(int taxonId) {
        return taxonNameDao.getTaxonNameById(taxonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveTaxons(List<? extends TaxonDTO> taxons) {
        taxonNameDao.saveTaxons(taxons);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTaxons(List<Integer> taxonIds) {
        taxonNameDao.deleteTaxons(taxonIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonNameUsedInReferential(int taxonNameId) {
        if (taxonNameDao.isTaxonNameUsedInReferential(taxonNameId)) {
            return true;
        }

        // If local, check also reference taxon
        boolean isLocaTaxonName = TemporaryDataHelper.isTemporaryId(taxonNameId);
        if (isLocaTaxonName) {
            TaxonDTO taxon = taxonNameDao.getTaxonNameById(taxonNameId);
            int refTaxonId = taxon.getReferenceTaxonId();
            return taxonNameDao.isReferenceTaxonUsedInReferential(refTaxonId, taxonNameId);
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonNameUsedInData(int taxonNameId) {
        // Retrieve reference taxon id
        TaxonDTO taxon = taxonNameDao.getTaxonNameById(taxonNameId);
        int refTaxonId = taxon.getReferenceTaxonId();

        // Check if used in data
        return taxonNameDao.isReferenceTaxonUsedInData(refTaxonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonNameUsedInValidatedData(int taxonNameId) {
        // Retrieve reference taxon id
        TaxonDTO taxon = taxonNameDao.getTaxonNameById(taxonNameId);
        int refTaxonId = taxon.getReferenceTaxonId();

        // Check if used in data
        return taxonNameDao.isReferenceTaxonUsedInValidatedData(refTaxonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceTaxon(TaxonDTO source, TaxonDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(source.getReferenceTaxonId());
        Assert.notNull(target);
        Assert.notNull(target.getId());
        Assert.notNull(target.getReferenceTaxonId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Integer sourceReferenceId = source.getReferenceTaxonId();
        Integer targetReferenceId = target.getReferenceTaxonId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a taxon name with a positive id %s", sourceId));
        Assert.isTrue(sourceReferenceId < 0, String.format("Can't replace a taxon name with a positive reference id %s", sourceId));
        // Taxon has no status
//        Preconditions.checkArgument(Daos.isLocalStatus(source.getStatus()), String.format("Can't replace a taxon name with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            taxonNameDao.replaceTemporaryTaxon(sourceId, sourceReferenceId, targetId, targetReferenceId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillTaxonsProperties(List<TaxonDTO> taxons) {
        taxonNameDao.fillTaxonsProperties(taxons);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillReferentTaxons(List<TaxonDTO> taxons) {
        taxonNameDao.fillReferents(taxons);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonomicLevelDTO> getTaxonomicLevels() {
        return taxonNameDao.getAllTaxonomicLevels();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CitationDTO> getCitations() {
        return new ArrayList<>(referentialDao.getAllCitations());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PositioningSystemDTO> getPositioningSystems() {
        return new ArrayList<>(referentialDao.getAllPositioningSystems());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterGroupDTO> getParameterGroup(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(parameterDao.getAllParameterGroups(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterDTO> getParameters(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(parameterDao.getAllParameters(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isParameterUsedInProgram(String parameterCode) {
        return parameterDao.isParameterUsedInProgram(parameterCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isParameterUsedInRules(String parameterCode) {
        return parameterDao.isParameterUsedInRules(parameterCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isParameterUsedInReferential(String parameterCode) {
        return parameterDao.isParameterUsedInReferential(parameterCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceParameter(ParameterDTO source, ParameterDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notBlank(source.getCode());
        Assert.notNull(target);
        Assert.notBlank(target.getCode());

        String sourceCode = source.getCode();
        String targetCode = target.getCode();
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a parameter with non local status %s", sourceCode));

        if (!sourceCode.equals(targetCode)) {
            // replace occurrences
            parameterDao.replaceTemporaryParameter(sourceCode, targetCode, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveParameters(List<? extends ParameterDTO> parameters) {
        parameterDao.saveParameters(parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteParameters(List<String> parametersCodes) {
        parameterDao.deleteParameters(parametersCodes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterDTO> searchParameters(StatusFilter statusFilter, String parameterCode, String statusCode, ParameterGroupDTO parameterGroup) {
        Assert.notNull(statusFilter);
        Integer parameterGroupId = parameterGroup == null ? null : parameterGroup.getId();
        return parameterDao.findParameters(parameterCode, parameterGroupId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MatrixDTO> getMatrices(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(matrixDao.getAllMatrices(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMatrixUsedInProgram(int matrixId) {
        return matrixDao.isMatrixUsedInProgram(matrixId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMatrixUsedInRules(int matrixId) {
        return matrixDao.isMatrixUsedInRules(matrixId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMatrixUsedInReferential(int matrixId) {
        return matrixDao.isMatrixUsedInReferential(matrixId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceMatrix(MatrixDTO source, MatrixDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a matrix with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a matrix with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            matrixDao.replaceTemporaryMatrix(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMatrices(List<Integer> matrixIds) {
        matrixDao.deleteMatrices(matrixIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveMatrices(List<? extends MatrixDTO> matrices) {
        matrixDao.saveMatrices(matrices);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MatrixDTO> searchMatrices(StatusFilter statusFilter, Integer matrixId, String statusCode) {
        Assert.notNull(statusFilter);
        return matrixDao.findMatrices(matrixId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FractionDTO> getFractions(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(fractionDao.getAllFractions(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveFractions(List<? extends FractionDTO> fractions) {
        fractionDao.saveFractions(fractions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFractions(List<Integer> fractionIds) {
        fractionDao.deleteFractions(fractionIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFractionUsedInProgram(int fractionId) {
        return fractionDao.isFractionUsedInProgram(fractionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFractionUsedInRules(int fractionId) {
        return fractionDao.isFractionUsedInRules(fractionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isFractionUsedInReferential(int fractionId) {
        return fractionDao.isFractionUsedInReferential(fractionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceFraction(FractionDTO source, FractionDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a fraction with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a fraction with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            fractionDao.replaceTemporaryFraction(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FractionDTO> searchFractions(StatusFilter statusFilter, Integer fractionId, String statusCode) {
        Assert.notNull(statusFilter);
        return fractionDao.findFractions(fractionId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MethodDTO> getMethods(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(methodDao.getAllMethods(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveMethods(List<? extends MethodDTO> methods) {
        methodDao.saveMethods(methods);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMethods(List<Integer> methodIds) {
        methodDao.deleteMethods(methodIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMethodUsedInProgram(int methodId) {
        return methodDao.isMethodUsedInProgram(methodId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMethodUsedInRules(int methodId) {
        return methodDao.isMethodUsedInRules(methodId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isMethodUsedInReferential(int methodId) {
        return methodDao.isMethodUsedInReferential(methodId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceMethod(MethodDTO source, MethodDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a method with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a method with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            methodDao.replaceTemporaryMethod(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MethodDTO> searchMethods(StatusFilter statusFilter, Integer methodId, String statusCode) {
        Assert.notNull(statusFilter);
        return methodDao.findMethods(methodId, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfms(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(pmfmDao.getAllPmfms(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PmfmDTO getPmfm(int pmfmId) {
        return pmfmDao.getPmfmById(pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPmfmUsedInProgram(int pmfmId) {
        return pmfmDao.isPmfmUsedInProgram(pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPmfmUsedInRules(int pmfmId) {
        return pmfmDao.isPmfmUsedInRules(pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replacePmfm(PmfmDTO source, PmfmDTO target, boolean delete) {
        Assert.notNull(source);
        Assert.notNull(source.getId());
        Assert.notNull(target);
        Assert.notNull(target.getId());

        Integer sourceId = source.getId();
        Integer targetId = target.getId();
        Assert.isTrue(sourceId < 0, String.format("Can't replace a pmfmu with a positive id %s", sourceId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a pmfmu with non local status %s", sourceId));

        if (!sourceId.equals(targetId)) {
            // replace occurrences
            pmfmDao.replaceTemporaryPmfm(sourceId, targetId, delete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPmfmUsedInData(int pmfmId) {
        return pmfmDao.isPmfmUsedInData(pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPmfmUsedInValidatedData(int pmfmId) {
        return pmfmDao.isPmfmUsedInValidatedData(pmfmId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePmfms(List<Integer> pmfmIds) {
        pmfmDao.deletePmfms(pmfmIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePmfms(List<? extends PmfmDTO> pmfms) {
        pmfmDao.savePmfms(pmfms);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> searchPmfms(StatusFilter statusFilter, String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, String statusCode) {
        return pmfmDao.findPmfms(parameterCode, matrixId, fractionId, methodId, unitId, pmfmName, statusFilter.intersect(statusCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LevelDTO> getLevels() {
        return new ArrayList<>(referentialDao.getAllDepthLevels());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepthDTO> getDepths() {
        return new ArrayList<>(referentialDao.getAllDepthValues());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<QualityLevelDTO> getQualityLevels(StatusFilter statusFilter) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(referentialDao.getAllQualityFlags(statusFilter.toStatusCodes()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, Integer analysisInstrumentId, String statusCode) {
        Assert.notNull(statusFilter);
        return analysisInstrumentDao.findAnalysisInstruments(statusFilter.intersect(statusCode), analysisInstrumentId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, Integer samplingEquipmentId, String statusCode, Integer unitId) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(samplingEquipmentDao.findSamplingEquipments(statusFilter.intersect(statusCode), samplingEquipmentId, unitId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, String equipmentName, String statusCode) {
        return new ArrayList<>(samplingEquipmentDao.findSamplingEquipmentsByName(statusFilter.intersect(statusCode), equipmentName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UnitDTO> searchUnits(StatusFilter statusFilter, Integer unitId, String statusCode) {
        Assert.notNull(statusFilter);
        return new ArrayList<>(unitDao.findUnits(unitId, statusFilter.intersect(statusCode)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonDTO> getUsersInSameDepartment(PersonDTO person) {
        PersonCriteriaDTO searchCriteria = ReefDbBeanFactory.newPersonCriteriaDTO();
        if (person != null && person.getDepartment() != null) {
            searchCriteria.setDepartment(person.getDepartment());
        }
        return userService.searchUser(searchCriteria);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUserUsedInProgram(Integer id) {
        return userService.isUserUsedInProgram(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUserUsedInRules(Integer id) {
        return userService.isUserUsedInRules(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUserUsedInValidatedData(Integer id) {
        return userService.isUserUsedInValidatedData(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceUser(PersonDTO souce, PersonDTO target, boolean delete) {
        userService.replaceUser(souce, target, delete);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<QualityLevelDTO> searchQualityLevels(StatusFilter statusFilter, String qualityLevelCode, String statusCode) {
        Assert.notNull(statusFilter);
        return referentialDao.findQualityFlags(statusFilter.intersect(statusCode), qualityLevelCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GroupingTypeDTO> getGroupingTypes() {
        return new ArrayList<>(referentialDao.getAllGroupingTypes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> searchLocations(LocationCriteriaDTO searchCriteria) {
        StatusFilter statusFilter = StatusFilter.toLocalOrNational(searchCriteria.getLocal());
        List<String> statusCodes = statusFilter.intersect(searchCriteria.getStatus());
        String orderItemTypeCode = searchCriteria.getGroupingType() == null ? null : searchCriteria.getGroupingType().getCode();
        Integer orderItemId = searchCriteria.getGrouping() == null ? null : searchCriteria.getGrouping().getId();
        String programCode = searchCriteria.getProgram() == null ? null : searchCriteria.getProgram().getCode();

        return monitoringLocationDao.findLocations(statusCodes,
                orderItemTypeCode,
                orderItemId,
                programCode,
                searchCriteria.getLabel(),
                searchCriteria.getName(),
                searchCriteria.isStrictName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> searchDepartments(DepartmentCriteriaDTO searchCriteria) {
        StatusFilter statusFilter = StatusFilter.toLocalOrNational(searchCriteria.getLocal());
        List<String> statusCodes = statusFilter.intersect(searchCriteria.getStatus());
        Integer parentId = searchCriteria.getParentDepartment() == null ?
                null : searchCriteria.getParentDepartment().getId();
        return departmentDao.findDepartmentsByCodeAndName(
                searchCriteria.getCode(),
                searchCriteria.getName(),
                searchCriteria.isStrictName(),
                parentId,
                statusCodes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, String instrumentName) {
        return analysisInstrumentDao.findAnalysisInstrumentsByName(statusFilter.toStatusCodes(), instrumentName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> searchTaxons(TaxonCriteriaDTO taxonCriteria) {
        String levelCode = taxonCriteria.getLevel() == null ? null : taxonCriteria.getLevel().getCode();
        String name = StringUtils.isBlank(taxonCriteria.getName()) ? null : taxonCriteria.getName();
        if (taxonCriteria.isFullProperties()) {
            return taxonNameDao.findFullTaxonNamesByCriteria(levelCode, name, taxonCriteria.isStrictName(), taxonCriteria.getLocal());
        } else {
            return taxonNameDao.findTaxonNamesByCriteria(levelCode, name, taxonCriteria.isStrictName(), taxonCriteria.getLocal());
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> searchTaxonGroups(TaxonGroupCriteriaDTO searchCriteria) {
        StatusFilter statusFilter = StatusFilter.toLocalOrNational(searchCriteria.getLocal());
        List<String> statusCodes = statusFilter.intersect(searchCriteria.getStatus());
        Integer parentTaxonGroupId = searchCriteria.getParentTaxonGroup() == null ? null : searchCriteria.getParentTaxonGroup().getId();
        return taxonGroupDao.findTaxonGroups(parentTaxonGroupId,
                searchCriteria.getLabel(),
                searchCriteria.getName(),
                searchCriteria.isStrictName(),
                statusCodes);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PhotoTypeDTO> getPhotoTypes() {
        return new ArrayList<>(referentialDao.getAllPhotoTypes());
    }

    @Override
    public Integer getUniquePmfmIdFromPmfm(PmfmDTO pmfm) {
        String parameterCode = pmfm.getParameter().getCode();
        Integer matrixId = pmfm.getMatrix() != null ? pmfm.getMatrix().getId() : null;
        Integer fractionId = pmfm.getFraction() != null ? pmfm.getFraction().getId() : null;
        Integer methodId = pmfm.getMethod() != null ? pmfm.getMethod().getId() : null;
        Integer unitId = pmfm.getUnit() != null ? pmfm.getUnit().getId() : null;
        List<PmfmDTO> pmfms = pmfmDao.findPmfms(parameterCode, matrixId, fractionId, methodId, unitId, null, StatusFilter.ALL.toStatusCodes());
        if (CollectionUtils.size(pmfms) != 1) {
            if (LOG.isWarnEnabled()) {
                LOG.warn(String.format("Should found only 1 PMFMU but %s have been found with parameterCode=%s matrixId=%s fractionId=%s methodId=%s unitId=%s",
                        CollectionUtils.size(pmfms), parameterCode, matrixId, fractionId, methodId, unitId));
            }
            throw new ReefDbTechnicalException("Should found only 1 PMFMU");
        }
        return pmfms.get(0).getId();
    }

    @Override
    public PmfmDTO getUniquePmfmFromPmfm(PmfmDTO pmfm) {
        return getPmfm(getUniquePmfmIdFromPmfm(pmfm));
    }

    @Override
    public QualitativeValueDTO getQualitativeValue(int qualitativeValueId) {
        return qualitativeValueDao.getQualitativeValueById(qualitativeValueId);
    }

    @Override
    public List<QualitativeValueDTO> getQualitativeValues(Collection<Integer> qualitativeValueIds) {
        List<QualitativeValueDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(qualitativeValueIds)) {
            for (int qualitativeValueId : qualitativeValueIds) {
                result.add(qualitativeValueDao.getQualitativeValueById(qualitativeValueId));
            }
        }
        return result;
    }

    @Override
    public List<QualitativeValueDTO> getQualitativeValues(String parameterCode) {
        if (StringUtils.isBlank(parameterCode))
            return null;

        return qualitativeValueDao.getQualitativeValuesByParameterCode(parameterCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ParameterTypeDTO> getParameterTypes() {
        // If list not exist, create it
        if (parameterTypeList == null) {
            parameterTypeList = Lists.newArrayList();

            // Add all values
            for (ParameterTypeValues parameterTypeValue : ParameterTypeValues.values()) {

                // DTO
                ParameterTypeDTO parameterType = ReefDbBeanFactory.newParameterTypeDTO();

                // Add ID
                parameterType.setId(parameterTypeValue.ordinal());

                // Add parameterType label
                parameterType.setKeyLabel(parameterTypeValue.getLabel());

                // Add to list
                parameterTypeList.add(parameterType);
            }
        }
        return parameterTypeList;
    }

}
