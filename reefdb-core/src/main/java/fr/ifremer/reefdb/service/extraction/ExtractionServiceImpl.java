package fr.ifremer.reefdb.service.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.system.extraction.ReefDbExtractionDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.FilterTypeDTO;
import fr.ifremer.reefdb.service.administration.context.ContextService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Extraction Service
 * <p/>
 * Created by Ludovic on 02/12/2015.
 */
@Service("reefdbExtractionService")
public class ExtractionServiceImpl implements ExtractionService {

    private static final Log LOG = LogFactory.getLog(ExtractionServiceImpl.class);

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "reefdbContextService")
    protected ContextService contextService;

    @Resource(name = "reefDbExtractionDao")
    protected ReefDbExtractionDao extractionDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterTypeDTO> getFilterTypes() {
        List<FilterTypeDTO> result = Lists.newArrayList();

        // add list from ExtractionFilterValues
        for (ExtractionFilterValues extractionFilter : ExtractionFilterValues.values()) {
            if (extractionFilter.isHidden()) continue;
            FilterTypeDTO filterType = ReefDbBeanFactory.newFilterTypeDTO();
            filterType.setId(extractionFilter.getFilterTypeId());
            filterType.setName(extractionFilter.getLabel());
            result.add(filterType);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionDTO> getAllExtractions() {
        return extractionDao.getAllExtraction();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionDTO> getExtractions(Integer extractionId, String programCode) {
        if (extractionId != null) {
            return Lists.newArrayList(extractionDao.getExtractionById(extractionId));
        } else if (programCode != null) {
            return extractionDao.searchExtractionByProgram(programCode);
        } else {
            return extractionDao.getAllExtraction();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadFilteredElements(ExtractionDTO extraction) {
        Assert.notNull(extraction);

        if (extraction.getFilters() != null) {
            for (FilterDTO filter : extraction.getFilters()) {
                if (ExtractionFilterValues.getExtractionFilter(filter.getFilterTypeId()) == ExtractionFilterValues.PERIOD) {
                    continue;
                }

                if (!filter.isFilterLoaded()) {
                    contextService.loadFilteredElements(filter);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveExtractions(Collection<? extends ExtractionDTO> extractions) {
        if (extractions != null) {
            for (ExtractionDTO extraction : extractions) {
                if (extraction.isDirty()) {

                    // save
                    saveExtraction(extraction);

                    extraction.setDirty(false);
                }
            }
        }
    }

    private void saveExtraction(ExtractionDTO extraction) {
        Assert.notNull(extraction);
        Assert.notEmpty(extraction.getFilters());
        Assert.notNull(ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.PERIOD));
        Assert.notNull(ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.ORDER_ITEM_TYPE));

        // save extraction
        extractionDao.saveExtraction(extraction);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteExtractions(List<Integer> idExtractionToDelete) {
        Assert.notNull(idExtractionToDelete);
        Set<Integer> ids = idExtractionToDelete.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        ids.forEach(extractionDao::remove);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractionDTO duplicateExtraction(ExtractionDTO extraction) {
        if (extraction == null) return null;
        ExtractionDTO duplicatedExtraction = ReefDbBeans.clone(extraction);
        Assert.notNull(duplicatedExtraction);
        duplicatedExtraction.setId(null);
        duplicatedExtraction.setName(null);
        duplicatedExtraction.setDirty(true);
        duplicatedExtraction.setErrors(null);

        // filters
        duplicatedExtraction.setFilters(new ArrayList<>());
        if (!extraction.isFiltersEmpty()) {
            for (FilterDTO filter : extraction.getFilters()) {
                duplicatedExtraction.addFilters(contextService.duplicateFilter(filter));
            }
        }

        return duplicatedExtraction;
    }


}
