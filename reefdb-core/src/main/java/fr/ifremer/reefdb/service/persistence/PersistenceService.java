package fr.ifremer.reefdb.service.persistence;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import org.nuiton.version.Version;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Closeable;

/**
 * <p>PersistenceService interface.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Transactional(readOnly = true)
public interface PersistenceService extends fr.ifremer.quadrige3.core.service.persistence.PersistenceService, Closeable, InitializingBean {

    /**
     * <p>getDbVersion.</p>
     *
     * @return a {@link org.nuiton.version.Version} object.
     */
    Version getDbVersion();

    /**
     * <p>getApplicationVersion.</p>
     *
     * @return a {@link org.nuiton.version.Version} object.
     */
    Version getApplicationVersion();

    /**
     * <p>updateSchema.</p>
     */
    @Transactional()
    void updateSchema();

    /**
     * do a online compact
     */
    void compactDb();

    /**
     * Clear all caches
     */
    void clearAllCaches();

    /**
     * Load cache with entities need for ReefDb
     *
     * @param progressionModel a {@link ProgressionCoreModel} object.
     */
    void loadDefaultCaches(ProgressionCoreModel progressionModel);

    @Transactional(propagation = Propagation.SUPPORTS)
    void enableMassiveUpdate();

    @Transactional(propagation = Propagation.SUPPORTS)
    void disableMassiveUpdate();

    void compactDatabaseOnClose();
}
