package fr.ifremer.reefdb.service.observation;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
/**
 * @author peck7 on 31/05/2018.
 */
public class ValidationBean {

    private int surveyId;

    private boolean validated;

    private boolean readyToSynchronize;

    public ValidationBean(int surveyId) {
        this.surveyId = surveyId;
        this.validated = true; // default
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public boolean isReadyToSynchronize() {
        return readyToSynchronize;
    }

    public void setReadyToSynchronize(boolean readyToSynchronize) {
        this.readyToSynchronize = readyToSynchronize;
    }
}
