package fr.ifremer.reefdb.service;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;

import java.util.List;

/**
 * Enumeration to provide status codes from national or local status
 *
 * @author Ludovic
 */
public enum StatusFilter {

    NATIONAL(ImmutableList.of(StatusCode.ENABLE.getValue(), StatusCode.DISABLE.getValue(), StatusCode.TEMPORARY.getValue())),
    NATIONAL_ACTIVE(ImmutableList.of(StatusCode.ENABLE.getValue())),
    LOCAL(ImmutableList.of(StatusCode.LOCAL_ENABLE.getValue(), StatusCode.LOCAL_DISABLE.getValue())),
    LOCAL_ACTIVE(ImmutableList.of(StatusCode.LOCAL_ENABLE.getValue())),
    ACTIVE(ImmutableList.of(StatusCode.ENABLE.getValue(), StatusCode.LOCAL_ENABLE.getValue())),
    ALL(ImmutableList.of(StatusCode.ENABLE.getValue(), StatusCode.DISABLE.getValue(), StatusCode.TEMPORARY.getValue(),
            StatusCode.LOCAL_ENABLE.getValue(), StatusCode.LOCAL_DISABLE.getValue())),
    DISABLED(ImmutableList.of(StatusCode.DISABLE.getValue(), StatusCode.LOCAL_DISABLE.getValue()));

    private final List<String> statusCodes;

    StatusFilter(List<String> statusCodes) {
        this.statusCodes = statusCodes;
    }

    /**
     * <p>toStatusCodes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> toStatusCodes() {
        return statusCodes;
    }

    /**
     * <p>intersect.</p>
     *
     * @param statusCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<String> intersect(String statusCode) {
        if (statusCode == null) {
            return statusCodes;
        }
        if (statusCodes.contains(statusCode)) {
            return ImmutableList.of(statusCode);
        } else {
            return ImmutableList.of("BAD");
        }
    }

    /**
     * <p>intersect.</p>
     *
     * @param status a {@link StatusDTO} object.
     * @return a {@link java.util.List} object.
     */
    public List<String> intersect(StatusDTO status) {
        if (status == null) {
            return statusCodes;
        } else {
            return intersect(status.getCode());
        }
    }

    /**
     * <p>toLocalOrNational.</p>
     *
     * @param isLocal a {@link java.lang.Boolean} object.
     * @return a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     */
    public static StatusFilter toLocalOrNational(Boolean isLocal) {
        if (isLocal == null) {
            return ALL;
        } else {
            return isLocal ? LOCAL : NATIONAL;
        }
    }

    /**
     * <p>toActiveLocalOrNational.</p>
     *
     * @param isLocal a {@link java.lang.Boolean} object.
     * @return a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     */
    public static StatusFilter toActiveLocalOrNational(Boolean isLocal) {
        if (isLocal == null) {
            return ACTIVE;
        } else {
            return isLocal ? LOCAL_ACTIVE : NATIONAL_ACTIVE;
        }
    }

}
