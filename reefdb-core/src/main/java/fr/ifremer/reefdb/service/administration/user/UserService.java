package fr.ifremer.reefdb.service.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * <p>UserService interface.</p>
 *
 */
@Transactional(readOnly = true)
public interface UserService extends fr.ifremer.quadrige3.core.service.administration.user.UserService {

    /**
     * <p>getDepartmentIdByUserId.</p>
     *
     * @param userId a int.
     * @return a {@link java.lang.Integer} object.
     */
    Integer getDepartmentIdByUserId(int userId);

    /**
     * <p>isLoginExtranet.</p>
     *
     * @param login a {@link java.lang.String} object.
     * @return a {@link java.lang.Boolean} object.
     */
    Boolean isLoginExtranet(String login);

    /**
     * User has not password define ?
     *
     * @param login a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean hasPassword(String login);

    /**
     * Is user a local user with an empty password ?
     *
     * @param login a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isLocalUserWithNoPassword(String login);

    /**
     * <p>updatePasswordByUserId.</p>
     *
     * @param personId a int.
     * @param password a {@link java.lang.String} object.
     */
    @Transactional()
    void updatePasswordByUserId(int personId, String password);

    /**
     * <p>getActiveUsers.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getActiveUsers();

    /**
     * <p>resetPassword.</p>
     *
     * @param login a {@link java.lang.String} object.
     */
    @Transactional()
    void resetPassword(String login);

    /**
     * Search User.
     *
     * @param searchCriteria Search parameter
     * @return User list
     */
    List<PersonDTO> searchUser(PersonCriteriaDTO searchCriteria);
    
    /**
     * <p>getAllPrivileges.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PrivilegeDTO> getAllPrivileges();

    /**
     * <p>getAvailablePrivileges.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PrivilegeDTO> getAvailablePrivileges();

    /**
     * <p>getPrivilegesByUser.</p>
     *
     * @param userId a {@link java.lang.Integer} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<PrivilegeDTO> getPrivilegesByUser(Integer userId);
    
    /**
     * <p>saveUsers.</p>
     *
     * @param users a {@link List} object.
     * @return a {@link java.util.List} object.
     */
    @Transactional()
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void saveUsers(List<PersonDTO> users);
    
    /**
     * <p>deleteUsers.</p>
     *
     * @param personIds a {@link java.util.List} object.
     */
    @Transactional()
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void deleteUsers(List<Integer> personIds);
    
    /**
     * Replaces a local user by a national or local quser.
     * All data related to this former user should be updated to use the latter
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void replaceUser(PersonDTO source, PersonDTO target, boolean delete);

    /**
     * Check if a user is used (in program/strategy) or not
     *
     * @param userId a int.
     * @return a boolean.
     */
    boolean isUserUsedInProgram(int userId);

    /**
     * Check if a user is used (in control rules) or not
     *
     * @param userId a int.
     * @return a boolean.
     */
    boolean isUserUsedInRules(int userId);

    /**
     * Check if a user is used (in data) or not
     *
     * @param userId a int.
     * @return a boolean.
     */
    boolean isUserUsedInData(int userId);

    /**
     * <p>isUserUsedInValidatedData.</p>
     *
     * @param userId a int.
     * @return a boolean.
     */
    boolean isUserUsedInValidatedData(int userId);

}
