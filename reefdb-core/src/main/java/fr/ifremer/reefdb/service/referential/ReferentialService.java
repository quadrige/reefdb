package fr.ifremer.reefdb.service.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.ParameterTypeDTO;
import fr.ifremer.reefdb.dto.configuration.filter.department.DepartmentCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.location.LocationCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.taxon.TaxonCriteriaDTO;
import fr.ifremer.reefdb.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.*;
import fr.ifremer.reefdb.service.StatusFilter;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Les sevices pour les donnees de reference.
 */
@Transactional(readOnly = true)
public interface ReferentialService {

    // ---------- SamplingEquipmentDTO ----------

    /**
     * La liste des engins du référentiel.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return La liste des engins
     */
    List<SamplingEquipmentDTO> getSamplingEquipments(StatusFilter statusFilter);

    /**
     * <p>saveSamplingEquipments.</p>
     *
     * @param samplingEquipments a {@link java.util.List} object.
     */
    @Transactional()
    void saveSamplingEquipments(List<? extends SamplingEquipmentDTO> samplingEquipments);

    /**
     * <p>deleteSamplingEquipments.</p>
     *
     * @param samplingEquipmentIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteSamplingEquipments(List<Integer> samplingEquipmentIds);

    /**
     * Check if this sampling equipment is used in data
     * SAMPLING_OPERATION
     *
     * @param samplingEquipmentId a int.
     * @return a boolean.
     */
    boolean isSamplingEquipmentUsedInData(int samplingEquipmentId);

    /**
     * <p>isSamplingEquipmentUsedInValidatedData.</p>
     *
     * @param samplingEquipmentId a int.
     * @return a boolean.
     */
    boolean isSamplingEquipmentUsedInValidatedData(int samplingEquipmentId);

    /**
     * <p>replaceSamplingEquipment.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceSamplingEquipment(SamplingEquipmentDTO source, SamplingEquipmentDTO target, boolean delete);

    /**
     * The search service for sampling equipment
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param samplingEquipmentId a {@link java.lang.Integer} object.
     * @param statusCode a {@link java.lang.String} object.
     * @param unitId a {@link java.lang.Integer} object.
     * @return A list of sampling equipment
     */
    List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, Integer samplingEquipmentId, String statusCode, Integer unitId);

    /**
     * <p>searchSamplingEquipments.</p>
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param equipmentName a {@link java.lang.String} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<SamplingEquipmentDTO> searchSamplingEquipments(StatusFilter statusFilter, String equipmentName, String statusCode);

    // ---------- UnitDTO ----------

    /**
     * La liste des unites du referential.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return La liste des unites du referential
     */
    List<UnitDTO> getUnits(StatusFilter statusFilter);

    /**
     * <p>saveUnits.</p>
     *
     * @param units a {@link java.util.List} object.
     */
    @Transactional()
    void saveUnits(List<? extends UnitDTO> units);

    /**
     * <p>deleteUnits.</p>
     *
     * @param unitIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteUnits(List<Integer> unitIds);

    /**
     * <p>isUnitUsedInReferential.</p>
     *
     * @param unitId a int.
     * @return a boolean.
     */
    boolean isUnitUsedInReferential(int unitId);

    /**
     * <p>isUnitUsedInData.</p>
     *
     * @param unitId a int.
     * @return a boolean.
     */
    boolean isUnitUsedInData(int unitId);

    /**
     * <p>isUnitUsedInValidatedData.</p>
     *
     * @param unitId a int.
     * @return a boolean.
     */
    boolean isUnitUsedInValidatedData(int unitId);

    /**
     * <p>replaceUnit.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.UnitDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.UnitDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceUnit(UnitDTO source, UnitDTO target, boolean delete);

    /**
     * The search service for unit
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param unitId a {@link java.lang.Integer} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return A list of unit
     */
    List<UnitDTO> searchUnits(StatusFilter statusFilter, Integer unitId, String statusCode);

    // ---------- PersonDTO ----------

    /**
     * wrapped method used in referential replacement
     *
     * @param person a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getUsersInSameDepartment(PersonDTO person);

    /**
     * <p>isUserUsedInProgram.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     * @return a boolean.
     */
    boolean isUserUsedInProgram(Integer id);

    /**
     * <p>isUserUsedInRules.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     * @return a boolean.
     */
    boolean isUserUsedInRules(Integer id);

    /**
     * <p>isUserUsedInValidatedData.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     * @return a boolean.
     */
    boolean isUserUsedInValidatedData(Integer id);

    /**
     * wrapped method used in referential replacement
     *
     * @param souce a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceUser(PersonDTO souce, PersonDTO target, boolean delete);

    // ---------- DepartmentDTO ----------

    /**
     * La liste des libelles pour la recherche d'une date (entre, avant, apres).
     *
     * @return La liste des valeurs
     */
    List<DepartmentDTO> getDepartments();

    /**
     * <p>getDepartments.</p>
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return a {@link java.util.List} object.
     */
    List<DepartmentDTO> getDepartments(StatusFilter statusFilter);

    /**
     * <p>getDepartmentById.</p>
     *
     * @param departmentId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     */
    DepartmentDTO getDepartmentById(int departmentId);

    /**
     * <p>saveDepartments.</p>
     *
     * @param departments a {@link List} object.
     * @return a {@link java.util.List} object.
     */
    @Transactional()
    void saveDepartments(List<DepartmentDTO> departments);

    /**
     * <p>deleteDepartments.</p>
     *
     * @param departmentIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteDepartments(List<Integer> departmentIds);

    /**
     * <p>isDepartmentUsedInProgram.</p>
     *
     * @param departmentId a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInProgram(int departmentId);

    /**
     * <p>isDepartmentUsedInRules.</p>
     *
     * @param departmentId a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInRules(int departmentId);

    /**
     * <p>isDepartmentUsedInData.</p>
     *
     * @param departmentId a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInData(int departmentId);

    /**
     * <p>isDepartmentUsedInValidatedData.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     * @return a boolean.
     */
    boolean isDepartmentUsedInValidatedData(Integer id);

    /**
     * <p>replaceDepartment.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceDepartment(DepartmentDTO source, DepartmentDTO target, boolean delete);

    /**
     * Search department.
     *
     * @param searchCriteria a {@link fr.ifremer.reefdb.dto.configuration.filter.department.DepartmentCriteriaDTO} object.
     * @return Department list
     */
    List<DepartmentDTO> searchDepartments(DepartmentCriteriaDTO searchCriteria);

    // ---------- LocationDTO ----------

    /**
     * La liste de tous les lieux.
     *
     * @return Les lieux
     */
    List<LocationDTO> getActiveLocations();

    /**
     * Recupere la liste des lieux du referential
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return une liste de lieux du referential
     */
    List<LocationDTO> getLocations(StatusFilter statusFilter);

    /**
     * le lieu selon sont identifiant
     *
     * @param locationId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.LocationDTO} object.
     */
    LocationDTO getLocation(int locationId);

    /**
     * La liste des lieu selectionne suivant une campagne et un programme.
     *
     * @param campaignId  identifiant de la camapgne
     * @param programCode code du programme selectionne
     * @param activeOnly
     * @return La liste des lieux selectionnes
     */
    List<LocationDTO> getLocations(Integer campaignId, String programCode, boolean activeOnly);

    /**
     * <p>saveLocations.</p>
     *
     * @param locations a {@link java.util.List} object.
     */
    @Transactional()
    void saveLocations(List<? extends LocationDTO> locations);

    /**
     * <p>deleteLocations.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteLocations(List<Integer> locationIds);

    /**
     * <p>isLocationUsedInProgram.</p>
     *
     * @param locationId a int.
     * @return a boolean.
     */
    boolean isLocationUsedInProgram(int locationId);

    /**
     * <p>isLocationUsedInData.</p>
     *
     * @param locationId a int.
     * @return a boolean.
     */
    boolean isLocationUsedInData(int locationId);

    /**
     * <p>isLocationUsedInValidatedData.</p>
     *
     * @param locationId a int.
     * @return a boolean.
     */
    boolean isLocationUsedInValidatedData(int locationId);

    /**
     * <p>replaceLocation.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.LocationDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.LocationDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceLocation(LocationDTO source, LocationDTO target, boolean delete);

    /**
     * <p>searchLocations.</p>
     *
     * @param searchCriteria a {@link fr.ifremer.reefdb.dto.configuration.filter.location.LocationCriteriaDTO} object.
     * @return A list of Referential Location
     */
    List<LocationDTO> searchLocations(LocationCriteriaDTO searchCriteria);

    /**
     * <p>getHarbours.</p>
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return a {@link java.util.List} object.
     */
    List<HarbourDTO> getHarbours(StatusFilter statusFilter);

    // ---------- TaxonGroupDTO ----------

    /**
     * La liste des groupes de taxons.
     *
     * @return La liste des groupe de taxons
     */
    List<TaxonGroupDTO> getTaxonGroups();

    /**
     * Get taxon groups associated to the specified taxon at the specified date
     *
     * @param taxon the taxon
     * @return the list of taxon groups
     */
    List<TaxonGroupDTO> getTaxonGroups(TaxonDTO taxon);

    /**
     * <p>getTaxonGroup.</p>
     *
     * @param taxonGroupId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    TaxonGroupDTO getTaxonGroup(int taxonGroupId);

    /**
     * Reload the initial taxon group list from cache (or db)
     *
     * @param taxonGroups a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> getFullTaxonGroups(List<TaxonGroupDTO> taxonGroups);

    /**
     * <p>saveTaxonGroups.</p>
     *
     * @param taxonGroups a {@link java.util.List} object.
     */
    @Transactional()
    void saveTaxonGroups(List<? extends TaxonGroupDTO> taxonGroups);

    /**
     * <p>deleteTaxonGroups.</p>
     *
     * @param taxonGroupIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteTaxonGroups(List<Integer> taxonGroupIds);

    /**
     * <p>isTaxonGroupUsedInReferential.</p>
     *
     * @param taxonGroupId a int.
     * @return a boolean.
     */
    boolean isTaxonGroupUsedInReferential(int taxonGroupId);

    /**
     * <p>isTaxonGroupUsedInData.</p>
     *
     * @param taxonGroupId a int.
     * @return a boolean.
     */
    boolean isTaxonGroupUsedInData(int taxonGroupId);

    /**
     * <p>isTaxonGroupUsedInValidatedData.</p>
     *
     * @param taxonGroupId a int.
     * @return a boolean.
     */
    boolean isTaxonGroupUsedInValidatedData(int taxonGroupId);

    /**
     * <p>replaceTaxonGroup.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceTaxonGroup(TaxonGroupDTO source, TaxonGroupDTO target, boolean delete);

    /**
     * <p>searchTaxonGroups.</p>
     *
     * @param searchCriteria a {@link fr.ifremer.reefdb.dto.configuration.filter.taxongroup.TaxonGroupCriteriaDTO} object.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> searchTaxonGroups(TaxonGroupCriteriaDTO searchCriteria);

    // ---------- TaxonDTO ----------

    /**
     * List of taxon name. Could be filtered by parent taxon group
     *
     * @param taxonGroupId the parent taxon group, or null for all taxons
     * @return list of taxon names
     */
    List<TaxonDTO> getTaxons(Integer taxonGroupId);

    /**
     * <p>getTaxon.</p>
     *
     * @param taxonId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    TaxonDTO getTaxon(int taxonId);

    /**
     * <p>saveTaxons.</p>
     *
     * @param taxons a {@link java.util.List} object.
     */
    @Transactional()
    void saveTaxons(List<? extends TaxonDTO> taxons);

    /**
     * <p>deleteTaxons.</p>
     *
     * @param taxonIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteTaxons(List<Integer> taxonIds);

    /**
     * <p>isTaxonNameUsedInReferential.</p>
     *
     * @param taxonId a int.
     * @return a boolean.
     */
    boolean isTaxonNameUsedInReferential(int taxonId);

    /**
     * <p>isTaxonNameUsedInData.</p>
     *
     * @param taxonId a int.
     * @return a boolean.
     */
    boolean isTaxonNameUsedInData(int taxonId);

    /**
     * <p>isTaxonNameUsedInValidatedData.</p>
     *
     * @param taxonId a int.
     * @return a boolean.
     */
    boolean isTaxonNameUsedInValidatedData(int taxonId);

    /**
     * <p>replaceTaxon.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceTaxon(TaxonDTO source, TaxonDTO target, boolean delete);

    /**
     * <p>searchTaxons.</p>
     *
     * @param taxonCriteria
     * @return a {@link java.util.List} object.
     */
    List<TaxonDTO> searchTaxons(TaxonCriteriaDTO taxonCriteria);

    /**
     * Fill all properties of the taxon list
     *
     * @param taxons a {@link java.util.List} object.
     */
    void fillTaxonsProperties(List<TaxonDTO> taxons);

    /**
     * Fill referent taxon of the taxon list
     *
     * @param taxons a {@link java.util.List} object.
     */
    void fillReferentTaxons(List<TaxonDTO> taxons);

    /**
     * <p>getTaxonomicLevels.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<TaxonomicLevelDTO> getTaxonomicLevels();

    /**
     * <p>getCitations.</p>
     *
     * @return list of taxon citations
     */
    List<CitationDTO> getCitations();

    // ---------- ParameterGroupDTO ----------

    /**
     * All Pmfm parameter group.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return Pmfm parameter group list
     */
    List<ParameterGroupDTO> getParameterGroup(StatusFilter statusFilter);

    // ---------- ParameterDTO ----------

    /**
     * La liste des parametres.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return Les parametres
     */
    List<ParameterDTO> getParameters(StatusFilter statusFilter);

    /**
     * <p>saveParameters.</p>
     *
     * @param parameters a {@link java.util.List} object.
     */
    @Transactional()
    void saveParameters(List<? extends ParameterDTO> parameters);

    /**
     * <p>deleteParameters.</p>
     *
     * @param parametersCodes a {@link java.util.List} object.
     */
    @Transactional()
    void deleteParameters(List<String> parametersCodes);

    /**
     * <p>isParameterUsedInProgram.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isParameterUsedInProgram(String parameterCode);

    /**
     * <p>isParameterUsedInRules.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isParameterUsedInRules(String parameterCode);

    /**
     * <p>isParameterUsedInReferential.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isParameterUsedInReferential(String parameterCode);

    /**
     * <p>replaceParameter.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceParameter(ParameterDTO source, ParameterDTO target, boolean delete);

    /**
     * All Pmfm parameter.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param parameterCode a {@link java.lang.String} object.
     * @param statusCode a {@link java.lang.String} object.
     * @param parameterGroup a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO} object.
     * @return Pmfm Parameter list
     */
    List<ParameterDTO> searchParameters(StatusFilter statusFilter, String parameterCode, String statusCode, ParameterGroupDTO parameterGroup);

    /**
     * The parameterType list
     *
     * @return the parameterTypes
     */
    List<ParameterTypeDTO> getParameterTypes();

    // ---------- MatrixDTO ----------

    /**
     * La liste des supports.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return Les supports
     */
    List<MatrixDTO> getMatrices(StatusFilter statusFilter);

    /**
     * <p>saveMatrices.</p>
     *
     * @param matrices a {@link java.util.List} object.
     */
    @Transactional()
    void saveMatrices(List<? extends MatrixDTO> matrices);

    /**
     * <p>deleteMatrices.</p>
     *
     * @param matrixIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteMatrices(List<Integer> matrixIds);

    /**
     * <p>isMatrixUsedInProgram.</p>
     *
     * @param matrixId a int.
     * @return a boolean.
     */
    boolean isMatrixUsedInProgram(int matrixId);

    /**
     * <p>isMatrixUsedInRules.</p>
     *
     * @param matrixId a int.
     * @return a boolean.
     */
    boolean isMatrixUsedInRules(int matrixId);

    /**
     * <p>isMatrixUsedInReferential.</p>
     *
     * @param matrixId a int.
     * @return a boolean.
     */
    boolean isMatrixUsedInReferential(int matrixId);

    /**
     * <p>replaceMatrix.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceMatrix(MatrixDTO source, MatrixDTO target, boolean delete);

    /**
     * All Pmfm matrice.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param matrixId a {@link java.lang.Integer} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return Pmfm Matrice list
     */
    List<MatrixDTO> searchMatrices(StatusFilter statusFilter, Integer matrixId, String statusCode);

    // ---------- FractionDTO ----------

    /**
     * La liste des fractions.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return Les fractions
     */
    List<FractionDTO> getFractions(StatusFilter statusFilter);

    /**
     * <p>saveFractions.</p>
     *
     * @param fractions a {@link java.util.List} object.
     */
    @Transactional()
    void saveFractions(List<? extends FractionDTO> fractions);

    /**
     * <p>deleteFractions.</p>
     *
     * @param fractionIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteFractions(List<Integer> fractionIds);

    /**
     * <p>isFractionUsedInProgram.</p>
     *
     * @param fractionId a int.
     * @return a boolean.
     */
    boolean isFractionUsedInProgram(int fractionId);

    /**
     * <p>isFractionUsedInRules.</p>
     *
     * @param fractionId a int.
     * @return a boolean.
     */
    boolean isFractionUsedInRules(int fractionId);

    /**
     * <p>isFractionUsedInReferential.</p>
     *
     * @param fractionId a int.
     * @return a boolean.
     */
    boolean isFractionUsedInReferential(int fractionId);

    /**
     * <p>replaceFraction.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceFraction(FractionDTO source, FractionDTO target, boolean delete);

    /**
     * All Pmfm fraction.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param fractionId a {@link java.lang.Integer} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return Pmfm Fraction list
     */
    List<FractionDTO> searchFractions(StatusFilter statusFilter, Integer fractionId, String statusCode);

    // ---------- MethodDTO ----------

    /**
     * La liste des methodes.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return Les methodes
     */
    List<MethodDTO> getMethods(StatusFilter statusFilter);

    /**
     * <p>saveMethods.</p>
     *
     * @param methods a {@link java.util.List} object.
     */
    @Transactional()
    void saveMethods(List<? extends MethodDTO> methods);

    /**
     * <p>deleteMethods.</p>
     *
     * @param methodIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteMethods(List<Integer> methodIds);

    /**
     * <p>isMethodUsedInProgram.</p>
     *
     * @param methodId a int.
     * @return a boolean.
     */
    boolean isMethodUsedInProgram(int methodId);

    /**
     * <p>isMethodUsedInRules.</p>
     *
     * @param methodId a int.
     * @return a boolean.
     */
    boolean isMethodUsedInRules(int methodId);

    /**
     * <p>isMethodUsedInReferential.</p>
     *
     * @param methodId a int.
     * @return a boolean.
     */
    boolean isMethodUsedInReferential(int methodId);

    /**
     * <p>replaceMethod.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceMethod(MethodDTO source, MethodDTO target, boolean delete);

    /**
     * All Pmfm method.
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param methodId a {@link java.lang.Integer} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return Pmfm Method list
     */
    List<MethodDTO> searchMethods(StatusFilter statusFilter, Integer methodId, String statusCode);

    // ---------- PmfmDTO ----------

    /**
     * The quadruplet list
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return the quadruplets
     */
    List<PmfmDTO> getPmfms(StatusFilter statusFilter);

    /**
     * <p>getPmfm.</p>
     *
     * @param pmfmId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO} object.
     */
    PmfmDTO getPmfm(int pmfmId);

    /**
     * <p>savePmfms.</p>
     *
     * @param pmfms a {@link java.util.List} object.
     */
    @Transactional()
    void savePmfms(List<? extends PmfmDTO> pmfms);

    /**
     * <p>deletePmfms.</p>
     *
     * @param pmfmIds a {@link java.util.List} object.
     */
    @Transactional()
    void deletePmfms(List<Integer> pmfmIds);

    /**
     * <p>isPmfmUsedInData.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInData(int pmfmId);

    /**
     * <p>isPmfmUsedInValidatedData.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInValidatedData(int pmfmId);

    /**
     * <p>isPmfmUsedInProgram.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInProgram(int pmfmId);

    /**
     * <p>isPmfmUsedInRules.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInRules(int pmfmId);

    /**
     * <p>replacePmfm.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replacePmfm(PmfmDTO source, PmfmDTO target, boolean delete);

    /**
     * <p>searchPmfms.</p>
     *
     * @param statusFilter a {@link StatusFilter} object.
     * @param parameterCode a {@link String} object.
     * @param matrixId a {@link Integer} object.
     * @param fractionId a {@link Integer} object.
     * @param methodId a {@link Integer} object.
     * @param unitId
     * @param pmfmName a {@link String} object.
     * @param statusCode a {@link String} object.
     * @return a {@link java.util.List} object.
     */
    List<PmfmDTO> searchPmfms(StatusFilter statusFilter, String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, String statusCode);

    // ---------- AnalysisInstrumentDTO ----------

    /**
     * The list of Analysis Instruments
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return The list of Analysis Instruments
     */
    List<AnalysisInstrumentDTO> getAnalysisInstruments(StatusFilter statusFilter);

    /**
     * <p>saveAnalysisInstruments.</p>
     *
     * @param analysisInstruments a {@link java.util.List} object.
     */
    @Transactional()
    void saveAnalysisInstruments(List<? extends AnalysisInstrumentDTO> analysisInstruments);

    /**
     * <p>deleteAnalysisInstruments.</p>
     *
     * @param analysisInstrumentIds a {@link java.util.List} object.
     */
    @Transactional()
    void deleteAnalysisInstruments(List<Integer> analysisInstrumentIds);

    /**
     * Check if this analysis instrument is used in data
     * MEASUREMENT, TAXON_MEASUREMENT, TODO MEASUREMENT_FILE
     *
     * @param analysisInstrumentId a int.
     * @return a boolean.
     */
    boolean isAnalysisInstrumentUsedInData(int analysisInstrumentId);

    /**
     * Check if this analysis instrument is used in validated data
     * MEASUREMENT, TAXON_MEASUREMENT, TODO MEASUREMENT_FILE
     *
     * @param analysisInstrumentId a int.
     * @return a boolean.
     */
    boolean isAnalysisInstrumentUsedInValidatedData(int analysisInstrumentId);

    /**
     * Check if this analysis instrument is used in strategy
     * PMFM_APPLIED_STRATEGY
     *
     * @param analysisInstrumentId a int.
     * @return a boolean.
     */
    boolean isAnalysisInstrumentUsedInProgram(int analysisInstrumentId);

    /**
     * <p>replaceAnalysisInstrument.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO} object.
     * @param delete a boolean.
     */
    @Transactional()
    void replaceAnalysisInstrument(AnalysisInstrumentDTO source, AnalysisInstrumentDTO target, boolean delete);

    /**
     * The search service for analysis instrument
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param analysisInstrumentId a {@link java.lang.Integer} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return A list of analysis instruments
     */
    List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, Integer analysisInstrumentId, String statusCode);

    /**
     * <p>searchAnalysisInstruments.</p>
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param instrumentName a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> searchAnalysisInstruments(StatusFilter statusFilter, String instrumentName);

    // ---------- MISC. ----------

    /**
     * La liste des systemes de positionnement
     *
     * @return La liste des systemes de positionnement
     */
    List<PositioningSystemDTO> getPositioningSystems();

    /**
     * La liste des niveaux.
     *
     * @return La liste des niveaux
     */
    List<LevelDTO> getLevels();

    /**
     * La liste des profondeurs.
     *
     * @return Les profondeurs
     */
    List<DepthDTO> getDepths();

    /**
     * The list of referential status
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return the list of referential status
     */
    List<StatusDTO> getStatus(StatusFilter statusFilter);

    /**
     * The list of Quality Levels
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @return The list of Quality Levels
     */
    List<QualityLevelDTO> getQualityLevels(StatusFilter statusFilter);

    /**
     * The search service for quality level
     *
     * @param statusFilter a {@link fr.ifremer.reefdb.service.StatusFilter} object.
     * @param qualityLevelCode a {@link java.lang.String} object.
     * @param statusCode a {@link java.lang.String} object.
     * @return A list of quality level
     */
    List<QualityLevelDTO> searchQualityLevels(StatusFilter statusFilter, String qualityLevelCode, String statusCode);

    /**
     * The service for grouping type
     *
     * @return A list of grouping type
     */
    List<GroupingTypeDTO> getGroupingTypes();

    /**
     * <p>getPhotoTypes.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PhotoTypeDTO> getPhotoTypes();

    /**
     * Return the unique pmfm id from the quadruplet in the parameter pmfm
     *
     * @param pmfm the pmfm filter
     * @return the pmfm id
     */
    Integer getUniquePmfmIdFromPmfm(PmfmDTO pmfm);

    /**
     * Return the unique pmfm from the quadruplet in the parameter pmfm
     *
     * @param pmfm the pmfm filter
     * @return the pmfm
     */
    PmfmDTO getUniquePmfmFromPmfm(PmfmDTO pmfm);

    // ---------- QualitativeValueDTO ----------

    /**
     * Get the qualitative value by its Id
     *
     * @param qualitativeValueId the qualitative value id
     * @return Qualitative Value
     */
    QualitativeValueDTO getQualitativeValue(int qualitativeValueId);

    List<QualitativeValueDTO> getQualitativeValues(Collection<Integer> qualitativeValueIds);

    List<QualitativeValueDTO> getQualitativeValues(String parameterCode);
}


