package fr.ifremer.reefdb.service.observation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumPmfmDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Le sevice pour les donnees de type Survey, SamplingOperation, Measurement
 */
@Transactional(readOnly = true)
public interface ObservationService {

    /**
     * La liste des observations selectionnees suivant la campagne, le programme, le lieu, la date de début, la date de fin
     *
     * @param surveyFilter les parametres de recherche selectioné
     * @return La liste des observations selectionnees
     */
    List<SurveyDTO> getSurveys(SurveyFilterDTO surveyFilter);

    /**
     * Recherche du detail d'une observation pour l'ecran detail via son identifiant.
     *
     * @param surveyId l identifiant de l observation
     * @return Le detail de l'observation
     */
    SurveyDTO getSurvey(Integer surveyId);

    /**
     * <p>getSurveyWithoutPmfmFiltering.</p>
     *
     * @param surveyId a {@link java.lang.Integer} object.
     * @return a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     */
    SurveyDTO getSurveyWithoutPmfmFiltering(Integer surveyId);

    /**
     * Save home observations.
     *  @param surveys          Observations
     * @param progressionModel progression model
     */
    @Transactional()
    void saveSurveys(Collection<? extends SurveyDTO> surveys, ProgressionCoreModel progressionModel);

    /**
     * Save detail observation.
     *
     * @param survey Observation
     */
    @Transactional()
    void saveSurvey(SurveyDTO survey);

    /**
     * Delete observations.
     *
     * @param surveyIds Observations to delete
     */
    @Transactional()
    void deleteSurveys(List<Integer> surveyIds);

    /**
     * Load all sampling operations of the survey and fill them inside the survey itself
     *
     * @param survey                     the survey to load
     * @param withIndividualMeasurements a boolean.
     */
    void loadSamplingOperationsFromSurvey(SurveyDTO survey, boolean withIndividualMeasurements);

    /**
     * <p>newSamplingOperation.</p>
     *
     * @param survey a {@link fr.ifremer.reefdb.dto.data.survey.SurveyDTO} object.
     * @param pmfms  a {@link java.util.List} object.
     * @return a {@link fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO} object.
     */
    SamplingOperationDTO newSamplingOperation(SurveyDTO survey, List<PmfmDTO> pmfms);

    /**
     * Duplicate survey.
     *
     * @param survey          survey to duplicate
     * @param fullDuplication enable full duplication
     * @param copyCoordinates enable copy coordinates
     * @param duplicateSurveyMeasurements enable copy survey ungrouped measurements
     * @return the duplicated survey
     */
    SurveyDTO duplicateSurvey(SurveyDTO survey, boolean fullDuplication, boolean copyCoordinates, boolean duplicateSurveyMeasurements);

    /**
     * <p>getAvailablePrograms.</p>
     *
     * @param campaignId     a {@link Integer} object.
     * @param locationId     a {@link Integer} object.
     * @param date           a {@link LocalDate} object.
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<ProgramDTO> getAvailablePrograms(Integer campaignId, Integer locationId, LocalDate date, boolean forceNoContext, boolean writableOnly);

    List<CampaignDTO> getAvailableCampaigns(LocalDate date, boolean forceNoContext);

    /**
     * <p>getAvailableAnalysisInstruments.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> getAvailableAnalysisInstruments(boolean forceNoContext);

    /**
     * <p>getAvailableSamplingEquipments.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<SamplingEquipmentDTO> getAvailableSamplingEquipments(boolean forceNoContext);

    /**
     * <p>getAvailableLocations.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<LocationDTO> getAvailableLocations(boolean forceNoContext);

    /**
     * <p>getAvailableLocations.</p>
     *
     * @param campaignId     a {@link Integer} object.
     * @param programCode    a {@link String} object.
     * @param forceNoContext a boolean.
     * @param activeOnly
     * @return a {@link java.util.List} object.
     */
    List<LocationDTO> getAvailableLocations(Integer campaignId, String programCode, boolean forceNoContext, boolean activeOnly);

    /**
     * <p>getAvailableTaxonGroups.</p>
     *
     * @param taxon          a {@link TaxonDTO} object.
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> getAvailableTaxonGroups(TaxonDTO taxon, boolean forceNoContext);

    /**
     * <p>getAvailableTaxonGroups.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> getAvailableTaxonGroups(boolean forceNoContext);

    /**
     * <p>getAvailableTaxons.</p>
     *
     * @param taxonGroup     a {@link TaxonGroupDTO} object.
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<TaxonDTO> getAvailableTaxons(TaxonGroupDTO taxonGroup, boolean forceNoContext);

    /**
     * <p>getAvailableTaxons.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<TaxonDTO> getAvailableTaxons(boolean forceNoContext);

    /**
     * <p>getAvailablePmfms.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<PmfmDTO> getAvailablePmfms(boolean forceNoContext);

    /**
     * <p>getAvailableDepartments.</p>
     *
     * @param forceNoContext a boolean.
     * @return a {@link java.util.List} object.
     */
    List<DepartmentDTO> getAvailableDepartments(boolean forceNoContext);

    /**
     * <p>getAvailableUsers.</p>
     *
     * @param forceNoFilter a boolean.
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getAvailableUsers(boolean forceNoFilter);

    /**
     * <p>isDataReadyToSynchronize.</p>
     *
     * @return a boolean.
     */
    boolean isDataReadyToSynchronize();

    /**
     * <p>validateSurveys.</p>
     * new method to avoid memory limit reach (see Mantis #38832 &nd #42154)
     *  @param surveys           a {@link Collection} object.
     * @param validationComment a {@link String} object.
     * @param progressionModel  progression model
     */
    @Transactional(propagation = Propagation.NEVER)
    void validateSurveys(Collection<? extends SurveyDTO> surveys, String validationComment, ProgressionCoreModel progressionModel);

    /**
     * <p>unvalidateSurveys.</p>
     *  @param surveys             a {@link Collection} object.
     * @param unvalidationComment a {@link String} object.
     * @param progressionModel    progression model
     */
    @Transactional(propagation = Propagation.NEVER)
    void unvalidateSurveys(Collection<? extends SurveyDTO> surveys, String unvalidationComment, ProgressionCoreModel progressionModel);

    boolean isQualified(SurveyDTO survey);

    /**
     * <p>countSurveysWithProgramAndLocations.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @param locationIds a {@link java.util.List} object.
     * @return a {@link java.lang.Long} object.
     */
    Long countSurveysWithProgramAndLocations(String programCode, List<Integer> locationIds);

    Long countSurveysWithProgramLocationAndOutsideDates(String programCode, int appliedStrategyId, int locationId, LocalDate startDate, LocalDate endDate);

    Long countSurveysWithProgramLocationAndInsideDates(String programCode, int appliedStrategyId, int locationId, LocalDate startDate, LocalDate endDate);

    Long countSurveysWithProgramLocationAndOutsideDates(ProgStratDTO progStrat, int locationId);

    Long countSurveysWithCampaign(int campaignId);

    Set<MoratoriumPmfmDTO> getPmfmsUnderMoratorium(SurveyDTO survey);
}
