package fr.ifremer.reefdb.service.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.administration.user.PrivilegeCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.Encryption;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.person.PersonCriteriaDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * <p>UserServiceImpl class.</p>
 *
 */
@Service("reefDbUserService")
public class UserServiceImpl extends fr.ifremer.quadrige3.core.service.administration.user.UserServiceImpl implements UserService {

    @Autowired
    protected CacheService cacheService;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "reefDbDepartmentDao")
    private ReefDbDepartmentDao departmentDao;

    /** {@inheritDoc} */
    @Override
    public Integer getDepartmentIdByUserId(int userId) {
        return quserDao.getDepartmentIdByUserId(userId);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean isLoginExtranet(String login) {
        Assert.notBlank(login);
        return quserDao.isLoginExtranet(login);
    }

    /** {@inheritDoc} */
    @Override
    public boolean hasPassword(String login) {
        Assert.notBlank(login);
        return quserDao.hasPassword(login);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isLocalUserWithNoPassword(String login) {
        Assert.notBlank(login);
        return quserDao.isLocalUserWithNoPassword(login);
    }

    /** {@inheritDoc} */
    @Override
    public void updatePasswordByUserId(int personId, String password) {
        Assert.notBlank(password);
        quserDao.updatePasswordByUserId(personId, Encryption.sha(password));
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> getActiveUsers() {
        return quserDao.getAllUsers(StatusFilter.ACTIVE.toStatusCodes());
    }

    /** {@inheritDoc} */
    @Override
    public void resetPassword(String login) {
        Assert.notBlank(login);
        quserDao.resetPassword(login);
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> searchUser(PersonCriteriaDTO searchCriteria) {
        StatusFilter statusFilter = StatusFilter.toLocalOrNational(searchCriteria.getLocal());
        List<String> statusCodes = statusFilter.intersect(searchCriteria.getStatus());
        Integer departmentId = searchCriteria.getDepartment() == null ?
                null : searchCriteria.getDepartment().getId();
        String privilegeCode = searchCriteria.getPrivilege() == null ?
                null : searchCriteria.getPrivilege().getCode();
        return quserDao.findUsersByCriteria(
                searchCriteria.getName(),
                searchCriteria.getFirstName(),
                searchCriteria.isStrictName(),
                searchCriteria.getLogin(),
                departmentId,
                privilegeCode, statusCodes);
    }

    /** {@inheritDoc} */
    @Override
    public List<PrivilegeDTO> getAllPrivileges() {
        return quserDao.getAllPrivileges();
    }

    /** {@inheritDoc} */
    @Override
    public List<PrivilegeDTO> getAvailablePrivileges() {
        List<PrivilegeDTO> availablePrivileges = Lists.newArrayList();
        for (PrivilegeDTO privilege : getAllPrivileges()) {
            if (PrivilegeCode.LOCAL_ADMINISTRATOR.getValue().equals(privilege.getCode())
                    || PrivilegeCode.QUALIFIER.getValue().equals(privilege.getCode())) {
                availablePrivileges.add(privilege);
            }
        }
        return availablePrivileges;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PrivilegeDTO> getPrivilegesByUser(Integer userId) {
        return quserDao.getPrivilegesByUserId(userId);
    }

    /** {@inheritDoc} */
    @Override
    public void saveUsers(List<PersonDTO> users) {
        for (PersonDTO user : users) {
            if (user.isDirty()) {
                quserDao.saveTemporaryUser(user);
                user.setDirty(false);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void deleteUsers(List<Integer> personIds) {
        quserDao.deleteTemporaryUser(personIds);
    }

    /** {@inheritDoc} */
    @Override
    public void replaceUser(PersonDTO source, PersonDTO target, boolean delete) {

        Assert.notNull(source);
        Assert.notNull(target);

        Integer sourceQuserId = source.getId();
        Integer targetQuserId = target.getId();
        Assert.isTrue(sourceQuserId < 0, String.format("Can't replace a user with a positive id %s", sourceQuserId));
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), String.format("Can't replace a matrix with non local status %s", sourceQuserId));

        if (!sourceQuserId.equals(targetQuserId)) {

            // replace occurences
            quserDao.replaceTemporaryUserFks(sourceQuserId, targetQuserId, delete);

        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInProgram(int userId) {
        return quserDao.isUserUsedInProgram(userId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInRules(int userId) {
        return quserDao.isUserUsedInRules(userId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInData(int userId) {
        return quserDao.isUserUsedInData(userId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInValidatedData(int userId) {
        return quserDao.isUserUsedInValidatedData(userId);
    }

}
