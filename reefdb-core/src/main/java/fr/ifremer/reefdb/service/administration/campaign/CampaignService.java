package fr.ifremer.reefdb.service.administration.campaign;

/*-
 * #%L
 * ReefDb :: Core
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.reefdb.dto.SearchDateDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author peck7 on 02/08/2017.
 */
@Transactional(readOnly = true)
public interface CampaignService {

    List<CampaignDTO> getAllCampaigns();

    List<CampaignDTO> findCampaignsByCriteria(String name, SearchDateDTO startDateSearch, Date startDate1, Date startDate2, SearchDateDTO endDateSearch, Date endDate1, Date endDate2);

    List<CampaignDTO> findCampaignsIncludingDate(Date date);

    boolean checkCampaignNameDuplicates(Integer id, String name);

    @Transactional
    void saveCampaigns(AuthenticationInfo authenticationInfo, Collection<? extends CampaignDTO> campaigns);

    @Transactional
    void deleteCampaign(AuthenticationInfo authenticationInfo, List<Integer> campaignIds);

    boolean isCampaignUsedByFilter(int campaignId);

    Long countSurveysWithCampaign(int campaignId);
}
