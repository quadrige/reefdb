package fr.ifremer.reefdb.service.administration.program;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.core.service.administration.program.ProgramServiceImpl;
import fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRestClientService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.program.ReefDbProgramDao;
import fr.ifremer.reefdb.dao.administration.strategy.ReefDbStrategyDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dao.system.filter.ReefDbFilterDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.*;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.administration.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Service need for programs and strategy.
 */
@Service("reefdbProgramStrategyService")
public class ProgramStrategyServiceImpl
        extends ProgramServiceImpl
        implements ProgramStrategyService {

    /* Logger */
    private static final Log LOG = LogFactory.getLog(ProgramStrategyServiceImpl.class);
    private static final Predicate<PmfmStrategyDTO> PMFM_FOR_SURVEY_PREDICATE = object -> object.isSurvey() && !object.isGrouping();
    private static final Predicate<PmfmStrategyDTO> GROUPED_PMFM_FOR_SURVEY_PREDICATE = object -> object.isSurvey() && object.isGrouping();
    private static final Predicate<PmfmStrategyDTO> PMFM_FOR_SAMPLING_OPERATION_PREDICATE = object -> object.isSampling() && !object.isGrouping();
    private static final Predicate<PmfmStrategyDTO> GROUPED_PMFM_FOR_SAMPLING_OPERATION_PREDICATE = object -> object.isSampling() && object.isGrouping();
    @Resource(name = "reefDbProgramDao")
    protected ReefDbProgramDao programDao;
    @Resource(name = "reefDbStrategyDao")
    protected ReefDbStrategyDao strategyDao;
    @Resource(name = "reefDbMonitoringLocationDao")
    protected ReefDbMonitoringLocationDao locationDao;
    @Resource(name = "reefDbUserService")
    protected UserService userService;
    @Resource(name = "synchroRestClientService")
    protected SynchroRestClientService synchroRestClientService;
    @Resource(name = "reefDbFilterDao")
    protected ReefDbFilterDao filterDao;
    @Resource
    protected ReefDbConfiguration config;
    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Override
    public List<ProgramDTO> getProgramsByCodes(List<String> programCodes) {
        Assert.notEmpty(programCodes);
        List<ProgramDTO> programs = programDao.getProgramsByCodes(programCodes);
        programs.forEach(program -> program.getErrors().clear());
        return programs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> searchPrograms(String programCode, String programName, StatusFilter statusFilter) {
        return filterProgramDTOsByConfig(
                programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), programCode, programName)
        );
    }

    @Override
    public List<ProgramDTO> getReadableProgramsByUserAndStatus(int userId, StatusFilter statusFilter) {
        Assert.notNull(statusFilter);

        return filterReadablePrograms(
            userId,
            programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null)
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getWritableProgramsByUserAndStatus(int userId, StatusFilter statusFilter) {
        Assert.notNull(statusFilter);

        List<ProgramDTO> programsByStatus = programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null);
        return filterWritablePrograms(userId, programsByStatus);
    }

    @Override
    public List<ProgramDTO> getReadableProgramsByCampaignId(Integer campaignId) {
        return filterReadablePrograms(programDao.getProgramsByCampaignId(campaignId));
    }

    @Override
    public List<ProgramDTO> getWritableProgramsByCampaignId(Integer campaignId) {
        return filterWritablePrograms(programDao.getProgramsByCampaignId(campaignId));
    }

    @Override
    public List<ProgramDTO> getReadablePrograms() {
        // Return programs with read rights for current user
        return filterReadablePrograms(programDao.getAllPrograms());
    }

    @Override
    public List<ProgramDTO> getWritablePrograms() {
        // Return programs with write rights for current user
        return filterWritablePrograms(programDao.getAllPrograms());
    }

    @Override
    public List<ProgramDTO> getReadableProgramsByLocationAndDate(Integer locationId, Date date) {
        return filterReadablePrograms(programDao.findProgramsByLocationAndDate(StatusFilter.ACTIVE.toStatusCodes(), locationId, date));
    }

    @Override
    public List<ProgramDTO> getWritableProgramsByLocationAndDate(Integer locationId, Date date) {
        return filterWritablePrograms(programDao.findProgramsByLocationAndDate(StatusFilter.ACTIVE.toStatusCodes(), locationId, date));
    }

    @Override
    public ProgramDTO getReadableProgramByCode(String programCode) {
        return filterReadableProgram(programDao.getProgramByCode(programCode));
    }

    @Override
    public ProgramDTO getWritableProgramByCode(String programCode) {
        return filterWritableProgram(programDao.getProgramByCode(programCode));
    }

    @Override
    public List<ProgramDTO> getManagedPrograms() {
        return getManagedProgramsByUser(SecurityContextHelper.getQuadrigeUserId());
    }

    @Override
    public List<ProgramDTO> getManagedProgramsByUser(int userId) {
        return filterManagedPrograms(userId, programDao.getAllPrograms());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getManagedProgramsByUserAndStatus(int userId, StatusFilter statusFilter) {
        Assert.notNull(statusFilter);

        List<ProgramDTO> programsForStatus = programDao.findProgramsByCodeAndName(statusFilter.toStatusCodes(), null, null);
        return filterManagedPrograms(userId, programsForStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isProgramUsedByRuleList(String programCode) {
        return programDao.isProgramUsedByRuleList(programCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isProgramUsedByExtraction(String programCode) {

        // search the program in all existing extraction filters
        List<FilterDTO> programFilters = filterDao.getAllExtractionFilters(ExtractionFilterValues.PROGRAM.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(programFilters)) {
            for (FilterDTO programFilter : programFilters) {
                List<String> programCodes = filterDao.getFilteredElementsByFilterId(programFilter.getId());
                if (programCodes.contains(programCode)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isProgramUsedByFilter(String programCode) {

        // search the program in all existing context filters
        List<FilterDTO> programFilters = filterDao.getAllContextFilters(null, FilterTypeValues.PROGRAM.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(programFilters)) {
            for (FilterDTO programFilter : programFilters) {
                List<String> programCodes = filterDao.getFilteredElementsByFilterId(programFilter.getId());
                if (programCodes.contains(programCode)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveStrategiesByProgramAndLocation(List<ProgStratDTO> strategies, int locationId) {

        if (CollectionUtils.isEmpty(strategies)) {
            return;
        }

        for (ProgStratDTO strategy : strategies) {
            strategyDao.saveStrategyByLocation(strategy, locationId);
        }

    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void loadStrategiesAndLocations(ProgramDTO program) {
        Assert.notNull(program);
        Assert.notBlank(program.getCode());

        // Load strategies
        if (!program.isStrategiesLoaded()) {
            List<StrategyDTO> strategies = new ArrayList<>(strategyDao.getStrategiesByProgramCode(program.getCode()));
            program.setStrategies(strategies);
            program.setStrategiesLoaded(true);

            // Load applied strategies
            if (!program.isStrategiesEmpty()) {
                Multimap<Integer, AppliedStrategyDTO> allAppliedPeriods = strategyDao.getAllAppliedPeriodsByProgramCode(program.getCode());
                for (StrategyDTO strategy : program.getStrategies()) {
                    if (strategy.getId() != null && !strategy.isAppliedStrategiesLoaded()) {
                        Collection<AppliedStrategyDTO> appliedStrategies = allAppliedPeriods.get(strategy.getId());
                        strategy.setAppliedStrategies(new ArrayList<>(appliedStrategies));
                        strategy.setAppliedStrategiesLoaded(true);
                    }
                }
            }
        }

        // Load program's location
        if (!program.isLocationsLoaded()) {
            List<LocationDTO> locations = locationDao.getLocationsByCampaignAndProgram(null, program.getCode(), StatusFilter.ALL);
            program.setLocations(new ArrayList<>(locations));
            program.setLocationsLoaded(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadPmfmStrategy(StrategyDTO strategy) {
        Assert.notNull(strategy);

        if (strategy.getId() == null) {
            return;
        }

        if (!strategy.isPmfmStrategiesLoaded()) {
            List<PmfmStrategyDTO> pmfms = strategyDao.getPmfmsAppliedStrategy(strategy.getId());
            strategy.setPmfmStrategies(new ArrayList<>(pmfms));
            strategy.setPmfmStrategiesLoaded(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePrograms(List<? extends ProgramDTO> programs) {
        savePrograms(null, programs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void savePrograms(AuthenticationInfo authenticationInfo, List<? extends ProgramDTO> programs) {

        if (CollectionUtils.isEmpty(programs)) {
            return;
        }

        Set<String> remoteProgramCodes = Sets.newHashSet();
        for (ProgramDTO programToSave : programs) {

            // don't try to save an unmodified program
            if (!programToSave.isDirty()) {
                continue;
            }

            // Store remote program codes
            boolean isRemoteProgram = !ReefDbBeans.isLocalStatus(programToSave.getStatus());
            if (isRemoteProgram) {
                if (authenticationInfo == null) {
                    throw new ReefDbTechnicalException("Could not save a remote program: authentication is required to connect to synchro server");
                }

                remoteProgramCodes.add(programToSave.getCode());
            }

            // save
            programDao.saveProgram(programToSave);

            // reset dirty flag
            programToSave.setDirty(false);
            programToSave.setNewCode(false);
            programToSave.setStrategiesLoaded(false);
            programToSave.setLocationsLoaded(false);
        }

        // Save remote programs, using synchro server
        if (CollectionUtils.isNotEmpty(remoteProgramCodes)) {
            saveProgramOnServer(authenticationInfo, remoteProgramCodes);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadAppliedPeriods(ProgramDTO program, StrategyDTO strategy) {
        Assert.notNull(program);
        Assert.notNull(strategy);

        List<AppliedStrategyDTO> appliedStrategies;
        Map<Integer, LocationDTO> missingProgramMonitoringLocationIds = ReefDbBeans.mapById(program.getLocations());

        // If first load of this strategy
        if (strategy.getId() != null && !strategy.isAppliedStrategiesLoaded()) {
            // Load strategy's appliedPeriods
            appliedStrategies = new ArrayList<>(strategyDao.getAppliedPeriodsByStrategyId(strategy.getId()));

            // Add missing program's locations
            for (AppliedStrategyDTO appliedStrategy : appliedStrategies) {
                missingProgramMonitoringLocationIds.remove(appliedStrategy.getId());
            }

        }

        // Refresh the strategy (program could have new location)
        else {
            appliedStrategies = Lists.newArrayList();

            // Detect new/removed program's location
            for (AppliedStrategyDTO appliedStrategy : strategy.getAppliedStrategies()) {
                Integer monitoringLocationId = appliedStrategy.getId();

                // Only add location that exists in program's locations
                if (missingProgramMonitoringLocationIds.containsKey(monitoringLocationId)) {
                    appliedStrategies.add(appliedStrategy);
                    missingProgramMonitoringLocationIds.remove(appliedStrategy.getId());
                }
            }

        }

        // Add locations that exist in program but not in strategy
        if (!missingProgramMonitoringLocationIds.isEmpty()) {
            for (LocationDTO missingLocation : missingProgramMonitoringLocationIds.values()) {
                AppliedStrategyDTO newAppliedStrategy = ReefDbBeans.locationToAppliedStrategyDTO(missingLocation);
                // applied strategy ID, and dates remain null
                appliedStrategies.add(newAppliedStrategy);
            }
        }

        // Apply to the strategy
        strategy.setAppliedStrategies(appliedStrategies);
        strategy.setAppliedStrategiesLoaded(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmStrategyDTO> getPmfmStrategiesBySurvey(SurveyDTO survey) {

        if (survey == null || survey.getProgram() == null || StringUtils.isBlank(survey.getProgram().getCode())
                || survey.getLocation() == null || survey.getLocation().getId() == null
                || survey.getDate() == null) {
            return null;
        }

        Set<PmfmStrategyDTO> pmfmStrategies = strategyDao.getPmfmStrategiesByProgramCodeAndLocation(
                survey.getProgram().getCode(),
                survey.getLocation().getId(),
                survey.getDate());

        // Replace qualitative value in pmfm by those restricted by pmfm strategy
        pmfmStrategies.forEach(pmfmStrategyDTO -> {
            PmfmDTO pmfmForDataInput = ReefDbBeans.clone(pmfmStrategyDTO.getPmfm());
            pmfmForDataInput.setQualitativeValues(pmfmStrategyDTO.getQualitativeValues());
            pmfmStrategyDTO.setPmfm(pmfmForDataInput);
        });

        // Sort by rankOrder
        return pmfmStrategies.stream()
            .sorted(Comparator.nullsLast(Comparator.comparingInt(PmfmStrategyDTO::getRankOrder)))
            .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DepartmentDTO getDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey) {

        if (survey == null || survey.getProgram() == null || StringUtils.isBlank(survey.getProgram().getCode())
                || survey.getLocation() == null || survey.getLocation().getId() == null
                || survey.getDate() == null) {
            return null;
        }

        List<ProgStratDTO> appliedStrategies = strategyDao.getStrategiesByProgramCodeAndMonitoringLocationId(survey.getProgram().getCode(), survey.getLocation().getId());

        for (ProgStratDTO appliedStrategy : appliedStrategies) {
            // also filter on date
            if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null
                    && Dates.isBetween(survey.getDate(), appliedStrategy.getStartDate(), appliedStrategy.getEndDate())
                    && appliedStrategy.getDepartment() != null) {

                // return first not null department found
                return appliedStrategy.getDepartment();
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Integer, DepartmentDTO> getAnalysisDepartmentsByPmfmMapBySurvey(SurveyDTO survey) {

        // Get pmfm strategies
        List<PmfmStrategyDTO> pmfmStrategies = getPmfmStrategiesBySurvey(survey);
        if (CollectionUtils.isEmpty(pmfmStrategies)) {
            return null;
        }

        Map<Integer, DepartmentDTO> result = Maps.newHashMap();

        // Extract each analysis department, and store it by pmdm id
        for (PmfmStrategyDTO pmfmStrategyDTO : pmfmStrategies) {
            if (pmfmStrategyDTO.getPmfm() != null
                    && pmfmStrategyDTO.getPmfm().getId() != null
                    && pmfmStrategyDTO.getAnalysisDepartment() != null) {
                Integer pmfmId = pmfmStrategyDTO.getPmfm().getId();
                if (!result.containsKey(pmfmId)) {
                    result.put(pmfmId, pmfmStrategyDTO.getAnalysisDepartment());
                }
            }
        }

        return result;
    }

    @Override
    public DepartmentDTO getAnalysisDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey) {

        return getAnalysisDepartmentOfAppliedStrategyBySurvey(survey, null);
    }

    @Override
    public DepartmentDTO getAnalysisDepartmentOfAppliedStrategyBySurvey(SurveyDTO survey, Collection<PmfmDTO> pmfms) {

        if (survey == null || survey.getProgram() == null || StringUtils.isBlank(survey.getProgram().getCode())
                || survey.getLocation() == null || survey.getLocation().getId() == null
                || survey.getDate() == null) {
            return null;
        }

        List<ProgStratDTO> appliedStrategies = strategyDao.getStrategiesByProgramCodeAndMonitoringLocationId(survey.getProgram().getCode(), survey.getLocation().getId());

        for (ProgStratDTO appliedStrategy : appliedStrategies) {
            // also filter on date
            if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null
                    && Dates.isBetween(survey.getDate(), appliedStrategy.getStartDate(), appliedStrategy.getEndDate())) {

                DepartmentDTO analysisDepartment = CollectionUtils.isEmpty(pmfms)
                        ? strategyDao.getAnalysisDepartmentByAppliedStrategyId(appliedStrategy.getAppliedStrategyId())
                        : strategyDao.getAnalysisDepartmentByAppliedStrategyIdAndPmfmIds(
                        appliedStrategy.getAppliedStrategyId(),
                        pmfms.stream().map(PmfmDTO::getId).collect(Collectors.toList()));

                // return first not null department found
                if (analysisDepartment != null)
                    return analysisDepartment;
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfmsForSurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, PMFM_FOR_SURVEY_PREDICATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getGroupedPmfmsForSurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, GROUPED_PMFM_FOR_SURVEY_PREDICATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getPmfmsForSamplingOperationBySurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, PMFM_FOR_SAMPLING_OPERATION_PREDICATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getGroupedPmfmsForSamplingOperationBySurvey(SurveyDTO survey) {
        return getPredicatedPmfmsBySurvey(survey, GROUPED_PMFM_FOR_SAMPLING_OPERATION_PREDICATE);
    }

    private List<PmfmDTO> getPredicatedPmfmsBySurvey(SurveyDTO survey, Predicate<PmfmStrategyDTO> predicate) {

        Assert.notNull(survey);

        List<PmfmStrategyDTO> pmfmStrategies = getPmfmStrategiesBySurvey(survey);

        return CollectionUtils.isNotEmpty(pmfmStrategies)
                ? pmfmStrategies.stream().filter(predicate).map(PmfmStrategyDTO::getPmfm).collect(Collectors.toList())
                : new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgStratDTO> getStrategyUsageByLocationId(Integer locationId) {
        return strategyDao.getStrategiesByProgramCodeAndMonitoringLocationId(null, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgStratDTO> getStrategyUsageByProgramAndLocationId(String programCode, Integer locationId) {
        return strategyDao.getStrategiesByProgramCodeAndMonitoringLocationId(programCode, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePrograms(List<String> programCodes) {
        Assert.notNull(programCodes);
        programCodes.forEach(programCode -> programDao.remove(programCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StrategyDTO duplicateStrategy(final StrategyDTO strategy, ProgramDTO targetProgram, boolean skipLocalReferential) {
        Assert.notNull(strategy);
        Assert.notNull(targetProgram);

        // Strategy duplicated
        final StrategyDTO duplicateStrategy = ReefDbBeans.clone(strategy);
        duplicateStrategy.setAppliedStrategies(ReefDbBeans.clone(strategy.getAppliedStrategies()));
        duplicateStrategy.setPmfmStrategies(ReefDbBeans.clone(strategy.getPmfmStrategies()));

        // Remove ID and unused properties
        duplicateStrategy.setId(null);
        duplicateStrategy.setName(null);
        duplicateStrategy.setErrors(null);

        // Add locations to Program
        if (!duplicateStrategy.isAppliedStrategiesEmpty()) {

            List<Integer> programLocationIds = ReefDbBeans.collectIds(targetProgram.getLocations());
            ListIterator<AppliedStrategyDTO> appliedStrategyIterator = duplicateStrategy.getAppliedStrategies().listIterator();
            while (appliedStrategyIterator.hasNext()) {
                AppliedStrategyDTO appliedStrategy = appliedStrategyIterator.next();

                if (skipLocalReferential) {
                    // remove local location
                    if (ReefDbBeans.isLocalStatus(appliedStrategy.getStatus())) {
                        appliedStrategyIterator.remove();
                        continue;
                    }

                    // reset local department
                    if (appliedStrategy.getDepartment() != null && ReefDbBeans.isLocalStatus(appliedStrategy.getDepartment().getStatus())) {
                        appliedStrategy.setDepartment(null);
                    }

                }

                // reset properties
                appliedStrategy.setAppliedStrategyId(null);
                appliedStrategy.setStartDate(null);
                appliedStrategy.setEndDate(null);
                appliedStrategy.setPreviousStartDate(null);
                appliedStrategy.setPreviousEndDate(null);
                appliedStrategy.setDepartment(null);

                // add to program location
                if (!programLocationIds.contains(appliedStrategy.getId())) {
                    targetProgram.addLocations(appliedStrategy);
                }

            }

        }

        if (!duplicateStrategy.isPmfmStrategiesEmpty()) {
            ListIterator<PmfmStrategyDTO> pmfmStrategyIterator = duplicateStrategy.getPmfmStrategies().listIterator();
            while (pmfmStrategyIterator.hasNext()) {
                PmfmStrategyDTO pmfmStrategy = pmfmStrategyIterator.next();

                if (skipLocalReferential) {
                    // remove this pmfm strategy if the pmfm is local
                    if (pmfmStrategy.getPmfm() != null && ReefDbBeans.isLocalStatus(pmfmStrategy.getPmfm().getStatus())) {
                        pmfmStrategyIterator.remove();
                        continue;
                    }

                    // reset the local department
                    if (pmfmStrategy.getAnalysisDepartment() != null && ReefDbBeans.isLocalStatus(pmfmStrategy.getAnalysisDepartment().getStatus())) {
                        pmfmStrategy.setAnalysisDepartment(null);
                    }
                }
                pmfmStrategy.setId(null);
            }
        }

        // Return value duplicated
        return duplicateStrategy;
    }


    @Override
    public List<ProgramDTO> getRemoteReadableProgramsByUser(AuthenticationInfo authenticationInfo) {
        List<ProgramVO> programs = synchroRestClientService.getReadableProgramsForUser(authenticationInfo);
        return toProgramDTOs(programs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getRemoteWritableProgramsByUser(AuthenticationInfo authenticationInfo) {
        List<ProgramVO> programs = synchroRestClientService.getWritableProgramsForUser(authenticationInfo);
        return toProgramDTOs(programs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasRemoteReadOnProgram(AuthenticationInfo authenticationInfo) {
        return CollectionUtils.isNotEmpty(getRemoteReadableProgramsByUser(authenticationInfo));
    }

    @Override
    public boolean hasRemoteWriteOnProgram(AuthenticationInfo authenticationInfo) {
        return CollectionUtils.isNotEmpty(getRemoteWritableProgramsByUser(authenticationInfo));
    }

    @Override
    public ProgramDTO isProgramExists(String programCode) {
        List<ProgramDTO> foundPrograms = programDao.findProgramsByCodeAndName(StatusFilter.ALL.toStatusCodes(), programCode, null);
        return CollectionUtils.isEmpty(foundPrograms)
                ? null
                : foundPrograms.get(0);
    }

    @Override
    public boolean hasPotentialMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters) {
        return !getMoratoriums(programCodes, periodFilters, true).isEmpty();
    }

    @Override
    public List<MoratoriumDTO> getMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters) {
        return getMoratoriums(programCodes, periodFilters, false);
    }

    /* -- Internal methods -- */

    private List<MoratoriumDTO> getMoratoriums(Collection<String> programCodes, Collection<ExtractionPeriodDTO> periodFilters, boolean onlyPotential) {
        List<MoratoriumDTO> result = new ArrayList<>();
        // Get program by calling get method for each, using cache
        List<ProgramDTO> programs = programCodes.stream().map(programCode -> programDao.getProgramByCode(programCode)).collect(Collectors.toList());

        programs.stream()
            .filter(program -> !program.isMoratoriumsEmpty())
            // if only potential, filter by user privilege
            .filter(program -> !onlyPotential
                || dataContext.isRecorderAdministrator()
                || ReefDbBeans.isProgramFullyReadable(program, dataContext.getRecorderPersonId(), dataContext.getRecorderDepartmentId())
            )
            .flatMap(program -> program.getMoratoriums().stream())
            // A moratorium period overlaps an extraction period
            .filter(moratorium ->
                moratorium.getPeriods().stream()
                    .anyMatch(moratoriumPeriod -> periodFilters.stream()
                        .anyMatch(extractionPeriod ->
                            !extractionPeriod.getStartDate().isAfter(moratoriumPeriod.getEndDate()) && !extractionPeriod.getEndDate().isBefore(moratoriumPeriod.getStartDate())                            ))
            )
            .forEach(result::add);

        return result;
    }

    /**
     * <p>toProgramDTO.</p>
     *
     * @param source a {@link fr.ifremer.quadrige3.core.vo.administration.program.ProgramVO} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    private ProgramDTO toProgramDTO(ProgramVO source) {
        ProgramDTO target = ReefDbBeanFactory.newProgramDTO();
        target.setCode(source.getProgCd());
        target.setName(source.getProgNm());
        return target;
    }

    private List<ProgramDTO> toProgramDTOs(Collection<ProgramVO> sources) {
        return CollectionUtils.isEmpty(sources)
                ? null
                : sources.stream().map(this::toProgramDTO).collect(Collectors.toList());
    }

    /**
     * Save remote programs, using synchro server
     *
     * @param remoteProgramCodes a {@link java.util.Set} object.
     * @param authenticationInfo a {@link fr.ifremer.quadrige3.core.security.AuthenticationInfo} object.
     */
    protected void saveProgramOnServer(AuthenticationInfo authenticationInfo, Set<String> remoteProgramCodes) {
        // check not empty
        if (CollectionUtils.isEmpty(remoteProgramCodes)) {
            return;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Sending programs [%s] to server", Joiner
                    .on(',').join(remoteProgramCodes)));
        }

        // Convert to VOs
        List<ProgramVO> programsToSend = ImmutableList.copyOf(remoteProgramCodes).stream()
                .map(progCd -> (ProgramVO) programDao.load(ProgramDao.TRANSFORM_PROGRAMVO, progCd))
                .collect(Collectors.toList());

        // Do a remote save
        List<ProgramVO> savedPrograms = synchroRestClientService.savePrograms(
                authenticationInfo,
                programsToSend);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Updating programs [%s] from server response", Joiner
                    .on(',').join(remoteProgramCodes)));
        }
        for (ProgramVO savedProgram : savedPrograms) {
            programDao.save(savedProgram);
        }
    }

    /**
     * <p>filterProgramDTOsByConfig.</p>
     *
     * @param source a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Deprecated
    protected List<ProgramDTO> filterProgramDTOsByConfig(List<ProgramDTO> source) {

        final Set<String> allowedProgramCodes = QuadrigeConfiguration.getInstance().getSynchroProgramCodeIncludes();

        // Do not filter if programs list from config is empty
        // or if given list is empty
        if (CollectionUtils.isEmpty(allowedProgramCodes)
                || CollectionUtils.isEmpty(source)) {
            return source;
        }

        // Keep programs found in the config list
        return ReefDbBeans.filterCollection(source, program -> program != null
                && program.getCode() != null
                && allowedProgramCodes.contains(program.getCode())
        );
    }


    private List<ProgramDTO> filterReadablePrograms(List<ProgramDTO> programs) {

        return filterReadablePrograms(SecurityContextHelper.getQuadrigeUserId(), programs);
    }


    private List<ProgramDTO> filterWritablePrograms(List<ProgramDTO> programs) {

        return filterWritablePrograms(SecurityContextHelper.getQuadrigeUserId(), programs);
    }

    private List<ProgramDTO> filterReadablePrograms(int userId, List<ProgramDTO> programs) {

        if (CollectionUtils.isEmpty(programs)) {
            return new ArrayList<>();
        }

        // Filter programs with read rights for user
        return programs.stream()
            .filter(filterAllowedOrLocal(getReadableProgramCodesByQuserId(userId)))
            .collect(Collectors.toList());

    }

    private List<ProgramDTO> filterWritablePrograms(int userId, List<ProgramDTO> programs) {

        if (CollectionUtils.isEmpty(programs)) {
            return new ArrayList<>();
        }

        // Filter programs with write rights for user (or local program)
        return programs.stream()
                .filter(filterAllowedOrLocal(getWritableProgramCodesByQuserId(userId)))
                .collect(Collectors.toList());

    }


    private ProgramDTO filterReadableProgram(ProgramDTO program) {

        final Set<String> allowedProgramCodes = getReadableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        if (program == null) {
            return null;
        }
        return allowedProgramCodes.contains(program.getCode()) || ReefDbBeans.isLocalStatus(program.getStatus()) ? program : null;
    }

    private ProgramDTO filterWritableProgram(ProgramDTO program) {

        final Set<String> allowedProgramCodes = getWritableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        if (program == null) {
            return null;
        }
        return allowedProgramCodes.contains(program.getCode()) || ReefDbBeans.isLocalStatus(program.getStatus()) ? program : null;
    }

    private List<ProgramDTO> filterManagedPrograms(List<ProgramDTO> programs) {

        return filterManagedPrograms(SecurityContextHelper.getQuadrigeUserId(), programs);
    }

    private List<ProgramDTO> filterManagedPrograms(int userId, List<ProgramDTO> programs) {

        if (CollectionUtils.isEmpty(programs)) {
            return new ArrayList<>();
        }

        // Filter programs with manager rights for user (or local program)
        return programs.stream()
                .filter(filterAllowedOrLocal(getManagedProgramCodesByQuserId(userId)))
                .collect(Collectors.toList());

    }

    private Predicate<ProgramDTO> filterAllowedOrLocal(Collection<String> allowedProgramCodes) {
        return program -> program != null && program.getCode() != null && program.getStatus() != null
                && (
                // This program is allowed
                allowedProgramCodes.contains(program.getCode())
                        ||
                        // Or it is a local program
                        ReefDbBeans.isLocalStatus(program.getStatus())
        );
    }
}
