package fr.ifremer.reefdb.service.rulescontrol;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.NumericPreconditionDTO;
import fr.ifremer.reefdb.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import org.apache.commons.lang3.mutable.MutableInt;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Rules control service.
 */
public interface RuleListService {

    /**
     * Recupere une liste de regle de controle
     *
     * @param ruleListCode une regle de controle
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.RuleListDTO} object.
     */
    @Transactional(readOnly = true)
    RuleListDTO getRuleList(String ruleListCode);

    /**
     * Recupere toute la liste de regles de controle
     *
     * @return a {@link java.util.List} object.
     */
    @Transactional(readOnly = true)
    List<RuleListDTO> getRuleLists();

    @Transactional(readOnly = true)
    List<RuleListDTO> getRuleListsForProgram(String programCode);

    /**
     * Check the rule list code exists in database
     *
     * @param ruleListCode the rule list code to check
     * @return true if rule list code exists
     */
    @Transactional(readOnly = true)
    boolean ruleListCodeExists(String ruleListCode);

    /**
     * <p>saveRuleLists.</p>
     *
     * @param authenticationInfo
     * @param ruleLists          a {@link List} object.
     */
    @Transactional()
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void saveRuleLists(AuthenticationInfo authenticationInfo, List<? extends RuleListDTO> ruleLists);

    /**
     * Delete a list of rules (as well as the included rules)
     *
     * @param authenticationInfo
     * @param ruleLists          a {@link List} object.
     */
    @Transactional()
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    void deleteRuleLists(AuthenticationInfo authenticationInfo, List<RuleListDTO> ruleLists);

    /**
     * <p>duplicateRuleList.</p>
     *
     * @param ruleList                a {@link fr.ifremer.reefdb.dto.configuration.control.RuleListDTO} object.
     * @param newCode                 a {@link java.lang.String} object.
     * @param isDuplicateRulesEnabled if true rules included are also duplicated
     * @return duplicated ruleList
     */
    @Transactional(readOnly = true)
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    RuleListDTO duplicateRuleList(RuleListDTO ruleList, String newCode, boolean isDuplicateRulesEnabled);

    /**
     * Instantiates a new ControlRuleDTO with default values without saving it
     *
     * @param ruleList rule list used to initiate the control rule (code, ...)
     * @return a newly instantiated control rule
     */
    @PreAuthorize("hasPermission(null, T(fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority).LOCAL_ADMIN)")
    ControlRuleDTO newControlRule(RuleListDTO ruleList);

    /**
     * Check the rule code exists in database
     *
     * @param ruleCode the rule code to check
     * @return true if rule code exists
     */
    @Transactional(readOnly = true)
    boolean ruleCodeExists(String ruleCode);

    /**
     * Get a new unique MutableInt
     *
     * @return a MutableInt
     */
    MutableInt getUniqueMutableIndex();

    /**
     * Get the next non existing rule code with radical and a incremental-able suffix
     *
     * @param targetName  the base name
     * @param suffixIndex the suffix
     * @return the next rule code
     */
    @Transactional(readOnly = true)
    String getNextRuleCode(String targetName, MutableInt suffixIndex);

    /**
     * Retrieve all active preconditioned rules for a survey.
     * Used, for example, to filter values on measurements against another measurement value
     *
     * @param survey the survey
     * @return existing preconditioned rules for survey
     */
    @Transactional(readOnly = true)
    List<ControlRuleDTO> getPreconditionedControlRulesForSurvey(SurveyDTO survey);

    /**
     * Retrieve all active grouped rules for a survey.
     * Used, for example, to check multiple control rule on 1 line of individual measurements
     *
     * @param survey the survey
     * @return existing grouped rules for survey
     */
    @Transactional(readOnly = true)
    List<ControlRuleDTO> getGroupedControlRulesForSurvey(SurveyDTO survey);

    /**
     * Retrieve all active preconditioned rules for a list of programs.
     * Used, for example, to filter values on measurements against another measurement value
     * Note: used by Dali extraction
     *
     * @param programCodes the program codes
     * @return existing preconditioned rules for programs
     */
    @Transactional(readOnly = true)
    List<ControlRuleDTO> getPreconditionedControlRulesForProgramCodes(List<String> programCodes);

    /**
     * Build allowed qualitative values for a list of pmfm against a source value, regarding the precondition rules.
     * Result is stored in the AllowedQualitativeValuesMap object
     *
     * @param measurement                 the source measurement
     * @param targetPmfmIds               the target pmfm id list
     * @param preconditions               the precondition rule list
     * @param allowedQualitativeValuesMap the result map
     */
    void buildAllowedValuesByPmfmId(MeasurementDTO measurement,
                                    Collection<Integer> targetPmfmIds,
                                    Collection<PreconditionRuleDTO> preconditions,
                                    AllowedQualitativeValuesMap allowedQualitativeValuesMap);

    /**
     * Build a multi map of qualitative values correspondence from the rule preconditions
     *
     * @param preconditions rule preconditions
     * @return a multimap
     */
    @Transactional(readOnly = true)
    Multimap<QualitativeValueDTO, QualitativeValueDTO> buildQualitativeValueMapFromPreconditions(Collection<PreconditionRuleDTO> preconditions);

    /**
     * Build qualitative rule preconditions from a multi map of qualitative values correspondence
     *
     * @param rule     target rule
     * @param multimap the multimap
     */
    @Transactional(readOnly = true)
    void buildPreconditionsFromQualitativeValueMap(ControlRuleDTO rule, Multimap<QualitativeValueDTO, QualitativeValueDTO> multimap);

    /**
     * Build a list of numeric preconditions from the rule preconditions
     *
     * @param preconditions rule preconditions
     * @return a list
     */
    @Transactional(readOnly = true)
    List<NumericPreconditionDTO> buildNumericPreconditionListFromPreconditions(Collection<PreconditionRuleDTO> preconditions);

    /**
     * Build numerical rule preconditions from a list of numerical preconditions
     *
     * @param rule target rule
     * @param list the list
     */
    @Transactional(readOnly = true)
    void buildPreconditionsFromNumericPreconditionList(ControlRuleDTO rule, List<NumericPreconditionDTO> list);

    /**
     * Build groups for NOT_EMPTY_CONDITIONAL function
     * For now its limited to taxon_group not empty, taxon not empty and pmfm value not empty
     *
     * @param groupedRule the grouped rule
     */
    @Transactional(readOnly = true)
    void buildNotEmptyConditionalGroupsFromGroupedRule(ControlRuleDTO groupedRule);

}


