/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.service.administration.context;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * <p>ContextService interface.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Transactional(readOnly = true)
public interface ContextService {
    
	/**
	 * renvoit un contexte local en fonction d'un Id
	 *
	 * @param contextId a {@link java.lang.Integer} object.
	 * @return un contexte local
	 */
	ContextDTO getContext(Integer contextId);
    
	/**
	 * Recupere la liste des contextes locaux
	 *
	 * @return une liste de contextes locaux
	 */
	List<ContextDTO> getAllContexts();

    /**
     * <p>saveContexts.</p>
     *
     * @param contexts a {@link java.util.List} object.
     */
    @Transactional()
    void saveContexts(List<? extends ContextDTO> contexts);
    
    /**
     * <p>deleteContexts.</p>
     *
     * @param contexts a {@link java.util.List} object.
     */
    @Transactional()
    void deleteContexts(List<? extends ContextDTO> contexts);

    /**
     * <p>importContexts.</p>
     *
     * @param sourceContextFile a {@link java.io.File} object.
     * @return a {@link java.util.List} object.
     */
    @Transactional()
    List<ContextDTO> importContexts(File sourceContextFile);
    
    /**
     * <p>exportContexts.</p>
     *
     * @param contexts a {@link Collection} object.
     * @param targetContextFile a {@link File} object.
	 */
	void exportContexts(Collection<ContextDTO> contexts, File targetContextFile);
    
    /**
     * Get all filters by type
     *
     * @param filterTypeId a int.
     * @return a {@link java.util.List} object.
     */
    List<FilterDTO> getFiltersByType(int filterTypeId);

    /**
     * <p>getFilter.</p>
     *
     * @param filterId a {@link java.lang.Integer} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    FilterDTO getFilter(Integer filterId);

    /**
     * Load all filtered elements in filter
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    void loadFilteredElements(FilterDTO filter);

    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return a {@link java.util.List} object.
     */
    List<FilterDTO> getAllDepartmentFilters();

	/**
	 * All departments for context
	 *
	 * @param contextId context identifier
	 * @return Departments
	 */
	List<DepartmentDTO> getFilteredDepartments(Integer contextId);
	

    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return Filters
     */
    List<FilterDTO> getAllSamplingEquipmentFilters();


	/**
	 * All sampling equipment for context
	 *
	 * @param contextId context identifier
	 * @return Sampling equipment
	 */
	List<SamplingEquipmentDTO> getFilteredSamplingEquipments(Integer contextId);
	
    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return Filters
     */
    List<FilterDTO> getAllAnalysisInstrumentFilters();

	/**
	 * All analysis instrument for context
	 *
	 * @param contextId context identifier
	 * @return Analysis instrument
	 */
	List<AnalysisInstrumentDTO> getFilteredAnalysisInstruments(Integer contextId);
	
    /**
     * <p>Save generic filter</p>
     * <p>The filter type id must be set before calling this method</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    @Transactional()
    void saveFilter(FilterDTO filter);
    
    /**
     * <p>Save generic filters</p>
     * <p>The filter type id must be set on each filter before calling this method</p>
     *
     * @param filters a {@link java.util.List} object.
     */
    @Transactional()
    void saveFilters(List<? extends FilterDTO> filters);

    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return Filters
     */
    List<FilterDTO> getAllPmfmFilters();

	/**
	 * All pmfm for context
	 *
	 * @param contextId context identifier
	 * @return Pmfm list
	 */
	List<PmfmDTO> getFilteredPmfms(Integer contextId);
    
    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return filters
     */
    List<FilterDTO> getAllTaxonFilters();

	/**
	 * All taxon for context
	 *
	 * @param contextId context identifier
	 * @return Taxon
	 */
	List<TaxonDTO> getFilteredTaxons(Integer contextId);
	
    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return filters
     */
    List<FilterDTO> getAllTaxonGroupFilters();

	/**
	 *
	 * All taxon group for context
	 *
	 * @param contextId context identifier
	 * @return Taxon group
	 */
	List<TaxonGroupDTO> getFilteredTaxonGroups(Integer contextId);
    
    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return filters
     */
    List<FilterDTO> getAllLocationFilter();

	/**
	 * All locations for context
	 *
	 * @param contextId context identifier
	 * @return Locations
	 */
	List<LocationDTO> getFilteredLocations(Integer contextId);
	
    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return filters
     */
    List<FilterDTO> getAllUserFilter();

	/**
	 * All users for context
	 *
	 * @param contextId context identifier
	 * @return Locations
	 */
	List<PersonDTO> getFilteredUsers(Integer contextId);

    /**
     * CAUTION. Returns filters only, without filtered elements
     *
     * @return filters
     */
    List<FilterDTO> getAllProgramFilter();

	/**
	 * All programs for context
	 *
	 * @param contextId context identifier
	 * @return Programs
	 */
	List<ProgramDTO> getFilteredPrograms(Integer contextId);

	/**
	 * CAUTION. Returns filters only, without filtered elements
	 *
	 * @return filters
	 */
	List<FilterDTO> getAllCampaignFilter();

	/**
	 * All campaigns for context
	 *
	 * @param contextId context identifier
	 * @return Campaigns
	 */
	List<CampaignDTO> getFilteredCampaigns(Integer contextId);

	/**
     * Check if the filters are not used in any context
     *
     * @param filters the filter list to check
     * @return true if all filters in the list are free ^^, false if at least one filter is used
     */
    boolean checkFiltersNotUsedInContext(List<? extends FilterDTO> filters);

    /**
     * <p>deleteFilters.</p>
     *
     * @param filters a {@link java.util.List} object.
     */
    @Transactional()
    void deleteFilters(List<? extends FilterDTO> filters);
    
    /**
     * <p>importFilter.</p>
     *
     * @param sourceFilterFile a {@link java.io.File} object.
     * @param filterTypeId a int.
     * @return a {@link java.util.List} object.
     */
    @Transactional()
    List<FilterDTO> importFilter(File sourceFilterFile, int filterTypeId);
    
    /**
     * <p>exportFilter.</p>
     *
     * @param filters a {@link Collection} object.
     * @param targetFilterFile a {@link File} object.
	 */
	void exportFilter(Collection<FilterDTO> filters, File targetFilterFile);

    /**
     * <p>duplicateContext.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    ContextDTO duplicateContext(ContextDTO context);
    
    /**
     * <p>duplicateFilter.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    FilterDTO duplicateFilter(final FilterDTO filter);

}
