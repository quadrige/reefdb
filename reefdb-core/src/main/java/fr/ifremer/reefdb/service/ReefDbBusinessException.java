package fr.ifremer.reefdb.service;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.jaxx.application.ApplicationBusinessException;

/**
 * <p>ReefDbBusinessException class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class ReefDbBusinessException extends ApplicationBusinessException {

    /**
     * <p>Constructor for ReefDbBusinessException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public ReefDbBusinessException(String message) {
        super(message);
    }

    /**
     * <p>Constructor for ReefDbBusinessException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param cause a {@link java.lang.Throwable} object.
     */
    public ReefDbBusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <p>Constructor for ReefDbBusinessException.</p>
     *
     * @param cause a {@link java.lang.Throwable} object.
     */
    public ReefDbBusinessException(Throwable cause) {
        super(cause);
    }
}
