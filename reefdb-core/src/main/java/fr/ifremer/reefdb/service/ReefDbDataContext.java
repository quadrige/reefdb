package fr.ifremer.reefdb.service;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContext;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractBean;

import java.io.Closeable;
import java.util.Objects;

/**
 * Data context of ui.
 * <p/>
 * All shared data must be there to avoid reloading some stuff.
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 */
public class ReefDbDataContext extends AbstractBean implements Closeable, SecurityContext {

    /**
     * Constant <code>PROPERTY_RECORDER_PERSON_ID="recorderPersonId"</code>
     */
    public static final String PROPERTY_RECORDER_PERSON_ID = "recorderPersonId";
    /**
     * Constant <code>PROPERTY_RECORDER_DEPARTMENT_ID="recorderDepartmentId"</code>
     */
    public static final String PROPERTY_RECORDER_DEPARTMENT_ID = "recorderDepartmentId";
    /**
     * Constant <code>PROPERTY_CONTEXT="context"</code>
     */
    public static final String PROPERTY_CONTEXT = "context";
    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReefDbDataContext.class);
    // Singleton
    private static final ReefDbDataContext INSTANCE = new ReefDbDataContext();
    /**
     * the current recorder user
     */
    private Integer recorderPersonId;
    /**
     * the current recorder department
     */
    private Integer recorderDepartmentId;
    /**
     * the current context
     */
    private ContextDTO context;

    /**
     * <p>Constructor for ReefDbDataContext.</p>
     */
    protected ReefDbDataContext() {
        // protected constructor, for singleton
    }

    /**
     * <p>instance.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.ReefDbDataContext} object.
     */
    public static ReefDbDataContext instance() {
        return INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        clearContext();
    }

    /**
     * <p>clearContext.</p>
     */
    public void clearContext() {
        clearContext(false);
    }

    /**
     * <p>clearContextKeepRecorderPerson.</p>
     */
    public void clearContextKeepRecorderPerson() {
        clearContext(true);
    }

    private void clearContext(boolean keepRecorderPerson) {
        if (!keepRecorderPerson) {
            setRecorderPersonId(null);
        }

        // reset local caches
        resetLocalCache();
    }

    /**
     * <p>resetLocalCache.</p>
     */
    public void resetLocalCache() {

    }

    /*
     * recorder person Id
     */

    /**
     * <p>isRecorderPersonFilled.</p>
     *
     * @return a boolean.
     */
    public boolean isRecorderPersonFilled() {
        return recorderPersonId != null;
    }

    /**
     * <p>Getter for the field <code>recorderPersonId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getRecorderPersonId() {
        return recorderPersonId;
    }

    /**
     * <p>Setter for the field <code>recorderPersonId</code>.</p>
     *
     * @param recorderPersonId a {@link java.lang.Integer} object.
     */
    public void setRecorderPersonId(Integer recorderPersonId) {
        Integer oldId = getRecorderPersonId();
        this.recorderPersonId = recorderPersonId;
        firePropertyChange(PROPERTY_RECORDER_PERSON_ID, oldId, recorderPersonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPrincipalUserId() {
        return getRecorderPersonId();
    }

    public boolean isRecorderAdministrator() {
        return SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.ADMIN);
    }

    /*
     * recorder department Id
     */

    /**
     * <p>isRecorderDepartmentFilled.</p>
     *
     * @return a boolean.
     */
    public boolean isRecorderDepartmentFilled() {
        return recorderDepartmentId != null;
    }

    /**
     * <p>Getter for the field <code>recorderDepartmentId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getRecorderDepartmentId() {
        return recorderDepartmentId;
    }

    /**
     * <p>Setter for the field <code>recorderDepartmentId</code>.</p>
     *
     * @param recorderDepartmentId a {@link java.lang.Integer} object.
     */
    public void setRecorderDepartmentId(Integer recorderDepartmentId) {
        Integer oldId = getRecorderDepartmentId();
        this.recorderDepartmentId = recorderDepartmentId;
        firePropertyChange(PROPERTY_RECORDER_DEPARTMENT_ID, oldId, recorderDepartmentId);
    }

    /**
     * <p>Getter for the field <code>context</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public ContextDTO getContext() {
        return context;
    }

    /**
     * <p>Setter for the field <code>context</code>.</p>
     *
     * @param context a {@link fr.ifremer.reefdb.dto.configuration.context.ContextDTO} object.
     */
    public void setContext(ContextDTO context) {
        ContextDTO oldContext = getContext();
        this.context = context;
        firePropertyChange(PROPERTY_CONTEXT, oldContext, context);
    }

    /**
     * <p>getContextId.</p>
     *
     * @return a int.
     */
    public int getContextId() {
        Assert.notNull(getContext());
        return getContext().getId();
    }

    /**
     * <p>isContextFiltered.</p>
     *
     * @param contextFilter a {@link FilterTypeValues} object.
     * @return a boolean.
     */
    public boolean isContextFiltered(FilterTypeValues contextFilter) {
        Assert.notNull(contextFilter);
        if (getContext() == null || CollectionUtils.isEmpty(getContext().getFilters())) {
            return false;
        }
        for (FilterDTO filter : getContext().getFilters()) {
            if (Objects.equals(filter.getFilterTypeId(), contextFilter.getFilterTypeId())) {
                return true;
            }
        }
        return false;
    }

    public boolean isSurveyEditable(SurveyDTO survey) {
        ProgramDTO program = survey.getProgram();
        return
            // User is administrator
            isRecorderAdministrator()
                // User is program manager
                || ReefDbBeans.isProgramManager(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is program validator
                || ReefDbBeans.isProgramValidator(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is recorder
                || (
                ReefDbBeans.isProgramRecorder(program, getRecorderPersonId(), getRecorderDepartmentId())
                    // And data created by same service as user (or by privilege transfer)
                    && (
                    Objects.equals(survey.getRecorderDepartment().getId(), getRecorderDepartmentId())
                        || CollectionUtils.emptyIfNull(survey.getInheritedRecorderDepartmentIds()).contains(getRecorderDepartmentId())
                )
            );
    }

    public boolean isSurveyViewable(SurveyDTO survey) {
        ProgramDTO program = survey.getProgram();
        return
            // User is administrator
            isRecorderAdministrator()
                // User is program manager
                || ReefDbBeans.isProgramManager(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is full viewer
                || ReefDbBeans.isProgramFullViewer(program, getRecorderPersonId(), getRecorderDepartmentId())
                // Or user is recorder or program validator (Mantis #59582)
                || (
                (ReefDbBeans.isProgramRecorder(program, getRecorderPersonId(), getRecorderDepartmentId())
                    || ReefDbBeans.isProgramViewer(program, getRecorderPersonId(), getRecorderDepartmentId())
                    || ReefDbBeans.isProgramValidator(program, getRecorderPersonId(), getRecorderDepartmentId()))
                    // And data created by same service as user (or by privilege transfer)
                    && (
                    Objects.equals(survey.getRecorderDepartment().getId(), getRecorderDepartmentId())
                        || CollectionUtils.emptyIfNull(survey.getInheritedRecorderDepartmentIds()).contains(getRecorderDepartmentId())
                )
            );
    }
}
