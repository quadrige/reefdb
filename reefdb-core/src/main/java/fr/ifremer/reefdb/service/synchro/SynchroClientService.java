package fr.ifremer.reefdb.service.synchro;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.synchro.vo.SynchroImportContextVO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

/**
 * Created by blavenie on 03/09/15.
 */
@Transactional(propagation = Propagation.REQUIRED)
public interface SynchroClientService extends fr.ifremer.quadrige3.synchro.service.client.SynchroClientService {

    /**
     * Compute change log from a file to import
     *
     * @param userId a int.
     * @param dbZipFile a {@link File} object.
     * @param importContext a {@link SynchroImportContextVO} object.
     * @param progressionModel
     * @param progressionModelMaxCount a int.
     * @return a DTO to return
     */
    @Transactional(propagation = Propagation.NEVER)
    SynchroChangesDTO getImportFileChangesAsDTO(
            int userId,
            File dbZipFile,
            SynchroImportContextVO importContext,
            ProgressionCoreModel progressionModel,
            int progressionModelMaxCount);

    /**
     * <p>toPkToIncludesMap.</p>
     *
     * @param changes a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     * @return a {@link com.google.common.collect.Multimap} object.
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    Multimap<String, String> toPkToIncludesMap(SynchroChangesDTO changes);

    /**
     * Check if SynchroChangesDTO bean has at least one table from program/strategy or rules
     *
     * @param synchroChanges a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     * @return true if one of those tables is found
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    boolean hasProgramOrRulesChanges(SynchroChangesDTO synchroChanges);

    /**
     * Filter SynchroTableDTO and SynchroRowDTO with INSERT operation only on referential tables.<br/>
     * Keep UPDATE for program/strategy or rules tables.
     *
     * @param synchroChanges a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     * @return the filtered SynchroChangesDTO bean
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    SynchroChangesDTO getReferentialSynchroChangesToApplyFromFile(SynchroChangesDTO synchroChanges);

    /**
     * Filter SynchroTableDTO and SynchroRowDTO with INSERT and DUPLICATE operation only on SURVEY table
     *
     * @param synchroChanges a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     * @return the filtered SynchroChangesDTO bean
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    SynchroChangesDTO getSurveySynchroChangesFromFile(SynchroChangesDTO synchroChanges);


}
