package fr.ifremer.reefdb.service.observation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.data.measurement.ReefDbMeasurementDao;
import fr.ifremer.reefdb.dao.data.photo.ReefDbPhotoDao;
import fr.ifremer.reefdb.dao.data.samplingoperation.ReefDbSamplingOperationDao;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumPmfmDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyFilterDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.enums.SearchDateValues;
import fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.administration.campaign.CampaignService;
import fr.ifremer.reefdb.service.administration.context.ContextService;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.service.administration.user.UserService;
import fr.ifremer.reefdb.service.persistence.PersistenceService;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ObservationServiceImpl class.</p>
 *
 * @author Ludovic
 */
@Service("reefdbSurveyService")
public class ObservationServiceImpl implements ObservationInternalService {

    private static final Log log = LogFactory.getLog(ObservationServiceImpl.class);

    @Resource(name = "reefdbSurveyService")
    protected ObservationInternalService loopBackService;

    @Resource(name = "reefdbPersistenceService")
    protected PersistenceService persistenceService;

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource(name = "reefdbContextService")
    protected ContextService contextService;

    @Resource(name = "reefdbReferentialService")
    protected ReferentialService referentialService;

    @Resource(name = "reefDbUserService")
    protected UserService userService;

    @Resource(name = "reefdbProgramStrategyService")
    protected ProgramStrategyService programStrategyService;

    @Resource(name = "reefdbCampaignService")
    protected CampaignService campaignService;

    @Resource(name = "reefDbSurveyDao")
    protected ReefDbSurveyDao surveyDao;

    @Resource(name = "reefDbSamplingOperationDao")
    protected ReefDbSamplingOperationDao samplingOperationDao;

    @Resource(name = "reefDbMeasurementDao")
    protected ReefDbMeasurementDao measurementDao;

    @Resource(name = "reefDbPhotoDao")
    protected ReefDbPhotoDao photoDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Autowired
    protected ReefDbConfiguration config;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SurveyDTO> getSurveys(SurveyFilterDTO surveyFilter) {
        Integer campaignId = surveyFilter.getCampaignId();
        String programCode = surveyFilter.getProgramCode();
        Integer locationId = surveyFilter.getLocationId();
        Integer stateId = surveyFilter.getStateId();
        Integer synchronizationStatusId = surveyFilter.getShareId();

        String comment = surveyFilter.getComment();
        // TODO transform comment to ignore accentuation

        LocalDate startDate = null;
        LocalDate endDate = null;
        boolean strictDate = false;
        if (surveyFilter.getSearchDateId() != null) {
            SearchDateValues searchDateValue = SearchDateValues.values()[surveyFilter.getSearchDateId()];
            switch (searchDateValue) {
                case EQUALS:
                    startDate = surveyFilter.getDate1();
                    endDate = startDate;
                    strictDate = true;
                    break;
                case BETWEEN:
                    startDate = surveyFilter.getDate1();
                    endDate = surveyFilter.getDate2();
                    break;
                case BEFORE:
                    endDate = surveyFilter.getDate1();
                    strictDate = true;
                    break;
                case BEFORE_OR_EQUALS:
                    endDate = surveyFilter.getDate1();
                    break;
                case AFTER:
                    startDate = surveyFilter.getDate1();
                    strictDate = true;
                    break;
                case AFTER_OR_EQUALS:
                    startDate = surveyFilter.getDate1();
                    break;
            }
        }
        Collection<String> programCodes;
        // if no filter on single program, force filter on writable programs
        if (StringUtils.isBlank(programCode)) {
            // get readable programs including locals
            programCodes = programStrategyService.getReadablePrograms().stream().map(ProgramDTO::getCode).collect(Collectors.toSet());
        } else {
            programCodes = Collections.singleton(programCode);
        }

        List<SurveyDTO> surveys = surveyDao.getSurveysByCriteria(
                campaignId,
                programCodes,
                locationId,
                comment,
                stateId,
                synchronizationStatusId,
                Dates.convertToDate(startDate, config.getDbTimezone()),
                Dates.convertToDate(endDate, config.getDbTimezone()),
                strictDate);

        filterSurveysUnderMoratorium(surveys);

        return surveys;
    }

    private void filterSurveysUnderMoratorium(List<SurveyDTO> surveys) {

        Integer userId = dataContext.getRecorderPersonId();
        Assert.notNull(userId);
        Integer departmentId = dataContext.getRecorderDepartmentId();
        Assert.notNull(departmentId);

        surveys.removeIf(survey -> {

            ProgramDTO program = survey.getProgram();

            // No moratorium: keep it
            if (program.isMoratoriumsEmpty()) return false;

            // User is viewer or not recorder
            if (!dataContext.isSurveyViewable(survey)) {

                // Survey is in global moratorium period: remove it
                return program.getMoratoriums().stream()
                    .filter(MoratoriumDTO::isGlobal)
                    .flatMap(moratorium -> moratorium.getPeriods().stream())
                    .anyMatch(period -> !survey.getDate().isAfter(period.getEndDate()) && !survey.getDate().isBefore(period.getStartDate()));

            }

            // default: survey is not removed
            return false;

        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadSamplingOperationsFromSurvey(SurveyDTO survey, boolean withIndividualMeasurements) {

        if (survey == null || survey.getId() == null) {
            return;
        }

        if (!survey.isSamplingOperationsLoaded()) {

            // load and affect sampling operations
            survey.setSamplingOperations(samplingOperationDao.getSamplingOperationsBySurveyId(survey.getId(), withIndividualMeasurements));
            survey.setSamplingOperationsLoaded(true);

            if (!survey.isSamplingOperationsEmpty()) {

                // collect measurements (ungrouped only)
                for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {

                    // affect all pmfms to each sampling operation, even if it don't have measurement with all pmfms
                    if (samplingOperation.getPmfms() == null) {
                        samplingOperation.setPmfms(new ArrayList<>());
                    }

                    if (withIndividualMeasurements) {
                        if (samplingOperation.getIndividualPmfms() == null) {
                            samplingOperation.setIndividualPmfms(new ArrayList<>());
                        }
                    } else {
                        // clear individual pmfms and measurements
                        samplingOperation.setIndividualPmfms(null);
                        samplingOperation.setIndividualMeasurements(null);
                        samplingOperation.setIndividualMeasurementsLoaded(false); // should already be set to false
                    }

                    // add PMFMs from existing ungrouped measurements
                    ReefDbBeans.populatePmfmsFromMeasurements(samplingOperation);

                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SamplingOperationDTO newSamplingOperation(SurveyDTO survey, List<PmfmDTO> pmfms) {
        Assert.notNull(survey);
        Assert.notNull(pmfms);

        SamplingOperationDTO result = ReefDbBeanFactory.newSamplingOperationDTO();

        // add empty measurements
        for (final PmfmDTO pmfm : pmfms) {
            final MeasurementDTO measurement = ReefDbBeanFactory.newMeasurementDTO();
            measurement.setPmfm(pmfm);
            result.addMeasurements(measurement);
        }
        result.addAllPmfms(pmfms);

        // add department from strategy
        result.setSamplingDepartment(programStrategyService.getDepartmentOfAppliedStrategyBySurvey(survey));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO getSurvey(Integer surveyId) {
        return getSurvey(surveyId, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO getSurveyWithoutPmfmFiltering(Integer surveyId) {
        return getSurvey(surveyId, true);
    }

    private SurveyDTO getSurvey(Integer surveyId, boolean withoutFilter) {
        Assert.notNull(surveyId);

        SurveyDTO survey = surveyDao.getSurveyById(surveyId, withoutFilter);

        // Load Pmfm strategies
        List<PmfmStrategyDTO> pmfmStrategies = programStrategyService.getPmfmStrategiesBySurvey(survey);

        {
            List<PmfmDTO> pmfms = Lists.newArrayList();
            List<PmfmDTO> individualPmfms = Lists.newArrayList();

            // Collect Pmfms for survey
            for (PmfmStrategyDTO p : pmfmStrategies) {
                if (p.isSurvey()) {
                    if (p.isGrouping()) {
                        individualPmfms.add(p.getPmfm());
                    } else {
                        pmfms.add(p.getPmfm());
                    }
                }
            }

            // Add Pmfm strategies on survey
//            survey.setPmfms(pmfms);
            // Mantis #0027878 : filter out some predefined pmfm
            final List<Integer> pmfmIdsToIgnore = ImmutableList.of(config.getDepthValuesPmfmId());
            survey.setPmfms(ReefDbBeans.filterPmfm(pmfms, pmfmIdsToIgnore));
            survey.setIndividualPmfms(individualPmfms);
        }

        // Collect Pmfms from existing measurements
        ReefDbBeans.populatePmfmsFromMeasurements(survey);

        if (!survey.isSamplingOperationsEmpty()) {

            // Collect Pmfms for sampling operations
            List<PmfmDTO> pmfms = Lists.newArrayList();
            List<PmfmDTO> individualPmfms = Lists.newArrayList();
            for (PmfmStrategyDTO p : pmfmStrategies) {
                if (p.isSampling()) {
                    if (p.isGrouping()) {
                        individualPmfms.add(p.getPmfm());
                    } else {
                        pmfms.add(p.getPmfm());
                    }
                }
            }

            for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {

                // Add Pmfm strategies on sampling operations
                samplingOperation.setPmfms(pmfms);
                samplingOperation.setIndividualPmfms(individualPmfms);

                // Collect Pmfms from existing measurements
                ReefDbBeans.populatePmfmsFromMeasurements(samplingOperation);

            }

        }

        return survey;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveSurveys(Collection<? extends SurveyDTO> surveys, ProgressionCoreModel progressionModel) {

        List<SurveyDTO> surveysToSave = surveys.stream().filter(SurveyDTO::isDirty).collect(Collectors.toList());
        progressionModel.setTotal(surveysToSave.size());

        for (SurveyDTO survey : surveysToSave) {
            progressionModel.increments(t("reefdb.service.common.progression",
                    t("reefdb.service.observation.save"), progressionModel.getCurrent() + 1, progressionModel.getTotal()));

            if (survey.getId() == null) {

                // add recorderDepartment if any
                if (survey.getRecorderDepartment() == null) {
                    // get department from authenticated user
                    Integer recDepId = dataContext.getRecorderDepartmentId();
                    if (recDepId == null) {
                        throw new ReefDbTechnicalException("no RecorderDepartmentId found in data context");
                    }
                    survey.setRecorderDepartment(departmentDao.getDepartmentById(recDepId));
                }
            }

            saveSurvey(survey);
            survey.setDirty(false);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveSurvey(SurveyDTO survey) {
        // when saving, reset control date - see mantis #26451
        if (survey.isDirty() || survey.getControlDate() != null) {
            survey.setControlDate(null);
        }

        // Apply default analysis department, from strategy (mantis #28257)
        applyDefaultAnalysisDepartment(survey);

        // Persist the update survey
        surveyDao.save(survey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSurveys(List<Integer> surveyIds) {
        if (surveyIds == null) return;
        Integer recorderUserId = dataContext.getRecorderPersonId();

        // Do not delete, but insert into DeletedItemHistory
        surveyIds.stream().filter(Objects::nonNull).distinct().forEach(surveyId -> surveyDao.removeUsingDeletedItemHistory(surveyId, recorderUserId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SurveyDTO duplicateSurvey(SurveyDTO survey, boolean fullDuplication, boolean copyCoordinates, boolean duplicateSurveyMeasurements) {
        if (survey == null) return null;

        // Duplicate survey
        SurveyDTO duplicateSurvey = ReefDbBeans.clone(survey);
        Assert.notNull(duplicateSurvey);
        // Remove ID
        duplicateSurvey.setId(null);
        duplicateSurvey.setDirty(true);

        // Remove location if disabled
        if (ReefDbBeans.isDisabledStatus(survey.getLocation().getStatus())) {
            duplicateSurvey.setLocation(null);
        }

        // duplicated data to keep:
        if (survey.isObserversEmpty() && survey.getId() != null) {
            duplicateSurvey.setObservers(surveyDao.getObservers(survey.getId()));
        } else {
            duplicateSurvey.setObservers(ReefDbBeans.clone(survey.getObservers()));
        }

        // reset other survey data
        duplicateSurvey.setPhotos(null);
        duplicateSurvey.setPhotosLoaded(false);
        duplicateSurvey.setMeasurements(null);
        duplicateSurvey.setIndividualMeasurements(null);
        duplicateSurvey.setMeasurementsLoaded(false);
        duplicateSurvey.setPmfms(null);
        duplicateSurvey.setIndividualPmfms(null);
        duplicateSurvey.setComment(null);
        duplicateSurvey.setQualificationComment(null);
        duplicateSurvey.setValidationComment(null);
        duplicateSurvey.setValidationDate(null);
        duplicateSurvey.setQualificationDate(null);
        duplicateSurvey.setUpdateDate(null);
        duplicateSurvey.setControlDate(null);
        duplicateSurvey.setSynchronizationStatus(null);
        duplicateSurvey.setErrors(null);

        if (copyCoordinates) {
            duplicateSurvey.setCoordinate(ReefDbBeans.clone(survey.getCoordinate()));
            // and positioning system
            duplicateSurvey.setPositioning(survey.getPositioning());
            // keep positioning comment if we keep same positioning ?
            duplicateSurvey.setPositioningComment(survey.getPositioningComment());
        } else {
            duplicateSurvey.setCoordinate(null);
            duplicateSurvey.setPositioning(null);
            duplicateSurvey.setPositioningComment(null);
        }

        // same occasion ? or just when full duplication ?
        if (survey.getOccasion() != null) {
            duplicateSurvey.setOccasion(ReefDbBeans.clone(survey.getOccasion()));
            duplicateSurvey.getOccasion().setId(null);
        }

        // Copy survey ungrouped (= non individual) measurements (Mantis #47249)
        if (duplicateSurveyMeasurements && survey.getId() != null) {
            List<MeasurementDTO> measurements = measurementDao.getMeasurementsBySurveyId(survey.getId(), config.getDepthValuesPmfmId());
            duplicateSurvey.setMeasurements(measurements.stream().filter(measurement -> measurement.getIndividualId() == null).collect(Collectors.toList()));
            duplicateSurvey.getMeasurements().forEach(measurement -> measurement.setId(null));
            String comment = t("reefdb.service.observation.duplicate.measurementComment", LocalDate.now(), survey.getName());
            duplicateSurvey.getMeasurements().forEach(measurement -> measurement.setComment(comment));
            duplicateSurvey.setPmfms(ReefDbBeans.clone(survey.getPmfms())); // utile ?
            duplicateSurvey.setMeasurementsLoaded(true);
        }

        if (fullDuplication) {

            // ensure sampling operations are loaded
            loadSamplingOperationsFromSurvey(survey, true);

            // sampling operations
            List<SamplingOperationDTO> duplicateSamplingOperations = Lists.newArrayList();
            if (!survey.isSamplingOperationsEmpty()) {
                for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
                    SamplingOperationDTO duplicateSamplingOperation = ReefDbBeans.clone(samplingOperation);
                    Assert.notNull(duplicateSamplingOperation);
                    duplicateSamplingOperation.setId(null);
                    duplicateSamplingOperation.setMeasurements(null);
                    duplicateSamplingOperation.setIndividualMeasurements(null);
                    duplicateSamplingOperation.setMeasurementsLoaded(false);
                    duplicateSamplingOperation.setIndividualMeasurementsLoaded(false);
                    duplicateSamplingOperation.setComment(null);
                    duplicateSamplingOperation.setErrors(null);

                    if (copyCoordinates) {
                        duplicateSamplingOperation.setCoordinate(samplingOperation.getCoordinate());
                        duplicateSamplingOperation.setPositioning(samplingOperation.getPositioning());
                    } else {
                        duplicateSamplingOperation.setCoordinate(null);
                        duplicateSamplingOperation.setPositioning(null);
                    }

                    duplicateSamplingOperations.add(duplicateSamplingOperation);
                }
            }

            duplicateSurvey.setSamplingOperations(duplicateSamplingOperations);
            duplicateSurvey.setSamplingOperationsLoaded(true);

        } else {
            // Remove sampling operations
            duplicateSurvey.setSamplingOperations(null);
            duplicateSurvey.setSamplingOperationsLoaded(false);
        }

        return duplicateSurvey;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getAvailablePrograms(Integer campaignId, Integer locationId, LocalDate date, boolean forceNoContext, boolean writableOnly) {
        List<ProgramDTO> result;
        if (dataContext.isContextFiltered(FilterTypeValues.PROGRAM) && !forceNoContext) {
            result = contextService.getFilteredPrograms(dataContext.getContextId());
            // Get correct list of readable or writable programs (Mantis #49913)
            List<ProgramDTO> programs = writableOnly
                ? programStrategyService.getWritableProgramsByUserAndStatus(dataContext.getPrincipalUserId(), StatusFilter.ACTIVE)
                : programStrategyService.getReadableProgramsByUserAndStatus(dataContext.getPrincipalUserId(), StatusFilter.ACTIVE);
            result = result.stream().filter(programs::contains).collect(Collectors.toList());
        } else if (campaignId != null) {
            result = writableOnly
                ? programStrategyService.getWritableProgramsByCampaignId(campaignId)
                : programStrategyService.getReadableProgramsByCampaignId(campaignId);
        } else {
            result = writableOnly
                ? programStrategyService.getWritablePrograms()
                : programStrategyService.getReadablePrograms();
        }

        if (CollectionUtils.isNotEmpty(result) && locationId != null && date != null) {
            List<ProgramDTO> filteredPrograms = writableOnly
                ? programStrategyService.getWritableProgramsByLocationAndDate(locationId, Dates.convertToDate(date, config.getDbTimezone()))
                : programStrategyService.getReadableProgramsByLocationAndDate(locationId, Dates.convertToDate(date, config.getDbTimezone()));
            return (List<ProgramDTO>) CollectionUtils.intersection(result, filteredPrograms);
        }

        return result;
    }

    @Override
    public List<CampaignDTO> getAvailableCampaigns(LocalDate date, boolean forceNoContext) {
        List<CampaignDTO> result;
        if (dataContext.isContextFiltered(FilterTypeValues.CAMPAIGN) && !forceNoContext) {
            result = contextService.getFilteredCampaigns(dataContext.getContextId());
        } else {
            result = new ArrayList<>(campaignService.getAllCampaigns()); // put in a new List because the DAO returns an immutable list
        }

        if (CollectionUtils.isNotEmpty(result) && date != null) {
            List<CampaignDTO> filteredCampaigns = campaignService.findCampaignsIncludingDate(Dates.convertToDate(date, config.getDbTimezone()));
            return (List<CampaignDTO>) CollectionUtils.intersection(result, filteredCampaigns);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> getAvailableAnalysisInstruments(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.ANALYSIS_INSTRUMENT) && !forceNoContext) {
            return contextService.getFilteredAnalysisInstruments(dataContext.getContextId());
        } else {
            return referentialService.getAnalysisInstruments(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> getAvailableSamplingEquipments(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.SAMPLING_EQUIPMENT) && !forceNoContext) {
            return contextService.getFilteredSamplingEquipments(dataContext.getContextId());
        } else {
            return referentialService.getSamplingEquipments(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getAvailableLocations(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.LOCATION) && !forceNoContext) {
            return contextService.getFilteredLocations(dataContext.getContextId());
        } else {
            return referentialService.getActiveLocations();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getAvailableLocations(Integer campaignId, String programCode, boolean forceNoContext, boolean activeOnly) {
        List<LocationDTO> locations = referentialService.getLocations(campaignId, programCode, activeOnly);
        if (CollectionUtils.isNotEmpty(locations)) {
            if (dataContext.isContextFiltered(FilterTypeValues.LOCATION) && !forceNoContext) {
                List<LocationDTO> filteredLocations = contextService.getFilteredLocations(dataContext.getContextId());
                if (CollectionUtils.isNotEmpty(filteredLocations)) {
                    return (List<LocationDTO>) CollectionUtils.intersection(locations, filteredLocations);
                }
            }
        }
        return locations;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getAvailableTaxonGroups(TaxonDTO taxon, boolean forceNoContext) {
        List<TaxonGroupDTO> result;
        if (taxon == null) {
            return getAvailableTaxonGroups(forceNoContext);
        } else {
//            result = taxon.getTaxonGroups();
            // Use a service cal to get the taxon group according the historical records (Mantis #43919)
            result = referentialService.getTaxonGroups(taxon);
            if (CollectionUtils.isNotEmpty(result)) {
                if (dataContext.isContextFiltered(FilterTypeValues.TAXON_GROUP) && !forceNoContext) {
                    List<TaxonGroupDTO> filteredTaxonGroups = contextService.getFilteredTaxonGroups(dataContext.getContextId());
                    if (CollectionUtils.isNotEmpty(filteredTaxonGroups)) {
                        result = (List<TaxonGroupDTO>) CollectionUtils.intersection(result, filteredTaxonGroups);
                    }
                }
            }
        }
        return referentialService.getFullTaxonGroups(result);
    }

    @Override
    public List<TaxonGroupDTO> getAvailableTaxonGroups(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.TAXON_GROUP) && !forceNoContext) {
            return contextService.getFilteredTaxonGroups(dataContext.getContextId());
        } else {
            return referentialService.getTaxonGroups();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getAvailableTaxons(TaxonGroupDTO taxonGroup, boolean forceNoContext) {
        if (taxonGroup == null) {
            return getAvailableTaxons(forceNoContext);
        } else {
            // Mantis #0029620 don't use taxonGroup.getTaxons() because ehcache don't persist correctly the taxons and taxonGroups
            // Mantis #43919 Use a reference date to correctly use the historical record (Mantis #43919)
            List<TaxonDTO> availableTaxons = referentialService.getTaxons(taxonGroup.getId());
            if (CollectionUtils.isNotEmpty(availableTaxons)) {
                if (dataContext.isContextFiltered(FilterTypeValues.TAXON) && !forceNoContext) {
                    List<TaxonDTO> filteredTaxons = contextService.getFilteredTaxons(dataContext.getContextId());
                    if (CollectionUtils.isNotEmpty(filteredTaxons)) {
                        availableTaxons = (List<TaxonDTO>) CollectionUtils.intersection(availableTaxons, filteredTaxons);
                    }
                }
            }
            referentialService.fillReferentTaxons(availableTaxons);
            return availableTaxons;
        }
    }

    @Override
    public List<TaxonDTO> getAvailableTaxons(boolean forceNoContext) {
        List<TaxonDTO> availableTaxons;
        if (dataContext.isContextFiltered(FilterTypeValues.TAXON) && !forceNoContext) {
            availableTaxons = contextService.getFilteredTaxons(dataContext.getContextId());
        } else {
            availableTaxons = referentialService.getTaxons(null);
        }
        referentialService.fillReferentTaxons(availableTaxons);
        return availableTaxons;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getAvailablePmfms(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.PMFM) && !forceNoContext) {
            return contextService.getFilteredPmfms(dataContext.getContextId());
        } else {
            return referentialService.getPmfms(StatusFilter.ACTIVE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> getAvailableDepartments(boolean forceNoContext) {
        if (dataContext.isContextFiltered(FilterTypeValues.DEPARTMENT) && !forceNoContext) {
            return contextService.getFilteredDepartments(dataContext.getContextId());
        } else {
            return referentialService.getDepartments();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonDTO> getAvailableUsers(boolean forceNoFilter) {
        if (dataContext.isContextFiltered(FilterTypeValues.USER) && !forceNoFilter) {
            return contextService.getFilteredUsers(dataContext.getContextId());
        } else {
            return userService.getActiveUsers();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDataReadyToSynchronize() {
        // TODO retourner true si des données sont prêtes à être exporter
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateSurveys(Collection<? extends SurveyDTO> surveys, String validationComment, ProgressionCoreModel progressionModel) {

        if (CollectionUtils.isEmpty(surveys)) return;

        // compute validation date
        Date validationDate = new Date(System.currentTimeMillis());

        // collect surveys to validate
        List<SurveyDTO> surveysToValidate = surveys.stream()
                .filter(survey -> !survey.isDirty() && survey.getControlDate() != null && survey.getValidationDate() == null)
                .collect(Collectors.toList());

        // collect survey ids
        List<ValidationBean> allSurveyIds = surveysToValidate.stream().map(SurveyDTO::getId).distinct().map(ValidationBean::new).collect(Collectors.toList());
        int chunkSize = config.getMassiveProcessChunkSize();
        List<List<ValidationBean>> chunkSurveyIds = Lists.partition(allSurveyIds, chunkSize);
        boolean enableMassiveUpdate = chunkSurveyIds.size() > 1;

        if (enableMassiveUpdate) {
            // enable massive update
            persistenceService.enableMassiveUpdate();
        }

        long start = System.currentTimeMillis();
        try {
            progressionModel.setTotal(chunkSurveyIds.size());
            for (int chunk = 0; chunk < chunkSurveyIds.size(); chunk++) {

                List<ValidationBean> surveyIds = chunkSurveyIds.get(chunk);
                progressionModel.increments(t("reefdb.service.observation.validation.progression",
                        allSurveyIds.size(), chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size()));
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Validating %s-%s of %s surveys", chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size(), allSurveyIds.size()));
                }

                loopBackService.validateSurveys(surveyIds, validationDate, validationComment);
            }

        } finally {

            if (enableMassiveUpdate) {
                // disable massive update (default)
                persistenceService.disableMassiveUpdate();
            }
        }

        // perform update on survey DTO, if effectively validated
        surveysToValidate.forEach(survey -> {

            Optional<ValidationBean> validationFound = allSurveyIds.stream().filter(validationBean -> validationBean.getSurveyId() == survey.getId()).findFirst();
            Assert.isTrue(validationFound.isPresent());
            ValidationBean validation = validationFound.get();

            if (validation.isValidated()) {
                // update survey bean validation data
                survey.setValidationDate(validationDate);
                survey.setValidationComment(validationComment);

                // update synchronization status
                survey.setSynchronizationStatus(
                        validation.isReadyToSynchronize()
                                ? SynchronizationStatusValues.READY_TO_SYNCHRONIZE.toSynchronizationStatusDTO()
                                : SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO()
                );

                // reset dirty flag
                survey.setDirty(false);
            }

        });

        if (log.isDebugEnabled()) {
            log.debug(String.format("Validation total time = %s", Times.durationToString(System.currentTimeMillis() - start)));
        }

    }

    @Override
    public void validateSurveys(List<ValidationBean> validationBeans, Date validationDate, String validationComment) {

        Assert.notEmpty(validationBeans);
        Assert.notNull(validationDate);

        // Trim to null, to avoid empty comment
        final String finalValidationComment = StringUtils.trimToNull(validationComment);
        long start = System.currentTimeMillis();

        // load each survey to check local referential
        validationBeans.forEach(validationBean -> {

            // retrieve survey with full measurements
            SurveyDTO loadedSurvey = getSurveyWithoutPmfmFiltering(validationBean.getSurveyId());

            // get ready to synchronize flag
            boolean hasLocalReferential = hasLocalReferential(loadedSurvey);
            validationBean.setReadyToSynchronize(!hasLocalReferential);

            // If national program, should never use local referential
            if (!isLocalReferential(loadedSurvey.getProgram()) && hasLocalReferential) {
                log.warn(String.format("Cannot validate survey [id=%s] because of the use of local referential", validationBean.getSurveyId()));
                validationBean.setValidated(false);
            }

        });

        if (log.isDebugEnabled()) {
            log.debug(String.format("check local referential of %d survey(s) in %s",
                    validationBeans.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }

        // update survey validation date, set comment and update validation history (validator, date, comment, save old comment)
        List<ValidationBean> validatedBeans = validationBeans.stream().filter(ValidationBean::isValidated).collect(Collectors.toList());
        start = System.currentTimeMillis();
        validatedBeans.forEach(validated -> surveyDao.validate(validated.getSurveyId(), validationDate, finalValidationComment, dataContext.getRecorderPersonId(), validated.isReadyToSynchronize()));
        if (log.isDebugEnabled()) {
            log.debug(String.format("validation of %d survey(s) in %s",
                    validatedBeans.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }
        List<Integer> validatedSurveyIds = validatedBeans.stream().map(ValidationBean::getSurveyId).collect(Collectors.toList());

        // update sampling operation validation date
        start = System.currentTimeMillis();
        int nbSamplingOperations = samplingOperationDao.validateBySurveyIds(validatedSurveyIds, validationDate);
        if (log.isDebugEnabled()) {
            log.debug(String.format("validation of %d survey(s) in %s : %s sampling operations",
                    validatedSurveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbSamplingOperations));
        }

        // update measurements validation date
        start = System.currentTimeMillis();
        int nbMeasurements = measurementDao.validateAllMeasurementsBySurveyIds(validatedSurveyIds, validationDate);
        if (log.isDebugEnabled()) {
            log.debug(String.format("validation of %d survey(s) in %s : %s (taxon)measurements",
                    validatedSurveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        // update photos validation date
        start = System.currentTimeMillis();
        int nbPhotos = photoDao.validateBySurveyIds(validatedSurveyIds, validationDate);
        if (log.isDebugEnabled()) {
            log.debug(String.format("validation of %d survey(s) in %s : %s photos",
                    validatedSurveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }

    }

    private boolean hasLocalReferential(SurveyDTO survey) {
        return !hasNoLocalReferential(survey);
    }

    private boolean hasNoLocalReferential(SurveyDTO survey) {

        // read status properties of all survey properties and children
        if (isLocalReferential(survey.getLocation())) return false;
        if (isLocalReferential(survey.getObservers())) return false;
        if (isLocalReferential(survey.getRecorderDepartment())) return false;
        if (isLocalReferential(survey.getProgram())) return false;
        if (isLocalReferential(survey.getDepth())) return false;
        if (isLocalReferential(survey.getPositioning())) return false;

        // measurements
        if (CollectionUtils.isNotEmpty(survey.getMeasurements())) {
            for (MeasurementDTO measurement : survey.getMeasurements()) {
                if (isLocalReferential(measurement.getAnalyst())) return false;
                if (isLocalReferential(measurement.getPmfm())) return false;
                if (isLocalReferential(measurement.getQualitativeValue())) return false;
                if (isLocalReferential(measurement.getTaxon())) return false;
                if (isLocalReferential(measurement.getTaxonGroup())) return false;
            }
        }
        if (CollectionUtils.isNotEmpty(survey.getIndividualMeasurements())) {
            for (MeasurementDTO measurement : survey.getIndividualMeasurements()) {
                if (isLocalReferential(measurement.getAnalyst())) return false;
                if (isLocalReferential(measurement.getPmfm())) return false;
                if (isLocalReferential(measurement.getQualitativeValue())) return false;
                if (isLocalReferential(measurement.getTaxon())) return false;
                if (isLocalReferential(measurement.getTaxonGroup())) return false;
            }
        }

        // photos
        if (CollectionUtils.isNotEmpty(survey.getPhotos())) {
            for (PhotoDTO photo : survey.getPhotos()) {
                if (isLocalReferential(photo.getPhotoType())) return false;
            }
        }

        // sampling operations
        if (CollectionUtils.isNotEmpty(survey.getSamplingOperations())) {
            for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
                if (isLocalReferential(samplingOperation.getSamplingDepartment())) return false;
                if (isLocalReferential(samplingOperation.getSamplingEquipment())) return false;
                if (isLocalReferential(samplingOperation.getPositioning())) return false;
                if (isLocalReferential(samplingOperation.getSizeUnit())) return false;

                // measurements
                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        if (isLocalReferential(measurement.getAnalyst())) return false;
                        if (isLocalReferential(measurement.getPmfm())) return false;
                        if (isLocalReferential(measurement.getQualitativeValue())) return false;
                        if (isLocalReferential(measurement.getTaxon())) return false;
                        if (isLocalReferential(measurement.getTaxonGroup())) return false;
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        if (isLocalReferential(measurement.getAnalyst())) return false;
                        if (isLocalReferential(measurement.getPmfm())) return false;
                        if (isLocalReferential(measurement.getQualitativeValue())) return false;
                        if (isLocalReferential(measurement.getTaxon())) return false;
                        if (isLocalReferential(measurement.getTaxonGroup())) return false;
                    }
                }

            }
        }

        return true;
    }

    private boolean isLocalReferential(BaseReferentialDTO bean) {
        return bean != null && ReefDbBeans.isLocalReferential(bean);
    }

    private boolean isLocalReferential(Collection<? extends BaseReferentialDTO> beans) {
        if (CollectionUtils.isNotEmpty(beans)) {
            for (BaseReferentialDTO dto : beans) {
                if (isLocalReferential(dto)) return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unvalidateSurveys(Collection<? extends SurveyDTO> surveys, String unvalidationComment, ProgressionCoreModel progressionModel) {

        if (CollectionUtils.isEmpty(surveys)) return;

        // compute unvalidation date
        Date unvalidationDate = new Date(System.currentTimeMillis());

        // collect surveys to unvalidate
        List<SurveyDTO> surveysToUnvalidate = surveys.stream()
                .filter(survey -> survey.getId() != null && survey.getValidationDate() != null)
                .collect(Collectors.toList());

        // collect survey ids
        List<Integer> allSurveyIds = surveysToUnvalidate.stream().map(SurveyDTO::getId).distinct().collect(Collectors.toList());
        int chunkSize = config.getMassiveProcessChunkSize();
        List<List<Integer>> chunkSurveyIds = Lists.partition(allSurveyIds, chunkSize);
        boolean enableMassiveUpdate = chunkSurveyIds.size() > 1;

        if (enableMassiveUpdate) {
            // enable massive update
            persistenceService.enableMassiveUpdate();
        }

        long start = System.currentTimeMillis();
        try {
            progressionModel.setTotal(chunkSurveyIds.size());
            for (int chunk = 0; chunk < chunkSurveyIds.size(); chunk++) {

                List<Integer> surveyIds = chunkSurveyIds.get(chunk);
                progressionModel.increments(t("reefdb.service.observation.unvalidation.progression",
                        allSurveyIds.size(), chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size()));
                if (log.isDebugEnabled()) {
                    log.debug(String.format("Unvalidating %s-%s of %s surveys", chunk * chunkSize + 1, chunk * chunkSize + surveyIds.size(), allSurveyIds.size()));
                }
                loopBackService.unvalidateSurveys(surveyIds, unvalidationDate, unvalidationComment);
            }

        } finally {

            if (enableMassiveUpdate) {
                // disable massive update (default)
                persistenceService.disableMassiveUpdate();
            }
        }

        // perform update on DTO
        surveysToUnvalidate.forEach(survey -> {

            // update survey bean validation data
            survey.setValidationDate(null);
            survey.setValidationComment(null);
            survey.setQualificationDate(null);
            survey.setQualificationComment(null);

            // update synchronization status
            survey.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

            // reset dirty flag
            survey.setDirty(false);

        });

        if (log.isDebugEnabled()) {
            log.debug(String.format("Unvalidation total time = %s", Times.durationToString(System.currentTimeMillis() - start)));
        }
    }

    @Override
    public void unvalidateSurveys(List<Integer> surveyIds, Date unvalidationDate, String unvalidationComment) {

        Assert.notEmpty(surveyIds);
        Assert.notNull(unvalidationDate);
        Assert.notBlank(unvalidationComment);
        int validatorId = dataContext.getRecorderPersonId();

        // Trim to null, to avoid empty comment
        unvalidationComment = StringUtils.trimToNull(unvalidationComment);

        long start = System.currentTimeMillis();
        for (Integer surveyId : surveyIds) {

            // update survey validation date, set comment and update validation history (validator, date, comment, save old comment)
            surveyDao.unvalidate(surveyId, unvalidationDate, unvalidationComment, validatorId);

        }
        if (log.isDebugEnabled()) {
            log.debug(String.format("unvalidation of %d survey(s) in %s",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start)));
        }

        // unvalidateMeasurementsBySurveyId
        start = System.currentTimeMillis();
        int nbMeasurements = measurementDao.unvalidateAllMeasurementsBySurveyIds(surveyIds, unvalidationDate, validatorId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("unvalidation of %d survey(s) in %s : %s survey's (taxon)measurements",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        // unvalidatePhotosBySurveyId
        start = System.currentTimeMillis();
        int nbPhotos = photoDao.unvalidateBySurveyIds(surveyIds, unvalidationDate, validatorId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("unvalidation of %d survey(s) in %s : %s survey's photos",
                    surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }

        // unvalidateSamplingOperationsBySurveyId
        start = System.currentTimeMillis();
        int nbSamplingOperations = samplingOperationDao.unvalidateBySurveyIds(surveyIds, unvalidationDate, validatorId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("unvalidation of %d survey(s) in %s : %s sampling operations",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbSamplingOperations));
        }

        // Unvalidate all measurements and photos
        List<Integer> samplingOperationIds = surveyIds.stream().flatMap(surveyId -> samplingOperationDao.getSamplingOperationIdsBySurveyId(surveyId).stream()).collect(Collectors.toList());

        if (samplingOperationIds.isEmpty()) return;

        start = System.currentTimeMillis();
        nbMeasurements = measurementDao.unvalidateAllMeasurementsBySamplingOperationIds(samplingOperationIds, unvalidationDate, validatorId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("unvalidation of %d survey(s) in %s : %s sampling operation's (taxon)measurements",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbMeasurements));
        }

        start = System.currentTimeMillis();
        nbPhotos = photoDao.unvalidateBySamplingOperationIds(samplingOperationIds, unvalidationDate, validatorId);
        if (log.isDebugEnabled()) {
            log.debug(String.format("unvalidation of %d survey(s) in %s : %s sampling operation's photos",
                surveyIds.size(), Times.durationToString(System.currentTimeMillis() - start), nbPhotos));
        }

    }

    @Override
    public boolean isQualified(SurveyDTO survey) {
        Assert.notNull(survey);
        Assert.notNull(survey.getId());

        return survey.getQualificationDate() != null
               // Go deeper to find qualified data
               || surveyDao.isQualified(survey.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countSurveysWithProgramAndLocations(String programCode, List<Integer> locationIds) {
        Assert.notBlank(programCode);

        return surveyDao.countSurveysWithProgramAndLocations(programCode, locationIds);
    }

    @Override
    public Long countSurveysWithProgramLocationAndOutsideDates(String programCode, int appliedStrategyId, int locationId, LocalDate startDate, LocalDate endDate) {
        Assert.notBlank(programCode);
        Assert.notNull(startDate);
        Assert.notNull(endDate);

        return surveyDao.countSurveysWithProgramLocationAndOutsideDates(
                programCode,
                appliedStrategyId,
                locationId,
                Dates.convertToDate(startDate, config.getDbTimezone()),
                Dates.convertToDate(endDate, config.getDbTimezone()));
    }

    @Override
    public Long countSurveysWithProgramLocationAndInsideDates(String programCode, int appliedStrategyId, int locationId, LocalDate startDate, LocalDate endDate) {
        Assert.notBlank(programCode);
        Assert.notNull(startDate);
        Assert.notNull(endDate);

        return surveyDao.countSurveysWithProgramLocationAndInsideDates(
                programCode,
                appliedStrategyId,
                locationId,
                Dates.convertToDate(startDate, config.getDbTimezone()),
                Dates.convertToDate(endDate, config.getDbTimezone()));
    }

    @Override
    public Long countSurveysWithProgramLocationAndOutsideDates(ProgStratDTO progStrat, int locationId) {
        Assert.notNull(progStrat);

        return surveyDao.countSurveysWithProgramLocationAndOutsideDates(
                progStrat.getProgram().getCode(),
                progStrat.getAppliedStrategyId(),
                locationId,
                Dates.convertToDate(progStrat.getStartDate(), config.getDbTimezone()),
                Dates.convertToDate(progStrat.getEndDate(), config.getDbTimezone()));
    }

    @Override
    public Long countSurveysWithCampaign(int campaignId) {

        return campaignService.countSurveysWithCampaign(campaignId);
    }

    @Override
    public Set<MoratoriumPmfmDTO> getPmfmsUnderMoratorium(SurveyDTO survey) {
        if (survey == null || survey.getProgram() == null || survey.getDate() == null || survey.getLocation() == null)
            return null;
        ProgramDTO program = survey.getProgram();
        Set<MoratoriumPmfmDTO> result = new HashSet<>();

        if (!program.isMoratoriumsEmpty()
            && !dataContext.isSurveyViewable(survey)
        ) {
            // Populate partial moratoriums
            program.getMoratoriums().stream()
                .filter(moratorium -> !moratorium.isGlobal())
                .filter(moratorium -> moratorium.getPeriods().stream()
                    .anyMatch(period -> !survey.getDate().isAfter(period.getEndDate()) && !survey.getDate().isBefore(period.getStartDate()))
                )
                .filter(moratorium -> {
                        if (CollectionUtils.isEmpty(moratorium.getLocationIds())
                            && CollectionUtils.isEmpty(moratorium.getCampaignIds())
                            && CollectionUtils.isEmpty(moratorium.getOccasionIds())) {
                            return true;
                        }
                        boolean ok = false;
                        if (CollectionUtils.isNotEmpty(moratorium.getLocationIds())) {
                            ok = moratorium.getLocationIds().contains(survey.getLocation().getId());
                        }
                        if (CollectionUtils.isNotEmpty(moratorium.getCampaignIds()) && survey.getCampaign() != null) {
                            ok |= moratorium.getCampaignIds().contains(survey.getCampaign().getId());
                        }
                        if (CollectionUtils.isNotEmpty(moratorium.getOccasionIds()) && survey.getOccasion() != null) {
                            ok |= moratorium.getOccasionIds().contains(survey.getOccasion().getId());
                        }
                        return ok;
                    }
                )
                .forEach(moratorium -> result.addAll(moratorium.getPmfms()));
        }

        return result;
    }

    /**
     * Apply default analysis department on all measurements from a survey, from strategy
     *
     * TODO pas le meme fonctionnement que DALI, normal tant que l'analyste est liée au pmfm et pas à la strategie appliquée
     *
     * @param survey survey
     */
    private void applyDefaultAnalysisDepartment(SurveyDTO survey) {
        Assert.notNull(survey);

        // Get analysis departments from apply strategies
        Map<Integer, DepartmentDTO> analysisDepartmentByPmfmId = programStrategyService.getAnalysisDepartmentsByPmfmMapBySurvey(survey);

        // Nothing to apply: skip
        if (MapUtils.isEmpty(analysisDepartmentByPmfmId)) {
            return;
        }

        // If measurement loaded
        if (survey.isMeasurementsLoaded()) {
            // For each measurement, apply the default analysis department (if not set)
            for (MeasurementDTO measurement : survey.getMeasurements()) {
                if (measurement.getAnalyst() == null
                        && measurement.getPmfm() != null
                        && measurement.getPmfm().getId() != null) {

                    Integer pmfmId = measurement.getPmfm().getId();
                    DepartmentDTO analysisDepartment = analysisDepartmentByPmfmId.get(pmfmId);
                    measurement.setAnalyst(analysisDepartment);
                }
            }

            // For each individual measurement, apply the default analysis department (if not set)
            for (MeasurementDTO measurement : survey.getIndividualMeasurements()) {
                if (measurement.getAnalyst() == null
                        && measurement.getPmfm() != null
                        && measurement.getPmfm().getId() != null) {

                    Integer pmfmId = measurement.getPmfm().getId();
                    DepartmentDTO analysisDepartment = analysisDepartmentByPmfmId.get(pmfmId);
                    measurement.setAnalyst(analysisDepartment);
                }
            }
        }

        // if sampling operations are loaded
        if (survey.isSamplingOperationsLoaded()
                && CollectionUtils.isNotEmpty(survey.getSamplingOperations())) {

            // Apply on each sampling operation measurements
            for (SamplingOperationDTO samplingOperation : survey.getSamplingOperations()) {
                applyDefaultAnalysisDepartment(samplingOperation, analysisDepartmentByPmfmId);
            }
        }
    }

    /**
     * Apply default analysis department on all measurements from a sampling operation, from strategy
     *
     * @param samplingOperation          samplingOperation
     * @param analysisDepartmentByPmfmId analysisDepartmentByPmfmId
     */
    private void applyDefaultAnalysisDepartment(SamplingOperationDTO samplingOperation, Map<Integer, DepartmentDTO> analysisDepartmentByPmfmId) {
        if (samplingOperation.isMeasurementsLoaded()) {
            // For each measurement, apply the default analysis department (if not set)
            for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                if (measurement.getAnalyst() == null
                        && measurement.getPmfm() != null
                        && measurement.getPmfm().getId() != null) {

                    Integer pmfmId = measurement.getPmfm().getId();
                    DepartmentDTO analysisDepartment = analysisDepartmentByPmfmId.get(pmfmId);
                    measurement.setAnalyst(analysisDepartment);
                }
            }
        }

        if (samplingOperation.isIndividualMeasurementsLoaded()) {
            // For each measurement, apply the default analysis department (if not set)
            for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                if (measurement.getAnalyst() == null
                        && measurement.getPmfm() != null
                        && measurement.getPmfm().getId() != null) {

                    Integer pmfmId = measurement.getPmfm().getId();
                    DepartmentDTO analysisDepartment = analysisDepartmentByPmfmId.get(pmfmId);
                    measurement.setAnalyst(analysisDepartment);
                }
            }
        }
    }

}
