package fr.ifremer.reefdb.service.persistence;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.DatabaseSchemaDao;
import fr.ifremer.quadrige3.core.exception.DatabaseSchemaUpdateException;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.exception.VersionNotFoundException;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.program.ReefDbProgramDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.data.survey.ReefDbCampaignDao;
import fr.ifremer.reefdb.dao.referential.ReefDbAnalysisInstrumentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.referential.ReefDbSamplingEquipmentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dao.referential.pmfm.*;
import fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonGroupDao;
import fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonNameDao;
import fr.ifremer.reefdb.dao.system.context.ReefDbContextDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.system.SystemService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.version.Version;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonGroupDao.ALL_TAXON_GROUPS_CACHE;
import static fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonNameDao.TAXON_NAME_BY_TAXON_GROUP_ID_CACHE;

/**
 * <p>PersistenceServiceImpl class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Service("reefdbPersistenceService")
public class PersistenceServiceImpl implements PersistenceService {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(PersistenceServiceImpl.class);

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefdbSystemService")
    protected SystemService systemService;

    @Resource
    protected ReefDbConfiguration config;

    @Resource
    private DatabaseSchemaDao databaseSchemaDao;

    @Resource(name = "reefDbContextDao")
    private ReefDbContextDao contextDao;

    @Resource(name = "reefDbProgramDao")
    private ReefDbProgramDao programDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefDbReferentialDao")
    private ReefDbReferentialDao referentialDao;

    @Resource(name = "reefDbUnitDao")
    private ReefDbUnitDao unitDao;

    @Resource(name = "reefDbMonitoringLocationDao")
    private ReefDbMonitoringLocationDao monitoringLocationDao;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource(name = "reefDbParameterDao")
    private ReefDbParameterDao parameterDao;

    @Resource(name = "reefDbMatrixDao")
    private ReefDbMatrixDao matrixDao;

    @Resource(name = "reefDbFractionDao")
    private ReefDbFractionDao fractionDao;

    @Resource(name = "reefDbMethodDao")
    private ReefDbMethodDao methodDao;

    @Resource(name = "reefDbTaxonGroupDao")
    private ReefDbTaxonGroupDao taxonGroupDao;

    @Resource(name = "reefDbTaxonNameDao")
    private ReefDbTaxonNameDao taxonNameDao;

    @Resource(name = "reefDbAnalysisInstrumentDao")
    protected ReefDbAnalysisInstrumentDao analysisInstrumentDao;

    @Resource(name = "reefDbSamplingEquipmentDao")
    protected ReefDbSamplingEquipmentDao samplingEquipmentDao;

    @Resource(name = "reefDbCampaignDao")
    protected ReefDbCampaignDao campaignDao;

    private boolean compactDatabaseOnClose = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() {
        // Useful in dev mode
        if (config.isCleanCacheAtStartup()) {
            clearAllCaches();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        shutdownDatabase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Version getDbVersion() {
        try {
            if (!databaseSchemaDao.isDbLoaded()) {
                throw new VersionNotFoundException("db is not open");
            }
            return databaseSchemaDao.getSchemaVersion();
        } catch (VersionNotFoundException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("Could not find db version", e);
            }
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Version getApplicationVersion() {
        return databaseSchemaDao.getSchemaVersionIfUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateSchema() {
        try {
            databaseSchemaDao.updateSchema();
        } catch (DatabaseSchemaUpdateException e) {
            throw new ApplicationTechnicalException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void compactDb() {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Compacting database");
        }

        Daos.compactDatabase(getDataSource());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearAllCaches() {
        cacheService.clearAllCaches();
    }

    /**
     * {@inheritDoc}
     */
    public void loadDefaultCaches(ProgressionCoreModel progressionModel) {
        if (config.isCacheEnabledAtStartup()) {

            if (LOG.isDebugEnabled()) {
                LOG.debug("Loading default caches...");
            }

            loadReferentialCaches(progressionModel);

            if (LOG.isDebugEnabled()) {
                LOG.debug("Loading default caches... [OK]");
            }
        }
    }

    @Override
    public void enableMassiveUpdate() {

        if (!Daos.isHsqlFileDatabase(config.getJdbcUrl())) return;

        if (LOG.isDebugEnabled()) {
            LOG.debug("Enable massive update behavior [CHECKPOINT first]");
        }

        // checkpoint first
        Daos.sqlUpdate(getDataSource(), "CHECKPOINT");

        if (LOG.isDebugEnabled()) {
            LOG.debug("Enable massive update behavior");
        }

        // set incremental backup disabled
        Daos.sqlUpdate(getDataSource(), "SET FILES BACKUP INCREMENT FALSE");

    }

    @Override
    public void disableMassiveUpdate() {

        if (!Daos.isHsqlFileDatabase(config.getJdbcUrl())) return;

        if (LOG.isDebugEnabled()) {
            LOG.debug("Disable massive update behavior [CHECKPOINT first]");
        }

        // checkpoint first
        Daos.sqlUpdate(getDataSource(), "CHECKPOINT");

        if (LOG.isDebugEnabled()) {
            LOG.debug("Disable massive update behavior");
        }

        // set incremental backup enabled (default)
        Daos.sqlUpdate(getDataSource(), "SET FILES BACKUP INCREMENT TRUE");

    }

    @Override
    public void compactDatabaseOnClose() {
        this.compactDatabaseOnClose = true;
    }

    /* -- internal methods -- */

    /**
     * <p>loadReferentialCaches.</p>
     *
     * @param progressionModel a {@link ProgressionCoreModel} object.
     */
    private void loadReferentialCaches(ProgressionCoreModel progressionModel) {

        List<CacheLoaderTask> tasks = new ArrayList<>();
        tasks.add(new CacheLoaderTask("All contexts", () -> contextDao.getAllContext()));
        tasks.add(new CacheLoaderTask("Active departments", () -> departmentDao.getAllDepartments(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active users", () -> quserDao.getAllUsers(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All programs", () -> programDao.getAllPrograms()));
        tasks.add(new CacheLoaderTask("All campaigns", () -> campaignDao.getAllCampaigns()));
        tasks.add(new CacheLoaderTask("All depth levels", () -> referentialDao.getAllDepthLevels()));
        tasks.add(new CacheLoaderTask("All grouping types", () -> referentialDao.getAllGroupingTypes()));
        tasks.add(new CacheLoaderTask("All positioning systems", () -> referentialDao.getAllPositioningSystems()));
        tasks.add(new CacheLoaderTask("Active quality flags", () -> referentialDao.getAllQualityFlags(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active analysis instruments", () -> analysisInstrumentDao.getAllAnalysisInstruments(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active sampling equipments", () -> samplingEquipmentDao.getAllSamplingEquipments(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active units", () -> unitDao.getAllUnits(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All coordinates", () -> monitoringLocationDao.getAllCoordinates()));
        tasks.add(new CacheLoaderTask("All locations", () -> monitoringLocationDao.getAllLocations(StatusFilter.ALL.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active locations", () -> monitoringLocationDao.getAllLocations(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active harbours", () -> monitoringLocationDao.getAllHarbours(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active parameter groups", () -> parameterDao.getAllParameterGroups(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active parameters", () -> parameterDao.getAllParameters(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active matrices", () -> matrixDao.getAllMatrices(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active fractions", () -> fractionDao.getAllFractions(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active methods", () -> methodDao.getAllMethods(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("Active pmfmus", () -> pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes())));
        tasks.add(new CacheLoaderTask("All taxon names and reference taxons", () -> taxonNameDao.fillReferents(taxonNameDao.getAllTaxonNames())));
        tasks.add(new CacheLoaderTask("All taxon groups", () -> {
            // read previous reference date in taxon group - taxon map
            Cache cache = cacheService.getCache(TAXON_NAME_BY_TAXON_GROUP_ID_CACHE);
            if (cache != null && cache.get(LocalDate.now()) == null) {
                // if the cache doesn't hold this reference date, clear the taxon groups cache
                cacheService.clearCache(ALL_TAXON_GROUPS_CACHE);
            }
            taxonGroupDao.getAllTaxonGroups();
        }));

        // Run the garbage collector before (Mantis #59518)
        System.gc();

        // run all
        progressionModel.setTotal(tasks.size());
        tasks.forEach(task -> {
            try {
                try {
                    task.run();
                } catch (OutOfMemoryError error) {
                    LOG.warn(String.format("Try to resolve a memory exception when loading '%s': Run the garbage collector then retry.", task.getName()));
                    System.gc();
                    Thread.sleep(1000);
                    task.run();
                }
            } catch (Throwable t) {
                throw new QuadrigeTechnicalException(String.format("Error when loading '%s'", task.getName()), t);
            }
            progressionModel.increments(1);
        });
    }

    /**
     * <p>shutdownDatabase.</p>
     */
    private void shutdownDatabase() {
        // Do not shutdown if database run as server
        if (!Daos.isHsqlFileDatabase(config.getJdbcUrl())) {
            return;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("shutting down database");
        }
        Daos.shutdownDatabase(getDataSource(), compactDatabaseOnClose);
        if (LOG.isDebugEnabled()) {
            LOG.debug("database down");
        }
    }

    /**
     * <p>getDataSource.</p>
     *
     * @return a {@link javax.sql.DataSource} object.
     */
    protected DataSource getDataSource() {
        return ReefDbServiceLocator.instance().getService("dataSource", DataSource.class);
    }

    static class CacheLoaderTask implements Runnable {
        private final String name;
        private final Runnable runnable;

        CacheLoaderTask(String name, Runnable runnable) {
            this.name = name;
            this.runnable = runnable;
        }

        public String getName() {
            return name;
        }

        @Override
        public void run() {
            this.runnable.run();
        }
    }
}
