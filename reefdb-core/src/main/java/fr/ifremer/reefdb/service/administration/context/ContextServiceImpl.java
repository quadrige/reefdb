/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.service.administration.context;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonParseException;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.gson.Gsons;
import fr.ifremer.quadrige3.core.exception.Exceptions;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.dao.administration.program.ReefDbProgramDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.data.survey.ReefDbCampaignDao;
import fr.ifremer.reefdb.dao.referential.ReefDbAnalysisInstrumentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbSamplingEquipmentDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dao.referential.pmfm.ReefDbPmfmDao;
import fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonGroupDao;
import fr.ifremer.reefdb.dao.referential.taxon.ReefDbTaxonNameDao;
import fr.ifremer.reefdb.dao.system.context.ReefDbContextDao;
import fr.ifremer.reefdb.dao.system.filter.ReefDbFilterDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import fr.ifremer.reefdb.vo.ContextProxy;
import fr.ifremer.reefdb.vo.ContextVO;
import fr.ifremer.reefdb.vo.FilterProxy;
import fr.ifremer.reefdb.vo.FilterVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Service allowing a user to manage context
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Service("reefdbContextService")
public class ContextServiceImpl implements ContextService {

    private static final Log LOG = LogFactory.getLog(ContextServiceImpl.class);

    @Resource(name = "reefDbFilterDao")
    protected ReefDbFilterDao filterDao;

    @Resource(name = "reefDbContextDao")
    protected ReefDbContextDao contextDao;

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource(name = "reefDbAnalysisInstrumentDao")
    protected ReefDbAnalysisInstrumentDao analysisInstrumentDao;

    @Resource(name = "reefDbSamplingEquipmentDao")
    protected ReefDbSamplingEquipmentDao samplingEquipmentDao;

    @Resource(name = "reefDbTaxonNameDao")
    protected ReefDbTaxonNameDao taxonNameDao;

    @Resource(name = "reefDbTaxonGroupDao")
    protected ReefDbTaxonGroupDao taxonGroupDao;

    @Resource(name = "reefDbMonitoringLocationDao")
    protected ReefDbMonitoringLocationDao locationDao;

    @Resource(name = "reefDbProgramDao")
    protected ReefDbProgramDao programDao;

    @Resource(name = "reefDbCampaignDao")
    protected ReefDbCampaignDao campaignDao;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextDTO getContext(Integer contextId) {
        Assert.notNull(contextId);

        try {
            ContextDTO context = contextDao.getContextById(contextId);
            context.setFilters(filterDao.getAllContextFilters(contextId, null));
            return context;

        } catch (DataRetrievalFailureException e) {
            // no context found, return silently
            return null;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContextDTO> getAllContexts() {
        List<ContextDTO> contexts = contextDao.getAllContext();
        if (CollectionUtils.isNotEmpty(contexts)) {
            for (ContextDTO context : contexts) {
                context.setFilters(filterDao.getAllContextFilters(context.getId(), null));
            }
        }
        return contexts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveContexts(List<? extends ContextDTO> contexts) {

        for (ContextDTO context : contexts) {
            if (context.isDirty()) {
                contextDao.saveContext(context);
                context.setDirty(false);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteContexts(List<? extends ContextDTO> contexts) {

        contextDao.deleteContexts(ReefDbBeans.collectIds(contexts));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ContextDTO duplicateContext(ContextDTO context) {
        ContextDTO duplicatedContext = ReefDbBeans.clone(context);

        duplicatedContext.setId(null);
        duplicatedContext.setName("");

        // filters must be copied in a different list to avoid sharing same list
        duplicatedContext.setFilters(ReefDbBeans.clone(context.getFilters()));

        return duplicatedContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void exportContexts(Collection<ContextDTO> contexts, File targetContextFile) {

        // ensure filter elements are loaded
        contexts.forEach(context -> context.getFilters().forEach(this::loadFilteredElements));

        // use json serialization
        ContextProxy contextsToExport = new ContextProxy(contexts.stream().map(this::toContextVO).collect(Collectors.toList()));
        Gsons.serializeToFile(contextsToExport, targetContextFile);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContextDTO> importContexts(File sourceContextFile) {

        try {

            // try first to read new json format (Mantis #43800)
            ContextProxy contextProxy = Gsons.deserializeFile(sourceContextFile, ContextProxy.class);

            if (!contextProxy.getVersion().equals(ContextProxy.CURRENT_VERSION)) {
                LOG.warn(String.format("Context file %s to import is on version %s and the current version is %s",
                        sourceContextFile, contextProxy.getVersion(), ContextProxy.CURRENT_VERSION));
            }

            List<ContextDTO> contexts = new ArrayList<>();
            if (CollectionUtils.isEmpty(contextProxy.getContexts())) {
                if (LOG.isWarnEnabled()) LOG.warn("no context found in file " + sourceContextFile);
                return contexts;
            }

            for (ContextVO contextVO : contextProxy.getContexts()) {

                ContextDTO context = toContextDTO(contextVO);
                context.setDirty(true);
                computeNextContextName(context);

                for (FilterDTO filter : context.getFilters()) {

                    // assign null IDs to filters to force creation
                    filter.setId(null);
                    filter.setDirty(true);
                    // Compute new filter name (see Mantis #30776)
                    computeNextFilterName(filter);
                    saveFilter(filter);
                    filter.setDirty(false);
                }

                contexts.add(context);
            }

            saveContexts(contexts);
            return contexts;

        } catch (QuadrigeTechnicalException e) {

            if (Exceptions.hasCause(e, JsonParseException.class)) {
                // if json deserialization failed, before throw, try the old format
                List<ContextDTO> oldContexts = importOldContexts(sourceContextFile);
                if (oldContexts != null) return oldContexts;
            }

            // else throw exception
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    private List<ContextDTO> importOldContexts(File sourceContextFile) {

        try (FileInputStream fis = new FileInputStream(sourceContextFile);
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            List<ContextDTO> contexts = (List<ContextDTO>) ois.readObject();

            Map<Integer, Integer> savedFilterIds = Maps.newHashMap();
            Map<Integer, String> savedFilterNames = Maps.newHashMap();

            for (ContextDTO c : contexts) {
                c.setId(null);
                c.setDirty(true);
                computeNextContextName(c);

                for (FilterDTO f : c.getFilters()) {
                    Integer importId = f.getId();
                    if (!savedFilterIds.containsKey(importId)) {

                        // check if filters contains local elements. If it does, abort import
                        for (QuadrigeBean bean : f.getElements()) {
                            if (bean.getClass().isAssignableFrom(BaseReferentialDTO.class)
                                    && ((BaseReferentialDTO) bean).getStatus() != null
                                    && ReefDbBeans.isLocalStatus(((BaseReferentialDTO) bean).getStatus())) {
                                throw new ReefDbBusinessException(t("reefdb.error.filter.import.withLocalReferential", f.getName()));
                            }
                        }

                        // assign null IDs to filters to force creation
                        f.setId(null);
                        // Compute new filter name (see Mantis #30776)
                        computeNextFilterName(f);
                        saveFilter(f);
                        savedFilterIds.put(importId, f.getId());
                        savedFilterNames.put(importId, f.getName());
                    } else {
                        // affect already saved filter id and name
                        f.setId(savedFilterIds.get(importId));
                        f.setName(savedFilterNames.get(importId));
                    }
                }
            }

            saveContexts(contexts);
            return contexts;

        } catch (IOException | ClassNotFoundException | ClassCastException ex) {
            // Mantis #30776
            throw new ReefDbTechnicalException(t("reefdb.error.context.import.error"), ex);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getFiltersByType(int filterTypeId) {
        return filterDao.getAllContextFilters(null, filterTypeId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkFiltersNotUsedInContext(List<? extends FilterDTO> filters) {

        List<Integer> filterIds = ReefDbBeans.collectIds(filters);
        return filterIds.isEmpty() || filterDao.checkFiltersNotUsedInContext(filterIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFilters(List<? extends FilterDTO> filters) {
        if (filters == null) return;
        filterDao.deleteFilters(ReefDbBeans.collectIds(filters));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilterDTO getFilter(Integer filterId) {
        return filterDao.getFilterById(filterId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadFilteredElements(FilterDTO filter) {

        if (filter == null) {
            return;
        }

        if (!filter.isFilterLoaded()) {

            filter.setElements(getFilteredElements(filter.getFilterTypeId(), filter.getId()));
            filter.setFilterLoaded(true);
        }
    }

    private List<? extends QuadrigeBean> getFilteredElements(int filterTypeId, Integer filterId) {

        FilterTypeValues contextFilter = FilterTypeValues.getFilterType(filterTypeId);
        if (contextFilter != null) {
            switch (contextFilter) {
                case ANALYSIS_INSTRUMENT:
                    return getFilteredAnalysisInstrumentsByFilterId(filterId);
                case SAMPLING_EQUIPMENT:
                    return getFilteredSamplingEquipmentsByFilterId(filterId);
                case TAXON_GROUP:
                    return getFilteredTaxonGroupsByFilterId(filterId);
                case LOCATION:
                    return getFilteredLocationsByFilterId(filterId);
                case PROGRAM:
                    return getFilteredProgramsByFilterId(filterId);
                case CAMPAIGN:
                    return getFilteredCampaignsByFilterId(filterId);
                case PMFM:
                    return getFilteredPmfmsByFilterId(filterId);
                case DEPARTMENT:
                    return getFilteredDepartmentsByFilterId(filterId);
                case TAXON:
                    return getFilteredTaxonsByFilterId(filterId);
                case USER:
                    return getFilteredUsersByFilterId(filterId);
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllDepartmentFilters() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.DEPARTMENT.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DepartmentDTO> getFilteredDepartments(Integer contextId) {
        Set<DepartmentDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.DEPARTMENT.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredDepartmentsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<DepartmentDTO> getFilteredDepartmentsByFilterId(Integer filterId) {
        List<DepartmentDTO> deps = Lists.newArrayList();
        List<Integer> depIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(depIds)) {
            deps.addAll(departmentDao.getDepartmentsByIds(depIds));
        }
        return deps;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllSamplingEquipmentFilters() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.SAMPLING_EQUIPMENT.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SamplingEquipmentDTO> getFilteredSamplingEquipments(Integer contextId) {
        Set<SamplingEquipmentDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.SAMPLING_EQUIPMENT.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredSamplingEquipmentsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<SamplingEquipmentDTO> getFilteredSamplingEquipmentsByFilterId(Integer filterId) {
        List<SamplingEquipmentDTO> se = Lists.newArrayList();
        List<Integer> seIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(seIds)) {
            se.addAll(samplingEquipmentDao.getSamplingEquipmentsByIds(seIds));
        }
        return se;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllAnalysisInstrumentFilters() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.ANALYSIS_INSTRUMENT.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AnalysisInstrumentDTO> getFilteredAnalysisInstruments(Integer contextId) {
        Set<AnalysisInstrumentDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.ANALYSIS_INSTRUMENT.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredAnalysisInstrumentsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<AnalysisInstrumentDTO> getFilteredAnalysisInstrumentsByFilterId(Integer filterId) {
        List<AnalysisInstrumentDTO> analysisInstruments = Lists.newArrayList();
        List<Integer> aiIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(aiIds)) {
            analysisInstruments.addAll(analysisInstrumentDao.getAnalysisInstrumentsByIds(aiIds));
        }
        return analysisInstruments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveFilter(FilterDTO filter) {
        Assert.notNull(filter.getFilterTypeId());
        filterDao.saveFilter(filter, dataContext.getRecorderPersonId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveFilters(List<? extends FilterDTO> filters) {
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                if (filter.isDirty()) {
                    saveFilter(filter);
                    filter.setDirty(false);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllPmfmFilters() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.PMFM.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PmfmDTO> getFilteredPmfms(Integer contextId) {
        Set<PmfmDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.PMFM.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredPmfmsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<PmfmDTO> getFilteredPmfmsByFilterId(Integer filterId) {
        List<PmfmDTO> pmfms = Lists.newArrayList();
        List<Integer> pmfmIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (!pmfmIds.isEmpty()) {
            pmfms.addAll(pmfmDao.getPmfmsByIds(pmfmIds));
        }
        return pmfms;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllTaxonFilters() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.TAXON.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getFilteredTaxons(Integer contextId) {
        Set<TaxonDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.TAXON.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredTaxonsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<TaxonDTO> getFilteredTaxonsByFilterId(Integer filterId) {
        List<TaxonDTO> taxons = Lists.newArrayList();
        List<Integer> taxonIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(taxonIds)) {
            taxons.addAll(taxonNameDao.getTaxonNamesByIds(taxonIds));
        }
        return taxons;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllTaxonGroupFilters() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.TAXON_GROUP.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonGroupDTO> getFilteredTaxonGroups(Integer contextId) {
        Set<TaxonGroupDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.TAXON_GROUP.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredTaxonGroupsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<TaxonGroupDTO> getFilteredTaxonGroupsByFilterId(Integer filterId) {
        List<TaxonGroupDTO> taxonGroups = Lists.newArrayList();
        List<Integer> tgIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(tgIds)) {
            taxonGroups.addAll(taxonGroupDao.getTaxonGroupsByIds(tgIds));
        }
        return taxonGroups;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllLocationFilter() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.LOCATION.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getFilteredLocations(Integer contextId) {
        Set<LocationDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.LOCATION.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredLocationsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<LocationDTO> getFilteredLocationsByFilterId(Integer filterId) {
        List<LocationDTO> locations = Lists.newArrayList();
        List<Integer> locationIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(locationIds)) {
            locations.addAll(locationDao.getLocationsByIds(locationIds));
        }
        return locations;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllUserFilter() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.USER.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonDTO> getFilteredUsers(Integer contextId) {
        Set<PersonDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.USER.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredUsersByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<PersonDTO> getFilteredUsersByFilterId(Integer filterId) {
        List<PersonDTO> result = Lists.newArrayList();
        List<Integer> userIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(userIds)) {
            result.addAll(quserDao.getUsersByIds(userIds));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> getAllProgramFilter() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.PROGRAM.getFilterTypeId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProgramDTO> getFilteredPrograms(Integer contextId) {
        Set<ProgramDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.PROGRAM.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredProgramsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    @Override
    public List<FilterDTO> getAllCampaignFilter() {
        return filterDao.getAllContextFilters(null, FilterTypeValues.CAMPAIGN.getFilterTypeId());
    }

    @Override
    public List<CampaignDTO> getFilteredCampaigns(Integer contextId) {
        Set<CampaignDTO> result = Sets.newHashSet();
        List<FilterDTO> filters = filterDao.getAllContextFilters(contextId, FilterTypeValues.CAMPAIGN.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(filters)) {
            for (FilterDTO filter : filters) {
                result.addAll(getFilteredCampaignsByFilterId(filter.getId()));
            }
        }
        return Lists.newArrayList(result);
    }

    private List<CampaignDTO> getFilteredCampaignsByFilterId(Integer filterId) {
        List<CampaignDTO> campaigns = Lists.newArrayList();
        List<Integer> campaignIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(filterId), ReefDbBeans.ID_MAPPER);
        if (CollectionUtils.isNotEmpty(campaignIds)) {
            campaigns.addAll(campaignDao.getCampaignsByIds(campaignIds));
        }
        return campaigns;
    }

    private List<ProgramDTO> getFilteredProgramsByFilterId(Integer filterId) {
        List<ProgramDTO> programs = Lists.newArrayList();
        List<String> programCodes = filterDao.getFilteredElementsByFilterId(filterId);
        if (CollectionUtils.isNotEmpty(programCodes)) {
            programs.addAll(programDao.getProgramsByCodes(programCodes));
        }
        return programs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FilterDTO> importFilter(File sourceFilterFile, int filterTypeId) {

        try {

            // try first to read new json format (Mantis #43800)
            FilterProxy filterProxy = Gsons.deserializeFile(sourceFilterFile, FilterProxy.class);

            if (!filterProxy.getVersion().equals(FilterProxy.CURRENT_VERSION)) {
                LOG.warn(String.format("Filter file %s to import is on version %s and the current version is %s",
                        sourceFilterFile, filterProxy.getVersion(), FilterProxy.CURRENT_VERSION));
            }

            List<FilterDTO> filters = new ArrayList<>();
            if (CollectionUtils.isEmpty(filterProxy.getFilters())) {
                if (LOG.isWarnEnabled()) LOG.warn("no filter found in file " + sourceFilterFile);
                return filters;
            }

            for (FilterVO filterVO : filterProxy.getFilters()) {

                // check filter type
                if (filterVO.getType() != filterTypeId) {
                    throw new ReefDbBusinessException(t("reefdb.error.filter.import.wrongFilterType",
                            FilterTypeValues.getFilterType(filterTypeId),
                            FilterTypeValues.getFilterType(filterVO.getType())));
                }

                FilterDTO filter = toFilterDTO(filterVO);
                filter.setId(null);
                filter.setDirty(true);
                computeNextFilterName(filter);
                filters.add(filter);
            }

            saveFilters(filters);
            return filters;

        } catch (QuadrigeTechnicalException e) {

            if (Exceptions.hasCause(e, JsonParseException.class)) {
                // if json deserialization failed, before throw, try the old format
                List<FilterDTO> oldFilters = importOldFilter(sourceFilterFile, filterTypeId);
                if (oldFilters != null) return oldFilters;
            }

            // else throw exception
            throw e;
        }

    }

    @SuppressWarnings("unchecked")
    private List<FilterDTO> importOldFilter(File sourceFilterFile, int filterTypeId) {

        try (FileInputStream fis = new FileInputStream(sourceFilterFile);
             ObjectInputStream ois = new ObjectInputStream(fis)) {

            // apparently yes, works like a charm
            List<FilterDTO> filters = (List<FilterDTO>) ois.readObject();

            // assign null IDs to filters to force creation
            for (FilterDTO f : filters) {

                // check filter type
                if (f.getFilterTypeId() != filterTypeId) {
                    throw new ReefDbBusinessException(t("reefdb.error.filter.import.wrongFilterType",
                            FilterTypeValues.getFilterType(filterTypeId),
                            FilterTypeValues.getFilterType(f.getFilterTypeId())));
                }

                // check if filters contains local elements. If it does, abort import
                for (QuadrigeBean bean : f.getElements()) {
                    if (bean.getClass().isAssignableFrom(BaseReferentialDTO.class)
                            && ((BaseReferentialDTO) bean).getStatus() != null
                            && ReefDbBeans.isLocalStatus(((BaseReferentialDTO) bean).getStatus())) {
                        throw new ReefDbBusinessException(t("reefdb.error.filter.import.withLocalReferential", f.getName()));
                    }
                }

                f.setId(null);
                computeNextFilterName(f);
                saveFilter(f);
            }

            return filters;
        } catch (IOException | ClassNotFoundException | ClassCastException ex) {
            throw new ReefDbTechnicalException(t("reefdb.error.filter.import.error"), ex);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void exportFilter(Collection<FilterDTO> filters, File targetFilterFile) {

        // ensure filter elements are loaded
        filters.forEach(this::loadFilteredElements);

        // use json serialization
        FilterProxy filtersToExport = new FilterProxy(filters.stream().map(this::toFilterVO).collect(Collectors.toList()));
        Gsons.serializeToFile(filtersToExport, targetFilterFile);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FilterDTO duplicateFilter(FilterDTO filter) {
        FilterDTO duplicatedFilter = ReefDbBeans.clone(filter);
        duplicatedFilter.setId(null);
        duplicatedFilter.setName("");

        duplicatedFilter.setElements(ReefDbBeans.clone(filter.getElements()));

        return duplicatedFilter;
    }

    // PRIVATE METHODS

    private FilterVO toFilterVO(FilterDTO filter) {
        FilterVO vo = new FilterVO();
        vo.setType(filter.getFilterTypeId());
        vo.setName(filter.getName());
        vo.setElementIds(ReefDbBeans.getIdsAsString(filter.getElements()));
        return vo;
    }

    private FilterDTO toFilterDTO(FilterVO filter) {
        FilterDTO dto = ReefDbBeanFactory.newFilterDTO();
        dto.setName(filter.getName());
        dto.setFilterTypeId(filter.getType());

        FilterTypeValues filterType = FilterTypeValues.getFilterType(dto.getFilterTypeId());
        Assert.notNull(filterType);
        List<? extends QuadrigeBean> elements = null;
        switch (filterType) {
            case PROGRAM:
                elements = programDao.getProgramsByCodes(filter.getElementIds());
                break;
            case LOCATION:
                elements = locationDao.getLocationsByIds(getValidIds(filter));
                break;
            case CAMPAIGN:
                elements = campaignDao.getCampaignsByIds(getValidIds(filter));
                break;
            case SAMPLING_EQUIPMENT:
                elements = samplingEquipmentDao.getSamplingEquipmentsByIds(getValidIds(filter));
                break;
            case ANALYSIS_INSTRUMENT:
                elements = analysisInstrumentDao.getAnalysisInstrumentsByIds(getValidIds(filter));
                break;
            case TAXON:
                elements = taxonNameDao.getTaxonNamesByIds(getValidIds(filter));
                break;
            case TAXON_GROUP:
                elements = taxonGroupDao.getTaxonGroupsByIds(getValidIds(filter));
                break;
            case DEPARTMENT:
                elements = departmentDao.getDepartmentsByIds(getValidIds(filter));
                break;
            case PMFM:
                elements = pmfmDao.getPmfmsByIds(getValidIds(filter));
                break;
            case USER:
                elements = quserDao.getUsersByIds(getValidIds(filter));
                break;
        }

        if (CollectionUtils.size(filter.getElementIds()) != CollectionUtils.size(elements)) {
            throw new ReefDbBusinessException(t("reefdb.error.import.referentialNotFound.message", filterType.getLabel()));
        }

        dto.setElements(elements);
        dto.setFilterLoaded(true);
        return dto;
    }

    private List<Integer> getValidIds(FilterVO filter) {
        List<Integer> ids = ReefDbBeans.transformCollection(filter.getElementIds(), ReefDbBeans.ID_MAPPER);
        if (ids.stream().anyMatch(id -> id < 0)) {
            throw new ReefDbBusinessException(t("reefdb.error.filter.import.withLocalReferential", filter.getName()));
        }
        return ids;
    }

    private ContextVO toContextVO(ContextDTO context) {
        ContextVO vo = new ContextVO();
        vo.setName(context.getName());
        vo.setDescription(context.getDescription());
        vo.setFilters(context.getFilters().stream().map(this::toFilterVO).collect(Collectors.toList()));
        return vo;
    }

    private ContextDTO toContextDTO(ContextVO context) {
        ContextDTO dto = ReefDbBeanFactory.newContextDTO();
        dto.setName(context.getName());
        dto.setDescription(context.getDescription());
        dto.setFilters(context.getFilters().stream().map(this::toFilterDTO).collect(Collectors.toList()));
        return dto;
    }

    private void computeNextContextName(ContextDTO context) {
        Assert.notNull(context);
        String contextName = context.getName();
        Assert.notBlank(contextName);

        // Check existence of context with same name
        List<ContextDTO> existingContexts = getAllContexts();
        if (CollectionUtils.isNotEmpty(existingContexts)) {
            int suffixInc = 0;
            boolean nameCorrect = false;
            while (!nameCorrect && suffixInc < 100) {
                boolean nameAlreadyExists = false;
                suffixInc++;
                for (ContextDTO existingContext : existingContexts) {
                    if (contextName.equalsIgnoreCase(existingContext.getName())) {
                        nameAlreadyExists = true;
                        // try to add a suffix
                        String suffix = String.format(" (%d)", suffixInc);
                        contextName = context.getName().concat(suffix);
                        break;
                    }
                }
                if (!nameAlreadyExists) {
                    // now, this context has a unique name
                    context.setName(contextName);
                    nameCorrect = true;
                }
            }
        }
    }

    private void computeNextFilterName(FilterDTO filter) {
        Assert.notNull(filter);
        String filterName = filter.getName();
        Assert.notBlank(filterName);

        // Check existence of filter with same name
        List<FilterDTO> existingFilters = getFiltersByType(filter.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(existingFilters)) {
            int suffixInc = 0;
            boolean nameCorrect = false;
            while (!nameCorrect && suffixInc < 100) {
                boolean nameAlreadyExists = false;
                suffixInc++;
                for (FilterDTO existingFilter : existingFilters) {
                    if (filterName.equalsIgnoreCase(existingFilter.getName())) {
                        nameAlreadyExists = true;
                        // try to add a suffix
                        String suffix = String.format(" (%d)", suffixInc);
                        filterName = filter.getName().concat(suffix);
                        break;
                    }
                }
                if (!nameAlreadyExists) {
                    // now, this filter has a unique name
                    filter.setName(filterName);
                    nameCorrect = true;
                }
            }
        }
    }

}
