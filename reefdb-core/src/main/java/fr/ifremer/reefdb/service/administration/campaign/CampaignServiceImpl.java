package fr.ifremer.reefdb.service.administration.campaign;

/*-
 * #%L
 * ReefDb :: Core
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import fr.ifremer.quadrige3.core.dao.data.survey.CampaignDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.vo.data.survey.CampaignVO;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRestClientService;
import fr.ifremer.reefdb.dao.data.survey.ReefDbCampaignDao;
import fr.ifremer.reefdb.dao.system.filter.ReefDbFilterDao;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.SearchDateDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.enums.SearchDateValues;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author peck7 on 02/08/2017.
 */
@Service("reefdbCampaignService")
public class CampaignServiceImpl implements CampaignService {

    private static final Log LOG = LogFactory.getLog(CampaignServiceImpl.class);

    @Resource(name = "reefDbCampaignDao")
    private ReefDbCampaignDao campaignDao;

    @Resource(name = "synchroRestClientService")
    private SynchroRestClientService synchroRestClientService;

    @Resource(name = "reefDbFilterDao")
    private ReefDbFilterDao filterDao;

    @Override
    public List<CampaignDTO> getAllCampaigns() {
        return campaignDao.getAllCampaigns();
    }

    @Override
    public List<CampaignDTO> findCampaignsByCriteria(String name,
                                                     SearchDateDTO startDateSearch, Date startDate1, Date startDate2,
                                                     SearchDateDTO endDateSearch, Date endDate1, Date endDate2) {

        // compute start date operation
        boolean strictStartDate = false;
        if (startDateSearch == null) {
            startDate1 = startDate2 = null;
        } else {
            SearchDateValues startSearchDateValue = SearchDateValues.values()[startDateSearch.getId()];
            switch (startSearchDateValue) {

                case EQUALS:
                    if (startDate1 == null) return null;
                    startDate2 = startDate1;
                    strictStartDate = true;
                    break;
                case BETWEEN:
                    if (startDate1 == null || startDate2 == null) return null;
                    break;
                case BEFORE:
                    strictStartDate = true;
                case BEFORE_OR_EQUALS:
                    if (startDate1 == null) return null;
                    startDate2 = startDate1;
                    startDate1 = null;
                    break;
                case AFTER:
                    strictStartDate = true;
                case AFTER_OR_EQUALS:
                    if (startDate1 == null) return null;
                    startDate2 = null;
                    break;
            }
        }

        // compute end date operation
        boolean strictEndDate = false;
        if (endDateSearch == null) {
            endDate1 = endDate2 = null;
        } else {
            SearchDateValues endSearchDateValue = SearchDateValues.values()[endDateSearch.getId()];
            switch (endSearchDateValue) {

                case EQUALS:
                    if (endDate1 == null) return null;
                    endDate2 = endDate1;
                    strictEndDate = true;
                    break;
                case BETWEEN:
                    if (endDate1 == null || endDate2 == null) return null;
                    break;
                case BEFORE:
                    strictEndDate = true;
                case BEFORE_OR_EQUALS:
                    if (endDate1 == null) return null;
                    endDate2 = endDate1;
                    endDate1 = null;
                    break;
                case AFTER:
                    strictEndDate = true;
                case AFTER_OR_EQUALS:
                    if (endDate1 == null) return null;
                    endDate2 = null;
                    break;
            }
        }

        return campaignDao.getCampaignsByCriteria(name, startDate1, startDate2, strictStartDate, endDate1, endDate2, strictEndDate, false);
    }

    @Override
    public List<CampaignDTO> findCampaignsIncludingDate(Date date) {

        return campaignDao.getCampaignsByCriteria(null, null, date, false, date, null, false, true);
    }

    @Override
    public boolean checkCampaignNameDuplicates(Integer id, String name) {
        if (StringUtils.isBlank(name)) return false;

        List<CampaignDTO> campaigns = campaignDao.getCampaignsByName(name);
        int nb = CollectionUtils.size(campaigns);
        if (nb > 1) {
            // multiple campaign have same name (upper case), should be already an error
            LOG.warn(String.format("campaign with name '%s' already exists %s times in database", name, nb));
            return true;
        } else return nb == 1 && !CollectionUtils.extractSingleton(campaigns).getId().equals(id);

    }

    @Override
    public void saveCampaigns(AuthenticationInfo authenticationInfo, Collection<? extends CampaignDTO> campaigns) {

        Assert.notNull(campaigns);

        List<CampaignDTO> dirtyCampaigns = campaigns.stream().filter(CampaignDTO::isDirty).collect(Collectors.toList());

        // Nothing to save
        if (CollectionUtils.isEmpty(dirtyCampaigns)) return;

        Set<Integer> campaignIds = dirtyCampaigns.stream().map(campaign -> {

            // preconditions
            Assert.notNull(campaign.getName());
            Assert.notNull(campaign.getStartDate());
            Assert.notNull(campaign.getManager());

            // save
            campaignDao.saveCampaign(campaign);

            // Todo reporter à la fin
            campaign.setDirty(false);

            return campaign.getId();
        }).collect(Collectors.toSet());

        // save remote campaigns
        List<CampaignVO> savedCampaigns = saveCampaignsOnServer(authenticationInfo, campaignIds);

        // map new id from VO to DTO
        dirtyCampaigns.stream()
                // in case of new campaign, id is negative locally
                .filter(campaign -> campaign.getId() < 0)
                .forEach(campaign -> {
                    // get previous temporary id
                    int negativeCampaignId = campaign.getId();
                    // find by name
                    CampaignVO savedCampaign = ReefDbBeans.findByProperty(savedCampaigns, "campaignNm", campaign.getName());
                    if (savedCampaign == null) {
                        throw new ReefDbBusinessException(String.format("Unable to find saved campaign '%s'", campaign.getName()));
                    }
                    // affect correct id
                    campaign.setId(savedCampaign.getCampaignId());
                    // delete temp campaign
                    campaignDao.remove(negativeCampaignId);
            });

    }

    private List<CampaignVO> saveCampaignsOnServer(AuthenticationInfo authenticationInfo, Set<Integer> campaignIds) {

        if (CollectionUtils.isEmpty(campaignIds)) return new ArrayList<>();

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Sending campaigns [%s] to server", Joiner.on(',').join(campaignIds)));
        }

        // Load VOs
        List<CampaignVO> campaignsToSend = ReefDbBeans.transformCollection(campaignIds,
                campaignId -> (CampaignVO) campaignDao.load(CampaignDao.TRANSFORM_CAMPAIGNVO, campaignId));

        // send to server
        List<CampaignVO> savedCampaigns = synchroRestClientService.saveCampaigns(authenticationInfo, campaignsToSend);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Saving campaigns [%s] from server response", Joiner.on(',').join(savedCampaigns)));
        }

        // save locally
        savedCampaigns.forEach(campaignDao::save);

        return savedCampaigns;
    }

    @Override
    public void deleteCampaign(AuthenticationInfo authenticationInfo, List<Integer> campaignIds) {

        if (CollectionUtils.isEmpty(campaignIds)) return;
        for (Integer campaignId : campaignIds) {
            campaignDao.remove(campaignId);
        }

        deleteCampaignOnServer(authenticationInfo, campaignIds);
    }

    @Override
    public boolean isCampaignUsedByFilter(int campaignId) {

        // search the program in all existing context filters
        List<FilterDTO> campaignFilters = filterDao.getAllContextFilters(null, FilterTypeValues.CAMPAIGN.getFilterTypeId());
        if (CollectionUtils.isNotEmpty(campaignFilters)) {
            for (FilterDTO campaignFilter : campaignFilters) {
                List<Integer> campaignIds = ReefDbBeans.transformCollection(filterDao.getFilteredElementsByFilterId(campaignFilter.getId()), ReefDbBeans.ID_MAPPER);
                if (campaignIds.contains(campaignId)) {
                    return true;
                }
            }
        }
        return false;

    }

    @Override
    public Long countSurveysWithCampaign(int campaignId) {
        return campaignDao.countSurveyUsage(campaignId);
    }

    private void deleteCampaignOnServer(AuthenticationInfo authenticationInfo, List<Integer> campaignIds) {

        // filter only positive ids
        List<Integer> remoteIds = campaignIds.stream().filter(campaignId -> campaignId >= 0).collect(Collectors.toList());

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Delete campaigns [%s] from server", Joiner.on(',').join(remoteIds)));
        }

        // delete campaign from server (only positive ids)
        synchroRestClientService.deleteCampaigns(authenticationInfo, remoteIds);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Campaigns [%s] deleted from server", Joiner.on(',').join(remoteIds)));
        }
    }

}
