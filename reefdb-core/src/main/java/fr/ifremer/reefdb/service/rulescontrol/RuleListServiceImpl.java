package fr.ifremer.reefdb.service.rulescontrol;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.factorization.pmfm.AllowedQualitativeValuesMap;
import fr.ifremer.quadrige3.core.security.AuthenticationInfo;
import fr.ifremer.quadrige3.core.vo.system.rule.RuleListVO;
import fr.ifremer.quadrige3.synchro.service.client.SynchroRestClientService;
import fr.ifremer.quadrige3.ui.core.dto.referential.BaseReferentialDTO;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.system.rule.ReefDbRuleDao;
import fr.ifremer.reefdb.dao.system.rule.ReefDbRuleListDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.*;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.ControlElementValues;
import fr.ifremer.reefdb.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.reefdb.dto.enums.ControlFunctionValues;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation rules control service.
 */
@Service("reefDbRuleListService")
public class RuleListServiceImpl implements RuleListService {

    private static final Log LOG = LogFactory.getLog(RuleListServiceImpl.class);

    @Resource(name = "reefDbRuleListDao")
    protected ReefDbRuleListDao ruleListDao;
    @Resource(name = "reefDbRuleDao")
    protected ReefDbRuleDao ruleDao;
    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;
    @Resource(name = "synchroRestClientService")
    protected SynchroRestClientService synchroRestClientService;
    @Resource(name = "reefdbReferentialService")
    protected ReferentialService referentialService;
    @Resource(name = "reefDbControlRuleService")
    protected ControlRuleService controlRuleService;
    @Autowired
    protected ReefDbConfiguration configuration;

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListDTO getRuleList(String ruleListCode) {
        Assert.notBlank(ruleListCode);
        return ruleListDao.getRuleList(ruleListCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RuleListDTO> getRuleLists() {
        return ruleListDao.getRuleLists();
    }

    @Override
    public List<RuleListDTO> getRuleListsForProgram(String programCode) {
        return ruleListDao.getRuleListsForProgram(programCode);
    }

    @Override
    public boolean ruleListCodeExists(String ruleListCode) {
        return ruleListDao.ruleListExists(ruleListCode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRuleLists(AuthenticationInfo authenticationInfo, List<? extends RuleListDTO> ruleLists) {

        Assert.notNull(ruleLists);

        // get rule lists to save
        List<RuleListDTO> dirtyRuleList = ruleLists.stream().filter(RuleListDTO::isDirty).collect(Collectors.toList());

        // nothing to save
        if (CollectionUtils.isEmpty(dirtyRuleList)) return;

        // Collect rule list codes
        Set<String> ruleListCodes = dirtyRuleList.stream().map(ruleList -> {

            // preconditions
            Assert.notBlank(ruleList.getCode());
            Assert.notEmpty(ruleList.getPrograms());

            // save locally
            ruleListDao.saveRuleList(ruleList, dataContext.getRecorderPersonId());

            return ruleList.getCode();

        }).collect(Collectors.toSet());

        // Save remote rule lists
        saveRuleListsOnServer(authenticationInfo, ruleListCodes);

        // Reset newCode flags
        dirtyRuleList.forEach(ruleList -> {
            ruleList.setDirty(false);
            ruleList.setNewCode(false);
            ruleList.getControlRules().forEach(controlRule -> controlRule.setNewCode(false));
        });
    }

    private void saveRuleListsOnServer(AuthenticationInfo authenticationInfo, Set<String> ruleListCodes) {

        if (CollectionUtils.isEmpty(ruleListCodes)) return;

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Sending rule lists [%s] to server", Joiner.on(',').join(ruleListCodes)));
        }

        // get VOs
        List<RuleListVO> ruleListToSend = ruleListCodes.stream()
                .map(ruleListCode -> (RuleListVO) ruleListDao.load(RuleListDao.TRANSFORM_RULELISTVO, ruleListCode))
                .filter(ruleListVO -> !ReefDbBeans.isLocalStatus(ruleListVO.getStatusCd())) // REMOTE RULE LIST ONLY !!!
                .collect(Collectors.toList());

        if (ruleListToSend.isEmpty()) return;

        // send to server
        List<RuleListVO> savedRuleLists = synchroRestClientService.saveRuleLists(authenticationInfo, ruleListToSend);

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Saving rule lists [%s] from server response", Joiner.on(',').join(savedRuleLists.stream().map(RuleListVO::getRuleListCd).collect(Collectors.toList()))));
        }

        // save locally (update)
        for (RuleListVO savedRuleList : savedRuleLists) {
            ruleListDao.save(savedRuleList);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteRuleLists(AuthenticationInfo authenticationInfo, List<RuleListDTO> ruleLists) {
        if (CollectionUtils.isEmpty(ruleLists)) return;

        // delete locally
        ruleLists.forEach(ruleList -> ruleListDao.remove(ruleList.getCode()));

        // delete remote rule list
        deleteRuleListsOnServer(authenticationInfo, ruleLists);
    }

    private void deleteRuleListsOnServer(AuthenticationInfo authenticationInfo, List<RuleListDTO> ruleLists) {

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Delete rule lists [%s] from server", Joiner.on(',').join(ruleLists.stream().map(RuleListDTO::getCode).collect(Collectors.toList()))));
        }

        // Check if each ruleLists exists on server (it happens that a deleted ruleList had not been saved before)
        Set<RuleListDTO> ruleListsToDelete = ruleLists.stream()
                .filter(ruleList -> {
                    if (ReefDbBeans.isLocalStatus(ruleList.getStatus())) return false; // filter only remote rule lists
                    if (synchroRestClientService.getRuleListByCode(authenticationInfo, ruleList.getCode()) != null) {
                        return true;
                    } else {
                        if (LOG.isDebugEnabled()) {
                            LOG.debug(String.format("Rule List [%s] doesn't exists on server, skip it", ruleList));
                        }
                        return false;
                    }
                })
                .collect(Collectors.toSet());

        if (CollectionUtils.isNotEmpty(ruleListsToDelete)) {

            List<String> ruleListCodesToDelete = ruleListsToDelete.stream().map(RuleListDTO::getCode).collect(Collectors.toList());

            // remote delete
            synchroRestClientService.deleteRuleLists(authenticationInfo, ruleListCodesToDelete);

            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Rule lists [%s] deleted from server", Joiner.on(',').join(ruleListCodesToDelete)));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListDTO duplicateRuleList(RuleListDTO ruleList, String newCode, boolean isDuplicateRulesEnabled) {

        RuleListDTO duplicatedRuleList = ReefDbBeans.clone(ruleList);

        duplicatedRuleList.setCode(newCode);
        duplicatedRuleList.setNewCode(true);
        duplicatedRuleList.setStatus(ruleList.getStatus());
        duplicatedRuleList.setDepartments(ReefDbBeans.clone(ruleList.getDepartments()));
        duplicatedRuleList.setPrograms(ReefDbBeans.clone(ruleList.getPrograms()));
        duplicatedRuleList.setErrors(new ArrayList<>());

        duplicatedRuleList.setControlRules(new ArrayList<>());
        if (isDuplicateRulesEnabled && CollectionUtils.isNotEmpty(ruleList.getControlRules())) {
            MutableInt suffixIndex = getUniqueMutableIndex();
            for (ControlRuleDTO controlRule : ruleList.getControlRules()) {
                ControlRuleDTO duplicatedControlRule = duplicateControlRule(controlRule, duplicatedRuleList);
                // clone preconditions if exists
                if (!controlRule.isPreconditionsEmpty()) {
                    duplicatePreconditions(controlRule, duplicatedControlRule, suffixIndex);
                }
                duplicatedRuleList.addControlRules(duplicatedControlRule);
            }
        }

        return duplicatedRuleList;
    }

    private ControlRuleDTO duplicateControlRule(ControlRuleDTO controlRule, RuleListDTO ruleList) {

        ControlRuleDTO duplicatedControlRule = ReefDbBeans.clone(controlRule);

        duplicatedControlRule.setCode(ruleList != null ? getNextRuleCode(ruleList) : null);
        duplicatedControlRule.setNewCode(true);
        duplicatedControlRule.setErrors(null);
        duplicatedControlRule.setPreconditions(null);
        duplicatedControlRule.setRulePmfms(null);

        // clone pmfm list and reset RULE_PMFM ids
        for (RulePmfmDTO rulePmfm : controlRule.getRulePmfms()) {
            RulePmfmDTO duplicatedRulePmfm = ReefDbBeans.clone(rulePmfm);
            duplicatedRulePmfm.setId(null);
            duplicatedControlRule.addRulePmfms(duplicatedRulePmfm);
        }

        return duplicatedControlRule;
    }

    private void duplicatePreconditions(ControlRuleDTO controlRule, ControlRuleDTO duplicatedControlRule, MutableInt suffixIndex) {

        for (PreconditionRuleDTO precondition : controlRule.getPreconditions()) {
            PreconditionRuleDTO duplicatedPrecondition = ReefDbBeans.clone(precondition);
            duplicatedPrecondition.setId(null);
            duplicatedPrecondition.setRule(duplicatedControlRule);
            duplicatedPrecondition.setName(duplicatedControlRule.getCode());
            duplicatedPrecondition.setBaseRule(duplicateControlRule(precondition.getBaseRule(), null));
            duplicatedPrecondition.getBaseRule().setCode(getNextRuleCode(duplicatedControlRule.getCode(), suffixIndex));
            duplicatedPrecondition.setUsedRule(duplicateControlRule(precondition.getUsedRule(), null));
            duplicatedPrecondition.getUsedRule().setCode(getNextRuleCode(duplicatedControlRule.getCode(), suffixIndex));
            duplicatedControlRule.addPreconditions(duplicatedPrecondition);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ControlRuleDTO newControlRule(RuleListDTO ruleList) {

        Assert.notNull(ruleList);
        Assert.notBlank(ruleList.getCode());

        ControlRuleDTO rule = ReefDbBeanFactory.newControlRuleDTO();

        rule.setCode(getNextRuleCode(ruleList));
        rule.setNewCode(true);
        rule.setActive(true);
        rule.setBlocking(false);

        return rule;
    }

    @Override
    public boolean ruleCodeExists(String ruleCode) {
        Assert.notBlank(ruleCode);

        return ruleDao.ruleExists(ruleCode);
    }

    @Override
    public List<ControlRuleDTO> getPreconditionedControlRulesForSurvey(SurveyDTO survey) {
        Assert.notNull(survey);
        Assert.notNull(survey.getDate());
        Assert.notNull(survey.getProgram());
        Assert.notNull(survey.getRecorderDepartment());

        List<ControlRuleDTO> preconditionedRules = ruleDao.findActivePreconditionedRules(
                Dates.convertToDate(survey.getDate(), configuration.getDbTimezone()),
                survey.getProgram().getCode(),
                survey.getRecorderDepartment().getId());
        buildPmfmInformation(preconditionedRules);
        return preconditionedRules;
    }

    @Override
    public List<ControlRuleDTO> getGroupedControlRulesForSurvey(SurveyDTO survey) {
        Assert.notNull(survey);
        Assert.notNull(survey.getDate());
        Assert.notNull(survey.getProgram());
        Assert.notNull(survey.getRecorderDepartment());

        List<ControlRuleDTO> preconditionedRules = ruleDao.findActiveGroupedRules(
                Dates.convertToDate(survey.getDate(), configuration.getDbTimezone()),
                survey.getProgram().getCode(), survey.getRecorderDepartment().getId());
        buildPmfmInformation(preconditionedRules);
        return preconditionedRules;
    }

    @Override
    public List<ControlRuleDTO> getPreconditionedControlRulesForProgramCodes(List<String> programCodes) {
        Assert.notEmpty(programCodes);

        List<ControlRuleDTO> preconditionedRules = ruleDao.findActivePreconditionedRules(programCodes);
        buildPmfmInformation(preconditionedRules);
        return preconditionedRules;

    }

    @Override
    public void buildAllowedValuesByPmfmId(MeasurementDTO measurement,
                                           Collection<Integer> targetPmfmIds,
                                           Collection<PreconditionRuleDTO> preconditions,
                                           AllowedQualitativeValuesMap allowedQualitativeValuesMap) {

        int sourcePmfmId = measurement.getPmfm().getId();

        for (PreconditionRuleDTO precondition : preconditions) {

            ControlRuleDTO sourceRule;
            ControlRuleDTO targetRule;

            // determine base and used rule
            if (!precondition.isBidirectional() || Objects.equals(precondition.getBaseRule().getRulePmfms(0).getPmfm().getId(), sourcePmfmId)) {
                // simple case for mono directional rule or if the base rule pmfm is the actual pmfm
                sourceRule = precondition.getBaseRule();
                targetRule = precondition.getUsedRule();
            } else {
                // source and target are switched
                sourceRule = precondition.getUsedRule();
                targetRule = precondition.getBaseRule();
            }

            // find now the target column, and stop here if not found
            int targetPmfmId = targetRule.getRulePmfms(0).getPmfm().getId();
            if (!targetPmfmIds.contains(targetPmfmId)) continue; // if the target pmfm is not part of target pmfms, continue to next precondition

            // valid source && target rules
            if (ReefDbBeans.isQualitativeControlRule(targetRule)) {

                if (ReefDbBeans.isQualitativeControlRule(sourceRule)) {
                    BaseReferentialDTO sourceValue = measurement.getQualitativeValue();

                    // control the value on qualitative source rule
                    if (controlRuleService.controlUniqueObject(sourceRule, sourceValue)) {

                        // add allowed values from the target rule
                        allowedQualitativeValuesMap.addTargetValues(targetPmfmId, sourcePmfmId, sourceValue.getId(), getAllowedValueIds(targetRule));

                    } else {

                        // if this rule is invalid, try to reset allowed values if possible
                        AllowedQualitativeValuesMap.AllowedValues allowedValues =
                                allowedQualitativeValuesMap.getAllowedValues(targetPmfmId, sourcePmfmId, sourceValue != null ? sourceValue.getId() : null);
                        if (allowedValues != null && sourceValue == null) {
                            allowedValues.setAllAllowed();
                        }

                    }

                } else if (ReefDbBeans.isNumericalControlRule(sourceRule)) {
                    BigDecimal sourceValue = measurement.getNumericalValue();

                    // control the value on min max source rule
                    if (controlRuleService.controlUniqueObject(sourceRule, sourceValue)) {

                        // add allowed values from the target rule
                        allowedQualitativeValuesMap.addTargetValues(targetPmfmId, sourcePmfmId, sourceValue, getAllowedValueIds(targetRule));

                    } else {

                        // if this rule is invalid, try to reset allowed values if possible
                        AllowedQualitativeValuesMap.AllowedValues allowedValues =
                                allowedQualitativeValuesMap.getAllowedValues(targetPmfmId, sourcePmfmId, sourceValue);
                        if (allowedValues != null && sourceValue == null) {
                            allowedValues.setAllAllowed();
                        }

                    }


                } else {

                    // invalid source rule
                    LOG.warn(String.format("invalid source rule %s", sourceRule.getCode()));
                }

            } else if (ReefDbBeans.isNumericalControlRule(targetRule)) {

                // not implemented
//                LOG.warn("not yet implemented");

            } else {

                // invalid target rule
                LOG.warn(String.format("invalid target rule %s", targetRule.getCode()));
            }

        }
    }

    private Set<Integer> getAllowedValueIds(ControlRuleDTO rule) {
        return ReefDbBeans.getIntegerSetFromString(rule.getAllowedValues(), configuration.getValueSeparator());
    }

    @Override
    public Multimap<QualitativeValueDTO, QualitativeValueDTO> buildQualitativeValueMapFromPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        Multimap<QualitativeValueDTO, QualitativeValueDTO> multimap = HashMultimap.create();
        for (PreconditionRuleDTO precondition : preconditions) {

            List<QualitativeValueDTO> baseValues = referentialService.getQualitativeValues(
                    ReefDbBeans.getIntegerSetFromString(precondition.getBaseRule().getAllowedValues(), configuration.getValueSeparator()));
            for (QualitativeValueDTO baseValue : baseValues) {
                multimap.putAll(
                        baseValue,
                        referentialService.getQualitativeValues(
                                ReefDbBeans.getIntegerSetFromString(precondition.getUsedRule().getAllowedValues(), configuration.getValueSeparator()))
                );
            }
        }
        return multimap;
    }

    @Override
    public void buildPreconditionsFromQualitativeValueMap(ControlRuleDTO rule, Multimap<QualitativeValueDTO, QualitativeValueDTO> multimap) {
        Map<String, PreconditionRuleDTO> existingRulePreconditionsByBaseValues = getRulePreconditionsByBaseValues(rule);

        MutableInt suffixIndex = getUniqueMutableIndex();

        for (QualitativeValueDTO baseValue : multimap.keySet()) {
            PreconditionRuleDTO precondition = existingRulePreconditionsByBaseValues.remove(baseValue.getId().toString());
            if (precondition == null) {
                precondition = createPreconditionRule(rule);
            }
            precondition.setActive(rule.isActive());
            precondition.setName(rule.getCode());

            ControlRuleDTO baseRule = precondition.getBaseRule();
            if (baseRule == null) {
                baseRule = createQualitativeControlRule(rule, suffixIndex);
                baseRule.addRulePmfms(cloneRulePmfm(rule.getRulePmfms(0)));
                baseRule.setAllowedValues(baseValue.getId().toString());
                precondition.setBaseRule(baseRule);
            }

            ControlRuleDTO usedRule = precondition.getUsedRule();
            if (usedRule == null) {
                usedRule = createQualitativeControlRule(rule, suffixIndex);
                usedRule.addRulePmfms(cloneRulePmfm(rule.getRulePmfms(1)));
                precondition.setUsedRule(usedRule);
            }
            usedRule.setAllowedValues(ReefDbBeans.joinIds(multimap.get(baseValue), configuration.getValueSeparator()));

        }

        // remove unused preconditions
        rule.removeAllPreconditions(existingRulePreconditionsByBaseValues.values());

    }

    @Override
    public List<NumericPreconditionDTO> buildNumericPreconditionListFromPreconditions(Collection<PreconditionRuleDTO> preconditions) {
        List<NumericPreconditionDTO> list = new ArrayList<>();
        for (PreconditionRuleDTO precondition : preconditions) {
            NumericPreconditionDTO numericPrecondition = ReefDbBeanFactory.newNumericPreconditionDTO();
            Set<Integer> qvIds = ReefDbBeans.getIntegerSetFromString(precondition.getBaseRule().getAllowedValues(), configuration.getValueSeparator());
            if (qvIds.isEmpty()) continue;
            numericPrecondition.setQualitativeValue(referentialService.getQualitativeValue(qvIds.iterator().next()));
            numericPrecondition.setMin((Double) precondition.getUsedRule().getMin());
            numericPrecondition.setMax((Double) precondition.getUsedRule().getMax());
            list.add(numericPrecondition);
        }

        return list;
    }

    @Override
    public void buildPreconditionsFromNumericPreconditionList(ControlRuleDTO rule, List<NumericPreconditionDTO> list) {
        Map<String, PreconditionRuleDTO> existingRulePreconditionsByBaseValues = getRulePreconditionsByBaseValues(rule);

        MutableInt suffixIndex = getUniqueMutableIndex();

        for (NumericPreconditionDTO baseValue : list) {
            PreconditionRuleDTO precondition = existingRulePreconditionsByBaseValues.remove(baseValue.getQualitativeValue().getId().toString());
            if (precondition == null) {
                precondition = createPreconditionRule(rule);
            }
            precondition.setActive(rule.isActive());
            precondition.setName(rule.getCode());

            ControlRuleDTO baseRule = precondition.getBaseRule();
            if (baseRule == null) {
                baseRule = createQualitativeControlRule(rule, suffixIndex);
                baseRule.addRulePmfms(cloneRulePmfm(rule.getRulePmfms(0)));
                baseRule.setAllowedValues(baseValue.getQualitativeValue().getId().toString());
                precondition.setBaseRule(baseRule);
            }

            ControlRuleDTO usedRule = precondition.getUsedRule();
            if (usedRule == null) {
                usedRule = createNumericalControlRule(rule, suffixIndex);
                usedRule.addRulePmfms(cloneRulePmfm(rule.getRulePmfms(1)));
                precondition.setUsedRule(usedRule);
            }
            usedRule.setMin(baseValue.getMin());
            usedRule.setMax(baseValue.getMax());
        }

        // remove unused preconditions
        rule.removeAllPreconditions(existingRulePreconditionsByBaseValues.values());

    }

    @Override
    public void buildNotEmptyConditionalGroupsFromGroupedRule(ControlRuleDTO groupedRule) {

        // if no pmfm, just rest
        if (groupedRule.isRulePmfmsEmpty() || !ReefDbBeans.isGroupedRule(groupedRule)) {
            groupedRule.setGroups(null);
            return;
        }

        RuleGroupDTO taxonGroupRuleGroup = null;
        RuleGroupDTO taxonRuleGroup = null;
        RuleGroupDTO pmfmRuleGroup = null;
        MutableInt suffixIndex = getUniqueMutableIndex();

        // try find the 3 rules
        for (RuleGroupDTO group: groupedRule.getGroups()) {
            if (group.getRule() == null) continue;
            if (ControlFunctionValues.NOT_EMPTY.equals(group.getRule().getFunction())
                    && ControlElementValues.MEASUREMENT.equals(group.getRule().getControlElement())
                    && ControlFeatureMeasurementValues.TAXON_GROUP.equals(group.getRule().getControlFeature())) {
                taxonGroupRuleGroup = group;
            }
            if (ControlFunctionValues.NOT_EMPTY.equals(group.getRule().getFunction())
                    && ControlElementValues.MEASUREMENT.equals(group.getRule().getControlElement())
                    && ControlFeatureMeasurementValues.TAXON.equals(group.getRule().getControlFeature())) {
                taxonRuleGroup = group;
            }
            if (ControlFunctionValues.NOT_EMPTY.equals(group.getRule().getFunction())
                    && ControlElementValues.MEASUREMENT.equals(group.getRule().getControlElement())
                    && ControlFeatureMeasurementValues.PMFM.equals(group.getRule().getControlFeature())) {
                pmfmRuleGroup = group;
            }
        }

        if (taxonGroupRuleGroup == null) {
            taxonGroupRuleGroup = ReefDbBeanFactory.newRuleGroupDTO();
            taxonGroupRuleGroup.setIsOr(true);
            groupedRule.addGroups(taxonGroupRuleGroup);

            ControlRuleDTO taxonGroupRule = ReefDbBeanFactory.newControlRuleDTO();
            taxonGroupRule.setCode(getNextRuleCode(groupedRule.getCode(), suffixIndex));
            taxonGroupRule.setFunction(ControlFunctionValues.NOT_EMPTY.toFunctionDTO());
            taxonGroupRule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            taxonGroupRule.setControlFeature(ControlFeatureMeasurementValues.TAXON_GROUP.toControlFeatureDTO());
            taxonGroupRule.setActive(false); // must be false
            taxonGroupRuleGroup.setRule(taxonGroupRule);
        }
        taxonGroupRuleGroup.setName(groupedRule.getCode());
        taxonGroupRuleGroup.setActive(groupedRule.isActive());
        taxonGroupRuleGroup.getRule().setDescription(groupedRule.getDescription());
        taxonGroupRuleGroup.getRule().setMessage(groupedRule.getMessage());

        if (taxonRuleGroup == null) {
            taxonRuleGroup = ReefDbBeanFactory.newRuleGroupDTO();
            taxonRuleGroup.setIsOr(true);
            groupedRule.addGroups(taxonRuleGroup);

            ControlRuleDTO taxonRule = ReefDbBeanFactory.newControlRuleDTO();
            taxonRule.setCode(getNextRuleCode(groupedRule.getCode(), suffixIndex));
            taxonRule.setFunction(ControlFunctionValues.NOT_EMPTY.toFunctionDTO());
            taxonRule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            taxonRule.setControlFeature(ControlFeatureMeasurementValues.TAXON.toControlFeatureDTO());
            taxonRule.setActive(false); // must be false
            taxonRuleGroup.setRule(taxonRule);
        }
        taxonRuleGroup.setName(groupedRule.getCode());
        taxonRuleGroup.setActive(groupedRule.isActive());
        taxonRuleGroup.getRule().setDescription(groupedRule.getDescription());
        taxonRuleGroup.getRule().setMessage(groupedRule.getMessage());

        if (pmfmRuleGroup == null) {
            pmfmRuleGroup = ReefDbBeanFactory.newRuleGroupDTO();
            pmfmRuleGroup.setIsOr(true);
            groupedRule.addGroups(pmfmRuleGroup);

            ControlRuleDTO pmfmRule = ReefDbBeanFactory.newControlRuleDTO();
            pmfmRule.setCode(getNextRuleCode(groupedRule.getCode(), suffixIndex));
            pmfmRule.setFunction(ControlFunctionValues.NOT_EMPTY.toFunctionDTO());
            pmfmRule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            pmfmRule.setControlFeature(ControlFeatureMeasurementValues.PMFM.toControlFeatureDTO());
            pmfmRule.setActive(false); // must be false
            pmfmRuleGroup.setRule(pmfmRule);
        }
        pmfmRuleGroup.setName(groupedRule.getCode());
        pmfmRuleGroup.setActive(groupedRule.isActive());
        pmfmRuleGroup.getRule().setRulePmfms(new ArrayList<>(groupedRule.getRulePmfms()));
        pmfmRuleGroup.getRule().setDescription(groupedRule.getDescription());
        pmfmRuleGroup.getRule().setMessage(groupedRule.getMessage());

        // check
        Assert.equals(groupedRule.sizeGroups(), 3, "the groups count must be 3");
    }

    private void buildPmfmInformation(List<ControlRuleDTO> controlRules) {
        if (controlRules != null) {
            for (ControlRuleDTO controlRule : controlRules) {
                // Affect all pmfmId with the real pmfm Id from the referential: Allow better handling in UI
                for (RulePmfmDTO rulePmfm : controlRule.getRulePmfms()) {
                    rulePmfm.getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(rulePmfm.getPmfm()));
                }
                for (PreconditionRuleDTO precondition : controlRule.getPreconditions()) {
                    precondition.getBaseRule().getRulePmfms(0).getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(precondition.getBaseRule().getRulePmfms(0).getPmfm()));
                    precondition.getUsedRule().getRulePmfms(0).getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(precondition.getUsedRule().getRulePmfms(0).getPmfm()));
                }
                for (RuleGroupDTO group: controlRule.getGroups()) {
                    for (RulePmfmDTO rulePmfm : group.getRule().getRulePmfms()) {
                        rulePmfm.getPmfm().setId(referentialService.getUniquePmfmIdFromPmfm(rulePmfm.getPmfm()));
                    }
                }
            }
        }
    }

    /**
     * rule code = <code_liste_contrôles>_<numéro_règle_incrémenté_automatiquement>
     *
     * @param ruleList the rule list
     * @return the next rule code
     */
    private String getNextRuleCode(RuleListDTO ruleList) {
        Assert.notNull(ruleList);
        int maxSuffix = 0;

        if (CollectionUtils.isNotEmpty(ruleList.getControlRules())) {

            for (ControlRuleDTO r : ruleList.getControlRules()) {
                String ruleCode = r.getCode();

                if (StringUtils.isNotBlank(ruleCode)
                        && ruleCode.startsWith(ruleList.getCode())) {
                    int lastIndex = ruleCode.lastIndexOf('_');
                    if (lastIndex == -1) {
                        lastIndex = ruleCode.lastIndexOf('-');
                    }

                    if (lastIndex != -1 && lastIndex != ruleCode.length() - 1) {
                        String suffix = ruleCode.substring(lastIndex + 1);
                        try {
                            maxSuffix = Math.max(maxSuffix, Integer.parseInt(suffix));
                        } catch (NumberFormatException e) {
                            // skip this rule code
                        }
                    }
                }
            }
        }

        // return (max + 1)
        return String.format("%s_%d", ruleList.getCode(), maxSuffix + 1);
    }

    @Override
    public MutableInt getUniqueMutableIndex() {
        return new MutableInt(System.currentTimeMillis() / 1000);
    }

    /**
     * Compute unique rule code with target name and integer suffix
     *
     * @param targetName  the target name
     * @param suffixIndex the integer suffix
     * @return a unique and not existing code
     */
    @Override
    public String getNextRuleCode(String targetName, MutableInt suffixIndex) {
        String ruleCode;
        do {
            // trim to 40 characters
            String suffix = Integer.toString(suffixIndex.getAndIncrement());
            int suffixLength = suffix.length() + 1;
            int totalLength = targetName.length() + suffixLength;
            if (totalLength > 40) {
                targetName = targetName.substring(0, 40 - suffixLength);
            }
            ruleCode = String.format("%s_%s", targetName, suffix);
        } while (ruleCodeExists(ruleCode));
        return ruleCode;
    }

    private Map<String, PreconditionRuleDTO> getRulePreconditionsByBaseValues(ControlRuleDTO rule) {
        return ReefDbBeans.mapByProperty(rule.getPreconditions(), PreconditionRuleDTO.PROPERTY_BASE_RULE + "." + ControlRuleDTO.PROPERTY_ALLOWED_VALUES);
    }

    private PreconditionRuleDTO createPreconditionRule(ControlRuleDTO parentRule) {
        PreconditionRuleDTO precondition = ReefDbBeanFactory.newPreconditionRuleDTO();
        precondition.setRule(parentRule);
        precondition.setBidirectional(true); // by default
        parentRule.addPreconditions(precondition);
        return precondition;
    }

    private ControlRuleDTO createQualitativeControlRule(ControlRuleDTO parentRule, MutableInt suffixIndex) {
        ControlRuleDTO rule = ReefDbBeanFactory.newControlRuleDTO();
        rule.setCode(getNextRuleCode(parentRule.getCode(), suffixIndex));
        rule.setFunction(ControlFunctionValues.IS_AMONG.toFunctionDTO());
        rule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        rule.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        rule.setActive(false); // must be false
        return rule;
    }

    private ControlRuleDTO createNumericalControlRule(ControlRuleDTO parentRule, MutableInt suffixIndex) {
        ControlRuleDTO rule = ReefDbBeanFactory.newControlRuleDTO();
        rule.setCode(getNextRuleCode(parentRule.getCode(), suffixIndex));
        rule.setFunction(ControlFunctionValues.MIN_MAX.toFunctionDTO());
        rule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        rule.setControlFeature(ControlFeatureMeasurementValues.NUMERICAL_VALUE.toControlFeatureDTO());
        rule.setActive(false); // must be false
        return rule;
    }

    private RulePmfmDTO cloneRulePmfm(RulePmfmDTO rulePmfm) {
        RulePmfmDTO result = ReefDbBeanFactory.newRulePmfmDTO();
        result.setPmfm(ReefDbBeans.clone(rulePmfm.getPmfm()));
        return result;
    }

}
