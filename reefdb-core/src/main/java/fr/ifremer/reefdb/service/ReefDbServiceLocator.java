package fr.ifremer.reefdb.service;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.ClientServiceLocator;
import fr.ifremer.reefdb.decorator.DecoratorService;
import fr.ifremer.reefdb.service.administration.campaign.CampaignService;
import fr.ifremer.reefdb.service.administration.context.ContextService;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.service.administration.user.UserService;
import fr.ifremer.reefdb.service.extraction.ExtractionPerformService;
import fr.ifremer.reefdb.service.extraction.ExtractionService;
import fr.ifremer.reefdb.service.observation.ObservationService;
import fr.ifremer.reefdb.service.persistence.PersistenceService;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import fr.ifremer.reefdb.service.rulescontrol.ControlRuleService;
import fr.ifremer.reefdb.service.rulescontrol.RuleListService;
import fr.ifremer.reefdb.service.synchro.SynchroClientService;
import fr.ifremer.reefdb.service.system.SystemService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>ReefDbServiceLocator class.</p>
 *
 */
@Configuration
public class ReefDbServiceLocator extends ClientServiceLocator {

    static {
        INSTANCE = new ReefDbServiceLocator();
        initReefDbDefault();
    }

    private static final ReefDbServiceLocator INSTANCE;

    /**
     * <p>initReefDbDefault.</p>
     */
    public static void initReefDbDefault() {
        INSTANCE.init("reefdbBeanRefFactory.xml", "reefdbBeanRefFactory");
        ClientServiceLocator.setInstance(INSTANCE);
    }

    /**
     * <p>instance.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.ReefDbServiceLocator} object.
     */
    public static ReefDbServiceLocator instance() {
        return INSTANCE;
    }

    /**
     * <p>getDataContext.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.ReefDbDataContext} object.
     */
    @Bean
    public ReefDbDataContext getDataContext() {
        return ReefDbDataContext.instance();
    }

    /**
     * <p>getPersistenceService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.persistence.PersistenceService} object.
     */
    @Override
    public PersistenceService getPersistenceService() {
        return getService("reefdbPersistenceService", PersistenceService.class);
    }

    /**
     * <p>getSystemService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.system.SystemService} object.
     */
    public SystemService getSystemService() {
        return getService("reefdbSystemService", SystemService.class);
    }

    /**
     * <p>getObservationService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.observation.ObservationService} object.
     */
    public ObservationService getObservationService() {
        return getService("reefdbSurveyService", ObservationService.class);
    }

    /**
     * <p>getExtractionService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.extraction.ExtractionService} object.
     */
    public ExtractionService getExtractionService() {
        return getService("reefdbExtractionService", ExtractionService.class);
    }

    public ExtractionPerformService getExtractionPerformService() {
        return getService("reefdbExtractionPerformService", ExtractionPerformService.class);
    }

    /**
     * <p>getReferentialService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.referential.ReferentialService} object.
     */
    public ReferentialService getReferentialService() {
        return getService("reefdbReferentialService", ReferentialService.class);
    }

    /**
     * <p>getProgramStrategyService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.administration.program.ProgramStrategyService} object.
     */
    public ProgramStrategyService getProgramStrategyService() {
        return getService("reefdbProgramStrategyService", ProgramStrategyService.class);
    }

    public CampaignService getCampaignService() {
        return getService("reefdbCampaignService", CampaignService.class);
    }

    /**
     * <p>getContextService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.administration.context.ContextService} object.
     */
    public ContextService getContextService() {
        return getService("reefdbContextService", ContextService.class);
    }

    /**
     * <p>getUserService.</p>
     *
     * @return a {@link fr.ifremer.reefdb.service.administration.user.UserService} object.
     */
    public UserService getUserService() {
        return getService("reefDbUserService", UserService.class);
    }

    /**
     * <p>getRuleListService.</p>
     *
     * @return a {@link RuleListService} object.
     */
    public RuleListService getRuleListService() {
        return getService("reefDbRuleListService", RuleListService.class);
    }

    public ControlRuleService getControlRuleService() {
        return getService("reefDbControlRuleService", ControlRuleService.class);
    }

    @Override
    public SynchroClientService getSynchroClientService() {
        return getService("reefdbSynchroClientService", SynchroClientService.class);
    }

    @Override
    public DecoratorService getDecoratorService() {
        return (DecoratorService) super.getDecoratorService();
    }

}
