package fr.ifremer.reefdb.service.synchro;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.common.synchro.config.SynchroConfiguration;
import fr.ifremer.common.synchro.service.RejectedRow;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.dao.referential.ReferentialJdbcDao;
import fr.ifremer.quadrige3.core.dao.referential.ReferentialJdbcDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.decorator.Decorator;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.quadrige3.core.service.decorator.DecoratorService;
import fr.ifremer.quadrige3.core.vo.data.survey.LightSurveyVO;
import fr.ifremer.quadrige3.synchro.meta.administration.MoratoriumSynchroTables;
import fr.ifremer.quadrige3.synchro.meta.administration.ProgramStrategySynchroTables;
import fr.ifremer.quadrige3.synchro.meta.data.DataSynchroTables;
import fr.ifremer.quadrige3.synchro.meta.referential.ReferentialSynchroTables;
import fr.ifremer.quadrige3.synchro.meta.system.RuleSynchroTables;
import fr.ifremer.quadrige3.synchro.service.client.SynchroClientInternalService;
import fr.ifremer.quadrige3.synchro.service.client.vo.SynchroOperationType;
import fr.ifremer.quadrige3.synchro.vo.SynchroChangesVO;
import fr.ifremer.quadrige3.synchro.vo.SynchroImportContextVO;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroRowDTO;
import fr.ifremer.reefdb.dto.system.synchronization.SynchroTableDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.sql.SQLException;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * Created by blavenie on 03/09/15.
 */
@Service("reefdbSynchroClientService")
public class SynchroClientServiceImpl
        extends fr.ifremer.quadrige3.synchro.service.client.SynchroClientServiceImpl
        implements SynchroClientService {

    private static final Log log = LogFactory.getLog(SynchroClientServiceImpl.class);

    @Resource(name = "reefDbSurveyDao")
    private ReefDbSurveyDao surveyDao;

    @Resource(name = "referentialJdbcDao")
    private ReferentialJdbcDao targetReferentialJdbcDao;

    @Resource(name = "synchroClientService")
    private SynchroClientInternalService synchroClientInternalService;

    /**
     * <p>Constructor for SynchroClientServiceImpl.</p>
     *
     * @param config a {@link fr.ifremer.quadrige3.core.config.QuadrigeConfiguration} object.
     * @param synchroConfig a {@link fr.ifremer.common.synchro.config.SynchroConfiguration} object.
     * @param decoratorService a {@link fr.ifremer.reefdb.decorator.DecoratorService} object.
     */
    @Autowired
    public SynchroClientServiceImpl(QuadrigeConfiguration config, SynchroConfiguration synchroConfig, fr.ifremer.reefdb.decorator.DecoratorService decoratorService) {
        super(config, synchroConfig, decoratorService);
    }

    /**
     * {@inheritDoc}
     *
     * Compute change log from a file to import
     */
    @Override
    public SynchroChangesDTO getImportFileChangesAsDTO(
            int userId,
            File dbZipFile,
            SynchroImportContextVO importContext,
            ProgressionCoreModel progressionModel,
            int progressionModelMaxCount) {

        SynchroChangesDTO insertUpdateChanges;
        SynchroChangesDTO referentialDeleteChanges;

        // Import INSERT and UPDATE changes
        {
            SynchroChangesVO changes = synchroClientInternalService.getImportFileInsertAndUpdateChangesTransactional(userId,
                    dbZipFile,
                    importContext,
                    progressionModel,
                    progressionModelMaxCount,
                    true /* Keep temp directory */);

            // Read the used connection properties, to retrieve the db directory
            String tempDbJdbcUrl = Daos.getUrl(changes.getConnectionProperties());
            File tempDirectory = new File(Daos.getDbDirectoryFromJdbcUrl(tempDbJdbcUrl));

            insertUpdateChanges = toSynchroChangesDTO(changes.getConnectionProperties(), dbZipFile, changes);

            // Clean directory at end
            FileUtils.deleteQuietly(tempDirectory.getParentFile());
        }

        // Import DELETE changes
        {
            SynchroChangesVO changes = synchroClientInternalService.getImportFileReferentialDeleteChangesTransactional(userId,
                    dbZipFile,
                    importContext,
                    progressionModel,
                    progressionModelMaxCount,
                    true /* Keep temp directory */);

            // Read the used connection properties, to retrieve the db directory
            String tempDbJdbcUrl = Daos.getUrl(changes.getConnectionProperties());
            File tempDirectory = new File(Daos.getDbDirectoryFromJdbcUrl(tempDbJdbcUrl));

            referentialDeleteChanges = toSynchroChangesDTO(changes.getConnectionProperties(), dbZipFile, changes);

            // Clean directory at end
            FileUtils.deleteQuietly(tempDirectory.getParentFile());
        }

        // Merge the changes
        return mergeChanges(insertUpdateChanges, referentialDeleteChanges);
    }

    private SynchroChangesDTO mergeChanges(SynchroChangesDTO changes, SynchroChangesDTO changesToAdd) {
        Assert.notNull(changes);
        Assert.notNull(changesToAdd);

        Map<String, SynchroTableDTO> tablesByName = ReefDbBeans.mapByProperty(changes.getTables(), SynchroTableDTO.PROPERTY_NAME);

        for (SynchroTableDTO tableToAdd: changesToAdd.getTables()) {
            SynchroTableDTO table = tablesByName.get(tableToAdd.getName());
            if (table == null) {
                // add the table
                changes.addTables(tableToAdd);
            } else {
                // add rows
                table.addAllRows(tableToAdd.getRows());
            }
        }

        return changes;
    }

    /** {@inheritDoc} */
    @Override
    public Multimap<String, String> toPkToIncludesMap(SynchroChangesDTO changes) {
        Multimap<String, String> result = ArrayListMultimap.create();

        if (changes != null && changes.getTables() != null) {
            for (SynchroTableDTO table : changes.getTables()) {
                String tableName = table.getName();

                if (!table.isRowsEmpty()) {
                    for (SynchroRowDTO row : table.getRows()) {
                        String strategyStr = row.getStrategy();
                        if (StringUtils.isNotBlank(strategyStr)) {
                            RejectedRow.ResolveStrategy strategy = RejectedRow.ResolveStrategy.valueOf(strategyStr);
                            if (strategy == RejectedRow.ResolveStrategy.UPDATE
                                    || strategy == RejectedRow.ResolveStrategy.DUPLICATE) {
                                result.put(tableName, row.getPkStr());
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean hasProgramOrRulesChanges(SynchroChangesDTO synchroChanges) {

        if (synchroChanges == null || synchroChanges.isTablesEmpty()) {
            return false;
        }

        for (SynchroTableDTO table : synchroChanges.getTables()) {
            if (ProgramStrategySynchroTables.tableNames().contains(table.getName().toUpperCase())
                    || MoratoriumSynchroTables.tableNames().contains(table.getName().toUpperCase())
                    || RuleSynchroTables.tableNames().contains(table.getName().toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    /** {@inheritDoc} */
    @Override
    public SynchroChangesDTO getReferentialSynchroChangesToApplyFromFile(SynchroChangesDTO synchroChanges) {
        if (synchroChanges == null || synchroChanges.isTablesEmpty()) {
            return synchroChanges;
        }

        // get referential tables only (remove program/strategy tables)
        Set<String> referentialTablesOnly = new LinkedHashSet<>(ReferentialSynchroTables.tableNames());
        referentialTablesOnly.removeAll(ProgramStrategySynchroTables.tableNames());
        referentialTablesOnly.removeAll(MoratoriumSynchroTables.tableNames());

        SynchroChangesDTO filteredSynchroChanges = ReefDbBeans.clone(synchroChanges);
        filteredSynchroChanges.setTables(new ArrayList<>());

        // iterate over tables changes
        for (SynchroTableDTO table : synchroChanges.getTables()) {
            String tableName = table.getName().toUpperCase();
            boolean isProgramStrategy = ProgramStrategySynchroTables.tableNames().contains(tableName) || MoratoriumSynchroTables.tableNames().contains(tableName);
            boolean isRule = RuleSynchroTables.tableNames().contains(tableName);
            boolean isReferential = isProgramStrategy || isRule || referentialTablesOnly.contains(tableName);

            SynchroTableDTO filteredSynchroTable = ReefDbBeans.clone(table);
            filteredSynchroTable.setRows(new ArrayList<>());
            if (!table.isRowsEmpty()) {
                // iterate over rows changes
                for (SynchroRowDTO row : table.getRows()) {

                    boolean addRow = false;
                    switch (SynchroOperationType.valueOf(row.getOperationType().toUpperCase())) {

                        case INSERT:
                            // INSERT operations always to do
                            addRow = isReferential;
                            break;
                        case UPDATE:
                            // UPDATE operation only on program/strategy and rules tables
                            addRow = isProgramStrategy || isRule;
                            break;
                        case DELETE:
                            // DELETE operation only on program/strategy and rules tables
                            addRow = isProgramStrategy || isRule;
                            break;
                        case DUPLICATE:
                            // DUPLICATE operation not handled for now
                            break;
                    }

                    if (addRow) {
                        filteredSynchroTable.addRows(row);
                    }
                }
            }
            // don't add table if no insert row detected
            if (!filteredSynchroTable.isRowsEmpty()) {
                filteredSynchroChanges.addTables(filteredSynchroTable);
            }
        }

        // return a filtered synchro changes
        return filteredSynchroChanges;
    }

    /** {@inheritDoc} */
    @Override
    public SynchroChangesDTO getSurveySynchroChangesFromFile(SynchroChangesDTO synchroChanges) {
        if (synchroChanges == null || synchroChanges.isTablesEmpty()) {
            return synchroChanges;
        }

        SynchroChangesDTO filteredSynchroChanges = ReefDbBeans.clone(synchroChanges);
        filteredSynchroChanges.setTables(new ArrayList<>());

        // iterate over tables changes
        for (SynchroTableDTO table : synchroChanges.getTables()) {
            String tableName = table.getName().toUpperCase();

            if (DataSynchroTables.SURVEY.name().equals(tableName)) {

                SynchroTableDTO filteredSynchroTable = ReefDbBeans.clone(table);
                filteredSynchroTable.setRows(new ArrayList<>());
                if (!table.isRowsEmpty()) {
                    // iterate over rows changes
                    for (SynchroRowDTO row : table.getRows()) {
                        SynchroOperationType operationType = SynchroOperationType.valueOf(row.getOperationType().toUpperCase());
                        if (operationType == SynchroOperationType.INSERT
                                || operationType == SynchroOperationType.DUPLICATE
                                || operationType == SynchroOperationType.IGNORE) {
                            filteredSynchroTable.addRows(row);
                        }
                    }
                }

                // don't add table if no insert row detected
                if (!filteredSynchroTable.isRowsEmpty()) {
                    filteredSynchroChanges.addTables(filteredSynchroTable);
                }
            }
        }

        // return a filtered synchro changes
        return filteredSynchroChanges;
    }

    /* -- Internal methods -- */

    /**
     * <p>toSynchroChangesDTO.</p>
     *
     * @param changesConnectionProperties a {@link java.util.Properties} object.
     * @param file a {@link java.io.File} object.
     * @param source a {@link fr.ifremer.quadrige3.synchro.vo.SynchroChangesVO} object.
     * @return a {@link fr.ifremer.reefdb.dto.system.synchronization.SynchroChangesDTO} object.
     */
    protected SynchroChangesDTO toSynchroChangesDTO(Properties changesConnectionProperties, File file, SynchroChangesVO source) {
        SynchroChangesDTO target = ReefDbBeanFactory.newSynchroChangesDTO();

        // File
        target.setFile(file);

        if (source != null && !source.isEmpty()) {

            try {
                ReferentialJdbcDao changesReferentialJdbcDao = new ReferentialJdbcDaoImpl(changesConnectionProperties);

                List<SynchroTableDTO> tables = Lists.newArrayListWithCapacity(source.getTableNames().size());
                for (String tableName : source.getTableNames()) {
                    // Create table bean
                    SynchroTableDTO table = ReefDbBeanFactory.newSynchroTableDTO();
                    table.setName(tableName);

                    // create row list
                    List<SynchroRowDTO> rows = Lists.newArrayList();
                    table.setRows(rows);

                    // add inserts
                    rows.addAll(toSynchroRowDTOs(changesReferentialJdbcDao, tableName, source.getInserts(tableName), SynchroOperationType.INSERT));

                    // add updates
                    rows.addAll(toSynchroRowDTOs(changesReferentialJdbcDao, tableName, source.getUpdates(tableName), SynchroOperationType.UPDATE));

                    // add deletes
                    rows.addAll(toSynchroRowDTOs(changesReferentialJdbcDao, tableName, source.getDeletes(tableName), SynchroOperationType.DELETE));

                    // add rejects (on survey only)
                    if (tableName.equalsIgnoreCase(DataSynchroTables.SURVEY.name())) {
                        String rejectedRows = source.getRejectedRows(tableName);
                        rows.addAll(surveyRejectsToSynchroRowDTOs(rejectedRows, SynchroOperationType.DUPLICATE));
                        rows.addAll(surveyRejectsToSynchroRowDTOs(rejectedRows, SynchroOperationType.IGNORE));
                    }

                    // Add to result, if not empty
                    if (!table.isRowsEmpty()) {
                        tables.add(table);
                    }
                }
                target.addAllTables(tables);
            } finally {
                try {
                    Daos.shutdownDatabase(changesConnectionProperties);
                } catch (SQLException ignored) {
                    // Just log, but do nothing
                    log.warn(t("quadrige3.error.synchro.import.shutdown"));
                }
            }
            // Link row between them
            // TODO BLA
            //linkTableRows(connectionProperties, tables);

        }

        return target;
    }

    /**
     * <p>toSynchroRowDTOs.</p>
     *
     * @param changesReferentialJdbcDao a {@link fr.ifremer.quadrige3.core.dao.referential.ReferentialJdbcDao} object.
     * @param tableName a {@link java.lang.String} object.
     * @param pks a {@link java.util.Collection} object.
     * @param operationType a {@link fr.ifremer.quadrige3.synchro.service.client.vo.SynchroOperationType} object.
     * @return a {@link java.util.List} object.
     */
    protected List<SynchroRowDTO> toSynchroRowDTOs(ReferentialJdbcDao changesReferentialJdbcDao, String tableName, Collection<String> pks, SynchroOperationType operationType) {
        List<SynchroRowDTO> result = Lists.newArrayList();
        if (CollectionUtils.isEmpty(pks)) {
            return result;
        }

        DecoratorService decoratorService = getDecoratorService();
        Decorator<?> decorator = null;
        boolean useDecorator = true;

        for (String pk : pks) {
            String name = null;

            if (useDecorator) {
                Object vo = null;
                try {
                    // get VO from changes data table (ex: SURVEY)
                    vo = changesReferentialJdbcDao.getVOByTableNameAndPk(tableName, pk);
                    if (vo == null) {
                        // get VO from target data table (for delete)
                        vo = targetReferentialJdbcDao.getVOByTableNameAndPk(tableName, pk);
                    }
                } catch (QuadrigeTechnicalException ignored) {
                }

                // Retrieve the decorator (first loop)
                if (decorator == null) {
                    if (vo == null) {
                        log.warn(String.format("[%s] Unable to load object with [pk=%s]. Check if referentialJdbcDao.getVOByTableNameAndPk() is well implemented for this table.", tableName, pk));
                        useDecorator = false;
                    } else {
                        decorator = decoratorService.getDecorator(vo);
                        if (decorator == null) {
                            log.warn(String.format("[%s] Unable to find decorator for class [%s]. Please add a default decorator.", tableName, vo.getClass().getSimpleName()));
                            useDecorator = false;
                        }
                    }
                }
                if (useDecorator) {
                    name = decorator.toString(vo);
                }
            }

            // default basic output
            if (!useDecorator) {
                name = String.format("%s (%s)", tableName, pk);
            }

            // Create row bean
            SynchroRowDTO row = ReefDbBeanFactory.newSynchroRowDTO();
            row.setName(name);
            row.setOperationType(operationType.name());
            row.setPkStr(pk);

            // Default reject strategy
            row.setStrategy(null); //RejectedRow.ResolveStrategy.DO_NOTHING.name());
            result.add(row);
        }

        return result;
    }

    /**
     * <p>surveyRejectsToSynchroRowDTOs.</p>
     *
     * @param rejectedRows a {@link java.lang.String} object.
     * @param operationType a {@link fr.ifremer.quadrige3.synchro.service.client.vo.SynchroOperationType} object.
     * @return a {@link java.util.List} object.
     */
    protected List<SynchroRowDTO> surveyRejectsToSynchroRowDTOs(String rejectedRows, SynchroOperationType operationType) {
        List<SynchroRowDTO> result = Lists.newArrayList();
        if (StringUtils.isBlank(rejectedRows)) {
            return result;
        }

        List<RejectedRow> rejects = RejectedRow.parseFromString(rejectedRows);
        int counter = 0;
        for (RejectedRow reject : rejects) {

            if (operationType == SynchroOperationType.DUPLICATE) {
                if (reject.cause == RejectedRow.Cause.DUPLICATE_KEY
                    && reject.targetPkStr != null) {
                    int targetSurveyId = Integer.parseInt(reject.targetPkStr);
                    LightSurveyVO surveyVO = surveyDao.getLightSurveyById(targetSurveyId);
                    if (surveyVO == null) {
                        throw new ReefDbTechnicalException(String.format("Could not load survey with id [%s]", targetSurveyId));
                    }
                    String name = decorate(surveyVO);

                    // Create row bean
                    SynchroRowDTO row = ReefDbBeanFactory.newSynchroRowDTO();
                    row.setName(name);
                    row.setOperationType(operationType.name());
                    row.setPkStr(reject.pkStr);

                    // Default reject strategy
                    row.setStrategy(null); //RejectedRow.ResolveStrategy.DO_NOTHING.name());
                    result.add(row);
                }
            }

            else if (operationType == SynchroOperationType.IGNORE) {
                if (reject.cause == RejectedRow.Cause.MISSING_FOREIGN_KEY
                        && reject.targetPkStr != null
                        && reject.fkColumnName != null
                        && reject.targetFkValue != null) {

                    // Create row bean
                    SynchroRowDTO row = ReefDbBeanFactory.newSynchroRowDTO();
                    row.setName(reject.targetFkValue);
                    row.setOperationType(operationType.name());
                    row.setPkStr(reject.pkStr);

                    // Default reject strategy
                    row.setStrategy(null); //RejectedRow.ResolveStrategy.DO_NOTHING.name());
                    result.add(row);
                }
            }
            counter++;
        }

        return result;
    }

}
