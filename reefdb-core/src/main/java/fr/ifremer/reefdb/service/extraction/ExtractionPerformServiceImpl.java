package fr.ifremer.reefdb.service.extraction;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.Times;
import fr.ifremer.quadrige3.core.dao.technical.xmlQuery.XMLQuery;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.program.ReefDbProgramDao;
import fr.ifremer.reefdb.dao.administration.strategy.ReefDbStrategyDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.referential.pmfm.ReefDbPmfmDao;
import fr.ifremer.reefdb.dao.system.extraction.ReefDbExtractionResultDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumPeriodDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.enums.ExtractionOutputType;
import fr.ifremer.reefdb.dto.referential.GroupingTypeDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionContextDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPmfmInfoDTO;
import fr.ifremer.reefdb.service.*;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom2.Element;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * New extraction perform service
 * Base Mantis #49970
 *
 * @author peck7 on 14/05/2019.
 */
@Service("reefdbExtractionPerformService")
public class ExtractionPerformServiceImpl implements ExtractionPerformService {

    private static final Log LOG = LogFactory.getLog(ExtractionPerformServiceImpl.class);

    private static final String TABLE_NAME_PREFIX = "EXT_";
    private static final String BASE_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "B%s";
    private static final String RAW_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "R%s";
    private static final String PMFM_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "P%s_%s";
    private static final String COMMON_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "C%s";
    private static final String RESULT_TABLE_NAME_PATTERN = TABLE_NAME_PREFIX + "E%s";

    private static final String XML_QUERY_PATH = "xmlQuery/extraction";

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource(name = "reefdbProgramStrategyService")
    protected ProgramStrategyService programStrategyService;

    @Resource(name = "reefdbExtractionService")
    protected ExtractionService extractionService;

    @Resource(name = "reefDbExtractionResultDao")
    protected ReefDbExtractionResultDao extractionResultDao;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource(name = "reefDbProgramDao")
    private ReefDbProgramDao programDao;

    @Resource(name = "reefDbStrategyDao")
    protected ReefDbStrategyDao strategyDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Override
    public void performExtraction(ExtractionDTO extraction, ExtractionOutputType outputType, File outputFile, ProgressionCoreModel progressionModel) {
        Assert.notNull(extraction);
        Assert.notNull(extraction.getId());
        Assert.notEmpty(extraction.getFilters());
        Assert.notNull(ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.PERIOD));
        Assert.notNull(ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.ORDER_ITEM_TYPE));
        Assert.notNull(outputType);
        Assert.notNull(outputFile);

        // ensure all filters are loaded
        extractionService.loadFilteredElements(extraction);

        // init progression model
        progressionModel.setMessage("");
        progressionModel.setTotal(11);
        long startTime = System.currentTimeMillis();

        // Get programs
        List<String> programCodes = ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.PROGRAM);
        Assert.notNull(programCodes);
        // Filter by user privileges (Mantis #59187)
        Collection<String> allowedProgramCodes = programStrategyService.getReadableProgramCodesByQuserId(dataContext.getRecorderPersonId());
        // Allow all local programs (Mantis #62864)
        Collection<String> localProgramCodes = programStrategyService.getProgramsByCodes(programCodes).stream().filter(ReefDbBeans::isLocalReferential).map(ProgramDTO::getCode).collect(Collectors.toList());
        programCodes.retainAll(CollectionUtils.union(localProgramCodes, allowedProgramCodes));

        if (programCodes.isEmpty()) {
            throw new ReefDbBusinessException(t("reefdb.service.extraction.noData.error"));
        }

        List<ProgramDTO> programs = programCodes.stream()
            // Load program with moratorium
            .map(programCode -> programDao.getProgramByCode(programCode))
            .collect(Collectors.toList());

        // Extraction context
        ExtractionContextDTO context = ReefDbBeanFactory.newExtractionContextDTO();
        context.setExtraction(extraction);
        context.setPeriods(ReefDbBeans.getExtractionPeriods(extraction));
        context.setPrograms(programs);
        context.setUniqueId(System.currentTimeMillis());
        context.setBaseTableName(String.format(BASE_TABLE_NAME_PATTERN, context.getUniqueId()));
        context.setRawTableName(String.format(RAW_TABLE_NAME_PATTERN, context.getUniqueId()));
        context.setCommonTableName(String.format(COMMON_TABLE_NAME_PATTERN, context.getUniqueId()));
        context.setResultTableName(String.format(RESULT_TABLE_NAME_PATTERN, context.getUniqueId()));

        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Beginning a %s extraction (id=%s) with:", outputType, extraction.getId()));
            LOG.info(String.format("\t date ranges: %s", ReefDbBeans.toString(context.getPeriods())));
            LOG.info(String.format("\tgeo grouping: %s", ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.ORDER_ITEM_TYPE)));
            LOG.info(String.format("\t    programs: %s", getProgramCodes(context)));
            LOG.info(String.format("\t   locations: %s", ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.LOCATION)));
            LOG.info(String.format("\t departments: %s", ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.DEPARTMENT)));
            LOG.info(String.format("\t      taxons: %s", ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.TAXON)));
            LOG.info(String.format("\ttaxon groups: %s", ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.TAXON_GROUP)));
            LOG.info(String.format("\t       pmfmu: %s", ReefDbBeans.getFilterElementsIds(extraction, ExtractionFilterValues.PMFM)));
        }

        try {
            try {

                // create the concat functions used to concat distinct strings (ex: DEP_NM)
                createConcatDistinctFunction("STRING", "VARCHAR(2000)");
                createConcatDistinctFunction("INTEGER", "INTEGER");
                // create the distinct functions used for quality informations (Mantis #51297)
                createDistinctFunction("MEAS_QUAL_FLAG_NM", "VARCHAR(2000)");
                createDistinctFunction("MEAS_QUALIF_CM", "VARCHAR(2000)");

                // STEP 1 : Create the base table
                long nbRowsInserted = createBaseTable(context, outputType);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s surveys/sampling operations have to be extract (temp table : %s)", nbRowsInserted, context.getBaseTableName()));
                } else {
                    LOG.info(String.format("%s surveys/sampling operations have to be extract", nbRowsInserted));
                }
                if (nbRowsInserted == 0) {
                    throw new ReefDbBusinessException(t("reefdb.service.extraction.noData.error"));
                }

                // STEP 1b : Remove surveys under moratorium
                int nbRowsRemoved = cleanSurveysUnderMoratorium(context);
                progressionModel.increments(1);
                nbRowsInserted -= nbRowsRemoved;
                if (LOG.isDebugEnabled() && nbRowsRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from base data which are under moratorium", nbRowsRemoved));
                }
                // if all the rows have been removed because of moratoriums, there is no more data to extract
                if (nbRowsInserted == 0) {
                    throw new ReefDbBusinessException(t("reefdb.service.extraction.noData.error"));
                }

                // STEP 2 : Create the raw table with full measurements data in line
                nbRowsInserted = createRawTable(context, outputType);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s rows of raw data (temp table : %s)", nbRowsInserted, context.getRawTableName()));
                } else {
                    LOG.info(String.format("%s rows of raw data", nbRowsInserted));
                }

                // STEP 5a : Clean data
                nbRowsRemoved = cleanTaxonData(context, outputType);
                progressionModel.increments(1);
                nbRowsInserted -= nbRowsRemoved;
                if (LOG.isDebugEnabled() && nbRowsRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from raw data which not corresponding to taxon or taxon group filter", nbRowsRemoved));
                }

                // STEP 5b : Second clean operation: remove surveys from hermetic programs if user is not allowed (Mantis #42817)
                nbRowsRemoved = cleanDataUnderMoratorium(context);
                progressionModel.increments(1);
                nbRowsInserted -= nbRowsRemoved;
                if (LOG.isDebugEnabled() && nbRowsRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from raw data which are under moratorium", nbRowsRemoved));
                }

                // if all the rows have been removed because of filters, there is no more data to extract
                if (nbRowsInserted == 0) {
                    throw new ReefDbBusinessException(t("reefdb.service.extraction.noPmfm.error"));
                }

                // STEP 3 : Build PMFM metadata
                buildPmfmInformation(context);
                progressionModel.increments(1);

                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("list of pmfmu ids to split : %s", ReefDbBeans.collectProperties(context.getPmfmInfos(), ExtractionPmfmInfoDTO.PROPERTY_PMFM_ID)));
                }

                // STEP 5 : Create pmfm tables
                createPmfmTables(context);
                progressionModel.increments(1);

                Map<String, String> fieldNamesByAlias = Maps.newHashMap();
                Map<String, String> decimalFormats = Maps.newHashMap();
                Map<String, String> dateFormats = Maps.newHashMap();

                // STEP 6 : Create common table (todo ? with non individual measurements)
                createCommonTable(context, outputType);
                progressionModel.increments(1);

                // STEP 7 : Create result table
                nbRowsInserted = createResultTable(context, outputType, fieldNamesByAlias, decimalFormats, dateFormats);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s rows to write (result table : %s)", nbRowsInserted, context.getResultTableName()));
                } else {
                    LOG.info(String.format("%s rows to write", nbRowsInserted));
                }

                // STEP 7b : Add moratorium info
                int nbRowsUpdated = updateResultTable(context);
                progressionModel.increments(1);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("%s rows updated with moratorium information", nbRowsUpdated));
                }

                // STEP 8 : Final query and write to csv file
                writeExtraction(context, outputType, fieldNamesByAlias, decimalFormats, dateFormats, outputFile);
                progressionModel.increments(1);

                if (LOG.isInfoEnabled()) {
                    long time = System.currentTimeMillis() - startTime;
                    LOG.info(String.format("Extraction %s performed in %s. result file is : %s", outputType, Times.durationToString(time), outputFile.getAbsolutePath()));
                }

            } catch (ReefDbBusinessException e) {
                throw e; // throw directly
            } catch (Exception e) {
                throw new ReefDbTechnicalException(t("reefdb.service.extraction.error"), e);
            }
        } finally {

            // drop concat functions
            dropConcatDistinctFunction("STRING");
            dropConcatDistinctFunction("INTEGER");
        }
    }

    private long createBaseTable(ExtractionContextDTO context, ExtractionOutputType outputType) {
        XMLQuery xmlQuery = createXMLQuery("createBaseTable");
        xmlQuery.bind("baseTableName", context.getBaseTableName());
        xmlQuery.bind("orderItemTypeCode", getOrderItemTypeCode(context.getExtraction()));

        // active groups depending the output type
        setGroups(xmlQuery, outputType);

        // add mandatory period filter
        List<ExtractionPeriodDTO> periodFilters = context.getPeriods();
        Element periodFilter = xmlQuery.getFirstTag(XMLQuery.TAG_WHERE, XMLQuery.ATTR_GROUP, "periodFilter");
        Assert.notNull(periodFilter);
        for (int i = 0; i < periodFilters.size(); i++) {
            XMLQuery periodFilterQuery = createXMLQuery("injectionPeriodFilter");
            if (i > 0) {
                periodFilterQuery.getDocumentQuery().getRootElement().setAttribute(XMLQuery.ATTR_OPERATOR, "OR");
            }
            String periodAlias = "PERIOD" + i;
            periodFilterQuery.replaceAllBindings("PERIOD", periodAlias);
            periodFilter.addContent(periodFilterQuery.getDocument().getRootElement().detach());
            xmlQuery.bind(periodAlias + "_startDate", formatDate(periodFilters.get(i).getStartDate()));
            xmlQuery.bind(periodAlias + "_endDate", formatDate(periodFilters.get(i).getEndDate()));
        }

        // add program filter
        List<String> programCodes = getProgramCodes(context);
        xmlQuery.setGroup("programFilter", CollectionUtils.isNotEmpty(programCodes));
        xmlQuery.bind("progCodes", Daos.getInStatementFromStringCollection(programCodes));

        // add monitoring location filter
        List<Integer> locationIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.LOCATION);
        xmlQuery.setGroup("locationFilter", CollectionUtils.isNotEmpty(locationIds));
        xmlQuery.bind("monLocIds", Daos.getInStatementFromIntegerCollection(locationIds));

        // add campaign filter
        List<Integer> campaignIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.CAMPAIGN);
        xmlQuery.setGroup("campaignFilter", CollectionUtils.isNotEmpty(campaignIds));
        xmlQuery.bind("campaignIds", Daos.getInStatementFromIntegerCollection(campaignIds));

        // add department filter
        List<Integer> departmentIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.DEPARTMENT);
        xmlQuery.setGroup("departmentFilter", CollectionUtils.isNotEmpty(departmentIds));
        xmlQuery.bind("depIds", Daos.getInStatementFromIntegerCollection(departmentIds));

        // add pmfm filter
        List<Integer> pmfmIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.PMFM);
        xmlQuery.setGroup("pmfmFilter", CollectionUtils.isNotEmpty(pmfmIds));
        xmlQuery.bind("pmfmIds", Daos.getInStatementFromIntegerCollection(pmfmIds));

        // execute insertion
        execute(xmlQuery);

        // if no program filter, gather them from base table
        if (CollectionUtils.isEmpty(programCodes)) {
            gatherProgramCodes(context);
        }

        return countFrom(context.getBaseTableName());
    }

    private void gatherProgramCodes(ExtractionContextDTO context) {
        // gather program codes from base table
        XMLQuery xmlQuery = createXMLQuery("programCodes");
        xmlQuery.bind("baseTableName", context.getBaseTableName());
        List<String> programCodes = extractionResultDao.queryStringList(xmlQuery.getSQLQueryAsString(), null);

        List<ProgramDTO> programs = programCodes.stream()
            // Load program with moratorium
            .map(programCode -> programDao.getProgramByCode(programCode))
            .collect(Collectors.toList());

        // set into context
        context.setPrograms(programs);
    }

    private long createRawTable(ExtractionContextDTO context, ExtractionOutputType outputType) {

        XMLQuery xmlQuery = createXMLQuery("createRawTable");
        xmlQuery.bind("rawTableName", context.getRawTableName());
        xmlQuery.bind("baseTableName", context.getBaseTableName());

        // active groups depending the output type
        setGroups(xmlQuery, outputType);

        // add pampa configuration
        xmlQuery.bind("transcribingItemTypeLbForPampa", config.getTranscribingItemTypeLbForPampa());

        execute(xmlQuery);

        return countFrom(context.getRawTableName());
    }

    private void buildPmfmInformation(ExtractionContextDTO context) {

        // Gather pmfm from raw table
        List<ExtractionPmfmInfoDTO> pmfmInfos = getPmfmInfo(context);

        if (CollectionUtils.isEmpty(pmfmInfos)) {
            throw new ReefDbBusinessException(t("reefdb.service.extraction.noPmfm.error"));
        }

        // Get PMFM strategies corresponding to extraction filters and compute pmfm sort order
        Set<PmfmStrategyDTO> allPmfmStrategies = context.getPeriods().stream()
            .collect(HashSet::new,
                (pmfmStrategies, period) ->
                    pmfmStrategies.addAll(strategyDao.getPmfmStrategiesByProgramCodesAndDates(getProgramCodes(context), period.getStartDate(), period.getEndDate())),
                HashSet::addAll);

        Map<Integer, Integer> rankOrdersByPmfmId = allPmfmStrategies.stream()
            .collect(HashMap::new,
                (map, pmfmStrategy) ->
                    map.put(pmfmStrategy.getPmfm().getId(), pmfmStrategy.getRankOrder()),
                HashMap::putAll);

        // Affect rank order
        pmfmInfos.forEach(pmfmInfo -> {
                Integer rankOrder = rankOrdersByPmfmId.get(pmfmInfo.getPmfmId());
                // put the pmfm outside a strategy at the end (Mantis #43695)
                pmfmInfo.setRankOrder(rankOrder != null ? rankOrder : Integer.MAX_VALUE);
            }
        );

        // Sort by rankOrder
        pmfmInfos.sort(Comparator.comparingInt(ExtractionPmfmInfoDTO::getRankOrder));

        // Affect to context
        context.setPmfmInfos(pmfmInfos);
    }

    private List<ExtractionPmfmInfoDTO> getPmfmInfo(ExtractionContextDTO context) {

        XMLQuery xmlQuery = createXMLQuery("pmfmInfo");
        xmlQuery.bind("rawTableName", context.getRawTableName());

        // retrieve pmfm ids from filter
        List<Integer> pmfmIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.PMFM);
        xmlQuery.setGroup("pmfmFilter", CollectionUtils.isNotEmpty(pmfmIds));
        xmlQuery.bind("pmfmIds", Daos.getInStatementFromIntegerCollection(pmfmIds));

        return extractionResultDao.query(xmlQuery.getSQLQueryAsString(), null, (resultSet, i) -> newPmfmInfo(context,
            resultSet.getInt(1),
            resultSet.getBoolean(2),
            resultSet.getBoolean(3)));
    }

    private ExtractionPmfmInfoDTO newPmfmInfo(ExtractionContextDTO context, int pmfmId, boolean isSurvey, boolean isIndividual) {

        ExtractionPmfmInfoDTO pmfmInfo = ReefDbBeanFactory.newExtractionPmfmInfoDTO();
        pmfmInfo.setPmfmId(pmfmId);
        pmfmInfo.setSurvey(isSurvey);
        pmfmInfo.setIndividual(isIndividual);

        // compute alias
        String safePmfmId = pmfmInfo.getPmfmId() < 0 ? "M" : "" + pmfmInfo.getPmfmId();
        pmfmInfo.setAlias((pmfmInfo.isSurvey() ? "SU" : "SO") + (pmfmInfo.isIndividual() ? "I" : "") + safePmfmId);
        // Generate pmfm table name
        pmfmInfo.setTableName(String.format(PMFM_TABLE_NAME_PATTERN, context.getUniqueId(), pmfmInfo.getAlias()));

        return pmfmInfo;
    }

    private int cleanSurveysUnderMoratorium(ExtractionContextDTO context) {

        List<MoratoriumDTO> globalMoratoriums = getMoratoriums(context, true, true);

        if (globalMoratoriums.isEmpty()) return 0;
        int nbRemoves = 0;
        // Iterate over global moratoriums to remove surveys
        for (MoratoriumDTO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("cleanSurveysUnderMoratorium");
                xmlQuery.bind("tableName", context.getBaseTableName());
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                nbRemoves += execute(xmlQuery);
            }
        }

        return nbRemoves;
    }


    private int cleanTaxonData(ExtractionContextDTO context, ExtractionOutputType outputType) {

        // remove lines with taxon group or taxon corresponding to filters
        int nbRemoves = 0;
        XMLQuery xmlQuery = createXMLQuery("cleanTaxon");
        xmlQuery.bind("tableName", context.getRawTableName());
        // active groups depending the output type
        setGroups(xmlQuery, outputType);

        // if a taxon or taxon group filter is defined, will remove raw line not corresponding to filter (avoid blank cells in split table)
        List<Integer> taxonGroupIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.TAXON_GROUP);
        List<Integer> taxonNameIds = ReefDbBeans.getFilterElementsIds(context.getExtraction(), ExtractionFilterValues.TAXON);

        if (CollectionUtils.isNotEmpty(taxonGroupIds) || CollectionUtils.isNotEmpty(taxonNameIds)) {

            if (CollectionUtils.isNotEmpty(taxonGroupIds)) {
                xmlQuery.setGroup("taxonGroup", true);
                xmlQuery.bind("taxonGroupIds", Daos.getInStatementFromIntegerCollection(taxonGroupIds));
            } else {
                xmlQuery.setGroup("taxonGroup", false);
            }

            if (CollectionUtils.isNotEmpty(taxonNameIds)) {
                xmlQuery.setGroup("taxonName", true);
                xmlQuery.bind("taxonNameIds", Daos.getInStatementFromIntegerCollection(taxonNameIds));
            } else {
                xmlQuery.setGroup("taxonName", false);
            }

            int nbRemoved = execute(xmlQuery);

            if (LOG.isDebugEnabled()) {
                if (nbRemoved > 0) {
                    LOG.debug(String.format("%s rows removed from raw data which not corresponding to taxon and taxon group filter", nbRemoved));
                }
            }

            nbRemoves += nbRemoved;
        }

        return nbRemoves;
    }

    private int cleanDataUnderMoratorium(ExtractionContextDTO context) {

        List<MoratoriumDTO> partialMoratoriums = getMoratoriums(context, false, true);

        if (partialMoratoriums.isEmpty()) return 0;
        int nbRemoves = 0;

        // Iterate over partial moratoriums to remove measurements
        for (MoratoriumDTO moratorium : partialMoratoriums) {

            // Find all pmfms corresponding to moratorium
            Set<Integer> pmfmIds = moratorium.getPmfms().stream()
                .flatMap(moratoriumPmfm -> pmfmDao.findPmfms(
                        moratoriumPmfm.getParameterCode(),
                        moratoriumPmfm.getMatrixId(),
                        moratoriumPmfm.getFractionId(),
                        moratoriumPmfm.getMethodId(),
                        moratoriumPmfm.getUnitId(),
                        null,
                        StatusFilter.ALL.toStatusCodes())
                    .stream())
                .map(PmfmDTO::getId)
                .collect(Collectors.toSet());

            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("cleanDataUnderMoratorium");
                xmlQuery.bind("tableName", context.getRawTableName());
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                xmlQuery.bind("pmfmIds", Daos.getInStatementFromIntegerCollection(pmfmIds));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                addPartialMoratoriumFilter(xmlQuery, moratorium);

                nbRemoves += execute(xmlQuery);
            }
        }

        return nbRemoves;
    }

    private void createPmfmTables(ExtractionContextDTO context) {

        context.getPmfmInfos().forEach(pmfmInfo -> {
            XMLQuery xmlQuery = createXMLQuery("createPmfmTable");
            xmlQuery.bind("pmfmTableName", pmfmInfo.getTableName());
            xmlQuery.bind("rawTableName", context.getRawTableName());
            xmlQuery.bind("parentId", pmfmInfo.isSurvey() ? "SURVEY_ID" : "SAMPLING_OPER_ID");
            xmlQuery.bind("pmfmId", String.valueOf(pmfmInfo.getPmfmId()));
            xmlQuery.bind("isSurveyMeas", pmfmInfo.isSurvey() ? "1" : "0");
            xmlQuery.bind("measIndivId", pmfmInfo.isIndividual() ? "NOT NULL" : "NULL");
            execute(xmlQuery);
            if (LOG.isDebugEnabled()) {

                // Count inserted data
                long nbRowsInserted = countFrom(pmfmInfo.getTableName());
                LOG.debug(String.format("%s rows of pmfm raw data inserted into %s", nbRowsInserted, pmfmInfo.getTableName()));
            }
        });
    }

    private void createCommonTable(ExtractionContextDTO context, ExtractionOutputType outputType) {

        XMLQuery xmlQuery = createXMLQuery("createCommonTable");
        xmlQuery.bind("commonTableName", context.getCommonTableName());
        xmlQuery.bind("rawTableName", context.getRawTableName());

        // Active groups depending the output type
        setGroups(xmlQuery, outputType);

        execute(xmlQuery);

        if (LOG.isDebugEnabled()) {

            // Count inserted data
            long nbRowsInserted = countFrom(context.getCommonTableName());
            LOG.debug(String.format("%s rows of common data inserted into %s", nbRowsInserted, context.getCommonTableName()));
        }

    }

    private long createResultTable(ExtractionContextDTO context,
                                   ExtractionOutputType outputType,
                                   Map<String, String> fieldNamesByAlias,
                                   Map<String, String> decimalFormats,
                                   Map<String, String> dateFormats) {

        XMLQuery xmlQuery = createXMLQuery("createResultTable");
        xmlQuery.bind("resultTableName", context.getResultTableName());
        xmlQuery.bind("commonTableName", context.getCommonTableName());
        xmlQuery.bind("baseTableName", context.getBaseTableName());

        // Active groups depending on output type
        setGroups(xmlQuery, outputType);

        Collection<ExtractionPmfmInfoDTO> pmfmInfos = context.getPmfmInfos();

        Predicate<ExtractionPmfmInfoDTO> surveyMeasurementPredicate = pmfmInfo -> pmfmInfo.isSurvey() && !pmfmInfo.isIndividual();
        Predicate<ExtractionPmfmInfoDTO> samplingMeasurementPredicate = pmfmInfo -> !pmfmInfo.isSurvey() && !pmfmInfo.isIndividual();
        Predicate<ExtractionPmfmInfoDTO> surveyIndividualMeasurementPredicate = pmfmInfo -> pmfmInfo.isSurvey() && pmfmInfo.isIndividual();
        Predicate<ExtractionPmfmInfoDTO> samplingIndividualMeasurementPredicate = pmfmInfo -> !pmfmInfo.isSurvey() && pmfmInfo.isIndividual();

        // Active measurement groups
        xmlQuery.setGroup(getInjectionName(true, false), pmfmInfos.stream().anyMatch(surveyMeasurementPredicate));
        xmlQuery.setGroup(getInjectionName(false, false), pmfmInfos.stream().anyMatch(samplingMeasurementPredicate));
        xmlQuery.setGroup(getInjectionName(true, true), pmfmInfos.stream().anyMatch(surveyIndividualMeasurementPredicate));
        xmlQuery.setGroup(getInjectionName(false, true), pmfmInfos.stream().anyMatch(samplingIndividualMeasurementPredicate));

        // Add all measurements
        pmfmInfos.forEach(pmfmInfo -> {

            PmfmDTO pmfm = pmfmDao.getPmfmById(pmfmInfo.getPmfmId());

            // Add injection query for pmfm table (replacing all bindings by the pmfm alias)
            xmlQuery.injectQuery(getXMLQueryFile("injectionPmfm"), "PMFM_ALIAS", pmfmInfo.getAlias(), getInjectionName(pmfmInfo));

            // Bind table names
            xmlQuery.bind(pmfmInfo.getAlias() + "_pmfmTableName", pmfmInfo.getTableName());

            // Active value
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_numerical_without_zero", !pmfm.getParameter().isQualitative());
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_numerical_with_zero", false);
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_qualitative", pmfm.getParameter().isQualitative());

            // Active join link
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_surveyJoin", surveyMeasurementPredicate.test(pmfmInfo));
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_samplingOperationJoin", samplingMeasurementPredicate.test(pmfmInfo));
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_surveyJoin_individual", surveyIndividualMeasurementPredicate.test(pmfmInfo));
            xmlQuery.setGroup(pmfmInfo.getAlias() + "_samplingOperationJoin_individual", samplingIndividualMeasurementPredicate.test(pmfmInfo));

            // Build output name using transcribing (see Mantis #50725)
            String unitName = extractionResultDao.getPmfmUnitNameForExtraction(pmfm);
            String pmfmName = String.format("%s%s", extractionResultDao.getPmfmNameForExtraction(pmfm), StringUtils.isNotBlank(unitName) ? "-" + unitName : "");
            String formattedPmfmName = String.format("%s_%s",
                pmfmInfo.isSurvey() ? t("reefdb.service.extraction.fieldNamePrefix.SURVEY") : t("reefdb.service.extraction.fieldNamePrefix.SAMPLING_OPER"),
                pmfmName);
            fieldNamesByAlias.put(pmfmInfo.getAlias(), ReefDbBeans.toFullySecuredString(formattedPmfmName));

            // Build number format
            if (!pmfm.getParameter().isQualitative()) {
                decimalFormats.put(pmfmInfo.getAlias(), getNumericFormat(pmfm));
            }
        });

        // Add analyst from measurements
        xmlQuery.bind("depNmFields", getAliasedFields(pmfmInfos, "DEP_NM"));

        // Add measurement comment
        xmlQuery.bind("measCmFields", getAliasedFields(pmfmInfos, "MEAS_CM"));

        // add measurement quality flags
        xmlQuery.bind("measQualFlagFields", getAliasedFields(pmfmInfos, "MEAS_QUAL_FLAG_NM"));
        xmlQuery.bind("measQualFlagDefault", ReefDbBeans.toQuotedString(t("reefdb.service.extraction.notDistinct.MEAS_QUAL_FLAG_NM")));

        // add measurement qualification dates
        xmlQuery.bind("measQualifDtFields", getAliasedFields(pmfmInfos, "MEAS_QUALIF_DT"));

        // add measurement qualification comments
        xmlQuery.bind("measQualifCmFields", getAliasedFields(pmfmInfos, "MEAS_QUALIF_CM"));
        xmlQuery.bind("measQualifCmDefault", ReefDbBeans.toQuotedString(t("reefdb.service.extraction.notDistinct.MEAS_QUALIF_CM")));

        // add measurement and taxon measurement ids
        xmlQuery.bind("measIdFields", getAliasedFields(pmfmInfos, "MEAS_ID"));
        xmlQuery.bind("taxonMeasIdFields", getAliasedFields(pmfmInfos, "TAXON_MEAS_ID"));

        // inject empty column for moratorium, null by default, then update it with specific query
        xmlQuery.injectQuery(getXMLQueryFile("injectionUnderMoratorium"));

        prepare(xmlQuery, fieldNamesByAlias, decimalFormats, dateFormats);

        execute(xmlQuery);

        return countFrom(context.getResultTableName());
    }

    private String getInjectionName(ExtractionPmfmInfoDTO pmfmInfo) {
        return getInjectionName(pmfmInfo.isSurvey(), pmfmInfo.isIndividual());
    }

    private String getInjectionName(boolean isSurvey, boolean isIndividual) {
        return String.format("%s%sMeasurements", isSurvey ? "survey" : "sampling", isIndividual ? "Individual" : ""
        );
    }

    private int updateResultTable(ExtractionContextDTO context) {

        int nbUpdates = 0;
        List<MoratoriumDTO> globalMoratoriums = getMoratoriums(context, true, false);

        // Iterate over global moratoriums to update moratorium column
        for (MoratoriumDTO moratorium : globalMoratoriums) {
            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("updateGlobalMoratorium");
                xmlQuery.bind("tableName", context.getResultTableName());
                xmlQuery.bind("underMoratorium", t("reefdb.service.extraction.moratorium.yes"));
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                nbUpdates += execute(xmlQuery);
            }
        }

        List<MoratoriumDTO> partialMoratoriums = getMoratoriums(context, false, false);

        // Iterate over partial moratoriums to update moratorium column
        for (MoratoriumDTO moratorium : partialMoratoriums) {

            // Find all pmfms corresponding to moratorium
            Set<Integer> pmfmIds = moratorium.getPmfms().stream()
                .flatMap(moratoriumPmfm -> pmfmDao.findPmfms(
                        moratoriumPmfm.getParameterCode(),
                        moratoriumPmfm.getMatrixId(),
                        moratoriumPmfm.getFractionId(),
                        moratoriumPmfm.getMethodId(),
                        moratoriumPmfm.getUnitId(),
                        null,
                        StatusFilter.ALL.toStatusCodes())
                    .stream())
                .map(PmfmDTO::getId)
                .collect(Collectors.toSet());

            if (context.getPmfmInfos().stream().noneMatch(pmfmInfo -> pmfmIds.contains(pmfmInfo.getPmfmId()))) {
                // Skip this moratorium because none of the pmfm ids are extracted
                continue;
            }

            for (MoratoriumPeriodDTO period : moratorium.getPeriods()) {
                XMLQuery xmlQuery = createXMLQuery("updatePartialMoratorium");
                xmlQuery.bind("tableName", context.getResultTableName());
                xmlQuery.bind("underMoratorium", t("reefdb.service.extraction.moratorium.partial"));
                xmlQuery.bind("programCode", moratorium.getProgramCode());
                xmlQuery.bind("startDate", formatDate(period.getStartDate()));
                xmlQuery.bind("endDate", formatDate(period.getEndDate()));
                addMoratoriumFilter(context, xmlQuery, moratorium);
                addPartialMoratoriumFilter(xmlQuery, moratorium);

                nbUpdates += execute(xmlQuery);
            }
        }

        // Update remaining rows
        XMLQuery xmlQuery = createXMLQuery("updateNoMoratorium");
        xmlQuery.bind("tableName", context.getResultTableName());
        xmlQuery.bind("underMoratorium", t("reefdb.service.extraction.moratorium.no"));
        execute(xmlQuery);

        return nbUpdates;
    }

    private void addMoratoriumFilter(ExtractionContextDTO context, XMLQuery xmlQuery, MoratoriumDTO moratorium) {

        // Check if user is program recorder
        ProgramDTO program = getProgram(context, moratorium.getProgramCode());
        boolean isRecorder = ReefDbBeans.isProgramRecorderOrViewerOnly(program, dataContext.getRecorderPersonId(), dataContext.getRecorderDepartmentId());
        List<Integer> recorderDepartmentIds = new ArrayList<>();
        if (isRecorder) {
            recorderDepartmentIds.add(dataContext.getRecorderDepartmentId());
            List<Integer> inheritedDepartmentIds = departmentDao.getInheritedRecorderDepartmentIds(dataContext.getRecorderDepartmentId());
            if (CollectionUtils.isNotEmpty(inheritedDepartmentIds)) {
                recorderDepartmentIds.addAll(inheritedDepartmentIds);
            }
        }

        // Recorder department filter
        if (CollectionUtils.isNotEmpty(recorderDepartmentIds)) {
            xmlQuery.setGroup("recorderDepartmentFilter", true);
            xmlQuery.bind("recorderDepartmentIds", Daos.getInStatementFromIntegerCollection(recorderDepartmentIds));
        } else {
            xmlQuery.setGroup("recorderDepartmentFilter", false);
        }

    }

    private void addPartialMoratoriumFilter(XMLQuery xmlQuery, MoratoriumDTO moratorium) {

        // Moratorium optional components filters
        xmlQuery.setGroup("partialFilter", false);

        if (CollectionUtils.isNotEmpty(moratorium.getLocationIds())) {
            xmlQuery.setGroup("partialFilter", true);
            xmlQuery.setGroup("location", true);
            xmlQuery.bind("locationIds", Daos.getInStatementFromIntegerCollection(moratorium.getLocationIds()));
        } else {
            xmlQuery.setGroup("location", false);
        }

        if (CollectionUtils.isNotEmpty(moratorium.getCampaignIds())) {
            xmlQuery.setGroup("partialFilter", true);
            xmlQuery.setGroup("campaign", true);
            xmlQuery.bind("campaignIds", Daos.getInStatementFromIntegerCollection(moratorium.getCampaignIds()));
        } else {
            xmlQuery.setGroup("campaign", false);
        }

        if (CollectionUtils.isNotEmpty(moratorium.getOccasionIds())) {
            xmlQuery.setGroup("partialFilter", true);
            xmlQuery.setGroup("occasion", true);
            xmlQuery.bind("occasionIds", Daos.getInStatementFromIntegerCollection(moratorium.getOccasionIds()));
        } else {
            xmlQuery.setGroup("occasion", false);
        }
    }

    private void writeExtraction(ExtractionContextDTO context, ExtractionOutputType outputType,
                                 Map<String, String> fieldNamesByAlias,
                                 Map<String, String> decimalFormats,
                                 Map<String, String> dateFormats,
                                 File outputFile)
        throws IOException {

        XMLQuery xmlQuery = createXMLQuery("selectResultTable");
        xmlQuery.bind("resultTableName", context.getResultTableName());

        // Ignore some Fields
        List<String> fieldsToIgnore = new ArrayList<>();
        fieldsToIgnore.add("SURVEY_ID");
        fieldsToIgnore.add("REC_DEP_ID");
        fieldsToIgnore.add("CAMPAIGN_ID");
        fieldsToIgnore.add("OCCAS_ID");
        if (outputType != ExtractionOutputType.PAMPA)
            fieldsToIgnore.add("SAMPLING_OPER_ID");
        if (outputType != ExtractionOutputType.COMPLETE)
            fieldsToIgnore.add("MEAS_INDIV_ID");

        prepare(xmlQuery, fieldNamesByAlias, decimalFormats, dateFormats);

        // write result
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("write result into file : %s", outputFile.getAbsolutePath()));
        }
        extractionResultDao.dumpQueryToCSV(outputFile, xmlQuery.getSQLQueryAsString(), fieldNamesByAlias, dateFormats, decimalFormats, fieldsToIgnore);
    }

    private void setGroups(XMLQuery xmlQuery, ExtractionOutputType outputType) {
        for (ExtractionOutputType type : ExtractionOutputType.values()) {
            xmlQuery.setGroup(type.name().toLowerCase(), type == outputType);
        }
    }

    private void createConcatDistinctFunction(String functionName, String valueType) {

        dropConcatDistinctFunction(functionName);

        // new function with external java method
        String query = "CREATE FUNCTION CONCAT_DISTINCT_" + functionName + "(IN_ARRAY " + valueType + " ARRAY, SEPARATOR VARCHAR(10)) RETURNS LONGVARCHAR " +
            "LANGUAGE JAVA DETERMINISTIC NO SQL EXTERNAL NAME 'CLASSPATH:fr.ifremer.reefdb.dto.ReefDbBeans.getUnifiedSQLString';";

        extractionResultDao.queryUpdate(query, null);

    }

    private void dropConcatDistinctFunction(String functionName) {

        extractionResultDao.queryUpdate("DROP FUNCTION CONCAT_DISTINCT_" + functionName + " IF EXISTS", null);
    }

    private void createDistinctFunction(String functionName, String valueType) {

        dropDistinctFunction(functionName);

        // new function with external java method
        String query = "CREATE FUNCTION DISTINCT_" + functionName + "(" +
            "IN_ARRAY " + valueType + " ARRAY, NON_DISTINCT_RESULT LONGVARCHAR) RETURNS LONGVARCHAR " +
            "LANGUAGE JAVA DETERMINISTIC NO SQL EXTERNAL NAME 'CLASSPATH:fr.ifremer.reefdb.dto.ReefDbBeans.getDistinctSQLString';";

        extractionResultDao.queryUpdate(query, null);

    }

    private void dropDistinctFunction(String functionName) {

        extractionResultDao.queryUpdate("DROP FUNCTION DISTINCT_" + functionName + " IF EXISTS", null);
    }

    private String getAliasedFields(Collection<ExtractionPmfmInfoDTO> pmfmInfos, final String fieldName) {
        return pmfmInfos.isEmpty()
            ? "NULL"
            : pmfmInfos.stream().map(pmfmInfo -> String.format("%s.%s", pmfmInfo.getAlias(), fieldName)).collect(Collectors.joining(","));
    }

    private String getOrderItemTypeCode(ExtractionDTO extraction) {
        FilterDTO orderItemTypeFilter = ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.ORDER_ITEM_TYPE);
        Assert.notNull(orderItemTypeFilter);
        //noinspection ConstantConditions
        Assert.notEmpty(orderItemTypeFilter.getElements());
        Assert.equals(orderItemTypeFilter.getElements().size(), 1);

        GroupingTypeDTO groupingType = (GroupingTypeDTO) orderItemTypeFilter.getElements().get(0);
        Assert.notNull(groupingType);
        Assert.notBlank(groupingType.getCode());
        return groupingType.getCode();
    }

    private String getNumericFormat(PmfmDTO pmfm) {

        // fixme: the pmfm definition is incomplete
        return getDefaultNumericFormat();

//        if (pmfm.getMaxDecimals() == null) {
//            return getDefaultNumericFormat();
//        }
//
//        // format with max decimals from pmfm definition (decimal 0 is appended)
//        return "#." + StringUtils.repeat("0", pmfm.getMaxDecimals());
    }

    private String getDefaultNumericFormat() {
        return "#." + StringUtils.repeat("#", 8); // 8 decimals should be enough by default (decimal 0 is erased)
    }

    private XMLQuery createXMLQuery(String queryName) {
        XMLQuery query = ReefDbServiceLocator.instance().getService("XMLQuery", XMLQuery.class);
        query.setQuery(getXMLQueryFile(queryName));
        return query;
    }

    private URL getXMLQueryFile(String queryName) {
        URL fileURL = getClass().getClassLoader().getResource(XML_QUERY_PATH + "/" + queryName + ".xml");
        if (fileURL == null)
            throw new ReefDbTechnicalException(String.format("query '%s' not found in resources", queryName));
        return fileURL;
    }

    private void prepare(XMLQuery xmlQuery, Map<String, String> fieldNamesByAlias, Map<String, String> decimalFormats, Map<String, String> dateFormats) {

        xmlQuery.getFirstQueryTag().getChildren(XMLQuery.TAG_SELECT).forEach(select -> {

            // Compute alias i18n key
            String alias = select.getAttributeValue(XMLQuery.ATTR_ALIAS);
            fieldNamesByAlias.putIfAbsent(alias, t("reefdb.service.extraction.fieldName." + alias));

            String type = select.getAttributeValue(XMLQuery.ATTR_TYPE);
            if (XMLQuery.TYPE_DATE.equalsIgnoreCase(type)) {
                dateFormats.putIfAbsent(alias, "dd/MM/yyyy");
            } else if (XMLQuery.TYPE_NUMBER.equalsIgnoreCase(type)) {
                decimalFormats.putIfAbsent(alias, getDefaultNumericFormat());
            }

        });
    }

    private int execute(XMLQuery xmlQuery) {
        return extractionResultDao.queryUpdate(xmlQuery.getSQLQueryAsString());
    }

    private long countFrom(String tableName) {
        XMLQuery xmlQuery = createXMLQuery("countFrom");
        xmlQuery.bind("tableName", tableName);
        return extractionResultDao.queryCount(xmlQuery.getSQLQueryAsString());
    }

    private String formatDate(LocalDate date) {
        return Dates.formatDate(date, "dd/MM/yyyy");
    }

    private List<String> getProgramCodes(ExtractionContextDTO context) {
        return ReefDbBeans.transformCollection(context.getPrograms(), ProgramDTO::getCode);
    }

    private ProgramDTO getProgram(ExtractionContextDTO context, String programCode) {
        return context.getPrograms().stream().filter(program -> program.getCode().equals(programCode)).findFirst().orElseThrow(NullPointerException::new);
    }

    private List<MoratoriumDTO> getMoratoriums(ExtractionContextDTO context, boolean global, boolean activeForCurrentUser) {

        return context.getPrograms().stream()
            .filter(program -> !program.isMoratoriumsEmpty())
            // if only active for current user, filter by user privilege
            .filter(program -> !activeForCurrentUser
                || (!dataContext.isRecorderAdministrator() && ReefDbBeans.isProgramRecorderOrViewerOnly(program, dataContext.getRecorderPersonId(), dataContext.getRecorderDepartmentId()))
            )
            .flatMap(program -> program.getMoratoriums().stream())
            .filter(moratorium -> moratorium.isGlobal() == global)
            // A moratorium period overlaps an extraction period
            .filter(moratorium ->
                moratorium.getPeriods().stream()
                    .anyMatch(moratoriumPeriod -> context.getPeriods().stream()
                        .anyMatch(extractionPeriod ->
                            !extractionPeriod.getStartDate().isAfter(moratoriumPeriod.getEndDate()) && !extractionPeriod.getEndDate().isBefore(moratoriumPeriod.getStartDate())                            ))
            )
            .collect(Collectors.toList());
    }
}
