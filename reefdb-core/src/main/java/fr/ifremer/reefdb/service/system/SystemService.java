package fr.ifremer.reefdb.service.system;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.reefdb.dto.*;
import fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Le sevice pour les donnees de type Survey.
 */
@Transactional(readOnly = true)
public interface SystemService {
	
	/**
	 * La listes des etats
	 *
	 * @return Les etats
	 */
	List<StateDTO> getStates();
	
	/**
	 * La liste des libelles pour la recherche d'une date (entre, avant, apres).
	 *
	 * @return La liste des valeurs
	 */
	List<SearchDateDTO> getSearchDates();
	
	/**
	 * La liste des valeurs possible pour un Boolean.
	 *
	 * @return Liste des BooleanDTO
	 */
	List<BooleanDTO> getBooleanValues();
	
	/**
	 * Get synchronization status
	 *
	 * @return list of all synchronization status
	 * @param withReadyToSyncStatus a boolean.
	 */
	List<SynchronizationStatusDTO> getAllSynchronizationStatus(boolean withReadyToSyncStatus);

	/**
	 * Recuperation du partage local pour les nouvelles observations
	 *
	 * @return le partage local
	 */
	SynchronizationStatusDTO getLocalShare();
	
	/**
	 * Recherches les fonctions possible pour une regle de controle
	 *
	 * @return La liste des fonction
	 */
	List<FunctionDTO> getFunctionsControlSystem();
	
	/**
	 * Recherche la liste des elements a controle pour une regle de controle
	 *
	 * @return La liste des elements a controler
	 */
	List<ControlElementDTO> getControlElements();
	
	/**
	 * Recherche la liste des caracteristiques pour une regle de controle
	 *
	 * @param elementControl Element control
	 * @return la liste des caracteristiques a controler
	 */
	List<ControlFeatureDTO> getControlFeatures(ControlElementDTO elementControl);

	List<MonthDTO> getMonths();

	void clearCaches();

}
