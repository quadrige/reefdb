package fr.ifremer.reefdb.service.system;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.dao.system.synchronization.SynchronizationStatus;
import fr.ifremer.quadrige3.ui.core.dto.MonthDTO;
import fr.ifremer.quadrige3.ui.core.dto.month.MonthValues;
import fr.ifremer.reefdb.dto.*;
import fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import fr.ifremer.reefdb.dto.enums.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * SystemService implementation.
 */
@Service("reefdbSystemService")
public class SystemServiceImpl implements SystemService {
	
	/**
	 * BooleanDTO list.
	 */
	private List<BooleanDTO> booleanList = null;
	
	/**
	 * SynchronizationStatusDTO list.
	 */
	private List<SynchronizationStatusDTO> synchronizationStatusList = null;
	
	/**
	 * FunctionDTO list.
	 */
	private List<FunctionDTO> functionControlList = null;
	
	/**
	 * ControlElementDTO list.
	 */
	private List<ControlElementDTO> elementControlList = null;
	
	/**
	 * ControlFeatureDTO measures list.
	 */
	private List<ControlFeatureDTO> featureControlMeasureResultList = null;
	
	/**
	 * ControlFeatureDTO observation list.
	 */
	private List<ControlFeatureDTO> featureControlObservationList = null;
	
	/**
	 * ControlFeatureDTO Prelevement list.
	 */
	private List<ControlFeatureDTO> featureControlSamplingOperationList = null;
	
	/**
	 * SearchDateDTO list.
	 */
	private List<SearchDateDTO> searchDateList = null;
	
	/**
	 * EtatDTO list.
	 */
	private List<StateDTO> stateList = null;

	private List<MonthDTO> monthList = null;

	/** {@inheritDoc} */
	@Override
	public List<SearchDateDTO> getSearchDates() {
		
		// If list not exist, create it
		if (searchDateList == null) {
			searchDateList = Arrays.stream(SearchDateValues.values()).map(SearchDateValues::toSearchDateDTO).collect(Collectors.toList());
		}
		return searchDateList;
	}
	
	/** {@inheritDoc} */
	@Override
	public List<StateDTO> getStates() {
		
		// If list not exist, create it
		if (stateList == null) {
			stateList = Arrays.stream(StateValues.values()).map(StateValues::toStateDTO).collect(Collectors.toList());
		}
		return stateList;
	}
	
	/** {@inheritDoc} */
	@Override
	public List<BooleanDTO> getBooleanValues() {
		
		// If list not exist, create it
		if (booleanList == null) {
			booleanList = Arrays.stream(BooleanValues.values()).map(BooleanValues::toBooleanDTO).collect(Collectors.toList());
		}
		return booleanList;
	}
	
	/** {@inheritDoc} */
	@Override 
	public SynchronizationStatusDTO getLocalShare() {
		return SynchronizationStatusValues.toSynchronizationStatusDTO(SynchronizationStatus.DIRTY.getValue());
	}
	
	/** {@inheritDoc} */
	@Override
	public List<SynchronizationStatusDTO> getAllSynchronizationStatus(boolean withReadyToSyncStatus) {
		
		// If list not exist, create it
		if (synchronizationStatusList == null) {
			synchronizationStatusList = Arrays.stream(SynchronizationStatusValues.values())
					.filter(value -> withReadyToSyncStatus || value != SynchronizationStatusValues.READY_TO_SYNCHRONIZE)
					.map(SynchronizationStatusValues::toSynchronizationStatusDTO)
					.collect(Collectors.toList());
		}
		return synchronizationStatusList;
	}

	/** {@inheritDoc} */
	@Override
	public List<FunctionDTO> getFunctionsControlSystem() {
		
		// If list not exist, create it
		if (functionControlList == null) {
			functionControlList = Arrays.stream(ControlFunctionValues.values()).map(ControlFunctionValues::toFunctionDTO).collect(Collectors.toList());
		}
		return functionControlList;
	}
	
	/** {@inheritDoc} */
	@Override
	public List<ControlElementDTO> getControlElements() {
		
		// If list not exist, create it
		if (elementControlList == null) {
			elementControlList = Arrays.stream(ControlElementValues.values()).map(ControlElementValues::toControlElementDTO).collect(Collectors.toList());
		}
		return elementControlList;
	}
	
	/** {@inheritDoc} */
	@Override
	public List<ControlFeatureDTO> getControlFeatures(final ControlElementDTO controlElementDTO) {
		
		// Element control
		final ControlElementValues elementControl = ControlElementValues.getByCode(controlElementDTO.getCode());
		List<ControlFeatureDTO> features = new ArrayList<>();
        
        if (elementControl == null) {
            return features;
        }
        
        switch (elementControl) {
			case MEASUREMENT:
				return getFeatureControlMeasureResultList();
			case SURVEY:
				return getFeatureControlObservationList();	
			case SAMPLING_OPERATION:
				return getFeatureControlSamplingOperationList();
			default:
				return features;
		}
        
	}

	@Override
	public List<MonthDTO> getMonths() {
		// If list not exist, create it
		if (monthList == null) {
			monthList = Arrays.stream(MonthValues.values()).map(MonthValues::toDTO).collect(Collectors.toList());
		}
		return monthList;
	}

	@Override
	public void clearCaches() {
		booleanList = null;
		synchronizationStatusList = null;
		functionControlList = null;
		elementControlList = null;
		featureControlMeasureResultList = null;
		featureControlObservationList = null;
		featureControlSamplingOperationList = null;
		searchDateList = null;
		stateList = null;
	}

	/**
	 * Only one list of counting.
	 * @return ControlFeatureDTO measures list
	 */
	private List<ControlFeatureDTO> getFeatureControlMeasureResultList() {
		
		// If list not exist, create it
		if (featureControlMeasureResultList == null) {
			featureControlMeasureResultList = Arrays.stream(ControlFeatureMeasurementValues.values()).map(ControlFeatureMeasurementValues::toControlFeatureDTO).collect(Collectors.toList());
		}
		return featureControlMeasureResultList;
	}
	
	/**
	 * Only one list of counting.
	 * @return ControlFeatureDTO observation list
	 */
	private List<ControlFeatureDTO> getFeatureControlObservationList() {
		
		// If list not exist, create it
		if (featureControlObservationList == null) {
			featureControlObservationList = Arrays.stream(ControlFeatureSurveyValues.values()).map(ControlFeatureSurveyValues::toControlFeatureDTO).collect(Collectors.toList());
		}
		return featureControlObservationList;
	}
	
	/**
	 * Only one list of counting.
	 * @return ControlFeatureDTO prelevement list
	 */
	private List<ControlFeatureDTO> getFeatureControlSamplingOperationList() {
		
		// If list not exist, create it
		if (featureControlSamplingOperationList == null) {
			featureControlSamplingOperationList = Arrays.stream(ControlFeatureSamplingOperationValues.values()).map(ControlFeatureSamplingOperationValues::toControlFeatureDTO).collect(Collectors.toList());
		}
		return featureControlSamplingOperationList;
	}
}
