package fr.ifremer.reefdb.vo;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

/**
 * Proxy used to Serialize/Deserialize Contexts
 *
 * @author peck7 on 18/09/2019.
 */
public class ContextProxy implements Serializable {

    public static final String CURRENT_VERSION = "1.0";
    private final String version;
    private final List<? extends ContextVO> contexts;

    public ContextProxy(List<? extends ContextVO> contexts) {
        this.version = CURRENT_VERSION;
        this.contexts = contexts;
    }

    public final String getVersion() {
        return version;
    }

    public List<? extends ContextVO> getContexts() {
        return contexts;
    }

}
