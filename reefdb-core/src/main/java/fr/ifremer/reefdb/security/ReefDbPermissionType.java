package fr.ifremer.reefdb.security;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * <p>ReefDbPermissionType class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class ReefDbPermissionType {

    /** Constant <code>DELETE_OBSERVED_LOCATION="deleteObservedLocation"</code> */
    public static final String DELETE_OBSERVED_LOCATION = "deleteObservedLocation";
    /** Constant <code>REMOVE_LANDING_LINK_TO_FISHING_TRIP="removeLandingLinkToFishingTrip"</code> */
    public static final String REMOVE_LANDING_LINK_TO_FISHING_TRIP = "removeLandingLinkToFishingTrip";
    /** Constant <code>DELETE_LANDING="deleteLanding"</code> */
    public static final String DELETE_LANDING = "deleteLanding";
}
