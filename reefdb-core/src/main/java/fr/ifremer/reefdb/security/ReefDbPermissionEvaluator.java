package fr.ifremer.reefdb.security;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * <p>ReefDbPermissionEvaluator class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
@Service("reefDbPermissionEvaluator")
public class ReefDbPermissionEvaluator implements PermissionEvaluator {

//    @Autowired
//    @Lazy
//    LandingService landingService;

    /** {@inheritDoc} */
    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return hasPermission(authentication, targetDomainObject instanceof Serializable ? (Serializable) targetDomainObject : null, null, permission);
    }

    /** {@inheritDoc} */
    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {

        // basic permission
        if (!SecurityContextHelper.hasAuthority(authentication, permission)) {
            return false;
        }

        // specific permission
        // TODO mieux gérer ce test en fonction des cas spécifiques
/*        if (StringUtils.isNotBlank(targetType) && targetId != null) {

            boolean result = false;

            switch (targetType) {
                case ReefDbPermissionType.DELETE_OBSERVED_LOCATION: {
                    // no specific restriction because authenticated user has passed the basic permission evaluator
                    result = true;
                }
                break;

                case ReefDbPermissionType.DELETE_LANDING:
                case ReefDbPermissionType.REMOVE_LANDING_LINK_TO_FISHING_TRIP: {

//                if (SecurityContextHelper.hasAuthority(authentication, ReefDbAuthority.SUPERUSER)
//                    || (SecurityContextHelper.hasAuthority(authentication, ReefDbAuthority.USER)
//                    && !landingService.isLandingHasData((Integer) targetId))) {
//                    result = true;
//                }

                }
                break;

            }

            return result;
        }*/

        return true;
    }

}
