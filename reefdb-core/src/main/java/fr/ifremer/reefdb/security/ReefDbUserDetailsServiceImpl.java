package fr.ifremer.reefdb.security;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.administration.user.PrivilegeCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.security.Encryption;
import fr.ifremer.quadrige3.core.security.QuadrigeUserDetails;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>ReefDbUserDetailsServiceImpl class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
@Lazy
@Transactional
@Service("reefDbUserDetailsService")
public class ReefDbUserDetailsServiceImpl implements UserDetailsService {

    private static final Log LOG = LogFactory.getLog(ReefDbUserDetailsServiceImpl.class);

    private boolean disabled;

    private final String mockUsername;
    private final String mockCryptPassword;
    private final Integer mockUserId;

    @Resource
    protected ReefDbQuserDao reefDbQuserDao;

    /**
     * <p>Constructor for ReefDbUserDetailsServiceImpl.</p>
     *
     * @param config a {@link fr.ifremer.reefdb.config.ReefDbConfiguration} object.
     */
    @Autowired
    public ReefDbUserDetailsServiceImpl(ReefDbConfiguration config) {
        disabled = config.isAuthenticationDisabled();

        // If authentication is disable
        if (disabled) {
            Assert.notBlank(config.getAuthenticationMockUsername(),
                    "Mock username must be set, when authentication is disable");
            Assert.notBlank(config.getAuthenticationMockPassword(),
                    "Mock username must be set, when authentication is disable");

            mockUsername = config.getAuthenticationMockUsername();
            mockCryptPassword = Encryption.sha(config.getAuthenticationMockPassword());
            mockUserId = config.getAuthenticationMockUserId();

            LOG.debug(String.format("Authentication disable. Only this login/password is allowed: [%s/%s]",
                    mockUsername,
                    config.getAuthenticationMockPassword()));
        } else {
            mockUsername = null;
            mockCryptPassword = null;
            mockUserId = null;
        }
    }

    /**
     * <p>Setter for the field <code>disabled</code>.</p>
     *
     * @param disabled a boolean.
     */
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;

        if (disabled) {
            LOG.warn("Authentication has been disabled by the configuration. Please contact your administrator to ask the credentials for test.");
        }
    }

    /** {@inheritDoc} */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if (StringUtils.isBlank(username)) {
            String error = "username must not be blank";
            LOG.error(error);
            throw new UsernameNotFoundException(error);
        }

        if (disabled) {
            return loadTestUser(username);
        }

        PersonDTO person;
        try {
            // Only allowed user with status enable (or local enable)
            person = reefDbQuserDao.getUserByLogin(Lists.newArrayList(
                    StatusCode.ENABLE.value(),
                    StatusCode.LOCAL_ENABLE.value()),
                    username);
        } catch (Exception e) {
            // Log here, because Spring security is in silent mode
            LOG.error(e.getMessage(), e);
            throw new ReefDbTechnicalException(e);
        }

        if (person == null) {
            String error = I18n.t("reefdb.error.authentication.userNotFoundOrDisable", username);
            LOG.error(error);
            throw new UsernameNotFoundException(error);
        }

        String cryptedPassword = reefDbQuserDao.getPasswordByUserId(person.getId());
        List<String> privilegeCodes = reefDbQuserDao.getPrivilegeCodesByUserId(person.getId());
        boolean localUser = TemporaryDataHelper.isTemporaryId(person.getId()) || ReefDbBeans.isLocalStatus(person.getStatus());

        return new QuadrigeUserDetails(person.getId(), person.getFirstName() + " " + person.getName(), cryptedPassword, privilegeCodes, localUser);
    }

    private UserDetails loadTestUser(String username) throws UsernameNotFoundException {

        if (!mockUsername.equals(username)) {
            String error = String.format("Bad username [%s]. Only [%s] is allow, because authentication is disable.", username, mockUsername);
            LOG.error(error);
            throw new UsernameNotFoundException(error);
        }

        PersonDTO person = ReefDbBeanFactory.newPersonDTO();
        person.setId(mockUserId);
        person.setFirstName(mockUsername);
        person.setName("ReefDb");
        person.setDepartment(null);

        // add user privileges
        List<String> privilegesCodes = reefDbQuserDao.getPrivilegeCodesByUserId(person.getId());

        // add other privileges (useful for unit testing)
        if (!privilegesCodes.contains(PrivilegeCode.LOCAL_ADMINISTRATOR.value()))
            privilegesCodes.add(PrivilegeCode.LOCAL_ADMINISTRATOR.value());
        if (!privilegesCodes.contains(PrivilegeCode.QUALIFIER.value()))
            privilegesCodes.add(PrivilegeCode.QUALIFIER.value());

        return new QuadrigeUserDetails(mockUserId, mockUsername, mockCryptPassword, privilegesCodes, TemporaryDataHelper.isTemporaryId(mockUserId));
    }
}
