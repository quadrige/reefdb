package fr.ifremer.reefdb.dao.system.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingItemExtendDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.csv.CSVDaoImpl;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * Dao for extraction with JDBC access on spring data source
 * <p/>
 * Created by Ludovic on 11/12/2015.
 */
@Repository("reefDbExtractionResultDao")
@Lazy
public class ReefDbExtractionResultDaoImpl extends CSVDaoImpl implements ReefDbExtractionResultDao {

    private static final Log log = LogFactory.getLog(ReefDbExtractionResultDaoImpl.class);

    protected final Properties connectionProperties;

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "transcribingItemDao")
    private TranscribingItemExtendDao transcribingItemDao;

    /**
     * <p>Constructor for ReefDbExtractionResultDaoImpl.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     */
    @Autowired
    public ReefDbExtractionResultDaoImpl(DataSource dataSource) {
        super(dataSource);
        this.connectionProperties = null;
    }

    /**
     * <p>Constructor for ReefDbExtractionResultDaoImpl.</p>
     */
    public ReefDbExtractionResultDaoImpl() {
        this((Properties) null);
    }

    /**
     * <p>Constructor for ReefDbExtractionResultDaoImpl.</p>
     *
     * @param connectionProperties a {@link java.util.Properties} object.
     */
    public ReefDbExtractionResultDaoImpl(Properties connectionProperties) {
        super();
        this.connectionProperties = connectionProperties;
    }

    @Override
    public long queryCount(String query) {
        return queryCount(query, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long queryCount(String query, Map<String, Object> queryBindings) {
        Long count = queryCount(connectionProperties, query, queryBindings);
        if (count == null) throw new DataRetrievalFailureException(String.format("query count result is null: %s ; bindings=%s", query, queryBindings));
        return count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> List<T> query(String sql, Map<String, Object> queryBindings, RowMapper<T> rowMapper) {
        return query(connectionProperties, sql, queryBindings, rowMapper);
    }

    @Override
    public int queryUpdate(String query) {
        return queryUpdate(query, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int queryUpdate(String query, Map<String, Object> paramMap) {
        return queryUpdate(connectionProperties, query, paramMap);
    }

    @Override
    public String getPmfmNameForExtraction(PmfmDTO pmfm) {
        Assert.notNull(pmfm);
        return Optional
            .ofNullable(transcribingItemDao.getTranscribingItemById(config.getTranscribingItemTypeLbForPmfmExtraction(), pmfm.getId()))
            .orElse(
                Optional.ofNullable(transcribingItemDao.getTranscribingItemById(config.getTranscribingItemTypeLbForPmfmNm(), pmfm.getId()))
                    .orElse(
                        Optional.ofNullable(pmfm.getName())
                            .orElse(pmfm.getParameter().getName())
                    )
            );    }

    @Override
    public String getPmfmUnitNameForExtraction(PmfmDTO pmfm) {
        Assert.notNull(pmfm);
        return pmfm.getUnit() == null || config.getExtractionUnitIdsToIgnore().contains(pmfm.getUnit().getId()) ? "" : pmfm.getUnit().getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> queryIntegerList(String query, Map<String, Object> queryBindings) {
        return query(connectionProperties, query, queryBindings, (resultSet, i) -> resultSet.getInt(1));
    }

    @Override
    public List<String> queryStringList(String query, Map<String, Object> queryBindings) {
        return query(connectionProperties, query, queryBindings, (resultSet, i) -> resultSet.getString(1));
    }

}
