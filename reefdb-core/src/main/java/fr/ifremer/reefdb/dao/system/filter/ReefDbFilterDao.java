package fr.ifremer.reefdb.dao.system.filter;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.filter.Filter;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>ReefDbFilterDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbFilterDao {

    String ALL_FILTERS_CACHE = "all_filters";
    String FILTER_BY_ID_CACHE = "filter_by_id";
    String FILTERED_ELEMENTS_BY_FILTER_ID_CACHE = "filtered_elements_by_filter_id";

    /**
     * <p>getAllContextFilters.</p>
     *
     * @param contextId a {@link java.lang.Integer} object.
     * @param filterTypeId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_FILTERS_CACHE)
    List<FilterDTO> getAllContextFilters(Integer contextId, Integer filterTypeId);

    /**
     * <p>getAllExtractionFilters.</p>
     *
     * @param filterTypeId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<FilterDTO> getAllExtractionFilters(Integer filterTypeId);

    /**
     * <p>getFilterById.</p>
     *
     * @param filterId a {@link java.lang.Integer} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     */
    @Cacheable(value = FILTER_BY_ID_CACHE)
    FilterDTO getFilterById(Integer filterId);

    /**
     * <p>getFilteredElementsByFilterId.</p>
     *
     * @param filterId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = FILTERED_ELEMENTS_BY_FILTER_ID_CACHE)
    List<String> getFilteredElementsByFilterId(Integer filterId);

    /**
     * <p>saveFilter.</p>
     *
     * @param filter a {@link fr.ifremer.reefdb.dto.configuration.filter.FilterDTO} object.
     * @param quserId a int.
     * @return a {@link fr.ifremer.quadrige3.core.dao.system.filter.Filter} object.
     */
    @CacheEvict(value = {
            ALL_FILTERS_CACHE,
            FILTER_BY_ID_CACHE,
            FILTERED_ELEMENTS_BY_FILTER_ID_CACHE}, allEntries = true)
    Filter saveFilter(FilterDTO filter, int quserId);

    /**
     * <p>deleteFilters.</p>
     *
     * @param filterIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_FILTERS_CACHE,
            FILTER_BY_ID_CACHE,
            FILTERED_ELEMENTS_BY_FILTER_ID_CACHE}, allEntries = true)
    void deleteFilters(List<Integer> filterIds);

    /**
     * <p>checkFiltersNotUsedInContext.</p>
     *
     * @param filterIds a {@link java.util.List} object.
     * @return a boolean.
     */
    boolean checkFiltersNotUsedInContext(List<Integer> filterIds);

}
