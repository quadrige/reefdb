package fr.ifremer.reefdb.dao.administration.strategy;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramImpl;
import fr.ifremer.quadrige3.core.dao.administration.strategy.*;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.PmfmImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeanComparator;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.referential.pmfm.ReefDbPmfmDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.*;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ReefDbStrategyDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbStrategyDao")
public class ReefDbStrategyDaoImpl extends StrategyDaoImpl implements ReefDbStrategyDao {

    private static final Log log = LogFactory.getLog(ReefDbStrategyDaoImpl.class);
    private static final String DATE_PATTERN = "yyyyMMdd";

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource()
    protected PmfmStrategyDaoImpl pmfmStrategyDao;

    @Resource
    protected ReefDbConfiguration config;

    /**
     * <p>Constructor for ReefDbStrategyDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbStrategyDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<StrategyDTO> getStrategiesByProgramCode(String programCode) {

        Iterator<Object[]> rows = queryIterator("strategiesByProgramCode",
                "programCode", StringType.INSTANCE, programCode);

        List<StrategyDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            Object[] row = rows.next();
            result.add(toStrategyDTO(Arrays.asList(row).iterator()));
        }

        return ImmutableList.copyOf(result);

    }

    /** {@inheritDoc} */
    @Override
    public List<AppliedStrategyDTO> getAppliedStrategiesByProgramCode(String programCode) {

        Iterator<Object[]> rows = queryIterator("appliedStrategiesByProgramCode",
                "programCode", StringType.INSTANCE, programCode);

        List<AppliedStrategyDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            Iterator<Object> row = Arrays.asList(rows.next()).iterator();
            result.add(toAppliedStrategyDTO(row));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<AppliedStrategyDTO> getAppliedStrategiesByStrategyId(Integer strategyId) {

        Iterator<Object[]> rows = queryIterator("appliedStrategiesByStrategyId",
                "strategyId", IntegerType.INSTANCE, strategyId);

        List<AppliedStrategyDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            Iterator<Object> row = Arrays.asList(rows.next()).iterator();
            result.add(toAppliedStrategyDTO(row));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<AppliedStrategyDTO> getAppliedPeriodsByStrategyId(Integer strategyId) {

        Iterator<Object[]> rows = queryIterator("appliedPeriodsByStrategyId",
                "strategyId", IntegerType.INSTANCE, strategyId);

        TimeZone dbTimezone = config.getDbTimezone();
        List<AppliedStrategyDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            Iterator<Object> row = Arrays.asList(rows.next()).iterator();
            result.add(toAppliedPeriod(row, dbTimezone));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public Multimap<Integer, AppliedStrategyDTO> getAllAppliedPeriodsByProgramCode(String programCode) {

        Iterator<Object[]> rows = queryIterator("allAppliedPeriodsByProgramCode",
                "programCode", StringType.INSTANCE, programCode);

        Multimap<Integer, AppliedStrategyDTO> result = ArrayListMultimap.create();
        TimeZone dbTimezone = config.getDbTimezone();
        while (rows.hasNext()) {
            Iterator<Object> row = Arrays.asList(rows.next()).iterator();
            Integer strategyId = (Integer) row.next();
            AppliedStrategyDTO appliedStrategy = toAppliedPeriod(row, dbTimezone);
            result.put(strategyId, appliedStrategy);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmStrategyDTO> getPmfmsAppliedStrategy(Integer strategyId) {

        Iterator<Object[]> rows = queryIterator("pmfmsAppliedStrategyByStrategyId",
                "strategyId", IntegerType.INSTANCE, strategyId);

        List<PmfmStrategyDTO> result = Lists.newArrayList();
        PmfmStrategyDTO previousDTO = null;
        while (rows.hasNext()) {
            Iterator<Object> row = Arrays.asList(rows.next()).iterator();
            PmfmStrategyDTO currentDTO = toPmfmStrategyDTO(row);
            if (previousDTO != null) {
                // same pmfm
                if (Objects.equals(previousDTO, currentDTO)) {
                    currentDTO = previousDTO;
                }
                // new pmfm detected
                else {
                    result.add(previousDTO);
                    previousDTO = currentDTO;
                }
            } else {
                previousDTO = currentDTO;
            }

            String acquisitionLevelCode = (String) row.next();
            currentDTO.setSurvey(BooleanUtils.toBoolean(currentDTO.isSurvey())
                    || AcquisitionLevelCode.SURVEY.getValue().equals(acquisitionLevelCode));
            currentDTO.setSampling(BooleanUtils.toBoolean(currentDTO.isSampling())
                    || AcquisitionLevelCode.SAMPLING_OPERATION.getValue().equals(acquisitionLevelCode));
        }

        if (previousDTO != null) {
            result.add(previousDTO);
        }

        // Get restricted list of qualitative values
        fillQualitativeValues(result);

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public Set<PmfmStrategyDTO> getPmfmStrategiesByProgramCodeAndLocation(String programCode, Integer monitoringLocationId, LocalDate date) {
        Assert.notBlank(programCode);
        Assert.notNull(monitoringLocationId);
        Assert.notNull(date);

        List<ProgStratDTO> strategies = getStrategiesByProgramCodeAndMonitoringLocationId(programCode, monitoringLocationId);
        Set<PmfmStrategyDTO> pmfmStrategies = Sets.newHashSet();

        for (ProgStratDTO strategy : strategies) {
            // also filter pmfm on date
            if (strategy.getStartDate() != null && strategy.getEndDate() != null && Dates.isBetween(date, strategy.getStartDate(), strategy.getEndDate())) {
                pmfmStrategies.addAll(getPmfmsAppliedStrategy(strategy.getId()));
            }
        }

        return ImmutableSet.copyOf(pmfmStrategies);
    }

    @Override
    public Set<PmfmStrategyDTO> getPmfmStrategiesByProgramCodesAndDates(Collection<String> programCodes, LocalDate startDate, LocalDate endDate) {
        Set<PmfmStrategyDTO> pmfmStrategies = new HashSet<>();

        List<ProgStratDTO> appliedStrategies = getAppliedStrategiesByProgramCodes(programCodes);
        for (ProgStratDTO appliedStrategy: appliedStrategies) {
            // filter on date range
            if (appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null
                && !appliedStrategy.getStartDate().isAfter(endDate) && !appliedStrategy.getEndDate().isBefore(startDate)) {
                // add all pmfm strategies in date range
                pmfmStrategies.addAll(getPmfmsAppliedStrategy(appliedStrategy.getId()));
            }
        }

        return ImmutableSet.copyOf(pmfmStrategies);

    }

    @SuppressWarnings("unchecked")
    private List<ProgStratDTO> getAppliedStrategiesByProgramCodes(Collection<String> programCodes) {
        Query query = createQuery("appliedStrategiesByProgramCodes").setParameterList("programCodes", programCodes);
        Iterator<Object[]> rows = query.iterate();

        List<ProgStratDTO> result = Lists.newArrayList();
        TimeZone dbTimezone = config.getDbTimezone();
        while (rows.hasNext()) {
            result.add(toProgStratDTO(Arrays.asList(rows.next()).iterator(), dbTimezone));
        }

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public List<ProgStratDTO> getStrategiesByProgramCodeAndMonitoringLocationId(String programCode, int monitoringLocationId) {
        Iterator<Object[]> rows = queryIterator("strategiesByProgramCodeAndMonitoringLocationId",
                "programCode", StringType.INSTANCE, programCode,
                "monitoringLocationId", IntegerType.INSTANCE, monitoringLocationId);

        List<ProgStratDTO> result = Lists.newArrayList();
        TimeZone dbTimezone = config.getDbTimezone();
        while (rows.hasNext()) {
            Iterator<Object> row = Arrays.asList(rows.next()).iterator();
            ProgStratDTO progStrat = toProgStratDTO(row, dbTimezone);

            result.add(progStrat);
        }

        return result;
    }

    @Override
    public DepartmentDTO getAnalysisDepartmentByAppliedStrategyId(int appliedStrategyId) {
        List<Integer> depIds = queryListTyped("analysisDepartmentByAppliedStrategyId",
                "appliedStrategyId", IntegerType.INSTANCE, appliedStrategyId);

        depIds = depIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(depIds)) {
            return null;
        } else if (depIds.size() > 1) {
            // the department should be unique
            log.warn(String.format("More than one analysis department %s for applied strategy %s", depIds, appliedStrategyId));
        }

        return departmentDao.getDepartmentById(depIds.get(0));
    }

    @Override
    @SuppressWarnings("unchecked")
    public DepartmentDTO getAnalysisDepartmentByAppliedStrategyIdAndPmfmIds(int appliedStrategyId, Collection<Integer> pmfmIds) {
        Query query = createQuery("analysisDepartmentByAppliedStrategyIdAndPmfmIds",
                "appliedStrategyId", IntegerType.INSTANCE, appliedStrategyId);
        query.setParameterList("pmfmIds", pmfmIds);
        List<Integer> depIds = (List<Integer>) query.list();

        depIds = depIds.stream().filter(Objects::nonNull).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(depIds)) {
            return null;
        } else if (depIds.size() > 1) {
            // the department should be unique
            log.warn(String.format("More than one analysis department %s for applied strategy %s and pmfms %s", depIds, appliedStrategyId, pmfmIds));
        }

        return departmentDao.getDepartmentById(depIds.get(0));
    }

    /** {@inheritDoc} */
    @Override
    public void saveStrategies(ProgramDTO program) {
        Assert.notNull(program);
        Assert.notBlank(program.getCode());

        List<Integer> remainingStrategieIds = queryListTyped("stratIdsByProgCd",
                "programCode", StringType.INSTANCE, program.getCode());

        for (StrategyDTO strategy : program.getStrategies()) {
            Strategy target = null;

            if (strategy.getId() != null) {
                target = get(strategy.getId());
            }

            boolean isNew = false;
            if (target == null) {
                target = Strategy.Factory.newInstance();
                Integer newId = TemporaryDataHelper.<Integer>getNewNegativeIdForTemporaryData(getSession(), target.getClass());
                target.setStratId(newId);
                target.setProgram(load(ProgramImpl.class, program.getCode()));
                target.setStratCreationDt(newCreateDate());
                isNew = true;
            } else {
                remainingStrategieIds.remove(target.getStratId());
            }

            // Fill entity from DTO
            strategyDTOToEntity(program, strategy, target);

            // Save entity
            if (isNew) {
                getSession().save(target);
                strategy.setId(target.getStratId());
            }
            else {
                getSession().update(target);
            }

            getSession().flush();

            TimeZone dbTimezone = config.getDbTimezone();

            // Save PmfmStrategies before AppliedStrategies
            if (strategy.isPmfmStrategiesLoaded() || !strategy.isPmfmStrategiesEmpty()) {
                if (!strategy.isPmfmStrategiesEmpty()) {

                    Map<Integer, PmfmStrategy> remainingPmfmStrategy = ReefDbBeans.mapByProperty(target.getPmfmStrategies(), "pmfmStratId");
                    int rank = 1;

                    for (PmfmStrategyDTO pmfmStrategyDTO : strategy.getPmfmStrategies()) {

                        PmfmStrategy pmfmStrategy = null;
                        if (pmfmStrategyDTO.getId() != null) {
                            pmfmStrategy = remainingPmfmStrategy.remove(pmfmStrategyDTO.getId());
                        }
                        if (pmfmStrategy == null) {
                            // create new PmfmStrategy
                            pmfmStrategy = PmfmStrategy.Factory.newInstance();
                            Integer newId = TemporaryDataHelper.<Integer>getNewNegativeIdForTemporaryData(getSession(), pmfmStrategy.getClass());
                            pmfmStrategy.setPmfmStratId(newId);
                            pmfmStrategy.setPmfmStratParAcquisNumber(1);
                        }

                        savePmfmStrategy(pmfmStrategyDTO, pmfmStrategy, rank++);
                        pmfmStrategy.setStrategy(target);
                        target.addPmfmStrategies(pmfmStrategy);
                        getSession().save(pmfmStrategy);
                        getSession().flush();
                        pmfmStrategyDTO.setId(pmfmStrategy.getPmfmStratId());
                        // Update PMFM_STRAT_PMFM_QUAL_VALUE
                        saveQualitativeValues(pmfmStrategyDTO);
                    }

                    // remove remaining pmfmStrategy
                    if (!remainingPmfmStrategy.isEmpty()) {
                        target.getPmfmStrategies().removeAll(remainingPmfmStrategy.values());
                        List<Integer> pmfmStrategyIds = ReefDbBeans.collectProperties(remainingPmfmStrategy.values(), "pmfmStratId");
                        deletePmfmStrategies(pmfmStrategyIds);
                    }

                } else {
                    if (CollectionUtils.isNotEmpty(target.getPmfmStrategies())) {
                        target.getPmfmStrategies().clear();
                        List<Integer> pmfmStrategyIds = ReefDbBeans.collectProperties(target.getPmfmStrategies(), "pmfmStratId");
                        deletePmfmStrategies(pmfmStrategyIds);
                    }
                }
            }

            // Save AppliedStrategies and AppliedPeriods
            if (strategy.isAppliedStrategiesLoaded() || !strategy.isAppliedStrategiesEmpty()) {
                if (!strategy.isAppliedStrategiesEmpty()) {

                    Map<Integer, AppliedStrategy> remainingAppliedStrategies = ReefDbBeans.mapByProperty(target.getAppliedStrategies(), "appliedStratId");
                    Map<String, AppliedStrategy> remainingAppliedStrategyByKey = buildAppliedStrategyMap(remainingAppliedStrategies.values(), dbTimezone);
                    AppliedStrategy previousAppliedStrategy = null;
                    // sort AppliedStrategyDTO by natural Id = monitoringLocation.Id
                    strategy.getAppliedStrategies().sort(new QuadrigeBeanComparator());
                    for (AppliedStrategyDTO appliedStrategy : strategy.getAppliedStrategies()) {

                        // save an applied strategy only if dates are valid or department set
                        // the department is not part of applied period, so don't take care about it (Mantis #43233)
                        if (appliedStrategy.getStartDate() == null && appliedStrategy.getEndDate() == null /*&& appliedStrategy.getDepartment() == null*/) {
                            continue;
                        }

                        AppliedStrategy targetAppliedStrategy = null;
                        if (previousAppliedStrategy != null) {
                            // check if same AppliedStrategyDTO as previous iteration
                            Integer previousDepId = previousAppliedStrategy.getDepartment() == null ? 0 : previousAppliedStrategy.getDepartment().getDepId();
                            if (previousAppliedStrategy.getMonitoringLocation().getMonLocId().equals(appliedStrategy.getId())
                                    && previousDepId.equals(appliedStrategy.getDepartment() == null ? 0 : appliedStrategy.getDepartment().getId())) {
                                targetAppliedStrategy = previousAppliedStrategy;
                            }
                        }
                        if (targetAppliedStrategy == null) {
                            targetAppliedStrategy = remainingAppliedStrategies.remove(appliedStrategy.getAppliedStrategyId());
                            if (targetAppliedStrategy == null) {
                                targetAppliedStrategy = AppliedStrategy.Factory.newInstance();
                                Integer newId = TemporaryDataHelper.<Integer>getNewNegativeIdForTemporaryData(getSession(), targetAppliedStrategy.getClass());
                                targetAppliedStrategy.setAppliedStratId(newId);
                                targetAppliedStrategy.setStrategy(target);
                                target.addAppliedStrategies(targetAppliedStrategy);
                                targetAppliedStrategy.setMonitoringLocation(load(MonitoringLocationImpl.class, appliedStrategy.getId()));
                            }
                            if (appliedStrategy.getDepartment() != null) {
                                targetAppliedStrategy.setDepartment(load(DepartmentImpl.class, appliedStrategy.getDepartment().getId()));
                            } else {
                                targetAppliedStrategy.setDepartment(null);
                            }
                            getSession().save(targetAppliedStrategy);
                            getSession().flush();
                            appliedStrategy.setAppliedStrategyId(targetAppliedStrategy.getAppliedStratId());
                            previousAppliedStrategy = targetAppliedStrategy;
                        }

                        // build key for applied period
                        String appliedStrategyKey = buildAppliedStrategyKey(appliedStrategy);
                        // get existing applied strategy with applied period
                        AppliedStrategy existingAppliedStrategy = remainingAppliedStrategyByKey.remove(appliedStrategyKey);
                        if (existingAppliedStrategy == null && appliedStrategy.getStartDate() != null && appliedStrategy.getEndDate() != null) {

                            // create period if both dates are set
                            AppliedPeriod appliedPeriod = AppliedPeriod.Factory.newInstance();
                            AppliedPeriodPK appliedPeriodPK = new AppliedPeriodPK();
                            appliedPeriodPK.setAppliedStrategy((AppliedStrategyImpl) targetAppliedStrategy);
                            appliedPeriodPK.setAppliedPeriodStartDt(Dates.convertToDate(appliedStrategy.getStartDate(), dbTimezone));
                            appliedPeriod.setAppliedPeriodPk(appliedPeriodPK);
                            appliedPeriod.setAppliedPeriodEndDt(Dates.convertToDate(appliedStrategy.getEndDate(), dbTimezone));

                            // if only end date change (PK still the same), the addAppliedPeriods will not works
                            AppliedPeriod periodToUpdate = null;
                            for (AppliedPeriod existingPeriod : targetAppliedStrategy.getAppliedPeriods()) {
                                LocalDate startDate = Dates.convertToLocalDate(existingPeriod.getAppliedPeriodPk().getAppliedPeriodStartDt(), dbTimezone);
                                if (Dates.isSameDay(startDate, appliedStrategy.getStartDate())) {
                                    periodToUpdate = existingPeriod;
                                    break;
                                }
                            }

                            if (periodToUpdate == null) {
                                // create new period
                                targetAppliedStrategy.addAppliedPeriods(appliedPeriod);
                            } else {
                                // update end date only
                                periodToUpdate.setAppliedPeriodEndDt(Dates.convertToDate(appliedStrategy.getEndDate(), dbTimezone));
                                getSession().update(periodToUpdate);
                            }
                            getSession().update(targetAppliedStrategy);
                        }

                        // save all PmfmStrategies into PmfmAppliedStrategies
                        savePmfmAppliedStrategies(strategy, target, targetAppliedStrategy);

                        // save appliedStrategy
                        getSession().update(targetAppliedStrategy);
                    }
                    // delete remaining data
                    if (!remainingAppliedStrategyByKey.isEmpty()) {
                        // some applied period to delete
                        for (String key : remainingAppliedStrategyByKey.keySet()) {
                            AppliedStrategy appliedStrategy = remainingAppliedStrategyByKey.get(key);
                            // if this applied strategy still in remainingAppliedStrategies, it will be remove later
                            if (remainingAppliedStrategies.containsValue(appliedStrategy)) {
                                continue;
                            }
                            List<String> value = Splitter.on('|').splitToList(key);
                            String startDateValue = value.get(1);
                            LocalDate startDate = parseAppliedStrategyDate(startDateValue);
                            String endDateValue = value.get(2);
                            LocalDate endDate = parseAppliedStrategyDate(endDateValue);
                            Iterator<AppliedPeriod> it = appliedStrategy.getAppliedPeriods().iterator();
                            while (it.hasNext()) {
                                AppliedPeriod appliedPeriod = it.next();
                                LocalDate appliedPeriodStartDate = Dates.convertToLocalDate(appliedPeriod.getAppliedPeriodStartDt(), dbTimezone);
                                LocalDate appliedPeriodEndDate = Dates.convertToLocalDate(appliedPeriod.getAppliedPeriodEndDt(), dbTimezone);
                                if (Dates.isSameDay(appliedPeriodStartDate, startDate) && Dates.isSameDay(appliedPeriodEndDate, endDate)) {
                                    it.remove();
                                }
                            }
                            getSession().update(appliedStrategy);
                        }
                    }

                    // delete remaining applied strategies
                    if (!remainingAppliedStrategies.isEmpty()) {
                        target.getAppliedStrategies().removeAll(remainingAppliedStrategies.values());
                        // delete
                        deleteAppliedStrategies(remainingAppliedStrategies.keySet());
                    }

                } else {

                    if (CollectionUtils.isNotEmpty(target.getAppliedStrategies())) {
                        List<Integer> appliedStrategyIds = ReefDbBeans.collectProperties(target.getAppliedStrategies(), "appliedStratId");
                        target.getAppliedStrategies().clear();
                        deleteAppliedStrategies(appliedStrategyIds);
                    }
                }
            }

            // save entity
            update(target);

            getSession().flush();
            getSession().clear();
        }

        // remove remaining strategies
        if (!remainingStrategieIds.isEmpty()) {
            removeByIds(remainingStrategieIds);
        }
    }

    private void savePmfmAppliedStrategies(StrategyDTO strategy, Strategy target, AppliedStrategy appliedStrategy) {

        if (strategy.isPmfmStrategiesLoaded()) {
            if (!strategy.isPmfmStrategiesEmpty()) {

                Map<Integer, PmfmStrategyDTO> psfmStrategyById = ReefDbBeans.mapById(strategy.getPmfmStrategies());
                Map<Integer, PmfmAppliedStrategy> remainingPmfmAppliedStrategiesByPmfmStrategyId =
                        ReefDbBeans.mapByProperty(appliedStrategy.getPmfmAppliedStrategies(), "pmfmAppliedStrategyPk.pmfmStrategy.pmfmStratId");

                // pmfmStrategies already in strategy entity
                for (PmfmStrategy pmfmStrategy : target.getPmfmStrategies()) {

                    Integer pmfmStrategyId = pmfmStrategy.getPmfmStratId();
                    PmfmStrategyDTO psfmProgStrat = psfmStrategyById.get(pmfmStrategyId);
                    // get already stored
                    PmfmAppliedStrategy pmfmAppliedStrategy = remainingPmfmAppliedStrategiesByPmfmStrategyId.remove(pmfmStrategyId);
                    if (pmfmAppliedStrategy == null) {
                        // create new
                        pmfmAppliedStrategy = PmfmAppliedStrategy.Factory.newInstance();
                        PmfmAppliedStrategyPK pmfmAppliedStrategyPK = new PmfmAppliedStrategyPK();
                        pmfmAppliedStrategyPK.setAppliedStrategy((AppliedStrategyImpl) appliedStrategy);
                        pmfmAppliedStrategyPK.setPmfmStrategy((PmfmStrategyImpl) pmfmStrategy);
                        pmfmAppliedStrategy.setPmfmAppliedStrategyPk(pmfmAppliedStrategyPK);
                    }

                    // update department
                    if (psfmProgStrat.getAnalysisDepartment() != null) {
                        pmfmAppliedStrategy.setDepartment(load(DepartmentImpl.class, psfmProgStrat.getAnalysisDepartment().getId()));
                    } else {
                        pmfmAppliedStrategy.setDepartment(null);
                    }

                    // save and affect to appliedStrategy
                    getSession().saveOrUpdate(pmfmAppliedStrategy);
                    appliedStrategy.addPmfmAppliedStrategies(pmfmAppliedStrategy);
                }

                // remove remaining PmfmAppliedStrategies
                if (!remainingPmfmAppliedStrategiesByPmfmStrategyId.isEmpty()) {
                    appliedStrategy.getPmfmAppliedStrategies().removeAll(remainingPmfmAppliedStrategiesByPmfmStrategyId.values());
                }

            } else {
                // remove pmfmAppliedStrategies
                if (appliedStrategy.getPmfmAppliedStrategies() != null) {
                    appliedStrategy.getPmfmAppliedStrategies().clear();
                }
            }
        }
    }

    private void savePmfmStrategy(PmfmStrategyDTO pmfmStrategyDTO, PmfmStrategy pmfmStrategy, int rank) {

        // presentation rank
        pmfmStrategy.setPmfmStratPresRk(rank);

        // pmfm
        pmfmStrategy.setPmfm(load(PmfmImpl.class, pmfmStrategyDTO.getPmfm().getId()));

        // grouping (= individualised measurement)
        pmfmStrategy.setPmfmStratParIsIndiv(Daos.convertToString(pmfmStrategyDTO.isGrouping()));

        // unique by taxon
        pmfmStrategy.setPmfmStratIsUniqueByTaxon(Daos.convertToString(pmfmStrategyDTO.isUnique()));

        // acquisition levels
        Map<String, AcquisitionLevel> remainingAcquisitionLevels = ReefDbBeans.mapByProperty(pmfmStrategy.getAcquisitionLevels(), "acquisLevelCd");

        // SURVEY acquisition level
        if (BooleanUtils.toBoolean(pmfmStrategyDTO.isSurvey())) {
            AcquisitionLevel acquisitionLevel = remainingAcquisitionLevels.remove(AcquisitionLevelCode.SURVEY.getValue());
            if (acquisitionLevel == null) {
                // add SURVEY acquisition level
                acquisitionLevel = load(AcquisitionLevelImpl.class, AcquisitionLevelCode.SURVEY.getValue());
                pmfmStrategy.addAcquisitionLevels(acquisitionLevel);
            }
        }

        // SAMPLING_OPERATION acquisition level
        if (BooleanUtils.toBoolean(pmfmStrategyDTO.isSampling())) {
            AcquisitionLevel acquisitionLevel = remainingAcquisitionLevels.remove(AcquisitionLevelCode.SAMPLING_OPERATION.getValue());
            if (acquisitionLevel == null) {
                // add SURVEY acquisition level
                acquisitionLevel = load(AcquisitionLevelImpl.class, AcquisitionLevelCode.SAMPLING_OPERATION.getValue());
                pmfmStrategy.addAcquisitionLevels(acquisitionLevel);
            }
        }

        // delete remaining acquisition levels
        if (!remainingAcquisitionLevels.isEmpty()) {
            pmfmStrategy.getAcquisitionLevels().removeAll(remainingAcquisitionLevels.values());
        }
    }

    /** {@inheritDoc} */
    @Override
    public void deleteAppliedStrategies(String programCode, Collection<Integer> monitoringLocationIds) {
        Assert.notBlank(programCode);
        if (monitoringLocationIds == null) return;
        Set<Integer> idsToDelete = monitoringLocationIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(idsToDelete)) {
            return;
        }

        Query query = createQuery("deleteAppliedStrategiesByProgramCodeAndMonitoringLocationIds", "programCode", StringType.INSTANCE, programCode);
        query.setParameterList("monitoringLocationIds", idsToDelete);
        query.executeUpdate();

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void removeByProgramCode(String programCode) {
        Assert.notBlank(programCode);

        // delete Strategies
        List<Integer> strategyIds = ReefDbBeans.collectIds(getStrategiesByProgramCode(programCode));
        removeByStrategyIds(strategyIds);
    }

    /** {@inheritDoc} */
    @Override
    public void removeByStrategyIds(Collection<Integer> strategyIds) {
        if (strategyIds == null) return;

        // the Hibernate remove method works fine (against direct query that can not handle PMFM_STRAT_ACQUIS_LEVEL table)
        // or execute a first query doing that...
        strategyIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteAppliedStrategies(Collection<Integer> appliedStrategyIds) {
        if (appliedStrategyIds == null) return;
        Set<Integer> idsToDelete = appliedStrategyIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(idsToDelete)) {
            return;
        }

        Query query = createQuery("deleteAppliedStrategiesByIds");
        query.setParameterList("appliedStrategyIds", idsToDelete);
        query.executeUpdate();

    }

    private void deletePmfmStrategies(List<Integer> pmfmStrategyIds) {
        if (pmfmStrategyIds == null) return;
        Set<Integer> idsToDelete = pmfmStrategyIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(idsToDelete)) {
            return;
        }

        // Delete associated qualitative values
        pmfmStrategyDao.deleteQualitativeValues(idsToDelete);

        Query query = createQuery("deletePmfmAppliedStrategiesByPmfmStrategyIds");
        query.setParameterList("pmfmStrategyIds", idsToDelete);
        query.executeUpdate();

        query = createQuery("deletePmfmStrategiesByIds");
        query.setParameterList("pmfmStrategyIds", idsToDelete);
        query.executeUpdate();
    }

    /** {@inheritDoc} */
    @Override
    public void saveStrategyByLocation(ProgStratDTO strategy, int locationId) {

        Assert.notNull(strategy);
        Assert.notNull(strategy.getId());
        Assert.isTrue((strategy.getStartDate() == null) == (strategy.getEndDate() == null));

        AppliedStrategy target = get(AppliedStrategyImpl.class, strategy.getAppliedStrategyId());
        // the applied strategy should exists
        Assert.notNull(target);
        // just to be sure
        Assert.equals(target.getMonitoringLocation().getMonLocId(), locationId);

        TimeZone dbTimezone = config.getDbTimezone();

        Collection<AppliedPeriod> appliedPeriods = target.getAppliedPeriods();
        if (appliedPeriods.size() > 1) {
            throw new ReefDbTechnicalException("can't update more than 1 applied periods");
        }
        if (strategy.getStartDate() != null && strategy.getEndDate() != null) {
            // add or update applied period
            boolean create = false;

            AppliedPeriod appliedPeriod = null;
            if (CollectionUtils.isEmpty(appliedPeriods)) {
                create = true;
            } else {
                appliedPeriod = appliedPeriods.iterator().next();
                if (!Dates.isSameDay(Dates.convertToLocalDate(appliedPeriod.getAppliedPeriodStartDt(), dbTimezone), strategy.getStartDate())) {
                    // update by delete and create, because update start date doesn't work
                    target.getAppliedPeriods().clear();
                    create = true;
                }
            }
            if (create) {
                appliedPeriod = AppliedPeriod.Factory.newInstance();
                AppliedPeriodPK appliedPeriodPK = new AppliedPeriodPK();
                appliedPeriodPK.setAppliedStrategy((AppliedStrategyImpl) target);
                appliedPeriodPK.setAppliedPeriodStartDt(Dates.convertToDate(strategy.getStartDate(), dbTimezone));
                appliedPeriod.setAppliedPeriodPk(appliedPeriodPK);
                appliedPeriod.setAppliedPeriodEndDt(Dates.convertToDate(strategy.getEndDate(), dbTimezone));
                target.addAppliedPeriods(appliedPeriod);
            } else {
                // update end date only
                appliedPeriod.setAppliedPeriodEndDt(Dates.convertToDate(strategy.getEndDate(), dbTimezone));
                getSession().update(appliedPeriod);
            }
            getSession().update(target);

        } else {
            // delete applied period if exists
            target.getAppliedPeriods().clear();
            getSession().update(target);
        }

        getSession().flush();
        getSession().clear();
    }

// PRIVATE METHODS

    private Map<String, AppliedStrategy> buildAppliedStrategyMap(Collection<AppliedStrategy> appliedStrategies, TimeZone dbTimezone) {
        Map<String, AppliedStrategy> result = Maps.newHashMap();

        for (AppliedStrategy appliedStrategy : appliedStrategies) {

            if (CollectionUtils.isNotEmpty(appliedStrategy.getAppliedPeriods())) {
                for (AppliedPeriod appliedPeriod : appliedStrategy.getAppliedPeriods()) {

                    Object[] parts = new Object[]{
                            appliedStrategy.getMonitoringLocation().getMonLocId(),
                            formatAppliedStrategyDate(Dates.convertToLocalDate(appliedPeriod.getAppliedPeriodStartDt(), dbTimezone)),
                            formatAppliedStrategyDate(Dates.convertToLocalDate(appliedPeriod.getAppliedPeriodEndDt(), dbTimezone))
                    };
                    String key = Joiner.on('|').useForNull("null").join(parts);
                    result.put(key, appliedStrategy);

                }
            } else {

                Object[] parts = new Object[]{
                        appliedStrategy.getMonitoringLocation().getMonLocId(),
                        null, null
                };
                String key = Joiner.on('|').useForNull("null").join(parts);
                result.put(key, appliedStrategy);
            }
        }

        return result;
    }

    private String buildAppliedStrategyKey(AppliedStrategyDTO appliedStrategy) {
        Assert.notNull(appliedStrategy);
        Assert.notNull(appliedStrategy.getId());
        return Joiner.on('|').useForNull("null").join(
                appliedStrategy.getId(),
                formatAppliedStrategyDate(appliedStrategy.getStartDate()),
                formatAppliedStrategyDate(appliedStrategy.getEndDate())
        );
    }

    private String formatAppliedStrategyDate(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(DATE_PATTERN));
    }

    private LocalDate parseAppliedStrategyDate(String string) {
        return LocalDate.parse(string, DateTimeFormatter.ofPattern(DATE_PATTERN));
    }

    private StrategyDTO toStrategyDTO(Iterator<Object> source) {
        StrategyDTO result = ReefDbBeanFactory.newStrategyDTO();

        // Id
        result.setId((Integer) source.next());

        // Name
        result.setName((String) source.next());

        // Descritpion => Comment
        result.setComment((String) source.next());

        return result;
    }

    private AppliedStrategyDTO toAppliedStrategyDTO(Iterator<Object> source) {
        AppliedStrategyDTO target = ReefDbBeanFactory.newAppliedStrategyDTO();

        // applied strategy : id
        target.setAppliedStrategyId((Integer) source.next());

        // monitoring location: id
        target.setId((Integer) source.next());

        // Label
        target.setLabel((String) source.next());

        // Name
        target.setName((String) source.next());

        // Comments
        target.setComment((String) source.next());

        // Status (location)
        target.setStatus(Daos.getStatus(((Status) source.next()).getStatusCd()));

        // Department
        Integer departmentId = (Integer) source.next();
        if (departmentId != null) {
            target.setDepartment(departmentDao.getDepartmentById(departmentId));
        }

        return target;
    }

    private AppliedStrategyDTO toAppliedPeriod(Iterator<Object> source, TimeZone dbTimezone) {
        // Starts with applied strategy columns
        AppliedStrategyDTO target = toAppliedStrategyDTO(source);

        // then start/end date with database timezone conversion (Mantis #41597)
        target.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone));
        target.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone));

        // set also previous dates
        target.setPreviousStartDate(target.getStartDate());
        target.setPreviousEndDate(target.getEndDate());

        return target;
    }

    private PmfmStrategyDTO toPmfmStrategyDTO(Iterator<Object> source) {
        PmfmStrategyDTO target = ReefDbBeanFactory.newPmfmStrategyDTO();

        // AppliedStrategyId
        target.setId((Integer) source.next());

        // Pmfm id
        target.setPmfm(pmfmDao.getPmfmById((Integer) source.next()));

        // department
        Integer departmentId = (Integer) source.next();
        if (departmentId != null) {
            target.setAnalysisDepartment(departmentDao.getDepartmentById(departmentId));
        }

        // note SBO (mantis #24206) :
        //    Actuellement la notion d'individus dans Quadrige est utilisée comme un regroupement de résultats.
        //  Cette définition correspond bien au besoin BD Recif.
        //    Ok pour utiliser PMFM_STRATEGY.PMFM_STRAT_PAR_IS_INDIV pour identifier le type de tableau (mesures
        //  regroupées ou non regroupées) dans lequel le PSFM doit être affiché
        target.setGrouping(Daos.safeConvertToBoolean(source.next(), false));

        target.setUnique(Daos.safeConvertToBoolean(source.next(), false));

        // rank order
        target.setRankOrder((Integer) source.next());

        return target;
    }

    /**
     * <p>toProgStratDTO.</p>
     *
     * @param source a {@link Iterator} object.
     * @param dbTimezone
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO} object.
     */
    protected ProgStratDTO toProgStratDTO(Iterator<Object> source, TimeZone dbTimezone) {
        ProgStratDTO target = ReefDbBeanFactory.newProgStratDTO();

        // applied strategy id
        target.setAppliedStrategyId((Integer) source.next());

        // program
        {
            ProgramDTO program = ReefDbBeanFactory.newProgramDTO();
            target.setProgram(program);

            // code, name
            program.setCode((String) source.next());
            program.setName((String) source.next());
            program.setStatus(Daos.getStatus((String) source.next()));
        }

        // strategy: id, name
        target.setId((Integer) source.next());
        target.setName((String) source.next());

        // status
        if (target.getId() < 0) {
            target.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }

        // then start/end date
        target.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone));
        target.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone));

        Integer depId = (Integer) source.next();
        if (depId != null) {
            target.setDepartment(departmentDao.getDepartmentById(depId));
        }

        return target;
    }

    /**
     * <p>strategyDTOToEntity.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     * @param source a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO} object.
     * @param target a {@link fr.ifremer.quadrige3.core.dao.administration.strategy.Strategy} object.
     */
    @SuppressWarnings("JavadocReference")
    protected void strategyDTOToEntity(ProgramDTO program, StrategyDTO source, Strategy target) {

        target.setStratNm(source.getName());
        target.setStratDc(source.getComment());

        // Set update date on strategy (only for local strategy)
        if (ReefDbBeans.isLocalStatus(program.getStatus())) {
            target.setUpdateDt(newUpdateTimestamp());
        }

        // User privileges: copy manager from program (see mantis #28029)
        if (CollectionUtils.isEmpty(program.getManagerPersons())) {
            if (CollectionUtils.isNotEmpty(target.getQusers())) {
                target.getQusers().clear();
            }
        }
        else {
            Daos.replaceEntities(
                    target.getQusers(),
                    program.getManagerPersons(),
                    person -> load(QuserImpl.class, person.getId())
            );
        }
    }


    private void fillQualitativeValues(List<PmfmStrategyDTO> pmfmStrategies) {
        pmfmStrategies.forEach(pmfmStrategy -> {
            // get actual restricted list

            List<Integer> qvIds = pmfmStrategyDao.getQualitativeValueIds(pmfmStrategy.getId());

            pmfmStrategy.setQualitativeValues(
                pmfmStrategy.getPmfm().getQualitativeValues().stream()
                    .filter(qualitativeValueDTO -> CollectionUtils.isEmpty(qvIds) || qvIds.contains(qualitativeValueDTO.getId()))
                    .collect(Collectors.toList())
            );

        });
    }

    private void saveQualitativeValues(PmfmStrategyDTO pmfmStrategy) {
        // delete all first

        pmfmStrategyDao.deleteQualitativeValues(pmfmStrategy.getId());

        if (pmfmStrategy.sizeQualitativeValues() != pmfmStrategy.getPmfm().sizeQualitativeValues()) {
            // then insert if sub-list is different from pmfm qualitative value list
            pmfmStrategy.getQualitativeValues()
                .forEach(qualitativeValueDTO -> pmfmStrategyDao.saveQualitativeValue(pmfmStrategy.getId(), pmfmStrategy.getPmfm().getId(), qualitativeValueDTO.getId()));
        }
    }


}
