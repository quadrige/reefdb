package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 17/07/2015.
 */
public interface ReefDbSamplingEquipmentDao {

    String SAMPLING_EQUIPMENT_BY_ID_CACHE = "sampling_equipment_by_id";
    String ALL_SAMPLING_EQUIPMENTS_CACHE = "all_sampling_equipments";
    String SAMPLING_EQUIPMENTS_BY_IDS_CACHE = "sampling_equipments_by_ids";

    /**
     * <p>getAllSamplingEquipments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_SAMPLING_EQUIPMENTS_CACHE)
    List<SamplingEquipmentDTO> getAllSamplingEquipments(List<String> statusCodes);

    /**
     * <p>getSamplingEquipmentById.</p>
     *
     * @param samplingEquipmentId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO} object.
     */
    @Cacheable(value = SAMPLING_EQUIPMENT_BY_ID_CACHE)
    SamplingEquipmentDTO getSamplingEquipmentById(int samplingEquipmentId);

    /**
     * <p>getSamplingEquipmentsByIds.</p>
     *
     * @param samplingEquipmentIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = SAMPLING_EQUIPMENTS_BY_IDS_CACHE)
    List<SamplingEquipmentDTO> getSamplingEquipmentsByIds(List<Integer> samplingEquipmentIds);

    /**
     * <p>findSamplingEquipments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param samplingEquipmentId a {@link java.lang.Integer} object.
     * @param unitId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<SamplingEquipmentDTO> findSamplingEquipments(List<String> statusCodes, Integer samplingEquipmentId, Integer unitId);

    /**
     * <p>findSamplingEquipmentsByName.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param samplingEquipmentName a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<SamplingEquipmentDTO> findSamplingEquipmentsByName(List<String> statusCodes, String samplingEquipmentName);

    /**
     * <p>saveSamplingEquipments.</p>
     *
     * @param samplingEquipments a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_SAMPLING_EQUIPMENTS_CACHE,
            SAMPLING_EQUIPMENT_BY_ID_CACHE,
            SAMPLING_EQUIPMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void saveSamplingEquipments(List<? extends SamplingEquipmentDTO> samplingEquipments);

    /**
     * <p>deleteSamplingEquipments.</p>
     *
     * @param samplingEquipmentIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_SAMPLING_EQUIPMENTS_CACHE,
            SAMPLING_EQUIPMENT_BY_ID_CACHE,
            SAMPLING_EQUIPMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void deleteSamplingEquipments(List<Integer> samplingEquipmentIds);

    /**
     * <p>replaceTemporarySamplingEquipment.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_SAMPLING_EQUIPMENTS_CACHE,
            SAMPLING_EQUIPMENT_BY_ID_CACHE,
            SAMPLING_EQUIPMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void replaceTemporarySamplingEquipment(Integer sourceId, Integer targetId, boolean delete);

    /**
     * Check if this sampling equipment is used in data
     * SAMPLING_OPERATION
     *
     * @param samplingEquipmentId a int.
     * @return a boolean.
     */
    boolean isSamplingEquipmentUsedInData(int samplingEquipmentId);

    /**
     * <p>isSamplingEquipmentUsedInValidatedData.</p>
     *
     * @param samplingEquipmentId a int.
     * @return a boolean.
     */
    boolean isSamplingEquipmentUsedInValidatedData(int samplingEquipmentId);
}
