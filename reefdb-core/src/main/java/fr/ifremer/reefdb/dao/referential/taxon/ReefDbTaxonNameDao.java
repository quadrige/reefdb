package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonomicLevelDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p>ReefDbTaxonNameDao interface.</p>
 *
 */
public interface ReefDbTaxonNameDao {

    String TAXON_NAME_BY_ID_CACHE = "taxon_name_by_id";
    String TAXON_NAMES_BY_IDS_CACHE = "taxon_names_by_ids";
    String TAXON_NAME_BY_REFERENCE_ID_CACHE = "taxon_name_by_reference_id";
    String ALL_TAXON_NAMES_CACHE = "all_taxon_names";
    String TAXON_NAME_BY_TAXON_GROUP_ID_CACHE = "taxon_name_by_taxon_group_id";
    String COMPOSITE_TAXON_NAMES_BY_TAXON_NAME_ID_CACHE = "composite_taxon_names_by_taxon_name_id";
    String TAX_REF_BY_TAXON_NAME_ID_CACHE = "tax_ref_by_taxon_name_id";
    String WORMS_BY_TAXON_NAME_ID_CACHE = "worms_by_taxon_name_id";

    /**
     * All taxon names.
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_TAXON_NAMES_CACHE)
    List<TaxonDTO> getAllTaxonNames();

    /**
     * <p>getTaxonNameById.</p>
     *
     * @param taxonId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    @Cacheable(value = TAXON_NAME_BY_ID_CACHE)
    TaxonDTO getTaxonNameById(int taxonId);

    /**
     * <p>getTaxonNamesByIds.</p>
     *
     * @param taxonIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = TAXON_NAMES_BY_IDS_CACHE)
    List<TaxonDTO> getTaxonNamesByIds(List<Integer> taxonIds);

    /**
     * <p>getTaxonNameByReferenceId.</p>
     *
     * @param referenceTaxonId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    @Cacheable(value = TAXON_NAME_BY_REFERENCE_ID_CACHE)
    TaxonDTO getTaxonNameByReferenceId(int referenceTaxonId);

    /**
     * Get All Taxon Names of the Taxon Group
     * Beware to reset time on date parameter before call this method
     *
     * @return a {@link com.google.common.collect.Multimap} object.
     * @param date
     */
    @Cacheable(value = TAXON_NAME_BY_TAXON_GROUP_ID_CACHE)
    Multimap<Integer, TaxonDTO> getAllTaxonNamesMapByTaxonGroupId(@Nonnull LocalDate date);

    /**
     * <p>findTaxonNamesByCriteria.</p>
     *
     * @param levelCode a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     * @param isStrictName a boolean.
     * @param isLocal      @return lightweight taxon names (without parent, composites and referent taxons)
     * @return a {@link java.util.List} object.
     */
    List<TaxonDTO> findTaxonNamesByCriteria(String levelCode, String name, boolean isStrictName, Boolean isLocal);

    /**
     * <p>findFullTaxonNamesByCriteria.</p>
     *
     * @param levelCode a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     * @param isStrictName a boolean.
     * @param isLocal      @return taxon names with complete information (parent, composites, referent taxons)
     * @return a {@link java.util.List} object.
     */
    List<TaxonDTO> findFullTaxonNamesByCriteria(String levelCode, String name, boolean isStrictName, Boolean isLocal);

    /**
     * Fill all properties of the taxon list
     *
     * @param taxons a {@link java.util.List} object.
     */
    void fillTaxonsProperties(List<TaxonDTO> taxons);

    /**
     * <p>fillParentAndReferent.</p>
     *
     * @param taxon a {@link fr.ifremer.reefdb.dto.referential.TaxonDTO} object.
     */
    void fillParentAndReferent(TaxonDTO taxon);

    /**
     * <p>fillReferent.</p>
     *
     * @param taxons a {@link TaxonDTO} object.
     */
    void fillReferents(List<TaxonDTO> taxons);

    /**
     * <p>getAllTaxonomicLevels.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = "all_taxonomic_levels")
    List<TaxonomicLevelDTO> getAllTaxonomicLevels();

    /**
     * <p>getCompositeTaxonNames.</p>
     *
     * @param taxonNameId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = COMPOSITE_TAXON_NAMES_BY_TAXON_NAME_ID_CACHE)
    List<TaxonDTO> getCompositeTaxonNames(Integer taxonNameId);

    /**
     * <p>getTaxRefByTaxonNameId.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    @Cacheable(value = TAX_REF_BY_TAXON_NAME_ID_CACHE)
    Map<Integer, String> getTaxRefByTaxonNameId();

    /**
     * <p>getWormsByTaxonNameId.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    @Cacheable(value = WORMS_BY_TAXON_NAME_ID_CACHE)
    Map<Integer, String> getWormsByTaxonNameId();

    /**
     * <p>saveTaxons.</p>
     *
     * @param taxons a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            TAXON_NAME_BY_ID_CACHE,
            TAXON_NAMES_BY_IDS_CACHE,
            TAXON_NAME_BY_REFERENCE_ID_CACHE,
            ALL_TAXON_NAMES_CACHE,
            TAXON_NAME_BY_TAXON_GROUP_ID_CACHE,
            COMPOSITE_TAXON_NAMES_BY_TAXON_NAME_ID_CACHE
    }, allEntries = true)
    void saveTaxons(List<? extends TaxonDTO> taxons);

    /**
     * <p>deleteTaxons.</p>
     *
     * @param taxonIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            TAXON_NAME_BY_ID_CACHE,
            TAXON_NAMES_BY_IDS_CACHE,
            TAXON_NAME_BY_REFERENCE_ID_CACHE,
            ALL_TAXON_NAMES_CACHE,
            TAXON_NAME_BY_TAXON_GROUP_ID_CACHE,
            COMPOSITE_TAXON_NAMES_BY_TAXON_NAME_ID_CACHE
    }, allEntries = true)
    void deleteTaxons(List<Integer> taxonIds);

    /**
     * <p>replaceTemporaryTaxon.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param sourceReferenceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param targetReferenceId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            TAXON_NAME_BY_ID_CACHE,
            TAXON_NAMES_BY_IDS_CACHE,
            TAXON_NAME_BY_REFERENCE_ID_CACHE,
            ALL_TAXON_NAMES_CACHE,
            TAXON_NAME_BY_TAXON_GROUP_ID_CACHE,
            COMPOSITE_TAXON_NAMES_BY_TAXON_NAME_ID_CACHE
    }, allEntries = true)
    void replaceTemporaryTaxon(Integer sourceId, Integer sourceReferenceId, Integer targetId, Integer targetReferenceId, boolean delete);

    /**
     * <p>isTaxonNameUsedInReferential.</p>
     *
     * @param taxonId a int.
     * @return a boolean.
     */
    boolean isTaxonNameUsedInReferential(int taxonId);

    /**
     * <p>isReferenceTaxonUsedInReferential.</p>
     *
     * @param refTaxonId a int.
     * @param excludedTaxonNameId a int.
     * @return a boolean.
     */
    boolean isReferenceTaxonUsedInReferential(int refTaxonId, int excludedTaxonNameId);

    /**
     * <p>isReferenceTaxonUsedInData.</p>
     *
     * @param refTaxonId a int.
     * @return a boolean.
     */
    boolean isReferenceTaxonUsedInData(int refTaxonId);

    /**
     * <p>isReferenceTaxonUsedInValidatedData.</p>
     *
     * @param refTaxonId a int.
     * @return a boolean.
     */
    boolean isReferenceTaxonUsedInValidatedData(int refTaxonId);
}
