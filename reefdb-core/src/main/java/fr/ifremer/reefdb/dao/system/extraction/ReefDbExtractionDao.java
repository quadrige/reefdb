package fr.ifremer.reefdb.dao.system.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.extraction.ExtractFilterDao;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;

import java.util.List;

/**
 * <p>ReefDbExtractionDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbExtractionDao extends ExtractFilterDao {

    /**
     * <p>getAllExtraction.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> getAllExtraction();

    /**
     * <p>getExtractionById.</p>
     *
     * @param extractionId a int.
     * @return a {@link fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO} object.
     */
    ExtractionDTO getExtractionById(int extractionId);

    /**
     * <p>searchExtractionByProgram.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<ExtractionDTO> searchExtractionByProgram(String programCode);

    /**
     * <p>saveExtraction.</p>
     *
     * @param extraction a {@link fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO} object.
     */
    void saveExtraction(ExtractionDTO extraction);
}
