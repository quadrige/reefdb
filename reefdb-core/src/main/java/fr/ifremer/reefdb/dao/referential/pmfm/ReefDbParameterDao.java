package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface ReefDbParameterDao {

    String PARAMETER_BY_CODE_CACHE = "parameter_by_code";
    String ALL_PARAMETERS_CACHE = "all_parameters";

    String ALL_PARAMETER_GROUPS_CACHE = "all_parameter_groups";
    String PARAMETER_GROUP_BY_ID_CACHE = "parameter_group_by_id";

    /**
     * <p>getAllParameters.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PARAMETERS_CACHE)
    List<ParameterDTO> getAllParameters(List<String> statusCodes);

    /**
     * <p>getParameterByCode.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO} object.
     */
    @Cacheable(value = PARAMETER_BY_CODE_CACHE)
    ParameterDTO getParameterByCode(String parameterCode);

    /**
     * <p>findParameters.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @param parameterGroupId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<ParameterDTO> findParameters(String parameterCode, Integer parameterGroupId, List<String> statusCodes);

    /**
     * <p>saveParameters.</p>
     *
     * @param parameters a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            PARAMETER_BY_CODE_CACHE,
            ALL_PARAMETERS_CACHE,
            ReefDbQualitativeValueDao.QUALITATIVE_VALUES_BY_PARAMETER_CODE_CACHE
    }, allEntries = true)
    void saveParameters(List<? extends ParameterDTO> parameters);

    /**
     * <p>deleteParameters.</p>
     *
     * @param parametersCodes a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            PARAMETER_BY_CODE_CACHE,
            ALL_PARAMETERS_CACHE,
            ReefDbQualitativeValueDao.QUALITATIVE_VALUES_BY_PARAMETER_CODE_CACHE
    }, allEntries = true)
    void deleteParameters(List<String> parametersCodes);

    /**
     * <p>replaceTemporaryParameter.</p>
     *
     * @param sourceCode a {@link java.lang.String} object.
     * @param targetCode a {@link java.lang.String} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            PARAMETER_BY_CODE_CACHE,
            ALL_PARAMETERS_CACHE,
            ReefDbQualitativeValueDao.QUALITATIVE_VALUES_BY_PARAMETER_CODE_CACHE
    }, allEntries = true)
    void replaceTemporaryParameter(String sourceCode, String targetCode, boolean delete);

    /**
     * <p>isParameterUsedInProgram.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isParameterUsedInProgram(String parameterCode);

    /**
     * <p>isParameterUsedInRules.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isParameterUsedInRules(String parameterCode);

    /**
     * <p>isParameterUsedInReferential.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isParameterUsedInReferential(String parameterCode);

    /**
     * <p>getAllParameterGroups.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PARAMETER_GROUPS_CACHE)
    List<ParameterGroupDTO> getAllParameterGroups(List<String> statusCodes);

    /**
     * <p>getParameterGroupById.</p>
     *
     * @param parameterGroupId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO} object.
     */
    @Cacheable(value = PARAMETER_GROUP_BY_ID_CACHE)
    ParameterGroupDTO getParameterGroupById(int parameterGroupId);

}
