package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.user.QuserExtendDao;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.Collection;
import java.util.List;

/**
 * <p>ReefDbQuserDao interface.</p>
 *
 */
public interface ReefDbQuserDao extends QuserExtendDao {

    int TRANSFORM_PERSON_DTO = 100;
    String USER_BY_ID_CACHE = "user_by_id";
    String ALL_USER_CACHE = "all_users";

    /**
     * <p>getUserById.</p>
     *
     * @param personId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    @Cacheable(value = USER_BY_ID_CACHE)
    PersonDTO getUserById(int personId);

    /**
     * <p>getAllUsers.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_USER_CACHE)
    List<PersonDTO> getAllUsers(List<String> statusCodes);

    /**
     * <p>findUsersByCriteria.</p>
     *
     * @param lastName a {@link java.lang.String} object.
     * @param firstName a {@link java.lang.String} object.
     * @param strictName a boolean.
     * @param login a {@link java.lang.String} object.
     * @param departmentId a {@link java.lang.Integer} object.
     * @param privilegeCode a {@link java.lang.String} object.
     * @param statusCodes    @return
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> findUsersByCriteria(String lastName, String firstName, boolean strictName, String login, Integer departmentId, String privilegeCode, List<String> statusCodes);

    /**
     * <p>getUserByLogin.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param login a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    PersonDTO getUserByLogin(List<String> statusCodes, String login);

    /**
     * <p>getUsersByIds.</p>
     *
     * @param userIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<PersonDTO> getUsersByIds(List<Integer> userIds);

    /**
     * <p>isUserUsedInProgram.</p>
     *
     * @param id a int.
     * @return a boolean.
     */
    boolean isUserUsedInProgram(int id);

    /**
     * <p>isUserUsedInRules.</p>
     *
     * @param id a int.
     * @return a boolean.
     */
    boolean isUserUsedInRules(int id);

    /**
     * <p>isUserUsedInData.</p>
     *
     * @param id a int.
     * @return a boolean.
     */
    boolean isUserUsedInData(int id);

    /**
     * <p>isUserUsedInValidatedData.</p>
     *
     * @param id a int.
     * @return a boolean.
     */
    boolean isUserUsedInValidatedData(int id);

    /**
     * <p>deleteTemporaryUser.</p>
     *
     * @param ids a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_USER_CACHE,
            USER_BY_ID_CACHE
    }, allEntries = true)
    void deleteTemporaryUser(List<Integer> ids);

    /**
     * <p>deleteTemporaryUser.</p>
     *
     * @param id a {@link java.lang.Integer} object.
     */
    @CacheEvict(value = {
            ALL_USER_CACHE,
            USER_BY_ID_CACHE
    }, allEntries = true)
    void deleteTemporaryUser(Integer id);

    /**
     * <p>getAllPrivileges.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<PrivilegeDTO> getAllPrivileges();

    Collection<PrivilegeDTO> getPrivilegesByUserId(Integer userId);

    /**
     * <p>saveTemporaryUser.</p>
     *
     * @param user a {@link PersonDTO} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    @CacheEvict(value = {
            ALL_USER_CACHE,
            USER_BY_ID_CACHE
    }, allEntries = true)
    void saveTemporaryUser(PersonDTO user);

    @CacheEvict(value = {
            ALL_USER_CACHE,
            USER_BY_ID_CACHE
    }, allEntries = true)
    void replaceTemporaryUserFks(Integer sourceQuserId, Integer targetQuserId, boolean delete);
}
