package fr.ifremer.reefdb.dao.data.survey;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.dao.administration.program.Program;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.Quser;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.dao.data.survey.CampaignImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.OccasionImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.Survey;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationImpl;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.PositionningSystemImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.PmfmImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValueImpl;
import fr.ifremer.quadrige3.core.dao.system.*;
import fr.ifremer.quadrige3.core.dao.system.synchronization.SynchronizationStatus;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.program.ReefDbProgramDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.data.measurement.ReefDbMeasurementDao;
import fr.ifremer.reefdb.dao.data.photo.ReefDbPhotoDao;
import fr.ifremer.reefdb.dao.data.samplingoperation.ReefDbSamplingOperationDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dao.technical.Geometries;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.OccasionDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.StateValues;
import fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbSurveyDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbSurveyDao")
public class ReefDbSurveyDaoImpl extends SurveyDaoImpl implements ReefDbSurveyDao, InitializingBean {

    private final static Log LOG = LogFactory.getLog(ReefDbSurveyDaoImpl.class);

    @Resource(name = "reefDbReferentialDao")
    protected ReefDbReferentialDao referentialDao;

    @Resource(name = "reefDbMonitoringLocationDao")
    protected ReefDbMonitoringLocationDao monitoringLocationDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefDbProgramDao")
    protected ReefDbProgramDao programDao;

    @Resource(name = "reefDbSamplingOperationDao")
    protected ReefDbSamplingOperationDao samplingOperationDao;

    @Resource(name = "reefDbMeasurementDao")
    protected ReefDbMeasurementDao measurementDao;

    @Resource(name = "reefDbPhotoDao")
    protected ReefDbPhotoDao photoDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "objectTypeDao")
    protected ObjectTypeDao objectTypeDao;

    @Resource(name = "validationHistoryDao")
    protected ValidationHistoryExtendDao validationHistoryDao;

    @Resource(name = "qualificationHistoryDao")
    protected QualificationHistoryDao qualificationHistoryDao;

    @Resource
    protected ReefDbConfiguration config;

    private int pmfmIdDepthValues;

    private int unitIdMeter;

    /**
     * <p>Constructor for ReefDbSurveyDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbSurveyDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public void afterPropertiesSet() {
        initConstants();
    }

    private void initConstants() {

        pmfmIdDepthValues = config.getDepthValuesPmfmId();
        unitIdMeter = UnitId.METER.getValue();

        if (config.isDbCheckConstantsEnable()) {
            Session session = getSessionFactory().openSession();
            try {
                Assert.notNull(session.get(PmfmImpl.class, pmfmIdDepthValues));
                Assert.notNull(session.get(UnitImpl.class, unitIdMeter));

            } finally {
                Daos.closeSilently(session);
            }

        }

    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<SurveyDTO> getSurveysByCriteria(Integer campaignId, Collection<String> programCodes, Integer locationId, String comment, Integer stateId, Integer synchronizationStatusId,
                                                Date startDate, Date endDate, boolean strictDate) {

        if (CollectionUtils.isEmpty(programCodes))
            return new ArrayList<>();

        // load query from named query
        StringBuilder queryString = new StringBuilder(getSession().getNamedQuery("surveysByCriteria").getQueryString());

        // add more predicates
        if (stateId != null) {
            if (stateId.equals(StateValues.CONTROLLED.ordinal())) {
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyControlDt is not null AND s.surveyValidDt is null");
            } else if (stateId.equals(StateValues.VALIDATED.ordinal())) {
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyValidDt is not null");
            }
        }

        // synchronization status
        if (synchronizationStatusId != null) {
            SynchronizationStatusValues synchronizationStatus = SynchronizationStatusValues.fromOrdinal(synchronizationStatusId);
            // Special case if DIRTY : add also READY_TO_SYNC (see mantis #26500)
            if (synchronizationStatus == SynchronizationStatusValues.DIRTY) {
                queryString.append(System.lineSeparator())
                        .append("AND s.synchronizationStatus IN ('")
                        .append(synchronizationStatus.getCode())
                        .append("','")
                        .append(SynchronizationStatusValues.READY_TO_SYNCHRONIZE.getCode())
                        .append("')");
            } else {
                queryString.append(System.lineSeparator())
                        .append("AND s.synchronizationStatus = '")
                        .append(synchronizationStatus.getCode())
                        .append('\'');
            }
        } else {
            // ignore DELETED rows by default
            queryString.append(System.lineSeparator())
                    .append("AND s.synchronizationStatus <> '")
                    .append(SynchronizationStatus.DELETED.getValue())
                    .append('\'');
        }

        if (startDate != null && endDate != null) {
            if (startDate == endDate) {
                // equals
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyDt = ");
                queryString.append(Daos.convertDateOnlyToSQLString(startDate));
            } else {
                // between TODO vérifier l'inclusion ou l'exclusion de la date de fin
                queryString.append(System.lineSeparator());
                queryString.append("AND s.surveyDt >=  ");
                queryString.append(Daos.convertDateOnlyToSQLString(startDate));
                queryString.append("AND s.surveyDt <=  ");
                queryString.append(Daos.convertDateOnlyToSQLString(endDate));
            }
        } else if (startDate != null) {
            // >= or >
            queryString.append(System.lineSeparator());
            queryString.append("AND s.surveyDt ");
            queryString.append((strictDate ? "> " : ">= "));
            queryString.append(Daos.convertDateOnlyToSQLString(startDate));
        } else if (endDate != null) {
            // <= or <
            queryString.append(System.lineSeparator());
            queryString.append("AND s.surveyDt ");
            queryString.append((strictDate ? "< " : "<= "));
            queryString.append(Daos.convertDateOnlyToSQLString(endDate));
        }

        // crete new query from query string
        Query q = getSession().createQuery(queryString.toString());
        setQueryParams(q, "surveysByCriteria",
                "campaignId", IntegerType.INSTANCE, campaignId,
                "locationId", IntegerType.INSTANCE, locationId,
                "comment", StringType.INSTANCE, comment,
                "pmfmIdDepthValues", IntegerType.INSTANCE, pmfmIdDepthValues);
        q.setParameterList("programCodes", programCodes);

        Iterator<Object[]> it = q.iterate();

        List<SurveyDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toSurveyDTO(Arrays.asList(source).iterator()));
        }

        // Load all geometries
        loadGeometries(result);

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Long countSurveysWithProgramAndLocations(String programCode, List<Integer> locationIds) {

        Query q;
        Set<Integer> locationIdsSet = locationIds != null ? locationIds.stream().filter(Objects::nonNull).collect(Collectors.toSet()) : null;
        if (CollectionUtils.isNotEmpty(locationIdsSet)) {
            q = createQuery("countSurveysByProgramAndLocations", "programCode", StringType.INSTANCE, programCode);
            q.setParameterList("locationIds", locationIdsSet);
        } else {
            q = createQuery("countSurveysByProgram", "programCode", StringType.INSTANCE, programCode);
        }
        return (Long) q.uniqueResult();
    }

    @Override
    public Long countSurveysWithProgramLocationAndOutsideDates(String programCode, int appliedStrategyId, int locationId, Date startDate, Date endDate) {

        return queryCount("countSurveysByProgramLocationAndOutsideDates",
                "programCode", StringType.INSTANCE, programCode,
                "locationId", IntegerType.INSTANCE, locationId,
                "appliedStrategyId", IntegerType.INSTANCE, appliedStrategyId,
                "startDate", DateType.INSTANCE, startDate,
                "endDate", DateType.INSTANCE, endDate,
                "synchronizationStatusToIgnore", StringType.INSTANCE, SynchronizationStatus.DELETED.getValue()
        );
    }

    @Override
    public Long countSurveysWithProgramLocationAndInsideDates(String programCode, int appliedStrategyId, int locationId, Date startDate, Date endDate) {

        return queryCount("countSurveysByProgramLocationAndInsideDates",
                "programCode", StringType.INSTANCE, programCode,
                "locationId", IntegerType.INSTANCE, locationId,
                "appliedStrategyId", IntegerType.INSTANCE, appliedStrategyId,
                "startDate", DateType.INSTANCE, startDate,
                "endDate", DateType.INSTANCE, endDate,
                "synchronizationStatusToIgnore", StringType.INSTANCE, SynchronizationStatus.DELETED.getValue()
        );
    }

    /** {@inheritDoc} */
    @Override
    public SurveyDTO getSurveyById(int surveyId, boolean dontExcludePmfm) {

        Object[] row = queryUnique("surveyById",
                "surveyId", IntegerType.INSTANCE, surveyId,
                "pmfmIdDepthValues", IntegerType.INSTANCE, pmfmIdDepthValues);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load survey with id = " + surveyId);
        }

        SurveyDTO survey = toSurveyDTO(Arrays.asList(row).iterator());

        // get observers
        survey.setObservers(getObservers(survey.getId()));
        survey.setObserversLoaded(true);

        // Get geometry
        loadGeometry(survey);

        // Get measurements
        if (dontExcludePmfm) {
            loadMeasurements(survey);
        } else {
            loadMeasurements(survey, pmfmIdDepthValues);
        }

        // Add sampling operations
        survey.setSamplingOperations(samplingOperationDao.getSamplingOperationsBySurveyId(survey.getId(), true /*withIndividualMeasurements*/));
        survey.setSamplingOperationsLoaded(true);

        // Add photos
        survey.setPhotos(photoDao.getPhotosBySurveyId(survey.getId()));
        survey.setPhotosLoaded(true);

        // Associate Sampling Operations in Photos
        if (!survey.isPhotosEmpty() && !survey.isSamplingOperationsEmpty()) {
            Map<Integer, SamplingOperationDTO> samplingOperationMap = ReefDbBeans.mapById(survey.getSamplingOperations());
            for (PhotoDTO photo : survey.getPhotos()) {
                if (photo.getSamplingOperation() != null && photo.getSamplingOperation().getId() != null) {
                    // photo.getSamplingOperation() has only an Id
                    SamplingOperationDTO samplingOperation = samplingOperationMap.get(photo.getSamplingOperation().getId());
                    if (samplingOperation == null) {
                        throw new DataRetrievalFailureException(
                                String.format("the sampling operation (id=%s) associated with the photo (id=%s) has not been loaded",
                                        photo.getSamplingOperation().getId(), photo.getId()));
                    }
                    photo.setSamplingOperation(samplingOperation);
                }
            }
        }

        return survey;
    }

    private void loadGeometries(List<SurveyDTO> surveys) {

        Map<Integer, String> geometriesById = new HashMap<>();
        Map<Integer, SurveyDTO> surveysById = new HashMap<>(surveys.size());
        // Collect surveys to load
        surveys.forEach(survey -> {
            if (survey.isActualPosition()) {
                surveysById.put(survey.getId(), survey);
            } else {
                // Reset positioning system
                survey.setPositioning(null);
            }
        });

        List<List<Integer>> partitions = ListUtils.partition(ImmutableList.copyOf(surveysById.keySet()), 1000);
        partitions.forEach(partition -> {
            Query query = createSQLQuery("surveyGeometries");
            query.setParameterList("surveyIds", partition);
            @SuppressWarnings("unchecked") List<Object[]> rows = query.list();
            if (CollectionUtils.isNotEmpty(rows)) {
                rows.stream()
                    .filter(Objects::nonNull)
                    .forEach(row -> geometriesById.put((Integer) row[0], (String) row[1]));
            }
        });
        // Fill each survey
        geometriesById.forEach((surveyId, geometry) -> surveysById.get(surveyId).setCoordinate(Geometries.getCoordinate(geometry)));
    }

    private void loadGeometry(SurveyDTO survey) {

        if (survey.isActualPosition()) {
            // Load geometry
            String geometry = queryUniqueTyped("surveyGeometry", "surveyId", IntegerType.INSTANCE, survey.getId());
            survey.setCoordinate(Geometries.getCoordinate(geometry));
        } else {
            // Reset positioning system
            survey.setPositioning(null);
        }
    }

    private void loadMeasurements(SurveyDTO survey, Integer... excludePmfmId) {

        List<MeasurementDTO> allMeasurements = measurementDao.getMeasurementsBySurveyId(survey.getId(), excludePmfmId);
        if (CollectionUtils.isNotEmpty(allMeasurements)) {

            // Iterate on all measurement to split them
            for (MeasurementDTO measurement : allMeasurements) {

                if (measurement.getIndividualId() != null) {
                    // If measurement has an individualId, split to other list
                    survey.addIndividualMeasurements(measurement);
                } else {
                    survey.addMeasurements(measurement);
                }
            }
        }

        survey.setMeasurementsLoaded(true);
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> getObservers(int surveyId) {

        List<PersonDTO> observers = Lists.newArrayList();
        Iterator<Integer> it = queryIteratorTyped("quserIdsBySurveyId", "surveyId", IntegerType.INSTANCE, surveyId);
        while (it.hasNext()) {
            Integer quserId = it.next();
            if (quserId != null) {
                observers.add(quserDao.getUserById(quserId));
            }
        }
        return observers;
    }

    /** {@inheritDoc} */
    @Override
    public SurveyDTO save(SurveyDTO bean) {
        Assert.notNull(bean);
        Assert.notNull(bean.getLocation());
        Assert.notNull(bean.getLocation().getId());
        Assert.notNull(bean.getRecorderDepartment());
        Assert.notNull(bean.getRecorderDepartment().getId());
        Assert.notNull(bean.getProgram());
        Assert.notNull(bean.getProgram().getCode());
        Assert.notNull(bean.getDate());

        Survey entity;
        boolean programChanged = false;
        if (bean.getId() == null) {
            entity = Survey.Factory.newInstance();
        } else {
            entity = get(bean.getId());
            if (entity == null) {
                throw new DataRetrievalFailureException("Could not retrieve survey with id=" + bean.getId());
            }

            // Check if program changed (if so, will update children)
            programChanged = !hasSameProgram(bean, entity);
        }

        beanToEntity(bean, entity);
        update(entity);

        // save coordinates
        saveGeometry(bean, entity);

        // save measurements
        if (bean.isMeasurementsLoaded()) {
            List<MeasurementDTO> allMeasurements = Lists.newArrayList();
            allMeasurements.addAll(bean.getMeasurements());
            allMeasurements.addAll(bean.getIndividualMeasurements());
            measurementDao.saveMeasurementsBySurveyId(bean.getId(), allMeasurements, pmfmIdDepthValues);
        }

        // save sampling operations
        if (bean.isSamplingOperationsLoaded()) {
            samplingOperationDao.saveSamplingOperationsBySurveyId(bean.getId(), bean.getSamplingOperations());
        }

        // save photos
        if (bean.isPhotosLoaded()) {
            photoDao.savePhotosBySurveyId(bean.getId(), bean.getPhotos());
        }

        // Update program on children entities (mantis #37520)
        if (programChanged) {
            applySurveyProgramsToChildren(bean.getId());
        }

        getSession().flush();
        getSession().clear();

        // Update flags has_meas on survey and sampling operations (mantis #28257)
        updateHasMeasFlag(bean.getId());
        samplingOperationDao.updateHasMeasFlagBySurveyId(bean.getId());



        return bean;
    }

    /** {@inheritDoc} */
    @Override
    public void remove(List<Integer> surveyIds) {
        if (CollectionUtils.isNotEmpty(surveyIds)) {
            for (Integer surveyId : surveyIds) {
                remove(surveyId);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void remove(Integer surveyId) {

        if (surveyId != null) {
            // if needed, do some removes here before remove main entity

            photoDao.removeBySurveyId(surveyId);
            measurementDao.removeAllMeasurementsBySurveyId(surveyId);
            samplingOperationDao.removeBySurveyId(surveyId);

            super.remove(surveyId);
            getSession().flush();
            getSession().clear();
        } // else this is temporary unsaved survey, no need to remove from DB
    }

    /** {@inheritDoc} */
    @Override
    public void validate(Integer surveyId, Date validationDate, String validationComment, Integer validatorId, boolean readyToSynchronize) {
        Survey survey = get(surveyId);

        // validation date
        survey.setSurveyValidDt(validationDate);

        // qualification comment
        String oldSurveyValidCm = survey.getSurveyValidCm();
        if (StringUtils.isBlank(oldSurveyValidCm)) {
            // try get old comment from history (mantis #44300)
            oldSurveyValidCm = validationHistoryDao.getLastValidationComment(surveyId, ObjectTypeCode.SURVEY.getValue());
        }
        survey.setSurveyValidCm(validationComment);

        // synchronization status
        survey.setSynchronizationStatus(readyToSynchronize
                ? SynchronizationStatus.READY_TO_SYNCHRONIZE.getValue()
                : SynchronizationStatus.DIRTY.getValue());

        // scope (mantis #28257)
        survey.setSurveyScope(Daos.convertToString(true));

        // geometry validation date (mantis #28257)
        survey.setSurveyGeometryValidDt(validationDate);

        // insert a validation history line
        ValidationHistory history = ValidationHistory.Factory.newInstance(
                surveyId,
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId)
        );
        history.setValidHistPreviousCm(oldSurveyValidCm);
        history.setValidHistOperationCm(validationComment);
        history.setUpdateDt(new Timestamp(validationDate.getTime()));

        validationHistoryDao.create(history);

        update(survey);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void unvalidate(Integer surveyId, Date unvalidationDate, String unvalidationComment, Integer validatorId) {
        Survey survey = get(surveyId);

        // Reset Validation date
        survey.setSurveyValidDt(null);

        // Reset Qualification date & comment (mantis #0026463)
        boolean wasQualified = QualityFlagCode.NOT_QUALIFIED.getValue().equals(survey.getQualityFlag().getQualFlagCd());
        survey.setSurveyQualifDt(null);
        survey.setSurveyQualifCm(null);

        String oldSurveyValidCm = survey.getSurveyValidCm();
        survey.setSurveyValidCm(null);

        // Synchronization status
        survey.setSynchronizationStatus(SynchronizationStatus.DIRTY.getValue());

        // Quality flag
        QualityFlag oldQualityFlag = survey.getQualityFlag();
        survey.setQualityFlag(getDefaultQualityFlag());

        // Reset geometry validation date (mantis 28257)
        survey.setSurveyGeometryValidDt(null);

        // insert a validation history line
        ValidationHistory validationHistory = ValidationHistory.Factory.newInstance(
                surveyId,
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId)
        );
        validationHistory.setValidHistPreviousCm(oldSurveyValidCm);
        validationHistory.setValidHistOperationCm(unvalidationComment);
        validationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
        validationHistoryDao.create(validationHistory);

        // find any qualified data
        wasQualified |= isQualified(surveyId);

        // insert a qualification history line
        QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(surveyId),
                objectTypeDao.get(ObjectTypeCode.SURVEY.getValue()),
                quserDao.get(validatorId));
        qualificationHistory.setQualHistOperationCm(
            wasQualified
                ? t("reefdb.service.observation.qualificationHistory.unqualify.message")
                : t("reefdb.service.observation.qualificationHistory.unvalidate.message")
        );
        qualificationHistory.setQualityFlag(oldQualityFlag);
        qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
        qualificationHistoryDao.create(qualificationHistory);

        update(survey);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void updateSurveysControlDate(Collection<Integer> controlledElementsPks, Date controlDate) {
        createQuery("updateSurveyControlDate")
                .setParameterList("surveyIds", controlledElementsPks)
                .setParameter("controlDate", controlDate).executeUpdate();
//        getSession().flush();
//        getSession().clear();
    }

    @Override
    public boolean isQualified(int surveyId) {

        return
            // count qualified measurements
            !measurementDao.getQualifiedMeasurementBySurveyId(surveyId).isEmpty()
            || !measurementDao.getQualifiedTaxonMeasurementBySurveyId(surveyId).isEmpty()
            // count qualified photos
            || !photoDao.getQualifiedPhotoBySurveyId(surveyId).isEmpty()
            // count qualified sampling operations
            || !samplingOperationDao.getQualifiedSamplingOperationBySurveyId(surveyId, true).isEmpty();
    }

    // ------------------------------------------------
    // ---------------PRIVATE METHODS------------------
    // ------------------------------------------------
    private SurveyDTO toSurveyDTO(Iterator<Object> source) {
        SurveyDTO result = ReefDbBeanFactory.newSurveyDTO();
        result.setId((Integer) source.next());

        // location
        Integer locationId = (Integer) source.next();
        result.setLocation(monitoringLocationDao.getLocationById(locationId));

        // department
        Integer recorderDepartmentId = (Integer) source.next();
        result.setRecorderDepartment(departmentDao.getDepartmentById(recorderDepartmentId));
        result.setInheritedRecorderDepartmentIds(departmentDao.getInheritedRecorderDepartmentIds(recorderDepartmentId));

        // Apply DB timezone to survey date (Mantis #41597)
        result.setDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), config.getDbTimezone()));

        result.setTime(Daos.convertToInteger((Number) source.next()));
        result.setName((String) source.next());
        result.setComment((String) source.next());
        result.setControlDate(Daos.convertToDate(source.next()));
        result.setValidationDate(Daos.convertToDate(source.next()));
        result.setValidationComment((String) source.next());
        result.setQualificationDate(Daos.convertToDate(source.next()));
        result.setQualificationComment((String) source.next());
        result.setUpdateDate(Daos.convertToDate(source.next()));
        result.setPreciseDepth(Daos.convertToDouble((Float) source.next()));

        // get depth value and analyst (Mantis #47991)
        Measurement depthMeasurement = (Measurement) source.next();
        if (depthMeasurement != null) {
            if (depthMeasurement.getQualitativeValue() != null)
                result.setDepth(referentialDao.getDepthValueById(depthMeasurement.getQualitativeValue().getQualValueId()));
            if (depthMeasurement.getDepartment() != null)
                result.setDepthAnalyst(departmentDao.getDepartmentById(depthMeasurement.getDepartment().getDepId()));
        }

        // program
        String programCode = (String) source.next();
        if (StringUtils.isNotBlank(programCode)) {
            result.setProgram(programDao.getProgramByCode(programCode));
        }

        // campaign
        CampaignDTO campaign = ReefDbBeanFactory.newCampaignDTO();
        campaign.setId((Integer) source.next());
        campaign.setName((String) source.next());
        campaign.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), config.getDbTimezone()));
        campaign.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(source.next()), config.getDbTimezone()));
        if (campaign.getId() != null) {
            result.setCampaign(campaign);
        }

        // get positionning system id
        Integer positioningSystemId = (Integer) source.next();
        if (positioningSystemId != null) {
            result.setPositioning(referentialDao.getPositioningSystemById(positioningSystemId));
        }

        result.setPositioningComment((String) source.next());

        // occasion
        OccasionDTO occasion = ReefDbBeanFactory.newOccasionDTO();
        occasion.setId((Integer) source.next());
        occasion.setName((String) source.next());
        if (occasion.getId() != null) {
            result.setOccasion(occasion);
        }

        // is actual position ?
        result.setActualPosition(Daos.safeConvertToBoolean(source.next(), false));

        // synchronization status
        result.setSynchronizationStatus(SynchronizationStatusValues.toSynchronizationStatusDTO((String) source.next()));

        return result;
    }

    private void beanToEntity(SurveyDTO bean, Survey entity) {

        // mandatory attributes

        // quality flag
        if (entity.getQualityFlag() == null) {
            // load a default quality flag
            entity.setQualityFlag(getDefaultQualityFlag());
        }

        // monitoring location
        if (entity.getMonitoringLocation() == null
                || entity.getMonitoringLocation().getMonLocId() == null
                || !entity.getMonitoringLocation().getMonLocId().equals(bean.getLocation().getId())) {
            entity.setMonitoringLocation(load(MonitoringLocationImpl.class, bean.getLocation().getId()));
        }

        // UT format from monitoring location
        entity.setSurveyUtFormat(entity.getMonitoringLocation().getMonLocUtFormat());

        // Recorder Department (Mantis #42615 Only if REC_DEP_ID is null)
        if (entity.getRecorderDepartment() == null) {
            entity.setRecorderDepartment(load(DepartmentImpl.class, bean.getRecorderDepartment().getId()));
        }

        // date
        Date beanDate = Dates.convertToDate(bean.getDate(), config.getDbTimezone());
        if (entity.getSurveyDt() == null || !entity.getSurveyDt().equals(beanDate)) {
            entity.setSurveyDt(beanDate);
        }

        // program
        if (bean.getProgram() != null) {
            // create a list to trace not updated items, to be able to removeMeasurementsByIds them later
            Set<Program> remainingPrograms = Sets.newHashSet(entity.getPrograms());
            // load item to store
            Program program = load(ProgramImpl.class, bean.getProgram().getCode());
            if (!entity.getPrograms().contains(program)) {
                entity.addPrograms(program);
            }
            remainingPrograms.remove(program);
            // removeMeasurementsByIds unchanged item
            entity.getPrograms().removeAll(remainingPrograms);
        } else {
            entity.getPrograms().clear();
        }

        // synchronization status: force to DIRTY
        entity.setSynchronizationStatus(SynchronizationStatus.DIRTY.getValue());

        // first save if new entity
        if (entity.getSurveyId() == null) {
            create(entity);
            bean.setId(entity.getSurveyId());
        }

        // optional attributes
        entity.setSurveyTime(bean.getTime());
        entity.setSurveyLb(bean.getName());
        entity.setSurveyCm(bean.getComment());

        // Bottom depth and unit
        if (bean.getPreciseDepth() == null) {
            entity.setSurveyBottomDepth(null);
            // Remove unit
            entity.setBottomDepthUnit(null);
        }
        else {
            // TODO fix mantis 28678 (conversion error : use double instead ? e.g. 1.4 -> 1.39999)
            entity.setSurveyBottomDepth(bean.getPreciseDepth().floatValue());

            // Set unit as meter (see mantis #28678)
            entity.setBottomDepthUnit(load(UnitImpl.class, unitIdMeter));
        }

        // depth measurement
        if (bean.getDepth() != null) {
            // read existing depth value or create if not exists
            Measurement depthValue = measurementDao.getMeasurementEntityBySurveyId(entity.getSurveyId(), pmfmIdDepthValues, true);

            // affect qualitative value
            depthValue.setQualitativeValue(load(QualitativeValueImpl.class, bean.getDepth().getId()));

            // Mantis #0027878 add missing properties
            depthValue.setRecorderDepartment(entity.getRecorderDepartment());
            if (depthValue.getPrograms() == null) {
                depthValue.setPrograms(new ArrayList<>());
            } else {
                depthValue.getPrograms().clear();
            }
            depthValue.getPrograms().addAll(entity.getPrograms());

            // add analyst (Mantis #47991)
            depthValue.setDepartment(bean.getDepthAnalyst() != null
                    ? load(DepartmentImpl.class, bean.getDepthAnalyst().getId())
                    : null);

            getSession().save(depthValue);

        } else {
            // read existing depth value
            Measurement depthValue = measurementDao.getMeasurementEntityBySurveyId(entity.getSurveyId(), pmfmIdDepthValues, false);
            if (depthValue != null) {
                // delete the measurement
                measurementDao.removeMeasurementBySurveyId(entity.getSurveyId(), pmfmIdDepthValues);
            }
        }

        // campaign
        if (bean.getCampaign() != null) {
            entity.setCampaign(load(CampaignImpl.class, bean.getCampaign().getId()));
        } else {
            entity.setCampaign(null);
        }

        // positionning system
        if (bean.getPositioning() != null) {
            entity.setPositionningSystem(load(PositionningSystemImpl.class, bean.getPositioning().getId()));
        } else {
            entity.setPositionningSystem(null);
        }
        entity.setSurveyPositionCm(bean.getPositioningComment());

        // occasion
        if (bean.getOccasion() != null) {
            entity.setOccasion(load(OccasionImpl.class, bean.getOccasion().getId()));
        } else {
            entity.setOccasion(null);
        }

        // observers
        if (bean.isObserversLoaded()) {
            if (!bean.isObserversEmpty()) {

                Map<Integer, Quser> remainingUsers = ReefDbBeans.mapByProperty(entity.getQusers(), "quserId");

                for (PersonDTO observer : bean.getObservers()) {
                    if (remainingUsers.remove(observer.getId()) == null) {
                        // add user
                        Quser quser = load(QuserImpl.class, observer.getId());
                        entity.addQusers(quser);
                    }
                }

                if (!remainingUsers.isEmpty()) {
                    entity.getQusers().removeAll(remainingUsers.values());
                }

            } else if (entity.getQusers() != null) {
                entity.getQusers().clear();
            }
        }

        // Reset control date (Mantis #59449)
        if (bean.getControlDate() == null) {
            entity.setSurveyControlDt(null);
        }
    }

    private boolean hasSameProgram(SurveyDTO bean, Survey entity){

        String beanProgCd = bean.getProgram() != null ?  bean.getProgram().getCode() : null;

        List<String> entityProgCds = CollectionUtils.isNotEmpty(entity.getPrograms())
                ? ReefDbBeans.collectProperties(entity.getPrograms(), "progCd")
                : null;

        return (beanProgCd == null && entityProgCds == null)
                || (CollectionUtils.size(entityProgCds) == 1 && Objects.equals(beanProgCd, entityProgCds.get(0)));
    }

    private void saveGeometry(SurveyDTO bean, Survey entity) {

        // If user has filled coordinates
        if (Geometries.isValid(bean.getCoordinate())) {

            if (Geometries.isPoint(bean.getCoordinate())) {

                // point
                if (entity.getSurveyPoints() != null && entity.getSurveyPoints().size() == 1) {

                    SurveyPoint surveyPointToUpdate = entity.getSurveyPoints().iterator().next();
                    if (!Geometries.equals(bean.getCoordinate(), surveyPointToUpdate.getSurveyPosition())) {
                        surveyPointToUpdate.setSurveyPosition(Geometries.getPointPosition(bean.getCoordinate()));
                        getSession().update(surveyPointToUpdate);
                    }

                } else {
                    // create a survey point
                    SurveyPoint surveyPoint = SurveyPoint.Factory.newInstance();
                    surveyPoint.setSurvey(entity);
                    surveyPoint.setSurveyPosition(Geometries.getPointPosition(bean.getCoordinate()));
                    getSession().save(surveyPoint);
                    entity.getSurveyPoints().clear();
                    entity.addSurveyPoints(surveyPoint);
                }
                entity.getSurveyAreas().clear();

            }

            // Area  geometry
            else {

                // Survey has an existing area: update it
                if (entity.getSurveyAreas() != null && entity.getSurveyAreas().size() == 1) {

                    SurveyArea surveyAreaToUpdate = entity.getSurveyAreas().iterator().next();
                    if (!Geometries.equals(bean.getCoordinate(), surveyAreaToUpdate.getSurveyPosition())) {
                        surveyAreaToUpdate.setSurveyPosition(Geometries.getAreaPosition(bean.getCoordinate()));
                        getSession().update(surveyAreaToUpdate);
                    }

                }

                // Survey has NO area: create new
                else {
                    // create a survey area
                    SurveyArea surveyArea = SurveyArea.Factory.newInstance();
                    surveyArea.setSurvey(entity);
                    surveyArea.setSurveyPosition(Geometries.getAreaPosition(bean.getCoordinate()));
                    getSession().save(surveyArea);
                    entity.getSurveyAreas().clear();
                    entity.addSurveyAreas(surveyArea);
                }
                entity.getSurveyPoints().clear();
            }

            // Update flag to known is coordinate are filled by user or not (mantis #28257)
            entity.setSurveyActualPosition(Daos.convertToString(true));

            // clear unused coordinates
            entity.getSurveyLines().clear();

        }

        // If a location has been define
        else if (bean.getLocation() != null && bean.getLocation().getId() != null) {

            // Copy geom from this location (mantis #28688)
            saveGeometryAndPositioningFromLocation(entity, bean.getLocation().getId());
        }

        // No geometry could be set: reset all geometry data
        else {
            removeGeometry(entity);
        }

        // Update survey
        // This is need because geometries list could have change (e.g. entity.getSurveyAreas().clear())
        update(entity);
    }

    private void saveGeometryAndPositioningFromLocation(Survey entity, int monLocId) {

        // Retrieve the location entity
        MonitoringLocation location = load(MonitoringLocationImpl.class, monLocId);

        // Point geometry
        if (CollectionUtils.size(location.getMonLocPoints()) == 1) {
            MonLocPoint locationPoint = CollectionUtils.extractSingleton(location.getMonLocPoints());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSurveyPoints()) == 1) {
                SurveyPoint surveyPoint = CollectionUtils.extractSingleton(entity.getSurveyPoints());
                if (!Objects.equals(locationPoint.getMonLocPosition(), surveyPoint.getSurveyPosition())) {
                    surveyPoint.setSurveyPosition(locationPoint.getMonLocPosition());
                    getSession().update(surveyPoint);
                }
            }

            // No existing area: create new
            else {
                SurveyPoint surveyPoint = SurveyPoint.Factory.newInstance();
                surveyPoint.setSurvey(entity);
                surveyPoint.setSurveyPosition(locationPoint.getMonLocPosition());
                getSession().save(surveyPoint);
                entity.getSurveyPoints().clear();
                entity.addSurveyPoints(surveyPoint);
            }

            // Clean unused
            entity.getSurveyLines().clear();
            entity.getSurveyAreas().clear();
        }

        // Line geometry
        else if (CollectionUtils.size(location.getMonLocLines()) == 1) {
            MonLocLine locationLine = CollectionUtils.extractSingleton(location.getMonLocLines());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSurveyLines()) == 1) {
                SurveyLine surveyLine = CollectionUtils.extractSingleton(entity.getSurveyLines());
                if (!Objects.equals(locationLine.getMonLocPosition(), surveyLine.getSurveyPosition())) {
                    surveyLine.setSurveyPosition(locationLine.getMonLocPosition());
                    getSession().update(surveyLine);
                }
            }

            // No existing area: create new
            else {
                SurveyLine surveyLine = SurveyLine.Factory.newInstance();
                surveyLine.setSurvey(entity);
                surveyLine.setSurveyPosition(locationLine.getMonLocPosition());
                getSession().save(surveyLine);
                entity.getSurveyLines().clear();
                entity.addSurveyLines(surveyLine);
            }

            // Clean unused
            entity.getSurveyPoints().clear();
            entity.getSurveyAreas().clear();
        }

        // Area geometry
        else if (CollectionUtils.size(location.getMonLocAreas()) == 1) {
            MonLocArea locationArea = CollectionUtils.extractSingleton(location.getMonLocAreas());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSurveyAreas()) == 1) {
                SurveyArea surveyArea = CollectionUtils.extractSingleton(entity.getSurveyAreas());
                if (!Objects.equals(locationArea.getMonLocPosition(), surveyArea.getSurveyPosition())) {
                    surveyArea.setSurveyPosition(locationArea.getMonLocPosition());
                    getSession().update(surveyArea);
                }
            }

            // No existing area: create new
            else {
                SurveyArea surveyArea = SurveyArea.Factory.newInstance();
                surveyArea.setSurvey(entity);
                surveyArea.setSurveyPosition(locationArea.getMonLocPosition());
                getSession().save(surveyArea);
                entity.getSurveyAreas().clear();
                entity.addSurveyAreas(surveyArea);
            }

            // Clean unused
            entity.getSurveyPoints().clear();
            entity.getSurveyLines().clear();
        }

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        entity.setSurveyActualPosition(Daos.convertToString(false));

        // Apply positioning system from location (see mantis #28706)
        entity.setPositionningSystem(location.getPositionningSystem());

    }

    private void removeGeometry(Survey entity) {

        entity.getSurveyPoints().clear();
        entity.getSurveyAreas().clear();
        entity.getSurveyLines().clear();

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        entity.setSurveyActualPosition(Daos.convertToString(false));
    }

    /**
     * return the default quality flag
     */
    private QualityFlag getDefaultQualityFlag() {
        return load(QualityFlagImpl.class, QualityFlagCode.NOT_QUALIFIED.getValue()); // = non qualifié
    }

}
