package fr.ifremer.reefdb.dao.data.photo;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.data.photo.Photo;
import fr.ifremer.quadrige3.core.dao.data.photo.PhotoDao;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>ReefDbPhotoDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbPhotoDao extends PhotoDao {
    
    /**
     * <p>getPhotosBySurveyId.</p>
     *
     * @param surveyId a int.
     * @return a {@link java.util.List} object.
     */
    List<PhotoDTO> getPhotosBySurveyId(int surveyId);
    
    /**
     * <p>savePhotosBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param photos a {@link java.util.Collection} object.
     */
    void savePhotosBySurveyId(int surveyId, Collection<PhotoDTO> photos);

    /**
     * <p>removeBySurveyId.</p>
     *
     * @param surveyId a int.
     */
    void removeBySurveyId(int surveyId);
    
    /**
     * <p>removeBySamplingOperationId.</p>
     *
     * @param samplingOperationId a int.
     */
    void removeBySamplingOperationId(int samplingOperationId);

    /**
     * Validate all photos attached to the survey and to the inner sampling operations
     *
     * @param surveyIds the survey id
     * @param validationDate the validation date
     */
    int validateBySurveyIds(Collection<Integer> surveyIds, Date validationDate);

    /**
     * Unvalidate all photos attached to the survey and to the inner sampling operations
     *
     * @param surveyIds        the survey id
     * @param unvalidationDate
     * @param validatorId
     */
    int unvalidateBySurveyIds(Collection<Integer> surveyIds, Date unvalidationDate, int validatorId);
    int unvalidateBySamplingOperationIds(List<Integer> samplingOperationIds, Date unvalidationDate, int validatorId);

    List<Photo> getQualifiedPhotoBySurveyId(int surveyId);
    List<Photo> getQualifiedPhotoBySamplingOperationId(int samplingOperationId);

}
