package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.*;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>ReefDbPmfmDao interface.</p>
 *
 */
public interface ReefDbPmfmDao {

    String ALL_PMFMS_CACHE = "all_pmfms";
    String PMFM_BY_ID_CACHE = "pmfm_by_id";
    String PMFMS_BY_IDS_CACHE = "pmfms_by_ids";
    String PMFMS_BY_CRITERIA_CACHE = "pmfms_by_criteria";

    /**
     * <p>getAllPmfms.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PMFMS_CACHE)
    List<PmfmDTO> getAllPmfms(List<String> statusCodes);

    /**
     * <p>getPmfmById.</p>
     *
     * @param pmfmId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO} object.
     */
    @Cacheable(value = PMFM_BY_ID_CACHE)
    PmfmDTO getPmfmById(int pmfmId);

    /**
     * <p>getPmfmsByIds.</p>
     *
     * @param pmfmIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PMFMS_BY_IDS_CACHE)
    List<PmfmDTO> getPmfmsByIds(List<Integer> pmfmIds);

    /**
     * <p>findPmfms.</p>
     *
     * @param parameterCode a {@link String} object.
     * @param matrixId a {@link Integer} object.
     * @param fractionId a {@link Integer} object.
     * @param methodId a {@link Integer} object.
     * @param pmfmName a {@link String} object.
     * @param statusCodes a {@link List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PMFMS_BY_CRITERIA_CACHE)
    List<PmfmDTO> findPmfms(String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, List<String> statusCodes);

    /**
     * <p>savePmfms.</p>
     *
     * @param pmfms a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_PMFMS_CACHE,
            PMFM_BY_ID_CACHE,
            PMFMS_BY_IDS_CACHE,
            PMFMS_BY_CRITERIA_CACHE,
            ReefDbQualitativeValueDao.QUALITATIVE_VALUES_BY_PMFM_ID_CACHE
    }, allEntries = true)
    void savePmfms(List<? extends PmfmDTO> pmfms);

    /**
     * <p>deletePmfms.</p>
     *
     * @param pmfmIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_PMFMS_CACHE,
            PMFM_BY_ID_CACHE,
            PMFMS_BY_IDS_CACHE,
            PMFMS_BY_CRITERIA_CACHE,
            ReefDbQualitativeValueDao.QUALITATIVE_VALUES_BY_PMFM_ID_CACHE
    }, allEntries = true)
    void deletePmfms(List<Integer> pmfmIds);

    /**
     * <p>replaceTemporaryPmfm.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_PMFMS_CACHE,
            PMFM_BY_ID_CACHE,
            PMFMS_BY_IDS_CACHE,
            PMFMS_BY_CRITERIA_CACHE,
            ReefDbQualitativeValueDao.QUALITATIVE_VALUES_BY_PMFM_ID_CACHE
    }, allEntries = true)
    void replaceTemporaryPmfm(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isPmfmUsedInData.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInData(int pmfmId);

    /**
     * <p>isPmfmUsedInValidatedData.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInValidatedData(int pmfmId);

    /**
     * <p>isPmfmUsedInProgram.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInProgram(int pmfmId);

    /**
     * <p>isPmfmUsedInRules.</p>
     *
     * @param pmfmId a int.
     * @return a boolean.
     */
    boolean isPmfmUsedInRules(int pmfmId);
}
