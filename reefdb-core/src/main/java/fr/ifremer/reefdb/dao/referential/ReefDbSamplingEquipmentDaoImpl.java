package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.SamplingEquipmentDTO;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Sampling Equipment DAO
 * <p/>
 * Created by Ludovic on 17/07/2015.
 */

@Repository("reefDbSamplingEquipmentDao")
public class ReefDbSamplingEquipmentDaoImpl extends SamplingEquipmentDaoImpl implements ReefDbSamplingEquipmentDao {

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("SAMPLING_OPERATION", "SAMPLING_EQUIPMENT_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("SAMPLING_OPERATION", "SAMPLING_OPER_VALID_DT").build();

    @Resource
    protected CacheService cacheService;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbSamplingEquipmentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingEquipmentDTO> getAllSamplingEquipments(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(SAMPLING_EQUIPMENT_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allSamplingEquipments"), statusCodes);

        List<SamplingEquipmentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            SamplingEquipmentDTO samplingEquipment = toSamplingEquipmentDTO(Arrays.asList(source).iterator());
            result.add(samplingEquipment);

            // update cache
            cacheById.put(samplingEquipment.getId(), samplingEquipment);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public SamplingEquipmentDTO getSamplingEquipmentById(int samplingEquipmentId) {

        Object[] source = queryUnique("samplingEquipmentById", "samplingEquipmentId", IntegerType.INSTANCE, samplingEquipmentId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load sampling equipment with id = " + samplingEquipmentId);
        }

        return toSamplingEquipmentDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<SamplingEquipmentDTO> getSamplingEquipmentsByIds(List<Integer> samplingEquipmentIds) {

        Iterator<Object[]> it = createQuery("samplingEquipmentsByIds")
                .setParameterList("samplingEquipmentIds", samplingEquipmentIds)
                .iterate();

        List<SamplingEquipmentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toSamplingEquipmentDTO(Arrays.asList(row).iterator()));
        }

        return result;

    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingEquipmentDTO> findSamplingEquipments(List<String> statusCodes, Integer samplingEquipmentId, Integer unitId) {

        Query query = createQuery("samplingEquipmentsByCriteria",
                "samplingEquipmentId", IntegerType.INSTANCE, samplingEquipmentId,
                "unitId", IntegerType.INSTANCE, unitId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<SamplingEquipmentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toSamplingEquipmentDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingEquipmentDTO> findSamplingEquipmentsByName(List<String> statusCodes, String samplingEquipmentName) {
        Query query = createQuery("samplingEquipmentsByName",
                "samplingEquipmentName", StringType.INSTANCE, samplingEquipmentName);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<SamplingEquipmentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toSamplingEquipmentDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);

    }

    /** {@inheritDoc} */
    @Override
    public void saveSamplingEquipments(List<? extends SamplingEquipmentDTO> samplingEquipments) {
        if (CollectionUtils.isEmpty(samplingEquipments)) {
            return;
        }

        for (SamplingEquipmentDTO samplingEquipment : samplingEquipments) {
            if (samplingEquipment.isDirty()) {
                saveSamplingEquipment(samplingEquipment);
                samplingEquipment.setDirty(false);
            }
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteSamplingEquipments(List<Integer> samplingEquipmentIds) {
        if (samplingEquipmentIds == null) return;
        Set<Integer> idsToRemove = samplingEquipmentIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        for (Integer id : idsToRemove) {
            remove(id);
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporarySamplingEquipment(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isSamplingEquipmentUsedInData(int samplingEquipmentId) {

        return executeMultipleCount(columnNamesByDataTableNames, samplingEquipmentId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isSamplingEquipmentUsedInValidatedData(int samplingEquipmentId) {

        return executeMultipleCountWithNotNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, samplingEquipmentId);
    }

    // private methods
    private void saveSamplingEquipment(SamplingEquipmentDTO samplingEquipment) {
        Assert.notNull(samplingEquipment);
        Assert.notBlank(samplingEquipment.getName());

        if (samplingEquipment.getStatus() == null) {
            samplingEquipment.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(samplingEquipment.getStatus()), "source must have local status");

        SamplingEquipment target;
        if (samplingEquipment.getId() == null) {
            target = SamplingEquipment.Factory.newInstance();
            target.setSamplingEquipmentId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(samplingEquipment.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setSamplingEquipmentNm(samplingEquipment.getName());
        target.setSamplingEquipmentDc(samplingEquipment.getDescription());
        target.setStatus(load(StatusImpl.class, samplingEquipment.getStatus().getCode()));
        target.setSamplingEquipmentSize(Daos.convertToFloat(samplingEquipment.getSize()));
        if (samplingEquipment.getSize() == null || samplingEquipment.getUnit() == null) {
            target.setUnitId(null);
            samplingEquipment.setUnit(null);
        } else {
            target.setUnitId(load(UnitImpl.class, samplingEquipment.getUnit().getId()));
        }
        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        samplingEquipment.setId(target.getSamplingEquipmentId());
    }

    private SamplingEquipmentDTO toSamplingEquipmentDTO(Iterator<Object> source) {
        SamplingEquipmentDTO result = ReefDbBeanFactory.newSamplingEquipmentDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setSize(Daos.convertToDouble((Float) source.next()));
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setUnit(toUnitDTO(source));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

    // TODO replace with getUnitById (change all queries)
    @Deprecated
    private UnitDTO toUnitDTO(Iterator<Object> source) {
        UnitDTO result = ReefDbBeanFactory.newUnitDTO();
        result.setId((Integer) source.next());
        if (result.getId() == null) {
            source.next();
            source.next();
            source.next();
            return null;
        }
        result.setName((String) source.next());
        result.setSymbol((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        return result;
    }
}
