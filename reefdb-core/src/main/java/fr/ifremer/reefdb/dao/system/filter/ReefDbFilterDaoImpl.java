package fr.ifremer.reefdb.dao.system.filter;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.dao.system.filter.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.service.ReefDbBusinessException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbFilterDaoImpl class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
@Repository("reefDbFilterDao")
public class ReefDbFilterDaoImpl extends FilterDaoImpl implements ReefDbFilterDao, InitializingBean {

//    private static final Log log = LogFactory.getLog(ReefDbFilterDaoImpl.class);

    @Resource(name = "filterTypeDao")
    protected FilterTypeDao filterTypeDao;

    @Resource(name = "filterCriteriaTypeDao")
    protected FilterCriteriaTypeDao filterCriteriaTypeDao;

    @Resource(name = "filterOperatorTypeDao")
    protected FilterOperatorTypeDao filterOperatorTypeDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource
    protected CacheService cacheService;

    @Resource
    protected ReefDbConfiguration config;

    /**
     * <p>Constructor for ReefDbFilterDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbFilterDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public void afterPropertiesSet() {

        checkDbConstants();

    }

    /** {@inheritDoc} */
    @Override
    public FilterDTO getFilterById(Integer filterId) {
        Assert.notNull(filterId);

        // query for filter
        Filter f = get(filterId);

        if (f == null) {
            return null;
        }

        Integer filterTypeId = f.getFilterType().getFilterTypeId();
        FilterTypeValues filterType = FilterTypeValues.getFilterType(filterTypeId);
        ExtractionFilterValues extractionFilterType = ExtractionFilterValues.getExtractionFilter(filterTypeId);

        if (filterType == null && extractionFilterType == null) {
            throw new DataRetrievalFailureException("Cannot determine type of filter (id=" + filterId + ", type=" + filterTypeId + ")");
        }

        return initFilterDTO(filterId, f.getFilterNm(), filterTypeId);
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getAllContextFilters(Integer contextId, Integer filterTypeId) {

        List<FilterDTO> result = Lists.newArrayList();
        if (contextId == null && filterTypeId == null) {
            return result;
        }

        Iterator<Object[]> it;

        if (filterTypeId == null) {
            it = queryIterator("filtersByContext",
                    "contextId", IntegerType.INSTANCE, contextId);

        } else if (contextId == null) {
            it = queryIterator("allContextFiltersByType",
                    "filterTypeId", IntegerType.INSTANCE, filterTypeId);

        } else {
            it = queryIterator("allFiltersByContextAndType",
                    "contextId", IntegerType.INSTANCE, contextId,
                    "filterTypeId", IntegerType.INSTANCE, filterTypeId);
        }

        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toFilterDTO(Arrays.asList(row).iterator()));
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<FilterDTO> getAllExtractionFilters(Integer filterTypeId) {
        List<FilterDTO> result = Lists.newArrayList();
        if (filterTypeId != null) {
            Iterator<Object[]> it = queryIterator("allExtractionFiltersByType",
                    "filterTypeId", IntegerType.INSTANCE, filterTypeId);

            while (it.hasNext()) {
                Object[] row = it.next();
                FilterDTO filter = toFilterDTO(Arrays.asList(row).iterator());
                result.add(filter);
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     *
     * Ensures consistency between the bean and the Filter entity.
     * Filter entity is created and saved or updated if a Filter with this bean's ID already exists.
     */
    @Override
    public Filter saveFilter(FilterDTO filter, int quserId) {

        Assert.notNull(filter);
        Assert.notNull(filter.getFilterTypeId());

        Filter target;
        boolean isExtractorFilter;

        FilterTypeValues filterType = FilterTypeValues.getFilterType(filter.getFilterTypeId());
        ExtractionFilterValues extractionFilterType = ExtractionFilterValues.getExtractionFilter(filter.getFilterTypeId());

        if (filterType == null && extractionFilterType == null) {
            throw new ReefDbBusinessException(t("reefdb.error.referential.missing", "FILTER_TYPE"));
        }

        // save a extraction filter
        isExtractorFilter = filterType == null;

        Long filterCount = queryCount("countFiltersByNameAndType",
                "filterName", StringType.INSTANCE, filter.getName(),
                "filterTypeId", IntegerType.INSTANCE, filter.getFilterTypeId());

        if (filter.getId() == null) {
            // check filter name unicity
            if (filterCount > 0) {
                throw new DataIntegrityViolationException("A filter of type " + filterType + " already exists with the name: " + filter.getName());
            } else {
                // create
                target = Filter.Factory.newInstance(quserDao.get(quserId), filterTypeDao.get(filter.getFilterTypeId()));
                target = create(target);

                filter.setId(target.getFilterId());

                // filter name
                target.setFilterNm(filter.getName());

                target.setFilterIsExtract(Daos.convertToString(isExtractorFilter));
            }

        } else {
            // update
            target = get(filter.getId());

            if (StringUtils.isNotEmpty(filter.getName()) && !filter.getName().equals(target.getFilterNm())) {
                // check filter name unicity
                if (filterCount > 0) {
                    throw new DataIntegrityViolationException("A filter of type " + filterType + " already exists with the name: " + filter.getName());
                } else {
                    // update filter name
                    target.setFilterNm(filter.getName());
                }
            }

        }

        int filterOperatorTypeId = -1;
        int filterCriteriaTypeId = -1;

        List<String> criteriaValues = Lists.newArrayList();

        // get the criteria values only if they have been loaded
        if (filter.isFilterLoaded()) {

            // determinate filter type and filtered elements
            if (isExtractorFilter) {

                switch (extractionFilterType) {
                    case ORDER_ITEM_TYPE:
                        filterCriteriaTypeId = FilterCriteriaTypeId.ORDER_ITEM_TYPE_CODE.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.TEXT_EQUAL.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_CODE));
                        break;
                }

            } else {

                switch (filterType) {
                    case LOCATION:
                        filterCriteriaTypeId = FilterCriteriaTypeId.MONITORING_LOCATION_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.MONITORING_LOCATION_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case PROGRAM:
                        filterCriteriaTypeId = FilterCriteriaTypeId.PROGRAM_CODE.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.PROGRAM_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_CODE));
                        break;
                    case CAMPAIGN:
                        filterCriteriaTypeId = FilterCriteriaTypeId.CAMPAIGN_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.CAMPAIGN_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case PMFM:
                        filterCriteriaTypeId = FilterCriteriaTypeId.PMFM_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.PMFM_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case DEPARTMENT:
                        filterCriteriaTypeId = FilterCriteriaTypeId.DEPARTMENT_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.DEPARTMENT_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case TAXON:
                        filterCriteriaTypeId = FilterCriteriaTypeId.TAXON_NAME_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.TAXON_NAME_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case TAXON_GROUP:
                        filterCriteriaTypeId = FilterCriteriaTypeId.TAXON_GROUP_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.TAXON_GROUP_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case ANALYSIS_INSTRUMENT:
                        filterCriteriaTypeId = FilterCriteriaTypeId.ANALYSIS_INSTRUMENT_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.ANALYSIS_INSTRUMENT_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case SAMPLING_EQUIPMENT:
                        filterCriteriaTypeId = FilterCriteriaTypeId.SAMPLING_EQUIPMENT_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.SAMPLING_EQUIPMENT_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                        break;
                    case USER:
                        filterCriteriaTypeId = FilterCriteriaTypeId.QUSER_ID.getValue();
                        filterOperatorTypeId = FilterOperatorTypeId.QUSER_IN.getValue();
                        criteriaValues.addAll(ReefDbBeans.transformCollection(filter.getElements(), ReefDbBeans.GET_ID_STRING));
                }
            }

            if (filterCriteriaTypeId < 0) {
                throw new ReefDbBusinessException(t("reefdb.error.referential.missing", "FILTER_CRITERIA_TYPE"));
            }
            if (filterOperatorTypeId < 0) {
                throw new ReefDbBusinessException(t("reefdb.error.referential.missing", "FILTER_OPERATOR_TYPE"));
            }

            // get or create filter block (only one by filter for ReefDB)
            FilterBlock fb;
            Collection<FilterBlock> fbs = target.getFilterBlocks();

            if (criteriaValues.isEmpty()) {
                if (CollectionUtils.isNotEmpty(fbs)) {
                    // if a filter criteria exists, delete it
                    FilterCriteria existingFc = getFilterCriteria(target.getFilterId(), filterCriteriaTypeId, filterOperatorTypeId);

                    // delete it and its criteria values (TODO: no cascade ?)
                    if (existingFc != null) {
                        // if block has no other filter criteria, delete the whole block
                        if (existingFc.getFilterBlock().getFilterCriterias().size() < 2) {
                            fbs.remove(existingFc.getFilterBlock());
                            getSession().delete(existingFc.getFilterBlock());
                            // TODO delete cascade works here ?
                        } else {
                            getSession().delete(existingFc);
                        }
                    }

                }

            } else {
                // create/update the filter criteria
                FilterCriteria fc = null;

                // Map<objectId, criteriaValue>
                Map<String, FilterCriteriaValue> fcvs = Maps.newHashMap();

                if (CollectionUtils.isEmpty(fbs)) {
                    // create block
                    fb = FilterBlock.Factory.newInstance(target);

                    getSession().save(fb);

                    target.addFilterBlocks(fb);

                } else {
                    // get the first block
                    fb = fbs.iterator().next();

                    // find criteria by criteria type and operator type
                    fc = getFilterCriteria(target.getFilterId(), filterCriteriaTypeId, filterOperatorTypeId);

                    if (fc != null) {
                        // retrieve existing filter criteria values
                        for (FilterCriteriaValue fcv : fc.getFilterCriteriaValues()) {
                            // transform to a map of <objectId, filterCriteriaValue>
                            fcvs.put(fcv.getFilterCritValueNm(), fcv);

                        }
                    }
                }

                if (fc == null) {
                    // create a new filter criteria

                    // get filter criteria type
                    FilterCriteriaType fct = filterCriteriaTypeDao.get(filterCriteriaTypeId);
                    // and filter operator type
                    FilterOperatorType fot = filterOperatorTypeDao.get(filterOperatorTypeId);

                    fc = FilterCriteria.Factory.newInstance(fct, fot, fb);
                    fb.addFilterCriterias(fc);
                }

                Collection<FilterCriteriaValue> filteredElements = fc.getFilterCriteriaValues();
                if (filteredElements == null) {
                    filteredElements = Sets.newHashSet();
                    fc.setFilterCriteriaValues(filteredElements);
                }

                // clear old records
                filteredElements.clear();

                for (String objectId : criteriaValues) {
                    if (fcvs.keySet().contains(objectId)) {
                        filteredElements.add(fcvs.remove(objectId)); // remove from map so remaining old criteria values can be deleted afterwards
                    } else {
                        // add a new criteriaValue
                        FilterCriteriaValue newFcv = FilterCriteriaValue.Factory.newInstance(objectId, fc);

                        filteredElements.add(newFcv);
                    }
                }

                // save will also persist new filter criteria values
                getSession().saveOrUpdate(fc);

                // remove remaining criteria values
                for (String objectIdToRemove : fcvs.keySet()) {
                    // delete criteriaValue
                    getSession().delete(fcvs.get(objectIdToRemove));
                }
            }
        }

        update(target);
        getSession().flush();
        getSession().clear();

        return target;
    }

    /** {@inheritDoc} */
    @Override
    public void deleteFilters(List<Integer> filterIds) {
        if (CollectionUtils.isEmpty(filterIds)) {
            return;
        }

        createQuery("deleteFilters").setParameterList("filterIds", filterIds).executeUpdate();
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean checkFiltersNotUsedInContext(List<Integer> filterIds) {

        Query q = createQuery("countContextsUsingFilters").setParameterList("filterIds", filterIds);
        Long count = (Long) q.uniqueResult();

        return count == 0;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> getFilteredElementsByFilterId(Integer filterId) {
        return queryListTyped("filteredElementsByFilterId", "filterId", IntegerType.INSTANCE, filterId);
    }

    // PRIVATE METHODS

    private void checkDbConstants() {
        // Check constants against database
        if (config.isDbCheckConstantsEnable()) {
            Session session = getSessionFactory().openSession();
            try {
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.LOCATION.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.PROGRAM.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.DEPARTMENT.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.PMFM.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.TAXON.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.TAXON_GROUP.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.ANALYSIS_INSTRUMENT.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.SAMPLING_EQUIPMENT.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, FilterTypeValues.USER.getFilterTypeId()));
                Assert.notNull(session.get(FilterTypeImpl.class, ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.MONITORING_LOCATION_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.PROGRAM_CODE.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.DEPARTMENT_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.PMFM_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.TAXON_NAME_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.TAXON_GROUP_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.ANALYSIS_INSTRUMENT_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.SAMPLING_EQUIPMENT_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.QUSER_ID.getValue()));
                Assert.notNull(session.get(FilterCriteriaTypeImpl.class, FilterCriteriaTypeId.ORDER_ITEM_TYPE_CODE.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.MONITORING_LOCATION_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.PROGRAM_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.DEPARTMENT_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.PMFM_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.TAXON_NAME_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.TAXON_GROUP_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.ANALYSIS_INSTRUMENT_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.SAMPLING_EQUIPMENT_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.QUSER_IN.getValue()));
                Assert.notNull(session.get(FilterOperatorTypeImpl.class, FilterOperatorTypeId.TEXT_EQUAL.getValue()));

            } finally {
                Daos.closeSilently(session);
            }

        }
    }

    private FilterCriteria getFilterCriteria(Integer filterId, Integer filterCriteriaTypeId, Integer filterOperatorTypeId) {
        return queryUniqueTyped("filterCriteriaByFilterId",
                "filterId", IntegerType.INSTANCE, filterId,
                "filterCriteriaTypeId", IntegerType.INSTANCE, filterCriteriaTypeId,
                "filterOperatorTypeId", IntegerType.INSTANCE, filterOperatorTypeId);
    }

    private FilterDTO initFilterDTO(Integer filterId, String filterName, Integer filterTypeId) {
        FilterDTO bean = ReefDbBeanFactory.newFilterDTO();

        bean.setId(filterId);
        bean.setName(filterName);
        bean.setFilterTypeId(filterTypeId);

        return bean;
    }

    private FilterDTO toFilterDTO(Iterator<Object> source) {
        FilterDTO target = ReefDbBeanFactory.newFilterDTO();

        target.setId((Integer) source.next());
        target.setName((String) source.next());
        target.setFilterTypeId((Integer) source.next());

        return target;
    }

}
