package fr.ifremer.reefdb.dao.technical;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Geometry utility class
 * <p/>
 * Created by Ludovic on 16/04/2015.
 */
public class Geometries extends fr.ifremer.quadrige3.core.dao.technical.Geometries {

    /**
     * <p>getCoordinate.</p>
     *
     * @param position a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     */
    public static CoordinateDTO getCoordinate(String position) {

        CoordinateDTO coordinate = ReefDbBeanFactory.newCoordinateDTO();

        if (StringUtils.isNotBlank(position)) {

            Geometry geometry = getGeometry(position);
            Envelope envelope = geometry.getEnvelopeInternal();

            coordinate.setMinLongitude(envelope.getMinX());
            coordinate.setMinLatitude(envelope.getMinY());
            if (envelope.getWidth() > 0 || envelope.getHeight() > 0) {
                coordinate.setMaxLongitude(envelope.getMaxX());
                coordinate.setMaxLatitude(envelope.getMaxY());
            }
        }

        return coordinate;
    }

    /**
     * <p>getPointPosition.</p>
     *
     * @param coordinate a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getPointPosition(CoordinateDTO coordinate) {

        if (coordinate == null || !isPoint(coordinate)) {
            return null;
        }

        Geometry geometry = createPoint(coordinate.getMinLongitude(), coordinate.getMinLatitude());
        return getWKTString(geometry);
    }

    /**
     * <p>getLinePosition.</p>
     *
     * @param coordinate a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getLinePosition(CoordinateDTO coordinate) {

        if (coordinate == null || isPoint(coordinate)) {
            return null;
        }

        Geometry geometry = createLine(coordinate.getMinLongitude(), coordinate.getMinLatitude(), coordinate.getMaxLongitude(), coordinate.getMaxLatitude());
        return getWKTString(geometry);
    }

    /**
     * <p>getAreaPosition.</p>
     *
     * @param coordinate a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getAreaPosition(CoordinateDTO coordinate) {

        if (coordinate == null || isPoint(coordinate)) {
            return null;
        }

        Geometry geometry = createPolygon(coordinate.getMinLongitude(), coordinate.getMinLatitude(), coordinate.getMaxLongitude(), coordinate.getMaxLatitude());
        return getWKTString(geometry);
    }

    /**
     * <p>isValid.</p>
     *
     * @param coordinate a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @return a boolean.
     */
    public static boolean isValid(CoordinateDTO coordinate) {
        return coordinate != null &&
                ((coordinate.getMinLongitude() != null && coordinate.getMaxLongitude() == null && coordinate.getMinLatitude() != null && coordinate.getMaxLatitude() == null)
                        || (coordinate.getMinLongitude() != null && coordinate.getMaxLongitude() != null && coordinate.getMinLatitude() != null && coordinate.getMaxLatitude() != null));

    }

    /**
     * <p>isPoint.</p>
     *
     * @param coordinate a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @return a boolean.
     */
    public static boolean isPoint(CoordinateDTO coordinate) {

        if (coordinate.getMinLongitude() != null && coordinate.getMinLatitude() != null
                && coordinate.getMaxLongitude() == null && coordinate.getMaxLatitude() == null) {

            // coordinate has only min long/lat
            return true;
        }

        // true if min long/lat equals max log/lat
        return coordinate.getMinLongitude() != null && coordinate.getMinLatitude() != null && coordinate.getMaxLongitude() != null && coordinate.getMaxLatitude() != null
                && Objects.equals(coordinate.getMinLongitude(), coordinate.getMaxLongitude()) && Objects.equals(coordinate.getMinLatitude(), coordinate.getMaxLatitude());

    }

    /**
     * <p>equals.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @param target a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @return a boolean.
     */
    public static boolean equals(CoordinateDTO source, CoordinateDTO target) {

        if (source == null && target == null) {
            // assume both null are equals
            return true;
        } else if (source == null ^ target == null) {
            // but if one is null, return false
            return false;
        }

        if (isPoint(source) && isPoint(target)) {
            // if both are point, check only min long/lat
            return Objects.equals(source.getMinLongitude(), target.getMinLongitude()) && Objects.equals(source.getMinLatitude(), target.getMinLatitude());
        } else if (!isPoint(source) && !isPoint(target)) {
            // check all values
            return Objects.equals(source.getMinLongitude(), target.getMinLongitude()) && Objects.equals(source.getMinLatitude(), target.getMinLatitude())
                    && Objects.equals(source.getMaxLongitude(), target.getMaxLongitude()) && Objects.equals(source.getMaxLatitude(), target.getMaxLatitude());
        }

        // otherwise not equals
        return false;

    }

    /**
     * <p>equals.</p>
     *
     * @param source a {@link fr.ifremer.reefdb.dto.CoordinateDTO} object.
     * @param targetPosition a {@link java.lang.String} object.
     * @return a boolean.
     */
    public static boolean equals(CoordinateDTO source, String targetPosition) {

        CoordinateDTO target = getCoordinate(targetPosition);

        return equals(source, target);
    }

}
