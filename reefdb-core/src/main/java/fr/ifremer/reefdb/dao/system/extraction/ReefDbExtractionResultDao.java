package fr.ifremer.reefdb.dao.system.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.technical.csv.CSVDao;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;
import java.util.Map;

/**
 * Dao for extraction with JDBC access on spring data source
 * <p/>
 * Created by Ludovic on 11/12/2015.
 */
public interface ReefDbExtractionResultDao extends CSVDao {

    /**
     * <p>queryCount.</p>
     *
     * @param query a {@link String} object.
     * @return a {@link java.lang.Long} object.
     */
    long queryCount(String query);

    /**
     * <p>queryCount.</p>
     *
     * @param query a {@link java.lang.String} object.
     * @param queryBindings a {@link java.util.Map} object.
     * @return a {@link java.lang.Long} object.
     */
    Long queryCount(String query, Map<String, Object> queryBindings);

    /**
     * <p>query.</p>
     *
     * @param sql a {@link java.lang.String} object.
     * @param queryBindings a {@link java.util.Map} object.
     * @param rowMapper a {@link org.springframework.jdbc.core.RowMapper} object.
     * @param <T> a T object.
     * @return a {@link java.util.List} object.
     */
    <T> List<T> query(String sql, Map<String, Object> queryBindings, RowMapper<T> rowMapper);

    /**
     * <p>queryIntegerList.</p>
     *
     * @param query a {@link java.lang.String} object.
     * @param queryBindings a {@link java.util.Map} object.
     * @return a {@link java.util.List} object.
     */
    List<Integer> queryIntegerList(String query, Map<String, Object> queryBindings);

    List<String> queryStringList(String query, Map<String, Object> queryBindings);

    /**
     * <p>queryUpdate</p>
     *
     * @param query a {@link java.lang.String} object.
     * @return a int.
     */
    int queryUpdate(String query);

    /**
     * <p>queryUpdate.</p>
     *
     * @param query a {@link java.lang.String} object.
     * @param queryBindings a {@link java.util.Map} object.
     * @return a int.
     */
    int queryUpdate(String query, Map<String, Object> queryBindings);

    String getPmfmNameForExtraction(PmfmDTO pmfm);

    String getPmfmUnitNameForExtraction(PmfmDTO pmfm);


}
