package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.AnalysisInstrument;
import fr.ifremer.quadrige3.core.dao.referential.AnalysisInstrumentDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Analysis Instrument DAO
 * <p/>
 * Created by Ludovic on 17/07/2015.
 */
@Repository("reefDbAnalysisInstrumentDao")
public class ReefDbAnalysisInstrumentDaoImpl extends AnalysisInstrumentDaoImpl implements ReefDbAnalysisInstrumentDao {

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("MEASUREMENT", "ANAL_INST_ID")
            .put("MEASUREMENT_FILE", "ANAL_INST_ID")
            .put("TAXON_MEASUREMENT", "ANAL_INST_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("MEASUREMENT", "MEAS_VALID_DT")
            .put("MEASUREMENT_FILE", "MEAS_FILE_VALID_DT")
            .put("TAXON_MEASUREMENT", "TAXON_MEAS_VALID_DT").build();

    private static final Multimap<String, String> columnNamesByProgramTableNames = ImmutableListMultimap.<String, String>builder()
            .put("PMFM_APPLIED_STRATEGY", "ANAL_INST_ID").build();

    @Resource
    protected CacheService cacheService;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbAnalysisInstrumentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<AnalysisInstrumentDTO> getAllAnalysisInstruments(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(ANALYSIS_INSTRUMENT_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allAnalysisInstruments"), statusCodes);

        List<AnalysisInstrumentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            AnalysisInstrumentDTO analysisInstrument = toAnalysisInstrumentDTO(Arrays.asList(source).iterator());
            result.add(analysisInstrument);

            cacheById.put(analysisInstrument.getId(), analysisInstrument);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<AnalysisInstrumentDTO> findAnalysisInstruments(List<String> statusCodes, Integer analysisInstrumentId) {

        Query query = createQuery("analysisInstrumentsByCriteria",
                "analysisInstrumentId", IntegerType.INSTANCE, analysisInstrumentId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<AnalysisInstrumentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toAnalysisInstrumentDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<AnalysisInstrumentDTO> findAnalysisInstrumentsByName(List<String> statusCodes, String analysisInstrumentName) {
        Query query = createQuery("analysisInstrumentsByName",
                "analysisInstrumentName", StringType.INSTANCE, analysisInstrumentName);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<AnalysisInstrumentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toAnalysisInstrumentDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public AnalysisInstrumentDTO getAnalysisInstrumentById(int analysisInstrumentId) {

        Object[] source = queryUnique("analysisInstrumentById", "analysisInstrumentId", IntegerType.INSTANCE, analysisInstrumentId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load analysis instrument with id = " + analysisInstrumentId);
        }

        return toAnalysisInstrumentDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public void saveAnalysisInstruments(List<? extends AnalysisInstrumentDTO> analysisInstruments) {
        if (CollectionUtils.isEmpty(analysisInstruments)) {
            return;
        }

        for (AnalysisInstrumentDTO analysisInstrument : analysisInstruments) {
            if (analysisInstrument.isDirty()) {
                saveAnalysisInstrument(analysisInstrument);
                analysisInstrument.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();

    }

    /** {@inheritDoc} */
    @Override
    public void deleteAnalysisInstruments(List<Integer> analysisInstrumentIds) {
        if (analysisInstrumentIds == null) return;
        Set<Integer> idsToRemove = analysisInstrumentIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        for (Integer id : idsToRemove) {
            remove(id);
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryAnalysisInstrument(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();

    }

    /** {@inheritDoc} */
    @Override
    public boolean isAnalysisInstrumentUsedInData(int analysisInstrumentId) {

        return executeMultipleCount(columnNamesByDataTableNames, analysisInstrumentId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isAnalysisInstrumentUsedInValidatedData(int analysisInstrumentId) {

        return executeMultipleCountWithNotNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, analysisInstrumentId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isAnalysisInstrumentUsedInProgram(int analysisInstrumentId) {

        return executeMultipleCount(columnNamesByProgramTableNames, analysisInstrumentId);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<AnalysisInstrumentDTO> getAnalysisInstrumentsByIds(List<Integer> analysisInstrumentIds) {

        Iterator<Object[]> it = createQuery("analysisInstrumentsByIds")
                .setParameterList("analysisInstrumentIds", analysisInstrumentIds)
                .iterate();

        List<AnalysisInstrumentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toAnalysisInstrumentDTO(Arrays.asList(row).iterator()));
        }

        return result;

    }


    /* private methods */

    private void saveAnalysisInstrument(AnalysisInstrumentDTO analysisInstrument) {
        Assert.notNull(analysisInstrument);
        Assert.notBlank(analysisInstrument.getName());

        if (analysisInstrument.getStatus() == null) {
            analysisInstrument.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(analysisInstrument.getStatus()), "source must have local status");

        AnalysisInstrument target;
        if (analysisInstrument.getId() == null) {
            target = AnalysisInstrument.Factory.newInstance();
            target.setAnalInstId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(analysisInstrument.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setAnalInstNm(analysisInstrument.getName());
        target.setAnalInstDc(analysisInstrument.getDescription());
        target.setStatus(load(StatusImpl.class, analysisInstrument.getStatus().getCode()));

        getSession().save(target);
        analysisInstrument.setId(target.getAnalInstId());
    }

    private AnalysisInstrumentDTO toAnalysisInstrumentDTO(Iterator<Object> source) {
        AnalysisInstrumentDTO result = ReefDbBeanFactory.newAnalysisInstrumentDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
