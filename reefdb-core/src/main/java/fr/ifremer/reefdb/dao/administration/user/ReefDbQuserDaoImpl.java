package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.administration.user.*;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.security.Encryption;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.dto.referential.PrivilegeDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbQuserDaoImpl class.</p>
 *
 */
@Repository("reefDbQuserDao")
@Lazy
public class ReefDbQuserDaoImpl extends QuserDaoImpl implements ReefDbQuserDao {

    private static final Log log = LogFactory.getLog(ReefDbQuserDaoImpl.class);

    private static final Multimap<String, String> columnNamesByProgramTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_LIST_RESP_QUSER", "QUSER_ID")
            .put("PROG_QUSER_PROG_PRIV", "QUSER_ID").build();

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_LIST_RESP_QUSER", "QUSER_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("CAMPAIGN", "QUSER_ID")
            .put("OCCAS_QUSER", "QUSER_ID")
            .put("CONTEXT", "QUSER_ID")
            .put("FILTER", "QUSER_ID")
            .put("DELETED_ITEM_HISTORY", "REC_QUSER_ID")
            .put("QUALIFICATION_HISTORY", "QUSER_ID").build();

    private static final Multimap<String, String> columnNamesBySurveyTableNames = ImmutableListMultimap.<String, String>builder()
            .put("SURVEY_QUSER", "QUSER_ID").build();

    @Resource
    protected ReefDbConfiguration config;

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "privilegeDao")
    protected PrivilegeDao privilegeDao;

    /**
     * <p>Constructor for ReefDbQuserDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbQuserDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getUserById(int quserId) {
        Object[] row = queryUnique("userById",
                "quserId", IntegerType.INSTANCE, quserId);

        if (row == null) {
            throw new DataRetrievalFailureException(String.format("can't load quser with id = %s", quserId));
        }
        return toPersonDTO(Arrays.asList(row).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> getAllUsers(List<String> statusCodes) {
        Cache personByIdCache = cacheService.getCache(USER_BY_ID_CACHE);

        Iterator<Object[]> list = Daos.queryIteratorWithStatus(createQuery("allUsers"), statusCodes);

        List<PersonDTO> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            PersonDTO target = toPersonDTO(Arrays.asList(source).iterator());
            result.add(target);

            // Add to cache by id
            personByIdCache.put(target.getId(), target);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<PersonDTO> findUsersByCriteria(String lastName, String firstName, boolean strictName, String login, Integer departmentId, String privilegeCode, List<String> statusCodes) {
        Query q = createQuery("usersByCriteria",
                "lastName", StringType.INSTANCE, strictName ? null : lastName,
                "firstName", StringType.INSTANCE, strictName ? null : firstName,
                "strictLastName", StringType.INSTANCE, strictName ? lastName : null,
                "strictFirstName", StringType.INSTANCE, strictName ? firstName : null,
                "departmentId", IntegerType.INSTANCE, departmentId,
                "privilegeCd", StringType.INSTANCE, privilegeCode,
                "login", StringType.INSTANCE, login);

        Iterator<Object[]> list = Daos.queryIteratorWithStatus(q, statusCodes);
        List<PersonDTO> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            PersonDTO target = toPersonDTO(Arrays.asList(source).iterator());

            // append privileges
            target.addAllPrivilege(getPrivilegesByUserId(target.getId()));

            result.add(target);
        }
        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public PersonDTO getUserByLogin(List<String> statusCodes, String login) {
        Iterator<Object[]> list = Daos.queryIteratorWithStatus(createQuery("usersByLogin", "login", StringType.INSTANCE, login), statusCodes);

        // take the first user to eliminate duplicates (should be a local user if exists first)
        if (list.hasNext()) {
            Object[] row = list.next();
            return toPersonDTO(Arrays.asList(row).iterator());
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<PersonDTO> getUsersByIds(List<Integer> userIds) {
        List<PersonDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(userIds)) {
            Query q = createQuery("usersByIds").setParameterList("quserIds", userIds);
            Iterator<Object[]> it = q.iterate();
            while (it.hasNext()) {
                result.add(toPersonDTO(Arrays.asList(it.next()).iterator()));
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<PrivilegeDTO> getAllPrivileges() {
        Iterator<Object[]> list = queryIterator("allPrivileges");

        List<PrivilegeDTO> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            PrivilegeDTO target = toPrivilegeDTO(Arrays.asList(source).iterator());
            result.add(target);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<PrivilegeDTO> getPrivilegesByUserId(Integer userId) {
        Iterator<Object[]> list = queryIterator("privilegesByUserId",
                "userId", IntegerType.INSTANCE, userId);

        List<PrivilegeDTO> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            PrivilegeDTO target = toPrivilegeDTO(Arrays.asList(source).iterator());
            result.add(target);
        }
        return result;

    }

    /** {@inheritDoc} */
    @Override
    public void saveTemporaryUser(PersonDTO user) {

        Quser quser = beanToEntity(user);
        getSession().update(quser);
        getSession().flush();
        getSession().clear();

        user.setId(quser.getQuserId());
    }

    /** {@inheritDoc} */
    @Override
    public void deleteTemporaryUser(List<Integer> ids) {
        if (ids == null) return;
        ids.stream().filter(Objects::nonNull).distinct().forEach(this::deleteTemporaryUser);
    }

    /** {@inheritDoc} */
    @Override
    public void deleteTemporaryUser(Integer id) {
        Assert.notNull(id);
        Assert.isTrue(id < 0, String.format("Can't delete a user with a positive id %s", id));
        Assert.isTrue(!isUserUsedInData(id), String.format("The user with id %s is used, can't delete it", id));
        remove(id);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInProgram(int id) {

        return executeMultipleCount(columnNamesByProgramTableNames, id);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInRules(int id) {
        return executeMultipleCount(columnNamesByRulesTableNames, id);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInData(int id) {

        return executeMultipleCount(columnNamesByDataTableNames, id)
                || executeMultipleCount(columnNamesBySurveyTableNames, id);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUserUsedInValidatedData(int id) {

        // query SURVEY_QUSER first to fasten this method
        if (!executeMultipleCount(columnNamesBySurveyTableNames, id)) {
            return false;
        }

        // now query with join on survey
        Long count = queryCount("countValidatedSurveyByQuserId", "quserId", IntegerType.INSTANCE, id);

        return count > 0;
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryUserFks(Integer sourceQuserId, Integer targetQuserId, boolean delete) {

        Assert.notNull(sourceQuserId);
        Assert.notNull(targetQuserId);

        // basic replacement
        executeMultipleUpdate(columnNamesByDataTableNames, sourceQuserId, targetQuserId);

        // get survey ids where target user is already present
        List surveyIds = queryList("unvalidatedSurveyIdsByQuserId", "quserId", IntegerType.INSTANCE, targetQuserId);
        if (CollectionUtils.isEmpty(surveyIds)) {
            // Create dummy list with 1 element (=0) witch should not corresponding to any surveyId
            // Because setParameterList don't accept empty list (cause SQLGrammarException)
            surveyIds = Lists.newArrayList(0);
        }

        // replace SURVEY_QUSER entry only if SURVEY in not validated
        Query query = createQuery("replaceQuserInUnvalidatedSurvey",
                "sourceQuserId", IntegerType.INSTANCE, sourceQuserId,
                "targetQuserId", IntegerType.INSTANCE, targetQuserId);
        query.setParameterList("excludeSurveyIds", surveyIds);
        query.executeUpdate();

        if (delete) {
            // delete temporary user
            deleteTemporaryUser(sourceQuserId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public Object transformEntity(final int transform, final Quser entity) {
        if (entity != null) {
            switch (transform) {
                case ReefDbQuserDao.TRANSFORM_PERSON_DTO: // fall-through
                    return toPersonDTO(entity);
                default:
                    return super.transformEntity(transform, entity);
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * Transforms a collection of entities using the {@link #transformEntities(int, Collection)} method. This method
     * does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     * @see #transformEntities(int, Collection)
     */
    @Override
    public void transformEntities(final int transform, final java.util.Collection<?> entities) {
        switch (transform) {
            case TRANSFORM_PERSON_DTO:
                toPersonDTOCollection(entities);
                break;
            default:
                super.transformEntities(transform, entities);
                // do nothing;
        }
    }

    /* -- Internal Methods -- */
    /**
     * <p>toPersonDTO.</p>
     *
     * @param source a {@link java.util.Iterator} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    protected PersonDTO toPersonDTO(Iterator<Object> source) {
        PersonDTO target = ReefDbBeanFactory.newPersonDTO();

        // Id
        target.setId((Integer) source.next());

        // Registration Code
        target.setRegCode((String) source.next());

        // LastName:
        String lastname = (String) source.next();
        if (StringUtils.isBlank(lastname) || "-".equals(lastname)) {
            lastname = "";
        }
        target.setName(lastname);

        // FirstName:
        String firstname = (String) source.next();
        if (StringUtils.isBlank(firstname) || "-".equals(firstname)) {
            firstname = "";
        }
        target.setFirstName(firstname);

        // Department (id)
        target.setDepartment(departmentDao.getDepartmentById((Integer) source.next()));

        // logins
        target.setIntranetLogin((String) source.next());
        target.setExtranetLogin((String) source.next());
        target.setHasPassword(StringUtils.isNotBlank((CharSequence) source.next()));

        // email
        target.setEmail((String) source.next());

        // phone
        target.setPhone((String) source.next());

        // address
        target.setAddress((String) source.next());

        // organism
        target.setOrganism((String) source.next());

        // admin center
        target.setAdminCenter((String) source.next());

        // site
        target.setSite((String) source.next());

        // Status (code)
        target.setStatus(Daos.getStatus((String) source.next()));

        target.setComment((String) source.next());
        target.setCreationDate(Daos.convertToDate(source.next()));
        target.setUpdateDate(Daos.convertToDate(source.next()));

        return target;
    }

    /**
     * <p>toPersonDTO.</p>
     *
     * @param source a {@link fr.ifremer.quadrige3.core.dao.administration.user.Quser} object.
     * @return a {@link fr.ifremer.reefdb.dto.referential.PersonDTO} object.
     */
    protected PersonDTO toPersonDTO(Quser source) {
        PersonDTO target = ReefDbBeanFactory.newPersonDTO();
        target.setId(source.getQuserId());
        target.setRegCode(source.getQuserCd());
        target.setName(source.getQuserLastNm());
        target.setFirstName(source.getQuserFirstNm());
        target.setDepartment(departmentDao.getDepartmentById(source.getDepartment().getDepId()));
        target.setIntranetLogin(source.getQuserIntranetLg());
        target.setExtranetLogin(source.getQuserExtranetLg());
        target.setEmail(source.getQuserEMail());
        target.setPhone(source.getQuserPhone());
        target.setAddress(source.getQuserAddress());
        target.setOrganism(source.getQuserOrgan());
        target.setAdminCenter(source.getQuserAdminCenter());
        target.setSite(source.getQuserSite());
        Daos.getStatus(source.getStatus().getStatusCd());
        target.setComment(source.getQuserCm());
        target.setCreationDate(source.getQuserCreationDt());
        target.setUpdateDate(source.getUpdateDt());
        return target;
    }

    /**
     * <p>toPersonDTOCollection.</p>
     *
     * @param entities a {@link java.util.Collection} object.
     */
    @SuppressWarnings("unchecked")
    protected void toPersonDTOCollection(java.util.Collection<?> entities) {
        if (entities != null) {
            org.apache.commons.collections4.CollectionUtils.transform(entities, PERSON_DTO_TRANSFORMER);
        }
    }

    protected final org.apache.commons.collections4.Transformer PERSON_DTO_TRANSFORMER
            = input -> {
                Object result = null;
                if (input instanceof Quser) {
                    result = toPersonDTO((Quser) input);
                }

                return result;
            };

    private PrivilegeDTO toPrivilegeDTO(Iterator<Object> source) {
        PrivilegeDTO target = ReefDbBeanFactory.newPrivilegeDTO();

        target.setCode((String) source.next());
        target.setName((String) source.next());
        target.setDescription((String) source.next());

        // Override name if privilege is part of ReefDbAuthorities
        switch (PrivilegeCode.fromValue(target.getCode())) {
            case REFERENTIAL_ADMINISTRATOR:
                target.setName(t("quadrige3.security.authority.ROLE_ADMIN"));
                break;
            case LOCAL_ADMINISTRATOR:
                target.setName(t("quadrige3.security.authority.ROLE_LOCAL_ADMIN"));
                break;
            case VALIDATOR:
                target.setName(t("quadrige3.security.authority.ROLE_VALIDATOR"));
                break;
            case QUALIFIER:
                target.setName(t("quadrige3.security.authority.ROLE_QUALIFIER"));
                break;
        }

        return target;
    }

    private Quser beanToEntity(PersonDTO source) {

        // unicity key: login + status

        Status status = load(StatusImpl.class, source.getStatus().getCode());
        Department department = load(DepartmentImpl.class, source.getDepartment().getId());
        String lastname = source.getName();
        String firstname = source.getFirstName();

        Quser target;
        if (source.getId() == null) {
            target = createAsTemporary(lastname, firstname, source.getDepartment().getId());
        } else {
            target = get(source.getId());
            target.setQuserLastNm(lastname);
            target.setQuserFirstNm(firstname);
            target.setDepartment(department);
        }

        // status ?
        target.setStatus(status);
        target.setQuserEMail(source.getEmail());
        target.setQuserAddress(source.getAddress());
        target.setQuserPhone(source.getPhone());

        // login & password
        target.setQuserIntranetLg(source.getIntranetLogin());
        // if a new password has been set
        if (source.getNewPassword() != null) {
            if (source.getNewPassword().equals("")) {
                // reset password if empty string
                target.setQuserCryptPassword(null);
            } else {
                // encrypt new password
                target.setQuserCryptPassword(Encryption.sha(source.getNewPassword()));
            }
        }

        // add privileges
        Collection<Privilege> existingPrivileges = target.getPrivileges();
        Map<String, Privilege> privilegesByCodes = ReefDbBeans.mapByProperty(existingPrivileges, "privilegeCd");
        existingPrivileges.clear();
        Collection<PrivilegeDTO> sourcePrivileges = source.getPrivilege();

        for (PrivilegeDTO sourcePriv : sourcePrivileges) {
            if (privilegesByCodes.containsKey(sourcePriv.getCode())) {
                existingPrivileges.add(privilegesByCodes.remove(sourcePriv.getCode()));
            } else {
                existingPrivileges.add((Privilege) getSession().get(PrivilegeImpl.class, sourcePriv.getCode()));
            }
        }

        return target;
    }

}
