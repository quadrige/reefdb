package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Method;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MethodDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Method DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("reefDbMethodDao")
public class ReefDbMethodDaoImpl extends MethodDaoImpl implements ReefDbMethodDao {

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_PMFM", "METHOD_ID").build();

    private static final Multimap<String, String> columnNamesByReferentialTableNames = ImmutableListMultimap.<String, String>builder()
            .put("PMFM", "METHOD_ID").build();

    @Resource
    protected CacheService cacheService;

    /**
     * <p>Constructor for ReefDbMethodDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbMethodDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<MethodDTO> getAllMethods(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(METHOD_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allMethods"), statusCodes);

        List<MethodDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            MethodDTO method = toMethodDTO(Arrays.asList(source).iterator());
            result.add(method);

            cacheById.put(method.getId(), method);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public MethodDTO getMethodById(int methodId) {

        Object[] source = queryUnique("methodById", "methodId", IntegerType.INSTANCE, methodId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load method with id = " + methodId);
        }

        return toMethodDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<MethodDTO> findMethods(Integer methodId, List<String> statusCodes) {

        Query query = createQuery("methodsByCriteria", "methodId", IntegerType.INSTANCE, methodId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<MethodDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            MethodDTO method = toMethodDTO(Arrays.asList(source).iterator());
            result.add(method);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public void saveMethods(List<? extends MethodDTO> methods) {
        if (CollectionUtils.isEmpty(methods)) {
            return;
        }

        for (MethodDTO method : methods) {
            if (method.isDirty()) {
                saveMethod(method);
                method.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteMethods(List<Integer> methodIds) {
        if (methodIds == null) return;
        methodIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryMethod(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdate(columnNamesByReferentialTableNames, sourceId, targetId);

        if (delete) {
            // remove temp
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMethodUsedInProgram(int methodId) {

        return queryCount("countPmfmStrategyByMethodId", "methodId", IntegerType.INSTANCE, methodId) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMethodUsedInRules(int methodId) {

        return executeMultipleCount(columnNamesByRulesTableNames, methodId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMethodUsedInReferential(int methodId) {

        return executeMultipleCount(columnNamesByReferentialTableNames, methodId) || isMethodUsedInRules(methodId);
    }

    private void saveMethod(MethodDTO method) {
        Assert.notNull(method);
        Assert.notBlank(method.getName());

        if (method.getStatus() == null) {
            method.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(method.getStatus()), "source must have local status");

        Method target;
        if (method.getId() == null) {
            target = Method.Factory.newInstance();
            target.setMethodId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(method.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setMethodNm(method.getName());
        target.setStatus(load(StatusImpl.class, method.getStatus().getCode()));
        target.setMethodRk("1");

        target.setMethodRef(method.getReference());
        target.setMethodDc(method.getDescription());
        target.setMethodCondition(method.getDescriptionPackaging());
        target.setMethodPrepar(method.getDescriptionPreparation());
        target.setMethodConserv(method.getDescriptionPreservation());

        if (target.getMethodCreationDt() == null) {
            target.setMethodCreationDt(newCreateDate());
        }
        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        method.setId(target.getMethodId());

    }

    private MethodDTO toMethodDTO(Iterator<Object> source) {
        MethodDTO result = ReefDbBeanFactory.newMethodDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setReference((String) source.next());
        result.setNumber((String) source.next());
        result.setDescriptionPackaging((String) source.next());
        result.setDescriptionPreparation((String) source.next());
        result.setDescriptionPreservation((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
