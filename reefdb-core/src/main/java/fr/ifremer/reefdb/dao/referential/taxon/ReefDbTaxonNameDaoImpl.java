package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.TaxonGroupTypeCode;
import fr.ifremer.quadrige3.core.dao.referential.taxon.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.synchro.meta.DatabaseColumns;
import fr.ifremer.quadrige3.synchro.meta.data.DataSynchroTables;
import fr.ifremer.quadrige3.synchro.meta.referential.ReferentialSynchroTables;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.referential.transcribing.ReefDbTranscribingItemDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.CitationDTO;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonomicLevelDTO;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.i18n.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ReefDbTaxonNameDaoImpl class.</p>
 */
@Repository("reefDbTaxonNameDao")
public class ReefDbTaxonNameDaoImpl extends TaxonNameDaoImpl implements ReefDbTaxonNameDao {

    private static final Log LOG = LogFactory.getLog(ReefDbTaxonNameDaoImpl.class);

    private static final Multimap<String, String> taxonNameIdColumnsByReferentialTables = ImmutableListMultimap.<String, String>builder()
            .put(ReferentialSynchroTables.TAXON_GROUP_HISTORICAL_RECORD.name(), DatabaseColumns.TAXON_NAME_ID.name())
            .put(ReferentialSynchroTables.TAXON_NAME.name(), DatabaseColumns.PARENT_TAXON_NAME_ID.name()).build();

    private static final Multimap<String, String> refTaxonIdColumnsByDataTables = ImmutableListMultimap.<String, String>builder()
            .put(DataSynchroTables.TAXON_MEASUREMENT.name(), DatabaseColumns.REF_TAXON_ID.name()).build();

    private static final Multimap<String, String> taxonNameIdColumnsByDataTables = ImmutableListMultimap.<String, String>builder()
            .put(DataSynchroTables.TAXON_MEASUREMENT.name(), DatabaseColumns.TAXON_NAME_ID.name()).build();

    private static final Map<String, String> validDtColumnsByDataTables = ImmutableMap.<String, String>builder()
            .put(DataSynchroTables.TAXON_MEASUREMENT.name(), DatabaseColumns.TAXON_MEAS_VALID_DT.name()).build();

    @Resource
    protected CacheService cacheService;
    @Resource
    protected ReefDbConfiguration config;
    @Resource(name = "reefDbTaxonNameDao")
    protected ReefDbTaxonNameDao loopbackTaxonNameDao;
    @Resource(name = "referenceTaxonDao")
    protected ReferenceTaxonDao referenceTaxonDao;
    @Resource(name = "reefDbTranscribingItemDao")
    protected ReefDbTranscribingItemDao transcribingItemDao;

    /**
     * <p>Constructor for ReefDbTaxonNameDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbTaxonNameDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getAllTaxonNames() {

        Cache cacheByReferenceId = cacheService.getCache(TAXON_NAME_BY_REFERENCE_ID_CACHE);
        Cache cacheById = cacheService.getCache(TAXON_NAME_BY_ID_CACHE);

        Map<Integer, String> taxRefMap = loopbackTaxonNameDao.getTaxRefByTaxonNameId();
        Map<Integer, String> wormsMap = loopbackTaxonNameDao.getWormsByTaxonNameId();

        Iterator<Object[]> it = queryIterator("allTaxonName");

        long nbTaxonName = 0;
        long nbRefTaxon = 0;
        List<TaxonDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonDTO taxon = toTaxonDTO(Arrays.asList(source).iterator());

            // add other references
            taxon.setTaxRef(taxRefMap.get(taxon.getId()));
            taxon.setWormsRef(wormsMap.get(taxon.getId()));

            result.add(taxon);

            // update cache by id
            cacheById.put(taxon.getId(), taxon);
            nbTaxonName++;
            // update cache by reference id
            if (taxon.isReferent()) {
                cacheByReferenceId.put(taxon.getReferenceTaxonId(), taxon);
                nbRefTaxon++;
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("%s Taxon name loaded, %s reference taxon loaded", nbTaxonName, nbRefTaxon));
        }
        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxonDTO getTaxonNameByReferenceId(int referenceTaxonId) {

        Object[] source = queryUnique("taxonNameByReferenceId", "taxonReferenceId", IntegerType.INSTANCE, referenceTaxonId);

        if (source == null) {
            throw new DataRetrievalFailureException("Can't load taxon name with reference taxon id = " + referenceTaxonId);
        }

        return toTaxonDTO(Arrays.asList(source).iterator());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxonDTO getTaxonNameById(int taxonId) {

        Object[] source = queryUnique("taxonNameById", "taxonNameId", IntegerType.INSTANCE, taxonId);

        if (source == null) {
            return null;
        }

        TaxonDTO taxon = toTaxonDTO(Arrays.asList(source).iterator());
        fillReferent(taxon);
        return taxon;
    }

    /**
     * {@inheritDoc}
     *
     * @param date
     */
    @Override
    public Multimap<Integer, TaxonDTO> getAllTaxonNamesMapByTaxonGroupId(@Nonnull LocalDate date) {

        Iterator<Object[]> it = queryIterator("taxonNameIdsWithTaxonGroupId",
                "referenceDate", DateType.INSTANCE, Dates.convertToDate(date, config.getDbTimezone()),
                "taxonGroupTypeCode", StringType.INSTANCE, TaxonGroupTypeCode.IDENTIFICATION.getValue());

        Multimap<Integer, TaxonDTO> result = ArrayListMultimap.create();
        while (it.hasNext()) {
            Object[] source = it.next();
            Iterator<Object> row = Arrays.asList(source).iterator();
            Integer taxonGroupId = (Integer) row.next();
            result.put(taxonGroupId, loopbackTaxonNameDao.getTaxonNameById((Integer) row.next()));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getTaxonNamesByIds(List<Integer> taxonIds) {

        if (CollectionUtils.isEmpty(taxonIds)) return new ArrayList<>();

        return loopbackTaxonNameDao.getAllTaxonNames().stream().filter(taxon -> taxonIds.contains(taxon.getId())).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillTaxonsProperties(List<TaxonDTO> taxons) {

        if (CollectionUtils.isEmpty(taxons)) {
            return;
        }

        Map<Integer, String> taxRefMap = loopbackTaxonNameDao.getTaxRefByTaxonNameId();
        Map<Integer, String> wormsMap = loopbackTaxonNameDao.getWormsByTaxonNameId();

        for (TaxonDTO taxon : taxons) {

            // parent taxon and reference taxon
            fillParentAndReferent(taxon);

            // composites
            if (taxon.isVirtual()) {
                taxon.setCompositeTaxons(loopbackTaxonNameDao.getCompositeTaxonNames(taxon.getId()));
            }

            // fill other references like (WORMS and TAXREFv80)
            taxon.setTaxRef(taxRefMap.get(taxon.getId()));
            taxon.setWormsRef(wormsMap.get(taxon.getId()));

        }
    }

    private void fillTaxonProperties(TaxonDTO taxon) {

        // parent taxon and reference taxon
        fillParentAndReferent(taxon);

        // fill other references like (WORMS and TAXREFv80)
        taxon.setTaxRef(loopbackTaxonNameDao.getTaxRefByTaxonNameId().get(taxon.getId()));
        taxon.setWormsRef(loopbackTaxonNameDao.getWormsByTaxonNameId().get(taxon.getId()));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> findFullTaxonNamesByCriteria(String levelCode, String name, boolean isStrictName, Boolean isLocal) {
        List<TaxonDTO> result = findTaxonNamesByCriteria(levelCode, name, isStrictName, isLocal);

        fillTaxonsProperties(result);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> findTaxonNamesByCriteria(String levelCode, String name, boolean isStrictName, final Boolean isLocal) {

        List<TaxonDTO> result = Lists.newArrayList();

        // for better performance if no parameter
        if (StringUtils.isBlank(levelCode) && StringUtils.isBlank(name)) {

            result.addAll(loopbackTaxonNameDao.getAllTaxonNames());

        } else {

            Iterator<Object[]> it = queryIterator("taxonNamesByCriteria",
                    "levelCd", StringType.INSTANCE, levelCode,
                    "name", StringType.INSTANCE, isStrictName ? null : name,
                    "strictName", StringType.INSTANCE, isStrictName ? name : null);

            while (it.hasNext()) {
                Object[] source = it.next();
                result.add(toTaxonDTO(Arrays.asList(source).iterator()));
            }
        }

        fillReferents(result);

        return ImmutableList.copyOf(isLocal == null
                ? result
                : ReefDbBeans.filterReferential(result, isLocal)
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonomicLevelDTO> getAllTaxonomicLevels() {
        Iterator<Object[]> it = queryIterator("allTaxonomicLevels");

        List<TaxonomicLevelDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toTaxonomicLevelDTO(Arrays.asList(source).iterator()));
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TaxonDTO> getCompositeTaxonNames(Integer taxonNameId) {
        Iterator<Object[]> it = queryIterator("compositeTaxonNamesByTaxonNameId",
                "taxonNameId", IntegerType.INSTANCE, taxonNameId);

        List<TaxonDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonDTO taxon = toTaxonDTO(Arrays.asList(source).iterator());
            fillTaxonProperties(taxon);
            result.add(taxon);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Integer, String> getTaxRefByTaxonNameId() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForTaxRef());
//        return getAlternateReferencesMap(config.getAlternativeTaxonOriginTaxRef());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Integer, String> getWormsByTaxonNameId() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForWorms());
//        return getAlternateReferencesMap(config.getAlternativeTaxonOriginWorms());
    }

//    private Map<Integer, String> getAlternateReferencesMap(String originCode) {
//
//        Map<Integer, String> result = Maps.newHashMap();
//        if (StringUtils.isNotBlank(originCode)) {
//
//            Iterator<Object[]> rows = queryIterator("alternateTaxonCode",
//                    "originCode", StringType.INSTANCE, originCode);
//
//            while (rows.hasNext()) {
//                Object[] row = rows.next();
//                result.put((Integer) row[0], (String) row[1]);
//            }
//        }
//
//        return result;
//    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveTaxons(List<? extends TaxonDTO> taxons) {
        if (CollectionUtils.isEmpty(taxons)) {
            return;
        }

        for (TaxonDTO taxon : taxons) {
            if (taxon.isDirty()) {
                saveTaxon(taxon);
                taxon.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTaxons(List<Integer> taxonIds) {
        if (taxonIds == null) return;
        taxonIds.stream().filter(Objects::nonNull).distinct().forEach(id -> {
            remove(id);

            // If local taxon, delete the associated reference taxon
            boolean isLocalTaxon = TemporaryDataHelper.isTemporaryId(id);
            if (isLocalTaxon) {
                referenceTaxonDao.remove(id);
            }
        });
        getSession().flush();
        getSession().clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceTemporaryTaxon(Integer sourceId, Integer sourceReferenceId, Integer targetId, Integer targetReferenceId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);
        Assert.notNull(sourceReferenceId);
        Assert.notNull(targetReferenceId);

        // taxon name: Replace inside referential tables
        executeMultipleUpdate(taxonNameIdColumnsByReferentialTables, sourceId, targetId);

        // taxon name: Replace inside data tables, but NOT if already validate
        executeMultipleUpdateWithNullCondition(taxonNameIdColumnsByDataTables, validDtColumnsByDataTables, sourceId, targetId);

        // reference taxon: Replace inside data tables, but NOT if already validate
        executeMultipleUpdateWithNullCondition(refTaxonIdColumnsByDataTables, validDtColumnsByDataTables, sourceReferenceId, targetReferenceId);

        if (delete) {
            // delete temporary taxon name
            remove(sourceId);

            // If local taxon name, delete the associated reference taxon (fix mantis #39386)
            boolean isLocalTaxon = TemporaryDataHelper.isTemporaryId(sourceId);
            if (isLocalTaxon) {
                referenceTaxonDao.remove(sourceId);
            }
        }

        getSession().flush();
        getSession().clear();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTaxonNameUsedInReferential(int taxonId) {

        return executeMultipleCount(taxonNameIdColumnsByReferentialTables, taxonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReferenceTaxonUsedInReferential(int refTaxonId, int excludedTaxonNameId) {

        Long countInTaxonName = queryUniqueTyped("countReferenceTaxonInTaxonName",
                "refTaxonId", IntegerType.INSTANCE, refTaxonId,
                "excludedTaxonNameId", IntegerType.INSTANCE, excludedTaxonNameId);
        return countInTaxonName > 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReferenceTaxonUsedInData(int refTaxonId) {

        return executeMultipleCount(refTaxonIdColumnsByDataTables, refTaxonId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReferenceTaxonUsedInValidatedData(int refTaxonId) {

        return executeMultipleCountWithNotNullCondition(refTaxonIdColumnsByDataTables, validDtColumnsByDataTables, refTaxonId);
    }

    // INTERNAL METHODS

    private void saveTaxon(TaxonDTO taxon) {
        Assert.notNull(taxon);
        Assert.isTrue(taxon.getId() == null || taxon.getId() < 0);
        Assert.notBlank(taxon.getName());
        Assert.notNull(taxon.getParentTaxon());
        Assert.notNull(taxon.getLevel());

        TaxonName target;
        if (taxon.getId() == null) {
            target = TaxonName.Factory.newInstance();
            target.setTaxonNameId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(taxon.getId());
        }

        target.setTaxonNameCompleteNm(taxon.getName());
        target.setTaxonomicLevel(load(TaxonomicLevelImpl.class, taxon.getLevel().getCode()));

        if (target.getReferenceTaxon() == null) {
            // By default, use: refTaxonId = taxonNameId
            int refTaxonId = target.getTaxonNameId();

            // Check this refTaxonId if not used
            if (get(ReferenceTaxonImpl.class, refTaxonId) != null) {
                throw new ReefDbTechnicalException(I18n.t("reefdb.error.referential.badLocalTaxonNameId", refTaxonId));
            }

            ReferenceTaxon refTaxon = ReferenceTaxon.Factory.newInstance();
            refTaxon.setRefTaxonId(refTaxonId);
            refTaxon.setUpdateDt(newUpdateTimestamp());
            target.setReferenceTaxon(refTaxon);
            getSession().save(refTaxon);

            // Affect to bean (Mantis #39754)
            taxon.setReferenceTaxonId(refTaxonId);
        }

        target.setParentTaxonName(load(taxon.getParentTaxon().getId()));

        target.setTaxonNameCm(taxon.getComment());
        target.setTaxonNameIsRefer(true);
        target.setTaxonNameTempor(true);
        target.setTaxonNameIsVirtual(true);
        target.setTaxonNameObsol(taxon.isObsolete());
        if (taxon.getCitation() != null) {
            target.setCitId(load(CitationImpl.class, taxon.getCitation().getId()));
        } else {
            target.setCitId(null);
        }

        getSession().save(target);
        taxon.setId(target.getTaxonNameId());
    }

    private TaxonDTO toTaxonDTO(Iterator<Object> source) {
        TaxonDTO result = ReefDbBeanFactory.newTaxonDTO();

        // Id
        result.setId((Integer) source.next());

        // Name (complete name)
        result.setName((String) source.next());

        result.setComment((String) source.next());

        result.setReferent((Boolean) source.next());
        result.setVirtual((Boolean) source.next());
        result.setObsolete((Boolean) source.next());
        result.setTemporary((Boolean) source.next());

        result.setReferenceTaxonId((Integer) source.next());
        result.setParentTaxonId((Integer) source.next());

        String levelCode = (String) source.next();
        String levelLabel = (String) source.next();
        String levelName = (String) source.next();
        Integer levelNb = (Integer) source.next();
        Integer citationId = (Integer) source.next();
        String citationName = (String) source.next();

        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // taxonomic level
        if (levelCode != null) {
            TaxonomicLevelDTO level = ReefDbBeanFactory.newTaxonomicLevelDTO();
            level.setCode(levelCode);
            level.setLabel(levelLabel);
            level.setName(levelName);
            level.setNumber(levelNb);

            result.setLevel(level);
        }

        // citation
        if (citationId != null) {
            CitationDTO citation = ReefDbBeanFactory.newCitationDTO();
            citation.setId(citationId);
            citation.setName(citationName);

            result.setCitation(citation);
        }

        // Status
        StatusCode statusCode;
        if (Boolean.TRUE.equals(result.isTemporary())) {
            statusCode = StatusCode.TEMPORARY;
        } else {
            statusCode = StatusCode.ENABLE;
        }
        result.setStatus(Daos.getStatus(statusCode));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillParentAndReferent(TaxonDTO taxon) {

        // referent Taxon
        fillReferent(taxon);

        // parent Taxon
        if (taxon.getParentTaxonId() != null) {
            if (taxon.getParentTaxonId().equals(taxon.getId())) {
                taxon.setParentTaxon(taxon);
            } else {
                taxon.setParentTaxon(loopbackTaxonNameDao.getTaxonNameById(taxon.getParentTaxonId()));
            }
        }
    }

    @Override
    public void fillReferents(List<TaxonDTO> taxons) {

        if (taxons == null) return;
        taxons.forEach(this::fillReferent);
    }

    private void fillReferent(TaxonDTO taxon) {

        // get referent taxon
        if (taxon.getReferenceTaxon() == null || !Objects.equals(taxon.getReferenceTaxon().getId(), taxon.getReferenceTaxonId())) {
            taxon.setReferenceTaxon(loopbackTaxonNameDao.getTaxonNameByReferenceId(taxon.getReferenceTaxonId()));
        }
    }

    private TaxonomicLevelDTO toTaxonomicLevelDTO(Iterator<Object> source) {
        TaxonomicLevelDTO result = ReefDbBeanFactory.newTaxonomicLevelDTO();

        // code
        result.setCode((String) source.next());

        // label
        result.setLabel((String) source.next());

        // name
        result.setName((String) source.next());

        //number
        result.setNumber((Integer) source.next());

        return result;

    }
}
