package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Parameter;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.ParameterDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.ParameterGroupImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValue;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.ParameterGroupDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Parameter DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("reefDbParameterDao")
public class ReefDbParameterDaoImpl extends ParameterDaoImpl implements ReefDbParameterDao {

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_PMFM", "PAR_CD").build();

    private static final Multimap<String, String> columnNamesByReferentialTableNames = ImmutableListMultimap.<String, String>builder()
            .put("PMFM", "PAR_CD").build();

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource(name = "reefDbQualitativeValueDao")
    protected ReefDbQualitativeValueDao qualitativeValueDao;

    @Resource(name = "reefDbParameterDao")
    protected ReefDbParameterDao loopbackDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbParameterDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<ParameterGroupDTO> getAllParameterGroups(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(PARAMETER_GROUP_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allParameterGroups"), statusCodes);

        List<ParameterGroupDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            ParameterGroupDTO parameter = toParameterGroupDTO(Arrays.asList(source).iterator());
            result.add(parameter);
            cacheById.put(parameter.getId(), parameter);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public ParameterGroupDTO getParameterGroupById(int parameterGroupId) {
        Object[] source = queryUnique("parameterGroupById", "parameterGroupId", IntegerType.INSTANCE, parameterGroupId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load parameter group with id = " + parameterGroupId);
        }

        return toParameterGroupDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<ParameterDTO> getAllParameters(List<String> statusCodes) {

        Cache cacheByCode = cacheService.getCache(PARAMETER_BY_CODE_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allParameters"), statusCodes);

        List<ParameterDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            ParameterDTO parameter = toParameterDTO(Arrays.asList(source).iterator());
            // without qualitative values
            result.add(parameter);
            cacheByCode.put(parameter.getCode(), parameter);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public ParameterDTO getParameterByCode(String parameterCode) {
        Assert.notBlank(parameterCode);

        Object[] source = queryUnique("parameterByCode", "parameterCode", StringType.INSTANCE, parameterCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load parameter with code = " + parameterCode);
        }

        ParameterDTO result = toParameterDTO(Arrays.asList(source).iterator());
        // add all associated Qualitative values
        result.setQualitativeValues(qualitativeValueDao.getQualitativeValuesByParameterCode(result.getCode()));
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<ParameterDTO> findParameters(String parameterCode, Integer parameterGroupId, List<String> statusCodes) {
        Query query = createQuery("parametersByCriteria",
                "parameterCode", StringType.INSTANCE, parameterCode,
                "parameterGroupId", IntegerType.INSTANCE, parameterGroupId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        // other solution: use criteria API and findByExample
        List<ParameterDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            ParameterDTO parameter = toParameterDTO(Arrays.asList(source).iterator());
            // add all associated Qualitative values
            parameter.setQualitativeValues(qualitativeValueDao.getQualitativeValuesByParameterCode(parameter.getCode()));
            result.add(parameter);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public void saveParameters(List<? extends ParameterDTO> parameters) {
        if (CollectionUtils.isEmpty(parameters)) {
            return;
        }

        for (ParameterDTO parameter : parameters) {
            if (parameter.isDirty()) {
                saveParameter(parameter);
                parameter.setDirty(false);
                parameter.setNewCode(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteParameters(List<String> parametersCodes) {
        if (parametersCodes == null) return;
        Set<String> idsToRemove = parametersCodes.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        for (String code : idsToRemove) {
            remove(code);
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void remove(String parCd) {

        // remove qualitative values
        Parameter parameter = get(parCd);
        Collection<QualitativeValue> qualitativeValues = parameter.getQualitativeValues();
        if (CollectionUtils.isNotEmpty(qualitativeValues)) {
            qualitativeValueDao.remove(qualitativeValues);
        }

        // remove entity
        super.remove(parCd);
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryParameter(String sourceCode, String targetCode, boolean delete) {
        Assert.notBlank(sourceCode);
        Assert.notBlank(targetCode);

        executeMultipleUpdate(columnNamesByReferentialTableNames, sourceCode, targetCode);

        if (delete) {
            // delete temporary
            remove(sourceCode);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isParameterUsedInProgram(String parameterCode) {

        return queryCount("countPmfmStrategyByParameterCode", "parameterCode", StringType.INSTANCE, parameterCode) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isParameterUsedInRules(String parameterCode) {

        return executeMultipleCount(columnNamesByRulesTableNames, parameterCode);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isParameterUsedInReferential(String parameterCode) {

        return executeMultipleCount(columnNamesByReferentialTableNames, parameterCode) || isParameterUsedInRules(parameterCode);
    }

    private void saveParameter(ParameterDTO parameter) {
        Assert.notNull(parameter);
        Assert.notBlank(parameter.getCode());
        Assert.notNull(parameter.getParameterGroup());

        if (parameter.getStatus() == null) {
            parameter.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(parameter.getStatus()), "source must have local status");

        Parameter target = get(parameter.getCode());
        if (target == null) {
            target = Parameter.Factory.newInstance();
            target.setParCd(parameter.getCode());
        } else {
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }
        target.setStatus(load(StatusImpl.class, parameter.getStatus().getCode()));
        target.setParGroupId(load(ParameterGroupImpl.class, parameter.getParameterGroup().getId()));
        target.setParNm(parameter.getName());
        target.setParDc(parameter.getDescription());
        target.setParIsQualitative(Daos.convertToString(parameter.isQualitative()));
        target.setParIsTaxonomic(Daos.convertToString(parameter.isTaxonomic()));
        target.setParIsCalculated(Daos.convertToString(parameter.isCalculated()));
        if (target.getParCreationDt() == null) {
            target.setParCreationDt(newCreateDate());
        }
        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        getSession().flush();

        // qualitative values
        if (!parameter.isQualitativeValuesEmpty()) {

            Map<Integer, QualitativeValue> remainingQualitativeValuesById = ReefDbBeans.mapByProperty(target.getQualitativeValues(), "qualValueId");
            for (QualitativeValueDTO qualitativeValue : parameter.getQualitativeValues()) {

                remainingQualitativeValuesById.remove(qualitativeValue.getId());
                // create & add  OR  update
                qualitativeValueDao.saveQualitativeValue(parameter.getCode(), qualitativeValue);
            }
            if (!remainingQualitativeValuesById.isEmpty()) {
                // remove remaining values
                qualitativeValueDao.remove(remainingQualitativeValuesById.values());
            }

        } else if (target.getQualitativeValues() != null) {
            qualitativeValueDao.remove(target.getQualitativeValues());
        }

        getSession().save(target);
        getSession().flush();
        getSession().clear();
    }

    private ParameterGroupDTO toParameterGroupDTO(Iterator<Object> source) {
        ParameterGroupDTO parameterGroup = ReefDbBeanFactory.newParameterGroupDTO();
        parameterGroup.setId((Integer) source.next());
        parameterGroup.setName((String) source.next());
        parameterGroup.setDescription((String) source.next());
        parameterGroup.setStatus(Daos.getStatus((String) source.next()));
        Integer parentId = (Integer) source.next();
        if (parentId != null) {
            parameterGroup.setParentParameterGroup(loopbackDao.getParameterGroupById(parentId));
        }
        return parameterGroup;
    }

    private ParameterDTO toParameterDTO(Iterator<Object> source) {
        ParameterDTO result = ReefDbBeanFactory.newParameterDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setQualitative(Daos.safeConvertToBoolean(source.next()));
        result.setCalculated(Daos.safeConvertToBoolean(source.next()));
        result.setTaxonomic(Daos.safeConvertToBoolean(source.next()));
        result.setStatus(Daos.getStatus((String) source.next()));
        Integer groupId = (Integer) source.next();
        if (groupId != null) {
            result.setParameterGroup(loopbackDao.getParameterGroupById(groupId));
        }
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
