package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.HibernateDaoSupport;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.referential.pmfm.ReefDbPmfmDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>ReefDbReferentialDaoImpl class.</p>
 *
 */
@Repository("reefDbReferentialDao")
public class ReefDbReferentialDaoImpl extends HibernateDaoSupport implements ReefDbReferentialDao {

    /**
     * Logger.
     */
//    private static final Log log = LogFactory.getLog(ReefDbReferentialDaoImpl.class);

    @Resource
    protected CacheService cacheService;

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    /**
     * <p>Constructor for ReefDbReferentialDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbReferentialDaoImpl(SessionFactory sessionFactory) {
        super();
        setSessionFactory(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<LevelDTO> getAllDepthLevels() {

        // only enable (=national) depth levels
        String statusCode = StatusCode.ENABLE.getValue();

        Cache cacheById = cacheService.getCache(DEPTH_LEVEL_BY_ID_CACHE);

        Iterator<Object[]> it = queryIterator("allDepthLevels",
                "statusCode", StringType.INSTANCE, statusCode);

        List<LevelDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            LevelDTO depthLevel = toDepthLevelDTO(Arrays.asList(source).iterator(), statusCode);
            result.add(depthLevel);

            // update cache
            cacheById.put(depthLevel.getId(), depthLevel);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public LevelDTO getDepthLevelById(int depthLevelId) {

        // only enable (=national) depth levels
        String statusCode = StatusCode.ENABLE.getValue();

        Object[] source = queryUnique("depthLevelById",
                "statusCode", StringType.INSTANCE, statusCode,
                "depthLevelId", IntegerType.INSTANCE, depthLevelId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load depth level with id = " + depthLevelId);
        }

        return toDepthLevelDTO(Arrays.asList(source).iterator(), statusCode);
    }

    /** {@inheritDoc} */
    @Override
    public List<DepthDTO> getAllDepthValues() {

        final Cache cacheById = cacheService.getCache(DEPTH_VALUE_BY_ID_CACHE);

        PmfmDTO pmfm = pmfmDao.getPmfmById(config.getDepthValuesPmfmId());
        Assert.notNull(pmfm);

        Collection<QualitativeValueDTO> values;

        if (pmfm.isQualitativeValuesEmpty()) {

            // if pmfm has no qualitative values, get them from parameter
            Assert.notNull(pmfm.getParameter());
            Assert.isTrue(!pmfm.getParameter().isQualitativeValuesEmpty());
            values = pmfm.getParameter().getQualitativeValues();
        } else {

            values = pmfm.getQualitativeValues();
        }

        return ImmutableList.copyOf(ReefDbBeans.transformCollection(values,
                input -> {
                    DepthDTO result = ReefDbBeanFactory.newDepthDTO();
                    result.setId(input.getId());
                    result.setName(input.getName());
                    result.setStatus(input.getStatus());
                    // add to cache
                    cacheById.put(result.getId(), result);
                    return result;
                }));

    }

    /** {@inheritDoc} */
    @Override
    public DepthDTO getDepthValueById(int depthValueId) {

        PmfmDTO pmfm = pmfmDao.getPmfmById(config.getDepthValuesPmfmId());
        Assert.notNull(pmfm);

        Collection<QualitativeValueDTO> values;

        if (pmfm.isQualitativeValuesEmpty()) {

            // if pmfm has no qualitative values, get them from parameter
            Assert.notNull(pmfm.getParameter());
            Assert.isTrue(!pmfm.getParameter().isQualitativeValuesEmpty());
            values = pmfm.getParameter().getQualitativeValues();
        } else {

            values = pmfm.getQualitativeValues();
        }

        Map<Integer, QualitativeValueDTO> qualitativeValuesById = ReefDbBeans.mapById(values);
        QualitativeValueDTO qualitativeValue = qualitativeValuesById.get(depthValueId);
        if (qualitativeValue != null) {
            DepthDTO result = ReefDbBeanFactory.newDepthDTO();
            result.setId(qualitativeValue.getId());
            result.setName(qualitativeValue.getName());
            result.setStatus(qualitativeValue.getStatus());
            return result;
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<StatusDTO> getAllStatus(List<String> statusCodes) {

        Assert.notNull(statusCodes);

        List<StatusDTO> result = Lists.newArrayList();
        for (String statusCode : statusCodes) {
            result.add(Daos.getStatus(statusCode));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<QualityLevelDTO> getAllQualityFlags(List<String> statusCodes) {

        Cache cacheByCode = cacheService.getCache(QUALITY_FLAG_BY_CODE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allQualityFlags"), statusCodes);

        List<QualityLevelDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            QualityLevelDTO qualityFlag = toQualityLevelDTO(Arrays.asList(source).iterator());
            result.add(qualityFlag);

            cacheByCode.put(qualityFlag.getCode(), qualityFlag);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualityLevelDTO> findQualityFlags(List<String> statusCodes, String qualityFlagCode) {

        Query query = createQuery("qualityFlagsByCriteria",
                "qualityFlagCode", StringType.INSTANCE, qualityFlagCode);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<QualityLevelDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toQualityLevelDTO(Arrays.asList(source).iterator()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public QualityLevelDTO getQualityFlagByCode(String qualityFlagCode) {

        Object[] source = queryUnique("qualityFlagByCode", "qualityFlagCode", StringType.INSTANCE, qualityFlagCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load quality flag with code = " + qualityFlagCode);
        }

        return toQualityLevelDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<PositioningSystemDTO> getAllPositioningSystems() {

        Cache cacheById = cacheService.getCache(POSITIONING_SYSTEM_BY_ID);

        Iterator<Object[]> it = queryIterator("allPositioningSystems");

        List<PositioningSystemDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            PositioningSystemDTO positioningSystem = toPositioningSystemDTO(Arrays.asList(source).iterator());
            result.add(positioningSystem);

            cacheById.put(positioningSystem.getId(), positioningSystem);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public PositioningSystemDTO getPositioningSystemById(int posSystemId) {
        Object[] source = queryUnique("positioningSystemById", "posSystemId", IntegerType.INSTANCE, posSystemId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load positioning system with id = " + posSystemId);
        }

        return toPositioningSystemDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<GroupingTypeDTO> getAllGroupingTypes() {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allOrderItemTypes"), StatusFilter.ACTIVE.toStatusCodes());

        List<GroupingTypeDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            GroupingTypeDTO groupingType = toGroupingTypeDTO(Arrays.asList(source).iterator());

            groupingType.setGrouping(new ArrayList<>(getGroupingsByType(groupingType.getCode())));

            result.add(groupingType);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<GroupingDTO> getGroupingsByType(String groupingType) {
        Iterator<Object[]> it = queryIterator("allOrderItemsByTypeCode", "typeCode", StringType.INSTANCE, groupingType);

        List<GroupingDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toGroupingDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<CitationDTO> getAllCitations() {
        Iterator<Object[]> it = queryIterator("allCitations");

        List<CitationDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toCitationDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<PhotoTypeDTO> getAllPhotoTypes() {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allPhotoTypes"), StatusFilter.ACTIVE.toStatusCodes());

        List<PhotoTypeDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toPhotoTypeDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public PhotoTypeDTO getPhotoTypeByCode(String photoTypeCode) {
        Object[] source = queryUnique("photoTypeByCode", "photoTypeCode", StringType.INSTANCE, photoTypeCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load photo type with id = " + photoTypeCode);
        }

        return toPhotoTypeDTO(Arrays.asList(source).iterator());
    }

    // INTERNAL METHODS
    private LevelDTO toDepthLevelDTO(Iterator<Object> source, String statusCode) {
        LevelDTO result = ReefDbBeanFactory.newLevelDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus(statusCode));
        return result;
    }

    private PhotoTypeDTO toPhotoTypeDTO(Iterator<Object> source) {
        PhotoTypeDTO result = ReefDbBeanFactory.newPhotoTypeDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        return result;
    }

    private QualityLevelDTO toQualityLevelDTO(Iterator<Object> source) {
        QualityLevelDTO result = ReefDbBeanFactory.newQualityLevelDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        return result;
    }

    private PositioningSystemDTO toPositioningSystemDTO(Iterator<Object> source) {
        PositioningSystemDTO result = ReefDbBeanFactory.newPositioningSystemDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setPrecision((String) source.next());
        // a positioning system is always enable (=national)
        result.setStatus(Daos.getStatus(StatusCode.ENABLE.getValue()));
        return result;
    }

    private GroupingTypeDTO toGroupingTypeDTO(Iterator<Object> source) {
        GroupingTypeDTO result = ReefDbBeanFactory.newGroupingTypeDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        return result;
    }

    private GroupingDTO toGroupingDTO(Iterator<Object> source) {
        GroupingDTO result = ReefDbBeanFactory.newGroupingDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        return result;
    }

    private CitationDTO toCitationDTO(Iterator<Object> source) {
        CitationDTO result = ReefDbBeanFactory.newCitationDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        return result;
    }
}
