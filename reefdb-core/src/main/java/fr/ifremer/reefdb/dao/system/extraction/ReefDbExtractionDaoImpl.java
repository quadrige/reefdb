package fr.ifremer.reefdb.dao.system.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.ProjectionSystemImpl;
import fr.ifremer.quadrige3.core.dao.system.extraction.*;
import fr.ifremer.quadrige3.core.dao.system.filter.Filter;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterTypeId;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.system.filter.ReefDbFilterDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.referential.GroupingTypeDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>ReefDbExtractionDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbExtractionDao")
public class ReefDbExtractionDaoImpl extends ExtractFilterDaoImpl implements ReefDbExtractionDao, InitializingBean {

//    private static final Log log = LogFactory.getLog(ReefDbExtractionDaoImpl.class);

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "reefDbFilterDao")
    protected ReefDbFilterDao filterDao;

    @Resource(name = "reefDbReferentialDao")
    protected ReefDbReferentialDao referentialDao;

    @Resource
    protected ReefDbConfiguration config;

    @Resource
    protected CacheService cacheService;

    private String defaultFileTypeCd;
    private String defaultGroupTypePmfmCd;
    private String defaultTableTypeCd;
    private String defaultProjectionSystemCd;

    /**
     * <p>Constructor for ReefDbExtractionDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbExtractionDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public void afterPropertiesSet() {
        initConstants();
    }

    /** {@inheritDoc} */
    @Override
    public List<ExtractionDTO> getAllExtraction() {
        Iterator<Object[]> it = queryIterator("allExtractions");

        List<ExtractionDTO> extractions = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            extractions.add(toExtractionDTO(Arrays.asList(row).iterator()));
        }

        return extractions;
    }

    /** {@inheritDoc} */
    @Override
    public ExtractionDTO getExtractionById(int extractionId) {

        Object[] row = queryUnique("extractionById", "extractionId", IntegerType.INSTANCE, extractionId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load extraction with id = " + extractionId);
        }

        return toExtractionDTO(Arrays.asList(row).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<ExtractionDTO> searchExtractionByProgram(String programCode) {
        Assert.notBlank(programCode);

        Iterator<Object[]> it = queryIterator("extractionsByProgramCode",
                "programFilterTypeId", IntegerType.INSTANCE, FilterTypeId.PROGRAM.getValue(),
                "programCode", StringType.INSTANCE, programCode);

        List<ExtractionDTO> extractions = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            extractions.add(toExtractionDTO(Arrays.asList(row).iterator()));
        }

        return extractions;
    }

    /** {@inheritDoc} */
    @Override
    public void saveExtraction(ExtractionDTO extraction) {
        Assert.notNull(extraction);
        Assert.notBlank(extraction.getName());
        Assert.notEmpty(extraction.getFilters());

        ExtractFilter entity;
        boolean isNew = false;
        if (extraction.getId() == null) {
            entity = ExtractFilter.Factory.newInstance();
            isNew = true;
        } else {
            entity = get(extraction.getId());
            if (entity == null) {
                throw new DataRetrievalFailureException("Could not retrieve extraction with id=" + extraction.getId());
            }
        }

        beanToEntity(extraction, entity);

        if (isNew) {

            getSession().save(entity);
            extraction.setId(entity.getExtractFilterId());
        } else {
            getSession().update(entity);
        }

        savePeriods(extraction, entity);

        saveFilters(extraction, entity);

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void remove(ExtractFilter extractFilter) {

        extractFilter.getExtractSurveyPeriods().clear();
        extractFilter.getFilters().clear();

        super.remove(extractFilter);
    }

    // PRIVATE METHODS

    private void beanToEntity(ExtractionDTO extraction, ExtractFilter entity) {

        // mandatory attribute
        entity.setExtractFilterNm(extraction.getName());

        // add user
        if (extraction.getUser() == null) {
            int userId = dataContext.getPrincipalUserId();
            extraction.setUser(quserDao.getUserById(userId));
            entity.setQuser(quserDao.get(userId));
        }

        // optional attributes
        entity.setExtractFilterInSitu(Daos.convertToString(false));
        entity.setExtractFilterIsQualif(Daos.convertToString(false));

        // file type (= Text only)
        if (entity.getExtractFileTypes() != null) {
            if (entity.getExtractFileTypes().size() != 1 || !Objects.equals(entity.getExtractFileTypes().iterator().next().getExtractFileTypeCd(), defaultFileTypeCd)) {
                // bad file type, so recreate it
                entity.getExtractFileTypes().clear();
                entity.addExtractFileTypes(load(ExtractFileTypeImpl.class, defaultFileTypeCd));
            }
        } else {
            // no file type, so create it
            entity.setExtractFileTypes(Lists.<ExtractFileType>newArrayList(load(ExtractFileTypeImpl.class, defaultFileTypeCd)));
        }

        // group type pmfm (=PMFM)
        if (entity.getExtractGroupTypePmfms() != null) {
            if (entity.getExtractGroupTypePmfms().size() != 1 || !Objects.equals(entity.getExtractGroupTypePmfms().iterator().next().getExtractGroupTypePmfmCd(), defaultGroupTypePmfmCd)) {
                // bad group type, so recreate it
                entity.getExtractGroupTypePmfms().clear();
                entity.addExtractGroupTypePmfms(load(ExtractGroupTypePmfmImpl.class, defaultGroupTypePmfmCd));
            }
        } else {
            // no group type, so create it
            entity.setExtractGroupTypePmfms(Lists.<ExtractGroupTypePmfm>newArrayList(load(ExtractGroupTypePmfmImpl.class, defaultGroupTypePmfmCd)));
        }

        // table type (=inline)
        entity.setExtractTableType(load(ExtractTableTypeImpl.class, defaultTableTypeCd));
        // no aggregation level
        entity.setExtractAgregationLevel(null);

        // projection system
        entity.setProjectionSystem(load(ProjectionSystemImpl.class, defaultProjectionSystemCd));

    }

    private void savePeriods(ExtractionDTO extraction, ExtractFilter entity) {

        // Get existing periods
        Map<Integer, ExtractSurveyPeriod> existingExtractSurveyPeriods = ReefDbBeans.mapByProperty(entity.getExtractSurveyPeriods(), "extractSurveyPeriodId");

        // find period filter
        FilterDTO periodFilter = ReefDbBeans.findByProperty(extraction.getFilters(), FilterDTO.PROPERTY_FILTER_TYPE_ID, ExtractionFilterValues.PERIOD.getFilterTypeId());
        Assert.notNull(periodFilter);
        Assert.notEmpty(periodFilter.getElements());

        for (QuadrigeBean bean : periodFilter.getElements()) {
            ExtractionPeriodDTO period = (ExtractionPeriodDTO) bean;
            Assert.notNull(period.getStartDate());
            Assert.notNull(period.getEndDate());

            boolean isNew = false;
            ExtractSurveyPeriod extractSurveyPeriod = existingExtractSurveyPeriods.remove(period.getId());
            if (extractSurveyPeriod == null) {
                extractSurveyPeriod = ExtractSurveyPeriod.Factory.newInstance();
                extractSurveyPeriod.setExtractFilter(entity);
                isNew = true;
            }
            extractSurveyPeriod.setExtractSurveyPeriodStartDt(Dates.convertToDate(period.getStartDate(), config.getDbTimezone()));
            extractSurveyPeriod.setExtractSurveyPeriodEndDt(Dates.convertToDate(period.getEndDate(), config.getDbTimezone()));

            if (isNew) {
                getSession().save(extractSurveyPeriod);
                period.setId(extractSurveyPeriod.getExtractSurveyPeriodId());
            } else {
                getSession().update(extractSurveyPeriod);
            }
        }

        // remove remaining
        if (!existingExtractSurveyPeriods.isEmpty()) {
            entity.getExtractSurveyPeriods().removeAll(existingExtractSurveyPeriods.values());
        }
    }

    private void saveFilters(ExtractionDTO extraction, ExtractFilter entity) {

        int userId = extraction.getUser().getId();
        Map<Integer, Filter> existingFilters = ReefDbBeans.mapByProperty(entity.getFilters(), "filterId");

        if (CollectionUtils.isNotEmpty(extraction.getFilters())) {
            for (FilterDTO filter : extraction.getFilters()) {

                // don't save period as filter
                if (ExtractionFilterValues.getExtractionFilter(filter.getFilterTypeId()) == ExtractionFilterValues.PERIOD) {
                    continue;
                }

                // compute a specific name for each filter
                filter.setName(String.format("EXT_%d_%s", extraction.getId(), ExtractionFilterValues.getExtractionFilter(filter.getFilterTypeId())));

                if (existingFilters.remove(filter.getId()) == null) {
                    // new filter
                    Filter newFilter = filterDao.saveFilter(filter, userId);
                    newFilter.setExtractFilter(entity);
                    getSession().update(newFilter);
                    entity.addFilters(newFilter);
                } else if (filter.isDirty()) {
                    // update filter
                    filterDao.saveFilter(filter, userId);
                }
            }
        }

        if (!existingFilters.isEmpty()) {
            // remove remaining
            for (Filter filterToRemove : existingFilters.values()) {
                entity.removeFilters(filterToRemove);
            }
        }
    }

    private ExtractionDTO toExtractionDTO(Iterator<Object> iterator) {
        ExtractionDTO extraction = ReefDbBeanFactory.newExtractionDTO();
        extraction.setId((Integer) iterator.next());
        extraction.setName((String) iterator.next());

        Integer userId = (Integer) iterator.next();
        if (userId != null) {
            extraction.setUser(quserDao.getUserById(userId));
        }

        extraction.setFilters(getFilters(extraction.getId()));

        // get the orderItemType filter and load the groupingType now
        FilterDTO orderItemTypeFilter = ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.ORDER_ITEM_TYPE);
        if (orderItemTypeFilter != null) {
            if (!orderItemTypeFilter.isFilterLoaded()) {
                List<String> groupingTypeCodes = filterDao.getFilteredElementsByFilterId(orderItemTypeFilter.getId());
                if (groupingTypeCodes != null && groupingTypeCodes.size() > 0) {

                    GroupingTypeDTO groupingType = ReefDbBeans.findByProperty(
                            referentialDao.getAllGroupingTypes(),
                            GroupingTypeDTO.PROPERTY_CODE,
                            groupingTypeCodes.get(0));

                    orderItemTypeFilter.setElements(Collections.singletonList(groupingType));
                    orderItemTypeFilter.setFilterLoaded(true);
                }
            }
        }

        // add extraction periods in filters
        FilterDTO periodFilter = ReefDbBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(getPeriods(extraction.getId()));
        periodFilter.setFilterLoaded(true);
        extraction.addFilters(periodFilter);

        return extraction;
    }

    private Collection<FilterDTO> getFilters(Integer extractionId) {

        List<FilterDTO> filters = Lists.newArrayList();
        Iterator<Integer> rows = queryIteratorTyped("extractionFilterIdByExtractionById", "extractionId", IntegerType.INSTANCE, extractionId);
        Cache filterCache = cacheService.getCache(ReefDbFilterDao.FILTER_BY_ID_CACHE);
        while (rows.hasNext()) {
            Integer filterId = rows.next();

            // must evict actual filter from cache because it continues to be linked even the extraction is not saved
            filterCache.evict(filterId);

            // get the filter but don't load elements
            filters.add(filterDao.getFilterById(filterId));
        }
        return filters;
    }

    private List<ExtractionPeriodDTO> getPeriods(int extractionId) {

        Iterator<Object[]> rows = queryIterator("extractionPeriodByExtractionById", "extractionId", IntegerType.INSTANCE, extractionId);
        List<ExtractionPeriodDTO> periods = Lists.newArrayList();

        while (rows.hasNext()) {
            Object[] row = rows.next();
            ExtractionPeriodDTO period = ReefDbBeanFactory.newExtractionPeriodDTO();
            period.setId((Integer) row[0]);
            period.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(row[1]), config.getDbTimezone()));
            period.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(row[2]), config.getDbTimezone()));
            periods.add(period);
        }
        return periods;
    }

    private void initConstants() {

        defaultFileTypeCd = config.getExtractionFileTypeCode();
        defaultGroupTypePmfmCd = config.getExtractionGroupTypePmfmCode();
        defaultTableTypeCd = config.getExtractionTableTypeCode();
        defaultProjectionSystemCd = config.getExtractionProjectionSystemCode();

        // check constants in DB
        if (config.isDbCheckConstantsEnable()) {
            checkDbConstants();
        }

    }

    private void checkDbConstants() {

        Session session = getSessionFactory().openSession();
        try {
            Assert.notNull(session.get(ExtractFileTypeImpl.class, defaultFileTypeCd));
            Assert.notNull(session.get(ExtractGroupTypePmfmImpl.class, defaultGroupTypePmfmCd));
            Assert.notNull(session.get(ExtractTableTypeImpl.class, defaultTableTypeCd));
            Assert.notNull(session.get(ProjectionSystemImpl.class, defaultProjectionSystemCd));
        } finally {
            Daos.closeSilently(session);
        }
    }

}
