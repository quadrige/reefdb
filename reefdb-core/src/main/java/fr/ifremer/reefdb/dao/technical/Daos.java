package fr.ifremer.reefdb.dao.technical;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2013 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.quadrige3.core.dao.data.photo.Photo;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.system.synchronization.SynchronizationStatus;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Helper class for all DAO operations
 *
 * @author Benoit Lavenier <benoit.lavenier@e-is.pro>
 * @since 1.0
 */
public class Daos extends fr.ifremer.quadrige3.core.dao.technical.Daos {

    // TODO get this constant by configuration
    /** Constant <code>SURVEY_OBJECT_TYPE="PASS"</code> */
    public final static String SURVEY_OBJECT_TYPE = "PASS";
    /** Constant <code>SAMPLING_OPERATION_OBJECT_TYPE="PREL"</code> */
    public final static String SAMPLING_OPERATION_OBJECT_TYPE = "PREL";

    private static final Map<String, StatusDTO> cachedStatusByCode = Maps.newHashMap();

    static {
        // reserve I18n entries
        for (StatusCode statusCode : StatusCode.values()) {
            String nameKey = String.format("reefdb.persistence.status.%s.name", statusCode.name());
            n(nameKey);
        }
    }

    /**
     * <p>Constructor for Daos.</p>
     */
    protected Daos() {
        // Helper class = protected constructor
    }

    /**
     * <p>getStatus.</p>
     *
     * @param statusCode a {@link fr.ifremer.quadrige3.core.dao.referential.StatusCode} object.
     * @return a {@link StatusDTO} object.
     */
    public static StatusDTO getStatus(StatusCode statusCode) {
        Assert.notNull(statusCode);
        return getStatus(statusCode.value());
    }

    /**
     * <p>getStatus.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link StatusDTO} object.
     */
    public static StatusDTO getStatus(String code) {
        Assert.notBlank(code);

        // Read from static cache (no DB read or this tiny table)
        StatusDTO result = cachedStatusByCode.get(code);
        if (result != null) {
            return result;
        }

        result = QuadrigeBeanFactory.newStatusDTO();

        result.setCode(code);

        String nameKey = String.format("reefdb.persistence.status.%s.name", StatusCode.fromValue(code).name());
        result.setName(t(nameKey));
        cachedStatusByCode.put(code, result);

        return result;
    }

    /**
     * <p>withStatus.</p>
     *
     * @param query a {@link org.hibernate.Query} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link org.hibernate.Query} object.
     */
    public static Query withStatus(Query query, List<String> statusCodes) {
        Assert.notEmpty(statusCodes);
        query.setParameterList("statusCodes", statusCodes);
        return query;
    }

    /**
     * <p>queryIteratorWithStatus.</p>
     *
     * @param query a {@link org.hibernate.Query} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.Iterator} object.
     */
    @SuppressWarnings("unchecked")
    public static Iterator<Object[]> queryIteratorWithStatus(Query query, List<String> statusCodes) {
        return withStatus(query, statusCodes).iterate();
    }

    /**
     * <p>convertDateOnlyToSQLString.</p>
     *
     * @param date a {@link java.util.Date} object.
     * @return a {@link java.lang.String} object.
     */
    public static String convertDateOnlyToSQLString(Date date) {
        Assert.notNull(date);
        java.sql.Date jdbcDate = date instanceof java.sql.Date
            ? (java.sql.Date) date
            : new java.sql.Date(date.getTime());

        return "\'" + jdbcDate.toString() + " 00:00:00\'";
    }



    /**
     * <p>isSynchronizationStatusDirty.</p>
     *
     * @param entity a {@link java.lang.Object} object.
     * @return a boolean.
     */
    public static boolean isSynchronizationStatusDirty(Object entity) {
        Assert.notNull(entity);
        String synchronizationStatus;
        try {
            synchronizationStatus = BeanUtils.getSimpleProperty(entity, "synchronizationStatus");
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalArgumentException("Could not read property 'synchronizationStatus' for class:" + entity.getClass(), e);
        }
        return SynchronizationStatus.DIRTY.getValue().equals(synchronizationStatus);
    }

    /* -- internal methods -- */
    /**
     * Build a file path for a Photo like Q² <br/>
     * ex: PASS/OBJ60092549/PASS-OBJ60092549-60000320.jpg for the survey id=60092549 and photo id=60000320 <br/>
     * or: PREL/OBJ60165512/PREL-OBJ60165512-60003120.jpg for the sampling operation id=60165512 and photo id=60003120
     *
     * Note: the ids are local only, need to recompute with remote ids for synchronization
     *
     * @param source the source photo to obtain file extension
     * @param target the target photo (the entity must be persisted before calling this method)
     * @return the local file path
     */
    public static String computePhotoFilePath(PhotoDTO source, Photo target) {
        if (source == null || StringUtils.isBlank(source.getFullPath())) {
            return null;
        }
        if (target == null || target.getPhotoId() == null || target.getObjectType() == null || target.getObjectId() == null) {
            return null;
        }

        return computePhotoFilePath(
            target.getObjectType().getObjectTypeCd(),
            target.getObjectId(),
            target.getPhotoId(),
            FilenameUtils.getExtension(source.getFullPath()));
    }

    /**
     * Build a file path for a Photo like Q² <br/>
     * ex: PASS/OBJ60092549/PASS-OBJ60092549-60000320.jpg for the survey id=60092549 and photo id=60000320 <br/>
     * or: PREL/OBJ60165512/PREL-OBJ60165512-60003120.jpg for the sampling operation id=60165512 and photo id=60003120
     *
     * @param objectTypeCode a {@link java.lang.String} object.
     * @param objectId a {@link java.lang.Integer} object.
     * @param photoId a {@link java.lang.Integer} object.
     * @param fileExtension a {@link java.lang.String} object.
     * @return the local file path
     */
    public static String computePhotoFilePath(String objectTypeCode, Integer objectId, Integer photoId, String fileExtension) {
        if (StringUtils.isBlank(objectTypeCode) || StringUtils.isBlank(fileExtension) || photoId == null || objectId == null) {
            return null;
        }

        return String.format("%1$s/%2$s%3$s/%1$s-%2$s%3$s-%4$s.%5$s", objectTypeCode, "OBJ", objectId, photoId, fileExtension);
    }

}
