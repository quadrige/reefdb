package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.Unit;
import fr.ifremer.quadrige3.core.dao.referential.UnitDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.UnitDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * Unit DAO
 * <p/>
 * Created by Ludovic on 28/07/2015.
 */
@Repository("reefDbUnitDao")
public class ReefDbUnitDaoImpl extends UnitDaoImpl implements ReefDbUnitDao {

    private static final Multimap<String, String> columnNamesByReferentialTableNames = ImmutableListMultimap.<String, String>builder()
            .put("PMFM", "UNIT_ID")
            .put("SAMPLING_EQUIPMENT", "UNIT_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("SAMPLE", "SAMPLE_SIZE_UNIT_ID")
            .put("SAMPLING_OPERATION", "SAMPLING_OPER_DEPTH_UNIT_ID")
            .put("SAMPLING_OPERATION", "SAMPLING_OPER_SIZE_UNIT_ID")
            .put("SURVEY", "SURVEY_BOTTOM_DEPTH_UNIT_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("SAMPLING_OPERATION", "SAMPLING_OPER_VALID_DT")
            .put("SURVEY", "SURVEY_VALID_DT").build();

    @Resource
    protected CacheService cacheService;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbUnitDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public UnitDTO getUnitById(int unitId) {

        Object[] source = queryUnique("unitById", "unitId", IntegerType.INSTANCE, unitId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load unit with id = " + unitId);
        }

        return toUnitDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public List<UnitDTO> getAllUnits(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(UNIT_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allUnits"), statusCodes);

        List<UnitDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            UnitDTO unit = toUnitDTO(Arrays.asList(source).iterator());
            result.add(unit);

            // update cache
            if (unit != null)
                cacheById.put(unit.getId(), unit);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<UnitDTO> findUnits(Integer unitId, List<String> statusCodes) {

        Query query = createQuery("unitsByCriteria", "unitId", IntegerType.INSTANCE, unitId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<UnitDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toUnitDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public void saveUnits(List<? extends UnitDTO> units) {
        if (CollectionUtils.isEmpty(units)) {
            return;
        }

        for (UnitDTO unit : units) {
            if (unit.isDirty()) {
                saveUnit(unit);
                unit.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteUnits(List<Integer> unitIds) {
        if (unitIds == null) return;
        unitIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryUnit(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdate(columnNamesByReferentialTableNames, sourceId, targetId);
        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUnitUsedInReferential(int unitId) {

        return executeMultipleCount(columnNamesByReferentialTableNames, unitId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUnitUsedInData(int unitId) {

        return executeMultipleCount(columnNamesByDataTableNames, unitId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isUnitUsedInValidatedData(int unitId) {

        return executeMultipleCountWithNotNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, unitId);
    }

    // internal methods
    private void saveUnit(UnitDTO unit) {
        Assert.notNull(unit);
        Assert.notBlank(unit.getName());

        if (unit.getStatus() == null) {
            unit.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(unit.getStatus()), "source must have local status");

        Unit target;
        if (unit.getId() == null) {
            target = Unit.Factory.newInstance();
            target.setUnitId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(unit.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setUnitNm(unit.getName());
        target.setStatus(load(StatusImpl.class, unit.getStatus().getCode()));
        target.setUnitSymbol(unit.getSymbol());
        target.setUpdateDt(newUpdateTimestamp());
        if (target.getUnitCreationDt() == null) {
            target.setUnitCreationDt(newCreateDate());
        }

        getSession().save(target);
        unit.setId(target.getUnitId());
    }

    private UnitDTO toUnitDTO(Iterator<Object> source) {
        UnitDTO result = ReefDbBeanFactory.newUnitDTO();
        result.setId((Integer) source.next());
        if (result.getId() == null) {
            source.next();
            source.next();
            source.next();
            return null;
        }
        result.setName((String) source.next());
        result.setSymbol((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
