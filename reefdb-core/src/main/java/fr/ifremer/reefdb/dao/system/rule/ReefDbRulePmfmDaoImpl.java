package fr.ifremer.reefdb.dao.system.rule;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.UnitImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.FractionImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MatrixImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MethodImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.ParameterImpl;
import fr.ifremer.quadrige3.core.dao.system.rule.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dao.referential.pmfm.*;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.hibernate.SessionFactory;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author peck7 on 03/07/2018.
 */
@Repository("reefDbRulePmfmDao")
public class ReefDbRulePmfmDaoImpl extends RulePmfmDaoImpl implements ReefDbRulePmfmDao {

    @Resource(name = "reefDbPmfmDao")
    protected ReefDbPmfmDao pmfmDao;

    @Resource(name = "reefDbParameterDao")
    protected ReefDbParameterDao parameterDao;

    @Resource(name = "reefDbMatrixDao")
    protected ReefDbMatrixDao matrixDao;

    @Resource(name = "reefDbFractionDao")
    protected ReefDbFractionDao fractionDao;

    @Resource(name = "reefDbMethodDao")
    protected ReefDbMethodDao methodDao;

    @Resource(name = "reefDbUnitDao")
    protected ReefDbUnitDao unitDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory
     */
    @Autowired
    public ReefDbRulePmfmDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<RulePmfmDTO> getRulePmfmByRuleCode(String ruleCode) {
        Assert.notBlank(ruleCode);

        Iterator<Object[]> it = queryIterator("rulePmfmByRuleCode",
                "ruleCode", StringType.INSTANCE, ruleCode);

        List<RulePmfmDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toRulePmfmDTO(Arrays.asList(source).iterator()));
        }

        return result;
    }

    @Override
    public RulePmfmDTO save(RulePmfmDTO source, String ruleCd) {
        Assert.notNull(source);
        Assert.notNull(source.getPmfm());
        Assert.notNull(source.getPmfm().getParameter());
        Assert.notBlank(source.getPmfm().getParameter().getCode());
        Assert.notBlank(ruleCd);

        // Parent
        Rule parent = get(RuleImpl.class, ruleCd);

        // Load entity
        RulePmfm target = source.getId() != null ? get(source.getId()) : null;
        boolean isNew = false;
        if (target == null) {
            target = RulePmfm.Factory.newInstance();
            target.setRulePmfmId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), RulePmfmImpl.class));
            parent.addRulePmfms(target);
            target.setRule(parent);
            isNew = true;
        }

        // DTO -> VO
        beanToEntity(source, target);

        // Save or update
        if (isNew) {
            getSession().save(target);
            // Update source
            source.setId(target.getRulePmfmId());
        } else {
            getSession().update(target);
        }

        return source;
    }

    private void beanToEntity(RulePmfmDTO source, RulePmfm target) {
        // Set properties
        target.setParameter(load(ParameterImpl.class, source.getPmfm().getParameter().getCode()));
        target.setMatrix(source.getPmfm().getMatrix() == null ? null : load(MatrixImpl.class, source.getPmfm().getMatrix().getId()));
        target.setFraction(source.getPmfm().getFraction() == null ? null : load(FractionImpl.class, source.getPmfm().getFraction().getId()));
        target.setMethod(source.getPmfm().getMethod() == null ? null : load(MethodImpl.class, source.getPmfm().getMethod().getId()));
        target.setUnit(source.getPmfm().getUnit() == null ? null : load(UnitImpl.class, source.getPmfm().getUnit().getId()));
    }

    // internal methods

    private RulePmfmDTO toRulePmfmDTO(Iterator<Object> source) {
        RulePmfmDTO result = ReefDbBeanFactory.newRulePmfmDTO();
        result.setId((Integer) source.next());

        String parameterCode = (String) source.next();
        Integer matrixId = (Integer) source.next();
        Integer fractionId = (Integer) source.next();
        Integer methodId = (Integer) source.next();
        Integer unitId = (Integer) source.next();

        // get existing pmfm by the four criteria
        List<PmfmDTO> pmfms = pmfmDao.findPmfms(parameterCode, matrixId, fractionId, methodId, unitId, null, StatusFilter.ACTIVE.toStatusCodes());

        if (pmfms.size() == 1) {
            // corresponding pmfm found
            result.setPmfm(pmfms.get(0));

        } else {

            PmfmDTO pmfm = ReefDbBeanFactory.newPmfmDTO();
            // only parameter is mandatory
            pmfm.setParameter(parameterDao.getParameterByCode(parameterCode));

            if (matrixId != null) {
                pmfm.setMatrix(matrixDao.getMatrixById(matrixId));
            }
            if (fractionId != null) {
                pmfm.setFraction(fractionDao.getFractionById(fractionId));
            }
            if (methodId != null) {
                pmfm.setMethod(methodDao.getMethodById(methodId));
            }
            if (unitId != null) {
                pmfm.setUnit(unitDao.getUnitById(unitId));
            }

            result.setPmfm(pmfm);

        }
        return result;
    }
}
