package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface ReefDbMethodDao {

    String ALL_METHODS_CACHE = "all_methods";
    String METHOD_BY_ID_CACHE = "method_by_id";

    /**
     * <p>getAllMethods.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_METHODS_CACHE)
    List<MethodDTO> getAllMethods(List<String> statusCodes);

    /**
     * <p>getMethodById.</p>
     *
     * @param methodId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.MethodDTO} object.
     */
    @Cacheable(value = METHOD_BY_ID_CACHE)
    MethodDTO getMethodById(int methodId);

    /**
     * <p>findMethods.</p>
     *
     * @param methodId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<MethodDTO> findMethods(Integer methodId, List<String> statusCodes);

    /**
     * <p>saveMethods.</p>
     *
     * @param methods a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_METHODS_CACHE,
            METHOD_BY_ID_CACHE
    }, allEntries = true)
    void saveMethods(List<? extends MethodDTO> methods);

    /**
     * <p>deleteMethods.</p>
     *
     * @param methodIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_METHODS_CACHE,
            METHOD_BY_ID_CACHE
    }, allEntries = true)
    void deleteMethods(List<Integer> methodIds);

    /**
     * <p>replaceTemporaryMethod.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_METHODS_CACHE,
            METHOD_BY_ID_CACHE
    }, allEntries = true)
    void replaceTemporaryMethod(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isMethodUsedInProgram.</p>
     *
     * @param methodId a int.
     * @return a boolean.
     */
    boolean isMethodUsedInProgram(int methodId);

    /**
     * <p>isMethodUsedInRules.</p>
     *
     * @param methodId a int.
     * @return a boolean.
     */
    boolean isMethodUsedInRules(int methodId);

    /**
     * <p>isMethodUsedInReferential.</p>
     *
     * @param methodId a int.
     * @return a boolean.
     */
    boolean isMethodUsedInReferential(int methodId);

}
