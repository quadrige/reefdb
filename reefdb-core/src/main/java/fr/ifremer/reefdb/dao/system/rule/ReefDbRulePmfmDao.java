package fr.ifremer.reefdb.dao.system.rule;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RulePmfmDao;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;

import java.util.List;

/**
 * @author peck7 on 03/07/2018.
 */
public interface ReefDbRulePmfmDao extends RulePmfmDao {

    /**
     * <p>getRulePmfmByRuleCode.</p>
     *
     * @param ruleCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<RulePmfmDTO> getRulePmfmByRuleCode(String ruleCode);

    /**
     * Save rule pmfm
     *
     * @param pmfm   a {@link PmfmDTO} object.
     * @param ruleCd the parent rule
     */
    RulePmfmDTO save(RulePmfmDTO pmfm, String ruleCd);

}
