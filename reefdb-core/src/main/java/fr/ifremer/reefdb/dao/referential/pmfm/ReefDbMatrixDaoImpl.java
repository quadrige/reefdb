package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Fraction;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.FractionImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Matrix;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MatrixDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * Matrix DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("reefDbMatrixDao")
public class ReefDbMatrixDaoImpl extends MatrixDaoImpl implements ReefDbMatrixDao {

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_PMFM", "MATRIX_ID").build();

    private static final Multimap<String, String> columnNamesByReferentialTableNames = ImmutableListMultimap.<String, String>builder()
            .put("FRACTION_MATRIX", "MATRIX_ID")
            .put("PMFM", "MATRIX_ID").build();

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbFractionDao")
    protected ReefDbFractionDao fractionDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbMatrixDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<MatrixDTO> getAllMatrices(List<String> statusCodes) {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allMatrices"), statusCodes);

        List<MatrixDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            MatrixDTO matrix = toMatrixDTO(Arrays.asList(source).iterator());
            // add fractions
            matrix.addAllFractions(fractionDao.getFractionsByMatrixId(matrix.getId()));
            result.add(matrix);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public MatrixDTO getMatrixById(int matrixId) {

        Object[] source = queryUnique("matrixById", "matrixId", IntegerType.INSTANCE, matrixId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load matrix with id = " + matrixId);
        }

        MatrixDTO result = toMatrixDTO(Arrays.asList(source).iterator());

        // add fractions
        result.addAllFractions(fractionDao.getFractionsByMatrixId(result.getId()));

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<MatrixDTO> findMatrices(Integer matrixId, List<String> statusCodes) {

        Query query = createQuery("matrixByCriteria", "matrixId", IntegerType.INSTANCE, matrixId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<MatrixDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            MatrixDTO matrix = toMatrixDTO(Arrays.asList(source).iterator());
            // add fractions
            matrix.addAllFractions(fractionDao.getFractionsByMatrixId(matrix.getId()));

            result.add(matrix);
        }

        return ImmutableList.copyOf(result);

    }

    /** {@inheritDoc} */
    @Override
    public void saveMatrices(List<? extends MatrixDTO> matrices) {
        if (CollectionUtils.isEmpty(matrices)) {
            return;
        }

        for (MatrixDTO matrix : matrices) {
            if (matrix.isDirty()) {
                saveMatrix(matrix);
                matrix.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteMatrices(List<Integer> matrixIds) {
        if (matrixIds == null) return;
        matrixIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryMatrix(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdate(columnNamesByReferentialTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMatrixUsedInProgram(int matrixId) {

        return queryCount("countPmfmStrategyByMatrixId", "matrixId", IntegerType.INSTANCE, matrixId) > 0;
   }

    /** {@inheritDoc} */
    @Override
    public boolean isMatrixUsedInRules(int matrixId) {

        return executeMultipleCount(columnNamesByRulesTableNames, matrixId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isMatrixUsedInReferential(int matrixId) {

        return executeMultipleCount(columnNamesByReferentialTableNames, matrixId) || isMatrixUsedInRules(matrixId);
    }

    private void saveMatrix(MatrixDTO matrix) {
        Assert.notNull(matrix);
        Assert.notBlank(matrix.getName());

        if (matrix.getStatus() == null) {
            matrix.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(matrix.getStatus()), "source must have local status");

        Matrix target;
        if (matrix.getId() == null) {
            target = Matrix.Factory.newInstance();
            target.setMatrixId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(matrix.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setStatus(load(StatusImpl.class, matrix.getStatus().getCode()));
        target.setMatrixNm(matrix.getName());
        target.setMatrixDc(matrix.getDescription());
        if (target.getMatrixCreationDt() == null) {
            target.setMatrixCreationDt(newCreateDate());
        }
        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        matrix.setId(target.getMatrixId());

        // save associated fractions
        if (matrix.sizeFractions() > 0) {

            Map<Integer, Fraction> remainingFractions = ReefDbBeans.mapByProperty(target.getFractions(), "fractionId");
            for (FractionDTO fraction : matrix.getFractions()) {
                if (remainingFractions.remove(fraction.getId()) == null) {
                    // add fraction
                    target.addFractions(load(FractionImpl.class, fraction.getId()));
                }
            }

            if (!remainingFractions.isEmpty()) {
                target.getFractions().removeAll(remainingFractions.values());
            }

        } else if (target.getFractions() != null) {
            target.getFractions().clear();
        }

        getSession().save(target);

    }

    private MatrixDTO toMatrixDTO(Iterator<Object> source) {
        MatrixDTO result = ReefDbBeanFactory.newMatrixDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
