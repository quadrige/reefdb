package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 17/07/2015.
 */
public interface ReefDbAnalysisInstrumentDao {

    String ANALYSIS_INSTRUMENT_BY_ID_CACHE = "analysis_instrument_by_id";
    String ALL_ANALYSIS_INSTRUMENTS_CACHE = "all_analysis_instruments";
    String ANALYSIS_INSTRUMENTS_BY_IDS_CACHE = "analysis_instruments_by_ids";

    /**
     * <p>getAllAnalysisInstruments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_ANALYSIS_INSTRUMENTS_CACHE)
    List<AnalysisInstrumentDTO> getAllAnalysisInstruments(List<String> statusCodes);

    /**
     * <p>getAnalysisInstrumentById.</p>
     *
     * @param analysisInstrumentId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.AnalysisInstrumentDTO} object.
     */
    @Cacheable(value = ANALYSIS_INSTRUMENT_BY_ID_CACHE)
    AnalysisInstrumentDTO getAnalysisInstrumentById(int analysisInstrumentId);

    /**
     * <p>getAnalysisInstrumentsByIds.</p>
     *
     * @param analysisInstrumentsIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ANALYSIS_INSTRUMENTS_BY_IDS_CACHE)
    List<AnalysisInstrumentDTO> getAnalysisInstrumentsByIds(List<Integer> analysisInstrumentsIds);

    /**
     * <p>findAnalysisInstruments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param analysisInstrumentId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> findAnalysisInstruments(List<String> statusCodes, Integer analysisInstrumentId);

    /**
     * <p>findAnalysisInstrumentsByName.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param analysisInstrumentName a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<AnalysisInstrumentDTO> findAnalysisInstrumentsByName(List<String> statusCodes, String analysisInstrumentName);

    /**
     * <p>saveAnalysisInstruments.</p>
     *
     * @param analysisInstruments a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_ANALYSIS_INSTRUMENTS_CACHE,
            ANALYSIS_INSTRUMENT_BY_ID_CACHE,
            ANALYSIS_INSTRUMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void saveAnalysisInstruments(List<? extends AnalysisInstrumentDTO> analysisInstruments);

    /**
     * <p>deleteAnalysisInstruments.</p>
     *
     * @param analysisInstrumentIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_ANALYSIS_INSTRUMENTS_CACHE,
            ANALYSIS_INSTRUMENT_BY_ID_CACHE,
            ANALYSIS_INSTRUMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void deleteAnalysisInstruments(List<Integer> analysisInstrumentIds);

    /**
     * <p>replaceTemporaryAnalysisInstrument.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_ANALYSIS_INSTRUMENTS_CACHE,
            ANALYSIS_INSTRUMENT_BY_ID_CACHE,
            ANALYSIS_INSTRUMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void replaceTemporaryAnalysisInstrument(Integer sourceId, Integer targetId, boolean delete);

    /**
     * Check if this analysis instrument is used in data
     * MEASUREMENT, TAXON_MEASUREMENT, TODO MEASUREMENT_FILE
     *
     * @param analysisInstrumentId a int.
     * @return a boolean.
     */
    boolean isAnalysisInstrumentUsedInData(int analysisInstrumentId);

    /**
     * Check if this analysis instrument is used in validated data
     * MEASUREMENT, TAXON_MEASUREMENT, TODO MEASUREMENT_FILE
     * validated data has a validation date
     *
     * @param analysisInstrumentId a int.
     * @return a boolean.
     */
    boolean isAnalysisInstrumentUsedInValidatedData(int analysisInstrumentId);

    /**
     * Check if this analysis instrument is used in strategy
     * PMFM_APPLIED_STRATEGY
     *
     * @param analysisInstrumentId a int.
     * @return a boolean.
     */
    boolean isAnalysisInstrumentUsedInProgram(int analysisInstrumentId);
}
