package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentExtendDao;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>ReefDbDepartmentDao interface.</p>
 *
 */
public interface ReefDbDepartmentDao extends DepartmentExtendDao {

    String ALL_DEPARTMENTS_CACHE = "all_departments";
    String DEPARTMENT_BY_ID_CACHE = "department_by_id";
    String DEPARTMENTS_BY_IDS_CACHE = "departments_by_ids";
    String INHERITED_DEPARTMENT_IDS_CACHE = "inherited_department_ids";

    /**
     * <p>getAllDepartments.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_DEPARTMENTS_CACHE)
    List<DepartmentDTO> getAllDepartments(List<String> statusCodes);

    /**
     * <p>getDepartmentById.</p>
     *
     * @param departementId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     */
    @Cacheable(value = DEPARTMENT_BY_ID_CACHE)
    DepartmentDTO getDepartmentById(int departementId);

    /**
     * <p>getDepartmentsByIds.</p>
     *
     * @param departementIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = DEPARTMENTS_BY_IDS_CACHE)
    List<DepartmentDTO> getDepartmentsByIds(List<Integer> departementIds);

    /**
     * <p>findDepartmentsByCodeAndName.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     * @param strictName a boolean.
     * @param parentId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<DepartmentDTO> findDepartmentsByCodeAndName(String code, String name, boolean strictName, Integer parentId, List<String> statusCodes);

    /**
     * <p>saveTemporaryDepartment.</p>
     *
     * @param departmentBean a {@link fr.ifremer.reefdb.dto.referential.DepartmentDTO} object.
     */
    @CacheEvict(value = {
            ALL_DEPARTMENTS_CACHE,
            DEPARTMENT_BY_ID_CACHE,
            DEPARTMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void saveTemporaryDepartment(DepartmentDTO departmentBean);

    /**
     * <p>deleteTemporaryDepartments.</p>
     *
     * @param departmentIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_DEPARTMENTS_CACHE,
            DEPARTMENT_BY_ID_CACHE,
            DEPARTMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void deleteTemporaryDepartments(List<Integer> departmentIds);

    /**
     * <p>replaceTemporaryDepartmentFks.</p>
     *
     * @param sourceDepId a {@link java.lang.Integer} object.
     * @param targetDepId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_DEPARTMENTS_CACHE,
            DEPARTMENT_BY_ID_CACHE,
            DEPARTMENTS_BY_IDS_CACHE
    }, allEntries = true)
    void replaceTemporaryDepartmentFks(Integer sourceDepId, Integer targetDepId, boolean delete);

    /**
     * <p>isDepartmentUsedInProgram.</p>
     *
     * @param departmentId a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInProgram(int departmentId);

    /**
     * <p>isDepartmentUsedInRules.</p>
     *
     * @param departmentId a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInRules(int departmentId);

    /**
     * <p>isDepartmentUsedInData.</p>
     *
     * @param departmentId a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInData(int departmentId);

    /**
     * <p>isDepartmentUsedInValidatedData.</p>
     *
     * @param id a int.
     * @return a boolean.
     */
    boolean isDepartmentUsedInValidatedData(int id);

    @Cacheable(value = INHERITED_DEPARTMENT_IDS_CACHE)
    List<Integer> getInheritedRecorderDepartmentIds(int departmentId);

}
