package fr.ifremer.reefdb.dao.data.samplingoperation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperation;
import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperationExtendDao;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>ReefDbSamplingOperationDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbSamplingOperationDao extends SamplingOperationExtendDao {

    /**
     * <p>getSamplingOperationsBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param withIndividualMeasurements a boolean.
     * @return a {@link java.util.List} object.
     */
    List<SamplingOperationDTO> getSamplingOperationsBySurveyId(int surveyId, boolean withIndividualMeasurements);

    List<Integer> getSamplingOperationIdsBySurveyId(int surveyId);

    /**
     * <p>saveSamplingOperationsBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param samplingOperations a {@link java.util.Collection} object.
     */
    void saveSamplingOperationsBySurveyId(int surveyId, Collection<SamplingOperationDTO> samplingOperations);

    /**
     * <p>removeBySurveyId.</p>
     *
     * @param surveyId a int.
     */
    void removeBySurveyId(int surveyId);

    /**
     * Validate all sampling operations by survey id
     *
     * @param surveyIds the survey id
     * @param validationDate the validation date
     */
    int validateBySurveyIds(Collection<Integer> surveyIds, Date validationDate);

    /**
     * Unvalidate all sampling operations by survey id
     *
     * @param surveyIds        the survey id
     * @param unvalidationDate
     * @param validatorId
     */
    int unvalidateBySurveyIds(Collection<Integer> surveyIds, Date unvalidationDate, int validatorId);

    List<SamplingOperation> getQualifiedSamplingOperationBySurveyId(int surveyId, boolean withData);

    /**
     * <p>updateSamplingOperationsControlDate.</p>
     *
     * @param controlledElementsPks a {@link java.util.Collection} object.
     * @param controlDate a {@link java.util.Date} object.
     */
    void updateSamplingOperationsControlDate(Collection<Integer> controlledElementsPks, Date controlDate);


}
