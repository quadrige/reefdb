package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface ReefDbFractionDao {

    String FRACTION_BY_ID_CACHE = "fraction_by_id";
    String FRACTIONS_BY_MATRIX_ID_CACHE = "fractions_by_matrix_id";
    String ALL_FRACTIONS_CACHE = "all_fractions";

    /**
     * <p>getAllFractions.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_FRACTIONS_CACHE)
    List<FractionDTO> getAllFractions(List<String> statusCodes);

    /**
     * <p>getFractionById.</p>
     *
     * @param fractionId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO} object.
     */
    @Cacheable(value = FRACTION_BY_ID_CACHE)
    FractionDTO getFractionById(int fractionId);

    /**
     * <p>getFractionsByMatrixId.</p>
     *
     * @param matrixId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = FRACTIONS_BY_MATRIX_ID_CACHE)
    List<FractionDTO> getFractionsByMatrixId(Integer matrixId);

    /**
     * <p>findFractions.</p>
     *
     * @param fractionId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<FractionDTO> findFractions(Integer fractionId, List<String> statusCodes);

    /**
     * <p>saveFractions.</p>
     *
     * @param fractions a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            FRACTION_BY_ID_CACHE,
            FRACTIONS_BY_MATRIX_ID_CACHE,
            ALL_FRACTIONS_CACHE
    }, allEntries = true)
    void saveFractions(List<? extends FractionDTO> fractions);

    /**
     * <p>deleteFractions.</p>
     *
     * @param fractionIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            FRACTION_BY_ID_CACHE,
            FRACTIONS_BY_MATRIX_ID_CACHE,
            ALL_FRACTIONS_CACHE
    }, allEntries = true)
    void deleteFractions(List<Integer> fractionIds);

    /**
     * <p>replaceTemporaryFraction.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            FRACTION_BY_ID_CACHE,
            FRACTIONS_BY_MATRIX_ID_CACHE,
            ALL_FRACTIONS_CACHE
    }, allEntries = true)
    void replaceTemporaryFraction(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isFractionUsedInProgram.</p>
     *
     * @param fractionId a int.
     * @return a boolean.
     */
    boolean isFractionUsedInProgram(int fractionId);

    /**
     * <p>isFractionUsedInRules.</p>
     *
     * @param fractionId a int.
     * @return a boolean.
     */
    boolean isFractionUsedInRules(int fractionId);

    /**
     * <p>isFractionUsedInReferential.</p>
     *
     * @param fractionId a int.
     * @return a boolean.
     */
    boolean isFractionUsedInReferential(int fractionId);

}
