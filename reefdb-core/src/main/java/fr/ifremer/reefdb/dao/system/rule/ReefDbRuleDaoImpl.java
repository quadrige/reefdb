package fr.ifremer.reefdb.dao.system.rule;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.system.rule.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.QuadrigeEnumerationDef;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.exception.QuadrigeTechnicalException;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.FunctionDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.*;
import fr.ifremer.reefdb.dto.enums.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListValuedMap;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.MultiMapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.i18n.I18n;
import org.nuiton.util.DateUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.*;

/**
 * @author peck7 on 03/07/2018.
 */
@Repository("reefDbRuleDao")
public class ReefDbRuleDaoImpl extends RuleDaoImpl implements ReefDbRuleDao, InitializingBean {

    private static final Log LOG = LogFactory.getLog(ReefDbRuleDaoImpl.class);
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "rulePreconditionDao")
    private RulePreconditionExtendDao rulePreconditionDao;
    @Resource(name = "ruleGroupDao")
    private RuleGroupExtendDao ruleGroupDao;
    @Resource(name = "reefDbRulePmfmDao")
    protected ReefDbRulePmfmDao rulePmfmDao;
    @Resource(name = "ruleParameterDao")
    protected RuleParameterDao ruleParameterDao;

    private int functionIdMinMax;
    private int functionIdIn;
    private int functionIdDateMinMax;
    private int functionParameterIdMin;
    private int functionParameterIdMax;
    private int functionParameterIdIn;
    private int functionParameterIdDateMin;
    private int functionParameterIdDateMax;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory sessionFactory
     */
    @Autowired
    public ReefDbRuleDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void afterPropertiesSet() {
        initConstants();
    }

    private void initConstants() {

        functionIdMinMax = ControlFunctionValues.MIN_MAX.getFunctionId();
        functionIdDateMinMax = ControlFunctionValues.MIN_MAX_DATE.getFunctionId();
        functionIdIn = ControlFunctionValues.IS_AMONG.getFunctionId();
//        functionIdEmpty = ControlFunctionValues.IS_EMPTY.getFunctionId();
//        functionIdNotEmpty = ControlFunctionValues.NOT_EMPTY.getFunctionId();

        functionParameterIdMin = FunctionParameterId.MIN.getValue();
        functionParameterIdMax = FunctionParameterId.MAX.getValue();
        functionParameterIdIn = FunctionParameterId.IN.getValue();
        functionParameterIdDateMin = FunctionParameterId.DATE_MIN.getValue();
        functionParameterIdDateMax = FunctionParameterId.DATE_MAX.getValue();
    }

    @Override
    public List<ControlRuleDTO> getControlRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule) {
        Assert.notBlank(ruleListCode);

        Iterator<Object[]> it = queryIterator("rulesForControlOnlyByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode,
                "functionIdMinMax", IntegerType.INSTANCE, functionIdMinMax,
                "functionParameterIdMin", IntegerType.INSTANCE, functionParameterIdMin,
                "functionParameterIdMax", IntegerType.INSTANCE, functionParameterIdMax,
                "functionIdDateMinMax", IntegerType.INSTANCE, functionIdDateMinMax,
                "functionParameterIdDateMin", IntegerType.INSTANCE, functionParameterIdDateMin,
                "functionParameterIdDateMax", IntegerType.INSTANCE, functionParameterIdDateMax,
                "functionIdIn", IntegerType.INSTANCE, functionIdIn,
                "functionParameterIdIn", IntegerType.INSTANCE, functionParameterIdIn,
                "ruleIsActive", StringType.INSTANCE, onlyActive ? Daos.convertToString(true) : null);

        List<ControlRuleDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            ControlRuleDTO rule = toRuleDTO(Arrays.asList(source).iterator());
            if (rule != null) {
                // Add this rule if valid (!= null)
                result.add(rule);
            } else if (incompatibleRule != null) {
                incompatibleRule.setTrue();
            }
        }

        for (ControlRuleDTO rule : result) {

            // add pmfm list
            rule.setRulePmfms(rulePmfmDao.getRulePmfmByRuleCode(rule.getCode()));
        }

        return result;

    }

    @Override
    public List<ControlRuleDTO> getPreconditionedRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule) {

        List<ControlRuleDTO> result = new ArrayList<>();
        List<PreconditionRuleDTO> allPreconditionRules = getPreconditionsByRuleListCode(ruleListCode, onlyActive, incompatibleRule);

        // group preconditions by their name
        ListValuedMap<String, PreconditionRuleDTO> preconditionRulesByName = MultiMapUtils.newListValuedHashMap();
        for (PreconditionRuleDTO preconditionRule : allPreconditionRules) {
            preconditionRulesByName.put(preconditionRule.getName(), preconditionRule);
        }

        // iterate on the map to created preconditioned ('virtual') control rule
        for (String name : preconditionRulesByName.keySet()) {
            ControlRuleDTO preconditionedControlRule = ReefDbBeanFactory.newControlRuleDTO();
            // set the name as rule code
            preconditionedControlRule.setCode(name);
            // set control element to measurement
            preconditionedControlRule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            // set control attribute to qualitative value
            preconditionedControlRule.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());

            // add all preconditions
            boolean active = true;
            ControlFunctionValues function = null;
            Set<String> pmfmPairKeys = new HashSet<>();
            List<PreconditionRuleDTO> preconditionRules = preconditionRulesByName.get(name);
            for (PreconditionRuleDTO preconditionRule: preconditionRules) {
                active &= preconditionRule.isActive();

                // find the function
                if (function == null) {
                    function = getFunction(preconditionRule);
                } else {
                    ControlFunctionValues thisFunction = getFunction(preconditionRule);
                    if (!function.equals(thisFunction)) {
                        if (LOG.isWarnEnabled()) {
                            LOG.warn(String.format("Different precondition functions found (preconditionLb=%s)", preconditionedControlRule.getCode()));
                        }
                        if (incompatibleRule != null) incompatibleRule.setTrue();
                        // skip it
                        continue;
                    }
                }

                // compute pmfm pair key and add it to the hash set
                pmfmPairKeys.add(String.format("%s#%s",
                    getRulePmfmKey(preconditionRule.getBaseRule().getRulePmfms(0)),
                    getRulePmfmKey(preconditionRule.getUsedRule().getRulePmfms(0))));
                // affect parent link
                preconditionRule.setRule(preconditionedControlRule);
                preconditionedControlRule.addPreconditions(preconditionRule);
            }
            // set the active flag (if 1 precondition is inactive, all control rule is inactive)
            preconditionedControlRule.setActive(active);

            // set description and message from first preconditioned rule
            ControlRuleDTO firstBaseRule = CollectionUtils.isNotEmpty(preconditionRules) ? preconditionRules.get(0).getBaseRule() : null;
            if (firstBaseRule != null) {
                preconditionedControlRule.setDescription(firstBaseRule.getDescription());
                preconditionedControlRule.setMessage(firstBaseRule.getMessage());
            }

            // set the function
            if (function == null) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(String.format("Precondition function not found (preconditionLb=%s)", preconditionedControlRule.getCode()));
                }
                if (incompatibleRule != null) incompatibleRule.setTrue();
                // skip it
                continue;
            }
            preconditionedControlRule.setFunction(function.toFunctionDTO());

            // check rule pmfm equality
            if (pmfmPairKeys.size() > 1) {
                // if more than 1 pmfm is detected, it is an incompatible rule precondition
                if (LOG.isWarnEnabled()) {
                    LOG.warn(String.format("Only 1 pair of PMFMU is allowed in preconditioned rules (preconditionLb=%s)",
                            preconditionedControlRule.getCode()));
                }
                if (incompatibleRule != null) incompatibleRule.setTrue();
                // skip it
                continue;
            }
            // populate the rulePmfm from the first precondition
            preconditionedControlRule.addRulePmfms(preconditionedControlRule.getPreconditions(0).getBaseRule().getRulePmfms(0));
            preconditionedControlRule.addRulePmfms(preconditionedControlRule.getPreconditions(0).getUsedRule().getRulePmfms(0));

            result.add(preconditionedControlRule);
        }

        return result;
    }

    /**
     *  Create virtual control rule with rule groups
     *  For now only the group with TaxonGroup, Taxon and PMFM value is allowed (with OR logical)
     *
     * @param ruleListCode
     * @param onlyActive
     * @param incompatibleRule
     * @return
     */
    @Override
    public List<ControlRuleDTO> getGroupedRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule) {

        List<ControlRuleDTO> result = new ArrayList<>();
        List<RuleGroupDTO> groups = getGroupsByRuleListCode(ruleListCode, onlyActive, incompatibleRule);

        // group groups by their name
        ListValuedMap<String, RuleGroupDTO> groupsByName = MultiMapUtils.newListValuedHashMap();
        for (RuleGroupDTO group : groups) {
            groupsByName.put(group.getName(), group);
        }

        // iterate on the map to created grouped ('virtual') control rule
        for (String name : groupsByName.keySet()) {
            ControlRuleDTO groupedRule = ReefDbBeanFactory.newControlRuleDTO();
            groupedRule.setCode(name);
            // set control element to measurement
            groupedRule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
            // set control attribute to PMFM
            groupedRule.setControlFeature(ControlFeatureMeasurementValues.PMFM.toControlFeatureDTO());
            // set function (only this function is allowed)
            groupedRule.setFunction(ControlFunctionValues.NOT_EMPTY_CONDITIONAL.toFunctionDTO());

            boolean active = true;
            // check and add all rules
            for (RuleGroupDTO group: groupsByName.get(name)) {
                active &= group.isActive();

                if (!group.isIsOr()) {
                    if (LOG.isWarnEnabled()) {
                        LOG.warn(String.format("a group with AND logical operator found (ruleGroupLb=%s), group ignored", group.getName()));
                    }
                    if (incompatibleRule != null) incompatibleRule.setTrue();
                    // skip it
                    continue;
                }

                groupedRule.addGroups(group);
            }
            // set the active flag (if 1 group is inactive, all control rule is inactive)
            groupedRule.setActive(active);

            // check the 3 rules = taxon_group not empty, taxon not empty and a pmfm value not empty
            Collection<RulePmfmDTO> rulePmfms = null;
            if (groupedRule.sizeGroups() == 3) {
                boolean taxonGroupRuleFound = false;
                boolean taxonRuleFound = false;
                boolean pmfmRuleFound = false;
                for (RuleGroupDTO group : groupedRule.getGroups()) {
                    if (ControlFunctionValues.NOT_EMPTY.equals(group.getRule().getFunction())
                        && ControlElementValues.MEASUREMENT.equals(group.getRule().getControlElement())
                        && ControlFeatureMeasurementValues.TAXON_GROUP.equals(group.getRule().getControlFeature())) {
                        taxonGroupRuleFound = true;
                    }
                    if (ControlFunctionValues.NOT_EMPTY.equals(group.getRule().getFunction())
                        && ControlElementValues.MEASUREMENT.equals(group.getRule().getControlElement())
                        && ControlFeatureMeasurementValues.TAXON.equals(group.getRule().getControlFeature())) {
                        taxonRuleFound = true;
                    }
                    if (ControlFunctionValues.NOT_EMPTY.equals(group.getRule().getFunction())
                        && ControlElementValues.MEASUREMENT.equals(group.getRule().getControlElement())
                        && ControlFeatureMeasurementValues.PMFM.equals(group.getRule().getControlFeature())) {
                        pmfmRuleFound = true;
                        rulePmfms = group.getRule().getRulePmfms();
                    }
                }
                if (!taxonGroupRuleFound || !taxonRuleFound || !pmfmRuleFound) {
                    if (LOG.isWarnEnabled()) {
                        LOG.warn(String.format("the 3 rules have not been found : taxon_group=%s taxon=%s pmfmu=%s (ruleGroupLb=%s), group ignored",
                                taxonGroupRuleFound, taxonRuleFound, pmfmRuleFound,
                                groupedRule.getCode()));
                    }
                    if (incompatibleRule != null) incompatibleRule.setTrue();
                    continue;
                }

            } else {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(String.format("a group without 3 rules has been found (ruleGroupLb=%s), group ignored", groupedRule.getCode()));
                }
                if (incompatibleRule != null) incompatibleRule.setTrue();
                continue;
            }

            // populate the pmfms on virtual rule
            groupedRule.addAllRulePmfms(rulePmfms);

            // set description and message from first rule (should be the same on all rules)
            groupedRule.setDescription(groupedRule.getGroups(0).getRule().getDescription());
            groupedRule.setMessage(groupedRule.getGroups(0).getRule().getMessage());

            result.add(groupedRule);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ControlRuleDTO> findActiveControlRules(Date date, String programCode, Integer departmentId) {

        List<ControlRuleDTO> result = Lists.newArrayList();
        Iterator<String> ruleListCodeIt = getActiveRuleListCodes(date, programCode, departmentId);
        while (ruleListCodeIt.hasNext()) {
            String ruleListCode = ruleListCodeIt.next();
            result.addAll(getControlRulesByRuleListCode(ruleListCode, true /*active only*/, null));
        }

        return result;
    }

    @Override
    public List<ControlRuleDTO> findActivePreconditionedRules(Date date, String programCode, Integer departmentId) {

        return findActivePreconditionedRules(getActiveRuleListCodes(date, programCode, departmentId));
    }

    @Override
    public List<ControlRuleDTO> findActiveGroupedRules(Date date, String programCode, Integer departmentId) {

        return findActiveGroupedRules(getActiveRuleListCodes(date, programCode, departmentId));
    }

    @Override
    public List<ControlRuleDTO> findActivePreconditionedRules(List<String> programCodes) {

        return findActivePreconditionedRules(getActiveRuleListCodes(programCodes));
    }

    private List<ControlRuleDTO> findActivePreconditionedRules(Iterator<String> ruleListCodeIt) {
        List<ControlRuleDTO> result = Lists.newArrayList();
        while (ruleListCodeIt.hasNext()) {
            String ruleListCode = ruleListCodeIt.next();
            result.addAll(getPreconditionedRulesByRuleListCode(ruleListCode, true /*active only*/, null));
        }
        return result;
    }

    private List<ControlRuleDTO> findActiveGroupedRules(Iterator<String> ruleListCodeIt) {
        List<ControlRuleDTO> result = Lists.newArrayList();
        while (ruleListCodeIt.hasNext()) {
            String ruleListCode = ruleListCodeIt.next();
            result.addAll(getGroupedRulesByRuleListCode(ruleListCode, true /*active only*/, null));
        }
        return result;
    }


    @Override
    public void save(final ControlRuleDTO source, String ruleListCd) {
        Assert.notNull(source);
        Assert.notNull(source.getFunction());
        Assert.notNull(source.getFunction().getId());

        // Parent
        RuleList parent = get(RuleListImpl.class, ruleListCd);

        if (source.getFunction().getId() > 0) {
            save(source, parent);
        } else if (!source.isPreconditionsEmpty()) {
            saveWithPreconditions(source, parent);
        } else if (!source.isGroupsEmpty()) {
            saveWithGroups(source, parent);
        }
    }

    private void save(ControlRuleDTO source, RuleList parent) {

        Assert.isTrue(source.isPreconditionsEmpty());
        Assert.isTrue(source.isGroupsEmpty());

        // delete previous rule with the same code (Mantis #45374) but not itself (Mantis #45919)
        deleteObsoleteRulePreconditions(source.getCode());
        deleteObsoleteRuleGroups(source.getCode());

        // Load entity
        Rule entity = get(source.getCode());
        boolean isNew = false;
        if (entity == null) {
            entity = Rule.Factory.newInstance();
            entity.setRuleCd(source.getCode());
            parent.addRules(entity);
            entity.setRuleList(parent);
            isNew = true;
        }

        // DTO -> VO
        beanToEntity(source, entity);

        // Save it
        if (isNew) {
            getSession().save(entity);
        } else {
            getSession().update(entity);
        }

        // Save rule parameters
        {
            Integer functionId = source.getFunction().getId();
            Map<Integer, RuleParameter> ruleParametersByFunctionParamIds = ReefDbBeans.mapByProperty(entity.getRuleParameters(), "functionParameter.functionParId");
            final Iterator<Integer> ruleParIdSequence = TemporaryDataHelper.getNewNegativeTemporarySequence(getSession(), RuleParameterImpl.class, true);

            // MinMax function
            if (functionId.equals(functionIdMinMax)) {
                if (source.getMin() != null && source.getMin() instanceof Double) {
                    updateOrCreateRuleParameter(entity,
                            ruleParIdSequence,
                            ruleParametersByFunctionParamIds,
                            functionParameterIdMin,
                            source.getMin().toString());
                }
                if (source.getMax() != null && source.getMax() instanceof Double) {
                    updateOrCreateRuleParameter(entity,
                            ruleParIdSequence,
                            ruleParametersByFunctionParamIds,
                            functionParameterIdMax,
                            source.getMax().toString());
                }
            }

            // MinMax Date function
            else if (functionId.equals(functionIdDateMinMax)) {
                if (source.getMin() != null && source.getMin() instanceof Date) {
                    updateOrCreateRuleParameter(entity,
                            ruleParIdSequence,
                            ruleParametersByFunctionParamIds,
                            functionParameterIdDateMin,
                            DateUtil.formatDate((Date) source.getMin(), DATE_PATTERN)
                    );
                }
                if (source.getMax() != null && source.getMax() instanceof Date) {
                    updateOrCreateRuleParameter(entity,
                            ruleParIdSequence,
                            ruleParametersByFunctionParamIds,
                            functionParameterIdDateMax,
                            DateUtil.formatDate((Date) source.getMax(), DATE_PATTERN)
                    );
                }
            }

            // In function
            else if (functionId.equals(functionIdIn) && source.getAllowedValues() != null) {
                updateOrCreateRuleParameter(entity,
                        ruleParIdSequence,
                        ruleParametersByFunctionParamIds,
                        functionParameterIdIn,
                        source.getAllowedValues());
            }

            // Remove unused rule parameters
            if (MapUtils.isNotEmpty(ruleParametersByFunctionParamIds)) {
                ruleParameterDao.remove(ruleParametersByFunctionParamIds.values());
                entity.getRuleParameters().removeAll(ruleParametersByFunctionParamIds.values());
            }

            getSession().update(entity);
        }

        getSession().flush();

        // Save rule pmfms
        {
            final Map<Integer, RulePmfm> rulePmfmsToRemove = ReefDbBeans.mapByProperty(entity.getRulePmfms(), "rulePmfmId");
            if (CollectionUtils.isNotEmpty(source.getRulePmfms())) {
                source.getRulePmfms().forEach(rulePmfm -> {
                    rulePmfm = rulePmfmDao.save(rulePmfm, source.getCode());
                    rulePmfmsToRemove.remove(rulePmfm.getId());
                });
            }

            // Remove unused rule pmfms
            if (MapUtils.isNotEmpty(rulePmfmsToRemove)) {
                rulePmfmDao.removeByIds(rulePmfmsToRemove.keySet());
                entity.getRulePmfms().removeAll(rulePmfmsToRemove.values());
            }
        }

        // reset this flag to avoid code changes
        source.setNewCode(false);
    }

    private void saveWithPreconditions(ControlRuleDTO source, RuleList parent) {

        Assert.isTrue(!source.isPreconditionsEmpty());
        Assert.isTrue(source.isGroupsEmpty());

        // delete previous rule with the same code (Mantis #45374)
        deleteObsoleteRule(source.getCode());
        // delete also obsolete rule groups
        deleteObsoleteRuleGroups(source.getCode());

        final Iterator<Integer> preconditionIdSequence = TemporaryDataHelper.getNewNegativeTemporarySequence(getSession(), RulePreconditionImpl.class, true);

        for (PreconditionRuleDTO precondition: source.getPreconditions()) {

            // Set description and message
            precondition.getBaseRule().setDescription(source.getDescription());
            precondition.getBaseRule().setMessage(source.getMessage());
            precondition.getUsedRule().setDescription(source.getDescription());
            precondition.getUsedRule().setMessage(source.getMessage());

            // save base and used rules first
            save(precondition.getBaseRule(), parent);
            save(precondition.getUsedRule(), parent);

            RulePrecondition entity = precondition.getId() != null ? rulePreconditionDao.get(precondition.getId()) : null;
            boolean isNew = false;
            if (entity == null) {
                entity = RulePrecondition.Factory.newInstance();
                entity.setRulePrecondId(preconditionIdSequence.next());
                isNew = true;
            }

            Rule baseRule = get(precondition.getBaseRule().getCode());
            Assert.notNull(baseRule);
            Rule usedRule = get(precondition.getUsedRule().getCode());
            Assert.notNull(usedRule);

            entity.setRule(baseRule);
            entity.setUsedRule(usedRule);
            // the precondition is active if user active the virtual rule
            entity.setRulePrecondIsActive(Daos.convertToString(source.isActive()));
            entity.setRulePrecondIsBidir(Daos.convertToString(precondition.isBidirectional()));
            // use the virtual rule code
            entity.setRulePrecondLb(source.getCode());

            if (isNew) {
                getSession().save(entity);
                precondition.setId(entity.getRulePrecondId());
            } else {
                getSession().update(entity);
            }
        }

    }

    private void saveWithGroups(ControlRuleDTO source, RuleList parent) {

        Assert.isTrue(!source.isGroupsEmpty());
        Assert.isTrue(source.isPreconditionsEmpty());
        final Iterator<Integer> groupIdSequence = TemporaryDataHelper.getNewNegativeTemporarySequence(getSession(), RuleGroupImpl.class, true);

        // delete previous rule with the same code (Mantis #45374)
        deleteObsoleteRule(source.getCode());
        // delete also obsolete rule preconditions
        deleteObsoleteRulePreconditions(source.getCode());

        for (RuleGroupDTO group: source.getGroups()) {

            // get the rule
            ControlRuleDTO groupedRule = group.getRule();
            // copy some properties from the virtual rule
            groupedRule.setDescription(source.getDescription());
            groupedRule.setMessage(source.getMessage());
            // save grouped rule first
            save(groupedRule, parent);

            RuleGroup entity = group.getId() != null ? ruleGroupDao.get(group.getId()) : null;
            boolean isNew = false;
            if (entity == null) {
                entity = RuleGroup.Factory.newInstance();
                entity.setRuleGroupId(groupIdSequence.next());
                isNew = true;
            }

            Rule rule = get(group.getRule().getCode());
            Assert.notNull(rule);

            entity.setRule(rule);
            // the precondition is active if user active the virtual rule
            entity.setRuleGroupIsActive(Daos.convertToString(source.isActive()));
            entity.setRuleGroupIsOr(Daos.convertToString(group.isIsOr()));
            // use the virtual rule code
            entity.setRuleGroupLb(source.getCode());

            if (isNew) {
                getSession().save(entity);
                group.setId(entity.getRuleGroupId());
            } else {
                getSession().update(entity);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FunctionDTO> getAllFunction() {

        // TODO by query or by FunctionDao ??

        Iterator<Function> it = queryIteratorTyped("allFunctionEntity");

        List<FunctionDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Function source = it.next();
            FunctionDTO target = toFunctionDTO(source, true /*Could return null if not found in rule function enumeration*/);
            if (target != null) {
                result.add(target);
            }
        }

        return result;
    }

    @Override
    public boolean ruleExists(String ruleCode) {
        Assert.notBlank(ruleCode);

        return queryUnique("ruleExists", "ruleCode", StringType.INSTANCE, ruleCode) != null;
    }

    /* -- private methods -- */

    private void deleteObsoleteRule(String ruleCode) {
        Rule rule = get(ruleCode);
        if (rule != null) {
            remove(rule);
        }
    }

    private void deleteObsoleteRulePreconditions(String ruleCode) {
        List<RulePrecondition> rulePreconditions = rulePreconditionDao.getByCode(ruleCode);
        if (CollectionUtils.isNotEmpty(rulePreconditions)) {
            rulePreconditionDao.remove(rulePreconditions);
        }
    }

    private void deleteObsoleteRuleGroups(String ruleCode) {
        List<RuleGroup> ruleGroups = ruleGroupDao.getByCode(ruleCode);
        if (CollectionUtils.isNotEmpty(ruleGroups)) {
            ruleGroupDao.remove(ruleGroups);
        }
    }

    private Iterator<String> getActiveRuleListCodes(Date date, String programCode, Integer departmentId) {

        // find active rule list corresponding to criteria
        return queryIteratorTyped("activeRuleListCodesByCriteria",
                "programCode", StringType.INSTANCE, programCode,
                "departmentId", IntegerType.INSTANCE, departmentId,
                "month", IntegerType.INSTANCE, Dates.convertToLocalDate(date, config.getDbTimezone()).getMonthValue());

    }

    @SuppressWarnings("unchecked")
    private Iterator<String> getActiveRuleListCodes(List<String> programCodes) {

        // find active rule list corresponding to program codes
        Query query = createQuery("activeRuleListCodesByProgramCodes").setParameterList("programCodes", programCodes);
        return query.iterate();

    }

    private ControlRuleDTO getRule(String ruleCode) {

        Object[] row = queryUnique("ruleByCode",
                "ruleCode", StringType.INSTANCE, ruleCode,
                "functionIdMinMax", IntegerType.INSTANCE, functionIdMinMax,
                "functionParameterIdMin", IntegerType.INSTANCE, functionParameterIdMin,
                "functionParameterIdMax", IntegerType.INSTANCE, functionParameterIdMax,
                "functionIdDateMinMax", IntegerType.INSTANCE, functionIdDateMinMax,
                "functionParameterIdDateMin", IntegerType.INSTANCE, functionParameterIdDateMin,
                "functionParameterIdDateMax", IntegerType.INSTANCE, functionParameterIdDateMax,
                "functionIdIn", IntegerType.INSTANCE, functionIdIn,
                "functionParameterIdIn", IntegerType.INSTANCE, functionParameterIdIn);

        if (row == null)
            throw new DataRetrievalFailureException("can't load rule with code = " + ruleCode);

        // the returned value can be null is rule is incompatible
        ControlRuleDTO controlRule = toRuleDTO(Arrays.asList(row).iterator());
        if (controlRule != null) {
            controlRule.setRulePmfms(rulePmfmDao.getRulePmfmByRuleCode(controlRule.getCode()));
        }
        return controlRule;
    }

    private List<PreconditionRuleDTO> getPreconditionsByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule) {

        Iterator<Object[]> rows = queryIterator("rulePreconditionsByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode,
                "rulePreconditionIsActive", StringType.INSTANCE, onlyActive ? Daos.convertToString(true) : null);

        List<PreconditionRuleDTO> result = new ArrayList<>();
        while (rows.hasNext()) {
            Object[] row = rows.next();
            PreconditionRuleDTO preconditionRule = toPreconditionRuleDTO(Arrays.asList(row).iterator());
            if (preconditionRule != null) {
                // a null preconditionRule means one the rule is incompatible
                result.add(preconditionRule);
            } else if (incompatibleRule != null) {
                incompatibleRule.setTrue();
            }
        }
        return result;
    }

    private PreconditionRuleDTO toPreconditionRuleDTO(Iterator<Object> source) {
        PreconditionRuleDTO result = ReefDbBeanFactory.newPreconditionRuleDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setActive(Daos.safeConvertToBoolean(source.next()));
        result.setBidirectional(Daos.safeConvertToBoolean(source.next()));

        // get base and used rules
        ControlRuleDTO baseRule = getRule((String) source.next());
        ControlRuleDTO usedRule = getRule((String) source.next());

        // if one of the rules is incompatible the precondition also
        if (baseRule == null || usedRule == null) return null;

        for (ControlRuleDTO rule : ImmutableList.of(baseRule, usedRule)) {
            // for now, accept only rules on measurement's qualitative or numerical value
            if (
                    (!ControlElementValues.MEASUREMENT.equals(rule.getControlElement()))
                            ||
                            (!ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(rule.getControlFeature())
                                    && !ControlFeatureMeasurementValues.NUMERICAL_VALUE.equals(rule.getControlFeature())
                            )) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(String.format("Only measurement's qualitative or numerical value are allowed in preconditioned rule (precondition=%s, rule=%s)",
                            result.getId(), rule.getCode()));
                }
                return null;
            }

            // accept only with 1 PMFM
            if (rule.sizeRulePmfms() != 1) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(String.format("Only 1 PMFMU is allowed in preconditioned rule (precondition=%s, rule=%s)",
                            result.getId(), rule.getCode()));
                }
                return null;
            }
        }

        // affect if all ok
        result.setBaseRule(baseRule);
        result.setUsedRule(usedRule);
        return result;
    }

    private List<RuleGroupDTO> getGroupsByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule) {

        Iterator<Object[]> rows = queryIterator("ruleGroupsByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode,
                "ruleGroupIsActive", StringType.INSTANCE, onlyActive ? Daos.convertToString(true) : null);

        List<RuleGroupDTO> result = new ArrayList<>();
        while (rows.hasNext()) {
            Object[] row = rows.next();
            RuleGroupDTO ruleGroup = toRuleGroupDTO(Arrays.asList(row).iterator());
            if (ruleGroup != null) {
                // a null preconditionRule means one the rule is incompatible
                result.add(ruleGroup);
            } else if (incompatibleRule != null) {
                incompatibleRule.setTrue();
            }
        }
        return result;
    }

    private RuleGroupDTO toRuleGroupDTO(Iterator<Object> source) {
        RuleGroupDTO result = ReefDbBeanFactory.newRuleGroupDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setActive(Daos.safeConvertToBoolean(source.next()));
        result.setIsOr(Daos.safeConvertToBoolean(source.next()));

        // get the grouped rule
        ControlRuleDTO rule = getRule((String) source.next());

        // if the rule is incompatible the group also
        if (rule == null) return null;
        result.setRule(rule);

        return result;
    }

    private ControlRuleDTO toRuleDTO(Iterator<Object> source) {
        ControlRuleDTO result = ReefDbBeanFactory.newControlRuleDTO();
        result.setCode((String) source.next());

        String controlledElementString = (String) source.next();

        if (controlledElementString != null) {
            int separatorIndex = controlledElementString.lastIndexOf(config.getAttributeSeparator());
            // Check if a separator exists (if not: skip controlled element)
            if (separatorIndex == -1 || separatorIndex == controlledElementString.length() - 1) {
                if (LOG.isWarnEnabled()) {
                    LOG.warn(I18n.t("reefdb.error.dao.ruleList.parse.controlledAttribute", controlledElementString, result.getCode()));
                }
                return null;
            } else {
                String controlElementCode = controlledElementString.substring(0, separatorIndex);
                String controlFeatureCode = controlledElementString.substring(separatorIndex + 1);

                // get control element by its code
                ControlElementValues controlElementValue = ControlElementValues.getByCode(controlElementCode);
                if (controlElementValue != null) {
                    result.setControlElement(controlElementValue.toControlElementDTO());

                    ControlFeatureDTO controlFeature = null;
                    // get control feature depending in control element
                    switch (controlElementValue) {

                        case MEASUREMENT:
                            controlFeature = ControlFeatureMeasurementValues.toControlFeatureDTO(controlFeatureCode);
                            break;
                        case SAMPLING_OPERATION:
                            controlFeature = ControlFeatureSamplingOperationValues.toControlFeatureDTO(controlFeatureCode);
                            break;
                        case SURVEY:
                            controlFeature = ControlFeatureSurveyValues.toControlFeatureDTO(controlFeatureCode);
                            break;
                    }

                    if (controlFeature != null) {
                        result.setControlFeature(controlFeature);
                    } else {
                        if (LOG.isWarnEnabled()) {
                            LOG.warn(I18n.t("reefdb.error.dao.ruleList.rule.skip", result.getCode(), controlledElementString));
                        }
                        return null;
                    }
                } else {
                    if (LOG.isWarnEnabled()) {
                        LOG.warn(I18n.t("reefdb.error.dao.ruleList.rule.skip", result.getCode(), controlElementCode));
                    }
                    return null;
                }
            }
        }

        result.setDescription((String) source.next());
        result.setActive(Daos.safeConvertToBoolean(source.next()));
        result.setBlocking(Daos.safeConvertToBoolean(source.next()));
        result.setMessage((String) source.next());

        // function entity
        Function function = (Function) source.next();
        result.setFunction(toFunctionDTO(function, false/*Could not return null value*/));

        // function parameters
        String valueMin = (String) source.next();
        String valueMax = (String) source.next();
        String dateMin = (String) source.next();
        String dateMax = (String) source.next();
        if (result.getFunction().getId().equals(functionIdDateMinMax)) {
            try {
                result.setMin(dateMin == null ? null : parseDate(dateMin));
            } catch (ParseException e) {
                LOG.warn(String.format("Error when parsing %s as date", dateMin), e);
                return null;
            }
            try {
                result.setMax(dateMax == null ? null : parseDate(dateMax));
            } catch (ParseException e) {
                LOG.warn(String.format("Error when parsing %s as date", dateMax), e);
                return null;
            }
        } else {
            result.setMin(valueMin == null ? null : Double.parseDouble(valueMin));
            result.setMax(valueMax == null ? null : Double.parseDouble(valueMax));
        }
        result.setAllowedValues((String) source.next());

        return result;
    }

    private Date parseDate(String s) throws ParseException {
        return StringUtils.isNumeric(s) ? new Date(Long.parseLong(s)) : DateUtils.parseDate(s, DATE_PATTERN);
    }

    private FunctionDTO toFunctionDTO(Function function, boolean couldBeNull) {
        FunctionDTO result = ReefDbBeanFactory.newFunctionDTO();
        result.setId(function.getFunctionId());
        ControlFunctionValues functionControlValue = ControlFunctionValues.getFunctionValue(function.getFunctionId());
        if (functionControlValue == null) {
            String errorMessage = String.format("Could not found enumeration for Function with id [%s]. Make sure enumeration file has a property like '%s functionId.(...)=%s'",
                    function.getFunctionId(),
                    QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX,
                    function.getFunctionId());
            if (!couldBeNull) {
                throw new QuadrigeTechnicalException(errorMessage);
            }

            LOG.warn(errorMessage);
            return null;
        }

        result.setName(functionControlValue.getLabel());
        return result;
    }

    private String getRulePmfmKey(RulePmfmDTO rulePmfm) {

        return String.format("%s|%s|%s|%s|%s",
                rulePmfm.getPmfm().getParameter().getCode(),
                rulePmfm.getPmfm().getMatrix() != null ? rulePmfm.getPmfm().getMatrix().getId() : "null",
                rulePmfm.getPmfm().getFraction() != null ? rulePmfm.getPmfm().getFraction().getId() : "null",
                rulePmfm.getPmfm().getMethod() != null ? rulePmfm.getPmfm().getMethod().getId() : "null",
                rulePmfm.getPmfm().getUnit() != null ? rulePmfm.getPmfm().getUnit().getId() : "null"
        );
    }

    private void beanToEntity(ControlRuleDTO source, Rule target) {
        // Basic properties
        target.setRuleDc(source.getDescription());
        String controlledAttribute = source.getControlElement().getCode()
                .concat(config.getAttributeSeparator())
                .concat(source.getControlFeature().getCode());
        target.setRuleControledAttribut(controlledAttribute);
        target.setRuleErrorMsg(source.getMessage());
        target.setRuleIsActive(Daos.convertToString(source.isActive()));
        target.setRuleIsBlocking(Daos.convertToString(source.isBlocking()));

        // Function
        Function function;
        if (source.getFunction() != null) {
            function = load(FunctionImpl.class, source.getFunction().getId());
        } else {
            throw new DataRetrievalFailureException("ControlRuleDTO object has no Function");
        }
        if (function == null) {
            throw new DataRetrievalFailureException(String.format("function not found with id=%s", source.getFunction().getId()));
        }
        target.setFunction(function);
    }

    private void updateOrCreateRuleParameter(Rule parent,
                                             Iterator<Integer> ruleParIdSequence,
                                             Map<Integer, RuleParameter> ruleParametersByFunctionParamIds,
                                             int functionParameterId,
                                             String ruleParameterValue) {
        RuleParameter rp = ruleParametersByFunctionParamIds.remove(functionParameterId);
        boolean isNew = false;
        if (rp == null) {
            rp = RuleParameterImpl.Factory.newInstance();
            rp.setFunctionParameter(load(FunctionParameterImpl.class, functionParameterId));
            rp.setRuleParId(ruleParIdSequence.next());
            parent.addRuleParameters(rp);
            rp.setRule(parent);
            isNew = true;
        }

        // Update properties
        rp.setRuleParValue(ruleParameterValue);

        // Save to session
        if (isNew) {
            getSession().save(rp);
        } else {
            getSession().update(rp);
        }

    }

    private ControlFunctionValues getFunction(PreconditionRuleDTO preconditionRule) {
        if (ControlFunctionValues.IS_AMONG.equals(preconditionRule.getBaseRule().getFunction())
                && ControlFunctionValues.IS_AMONG.equals(preconditionRule.getUsedRule().getFunction())) {
            return ControlFunctionValues.PRECONDITION_QUALITATIVE;
        } else if (
                (ControlFunctionValues.IS_AMONG.equals(preconditionRule.getBaseRule().getFunction())
                        && ControlFunctionValues.MIN_MAX.equals(preconditionRule.getUsedRule().getFunction()))
                        ||
                        (ControlFunctionValues.MIN_MAX.equals(preconditionRule.getBaseRule().getFunction())
                                && ControlFunctionValues.IS_AMONG.equals(preconditionRule.getUsedRule().getFunction()))) {
            return ControlFunctionValues.PRECONDITION_NUMERICAL;
        }
        return null;
    }
}
