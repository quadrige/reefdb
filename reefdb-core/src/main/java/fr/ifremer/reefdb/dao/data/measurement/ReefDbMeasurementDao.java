package fr.ifremer.reefdb.dao.data.measurement;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.dao.data.measurement.Measurement;
import fr.ifremer.quadrige3.core.dao.data.measurement.TaxonMeasurement;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>ReefDbMeasurementDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbMeasurementDao {

    /**
     * <p>getMeasurementEntityBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param pmfmId a int.
     * @param createIfNotExists a boolean.
     * @return a {@link fr.ifremer.quadrige3.core.dao.data.measurement.Measurement} object.
     */
    Measurement getMeasurementEntityBySurveyId(int surveyId, int pmfmId, boolean createIfNotExists);
    
    /**
     * <p>removeMeasurementBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param pmfmId a int.
     */
    void removeMeasurementBySurveyId(int surveyId, int pmfmId);
    
    /**
     * <p>getMeasurementsBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param excludePmfmId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<MeasurementDTO> getMeasurementsBySurveyId(int surveyId, Integer... excludePmfmId);

    /**
     * <p>getMeasurementsBySamplingOperationId.</p>
     *
     * @param samplingOperationId a int.
     * @param withTaxonMeasurements a boolean.
     * @param excludePmfmId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<MeasurementDTO> getMeasurementsBySamplingOperationId(int samplingOperationId, boolean withTaxonMeasurements, Integer... excludePmfmId);

    /**
     * <p>saveMeasurementsBySurveyId.</p>
     *
     * @param surveyId a int.
     * @param measurements a {@link java.util.Collection} object.
     * @param excludePmfmId a {@link java.lang.Integer} object.
     */
    void saveMeasurementsBySurveyId(int surveyId, Collection<MeasurementDTO> measurements, Integer... excludePmfmId);

    /**
     * <p>saveMeasurementsBySamplingOperationId.</p>
     *
     * @param samplingOperationId a int.
     * @param measurements a {@link java.util.Collection} object.
     * @param withIndividual a boolean.
     * @param excludePmfmId a {@link java.lang.Integer} object.
     */
    void saveMeasurementsBySamplingOperationId(int samplingOperationId, Collection<MeasurementDTO> measurements, boolean withIndividual, Integer... excludePmfmId);

    /**
     * Remove a list of measurement
     *
     * @param measurementIds a {@link java.util.Collection} object.
     */
    void removeMeasurementsByIds(Collection<Integer> measurementIds);

    /**
     * Remove a list of individual measurements
     *
     * @param taxonMeasurementIds a {@link java.util.Collection} object.
     */
    void removeTaxonMeasurementsByIds(Collection<Integer> taxonMeasurementIds);

    /**
     * <p>removeAllMeasurementsBySurveyId.</p>
     *
     * @param surveyId a int.
     */
    void removeAllMeasurementsBySurveyId(int surveyId);

    /**
     * <p>removeAllMeasurementsBySamplingOperationId.</p>
     *
     * @param samplingOperationId a int.
     */
    void removeAllMeasurementsBySamplingOperationId(int samplingOperationId);

    /**
     * Validate all measurements and taxon measurements attached to the survey and to the inner sampling operations
     *
     * @param surveyIds the survey id
     */
    int validateAllMeasurementsBySurveyIds(Collection<Integer> surveyIds, Date validationDate);

    /**
     * Unvalidate all measurements and taxon measurements attached to the survey and to the inner sampling operations
     *
     * @param surveyIds        the survey id
     * @param unvalidationDate
     * @param validatorId
     */
    int unvalidateAllMeasurementsBySurveyIds(Collection<Integer> surveyIds, Date unvalidationDate, int validatorId);
    int unvalidateAllMeasurementsBySamplingOperationIds(List<Integer> samplingOperationIds, Date unvalidationDate, int validatorId);

    List<Measurement> getQualifiedMeasurementBySurveyId(int surveyId);
    List<TaxonMeasurement> getQualifiedTaxonMeasurementBySurveyId(int surveyId);
    List<Measurement> getQualifiedMeasurementBySamplingOperationId(int samplingOperationId);
    List<TaxonMeasurement> getQualifiedTaxonMeasurementBySamplingOperationId(int samplingOperationId);

    /**
     * <p>updateMeasurementsControlDate.</p>
     *
     * @param controlledElementsPks a {@link java.util.Collection} object.
     * @param controlDate a {@link java.util.Date} object.
     */
    void updateMeasurementsControlDate(Collection<Integer> controlledElementsPks, Date controlDate);

    /**
     * <p>updateTaxonMeasurementsControlDate.</p>
     *
     * @param controlledElementsPks a {@link java.util.Collection} object.
     * @param controlDate a {@link java.util.Date} object.
     */
    void updateTaxonMeasurementsControlDate(Collection<Integer> controlledElementsPks, Date controlDate);

}
