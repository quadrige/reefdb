package fr.ifremer.reefdb.dao.system.rule;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RuleListDao;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;

import java.util.List;

/**
 * <p>ReefDbRuleListDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbRuleListDao extends RuleListDao {

    /**
     * Get all rule lists
     *
     * @return a {@link java.util.List} object.
     */
    List<RuleListDTO> getRuleLists();

    List<RuleListDTO> getRuleListsForProgram(String programCode);

    /**
     * Get a rule list by its code
     *
     * @param ruleListCode a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.RuleListDTO} object.
     */
    RuleListDTO getRuleList(String ruleListCode);

    /**
     * Save rule list
     *
     * @param ruleList a {@link fr.ifremer.reefdb.dto.configuration.control.RuleListDTO} object.
     * @param quserId a {@link java.lang.Integer} object.
     */
    void saveRuleList(RuleListDTO ruleList, Integer quserId);

    boolean ruleListExists(String ruleListCode);
}
