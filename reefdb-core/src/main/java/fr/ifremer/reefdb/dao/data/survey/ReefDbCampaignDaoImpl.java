package fr.ifremer.reefdb.dao.data.survey;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.Campaign;
import fr.ifremer.quadrige3.core.dao.data.survey.CampaignDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.OccasionDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>ReefDbCampaignDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbCampaignDao")
public class ReefDbCampaignDaoImpl extends CampaignDaoImpl implements ReefDbCampaignDao {

    private static final Log log = LogFactory.getLog(ReefDbCampaignDaoImpl.class);

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource
    protected ReefDbConfiguration config;

    /**
     * <p>Constructor for ReefDbCampaignDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbCampaignDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CampaignDTO> getAllCampaigns() {
        Iterator<Object[]> it = queryIterator("allCampaigns");

        List<CampaignDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toCampaignDTO(Arrays.asList(row).iterator()));
        }

        return ImmutableList.copyOf(result); // return an immutable list to avoid concurrency modification against cache
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<CampaignDTO> getCampaignsByIds(List<Integer> campaignIds) {
        List<CampaignDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(campaignIds)) {
            Query query = createQuery("campaignsByIds").setParameterList("campaignIds", campaignIds);
            Iterator<Object[]> it = query.iterate();
            while (it.hasNext()) {
                result.add(toCampaignDTO(Arrays.asList(it.next()).iterator()));
            }
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<CampaignDTO> getCampaignsByCriteria(String name,
                                                    Date startDate1, Date startDate2, boolean strictStartDate,
                                                    Date endDate1, Date endDate2, boolean strictEndDate, boolean canEndDateBeNull) {
        // load query from named query
        StringBuilder queryString = new StringBuilder(getSession().getNamedQuery("campaignsByCriteria").getQueryString());

        // add start date restriction
        StringBuilder startDateString = new StringBuilder();
        if (startDate1 != null && startDate2 != null) {
            if (startDate1 == startDate2) {
                // equals
                startDateString.append("campaignStartDt = ").append(Daos.convertDateOnlyToSQLString(startDate1));
            } else {
                // between
                startDateString.append("campaignStartDt >= ").append(Daos.convertDateOnlyToSQLString(startDate1));
                startDateString.append(" AND campaignStartDt <= ").append(Daos.convertDateOnlyToSQLString(startDate2));
            }
        } else if (startDate1 != null) {
            // >= or >
            startDateString.append("campaignStartDt ").append((strictStartDate ? "> " : ">= ")).append(Daos.convertDateOnlyToSQLString(startDate1));
        } else if (startDate2 != null) {
            // <= or <
            startDateString.append("campaignStartDt ").append((strictStartDate ? "< " : "<= ")).append(Daos.convertDateOnlyToSQLString(startDate2));
        }
        if (startDateString.length() > 0) {
            queryString.append(System.lineSeparator()).append("AND ").append(startDateString);
        }

        // add end date restriction
        StringBuilder endDateString = new StringBuilder();
        if (endDate1 != null && endDate2 != null) {
            if (endDate1 == endDate2) {
                // equals
                endDateString.append("campaignEndDt = ").append(Daos.convertDateOnlyToSQLString(endDate1));
            } else {
                // between
                endDateString.append("campaignEndDt >= ").append(Daos.convertDateOnlyToSQLString(endDate1));
                endDateString.append(" AND campaignEndDt <= ").append(Daos.convertDateOnlyToSQLString(endDate2));
            }
        } else if (endDate1 != null) {
            // >= or >
            endDateString.append("campaignEndDt ").append((strictEndDate ? "> " : ">= ")).append(Daos.convertDateOnlyToSQLString(endDate1));
        } else if (endDate2 != null) {
            // <= or <
            endDateString.append("campaignEndDt ").append((strictEndDate ? "< " : "<= ")).append(Daos.convertDateOnlyToSQLString(endDate2));
        }
        if (endDateString.length() > 0) {
            queryString.append(System.lineSeparator()).append("AND ");
            if (canEndDateBeNull) {
                queryString.append("(campaignEndDt is null OR (").append(endDateString).append("))");
            } else {
                queryString.append(endDateString);
            }
        }

        // crete new query from query string
        Query q = getSession().createQuery(queryString.toString());
        setQueryParams(q, "campaignsByCriteria",
                "name", StringType.INSTANCE, name);

        Iterator<Object[]> it = q.iterate();

        List<CampaignDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toCampaignDTO(Arrays.asList(row).iterator()));
        }

        return result;
    }

    @Override
    public List<CampaignDTO> getCampaignsByName(String name) {
        Iterator<Object[]> it = queryIterator("campaignsByName",
                "name", StringType.INSTANCE, name);

        List<CampaignDTO> result = new ArrayList<>();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toCampaignDTO(Arrays.asList(row).iterator()));
        }
        return result;
    }

    @Override
    public void saveCampaign(CampaignDTO campaign) {
        Assert.notNull(campaign);

        Campaign target = null;
        boolean isNew = false;
        if (campaign.getId() != null) {
            target = get(campaign.getId());
        }
        if (target == null) {
            target = Campaign.Factory.newInstance();
            target.setCampaignId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
            isNew = true;
        }

        beanToEntity(campaign, target);

        if (isNew) {
            getSession().save(target);
            campaign.setId(target.getCampaignId());
        } else {
            getSession().update(target);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<OccasionDTO> getAllOccasions() {

        Iterator<Object[]> it = queryIterator("allOccasions");

        List<OccasionDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toOccasionDTO(Arrays.asList(row).iterator()));
        }

        return result;
    }

    // Private Methods

    private void beanToEntity(CampaignDTO source, Campaign target) {

        target.setCampaignNm(source.getName());

        // Apply DB timezone to campaign's dates (Mantis #61864)
        target.setCampaignStartDt(Dates.convertToDate(source.getStartDate(), config.getDbTimezone()));
        target.setCampaignEndDt(Dates.convertToDate(source.getEndDate(), config.getDbTimezone()));

        target.setCampaignSismerLk(source.getSismerLink());
        target.setCampaignCm(source.getComment());

        target.setQuser(load(QuserImpl.class, source.getManager().getId()));

        // Recorder Department (Mantis #42614 Only if REC_DEP_ID is null)
        if (target.getRecorderDepartment() == null) {
            Assert.notNull(dataContext.getRecorderDepartmentId());
            target.setRecorderDepartment(departmentDao.get(dataContext.getRecorderDepartmentId()));
        }

    }

    private CampaignDTO toCampaignDTO(Iterator<Object> iterator) {
        CampaignDTO result = ReefDbBeanFactory.newCampaignDTO();
        result.setId((Integer) iterator.next());
        result.setName((String) iterator.next());

        // Apply DB timezone to campaign's dates (Mantis #61864)
        result.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(iterator.next()), config.getDbTimezone()));
        result.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(iterator.next()), config.getDbTimezone()));

        result.setSismerLink((String) iterator.next());
        result.setComment((String) iterator.next());
        result.setManager(quserDao.getUserById((Integer) iterator.next()));
        result.setRecorderDepartment(departmentDao.getDepartmentById((Integer) iterator.next()));
        result.setUpdateDate(Daos.convertToDate(iterator.next()));
        return result;
    }

    private OccasionDTO toOccasionDTO(Iterator<Object> iterator) {
        OccasionDTO result = ReefDbBeanFactory.newOccasionDTO();
        result.setId((Integer) iterator.next());
        result.setName((String) iterator.next());
        return result;
    }
}
