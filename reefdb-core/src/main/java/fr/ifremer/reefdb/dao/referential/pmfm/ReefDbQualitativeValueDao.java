package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValueDao;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 22/09/2015.
 */
public interface ReefDbQualitativeValueDao extends QualitativeValueDao {

    String QUALITATIVE_VALUE_BY_ID_CACHE = "qualitative_value_by_id";
    String QUALITATIVE_VALUES_BY_PARAMETER_CODE_CACHE = "qualitative_values_by_parameter_code";
    String QUALITATIVE_VALUES_BY_PMFM_ID_CACHE = "qualitative_values_by_pmfm_id";

    /**
     * <p>getQualitativeValueById.</p>
     *
     * @param qualitativeValueId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO} object.
     */
    @Cacheable(value = QUALITATIVE_VALUE_BY_ID_CACHE)
    QualitativeValueDTO getQualitativeValueById(int qualitativeValueId);

    /**
     * <p>getQualitativeValuesByPmfmId.</p>
     *
     * @param pmfmId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = QUALITATIVE_VALUES_BY_PMFM_ID_CACHE)
    List<QualitativeValueDTO> getQualitativeValuesByPmfmId(Integer pmfmId);

    /**
     * <p>getQualitativeValuesByParameterCode.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = QUALITATIVE_VALUES_BY_PARAMETER_CODE_CACHE)
    List<QualitativeValueDTO> getQualitativeValuesByParameterCode(String parameterCode);

    // no cache eviction here because this method is called by ReefDbParameterDao.saveParameter(...)
    /**
     * <p>saveQualitativeValue.</p>
     *
     * @param parameterCode a {@link java.lang.String} object.
     * @param qualitativeValue a {@link fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO} object.
     */
    void saveQualitativeValue(String parameterCode, QualitativeValueDTO qualitativeValue);
}
