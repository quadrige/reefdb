package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.administration.user.Department;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentDaoImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.referential.Status;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ReefDbDepartmentDaoImpl class.</p>
 *
 */
@Repository("reefDbDepartmentDao")
public class ReefDbDepartmentDaoImpl extends DepartmentDaoImpl implements ReefDbDepartmentDao {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ReefDbDepartmentDaoImpl.class);

    private static final Multimap<String, String> columnNamesByProgramTableNames = ImmutableListMultimap.<String, String>builder()
            .put("DEPARTMENT_PRIVILEGE", "DEP_ID")
            .put("PRIVILEGE_TRANSFER", "PRIV_TRANSFER_FROM_DEP_ID")
            .put("PRIVILEGE_TRANSFER", "PRIV_TRANSFER_TO_DEP_ID")
            .put("APPLIED_STRATEGY", "DEP_ID")
            .put("PMFM_APPLIED_STRATEGY", "DEP_ID")
            .put("PROG_DEP_PROG_PRIV", "DEP_ID").build();

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_LIST_CONTROLED_DEP", "DEP_ID")
            .put("RULE_LIST_RESP_DEP", "DEP_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("CAMPAIGN", "REC_DEP_ID")
            .put("CONTEXT_DEP", "DEP_ID")
            .put("DELETED_ITEM_HISTORY", "REC_DEP_ID")
            .put("EVENT", "REC_DEP_ID")
            .put("FIELD_OBSERVATION", "REC_DEP_ID")
            .put("FILTER", "DEP_ID")
            .put("MEASUREMENT", "REC_DEP_ID")
            .put("MEASUREMENT", "DEP_ID")
            .put("MEASUREMENT_FILE", "DEP_ID")
            .put("MEASUREMENT_FILE", "REC_DEP_ID")
            .put("MEASURED_PROFILE", "REC_DEP_ID")
            .put("OCCASION", "REC_DEP_ID")
            .put("PHOTO", "REC_DEP_ID")
            .put("SAMPLE", "REC_DEP_ID")
            .put("SAMPLING_OPERATION", "REC_DEP_ID")
            .put("SAMPLING_OPERATION", "DEP_ID")
            .put("SURVEY", "REC_DEP_ID")
            .put("TAXON_MEASUREMENT", "REC_DEP_ID")
            .put("TAXON_MEASUREMENT", "DEP_ID").build();

    private static final Multimap<String, String> parentColumnNameByTableNames = ImmutableListMultimap.<String, String>builder()
            .put("DEPARTMENT", "PARENT_DEP_ID").build();

    private static final Multimap<String, String> columnNamesByValidatedDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("MEASUREMENT", "REC_DEP_ID")
            .put("MEASUREMENT", "DEP_ID")
            .put("MEASUREMENT_FILE", "DEP_ID")
            .put("MEASUREMENT_FILE", "REC_DEP_ID")
            .put("PHOTO", "REC_DEP_ID")
            .put("SAMPLING_OPERATION", "REC_DEP_ID")
            .put("SAMPLING_OPERATION", "DEP_ID")
            .put("SURVEY", "REC_DEP_ID")
            .put("TAXON_MEASUREMENT", "REC_DEP_ID")
            .put("TAXON_MEASUREMENT", "DEP_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("MEASUREMENT", "MEAS_VALID_DT")
            .put("MEASUREMENT_FILE", "MEAS_FILE_VALID_DT")
            .put("PHOTO", "PHOTO_VALID_DT")
            .put("SAMPLING_OPERATION", "SAMPLING_OPER_VALID_DT")
            .put("SURVEY", "SURVEY_VALID_DT")
            .put("TAXON_MEASUREMENT", "TAXON_MEAS_VALID_DT").build();

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    /**
     * <p>Constructor for ReefDbDepartmentDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbDepartmentDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<DepartmentDTO> getAllDepartments(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(DEPARTMENT_BY_ID_CACHE);

        Iterator<Object[]> rows = Daos.queryIteratorWithStatus(createQuery("allDepartments"), statusCodes);

        List<DepartmentDTO> result = Lists.newArrayList();
        if (rows != null) {
            while (rows.hasNext()) {
                Object[] row = rows.next();
                DepartmentDTO department = toDepartmentDTO(Arrays.asList(row).iterator(), true);
                result.add(department);

                cacheById.put(department.getId(), department);
            }
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public DepartmentDTO getDepartmentById(int departementId) {
        return getDepartmentById(departementId, false);
    }

    private DepartmentDTO getDepartmentById(int departementId, boolean fetchParent) {

        Object[] row = queryUnique("departmentById",
                "departementId", IntegerType.INSTANCE, departementId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load department with id = " + departementId);
        }

        return toDepartmentDTO(Arrays.asList(row).iterator(), fetchParent);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public List<DepartmentDTO> getDepartmentsByIds(List<Integer> departementIds) {
        Iterator<Object[]> it = createQuery("departmentsByIds")
                .setParameterList("departementIds", departementIds)
                .iterate();

        List<DepartmentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toDepartmentDTO(Arrays.asList(row).iterator(), false));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<DepartmentDTO> findDepartmentsByCodeAndName(String code, String name, boolean strictName, Integer parentId, List<String> statusCodes) {
        Query query = createQuery("departmentsByCodeAndName",
                "departmentCode", StringType.INSTANCE, strictName ? null : code,
                "departmentName", StringType.INSTANCE, strictName ? null : name,
                "strictDepartmentCode", StringType.INSTANCE, strictName ? code : null,
                "strictDepartmentName", StringType.INSTANCE, strictName ? name : null,
                "parentId", IntegerType.INSTANCE, parentId);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<DepartmentDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toDepartmentDTO(Arrays.asList(source).iterator(), true));
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public void saveTemporaryDepartment(DepartmentDTO source) {
        Department target;
        boolean propagateDisable = false;

        if (source.getStatus() == null) {
            source.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(source.getStatus()), "source must have local status");

        Status status = load(StatusImpl.class, source.getStatus().getCode());

        if (source.getId() == null) {
            target = DepartmentImpl.Factory.newInstance(source.getCode(), source.getName(), status);
            Integer targetId = TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), DepartmentImpl.class);
            target.setDepId(targetId);
            target.setDepCreationDt(newCreateDate());
            source.setId(targetId);
            target = create(target);
        } else {
            target = get(source.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
            Status oldStatus = target.getStatus();
            target.setStatus(status);

            if (!Objects.equals(oldStatus, status) && StatusCode.LOCAL_DISABLE.getValue().equals(status.getStatusCd())) {
                // disable sub department and users
                propagateDisable = true;
            }

            target.setDepNm(source.getName());
            target.setDepCd(source.getCode());
        }

        target.setDepDc(source.getDescription());
        target.setDepAddress(source.getAddress());
        target.setDepEMail(source.getEmail());
        target.setDepPhone(source.getPhone());
        target.setDepSiret(source.getSiret());
        target.setDepUrl(source.getUrl());

        // parent department
        DepartmentDTO parentDepartment = source.getParentDepartment();
        Department parentDepEntity = parentDepartment == null ? null : get(parentDepartment.getId());
        target.setParentDepartment(parentDepEntity);

        update(target);

        if (propagateDisable) {

            // propagate to users
            List<PersonDTO> persons = quserDao.findUsersByCriteria(null, null, false, null, source.getId(), null, StatusFilter.LOCAL.toStatusCodes());
            for (PersonDTO person : persons) {
                person.setStatus(Daos.getStatus(StatusCode.LOCAL_DISABLE));
                quserDao.saveTemporaryUser(person);
            }

            // propagate to sub departments
            List<Integer> subDepartmentIds = getSubDepartmentIds(source.getId());
            for (Integer subDepartmentId : subDepartmentIds) {
                if (subDepartmentId < 0) {
                    DepartmentDTO subDepartment = getDepartmentById(subDepartmentId, true);
                    subDepartment.setStatus(Daos.getStatus(StatusCode.LOCAL_DISABLE));
                    saveTemporaryDepartment(subDepartment);
                }
            }
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteTemporaryDepartments(List<Integer> departmentIds) {
        if (departmentIds == null) return;
        for (Integer depId : departmentIds.stream().filter(Objects::nonNull).collect(Collectors.toSet())) {
            Assert.isTrue(depId < 0, String.format("Can't delete a department with a positive id %s", depId));

            deleteTemporaryDepartment(depId);
        }

        getSession().flush();
        getSession().clear();
    }

    private void deleteTemporaryDepartment(int departmentId) {

        // delete sub department first
        List<Integer> subDepartmentIds = getSubDepartmentIds(departmentId);
        for (Integer subDepartmentId : subDepartmentIds) {
            if (subDepartmentId < 0) {
                deleteTemporaryDepartment(subDepartmentId);
            }
        }

        // delete users
        List<PersonDTO> persons = quserDao.findUsersByCriteria(null, null, false, null, departmentId, null, StatusFilter.LOCAL.toStatusCodes());
        for (PersonDTO person : persons) {
            quserDao.deleteTemporaryUser(person.getId());
        }

        // delete department
        remove(departmentId);
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryDepartmentFks(Integer sourceDepId, Integer targetDepId, boolean delete) {

        Assert.notNull(sourceDepId);
        Assert.notNull(targetDepId);

        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceDepId, targetDepId);

        // check sub departments for parent link
        boolean canReplaceParentLink = true;
        List<Integer> subDepartmentIds = getSubDepartmentIds(sourceDepId);
        for (Integer subDepartmentId : subDepartmentIds) {
            // update parent link only if child not used in program or rules
            if (isDepartmentUsedInProgram(subDepartmentId) || isDepartmentUsedInRules(subDepartmentId)) {
                canReplaceParentLink = false;
                break;
            }
        }
        if (canReplaceParentLink) {
            executeMultipleUpdate(parentColumnNameByTableNames, sourceDepId, targetDepId);
        }

        if (delete) {
            // delete temporary department
            deleteTemporaryDepartment(sourceDepId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDepartmentUsedInData(int departmentId) {

        if (executeMultipleCount(columnNamesByDataTableNames, departmentId)
                || executeMultipleCount(parentColumnNameByTableNames, departmentId)) {
            return true;
        }

        // check users in department
        List<PersonDTO> persons = quserDao.findUsersByCriteria(null, null, false, null, departmentId, null, StatusFilter.LOCAL.toStatusCodes());
        for (PersonDTO person : persons) {
            if (quserDao.isUserUsedInData(person.getId())) {
                return true;
            }
        }

        // check sub departments
        List<Integer> subDepartmentIds = getSubDepartmentIds(departmentId);
        for (Integer subDepartmentId : subDepartmentIds) {
            if (isDepartmentUsedInData(subDepartmentId)) {
                return true;
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDepartmentUsedInValidatedData(int departmentId) {

        if (executeMultipleCountWithNotNullCondition(columnNamesByValidatedDataTableNames, validationDateColumnNameByDataTableNames, departmentId)) {
            return true;
        }

        // check users in department
        List<PersonDTO> persons = quserDao.findUsersByCriteria(null, null, false, null, departmentId, null, StatusFilter.LOCAL.toStatusCodes());
        for (PersonDTO person : persons) {
            if (quserDao.isUserUsedInValidatedData(person.getId())) {
                return true;
            }
        }

        // check sub departments
        List<Integer> subDepartmentIds = getSubDepartmentIds(departmentId);
        for (Integer subDepartmentId : subDepartmentIds) {
            if (isDepartmentUsedInValidatedData(subDepartmentId)) {
                return true;
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDepartmentUsedInProgram(int departmentId) {

        if (executeMultipleCount(columnNamesByProgramTableNames, departmentId)) {
            return true;
        }

        // check users in department
        List<PersonDTO> persons = quserDao.findUsersByCriteria(null, null, false, null, departmentId, null, StatusFilter.LOCAL.toStatusCodes());
        for (PersonDTO person : persons) {
            if (quserDao.isUserUsedInProgram(person.getId())) {
                return true;
            }
        }

        // check sub departments
        List<Integer> subDepartmentIds = getSubDepartmentIds(departmentId);
        for (Integer subDepartmentId : subDepartmentIds) {
            if (isDepartmentUsedInProgram(subDepartmentId)) {
                return true;
            }
        }

        return false;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isDepartmentUsedInRules(int departmentId) {

        if (executeMultipleCount(columnNamesByRulesTableNames, departmentId)) {
            return true;
        }

        // check users in department
        List<PersonDTO> persons = quserDao.findUsersByCriteria(null, null, false, null, departmentId, null, StatusFilter.LOCAL.toStatusCodes());
        for (PersonDTO person : persons) {
            if (quserDao.isUserUsedInRules(person.getId())) {
                return true;
            }
        }

        // check sub departments
        List<Integer> subDepartmentIds = getSubDepartmentIds(departmentId);
        for (Integer subDepartmentId : subDepartmentIds) {
            if (isDepartmentUsedInRules(subDepartmentId)) {
                return true;
            }
        }

        return false;
    }

    private List<Integer> getSubDepartmentIds(int departmentId) {
        List<Integer> result = queryListTyped("departmentIdsByParentDepartmentId", "parentDepartementId", IntegerType.INSTANCE, departmentId);
        if (result == null) {
            return Lists.newArrayList();
        }
        return result;
    }

    // INTERNAL METHODS
    private DepartmentDTO toDepartmentDTO(Iterator<Object> source, boolean fetchParent) {
        DepartmentDTO result = ReefDbBeanFactory.newDepartmentDTO();
        result.setId((Integer) source.next());
        result.setCode((String) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setEmail((String) source.next());
        result.setAddress((String) source.next());
        result.setPhone((String) source.next());
        result.setSiret((String) source.next());
        result.setUrl((String) source.next());
        Integer parentId = (Integer) source.next();
        if (parentId != null && fetchParent) {
            result.setParentDepartment(getDepartmentById(parentId));
        }
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
