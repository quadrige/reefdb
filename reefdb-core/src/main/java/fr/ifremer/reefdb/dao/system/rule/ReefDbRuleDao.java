package fr.ifremer.reefdb.dao.system.rule;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import fr.ifremer.quadrige3.core.dao.system.rule.RuleDao;
import fr.ifremer.reefdb.dto.FunctionDTO;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import org.apache.commons.lang3.mutable.MutableBoolean;

import java.util.Date;
import java.util.List;

/**
 * @author peck7 on 03/07/2018.
 */
public interface ReefDbRuleDao extends RuleDao {

    /**
     * <p>getControlRulesByRuleListCode.</p>
     *
     * @param ruleListCode a {@link String} object.
     * @param onlyActive a boolean.
     * @param incompatibleRule a mutable boolean
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> getControlRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule);

    /**
     * <p>getPreconditionedRulesByRuleListCode.</p>
     *
     * @param ruleListCode a {@link java.lang.String} object.
     * @param onlyActive   a boolean.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> getPreconditionedRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule);

    /**
     * Get grouped control rules
     *
     * @param ruleListCode
     * @param onlyActive
     * @param incompatibleRule
     * @return
     */
    List<ControlRuleDTO> getGroupedRulesByRuleListCode(String ruleListCode, boolean onlyActive, MutableBoolean incompatibleRule);

    /**
     * <p>findActiveControlRules from 3 criteria: date, program code and department id</p>
     * returns only active rules used for control (without preconditioned or grouped rules)
     *
     * @param date a {@link java.util.Date} object.
     * @param programCode a {@link java.lang.String} object.
     * @param departmentId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> findActiveControlRules(Date date, String programCode, Integer departmentId);

    /**
     * <p>findPreconditionedRules.</p>
     *
     * @param date         a {@link java.util.Date} object.
     * @param programCode  a {@link java.lang.String} object.
     * @param departmentId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> findActivePreconditionedRules(Date date, String programCode, Integer departmentId);

    List<ControlRuleDTO> findActiveGroupedRules(Date date, String programCode, Integer departmentId);

    /**
     * <p>findPreconditionedRules.</p>
     *
     * @param programCodes  a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<ControlRuleDTO> findActivePreconditionedRules(List<String> programCodes);

    /**
     * Save rule
     *
     * @param rule a {@link ControlRuleDTO} object.
     */
    void save(ControlRuleDTO rule, String ruleListCd);

    /**
     * <p>getAllFunction.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<FunctionDTO> getAllFunction();

    boolean ruleExists(String ruleCode);

}
