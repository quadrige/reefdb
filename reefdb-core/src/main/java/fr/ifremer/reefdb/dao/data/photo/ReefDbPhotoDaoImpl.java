package fr.ifremer.reefdb.dao.data.photo;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.data.photo.Photo;
import fr.ifremer.quadrige3.core.dao.data.photo.PhotoDaoImpl;
import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperationImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.Survey;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyImpl;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.system.QualificationHistory;
import fr.ifremer.quadrige3.core.dao.system.QualificationHistoryDao;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Images;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import fr.ifremer.reefdb.service.ReefDbTechnicalException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbPhotoDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbPhotoDao")
public class ReefDbPhotoDaoImpl extends PhotoDaoImpl implements ReefDbPhotoDao {

    private final static Log LOG = LogFactory.getLog(ReefDbPhotoDaoImpl.class);

    @Autowired
    protected ReefDbConfiguration config;

    @Resource(name = "reefDbReferentialDao")
    protected ReefDbReferentialDao referentialDao;

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource(name = "objectTypeDao")
    protected ObjectTypeDao objectTypeDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "qualificationHistoryDao")
    protected QualificationHistoryDao qualificationHistoryDao;

    /**
     * <p>Constructor for ReefDbPhotoDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbPhotoDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<PhotoDTO> getPhotosBySurveyId(int surveyId) {

        Iterator<Object[]> it = queryIterator("photosBySurveyId",
                "surveyObjectTypeCode", StringType.INSTANCE, Daos.SURVEY_OBJECT_TYPE,
                "samplingOperationObjectTypeCode", StringType.INSTANCE, Daos.SAMPLING_OPERATION_OBJECT_TYPE,
                "surveyId", IntegerType.INSTANCE, surveyId);

        List<PhotoDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toPhotoDTO(Arrays.asList(row).iterator()));
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public void savePhotosBySurveyId(int surveyId, Collection<PhotoDTO> photos) {

        Survey survey = get(SurveyImpl.class, surveyId);

        List<Integer> existingPhotosIds = ReefDbBeans.collectIds(getPhotosBySurveyId(surveyId));

        if (photos != null) {
        for (PhotoDTO source : photos) {

            // remove from existing
            if (source.getId() != null) {
                existingPhotosIds.remove(source.getId());
            }

            // save if dirty
            if (source.isDirty()) {
                savePhotoInSurvey(source, survey);
                source.setDirty(false);
            }

        }
        }

        // remove the not saved
        if (CollectionUtils.isNotEmpty(existingPhotosIds)) {
            for (Integer idToRemove : existingPhotosIds) {
                remove(idToRemove);
            }
        }
    }

    private void savePhotoInSurvey(PhotoDTO photo, Survey survey) {

        Photo target;
        if (photo.getId() == null) {
            target = Photo.Factory.newInstance();
        } else {
            target = get(photo.getId());
        }

        // mandatory fields
        target.setSurvey(survey);
        if (photo.getSamplingOperation() != null) {
            // link the photo to the sampling operation
            target.setObjectType(load(ObjectTypeImpl.class, Daos.SAMPLING_OPERATION_OBJECT_TYPE));
            target.setObjectId(photo.getSamplingOperation().getId());
            target.setSamplingOperation(load(SamplingOperationImpl.class, photo.getSamplingOperation().getId()));
        } else {
            // if no sampling operation provided, link the photo to the survey
            target.setObjectType(load(ObjectTypeImpl.class, Daos.SURVEY_OBJECT_TYPE));
            target.setObjectId(survey.getSurveyId());
        }

        // Recorder Department (Mantis #42615 Only if REC_DEP_ID is null)
        if (target.getRecorderDepartment() == null) {
            target.setRecorderDepartment(load(DepartmentImpl.class, dataContext.getRecorderDepartmentId()));
        }

        // optional fields
        target.setPhotoNm(photo.getName());
        if (photo.getPhotoType() != null) {
            target.setPhotoType(load(PhotoTypeImpl.class, photo.getPhotoType().getCode())); // use this String as photo type waiting for PhotoTypeDTO
        } else {
            target.setPhotoType(null);
        }
        target.setPhotoCm(photo.getCaption());
        target.setPhotoDt(photo.getDate());
        target.setPhotoDirDc(photo.getDirection());
        target.setQualityFlag(getDefaultQualityFlag());

        // save now to obtain new id
        getSession().save(target);
        photo.setId(target.getPhotoId());

        // compute target file name
        String targetPath = Daos.computePhotoFilePath(photo, target);

        if (!targetPath.equals(photo.getPath())) {

            // save case :
            // 1 - the photo is just imported : path == null and fullPath points to tempDir
            // 2 - the photo has been associated to another parent : path == old path where the file should be

            File sourceFile = photo.getPath() == null
                    ? new File(photo.getFullPath())
                    : new File(config.getDbPhotoDirectory(), photo.getPath());

            Assert.isTrue(sourceFile.exists(), "photo not found : " + sourceFile);
            File targetFile = new File(config.getDbPhotoDirectory(), targetPath);

            try {
                // move main image
                    FileUtils.moveFile(sourceFile, targetFile);

                // try find other images
                File mediumSourceFile = Images.getMediumImageFile(sourceFile);
                if (mediumSourceFile.exists()) {
                    File mediumTargetFile = Images.getMediumImageFile(targetFile);
                    FileUtils.moveFile(mediumSourceFile, mediumTargetFile);
                }
                File smallSourceFile = Images.getSmallImageFile(sourceFile);
                if (smallSourceFile.exists()) {
                    File smallTargetFile = Images.getSmallImageFile(targetFile);
                    FileUtils.moveFile(smallSourceFile, smallTargetFile);
                }

            } catch (IOException ex) {
                throw new ReefDbTechnicalException(ex);
            }

            // update bean
            photo.setPath(targetPath);
            photo.setFullPath(targetFile.getAbsolutePath());
        }

        // update photo link if differs
        if (!targetPath.equals(target.getPhotoLk())) {

            target.setPhotoLk(targetPath);
            update(target);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void remove(Integer photoId) {
        // delete file
        String fileName = get(photoId).getPhotoLk();
        if (StringUtils.isNotBlank(fileName)) {
            Images.deleteImage(new File(config.getDbPhotoDirectory(), fileName));
        }

        // then remove entity
        super.remove(photoId);
    }

    /** {@inheritDoc} */
    @Override
    public void removeBySurveyId(int surveyId) {
        Iterator<Integer> it = queryIteratorTyped("photoIdsBySurveyId",
                "surveyObjectTypeCode", StringType.INSTANCE, Daos.SURVEY_OBJECT_TYPE,
                "samplingOperationObjectTypeCode", StringType.INSTANCE, Daos.SAMPLING_OPERATION_OBJECT_TYPE,
                "surveyId", IntegerType.INSTANCE, surveyId);

        while (it.hasNext()) {
            remove(it.next());
        }
    }

    /** {@inheritDoc} */
    @Override
    public void removeBySamplingOperationId(int samplingOperationId) {
        Iterator<Integer> it = queryIteratorTyped("photoIdsBySamplingOperationId",
                "samplingOperationObjectTypeCode", StringType.INSTANCE, Daos.SAMPLING_OPERATION_OBJECT_TYPE,
                "samplingOperationId", IntegerType.INSTANCE, samplingOperationId);

        while (it.hasNext()) {
            remove(it.next());
        }
    }

    @Override
    public int validateBySurveyIds(Collection<Integer> surveyIds, Date validationDate) {
        return createQuery("validatePhotosBySurveyIds",
                "surveyObjectTypeCode", StringType.INSTANCE, Daos.SURVEY_OBJECT_TYPE,
                "samplingOperationObjectTypeCode", StringType.INSTANCE, Daos.SAMPLING_OPERATION_OBJECT_TYPE,
                "validationDate", DateType.INSTANCE, validationDate)
                .setParameterList("surveyIds", surveyIds)
                .executeUpdate();

    }

    @Override
    public int unvalidateBySurveyIds(Collection<Integer> surveyIds, Date unvalidationDate, int validatorId) {

        // Get qualified photos for historisation
        surveyIds.forEach(surveyId -> {
            List<Photo> qualifiedPhotos = getQualifiedPhotoBySurveyId(surveyId);
            if (CollectionUtils.isNotEmpty(qualifiedPhotos)) {
                qualifiedPhotos.forEach(photo -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(photo.getPhotoId()),
                        objectTypeDao.get(ObjectTypeCode.PHOTO.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("reefdb.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(photo.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
        });

        return createQuery("unvalidatePhotosBySurveyIds",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
                .setParameterList("surveyIds", surveyIds).executeUpdate();
    }

    @Override
    public int unvalidateBySamplingOperationIds(List<Integer> samplingOperationIds, Date unvalidationDate, int validatorId) {
        // Get qualified photos for historisation
        samplingOperationIds.forEach(samplingOperationId -> {
            List<Photo> qualifiedPhotos = getQualifiedPhotoBySamplingOperationId(samplingOperationId);
            if (CollectionUtils.isNotEmpty(qualifiedPhotos)) {
                qualifiedPhotos.forEach(photo -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(photo.getPhotoId()),
                        objectTypeDao.get(ObjectTypeCode.PHOTO.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("reefdb.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(photo.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
        });

        return createQuery("unvalidatePhotosBySamplingOperationIds", "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
            .setParameterList("samplingOperIds", samplingOperationIds).executeUpdate();
    }

    @Override
    public List<Photo> getQualifiedPhotoBySurveyId(int surveyId) {
        return queryListTyped("qualifiedSurveyPhotos",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue(),
            "surveyId", IntegerType.INSTANCE, surveyId
        );
    }

    @Override
    public List<Photo> getQualifiedPhotoBySamplingOperationId(int samplingOperationId) {
        return queryListTyped("qualifiedSamplingOperationPhotos",
            "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue(),
            "samplingOperId", IntegerType.INSTANCE, samplingOperationId
        );
    }

    /**
     * return the default quality flag
     */
    private QualityFlag getDefaultQualityFlag() {
        return load(QualityFlagImpl.class, QualityFlagCode.NOT_QUALIFIED.getValue()); // = non qualifié
    }

    private PhotoDTO toPhotoDTO(Iterator<Object> it) {
        PhotoDTO result = ReefDbBeanFactory.newPhotoDTO();
        result.setId((Integer) it.next());
        String objectTypeCode = (String) it.next();
        Integer objectId = (Integer) it.next();
        if (Daos.SAMPLING_OPERATION_OBJECT_TYPE.equals(objectTypeCode)) {
            // set a dummy sampling operation
            // TODO full object should be set in service
            SamplingOperationDTO dummySamplingOperation = ReefDbBeanFactory.newSamplingOperationDTO();
            dummySamplingOperation.setId(objectId);
            result.setSamplingOperation(dummySamplingOperation);
        }
        result.setName((String) it.next());
        result.setCaption((String) it.next());
        result.setDate(Daos.convertToDate(it.next()));
        result.setDirection((String) it.next());
        result.setPath((String) it.next());
        File fullPath = new File(config.getDbPhotoDirectory(), result.getPath());
        result.setFullPath(fullPath.getPath());

        String photoTypeCode = (String) it.next();
        if (StringUtils.isNotBlank(photoTypeCode)) {
            result.setPhotoType(referentialDao.getPhotoTypeByCode(photoTypeCode));
        }

        // remote id for download purpose
        result.setRemoteId((Integer) it.next());

        return result;
    }

}
