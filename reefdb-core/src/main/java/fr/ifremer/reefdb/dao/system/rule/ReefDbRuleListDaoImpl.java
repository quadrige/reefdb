package fr.ifremer.reefdb.dao.system.rule;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.dao.administration.program.ProgramImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.Quser;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleList;
import fr.ifremer.quadrige3.core.dao.system.rule.RuleListDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Beans;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleGroupDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.service.system.SystemService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbRuleListDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbRuleListDao")
public class ReefDbRuleListDaoImpl extends RuleListDaoImpl implements ReefDbRuleListDao {

    private static final Log LOG = LogFactory.getLog(ReefDbRuleListDaoImpl.class);

    @Resource
    protected ReefDbConfiguration config;
    @Resource(name = "reefDbRuleDao")
    protected ReefDbRuleDao ruleDao;
    @Resource(name = "reefdbProgramStrategyService")
    protected ProgramStrategyService programStrategyService;
    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefdbSystemService")
    protected SystemService systemService;
    /**
     * <p>Constructor for ReefDbRuleListDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbRuleListDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RuleListDTO> getRuleLists() {

        Iterator<Object[]> it = queryIterator("allRuleList");

        return toRuleListDTOs(it);
    }

    @Override
    public List<RuleListDTO> getRuleListsForProgram(String programCode) {

        Iterator<Object[]> it = queryIterator("allRuleListWithProgramCode",
                "programCode", StringType.INSTANCE, programCode);

        return toRuleListDTOs(it);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleListDTO getRuleList(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        Object[] source = queryUnique("ruleListByCode", "ruleListCode", StringType.INSTANCE, ruleListCode);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load rule list with code = " + ruleListCode);
        }

        RuleListDTO ruleList = toRuleListDTO(Arrays.asList(source).iterator(), config.getDbTimezone());

        if (fillAndValidRuleList(ruleList,
                programStrategyService.getManagedProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId()),
                programStrategyService.getWritableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId())))
            // return the valid rule list
            return ruleList;

        return null;
    }

    @Override
    public boolean ruleListExists(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        return queryUnique("ruleListByCode", "ruleListCode", StringType.INSTANCE, ruleListCode) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveRuleList(RuleListDTO source, Integer quserId) {
        Assert.notNull(source);
        Assert.notBlank(source.getCode());
        Assert.notNull(source.getStatus());
        Assert.notNull(quserId);

        RuleList target = get(source.getCode());
        boolean isNew = false;
        if (target == null) {
            target = RuleList.Factory.newInstance();
            target.setRuleListCd(source.getCode());
            // Set status only if new entity
            target.setStatus(load(StatusImpl.class, source.getStatus().getCode()));
            isNew = true;
        }

        // DTO -> Entity
        beanToEntity(source, target, quserId, config.getDbTimezone());

        // Save it
        if (isNew) {
            getSession().save(target);
        } else {
            getSession().update(target);
        }

        // Save rules
        final List<String> rulesCdsToRemove = ReefDbBeans.collectProperties(target.getRules(), "ruleCd");

        // Save rules
        if (CollectionUtils.isNotEmpty(source.getControlRules())) {
            source.getControlRules().forEach(controlRule -> {
                ruleDao.save(controlRule, source.getCode());
                rulesCdsToRemove.remove(controlRule.getCode());
                if (!controlRule.isPreconditionsEmpty()) {
                    for (PreconditionRuleDTO preconditionRule : controlRule.getPreconditions()) {
                        rulesCdsToRemove.remove(preconditionRule.getBaseRule().getCode());
                        rulesCdsToRemove.remove(preconditionRule.getUsedRule().getCode());
                    }
                }
                if (!controlRule.isGroupsEmpty()) {
                    for (RuleGroupDTO groupedRule : controlRule.getGroups()) {
                        rulesCdsToRemove.remove(groupedRule.getRule().getCode());
                    }
                }
            });
        }

        getSession().flush();
        getSession().clear();

        // remove unused rules
        if (CollectionUtils.isNotEmpty(rulesCdsToRemove)) {
            ruleDao.removeByCds(Beans.asStringArray(rulesCdsToRemove));
            // flush again because another get or load can read the removes rules
            getSession().flush();
            getSession().clear();
        }
    }

    // INTERNAL METHODS

    private boolean fillAndValidRuleList(RuleListDTO ruleList, Set<String> managedProgramCodes, Set<String> writableProgramCodes) {

        // get programs
        List<String> programCodes = getProgramCodesByRuleListCode(ruleList.getCode());

        if (CollectionUtils.isEmpty(programCodes)) {
            // skip this rule list
            return false;
        }

        // Check program write privilege
        boolean canRead = programCodes.stream().anyMatch(writableProgramCodes::contains) || ReefDbBeans.isLocalStatus(ruleList.getStatus());
        if (!canRead) {
            if (LOG.isWarnEnabled()) {
                LOG.warn(t("reefdb.error.dao.ruleList.program.empty", ruleList.getCode()));
            }
            // skip this rule list
            return false;
        }

        // the current user must be manager of all programs to set this rule list as writable
        boolean canWrite = SecurityContextHelper.hasAuthority(QuadrigeUserAuthority.ADMIN) || managedProgramCodes.containsAll(programCodes) || ReefDbBeans.isLocalStatus(ruleList.getStatus());
        ruleList.setReadOnly(!canWrite);

        // add programs
        ruleList.setPrograms(programStrategyService.getProgramsByCodes(programCodes));

        // add services
        ruleList.setDepartments(getControlledDepartmentsByRuleListCode(ruleList.getCode()));

        MutableBoolean incompatibleRule = new MutableBoolean(false);

        // add rules
        ruleList.addAllControlRules(ruleDao.getControlRulesByRuleListCode(ruleList.getCode(), false /*active and non-active*/, incompatibleRule));
        // add preconditioned rules
        ruleList.addAllControlRules(ruleDao.getPreconditionedRulesByRuleListCode(ruleList.getCode(), false, incompatibleRule));
        // add grouped rules
        ruleList.addAllControlRules(ruleDao.getGroupedRulesByRuleListCode(ruleList.getCode(), false, incompatibleRule));

        // valid rules count (if loadRules is false, just count the rules in db)
        if (ruleList.isControlRulesEmpty()) {
            if (LOG.isWarnEnabled()) {
                LOG.warn(t("reefdb.error.dao.ruleList.rule.empty", ruleList.getCode()));
            }
            // skip this rule list
            return false;
        }
        // set the rule list as read only if at least one rule is not compatible
        if (incompatibleRule.booleanValue()) ruleList.setReadOnly(true);

        return true;
    }

    private List<String> getProgramCodesByRuleListCode(String ruleListCode) {

        Assert.notBlank(ruleListCode);

        return queryListTyped("programCodesByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode);
    }

    /**
     * <p>getControlledDepartmentsByRuleListCode.</p>
     *
     * @param ruleListCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    private List<DepartmentDTO> getControlledDepartmentsByRuleListCode(String ruleListCode) {
        Assert.notBlank(ruleListCode);

        List<Integer> depIds = queryListTyped("departmentIdsByRuleListCode",
                "ruleListCode", StringType.INSTANCE, ruleListCode);

        List<DepartmentDTO> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(depIds)) {
            for (Integer depId : new HashSet<>(depIds)) {
                result.add(departmentDao.getDepartmentById(depId));
            }
        }
        return result;
    }

    private List<RuleListDTO> toRuleListDTOs(Iterator<Object[]> it) {

        Set<String> managedProgramCodes = programStrategyService.getManagedProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());
        Set<String> writableProgramCodes = programStrategyService.getWritableProgramCodesByQuserId(SecurityContextHelper.getQuadrigeUserId());

        List<RuleListDTO> result = Lists.newArrayList();
        TimeZone dbTimezone = config.getDbTimezone();

        while (it.hasNext()) {
            Object[] source = it.next();
            RuleListDTO ruleList = toRuleListDTO(Arrays.asList(source).iterator(), dbTimezone);

            if (fillAndValidRuleList(ruleList, managedProgramCodes, writableProgramCodes))
                // add the valid rule list to result
                result.add(ruleList);
        }

        return result;
    }

    private RuleListDTO toRuleListDTO(Iterator<Object> source, TimeZone dbTimezone) {
        RuleListDTO result = ReefDbBeanFactory.newRuleListDTO();
        result.setCode((String) source.next());
        result.setActive(Daos.safeConvertToBoolean(source.next()));

        LocalDate startDate = Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone);
        result.setStartMonth(
            Optional.ofNullable(startDate)
                .map(LocalDate::getMonthValue)
                .flatMap(month -> systemService.getMonths().stream().filter(monthDTO -> monthDTO.getId().equals(month)).findFirst())
                .orElse(null)
        );

        LocalDate endDate = Dates.convertToLocalDate(Daos.convertToDate(source.next()), dbTimezone);
        result.setEndMonth(
            Optional.ofNullable(endDate)
                .map(LocalDate::getMonthValue)
                .flatMap(month -> systemService.getMonths().stream().filter(monthDTO -> monthDTO.getId().equals(month)).findFirst())
                .orElse(null)
        );

        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        return result;
    }

    private void beanToEntity(RuleListDTO source, RuleList target, int quserId, TimeZone dbTimezone) {

        target.setRuleListDc(source.getDescription());
        target.setRuleListIsActive(Daos.convertToString(source.isActive()));

        // convert month to date
        LocalDate startDate = Optional.ofNullable(source.getStartMonth())
            .map(monthDTO -> LocalDate.of(LocalDate.now().getYear(), monthDTO.getId(), 1))
            .orElse(null);
        target.setRuleListFirstMonth(Dates.convertToDate(startDate, dbTimezone));

        LocalDate endDate = Optional.ofNullable(source.getEndMonth())
            .map(monthDTO -> LocalDate.of(LocalDate.now().getYear(), monthDTO.getId(), 1).plusMonths(1).minusDays(1))
            .orElse(null);
        target.setRuleListLastMonth(Dates.convertToDate(endDate, dbTimezone));

        // update date (if remote = always null, as set by server)
        if (ReefDbBeans.isLocalStatus(source.getStatus())) {
            target.setUpdateDt(newUpdateTimestamp());
        }

        // creation date
        if (target.getRuleListCreationDt() == null) {
            target.setRuleListCreationDt(newCreateDate());
        }

        // manager user
        if (CollectionUtils.isEmpty(target.getQusers())) {
            // add current user as unique
            Quser quser = load(QuserImpl.class, quserId);
            target.setQusers(Sets.newHashSet(quser));
        } else if (ReefDbBeans.findByProperty(target.getQusers(), "quserId", quserId) == null) {
            // add current user to collection
            Quser quser = load(QuserImpl.class, quserId);
            target.getQusers().add(quser);
        }

        // manager department
        if (CollectionUtils.isEmpty(target.getRespDepartments())) {
            // add current user department as unique
            Quser quser = load(QuserImpl.class, quserId);
            target.setRespDepartments(Sets.newHashSet(quser.getDepartment()));
        } else {
            // add current user's department to collection
            Quser quser = load(QuserImpl.class, quserId);
            if (ReefDbBeans.findByProperty(target.getRespDepartments(), "depId", quser.getDepartment().getDepId()) == null) {
                target.getRespDepartments().add(quser.getDepartment());
            }
        }

        // programs
        if (source.getPrograms() == null) {
            target.getPrograms().clear();
        } else {
            Daos.replaceEntities(target.getPrograms(),
                    source.getPrograms(),
                    vo -> load(ProgramImpl.class, Objects.requireNonNull(vo).getCode()));
        }

        // do not remove remaining unused programs: links to programs will be automatically deleted when ruleList entity will be saved

        // departments
        if (source.getDepartments() == null) {
            target.getControledDepartments().clear();
        } else {
            Daos.replaceEntities(target.getControledDepartments(),
                    source.getDepartments(),
                    vo -> load(DepartmentImpl.class, Objects.requireNonNull(vo).getId()));
        }

    }

}
