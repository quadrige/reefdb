package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Fraction;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.FractionDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Matrix;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.MatrixImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * Fraction DAO
 * Created by Ludovic on 29/07/2015.
 */
@Repository("reefDbFractionDao")
public class ReefDbFractionDaoImpl extends FractionDaoImpl implements ReefDbFractionDao {

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_PMFM", "FRACTION_ID").build();

    private static final Multimap<String, String> columnNamesByReferentialTableNames = ImmutableListMultimap.<String, String>builder()
            .put("FRACTION_MATRIX", "FRACTION_ID")
            .put("PMFM", "FRACTION_ID").build();

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbMatrixDao")
    protected ReefDbMatrixDao matrixDao;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbFractionDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<FractionDTO> getAllFractions(List<String> statusCodes) {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allFractions"), statusCodes);

        List<FractionDTO> result = Lists.newArrayList();
        FractionDTO fraction = null;
        while (it.hasNext()) {
            Object[] row = it.next();
            Iterator<Object> rowIt = Arrays.asList(row).iterator();

            if (fraction == null || !fraction.getId().equals(row[0])) {
                fraction = toFractionDTO(rowIt);
                result.add(fraction);
            } else {
                // dummy iteration
                toFractionDTO(rowIt);
            }

            // the last element is the associated matrix
            Integer matrixId = (Integer) rowIt.next();
            if (matrixId != null) {
                MatrixDTO matrix = matrixDao.getMatrixById(matrixId);
                fraction.addMatrixes(matrix);
                matrix.addFractions(fraction);
            }
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public FractionDTO getFractionById(int fractionId) {

        Iterator<Object[]> it = queryIterator("fractionById", "fractionId", IntegerType.INSTANCE, fractionId);

        if (!it.hasNext()) {
            throw new DataRetrievalFailureException("can't load fraction with id = " + fractionId);
        }

        FractionDTO fraction = null;
        while (it.hasNext()) {
            Object[] row = it.next();
            Iterator<Object> rowIt = Arrays.asList(row).iterator();
            if (fraction == null) {
                fraction = toFractionDTO(rowIt);
            } else {
                toFractionDTO(rowIt);
            }
            // the last element is the associated matrix
            Integer matrixId = (Integer) rowIt.next();
            if (matrixId != null) {
                MatrixDTO matrix = matrixDao.getMatrixById(matrixId);
                fraction.addMatrixes(matrix);
                matrix.addFractions(fraction);
            }
        }
        return fraction;
    }

    /** {@inheritDoc} */
    @Override
    public List<FractionDTO> getFractionsByMatrixId(Integer matrixId) {
        Iterator<Object[]> it = queryIterator("fractionsByMatrixId", "matrixId", IntegerType.INSTANCE, matrixId);

        List<FractionDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toFractionDTO(Arrays.asList(row).iterator()));
        }

        return ImmutableList.copyOf(result);

    }

    /** {@inheritDoc} */
    @Override
    public List<FractionDTO> findFractions(Integer fractionId, List<String> statusCodes) {

        Query query = createQuery("fractionByCriteria", "fractionId", IntegerType.INSTANCE, fractionId);
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<FractionDTO> result = Lists.newArrayList();
        FractionDTO fraction = null;
        while (it.hasNext()) {
            Object[] row = it.next();
            Iterator<Object> rowIt = Arrays.asList(row).iterator();

            if (fraction == null || !fraction.getId().equals(row[0])) {
                fraction = toFractionDTO(rowIt);
                result.add(fraction);
            } else {
                // dummy iteration
                toFractionDTO(rowIt);
            }

            // the last element is the associated matrix
            Integer matrixId = (Integer) rowIt.next();
            if (matrixId != null) {
                MatrixDTO matrix = matrixDao.getMatrixById(matrixId);
                fraction.addMatrixes(matrix);
                matrix.addFractions(fraction);
            }
        }

        return ImmutableList.copyOf(result);

    }

    /** {@inheritDoc} */
    @Override
    public void saveFractions(List<? extends FractionDTO> fractions) {
        if (CollectionUtils.isEmpty(fractions)) {
            return;
        }

        for (FractionDTO fraction : fractions) {
            if (fraction.isDirty()) {
                saveFraction(fraction);
                fraction.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteFractions(List<Integer> fractionIds) {
        if (fractionIds == null) return;
        fractionIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryFraction(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdate(columnNamesByReferentialTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFractionUsedInProgram(int fractionId) {

        return queryCount("countPmfmStrategyByFractionId", "fractionId", IntegerType.INSTANCE, fractionId) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFractionUsedInRules(int fractionId) {

        return executeMultipleCount(columnNamesByRulesTableNames, fractionId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFractionUsedInReferential(int fractionId) {

        return executeMultipleCount(columnNamesByReferentialTableNames, fractionId) || isFractionUsedInRules(fractionId);
    }

    private void saveFraction(FractionDTO fraction) {
        Assert.notNull(fraction);
        Assert.notBlank(fraction.getName());

        if (fraction.getStatus() == null) {
            fraction.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(fraction.getStatus()), "source must have local status");

        Fraction target;
        if (fraction.getId() == null) {
            target = Fraction.Factory.newInstance();
            target.setFractionId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(fraction.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setFractionNm(fraction.getName());
        target.setFractionDc(fraction.getDescription());
        target.setStatus(load(StatusImpl.class, fraction.getStatus().getCode()));
        if (target.getFractionCreationDt() == null) {
            target.setFractionCreationDt(newCreateDate());
        }
        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        fraction.setId(target.getFractionId());

        // save associated matrices
        if (fraction.sizeMatrixes() > 0) {

            Map<Integer, Matrix> remainingMatrices = ReefDbBeans.mapByProperty(target.getMatrixes(), "matrixId");
            for (MatrixDTO matrix : fraction.getMatrixes()) {
                if (remainingMatrices.remove(matrix.getId()) == null) {
                    // add matrix
                    target.addMatrixes(load(MatrixImpl.class, matrix.getId()));
                }
            }

            if (!remainingMatrices.isEmpty()) {
                target.getMatrixes().removeAll(remainingMatrices.values());
            }

        } else if (target.getMatrixes() != null) {
            target.getMatrixes().clear();
        }

        getSession().save(target);
    }

    private FractionDTO toFractionDTO(Iterator<Object> source) {
        FractionDTO result = ReefDbBeanFactory.newFractionDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

}
