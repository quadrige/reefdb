package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.ParameterImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValue;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.QualitativeValueDaoImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Dao for QualitativeValue
 * Created by Ludovic on 22/09/2015.
 */
@Repository("reefDbQualitativeValueDao")
public class ReefDbQualitativeValueDaoImpl extends QualitativeValueDaoImpl implements ReefDbQualitativeValueDao {

    @Resource
    protected CacheService cacheService;

    /**
     * Constructor used by Spring
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbQualitativeValueDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<QualitativeValueDTO> getQualitativeValuesByParameterCode(String parameterCode) {

        Cache cacheById = cacheService.getCache(QUALITATIVE_VALUE_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(
                createQuery("allQualitativeValuesByParameterCode", "parameterCode", StringType.INSTANCE, parameterCode),
                StatusFilter.ALL.toStatusCodes());
        List<QualitativeValueDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            QualitativeValueDTO qualitativeValue = toQualitativeValueDTO(Arrays.asList(source).iterator());
            result.add(qualitativeValue);
            cacheById.put(qualitativeValue.getId(), qualitativeValue);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<QualitativeValueDTO> getQualitativeValuesByPmfmId(Integer pmfmId) {
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(
                createQuery("allQualitativeValuesByPmfmId", "pmfmId", IntegerType.INSTANCE, pmfmId),
                StatusFilter.ACTIVE.toStatusCodes());
        List<QualitativeValueDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            QualitativeValueDTO qualitativeValue = toQualitativeValueDTO(Arrays.asList(source).iterator());
            result.add(qualitativeValue);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public QualitativeValueDTO getQualitativeValueById(int qualitativeValueId) {
        Object[] source = queryUnique("qualitativeValueById", "qualitativeValueId", IntegerType.INSTANCE, qualitativeValueId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load qualitative value with id = " + qualitativeValueId);
        }

        return toQualitativeValueDTO(Arrays.asList(source).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public void saveQualitativeValue(String parameterCode, QualitativeValueDTO qualitativeValue) {
        Assert.notBlank(parameterCode);
        Assert.notNull(qualitativeValue);
        Assert.notBlank(qualitativeValue.getName());
        if (qualitativeValue.getStatus() == null) {
            qualitativeValue.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(qualitativeValue.getStatus()), "source must have local status");

        QualitativeValue target;
        if (qualitativeValue.getId() == null) {
            target = QualitativeValue.Factory.newInstance();
            target.setQualValueId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
            target.setParameter(load(ParameterImpl.class, parameterCode));
        } else {
            target = get(qualitativeValue.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
            Assert.equals(target.getParameter().getParCd(), parameterCode, "existing qualitative value for another parameter");
        }

        target.setQualValueNm(qualitativeValue.getName());
        target.setQualValueDc(qualitativeValue.getDescription());
        target.setStatus(load(StatusImpl.class, qualitativeValue.getStatus().getCode()));
//        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        getSession().flush();
        qualitativeValue.setId(target.getQualValueId());
    }

    private QualitativeValueDTO toQualitativeValueDTO(Iterator<Object> source) {
        QualitativeValueDTO result = ReefDbBeanFactory.newQualitativeValueDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));
        return result;
    }

}
