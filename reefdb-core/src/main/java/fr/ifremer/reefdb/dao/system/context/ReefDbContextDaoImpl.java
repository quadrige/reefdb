package fr.ifremer.reefdb.dao.system.context;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.quadrige3.core.dao.referential.Status;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.system.context.Context;
import fr.ifremer.quadrige3.core.dao.system.context.ContextDaoImpl;
import fr.ifremer.quadrige3.core.dao.system.filter.Filter;
import fr.ifremer.quadrige3.core.dao.system.filter.FilterImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * <p>ReefDbContextDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbContextDao")
public class ReefDbContextDaoImpl extends ContextDaoImpl implements ReefDbContextDao {

    private static final Log log = LogFactory.getLog(ReefDbContextDaoImpl.class);

    @Resource
    protected CacheService cacheService;

    /**
     * <p>Constructor for ReefDbContextDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbContextDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<ContextDTO> getAllContext() {

        // TODO why temp status ?

        Cache cacheById = cacheService.getCache(CONTEXT_BY_ID_CACHE);
        Iterator<Object[]> it = queryIterator("allContext",
                "statusCode", StringType.INSTANCE, StatusCode.LOCAL_ENABLE.getValue());

        List<ContextDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            ContextDTO context = toContextDTO(Arrays.asList(row).iterator());
            result.add(context);
            cacheById.put(context.getId(), context);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public ContextDTO getContextById(Integer contextId) {
        Assert.notNull(contextId);

        Object[] row = queryUnique("contextById",
                "contextId", IntegerType.INSTANCE, contextId,
                "statusCode", StringType.INSTANCE, StatusCode.LOCAL_ENABLE.getValue());

        if (row == null) {
            throw new DataRetrievalFailureException("can't load local context with id = " + contextId);
        }

        return toContextDTO(Arrays.asList(row).iterator());
    }

    /** {@inheritDoc} */
    @Override
    public void saveContext(ContextDTO context) {

        Context entity;
        Long contextCount = queryCount("countContextsByName", "contextName", StringType.INSTANCE, context.getName());
        if (context.getId() == null) {
            // check context name unicity
            if (contextCount > 0) {
                throw new DataIntegrityViolationException("A context already exists with the name: " + context.getName());
            } else {
                entity = create(beanToEntity(context));
            }
        } else {
            entity = get(context.getId());
            // name
            if (StringUtils.isNotEmpty(context.getName()) && !context.getName().equals(entity.getContextNm())) {
                // check context name unicity
                if (contextCount > 0) {
                    throw new DataIntegrityViolationException("A context already exists with the name: " + context.getName());
                } else {
                    entity.setContextNm(context.getName());
                }
            }

            // description
            if (StringUtils.isNotEmpty(context.getDescription())) {
                entity.setContextDc(context.getDescription());
            }

            // filters
            if (CollectionUtils.isNotEmpty(context.getFilters())) {
                List<Integer> filterIds = ReefDbBeans.collectIds(context.getFilters());

                Collection<Filter> filters = entity.getFilters();
                if (filters == null) {
                    filters = Sets.newHashSet();
                    entity.setFilters(filters);
                } else {
                    // clear old records
                    filters.clear();
                }

                for (Integer filterId : filterIds) {
                    Filter f = (Filter) getSession().get(FilterImpl.class, filterId);
                    filters.add(f);
                }
            }

        }

        getSession().saveOrUpdate(entity);
        // TODO make sure unused filters are removed

        context.setId(entity.getContextId());

        getSession().flush();
        getSession().clear();

    }

    /** {@inheritDoc} */
    @Override
    public void deleteContexts(List<Integer> contextIds) {

        if (CollectionUtils.isEmpty(contextIds)) {
            return;
        }

        createQuery("deleteContexts").setParameterList("contextIds", contextIds).executeUpdate();
        getSession().flush();
        getSession().clear();
    }
    
    /* internal methods */

    private ContextDTO toContextDTO(Iterator<Object> source) {
        ContextDTO result = ReefDbBeanFactory.newContextDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setDescription((String) source.next());
        // TODO-EIS set status (String) source.next()
        return result;
    }

    private Context beanToEntity(ContextDTO context) {
        Context entity = Context.Factory.newInstance();
        entity.setContextNm(context.getName());
        entity.setContextDc(context.getDescription());
        entity.setStatus((Status) getSession().get(StatusImpl.class, StatusCode.LOCAL_ENABLE.getValue()));
        // TODO creation date, quser  ...

        List<Integer> filterIds = ReefDbBeans.collectIds(context.getFilters());
        for (Integer filterId : filterIds) {
            Filter filter = (Filter) getSession().get(FilterImpl.class, filterId);
            entity.addFilters(filter);
        }

        return entity;
    }

}
