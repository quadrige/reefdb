package fr.ifremer.reefdb.dao.referential.transcribing;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import fr.ifremer.quadrige3.core.dao.referential.transcribing.TranscribingItemDaoImpl;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

/**
 * @author peck7 on 02/11/2017.
 */
@Repository("reefDbTranscribingItemDao")
public class ReefDbTranscribingItemDaoImpl extends TranscribingItemDaoImpl implements ReefDbTranscribingItemDao {

    public ReefDbTranscribingItemDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
