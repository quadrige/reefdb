package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 29/07/2015.
 */
public interface ReefDbMatrixDao {

    String ALL_MATRICES_CACHE = "all_matrices";
    String MATRIX_BY_ID_CACHE = "matrix_by_id";

    /**
     * <p>getAllMatrices.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_MATRICES_CACHE)
    List<MatrixDTO> getAllMatrices(List<String> statusCodes);

    /**
     * <p>getMatrixById.</p>
     *
     * @param matrixId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO} object.
     */
    @Cacheable(value = MATRIX_BY_ID_CACHE)
    MatrixDTO getMatrixById(int matrixId);

    /**
     * <p>findMatrices.</p>
     *
     * @param matrixId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<MatrixDTO> findMatrices(Integer matrixId, List<String> statusCodes);

    /**
     * <p>saveMatrices.</p>
     *
     * @param matrices a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_MATRICES_CACHE,
            MATRIX_BY_ID_CACHE,
            ReefDbFractionDao.FRACTIONS_BY_MATRIX_ID_CACHE
    }, allEntries = true)
    void saveMatrices(List<? extends MatrixDTO> matrices);

    /**
     * <p>deleteMatrices.</p>
     *
     * @param matrixIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_MATRICES_CACHE,
            MATRIX_BY_ID_CACHE,
            ReefDbFractionDao.FRACTIONS_BY_MATRIX_ID_CACHE
    }, allEntries = true)
    void deleteMatrices(List<Integer> matrixIds);

    /**
     * <p>replaceTemporaryMatrix.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_MATRICES_CACHE,
            MATRIX_BY_ID_CACHE,
            ReefDbFractionDao.FRACTIONS_BY_MATRIX_ID_CACHE
    }, allEntries = true)
    void replaceTemporaryMatrix(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isMatrixUsedInProgram.</p>
     *
     * @param matrixId a int.
     * @return a boolean.
     */
    boolean isMatrixUsedInProgram(int matrixId);

    /**
     * <p>isMatrixUsedInRules.</p>
     *
     * @param matrixId a int.
     * @return a boolean.
     */
    boolean isMatrixUsedInRules(int matrixId);

    /**
     * <p>isMatrixUsedInReferential.</p>
     *
     * @param matrixId a int.
     * @return a boolean.
     */
    boolean isMatrixUsedInReferential(int matrixId);

}
