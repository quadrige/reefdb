package fr.ifremer.reefdb.dao.administration.program;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.administration.program.ProgramDao;
import fr.ifremer.reefdb.dao.administration.strategy.ReefDbStrategyDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * <p>ReefDbProgramDao interface.</p>
 *
 */
public interface ReefDbProgramDao extends ProgramDao {

    String PROGRAM_BY_CODE_CACHE = "program_by_code";
    String ALL_PROGRAMS_CACHE = "all_programs";
    String PROGRAMS_BY_CAMPAIGN_ID_CACHE = "programs_by_campaign_id";
    String PROGRAMS_BY_CODES_CACHE = "programs_by_codes";

    /**
     * <p>getAllPrograms.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_PROGRAMS_CACHE)
    List<ProgramDTO> getAllPrograms();

    /**
     * <p>getProgramByCode.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    @Cacheable(value = PROGRAM_BY_CODE_CACHE)
    ProgramDTO getProgramByCode(String programCode);

    /**
     * <p>getProgramsByCampaignId.</p>
     *
     * @param campaignId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PROGRAMS_BY_CAMPAIGN_ID_CACHE)
    List<ProgramDTO> getProgramsByCampaignId(Integer campaignId);

    /**
     * <p>getProgramsByCodes.</p>
     *
     * @param programCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PROGRAMS_BY_CODES_CACHE)
    List<ProgramDTO> getProgramsByCodes(List<String> programCodes);

    /**
     * <p>saveProgram.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ALL_PROGRAMS_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAMS_BY_CAMPAIGN_ID_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAMS_BY_CODES_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAM_BY_CODE_CACHE, key = "#program.code"),
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true),
            @CacheEvict(value = ReefDbMonitoringLocationDao.LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE, allEntries = true)
    })
    void saveProgram(ProgramDTO program);

    /**
     * <p>remove.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ALL_PROGRAMS_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAMS_BY_CAMPAIGN_ID_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAMS_BY_CODES_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAM_BY_CODE_CACHE, key = "#programCode"),
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true),
            @CacheEvict(value = ReefDbMonitoringLocationDao.LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE, allEntries = true)
    })
    void remove(String programCode);

    /**
     * <p>deleteProgramLocations.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @param monitoringLocationIds a {@link java.util.Collection} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ALL_PROGRAMS_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAMS_BY_CAMPAIGN_ID_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAMS_BY_CODES_CACHE, allEntries = true),
            @CacheEvict(value = PROGRAM_BY_CODE_CACHE, key = "#programCode"),
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true),
            @CacheEvict(value = ReefDbMonitoringLocationDao.LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE, allEntries = true)
    })
    void deleteProgramLocations(String programCode, Collection<Integer> monitoringLocationIds);


    /**
     * <p>findProgramsByCodeAndName.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param code a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<ProgramDTO> findProgramsByCodeAndName(List<String> statusCodes, String code, String name);

    /**
     * <p>findProgramsByLocationAndDate.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @param locationId a int.
     * @param date a {@link java.util.Date} object.
     * @return a {@link java.util.List} object.
     */
    List<ProgramDTO> findProgramsByLocationAndDate(List<String> statusCodes, int locationId, Date date);

    /**
     * <p>isProgramUsedByRuleList.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean isProgramUsedByRuleList(String programCode);

}
