package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.UnitDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * Created by Ludovic on 28/07/2015.
 */
public interface ReefDbUnitDao {

    String ALL_UNITS_CACHE = "all_units";
    String UNIT_BY_ID_CACHE = "unit_by_id";

    /**
     * <p>getAllUnits.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_UNITS_CACHE)
    List<UnitDTO> getAllUnits(List<String> statusCodes);

    /**
     * <p>getUnitById.</p>
     *
     * @param unitId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.UnitDTO} object.
     */
    @Cacheable(value = UNIT_BY_ID_CACHE)
    UnitDTO getUnitById(int unitId);

    /**
     * <p>findUnits.</p>
     *
     * @param unitId a {@link java.lang.Integer} object.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<UnitDTO> findUnits(Integer unitId, List<String> statusCodes);

    /**
     * <p>saveUnits.</p>
     *
     * @param units a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_UNITS_CACHE,
            UNIT_BY_ID_CACHE
    }, allEntries = true)
    void saveUnits(List<? extends UnitDTO> units);

    /**
     * <p>deleteUnits.</p>
     *
     * @param unitIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_UNITS_CACHE,
            UNIT_BY_ID_CACHE
    }, allEntries = true)
    void deleteUnits(List<Integer> unitIds);

    /**
     * <p>replaceTemporaryUnit.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_UNITS_CACHE,
            UNIT_BY_ID_CACHE
    }, allEntries = true)
    void replaceTemporaryUnit(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isUnitUsedInReferential.</p>
     *
     * @param unitId a int.
     * @return a boolean.
     */
    boolean isUnitUsedInReferential(int unitId);

    /**
     * <p>isUnitUsedInData.</p>
     *
     * @param unitId a int.
     * @return a boolean.
     */
    boolean isUnitUsedInData(int unitId);

    /**
     * <p>isUnitUsedInValidatedData.</p>
     *
     * @param unitId a int.
     * @return a boolean.
     */
    boolean isUnitUsedInValidatedData(int unitId);
}
