package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.TaxonGroupTypeCode;
import fr.ifremer.quadrige3.core.dao.referential.TaxonGroupTypeImpl;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonGroup;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonGroupDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonGroupHistoricalRecord;
import fr.ifremer.quadrige3.core.dao.referential.taxon.TaxonNameImpl;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ReefDbTaxonGroupDaoImpl class.</p>
 *
 */
@Repository("reefDbTaxonGroupDao")
public class ReefDbTaxonGroupDaoImpl extends TaxonGroupDaoImpl implements ReefDbTaxonGroupDao {

//    private static final Log log = LogFactory.getLog(ReefDbTaxonGroupDaoImpl.class);

    private static final Multimap<String, String> columnNamesByReferentialTableNames = ImmutableListMultimap.<String, String>builder()
            .put("TAXON_GROUP", "PARENT_TAXON_GROUP_ID")
            .put("TAXON_GROUP_HISTORICAL_RECORD", "TAXON_GROUP_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("TAXON_MEASUREMENT", "TAXON_GROUP_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("TAXON_MEASUREMENT", "TAXON_MEAS_VALID_DT").build();

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbTaxonGroupDao")
    protected ReefDbTaxonGroupDao loopbackTaxonGroupDao;
    @Resource(name = "reefDbTaxonNameDao")
    protected ReefDbTaxonNameDao taxonNameDao;

    /**
     * <p>Constructor for ReefDbTaxonGroupDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbTaxonGroupDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonGroupDTO> getAllTaxonGroups() {

        // get all needed cache to update
        Cache cacheById = cacheService.getCache(TAXON_GROUP_BY_ID_CACHE);
        Cache allTaxonNamesCache = cacheService.getCache(ReefDbTaxonNameDao.ALL_TAXON_NAMES_CACHE);
        Cache taxonCache = cacheService.getCache(ReefDbTaxonNameDao.TAXON_NAME_BY_ID_CACHE);
        Cache referenceTaxonCache = cacheService.getCache(ReefDbTaxonNameDao.TAXON_NAME_BY_REFERENCE_ID_CACHE);
        Cache taxonNameByTaxonGroupIdCache = cacheService.getCache(ReefDbTaxonNameDao.TAXON_NAME_BY_TAXON_GROUP_ID_CACHE);

        // get all taxon names
        List<TaxonDTO> allTaxonNames = taxonNameDao.getAllTaxonNames();
        // get all taxon names map
        LocalDate taxonRefDate = LocalDate.now();
        Multimap<Integer, TaxonDTO> taxonMap = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(taxonRefDate);

        Query query = createQuery("allTaxonGroup",
                "taxonGroupTypeCode", StringType.INSTANCE, TaxonGroupTypeCode.IDENTIFICATION.getValue());
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query,
                ImmutableList.of(StatusCode.ENABLE.getValue(), StatusCode.TEMPORARY.getValue(), StatusCode.LOCAL_ENABLE.getValue()));

        // TODO : control TAXON_GROUP_EXCLUS, TAXON_GROUP_UPDATE ?
        List<TaxonGroupDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonGroupDTO taxonGroup = toTaxonGroupDTO(Arrays.asList(source).iterator());

            // Clone this taxon group and remove parent & children
            TaxonGroupDTO lightTaxonGroup = getLightTaxonGroup(taxonGroup);

            // add this taxon group to all taxon
            Collection<TaxonDTO> taxons = taxonMap.get(taxonGroup.getId());
            for (TaxonDTO taxon : taxons) {

                // add this taxon group
                if (!taxon.containsTaxonGroups(lightTaxonGroup)) {
                    taxon.addTaxonGroups(lightTaxonGroup);
                }

                // need to update taxonNameById cache
                {
                    TaxonDTO cachedTaxon = taxonCache.get(taxon.getId(), TaxonDTO.class);
                    if (cachedTaxon != null && !cachedTaxon.containsTaxonGroups(lightTaxonGroup)) {
                        cachedTaxon.addTaxonGroups(lightTaxonGroup);
                        taxonCache.evict(taxon.getId());
                        taxonCache.put(taxon.getId(), cachedTaxon);
                    }
                }

                // need to update reference taxon cache
                {
                    TaxonDTO cachedReferenceTaxon = referenceTaxonCache.get(taxon.getReferenceTaxonId(), TaxonDTO.class);
                    if (cachedReferenceTaxon != null && !cachedReferenceTaxon.containsTaxonGroups(lightTaxonGroup)) {
                        cachedReferenceTaxon.addTaxonGroups(lightTaxonGroup);
                        referenceTaxonCache.evict(taxon.getReferenceTaxonId());
                        referenceTaxonCache.put(taxon.getReferenceTaxonId(), cachedReferenceTaxon);
                    }
                }

                // need to update allTaxonNames cache also
                {
                    TaxonDTO cachedTaxon = ReefDbBeans.findById(allTaxonNames, taxon.getId());
                    if (!cachedTaxon.containsTaxonGroups(lightTaxonGroup)) {
                        cachedTaxon.addTaxonGroups(lightTaxonGroup);
                    }
                }

                // add taxon parent and referent
                taxonNameDao.fillParentAndReferent(taxon);
            }

            // add taxon names
            taxonGroup.addAllTaxons(taxons);

            // add to cache by id
            cacheById.put(taxonGroup.getId(), taxonGroup);

            result.add(taxonGroup);
        }

        // update taxonNameByTaxonGroupId cache
        taxonNameByTaxonGroupIdCache.clear();
        taxonNameByTaxonGroupIdCache.put(taxonRefDate, taxonMap);

        // update allTaxonNames cache
        allTaxonNamesCache.put(SimpleKey.EMPTY, allTaxonNames);

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public TaxonGroupDTO getTaxonGroupById(int taxonGroupId) {

        // TODO : control TAXON_GROUP_EXCLUS, TAXON_GROUP_UPDATE ?
        Object[] source = queryUnique("taxonGroupById", "taxonGroupId", IntegerType.INSTANCE, taxonGroupId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load taxon group with id = " + taxonGroupId);
        }

        TaxonGroupDTO taxonGroup = toTaxonGroupDTO(Arrays.asList(source).iterator());

        // Clone this taxon group and remove parent & children
        TaxonGroupDTO lightTaxonGroup = getLightTaxonGroup(taxonGroup);

        Collection<TaxonDTO> taxons = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now()).get(taxonGroupId);
        for (TaxonDTO taxon : taxons) {
            if (!taxon.containsTaxonGroups(lightTaxonGroup)) {
                taxon.addTaxonGroups(lightTaxonGroup);
            }
        }

//        taxonGroup.setTaxons(ReefDbBeans.getList(taxons));
        taxonGroup.addAllTaxons(taxons);
        return taxonGroup;
    }

    /** {@inheritDoc}
     * @param taxonGroupIds*/
    @Override
    public List<TaxonGroupDTO> getTaxonGroupsByIds(Collection<Integer> taxonGroupIds) {

        if (CollectionUtils.isEmpty(taxonGroupIds)) return new ArrayList<>();

        return loopbackTaxonGroupDao.getAllTaxonGroups().stream().filter(taxonGroup -> taxonGroupIds.contains(taxonGroup.getId())).collect(Collectors.toList());
    }

    /** {@inheritDoc} */
    @Override
    public List<TaxonGroupDTO> findTaxonGroups(Integer parentTaxonGroupId, String label, String name, boolean isStrictName, List<String> statusCodes) {
        // get all taxon names map
        Multimap<Integer, TaxonDTO> taxonMap = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now());

        Query query = createQuery("taxonGroupsByCriteria",
                "parentTaxonGroupId", IntegerType.INSTANCE, parentTaxonGroupId,
                "label", StringType.INSTANCE, label,
                "name", StringType.INSTANCE, isStrictName ? null : name,
                "strictName", StringType.INSTANCE, isStrictName ? name : null,
                "taxonGroupTypeCode", StringType.INSTANCE, TaxonGroupTypeCode.IDENTIFICATION.getValue());

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        List<TaxonGroupDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            TaxonGroupDTO taxonGroup = toTaxonGroupDTO(Arrays.asList(source).iterator());
            TaxonGroupDTO lightTaxonGroup = getLightTaxonGroup(taxonGroup);

            // add this taxon group to all taxon
            Collection<TaxonDTO> taxons = taxonMap.get(taxonGroup.getId());
            for (TaxonDTO taxon : taxons) {
                if (!taxon.containsTaxonGroups(lightTaxonGroup)) {
                    taxon.addTaxonGroups(lightTaxonGroup);
                }
            }

            taxonGroup.addAllTaxons(taxons);
            result.add(taxonGroup);
        }

        // Mantis #0027042: when parentTaxonGroupId is set, do a recursive search to populate sub groups
        if (CollectionUtils.isNotEmpty(result) && parentTaxonGroupId != null) {
            Set<TaxonGroupDTO> subTaxonGroups = Sets.newHashSet();
            for (TaxonGroupDTO taxonGroup : result) {
                // populate sub groups
                subTaxonGroups.addAll(findTaxonGroups(taxonGroup.getId(), label, name, isStrictName, statusCodes));
            }
            result.addAll(subTaxonGroups);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public void saveTaxonGroups(List<? extends TaxonGroupDTO> taxonGroups) {
        if (CollectionUtils.isEmpty(taxonGroups)) {
            return;
        }

        for (TaxonGroupDTO taxonGroup : taxonGroups) {
            if (taxonGroup.isDirty()) {
                saveTaxonGroup(taxonGroup);
                taxonGroup.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteTaxonGroups(List<Integer> taxonGroupIds) {
        if (taxonGroupIds == null) return;
        taxonGroupIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryTaxonGroup(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdate(columnNamesByReferentialTableNames, sourceId, targetId);
        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTaxonGroupUsedInReferential(int taxonGroupId) {

        return executeMultipleCount(columnNamesByReferentialTableNames, taxonGroupId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTaxonGroupUsedInData(int taxonGroupId) {

        return executeMultipleCount(columnNamesByDataTableNames, taxonGroupId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTaxonGroupUsedInValidatedData(int taxonGroupId) {

        return executeMultipleCountWithNotNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, taxonGroupId);
    }

    // INTERNAL METHODS
    private void saveTaxonGroup(TaxonGroupDTO taxonGroup) {
        Assert.notNull(taxonGroup);
        Assert.notBlank(taxonGroup.getName());

        if (taxonGroup.getStatus() == null) {
            taxonGroup.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(taxonGroup.getStatus()), "source must have local status");

        TaxonGroup target;
        if (taxonGroup.getId() == null) {
            target = TaxonGroup.Factory.newInstance();
            target.setTaxonGroupId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(taxonGroup.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setTaxonGroupNm(taxonGroup.getName());
        target.setStatus(load(StatusImpl.class, taxonGroup.getStatus().getCode()));
        target.setTaxonGroupType(load(TaxonGroupTypeImpl.class, "1"));
        target.setTaxonGroupUpdate(Daos.convertToString(true));
        target.setTaxonGroupExclus(Daos.convertToString(false));

        target.setTaxonGroupLb(taxonGroup.getLabel());
        if (taxonGroup.getParentTaxonGroup() != null) {
            target.setParentTaxonGroup(load(taxonGroup.getParentTaxonGroup().getId()));
        } else {
            target.setParentTaxonGroup(null);
        }
        target.setTaxonGroupCm(taxonGroup.getComment());
        if (target.getTaxonGroupCreationDt() == null) {
            target.setTaxonGroupCreationDt(newCreateDate());
        }
        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        taxonGroup.setId(target.getTaxonGroupId());

        // taxons
        saveTaxonsInTaxonGroup(taxonGroup, target);


    }

    private void saveTaxonsInTaxonGroup(TaxonGroupDTO source, TaxonGroup target) {

        Collection<TaxonGroupHistoricalRecord> existingTaxonGroupHistoricalRecords = target.getTaxonGroupHistoricalRecords();
        Map<Integer, TaxonGroupHistoricalRecord> tghrByTaxonId = ReefDbBeans.mapByProperty(existingTaxonGroupHistoricalRecords, "taxonName.taxonNameId");
        boolean needUpdate = false;

        for (TaxonDTO taxon : source.getTaxons()) {
            TaxonGroupHistoricalRecord tghr = tghrByTaxonId.remove(taxon.getId());

            if (tghr == null) {
                tghr = TaxonGroupHistoricalRecord.Factory.newInstance();
                tghr.setTaxonGroupHistRecordId((Integer) TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), TaxonGroupHistoricalRecord.class));
                tghr.setTaxonName(load(TaxonNameImpl.class, taxon.getId()));
                tghr.setTaxonGroup(target);
                getSession().save(tghr);
                target.addTaxonGroupHistoricalRecords(tghr);
                needUpdate = true;
            }
        }

        if (!tghrByTaxonId.isEmpty()) {
            // remove remaining
            for (TaxonGroupHistoricalRecord tghrToRemove : tghrByTaxonId.values()) {
                target.removeTaxonGroupHistoricalRecords(tghrToRemove);
                getSession().delete(tghrToRemove);
            }
            needUpdate = true;
        }

        if (needUpdate) {

            update(target);

            // get all taxon names map
            Multimap<Integer, TaxonDTO> taxonMap = taxonNameDao.getAllTaxonNamesMapByTaxonGroupId(LocalDate.now());
            taxonMap.replaceValues(source.getId(), source.getTaxons());

            // update taxonNameByTaxonGroupId cache
            Cache taxonNameByTaxonGroupIdCache = cacheService.getCache(ReefDbTaxonNameDao.TAXON_NAME_BY_TAXON_GROUP_ID_CACHE);
            taxonNameByTaxonGroupIdCache.evict(null);
            taxonNameByTaxonGroupIdCache.put(null, taxonMap);
        }
    }

    private TaxonGroupDTO toTaxonGroupDTO(Iterator<Object> source) {
        TaxonGroupDTO result = ReefDbBeanFactory.newTaxonGroupDTO();
        result.setId((Integer) source.next());
        result.setLabel((String) source.next());
        result.setName((String) source.next());
        result.setComment((String) source.next());
        result.setExclusive(Daos.safeConvertToBoolean(source.next()));
        result.setUpdate(Daos.safeConvertToBoolean(source.next()));
        result.setType((String) source.next());

        // parent
        Integer parentId = (Integer) source.next();
        String parentLabel = (String) source.next();
        String parentName = (String) source.next();

        if (parentId != null) {
            TaxonGroupDTO parent = ReefDbBeanFactory.newTaxonGroupDTO();
            parent.setId(parentId);
            parent.setLabel(parentLabel);
            parent.setName(parentName);

            result.setParentTaxonGroup(parent);
        }

        result.setStatus(Daos.getStatus((String) source.next()));
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        return result;
    }

    private TaxonGroupDTO getLightTaxonGroup(TaxonGroupDTO taxonGroup) {
        TaxonGroupDTO result = ReefDbBeans.clone(taxonGroup);
        result.setTaxons(null);
        result.setParentTaxonGroup(null);
        return result;
    }

}
