package fr.ifremer.reefdb.dao.administration.program;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.Search;
import fr.ifremer.quadrige3.core.dao.SearchParameter;
import fr.ifremer.quadrige3.core.dao.administration.program.*;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.administration.user.QuserImpl;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.Unit;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Fraction;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Matrix;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.Method;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.strategy.ReefDbStrategyDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumDTO;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumPeriodDTO;
import fr.ifremer.reefdb.dto.configuration.moratorium.MoratoriumPmfmDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ReefDbProgramDaoImpl class.</p>
 *
 */
@Repository("reefDbProgramDao")
public class ReefDbProgramDaoImpl extends ProgramDaoImpl implements ReefDbProgramDao {

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(ReefDbProgramDaoImpl.class);

    private static final Multimap<String, String> columnNamesByRulesTableNames = ImmutableListMultimap.<String, String>builder()
            .put("RULE_LIST_PROG", "PROG_CD").build();

    @Resource(name = "reefDbStrategyDao")
    protected ReefDbStrategyDao strategyDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource
    protected ProgQuserProgPrivDao progQuserProgPrivDao;

    @Resource
    protected ProgDepProgPrivDao progDepProgPrivDao;

    @Resource
    protected CacheService cacheService;

    @Resource
    protected MoratoriumDao moratoriumDao;

    @Resource
    protected ReefDbConfiguration config;

    /**
     * <p>Constructor for ReefDbProgramDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbProgramDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<ProgramDTO> getAllPrograms() {

        Cache cacheByCode = cacheService.getCache(PROGRAM_BY_CODE_CACHE);

        Iterator<Object[]> rows = queryIterator("allPrograms");

        List<ProgramDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            ProgramDTO program = toProgramDTO(Arrays.asList(rows.next()).iterator());

            // Add program privileges
            getProgramPrivileges(program);
            // Add moratorium
            getProgramMoratoriums(program);

            result.add(program);
            cacheByCode.put(program.getCode(), program);
        }

        return ImmutableList.copyOf(result);
    }

    private void getProgramPrivileges(ProgramDTO program) {

        // TODO save program privileges, penser à ajouter le recorder par défaut

        // query the PROG_QUSER_PROG_PRIV table
        Iterator<Object[]> rows = queryIterator("programPrivilegesForUsersByProgramCode",
                "programCode", StringType.INSTANCE, program.getCode());

        while (rows.hasNext()) {
            Object[] row = rows.next();
            Integer privilegeId = (Integer) row[0];
            Integer quserId = (Integer) row[1];

            if (privilegeId != null && quserId != null) {
                PersonDTO user = quserDao.getUserById(quserId);
                ProgramPrivilegeIds privilege = ProgramPrivilegeIds.fromValue(privilegeId);
                switch (privilege) {
                    case MANAGER:
                        program.addManagerPersons(user);
                        break;
                    case RECORDER:
                        program.addRecorderPersons(user);
                        break;
                    case FULL_VIEWER:
                        program.addFullViewerPersons(user);
                        break;
                    case VIEWER:
                        program.addViewerPersons(user);
                        break;
                    case VALIDATOR:
                        program.addValidatorPersons(user);
                        break;
                }
            }
        }

        // query the PROG_DEP_PROG_PRIV table
        rows = queryIterator("programPrivilegesForDepartmentsByProgramCode",
                "programCode", StringType.INSTANCE, program.getCode());

        while (rows.hasNext()) {
            Object[] row = rows.next();
            Integer privilegeId = (Integer) row[0];
            Integer depId = (Integer) row[1];

            if (privilegeId != null && depId != null) {
                DepartmentDTO department = departmentDao.getDepartmentById(depId);

                ProgramPrivilegeIds privilege = ProgramPrivilegeIds.fromValue(privilegeId);

                switch (privilege) {
                    case MANAGER:
                        program.addManagerDepartments(department);
                        break;
                    case RECORDER:
                        program.addRecorderDepartments(department);
                        break;
                    case FULL_VIEWER:
                        program.addFullViewerDepartments(department);
                        break;
                    case VIEWER:
                        program.addViewerDepartments(department);
                        break;
                    case VALIDATOR:
                        program.addValidatorDepartments(department);
                        break;
                }
            }

        }
    }

    private void getProgramMoratoriums(ProgramDTO program) {

        SearchParameter parameter = new SearchParameter("program.progCd", program.getCode());
        Search search = new Search(new SearchParameter[]{parameter});
        Set<Moratorium> moratoriums = moratoriumDao.search(search);
        TimeZone dbTimezone = config.getDbTimezone();

        moratoriums.forEach(moratorium -> {

            // convert to dto
            MoratoriumDTO moratoriumDTO = ReefDbBeanFactory.newMoratoriumDTO();
            moratoriumDTO.setProgramCode(program.getCode());
            moratoriumDTO.setId(moratorium.getMorId());
            moratoriumDTO.setName(moratorium.getMorDc());
            moratoriumDTO.setGlobal(Daos.convertToBoolean(moratorium.getMorIsGlobal()));
            moratoriumDTO.setLocationIds(new ArrayList<>());
            moratoriumDTO.setCampaignIds(new ArrayList<>());
            moratoriumDTO.setOccasionIds(new ArrayList<>());

            // periods
            moratorium.getMorPeriods().forEach(morPeriod -> {
                MoratoriumPeriodDTO periodDTO = ReefDbBeanFactory.newMoratoriumPeriodDTO();
                periodDTO.setStartDate(Dates.convertToLocalDate(Daos.convertToDate(morPeriod.getMorPerStartDt()), dbTimezone));
                periodDTO.setEndDate(Dates.convertToLocalDate(Daos.convertToDate(morPeriod.getMorPerEndDt()), dbTimezone));
                moratoriumDTO.addPeriods(periodDTO);
            });

            // locations
            moratorium.getMonLocProgs().forEach(monLocProg -> moratoriumDTO.getLocationIds().add(monLocProg.getMonitoringLocation().getMonLocId()));

            // campaigns
            moratorium.getCampaigns().forEach(campaign -> moratoriumDTO.getCampaignIds().add(campaign.getCampaignId()));

            // occasions
            moratorium.getOccasions().forEach(occasion -> moratoriumDTO.getOccasionIds().add(occasion.getOccasId()));

            // pmfms
            moratorium.getPmfmMors().forEach(pmfmMor -> {
                MoratoriumPmfmDTO pmfmDTO = ReefDbBeanFactory.newMoratoriumPmfmDTO();
                pmfmDTO.setParameterCode(pmfmMor.getParameter().getParCd());
                pmfmDTO.setMatrixId(Optional.ofNullable(pmfmMor.getMatrix()).map(Matrix::getMatrixId).orElse(null));
                pmfmDTO.setFractionId(Optional.ofNullable(pmfmMor.getFraction()).map(Fraction::getFractionId).orElse(null));
                pmfmDTO.setMethodId(Optional.ofNullable(pmfmMor.getMethod()).map(Method::getMethodId).orElse(null));
                pmfmDTO.setUnitId(Optional.ofNullable(pmfmMor.getUnit()).map(Unit::getUnitId).orElse(null));
                moratoriumDTO.addPmfms(pmfmDTO);
            });

            // add to program
            program.addMoratoriums(moratoriumDTO);
        });

    }

    /** {@inheritDoc} */
    @Override
    public ProgramDTO getProgramByCode(String programCode) {
        Assert.notBlank(programCode);

        Object[] row = queryUnique("programByCode",
                "programCode", StringType.INSTANCE, programCode);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load program with code = " + programCode);
        }

        ProgramDTO program = toProgramDTO(Arrays.asList(row).iterator());

        // Add program privileges
        getProgramPrivileges(program);
        // Add moratorium
        getProgramMoratoriums(program);

        return program;
    }

    /** {@inheritDoc} */
    @Override
    public List<ProgramDTO> getProgramsByCampaignId(Integer campaignId) {
        Iterator<Object[]> rows = queryIterator("programsByCampaignId",
                "campaignId", IntegerType.INSTANCE, campaignId);

        List<ProgramDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            result.add(toProgramDTO(Arrays.asList(rows.next()).iterator()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<ProgramDTO> getProgramsByCodes(List<String> programCodes) {

        Iterator<Object[]> rows = createQuery("programsByCodes")
                .setParameterList("programCodes", programCodes)
                .iterate();

        List<ProgramDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            result.add(toProgramDTO(Arrays.asList(rows.next()).iterator()));
        }

        return Collections.unmodifiableList(result);

    }

    /** {@inheritDoc} */
    @Override
    public List<ProgramDTO> findProgramsByCodeAndName(List<String> statusCodes, String code, String name) {
        Query query = createQuery("programsByCodeAndName",
                "code", StringType.INSTANCE, code,
                "name", StringType.INSTANCE, name);

        Iterator<Object[]> rows = Daos.queryIteratorWithStatus(query, statusCodes);

        List<ProgramDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            ProgramDTO program = toProgramDTO(Arrays.asList(rows.next()).iterator());

            // Add program privileges
            getProgramPrivileges(program);

            result.add(program);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<ProgramDTO> findProgramsByLocationAndDate(List<String> statusCodes, int locationId, Date date) {
        Query query = createQuery("programsByLocationAndDate",
                "locationId", IntegerType.INSTANCE, locationId,
                "date", DateType.INSTANCE, date);

        Iterator<Object[]> rows = Daos.queryIteratorWithStatus(query, statusCodes);

        List<ProgramDTO> result = Lists.newArrayList();
        while (rows.hasNext()) {
            result.add(toProgramDTO(Arrays.asList(rows.next()).iterator()));
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isProgramUsedByRuleList(String programCode) {
        return executeMultipleCount(columnNamesByRulesTableNames, programCode);
    }

    /** {@inheritDoc} */
    @Override
    public void saveProgram(ProgramDTO program) {
        Assert.notNull(program);
        Assert.notBlank(program.getCode());

        // Check program already exists
        Program target = get(program.getCode());
        boolean isNew = false;
        if (target == null) {
            // create a new local program
            target = Program.Factory.newInstance();
            target.setProgCd(program.getCode());
            target.setStatus(load(StatusImpl.class, StatusCode.LOCAL_ENABLE.getValue()));
            target.setProgCreationDt(newCreateDate());
            isNew = true;
        }

        // Transform to entity
        programDTOToEntity(program, target);

        // Save or update
        if (isNew) {
            getSession().save(target);
        } else {
            getSession().update(target);
        }

        // locations if loaded
        if (program.isLocationsLoaded() || !program.isLocationsEmpty()) {
            if (program.isLocationsEmpty()) {
                // clear locations
                if (!target.getMonLocProgs().isEmpty()) {
                    target.getMonLocProgs().clear();
                }
            } else {

                Multimap<Integer, MonLocProg> remainingMonLocProgs = ArrayListMultimap.create(
                        ReefDbBeans.populateByProperty(target.getMonLocProgs(), "monitoringLocation.monLocId")
                );

                for (LocationDTO locationDTO : program.getLocations()) {
                    boolean exists = remainingMonLocProgs.containsKey(locationDTO.getId());
                    if (!exists) {
                        // create new MonLocProg
                        MonLocProg monLocProg = MonLocProg.Factory.newInstance(target, load(MonitoringLocationImpl.class, locationDTO.getId()));
                        Integer monLocProgId = TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), monLocProg.getClass());
                        monLocProg.setMonLocProgId(monLocProgId);
                        getSession().save(monLocProg);
                        target.addMonLocProgs(monLocProg);
                    } else {
                        remainingMonLocProgs.removeAll(locationDTO.getId());
                    }
                }

                // remove unused MonLocProgs
                if (!remainingMonLocProgs.isEmpty()) {
                    target.getMonLocProgs().removeAll(remainingMonLocProgs.values());
                    // delete
                    deleteProgramLocations(program.getCode(), remainingMonLocProgs.keySet());

                    // remove locations from strategies
                    if (!program.isStrategiesEmpty()) {
                        for (StrategyDTO strategy : program.getStrategies()) {
                            if (!strategy.isAppliedStrategiesEmpty()) {
                                strategy.getAppliedStrategies().removeIf(lieu -> remainingMonLocProgs.containsKey(lieu.getId()));
                            }
                        }
                    }
                }
            }
        }

        getSession().flush();
        getSession().clear();

        // save strategies if loaded or strategies to save
        if (program.isStrategiesLoaded() || !program.isStrategiesEmpty()) {
            strategyDao.saveStrategies(program);

            // Make sure to flush the program
            getSession().flush();
            getSession().clear();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void remove(String programCode) {
        Assert.notBlank(programCode);

        // delete strategies
        strategyDao.removeByProgramCode(programCode);

        // delete Program
        super.remove(programCode);

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deleteProgramLocations(String programCode, Collection<Integer> monitoringLocationIds) {
        Assert.notBlank(programCode);
        if (monitoringLocationIds == null) return;
        Set<Integer> idsToDelete = monitoringLocationIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(idsToDelete)) {
            return;
        }

        // delete monLocProgs
        Query query = createQuery("deleteMonitoringLocationsByProgramCode", "programCode", StringType.INSTANCE, programCode);
        query.setParameterList("monitoringLocationIds", idsToDelete);
        query.executeUpdate();

        // delete applied strategies
        strategyDao.deleteAppliedStrategies(programCode, idsToDelete);

        getSession().flush();
        getSession().clear();
    }

    /* -- INTERNAL METHODS -- */

    private ProgramDTO toProgramDTO(Iterator<Object> source) {
        ProgramDTO result = ReefDbBeanFactory.newProgramDTO();

        // Code
        result.setCode((String) source.next());

        // Name
        result.setName((String) source.next());

        // Description
        result.setDescription((String) source.next());

        // Local status
        result.setStatus(Daos.getStatus((String) source.next()));

        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        return result;
    }

    private void programDTOToEntity(final ProgramDTO source, final Program target) {
        Assert.notNull(source);
        Assert.notNull(target);

        target.setProgNm(source.getName());
        target.setProgDc(source.getDescription());
        target.setProgCm(source.getComment());

        // Set update date (only if local program)
        if (ReefDbBeans.isLocalStatus(source.getStatus())) {
            target.setUpdateDt(newUpdateTimestamp());
        }

        // User privileges
        {
            Map<ProgQuserProgPrivPK, ProgQuserProgPriv> quserProgPrivs = new HashMap<>();

            // Manager user
            if (!source.isManagerPersonsEmpty()) {
                for (PersonDTO manager : source.getManagerPersons()) {
                    ProgQuserProgPriv progQuserProgPriv = loadProgQuserProgPriv(target, manager.getId(), ProgramPrivilegeIds.MANAGER);
                    quserProgPrivs.put(progQuserProgPriv.getProgQuserProgPrivPk(), progQuserProgPriv);
                }
            }

            // Recorder user
            if (!source.isRecorderPersonsEmpty()) {
                for (PersonDTO recorder : source.getRecorderPersons()) {
                    ProgQuserProgPriv progQuserProgPriv = loadProgQuserProgPriv(target, recorder.getId(), ProgramPrivilegeIds.RECORDER);
                    quserProgPrivs.put(progQuserProgPriv.getProgQuserProgPrivPk(), progQuserProgPriv);
                }
            }

            // Full viewer user
            if (!source.isFullViewerPersonsEmpty()) {
                for (PersonDTO fullViewer : source.getFullViewerPersons()) {
                    ProgQuserProgPriv progQuserProgPriv = loadProgQuserProgPriv(target, fullViewer.getId(), ProgramPrivilegeIds.FULL_VIEWER);
                    quserProgPrivs.put(progQuserProgPriv.getProgQuserProgPrivPk(), progQuserProgPriv);
                }
            }

            // Viewer user
            if (!source.isViewerPersonsEmpty()) {
                for (PersonDTO viewer : source.getViewerPersons()) {
                    ProgQuserProgPriv progQuserProgPriv = loadProgQuserProgPriv(target, viewer.getId(), ProgramPrivilegeIds.VIEWER);
                    quserProgPrivs.put(progQuserProgPriv.getProgQuserProgPrivPk(), progQuserProgPriv);
                }
            }

            // Validator user
            if (!source.isValidatorPersonsEmpty()) {
                for (PersonDTO validator : source.getValidatorPersons()) {
                    ProgQuserProgPriv progQuserProgPriv = loadProgQuserProgPriv(target, validator.getId(), ProgramPrivilegeIds.VALIDATOR);
                    quserProgPrivs.put(progQuserProgPriv.getProgQuserProgPrivPk(), progQuserProgPriv);
                }
            }

            // Update entity list
            if (MapUtils.isEmpty(quserProgPrivs)) {
                if (CollectionUtils.isNotEmpty(target.getProgQuserProgPrivs())) {
                    target.getProgQuserProgPrivs().clear();
                }
            } else {
                target.getProgQuserProgPrivs().clear();
                target.getProgQuserProgPrivs().addAll(quserProgPrivs.values());
            }
        }

        // Department privileges
        {
            Map<ProgDepProgPrivPK, ProgDepProgPriv> depProgPrivs = new HashMap<>();

            // Manager departments : keep all existing rights
            // TODO - en fonction de la reponse à la question mantis #28223
            if (CollectionUtils.isNotEmpty(target.getProgDepProgPrivs())) {
                for (ProgDepProgPriv depProgPriv : target.getProgDepProgPrivs()) {
                    ProgramPrivilege programPrivilege = depProgPriv.getProgDepProgPrivPk().getProgramPrivilege();
                    if (Objects.equals(programPrivilege.getProgPrivId(), ProgramPrivilegeIds.MANAGER.getValue())) {
                        depProgPrivs.put(depProgPriv.getProgDepProgPrivPk(), depProgPriv);
                    }
                }
            }

            // Recorder departments
            if (!source.isRecorderDepartmentsEmpty()) {
                for (DepartmentDTO recorder : source.getRecorderDepartments()) {
                    ProgDepProgPriv progDepProgPriv = loadProgDepProgPriv(target, recorder.getId(), ProgramPrivilegeIds.RECORDER);
                    depProgPrivs.put(progDepProgPriv.getProgDepProgPrivPk(), progDepProgPriv);
                }
            }

            // Full viewer user
            if (!source.isFullViewerDepartmentsEmpty()) {
                for (DepartmentDTO fullViewer : source.getFullViewerDepartments()) {
                    ProgDepProgPriv progDepProgPriv = loadProgDepProgPriv(target, fullViewer.getId(), ProgramPrivilegeIds.FULL_VIEWER);
                    depProgPrivs.put(progDepProgPriv.getProgDepProgPrivPk(), progDepProgPriv);
                }
            }

            // Viewer user
            if (!source.isViewerDepartmentsEmpty()) {
                for (DepartmentDTO viewer : source.getViewerDepartments()) {
                    ProgDepProgPriv progDepProgPriv = loadProgDepProgPriv(target, viewer.getId(), ProgramPrivilegeIds.VIEWER);
                    depProgPrivs.put(progDepProgPriv.getProgDepProgPrivPk(), progDepProgPriv);
                }
            }

            // Validator user
            if (!source.isValidatorDepartmentsEmpty()) {
                for (DepartmentDTO validator : source.getValidatorDepartments()) {
                    ProgDepProgPriv progDepProgPriv = loadProgDepProgPriv(target, validator.getId(), ProgramPrivilegeIds.VALIDATOR);
                    depProgPrivs.put(progDepProgPriv.getProgDepProgPrivPk(), progDepProgPriv);
                }
            }

            // Update entity list
            if (MapUtils.isEmpty(depProgPrivs)) {
                if (CollectionUtils.isNotEmpty(target.getProgDepProgPrivs())) {
                    target.getProgDepProgPrivs().clear();
                }
            } else {
                target.getProgDepProgPrivs().clear();
                target.getProgDepProgPrivs().addAll(depProgPrivs.values());
            }
        }

    }

    /**
     * Retrieves the entity object that is associated with the specified value object
     * from the object store. If no such entity object exists in the object store,
     * a new, blank entity is created
     */
    private ProgQuserProgPriv loadProgQuserProgPriv(Program program, int quserId, ProgramPrivilegeIds programPrivilegeId) {
        ProgQuserProgPrivPK pk = new ProgQuserProgPrivPK();

        pk.setProgram((ProgramImpl) program);
        pk.setQuser(load(QuserImpl.class, quserId));
        pk.setProgramPrivilege(load(ProgramPrivilegeImpl.class, programPrivilegeId.getValue()));

        ProgQuserProgPriv target = progQuserProgPrivDao.get(pk);
        if (target == null) {
            target = ProgQuserProgPriv.Factory.newInstance();
            target.setProgQuserProgPrivPk(pk);
        }
        return target;
    }

    private ProgDepProgPriv loadProgDepProgPriv(Program program, int depId, ProgramPrivilegeIds programPrivilegeId) {
        ProgDepProgPrivPK pk = new ProgDepProgPrivPK();

        pk.setProgram((ProgramImpl) program);
        pk.setDepartment(load(DepartmentImpl.class, depId));
        pk.setProgramPrivilege(load(ProgramPrivilegeImpl.class, programPrivilegeId.getValue()));

        ProgDepProgPriv target = progDepProgPrivDao.get(pk);
        if (target == null) {
            target = ProgDepProgPriv.Factory.newInstance();
            target.setProgDepProgPrivPk(pk);
        }
        return target;
    }
}
