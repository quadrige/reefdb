package fr.ifremer.reefdb.dao.administration.strategy;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.reefdb.dto.configuration.programStrategy.*;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * <p>ReefDbStrategyDao interface.</p>
 *
 * @author Ludovic
 */
public interface ReefDbStrategyDao {
    String PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE = "pmfm_strategies_by_program_location_date";

    /**
     * <p>getStrategiesByProgramCode.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<StrategyDTO> getStrategiesByProgramCode(String programCode);

    /**
     * <p>getAppliedStrategiesByProgramCode.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<AppliedStrategyDTO> getAppliedStrategiesByProgramCode(String programCode);

    /**
     * <p>getAppliedStrategiesByStrategyId.</p>
     *
     * @param strategyId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<AppliedStrategyDTO> getAppliedStrategiesByStrategyId(Integer strategyId);

    /**
     * <p>getAppliedPeriodsByStrategyId.</p>
     *
     * @param strategyId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<AppliedStrategyDTO> getAppliedPeriodsByStrategyId(Integer strategyId);

    /**
     * <p>getAllAppliedPeriodsByProgramCode.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @return a {@link com.google.common.collect.Multimap} object.
     */
    Multimap<Integer, AppliedStrategyDTO> getAllAppliedPeriodsByProgramCode(String programCode);

    /**
     * <p>getPmfmsAppliedStrategy.</p>
     *
     * @param strategyId a {@link java.lang.Integer} object.
     * @return a {@link java.util.List} object.
     */
    List<PmfmStrategyDTO> getPmfmsAppliedStrategy(Integer strategyId);

    /**
     * <p>getPmfmStrategiesByProgramCodeAndLocation.</p>
     *
     * @param programCode a {@link String} object.
     * @param monitoringLocationId a {@link Integer} object.
     * @param date a {@link LocalDate} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE)
    Set<PmfmStrategyDTO> getPmfmStrategiesByProgramCodeAndLocation(String programCode, Integer monitoringLocationId, LocalDate date);

    /**
     * Retrieve all PMFM strategies that are applied on given programs and date range
     *
     * @param programCodes program codes
     * @param startDate
     * @param endDate
     * @return set of pmfm strategies
     */
    Set<PmfmStrategyDTO> getPmfmStrategiesByProgramCodesAndDates(Collection<String> programCodes, LocalDate startDate, LocalDate endDate);

    /**
     * Retrieve all strategies that are applied on the given program and monitoring location
     *
     * @param programCode (can be null)
     * @param monitoringLocationId a int.
     * @return a {@link java.util.List} object.
     */
    List<ProgStratDTO> getStrategiesByProgramCodeAndMonitoringLocationId(String programCode, int monitoringLocationId);

    /**
     * Get the unique analysis department for this applied strategy
     *
     * @param appliedStrategyId the applied strategy id
     * @return the analysis department
     */
    DepartmentDTO getAnalysisDepartmentByAppliedStrategyId(int appliedStrategyId);

    /**
     * Get the unique analysis department for this applied strategy and pmfm
     *
     * @param appliedStrategyId the applied strategy id
     * @param pmfmIds
     * @return the analysis department
     */
    DepartmentDTO getAnalysisDepartmentByAppliedStrategyIdAndPmfmIds(int appliedStrategyId, Collection<Integer> pmfmIds);

    /**
     * <p>saveStrategies.</p>
     *
     * @param program a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true)
    })
    void saveStrategies(ProgramDTO program);

    /**
     * <p>removeByProgramCode.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true)
    })
    void removeByProgramCode(String programCode);

    /**
     * <p>removeByStrategyIds.</p>
     *
     * @param strategyIds a {@link java.util.Collection} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true)
    })
    void removeByStrategyIds(Collection<Integer> strategyIds);

    /**
     * <p>deleteAppliedStrategies.</p>
     *
     * @param appliedStrategyIds a {@link java.util.Collection} object.
     */
    @Caching(evict = {
            @CacheEvict(value = ReefDbStrategyDao.PMFM_STRATEGIES_BY_PROG_LOC_DATE_CACHE, allEntries = true)
    })
    void deleteAppliedStrategies(Collection<Integer> appliedStrategyIds);

    /**
     * <p>deleteAppliedStrategies.</p>
     *
     * @param programCode a {@link java.lang.String} object.
     * @param monitoringLocationIds a {@link java.util.Collection} object.
     */
    void deleteAppliedStrategies(String programCode, Collection<Integer> monitoringLocationIds);

    /**
     * <p>saveStrategyByLocation.</p>
     *
     * @param strategy a {@link fr.ifremer.reefdb.dto.configuration.programStrategy.ProgStratDTO} object.
     * @param locationId a int.
     */
    void saveStrategyByLocation(ProgStratDTO strategy, int locationId);

}
