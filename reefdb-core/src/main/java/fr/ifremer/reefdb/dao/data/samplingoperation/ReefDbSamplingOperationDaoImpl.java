package fr.ifremer.reefdb.dao.data.samplingoperation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.administration.program.Program;
import fr.ifremer.quadrige3.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperation;
import fr.ifremer.quadrige3.core.dao.data.samplingoperation.SamplingOperationDaoImpl;
import fr.ifremer.quadrige3.core.dao.data.survey.Survey;
import fr.ifremer.quadrige3.core.dao.data.survey.SurveyImpl;
import fr.ifremer.quadrige3.core.dao.referential.*;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.PositionningSystemImpl;
import fr.ifremer.quadrige3.core.dao.system.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.Daos;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dao.data.measurement.ReefDbMeasurementDao;
import fr.ifremer.reefdb.dao.data.photo.ReefDbPhotoDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.referential.ReefDbSamplingEquipmentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dao.technical.Geometries;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.service.ReefDbDataContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * <p>ReefDbSamplingOperationDaoImpl class.</p>
 *
 * @author Ludovic
 */
@Repository("reefDbSamplingOperationDao")
public class ReefDbSamplingOperationDaoImpl extends SamplingOperationDaoImpl implements ReefDbSamplingOperationDao, InitializingBean {

//    private final static Log LOG = LogFactory.getLog(ReefDbSamplingOperationDaoImpl.class);

    @Resource(name = "reefDbReferentialDao")
    protected ReefDbReferentialDao referentialDao;

    @Resource(name = "reefDbSamplingEquipmentDao")
    protected ReefDbSamplingEquipmentDao samplingEquipmentDao;

    @Resource(name = "reefDbDepartmentDao")
    protected ReefDbDepartmentDao departmentDao;

    @Resource(name = "reefDbMeasurementDao")
    protected ReefDbMeasurementDao measurementDao;

    @Resource(name = "reefDbPhotoDao")
    protected ReefDbPhotoDao photoDao;

    @Resource(name = "reefDbUnitDao")
    protected ReefDbUnitDao unitDao;

    @Resource(name = "reefdbDataContext")
    protected ReefDbDataContext dataContext;

    @Resource(name = "objectTypeDao")
    protected ObjectTypeDao objectTypeDao;

    @Resource(name = "reefDbQuserDao")
    protected ReefDbQuserDao quserDao;

    @Resource(name = "qualificationHistoryDao")
    protected QualificationHistoryDao qualificationHistoryDao;

    @Resource
    protected ReefDbConfiguration config;

    private int unitIdMeter;

    /**
     * <p>Constructor for ReefDbSamplingOperationDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbSamplingOperationDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public void afterPropertiesSet() {
        initConstants();
    }

    private void initConstants() {
        unitIdMeter = UnitId.METER.getValue();
        if (config.isDbCheckConstantsEnable()) {
            Session session = getSessionFactory().openSession();
            try {
                Assert.notNull(session.get(UnitImpl.class, unitIdMeter));
            } finally {
                fr.ifremer.reefdb.dao.technical.Daos.closeSilently(session);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public List<SamplingOperationDTO> getSamplingOperationsBySurveyId(int surveyId, boolean withIndividualMeasurements) {

        Iterator<Object[]> it = queryIterator("samplingOperationsBySurveyId",
                "surveyId", IntegerType.INSTANCE, surveyId);

        List<SamplingOperationDTO> result = Lists.newArrayList();

        while (it.hasNext()) {
            Object[] source = it.next();
            SamplingOperationDTO samplingOperation = toSamplingOperationDTO(Arrays.asList(source).iterator());

            // add measurements
            loadMeasurements(samplingOperation, withIndividualMeasurements);

            result.add(samplingOperation);
        }

        loadGeometries(result);

        return result;
    }

    @Override
    public List<Integer> getSamplingOperationIdsBySurveyId(int surveyId) {
        return queryListTyped("samplingOperationIdsBySurveyId", "surveyId", IntegerType.INSTANCE, surveyId);
    }

    private void loadGeometries(List<SamplingOperationDTO> samplingOperations) {

        Map<Integer, String> geometriesById = new HashMap<>();
        Map<Integer, SamplingOperationDTO> samplingOperationsById = new HashMap<>(samplingOperations.size());
        // Collect sampling operations to load
        samplingOperations.forEach(samplingOperation -> {
            if (samplingOperation.isActualPosition()) {
                samplingOperationsById.put(samplingOperation.getId(), samplingOperation);
            } else {
                // Reset positioning system
                samplingOperation.setPositioning(null);
            }
        });

        List<List<Integer>> partitions = ListUtils.partition(ImmutableList.copyOf(samplingOperationsById.keySet()), 1000);
        partitions.forEach(partition -> {
            Query query = createSQLQuery("samplingOperationGeometries");
            query.setParameterList("samplingOperationIds", partition);
            @SuppressWarnings("unchecked") List<Object[]> rows = query.list();
            if (CollectionUtils.isNotEmpty(rows)) {
                rows.stream()
                    .filter(Objects::nonNull)
                    .forEach(row -> geometriesById.put((Integer) row[0], (String) row[1]));
            }
        });
        // Fill each sampling operation
        geometriesById.forEach((surveyId, geometry) -> samplingOperationsById.get(surveyId).setCoordinate(Geometries.getCoordinate(geometry)));

    }

    /** {@inheritDoc} */
    @Override
    public void saveSamplingOperationsBySurveyId(int surveyId, Collection<SamplingOperationDTO> samplingOperations) {

        Survey survey = load(SurveyImpl.class, surveyId);
        List<Integer> existingSamplingOperationIds = ReefDbBeans.collectProperties(survey.getSamplingOperations(), "samplingOperId");

        if (CollectionUtils.isNotEmpty(samplingOperations)) {
            for (SamplingOperationDTO samplingOperation : samplingOperations) {
                SamplingOperation target;
                if (samplingOperation.getId() == null) {
                    samplingOperation.setDirty(true);
                    target = SamplingOperation.Factory.newInstance();
                } else {
                    target = get(samplingOperation.getId());
                    existingSamplingOperationIds.remove(target.getSamplingOperId());

                    // check for utFormat change
                    if (!Objects.equals(target.getSamplingOperUtFormat(), survey.getSurveyUtFormat())) {
                        samplingOperation.setDirty(true);
                    }

                    // check for geometry change (Mantis #46725)
                    if (!Daos.safeConvertToBoolean(target.getSamplingOperActualPosition())) {
                        samplingOperation.setDirty(true);
                    }
                }

                // save only if dirty
                if (samplingOperation.isDirty()) {

                    beanToEntity(samplingOperation, target, survey);

                    // set surveyId
                    target.setSurvey(survey);

                    getSession().save(target);

                    // save geometry
                    saveGeometry(samplingOperation, target, survey);

                    // update id
                    samplingOperation.setId(target.getSamplingOperId());

                    // save measurements
                    if (samplingOperation.isMeasurementsLoaded()) {
                        List<MeasurementDTO> allMeasurements = Lists.newArrayList();
                        allMeasurements.addAll(samplingOperation.getMeasurements());
                        if (samplingOperation.isIndividualMeasurementsLoaded()) {
                            allMeasurements.addAll(samplingOperation.getIndividualMeasurements());
                        }
                        measurementDao.saveMeasurementsBySamplingOperationId(samplingOperation.getId(), allMeasurements, samplingOperation.isIndividualMeasurementsLoaded());
                    }

                    getSession().flush();
                    getSession().clear();
                    samplingOperation.setDirty(false);
                }
            }
        }

        // remove remaining sampling operations
        if (!existingSamplingOperationIds.isEmpty()) {
            for (Integer samplingOperationId : existingSamplingOperationIds) {
                remove(samplingOperationId);
            }
            getSession().flush();
            getSession().clear();
        }
    }

    private void loadMeasurements(SamplingOperationDTO samplingOperation, boolean withIndividual) {

        List<MeasurementDTO> allMeasurements = measurementDao.getMeasurementsBySamplingOperationId(samplingOperation.getId(), withIndividual);
        if (CollectionUtils.isNotEmpty(allMeasurements)) {

            // Iterate on all measurement to split them
            for (MeasurementDTO measurement : allMeasurements) {

                // link each measurement to sampling operation
                measurement.setSamplingOperation(samplingOperation);

                if (measurement.getIndividualId() == null) {
                    // Add to measurement
                    samplingOperation.addMeasurements(measurement);
                } else if (withIndividual) {
                    // If measurement has an individualId, split to other list
                    samplingOperation.addIndividualMeasurements(measurement);
                }
            }
        }

        samplingOperation.setMeasurementsLoaded(true);
        samplingOperation.setIndividualMeasurementsLoaded(withIndividual);
    }

    /** {@inheritDoc} */
    @Override
    public void removeBySurveyId(int surveyId) {

        // list all sampling operations delete them unitarily
        Iterator<Object[]> it = queryIterator("samplingOperationsBySurveyId",
                "surveyId", IntegerType.INSTANCE, surveyId);

        List<Integer> samplingOperationIds = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();

            // read only the Id
            samplingOperationIds.add((Integer) source[0]);
        }

        for (Integer samplingOperationId : samplingOperationIds) {
            remove(samplingOperationId);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void remove(Integer samplingOperationId) {

        // remove measurement first
        measurementDao.removeAllMeasurementsBySamplingOperationId(samplingOperationId);

        // removeMeasurementsByIds photos
        photoDao.removeBySamplingOperationId(samplingOperationId);

        super.remove(samplingOperationId);
    }

    @Override
    public int validateBySurveyIds(Collection<Integer> surveyIds, Date validationDate) {
        return createQuery("validateSamplingOperationBySurveyIds",
                "validationDate", DateType.INSTANCE, validationDate)
                .setParameterList("surveyIds", surveyIds)
                .executeUpdate();
    }

    @Override
    public int unvalidateBySurveyIds(Collection<Integer> surveyIds, Date unvalidationDate, int validatorId) {

        // Get qualified sampling operations for historisation
        surveyIds.forEach(surveyId -> {
            List<SamplingOperation> qualifiedSamplingOperations = getQualifiedSamplingOperationBySurveyId(surveyId, false);
            if (CollectionUtils.isNotEmpty(qualifiedSamplingOperations)) {
                qualifiedSamplingOperations.forEach(samplingOperation -> {
                    QualificationHistory qualificationHistory = QualificationHistory.Factory.newInstance(String.valueOf(samplingOperation.getSamplingOperId()),
                        objectTypeDao.get(ObjectTypeCode.SAMPLING_OPERATION.getValue()),
                        quserDao.get(validatorId));
                    qualificationHistory.setQualHistOperationCm(t("reefdb.service.observation.qualificationHistory.unqualify.message"));
                    qualificationHistory.setQualityFlag(samplingOperation.getQualityFlag());
                    qualificationHistory.setUpdateDt(new Timestamp(unvalidationDate.getTime()));
                    qualificationHistoryDao.create(qualificationHistory);
                });
            }
        });

        return createQuery("unvalidateSamplingOperationBySurveyIds", "qualFlagCd", StringType.INSTANCE, QualityFlagCode.NOT_QUALIFIED.getValue())
                .setParameterList("surveyIds", surveyIds)
                .executeUpdate();
    }

    @Override
    public List<SamplingOperation> getQualifiedSamplingOperationBySurveyId(int surveyId, boolean withData) {
        List<Integer> samplingOperationIds = getSamplingOperationIdsBySurveyId(surveyId);
        return samplingOperationIds.stream()
            .map(this::get)
            .filter(samplingOperation -> {
                    boolean qualified = !QualityFlagCode.NOT_QUALIFIED.getValue().equals(samplingOperation.getQualityFlag().getQualFlagCd());
                    if (!qualified && withData) {
                        qualified = !measurementDao.getQualifiedMeasurementBySamplingOperationId(samplingOperation.getSamplingOperId()).isEmpty()
                                    || !measurementDao.getQualifiedTaxonMeasurementBySamplingOperationId(samplingOperation.getSamplingOperId()).isEmpty()
                                    || !photoDao.getQualifiedPhotoBySamplingOperationId(samplingOperation.getSamplingOperId()).isEmpty();
                    }
                    return qualified;
                }
            )
            .collect(Collectors.toList());
    }

    @Override
    public void updateSamplingOperationsControlDate(Collection<Integer> controlledElementsPks, Date controlDate) {
        createQuery("updateSamplingOperationControlDate")
                .setParameterList("operationIds", controlledElementsPks)
                .setParameter("controlDate", controlDate).executeUpdate();
//        getSession().flush();
//        getSession().clear();
    }

    // INTERNAL METHODS
    private SamplingOperationDTO toSamplingOperationDTO(Iterator<Object> it) {
        SamplingOperationDTO result = ReefDbBeanFactory.newSamplingOperationDTO();

        result.setId((Integer) it.next());
        result.setName((String) it.next());
        result.setTime(Daos.convertToInteger((Number) it.next()));
        result.setComment((String) it.next());
        result.setControlDate(Daos.convertToDate(it.next()));
        result.setSamplingEquipment(samplingEquipmentDao.getSamplingEquipmentById((int) it.next()));
        result.setSamplingDepartment(departmentDao.getDepartmentById((int) it.next()));
        result.setSize(Daos.convertToDouble((Float) it.next()));

        // optional
        Integer unitId = (Integer) it.next();
        if (unitId != null) {
            result.setSizeUnit(unitDao.getUnitById(unitId));
        }
        Integer depthLevelId = (Integer) it.next();
        if (depthLevelId != null) {
            result.setDepthLevel(referentialDao.getDepthLevelById(depthLevelId));
        }
        result.setDepth(Daos.convertToDouble((Float) it.next()));
        result.setMaxDepth(Daos.convertToDouble((Float) it.next()));
        result.setMinDepth(Daos.convertToDouble((Float) it.next()));
        result.setIndividualCount(Daos.convertToInteger((Number) it.next()));

        // Get positioning system
        Integer posSystemId = (Integer) it.next();
        if (posSystemId != null) {
            result.setPositioning(referentialDao.getPositioningSystemById(posSystemId));
        }

        // is actual position ?
        result.setActualPosition(Daos.safeConvertToBoolean(it.next(), false));

        return result;
    }

    private void beanToEntity(SamplingOperationDTO bean, SamplingOperation entity, Survey survey) {

        entity.setSamplingOperLb(bean.getName());
        entity.setSamplingOperTime(bean.getTime());
        entity.setSamplingOperUtFormat(survey.getSurveyUtFormat());
        entity.setSamplingOperCm(bean.getComment());
        entity.setSamplingOperControlDt(bean.getControlDate());
        entity.setSamplingEquipment(load(SamplingEquipmentImpl.class, bean.getSamplingEquipment().getId()));
        entity.setDepartment(load(DepartmentImpl.class, bean.getSamplingDepartment().getId()));

        // Recorder Department (Mantis #42615 Only if REC_DEP_ID is null)
        if (entity.getRecorderDepartment() == null) {
            entity.setRecorderDepartment(load(DepartmentImpl.class, dataContext.getRecorderDepartmentId()));
        }

        if (entity.getQualityFlag() == null) {
            entity.setQualityFlag(getDefaultQualityFlag());
        }
        if (CollectionUtils.isNotEmpty(survey.getPrograms())) {
            for (Program program: survey.getPrograms()) {
                entity.addPrograms(program);
            }
        }

        entity.setSamplingOperSize(bean.getSize() == null ? null : bean.getSize().floatValue());

        if (bean.getSizeUnit() == null) {
            entity.setSizeUnit(null);
        } else {
            entity.setSizeUnit(load(UnitImpl.class, bean.getSizeUnit().getId()));
        }

        if (bean.getDepthLevel() == null) {
            entity.setDepthLevel(null);
        } else {
            entity.setDepthLevel(load(DepthLevelImpl.class, bean.getDepthLevel().getId()));
        }
        entity.setSamplingOperDepth(bean.getDepth() == null ? null : bean.getDepth().floatValue());
        entity.setSamplingOperMaxDepth(bean.getMaxDepth() == null ? null : bean.getMaxDepth().floatValue());
        entity.setSamplingOperMinDepth(bean.getMinDepth() == null ? null : bean.getMinDepth().floatValue());
        // set depth unit if at least one depth not null (Mantis #45752)
        if (entity.getSamplingOperDepth() != null || entity.getSamplingOperMinDepth() != null || entity.getSamplingOperMaxDepth() != null) {
            entity.setDepthUnit(load(UnitImpl.class, unitIdMeter));
        } else {
            entity.setDepthUnit(null);
        }

        entity.setSamplingOperNumberIndiv(bean.getIndividualCount());

        if (bean.getPositioning() == null) {
            entity.setPositionningSystem(null);
        } else {
            entity.setPositionningSystem(load(PositionningSystemImpl.class, bean.getPositioning().getId()));
        }

    }

    private void saveGeometry(SamplingOperationDTO bean, SamplingOperation entity, Survey survey) {

        // If coordinates has been filled by user
        if (Geometries.isValid(bean.getCoordinate())) {

            if (Geometries.isPoint(bean.getCoordinate())) {

                // point
                if (entity.getSamplingOperPoints() != null && entity.getSamplingOperPoints().size() == 1) {

                    SamplingOperPoint pointToUpdate = entity.getSamplingOperPoints().iterator().next();
                    if (!Geometries.equals(bean.getCoordinate(), pointToUpdate.getSamplingOperPosition())) {
                        pointToUpdate.setSamplingOperPosition(Geometries.getPointPosition(bean.getCoordinate()));
                        getSession().update(pointToUpdate);
                    }

                } else {
                    // create a sampling operation point
                    SamplingOperPoint point = SamplingOperPoint.Factory.newInstance();
                    point.setSamplingOperation(entity);
                    point.setSamplingOperPosition(Geometries.getPointPosition(bean.getCoordinate()));
                    getSession().save(point);
                    entity.getSamplingOperPoints().clear();
                    entity.addSamplingOperPoints(point);
                }
                entity.getSamplingOperAreas().clear();

            } else {

                // area
                if (entity.getSamplingOperAreas() != null && entity.getSamplingOperAreas().size() == 1) {

                    SamplingOperArea areaToUpdate = entity.getSamplingOperAreas().iterator().next();
                    if (!Geometries.equals(bean.getCoordinate(), areaToUpdate.getSamplingOperPosition())) {
                        areaToUpdate.setSamplingOperPosition(Geometries.getAreaPosition(bean.getCoordinate()));
                        getSession().update(areaToUpdate);
                    }

                } else {
                    // create a sampling operation area
                    SamplingOperArea area = SamplingOperArea.Factory.newInstance();
                    area.setSamplingOperation(entity);
                    area.setSamplingOperPosition(Geometries.getAreaPosition(bean.getCoordinate()));
                    getSession().save(area);
                    entity.getSamplingOperAreas().clear();
                    entity.addSamplingOperAreas(area);
                }
                entity.getSamplingOperPoints().clear();

            }

            // clear unused coordinates
            entity.getSamplingOperLines().clear();

            // Update flag to known is coordinate are filled by user or not (mantis #28257)
            entity.setSamplingOperActualPosition(fr.ifremer.reefdb.dao.technical.Daos.convertToString(true));

        } else {
            saveGeometryFromSurvey(entity, survey);
        }



        update(entity);

    }

    private void saveGeometryFromSurvey(SamplingOperation entity, Survey survey) {

        // Point geometry
        if (CollectionUtils.size(survey.getSurveyPoints()) == 1) {
            SurveyPoint sourcePoint = CollectionUtils.extractSingleton(survey.getSurveyPoints());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSamplingOperPoints()) == 1) {
                SamplingOperPoint targetPoint = CollectionUtils.extractSingleton(entity.getSamplingOperPoints());
                if (!Objects.equals(targetPoint.getSamplingOperPosition(), sourcePoint.getSurveyPosition())) {
                    targetPoint.setSamplingOperPosition(sourcePoint.getSurveyPosition());
                    getSession().update(targetPoint);
                }
            }

            // No existing area: create new
            else {
                SamplingOperPoint targetPoint = SamplingOperPoint.Factory.newInstance();
                targetPoint.setSamplingOperation(entity);
                targetPoint.setSamplingOperPosition(sourcePoint.getSurveyPosition());
                getSession().save(targetPoint);
                entity.getSamplingOperPoints().clear();
                entity.addSamplingOperPoints(targetPoint);
            }

            // Clean unused
            entity.getSamplingOperLines().clear();
            entity.getSamplingOperAreas().clear();
        }

        // Line geometry
        else if (CollectionUtils.size(survey.getSurveyLines()) == 1) {
            SurveyLine sourceLine = CollectionUtils.extractSingleton(survey.getSurveyLines());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSamplingOperLines()) == 1) {
                SamplingOperLine targetLine = CollectionUtils.extractSingleton(entity.getSamplingOperLines());
                if (!Objects.equals(sourceLine.getSurveyPosition(), targetLine.getSamplingOperPosition())) {
                    targetLine.setSamplingOperPosition(sourceLine.getSurveyPosition());
                    getSession().update(targetLine);
                }
            }

            // No existing area: create new
            else {
                SamplingOperLine targetLine = SamplingOperLine.Factory.newInstance();
                targetLine.setSamplingOperation(entity);
                targetLine.setSamplingOperPosition(sourceLine.getSurveyPosition());
                getSession().save(targetLine);
                entity.getSamplingOperLines().clear();
                entity.addSamplingOperLines(targetLine);
            }

            // Clean unused
            entity.getSamplingOperPoints().clear();
            entity.getSamplingOperAreas().clear();
        }

        // Area geometry
        else if (CollectionUtils.size(survey.getSurveyAreas()) == 1) {
            SurveyArea sourceArea = CollectionUtils.extractSingleton(survey.getSurveyAreas());

            // Survey has already a area, so update it
            if (CollectionUtils.size(entity.getSamplingOperAreas()) == 1) {
                SamplingOperArea targetArea = CollectionUtils.extractSingleton(entity.getSamplingOperAreas());
                if (!Objects.equals(sourceArea.getSurveyPosition(), targetArea.getSamplingOperPosition())) {
                    targetArea.setSamplingOperPosition(sourceArea.getSurveyPosition());
                    getSession().update(targetArea);
                }
            }

            // No existing area: create new
            else {
                SamplingOperArea targetArea = SamplingOperArea.Factory.newInstance();
                targetArea.setSamplingOperation(entity);
                targetArea.setSamplingOperPosition(sourceArea.getSurveyPosition());
                getSession().save(targetArea);
                entity.getSamplingOperAreas().clear();
                entity.addSamplingOperAreas(targetArea);
            }

            // Clean unused
            entity.getSamplingOperPoints().clear();
            entity.getSamplingOperLines().clear();
        }
        else {
            throw new DataIntegrityViolationException(String.format("Could not found geometry on survey [surveyId=%s].", survey.getSurveyId()));
        }

        // Update flag to known is coordinate is not filled by user or not (mantis #28257)
        entity.setSamplingOperActualPosition(fr.ifremer.reefdb.dao.technical.Daos.convertToString(false));

        // Apply positioning system from survey (see mantis #28706)
        entity.setPositionningSystem(survey.getPositionningSystem());
    }

    /**
     * return the default quality flag
     *
     * @return the default quality flag
     */
    private QualityFlag getDefaultQualityFlag() {
        return load(QualityFlagImpl.class, QualityFlagCode.NOT_QUALIFIED.getValue()); // = non qualifié
    }

}
