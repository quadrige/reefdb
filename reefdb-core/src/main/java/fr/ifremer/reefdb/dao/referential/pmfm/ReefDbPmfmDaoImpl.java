package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.UnitImpl;
import fr.ifremer.quadrige3.core.dao.referential.pmfm.*;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dao.referential.transcribing.ReefDbTranscribingItemDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.FractionDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.MatrixDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>ReefDbPmfmDaoImpl class.</p>
 *
 */
@Repository("reefDbPmfmDao")
public class ReefDbPmfmDaoImpl extends PmfmDaoImpl implements ReefDbPmfmDao {

    private static final Log log = LogFactory.getLog(ReefDbPmfmDaoImpl.class);

    private static final Multimap<String, String> columnNamesByProgramTableNames = ImmutableListMultimap.<String, String>builder()
            .put("PMFM_STRATEGY", "PMFM_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("MEASUREMENT", "PMFM_ID")
            .put("MEASUREMENT_FILE", "PMFM_ID")
            .put("TAXON_MEASUREMENT", "PMFM_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("MEASUREMENT", "MEAS_VALID_DT")
            .put("MEASUREMENT_FILE", "MEAS_FILE_VALID_DT")
            .put("TAXON_MEASUREMENT", "TAXON_MEAS_VALID_DT").build();

    @Resource
    protected CacheService cacheService;

    @Resource
    protected ReefDbConfiguration config;

    @Resource(name = "reefDbParameterDao")
    protected ReefDbParameterDao parameterDao;

    @Resource(name = "reefDbMethodDao")
    protected ReefDbMethodDao methodDao;

    @Resource(name = "reefDbFractionDao")
    protected ReefDbFractionDao fractionDao;

    @Resource(name = "reefDbMatrixDao")
    protected ReefDbMatrixDao matrixDao;

    @Resource(name = "reefDbUnitDao")
    protected ReefDbUnitDao unitDao;

    @Resource(name = "reefDbTranscribingItemDao")
    protected ReefDbTranscribingItemDao transcribingItemDao;

    @Resource(name = "reefDbQualitativeValueDao")
    protected ReefDbQualitativeValueDao qualitativeValueDao;

    /**
     * <p>Constructor for ReefDbPmfmDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbPmfmDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmDTO> getAllPmfms(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(PMFM_BY_ID_CACHE);

        // Read all transcribing names
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allPmfms"), statusCodes);

        List<PmfmDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();

            // Create the VO
            PmfmDTO pmfm = toPmfmDTO(Arrays.asList(source).iterator(), transcribingNamesById);

            result.add(pmfm);
            cacheById.put(pmfm.getId(), pmfm);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public List<PmfmDTO> findPmfms(String parameterCode, Integer matrixId, Integer fractionId, Integer methodId, Integer unitId, String pmfmName, List<String> statusCodes) {
        // Read all transcribing names
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Query query = createQuery("pmfmsByCriteria",
                "parameterCode", StringType.INSTANCE, parameterCode,
                "matrixId", IntegerType.INSTANCE, matrixId,
                "fractionId", IntegerType.INSTANCE, fractionId,
                "methodId", IntegerType.INSTANCE, methodId,
                "unitId", IntegerType.INSTANCE, unitId,
                "pmfmName", StringType.INSTANCE, pmfmName);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(query, statusCodes);

        // other solution: use criteria API and findByExample
        List<PmfmDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();

            // Create the VO
            PmfmDTO pmfm = toPmfmDTO(Arrays.asList(source).iterator(), transcribingNamesById);
            result.add(pmfm);
        }

        return ImmutableList.copyOf(result);
    }

    /** {@inheritDoc} */
    @Override
    public void savePmfms(List<? extends PmfmDTO> pmfms) {
        if (CollectionUtils.isEmpty(pmfms)) {
            return;
        }

        for (PmfmDTO pmfm : pmfms) {
            if (pmfm.isDirty()) {
                savePmfm(pmfm);
                pmfm.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void deletePmfms(List<Integer> pmfmIds) {
        if (pmfmIds == null) return;
        pmfmIds.stream().filter(Objects::nonNull).distinct().forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public void replaceTemporaryPmfm(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmUsedInData(int pmfmId) {

        return executeMultipleCount(columnNamesByDataTableNames, pmfmId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmUsedInValidatedData(int pmfmId) {

        return executeMultipleCountWithNotNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, pmfmId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmUsedInProgram(int pmfmId) {

        return executeMultipleCount(columnNamesByProgramTableNames, pmfmId);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isPmfmUsedInRules(int pmfmId) {

        return queryCount("countRulePmfmByPmfmId", "pmfmId", IntegerType.INSTANCE, pmfmId) > 0;
    }

    /** {@inheritDoc} */
    @Override
    public PmfmDTO getPmfmById(int pmfmId) {
        Object[] source = queryUnique("pmfmById",
                "pmfmId", IntegerType.INSTANCE, pmfmId);

        if (source == null) {
            throw new DataRetrievalFailureException("can't load pmfm with id = " + pmfmId);
        }

        return toPmfmDTO(Arrays.asList(source).iterator(), getTranscribingNames());
    }

    /** {@inheritDoc} */
    @Override
    @SuppressWarnings("unchecked")
    public List<PmfmDTO> getPmfmsByIds(List<Integer> pmfmIds) {
        // Read all transcribing names
        Map<Integer, String> transcribingNamesById = getTranscribingNames();

        Iterator<Object[]> it = createQuery("pmfmByIds")
                .setParameterList("pmfmIds", pmfmIds)
                .iterate();

        List<PmfmDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();

            // Create the VO
            result.add(toPmfmDTO(Arrays.asList(source).iterator(), transcribingNamesById));
        }

        return result;
    }

    // INTERNAL METHODS

    private Map<Integer, String> getTranscribingNames() {
        return transcribingItemDao.getAllTranscribingItemsById(config.getTranscribingItemTypeLbForPmfmNm());
    }

    private void savePmfm(PmfmDTO pmfm) {
        Assert.notNull(pmfm);
        Assert.notNull(pmfm.getParameter());
        Assert.notNull(pmfm.getMatrix());
        Assert.notNull(pmfm.getFraction());
        Assert.notNull(pmfm.getMethod());
        Assert.notNull(pmfm.getUnit());

        if (pmfm.getStatus() == null) {
            pmfm.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(pmfm.getStatus()), "source must have local status");

        Pmfm target;
        if (pmfm.getId() == null) {
            target = Pmfm.Factory.newInstance();
            target.setPmfmId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(pmfm.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        target.setStatus(load(StatusImpl.class, pmfm.getStatus().getCode()));
        target.setParameter(load(ParameterImpl.class, pmfm.getParameter().getCode()));
        target.setMatrix(load(MatrixImpl.class, pmfm.getMatrix().getId()));
        target.setFraction(load(FractionImpl.class, pmfm.getFraction().getId()));
        target.setMethod(load(MethodImpl.class, pmfm.getMethod().getId()));
        target.setUnit(load(UnitImpl.class, pmfm.getUnit().getId()));

        target.setUpdateDt(newUpdateTimestamp());

        getSession().save(target);
        pmfm.setId(target.getPmfmId());

        // qualitative values
        if (pmfm.sizeQualitativeValues() > 0) {

            Map<Integer, PmfmQualValue> remainingPQV = ReefDbBeans.mapByProperty(target.getPmfmQualValues(), "pmfmQualValuePk.qualitativeValue.qualValueId");
            for (QualitativeValueDTO qualitativeValue : pmfm.getQualitativeValues()) {
                if (remainingPQV.remove(qualitativeValue.getId()) == null) {
                    // add PmfmQualValue
                    QualitativeValue targetQV = load(QualitativeValueImpl.class, qualitativeValue.getId());
                    PmfmQualValue pmfmQualValue = PmfmQualValue.Factory.newInstance();
                    pmfmQualValue.setPmfm(target);
                    pmfmQualValue.setQualitativeValue(targetQV);
                    PmfmQualValuePK pmfmQualValuePK = new PmfmQualValuePK((QualitativeValueImpl) targetQV, (PmfmImpl) target);
                    pmfmQualValue.setPmfmQualValuePk(pmfmQualValuePK);
                    target.addPmfmQualValues(pmfmQualValue);
                }

            }

            if (!remainingPQV.isEmpty()) {
                target.getPmfmQualValues().removeAll(remainingPQV.values());
            }

        } else if (target.getPmfmQualValues() != null) {
            target.getPmfmQualValues().clear();
        }

        getSession().save(target);
    }

    private PmfmDTO toPmfmDTO(Iterator<Object> source, Map<Integer, String> transcribingNamesById) {
        PmfmDTO result = ReefDbBeanFactory.newPmfmDTO();

        result.setId((Integer) source.next());
        result.setStatus(Daos.getStatus((String) source.next()));

        // parameter by cache
        result.setParameter(parameterDao.getParameterByCode((String) source.next()));

        // matrix by cache
        MatrixDTO matrix = ReefDbBeans.clone(matrixDao.getMatrixById((int) source.next()));
        // remove some redundant data
        matrix.setFractions(null);
        result.setMatrix(matrix);

        // fraction by cache
        FractionDTO fraction = ReefDbBeans.clone(fractionDao.getFractionById((int) source.next()));
        // remove some redundant data
        fraction.setMatrixes(null);
        result.setFraction(fraction);

        // method by cache
        result.setMethod(methodDao.getMethodById((int) source.next()));

        // unit by cache
        result.setUnit(unitDao.getUnitById((int) source.next()));

        result.setComment((String) source.next());
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));

        // name
        if (transcribingNamesById != null) {
            result.setName(transcribingNamesById.get(result.getId()));
        }

        // QV
        result.setQualitativeValues(qualitativeValueDao.getQualitativeValuesByPmfmId(result.getId()));

        return result;
    }

}
