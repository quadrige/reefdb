package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.Collection;
import java.util.List;

/**
 * <p>ReefDbTaxonGroupDao interface.</p>
 *
 */
public interface ReefDbTaxonGroupDao {

    String ALL_TAXON_GROUPS_CACHE = "all_taxon_groups";
    String TAXON_GROUP_BY_ID_CACHE = "taxon_group_by_id";
    String TAXON_GROUPS_BY_IDS_CACHE = "taxon_groups_by_ids";

    /**
     * <p>getAllTaxonGroups.</p>
     *
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_TAXON_GROUPS_CACHE)
    List<TaxonGroupDTO> getAllTaxonGroups();

    /**
     * <p>getTaxonGroupById.</p>
     *
     * @param taxonGroupId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.TaxonGroupDTO} object.
     */
    @Cacheable(value = TAXON_GROUP_BY_ID_CACHE)
    TaxonGroupDTO getTaxonGroupById(int taxonGroupId);

    /**
     * <p>getTaxonGroupsByIds.</p>
     *
     * @param taxonGroupIds a {@link List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = TAXON_GROUPS_BY_IDS_CACHE)
    List<TaxonGroupDTO> getTaxonGroupsByIds(Collection<Integer> taxonGroupIds);

    /**
     * <p>findTaxonGroups.</p>
     *
     * @param parentTaxonGroupId a {@link java.lang.Integer} object.
     * @param label a {@link java.lang.String} object.
     * @param name a {@link java.lang.String} object.
     * @param isStrictName a boolean.
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    List<TaxonGroupDTO> findTaxonGroups(Integer parentTaxonGroupId, String label, String name, boolean isStrictName, List<String> statusCodes);

    /**
     * <p>saveTaxonGroups.</p>
     *
     * @param taxonGroups a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_TAXON_GROUPS_CACHE,
            TAXON_GROUP_BY_ID_CACHE,
            TAXON_GROUPS_BY_IDS_CACHE
    }, allEntries = true)
    void saveTaxonGroups(List<? extends TaxonGroupDTO> taxonGroups);

    /**
     * <p>deleteTaxonGroups.</p>
     *
     * @param taxonGroupIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_TAXON_GROUPS_CACHE,
            TAXON_GROUP_BY_ID_CACHE,
            TAXON_GROUPS_BY_IDS_CACHE
    }, allEntries = true)
    void deleteTaxonGroups(List<Integer> taxonGroupIds);

    /**
     * <p>replaceTemporaryTaxonGroup.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete a boolean.
     */
    @CacheEvict(value = {
            ALL_TAXON_GROUPS_CACHE,
            TAXON_GROUP_BY_ID_CACHE,
            TAXON_GROUPS_BY_IDS_CACHE
    }, allEntries = true)
    void replaceTemporaryTaxonGroup(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isTaxonGroupUsedInReferential.</p>
     *
     * @param taxonGroupId a int.
     * @return a boolean.
     */
    boolean isTaxonGroupUsedInReferential(int taxonGroupId);

    /**
     * <p>isTaxonGroupUsedInData.</p>
     *
     * @param taxonGroupId a int.
     * @return a boolean.
     */
    boolean isTaxonGroupUsedInData(int taxonGroupId);

    /**
     * <p>isTaxonGroupUsedInValidatedData.</p>
     *
     * @param taxonGroupId a int.
     * @return a boolean.
     */
    boolean isTaxonGroupUsedInValidatedData(int taxonGroupId);
}
