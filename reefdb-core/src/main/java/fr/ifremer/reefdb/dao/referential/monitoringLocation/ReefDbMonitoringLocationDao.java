package fr.ifremer.reefdb.dao.referential.monitoringLocation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.referential.HarbourDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Map;

/**
 * <p>ReefDbMonitoringLocationDao interface.</p>
 */
public interface ReefDbMonitoringLocationDao {

    String ALL_LOCATIONS_CACHE = "all_locations";
    String LOCATION_BY_ID_CACHE = "location_by_id";
    String LOCATIONS_BY_IDS_CACHE = "locations_by_ids";
    String LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE = "locations_by_campaign_and_program";
    String ALL_HARBOURS_CACHE = "all_harbours";
    String ALL_COORDINATES_CACHE = "all_coordinates";
    String COORDINATE_BY_LOCATION_ID_CACHE = "coordinate_by_location_id";

    /**
     * <p>getAllLocations.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_LOCATIONS_CACHE)
    List<LocationDTO> getAllLocations(List<String> statusCodes);

    /**
     * <p>getLocationById.</p>
     *
     * @param locationId a int.
     * @return a {@link fr.ifremer.reefdb.dto.referential.LocationDTO} object.
     */
    @Cacheable(value = LOCATION_BY_ID_CACHE)
    LocationDTO getLocationById(int locationId);

    /**
     * <p>getLocationsByIds.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = LOCATIONS_BY_IDS_CACHE)
    List<LocationDTO> getLocationsByIds(List<Integer> locationIds);

    /**
     * <p>getLocationsByCampaignAndProgram.</p>
     *
     * @param campaignId   a {@link Integer} object.
     * @param programCode  a {@link String} object.
     * @param statusFilter
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE)
    List<LocationDTO> getLocationsByCampaignAndProgram(Integer campaignId, String programCode, StatusFilter statusFilter);

    /**
     * <p>findLocations.</p>
     *
     * @param statusCodes       a {@link java.util.List} object.
     * @param orderItemTypeCode a {@link java.lang.String} object.
     * @param orderItemId       a {@link java.lang.Integer} object.
     * @param programCode       a {@link java.lang.String} object.
     * @param label             a {@link java.lang.String} object.
     * @param name              a {@link java.lang.String} object.
     * @param isStrictName      a boolean.
     * @return a {@link java.util.List} object.
     */
    List<LocationDTO> findLocations(List<String> statusCodes, String orderItemTypeCode, Integer orderItemId, String programCode, String label, String name, boolean isStrictName);

    /**
     * <p>saveLocations.</p>
     *
     * @param locations a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_LOCATIONS_CACHE,
            LOCATIONS_BY_IDS_CACHE,
            LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE
    }, allEntries = true)
    void saveLocations(List<? extends LocationDTO> locations);

    /**
     * <p>deleteLocations.</p>
     *
     * @param locationIds a {@link java.util.List} object.
     */
    @CacheEvict(value = {
            ALL_LOCATIONS_CACHE,
            LOCATIONS_BY_IDS_CACHE,
            LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE
    }, allEntries = true)
    void deleteLocations(List<Integer> locationIds);

    /**
     * <p>replaceTemporaryLocation.</p>
     *
     * @param sourceId a {@link java.lang.Integer} object.
     * @param targetId a {@link java.lang.Integer} object.
     * @param delete   a boolean.
     */
    @CacheEvict(value = {
            ALL_LOCATIONS_CACHE,
            LOCATIONS_BY_IDS_CACHE,
            LOCATIONS_BY_CAMPAIGN_AND_PROGRAM_CACHE
    }, allEntries = true)
    void replaceTemporaryLocation(Integer sourceId, Integer targetId, boolean delete);

    /**
     * <p>isLocationUsedInProgram.</p>
     *
     * @param locationId a int.
     * @return a boolean.
     */
    boolean isLocationUsedInProgram(int locationId);

    /**
     * <p>isLocationUsedInData.</p>
     *
     * @param locationId a int.
     * @return a boolean.
     */
    boolean isLocationUsedInData(int locationId);

    /**
     * <p>isLocationUsedInValidatedData.</p>
     *
     * @param locationId a int.
     * @return a boolean.
     */
    boolean isLocationUsedInValidatedData(int locationId);

    /**
     * <p>getAllHarbours.</p>
     *
     * @param statusCodes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    @Cacheable(value = ALL_HARBOURS_CACHE)
    List<HarbourDTO> getAllHarbours(List<String> statusCodes);

    /**
     * Populate all geometry coordinates of all monitoring locations
     * Used to fill COORDINATE_BY_LOCATION_ID_CACHE
     *
     * @return the map of all coordinates by monitoring location id
     */
    @Cacheable(value = ALL_COORDINATES_CACHE)
    Map<Integer, CoordinateDTO> getAllCoordinates();

    /**
     * Get the position (geometry) of the monitoring location
     *
     * @param locationId monitoring location id
     * @return the position
     */
    @Cacheable(value = COORDINATE_BY_LOCATION_ID_CACHE)
    CoordinateDTO getCoordinateByLocationId(int locationId);
}
