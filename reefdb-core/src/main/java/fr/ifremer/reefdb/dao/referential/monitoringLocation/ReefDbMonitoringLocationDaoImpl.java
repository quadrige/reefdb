package fr.ifremer.reefdb.dao.referential.monitoringLocation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.*;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.dao.referential.StatusImpl;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.HarbourImpl;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocation;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.MonitoringLocationDaoImpl;
import fr.ifremer.quadrige3.core.dao.referential.monitoringLocation.PositionningSystemImpl;
import fr.ifremer.quadrige3.core.dao.system.MonLocArea;
import fr.ifremer.quadrige3.core.dao.system.MonLocPoint;
import fr.ifremer.quadrige3.core.dao.technical.Assert;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dao.technical.Geometries;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.HarbourDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>ReefDbMonitoringLocationDaoImpl class.</p>
 */
@Repository("reefDbMonitoringLocationDao")
public class ReefDbMonitoringLocationDaoImpl extends MonitoringLocationDaoImpl implements ReefDbMonitoringLocationDao {

    /**
     * Logger.
     */
//    private static final Log log = LogFactory.getLog(ReefDbMonitoringLocationDaoImpl.class);

    private static final Multimap<String, String> columnNamesByProgramTableNames = ImmutableListMultimap.<String, String>builder()
            .put("APPLIED_STRATEGY", "MON_LOC_ID")
            .put("MON_LOC_PROG", "MON_LOC_ID").build();

    private static final Multimap<String, String> columnNamesByDataTableNames = ImmutableListMultimap.<String, String>builder()
            .put("SURVEY", "MON_LOC_ID").build();

    private static final Map<String, String> validationDateColumnNameByDataTableNames = ImmutableMap.<String, String>builder()
            .put("SURVEY", "SURVEY_VALID_DT").build();

    @Resource
    protected ReefDbConfiguration config;

    @Resource
    protected CacheService cacheService;

    @Resource(name = "reefDbReferentialDao")
    protected ReefDbReferentialDao referentialDao;

    @Resource(name = "reefDbMonitoringLocationDao")
    protected ReefDbMonitoringLocationDao loopBackService;

    /**
     * <p>Constructor for ReefDbMonitoringLocationDaoImpl.</p>
     *
     * @param sessionFactory a {@link org.hibernate.SessionFactory} object.
     */
    @Autowired
    public ReefDbMonitoringLocationDaoImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getLocationsByCampaignAndProgram(Integer campaignId, String programCode, StatusFilter statusFilter) {

        Iterator<Object[]> it;
        if (campaignId == null) {
            it = Daos.queryIteratorWithStatus(
                createQuery("monitoringLocationsByProgramCode", "programCode", StringType.INSTANCE, programCode),
                statusFilter.toStatusCodes()
            );
        } else {
            it = Daos.queryIteratorWithStatus(
                createQuery("monitoringLocationsByCampaignIdAndProgramCode",
                    "programCode", StringType.INSTANCE, programCode,
                    "campaignId", IntegerType.INSTANCE, campaignId),
                statusFilter.toStatusCodes()
            );
        }

        List<LocationDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toLocationDTO(Arrays.asList(row).iterator()));
        }

        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<LocationDTO> getLocationsByIds(List<Integer> locationIds) {

        Iterator<Object[]> it = createQuery("monitoringLocationsByIds")
                .setParameterList("locationIds", locationIds)
                .iterate();

        List<LocationDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toLocationDTO(Arrays.asList(row).iterator()));
        }

        return Collections.unmodifiableList(result);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> getAllLocations(List<String> statusCodes) {

        Cache cacheById = cacheService.getCache(LOCATION_BY_ID_CACHE);

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allMonitoringLocations"), statusCodes);

        List<LocationDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            LocationDTO location = toLocationDTO(Arrays.asList(row).iterator());
            result.add(location);
            cacheById.put(location.getId(), location);
        }

        return ImmutableList.copyOf(result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocationDTO getLocationById(int locationId) {

        Object[] row = queryUnique("monitoringLocationById", "monitoringLocationId", IntegerType.INSTANCE, locationId);

        if (row == null) {
            throw new DataRetrievalFailureException("can't load monitoring location with id = " + locationId);
        }

        return toLocationDTO(Arrays.asList(row).iterator());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LocationDTO> findLocations(List<String> statusCodes, String orderItemTypeCode, Integer orderItemId, String programCode, String label, String name, boolean isStrictName) {

        StringBuilder queryString = new StringBuilder(createQuery("allMonitoringLocations").getQueryString());

        // filter on label (=monLocNm)
//        if (StringUtils.isNotBlank(label)) {
//            queryString.append(System.lineSeparator());
//            queryString.append("AND ml.monLocNm");
//            if (label.contains("*")) {
//                label = label.replace('*', '%');
//                queryString.append(" LIKE ");
//            }
//            else {
//                queryString.append(" = ");
//            }
//            queryString.append('\'').append(label).append('\'');
//        }
        // filter on identifier (=monLocLb)
//        if (StringUtils.isNotBlank(identifier)) {
//            queryString.append(System.lineSeparator());
//            queryString.append("AND ml.monLocLb");
//            if (identifier.contains("*")) {
//                identifier = identifier.replace('*', '%');
//                queryString.append(" LIKE ");
//            }
//            else {
//                queryString.append(" = ");
//            }
//            queryString.append('\'').append(identifier).append('\'');
//        }
        // filter on grouping type
        if (orderItemTypeCode != null || orderItemId != null) {
            queryString.append(System.lineSeparator());
            queryString.append("AND ml.monLocId IN (SELECT mloi.monLocOrderItemPk.monitoringLocation.monLocId ");
            queryString.append("FROM MonLocOrderItemImpl mloi WHERE ");
            if (orderItemTypeCode != null) {
                queryString.append("mloi.monLocOrderItemPk.orderItem.orderItemTypeCd.orderItemTypeCd = ");
                queryString.append("\'").append(orderItemTypeCode).append("\'");
                if (orderItemId != null) {
                    queryString.append(" AND ");
                }
            }
            if (orderItemId != null) {
                queryString.append("mloi.monLocOrderItemPk.orderItem.orderItemId = ");
                queryString.append(orderItemId);
            }
            queryString.append(")");
        }

        // filter on program
        if (programCode != null) {
            queryString.append(System.lineSeparator());
            queryString.append("AND ml.monLocId IN (SELECT mp.monitoringLocation.monLocId FROM MonLocProgImpl mp WHERE mp.program.progCd = ");
            queryString.append("\'").append(programCode).append("\'");
            queryString.append(")");
        }

        // filter on label
        if (label != null) {
            queryString.append(System.lineSeparator());
            queryString.append("AND lower(ml.monLocLb) like lower(\'%");
            queryString.append(label);
            queryString.append("%\')");
        }

        // filter on name
        if (name != null) {
            queryString.append(System.lineSeparator());
            if (isStrictName) {
                queryString.append("AND lower(ml.monLocNm) = lower(\'");
                queryString.append(name);
                queryString.append("\')");
            } else {
                queryString.append("AND lower(ml.monLocNm) like lower(\'%");
                queryString.append(name);
                queryString.append("%\')");
            }
        }

        // build new query
        Iterator<Object[]> it = Daos.queryIteratorWithStatus(getSession().createQuery(queryString.toString()), statusCodes);

        List<LocationDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] source = it.next();
            result.add(toLocationDTO(Arrays.asList(source).iterator()));
        }

        return ImmutableList.copyOf(result);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveLocations(List<? extends LocationDTO> locations) {
        if (CollectionUtils.isEmpty(locations)) {
            return;
        }

        for (LocationDTO location : locations) {
            if (location.isDirty()) {
                saveLocation(location);
                location.setDirty(false);
            }
        }
        getSession().flush();
        getSession().clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteLocations(List<Integer> locationIds) {
        if (locationIds == null) return;
        Set<Integer> idsToRemove = locationIds.stream().filter(Objects::nonNull).collect(Collectors.toSet());
        idsToRemove.forEach(this::remove);
        getSession().flush();
        getSession().clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void replaceTemporaryLocation(Integer sourceId, Integer targetId, boolean delete) {
        Assert.notNull(sourceId);
        Assert.notNull(targetId);

        executeMultipleUpdateWithNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, sourceId, targetId);

        if (delete) {
            // delete temporary
            remove(sourceId);
        }

        getSession().flush();
        getSession().clear();
    }

    @Override
    public void remove(Integer monLocId) {

        // remove from cache
        evictFromCache(monLocId);

        super.remove(monLocId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLocationUsedInProgram(int locationId) {

        return executeMultipleCount(columnNamesByProgramTableNames, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLocationUsedInData(int locationId) {

        return executeMultipleCount(columnNamesByDataTableNames, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLocationUsedInValidatedData(int locationId) {

        return executeMultipleCountWithNotNullCondition(columnNamesByDataTableNames, validationDateColumnNameByDataTableNames, locationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<HarbourDTO> getAllHarbours(List<String> statusCodes) {

        Iterator<Object[]> it = Daos.queryIteratorWithStatus(createQuery("allHarbours"), statusCodes);

        List<HarbourDTO> result = Lists.newArrayList();
        while (it.hasNext()) {
            Object[] row = it.next();
            result.add(toHarbourDTO(Arrays.asList(row).iterator()));
        }

        return result;
    }

    @Override
    public Map<Integer, CoordinateDTO> getAllCoordinates() {

        Map<Integer, CoordinateDTO> allPositions = new HashMap<>();

        // process in this order : point, line, area to have the most precise position if a location have multiple positions
        queryList("allMonitoringLocationPoints").forEach(line -> allPositions.put((int) line[0], Geometries.getCoordinate((String) line[1])));
        queryList("allMonitoringLocationLines").forEach(line -> allPositions.put((int) line[0], Geometries.getCoordinate((String) line[1])));
        queryList("allMonitoringLocationAreas").forEach(line -> allPositions.put((int) line[0], Geometries.getCoordinate((String) line[1])));

        // add to cache
        Cache cacheById = cacheService.getCache(COORDINATE_BY_LOCATION_ID_CACHE);
        allPositions.keySet().forEach(locationId -> cacheById.put(locationId, allPositions.get(locationId)));

        return allPositions;
    }

    @Override
    public CoordinateDTO getCoordinateByLocationId(int locationId) {
        String position = queryUniqueTyped("positionByLocationId", "monitoringLocationId", IntegerType.INSTANCE, locationId);
        return Geometries.getCoordinate(position);
    }

    // INTERNAL METHODS
    private void saveLocation(LocationDTO location) {
        Assert.notNull(location);
        Assert.notBlank(location.getName());
        Assert.notNull(location.getPositioning());
        if (location.getStatus() == null) {
            location.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));
        }
        Assert.isTrue(ReefDbBeans.isLocalStatus(location.getStatus()), "source must have local status");

        MonitoringLocation target;
        if (location.getId() == null) {
            target = MonitoringLocation.Factory.newInstance();
            target.setMonLocId(TemporaryDataHelper.getNewNegativeIdForTemporaryData(getSession(), target.getClass()));
        } else {
            target = get(location.getId());
            Assert.isTrue(ReefDbBeans.isLocalStatus(target.getStatus()), "target must have local status");
        }

        // mandatory properties
        target.setMonLocNm(location.getName());
        target.setStatus(load(StatusImpl.class, location.getStatus().getCode()));
        target.setPositionningSystem(load(PositionningSystemImpl.class, location.getPositioning().getId()));

        // optional properties
        target.setMonLocBathym(Daos.convertToFloat(location.getBathymetry()));
        target.setMonLocCm(location.getComment());
        target.setMonLocLb(location.getLabel());
        target.setMonLocUtFormat(location.getUtFormat());
        target.setMonLocDaylightSavingTime(Daos.convertToString(location.getDayLightSavingTime()));

        target.setUpdateDt(newUpdateTimestamp());
        if (target.getMonLocCreationDt() == null) {
            target.setMonLocCreationDt(newCreateDate());
        }

        // Harbour
        if (location.getHarbour() != null) {
            target.setHarbour(load(HarbourImpl.class, location.getHarbour().getCode()));
        } else {
            target.setHarbour(null);
        }

        getSession().save(target);
        location.setId(target.getMonLocId());

        // save coordinate
        saveGeometry(location, target);
        // remove coordinate cache
        evictFromCache(location.getId());
    }

    private void evictFromCache(Integer locationId) {

        if (locationId == null) return;

        Cache locationByIdCache = cacheService.getCache(LOCATION_BY_ID_CACHE);
        locationByIdCache.evict(locationId);

        Cache coordinateByIdCache = cacheService.getCache(COORDINATE_BY_LOCATION_ID_CACHE);
        coordinateByIdCache.evict(locationId);

    }

    private LocationDTO toLocationDTO(Iterator<Object> source) {
        LocationDTO result = ReefDbBeanFactory.newLocationDTO();
        result.setId((Integer) source.next());
        result.setName((String) source.next());
        result.setBathymetry(Daos.convertToDouble((Float) source.next()));
        result.setComment((String) source.next());
        result.setLabel((String) source.next());
        result.setUtFormat((Double) source.next());
        result.setDayLightSavingTime(Daos.convertToBoolean(source.next()));
        HarbourDTO harbour = toHarbourDTO(source);
        result.setHarbour(StringUtils.isNotBlank(harbour.getCode()) ? harbour : null);
        result.setStatus(Daos.getStatus((String) source.next()));
        result.setPositioning(referentialDao.getPositioningSystemById((Integer) source.next()));
        result.setCoordinate(loopBackService.getCoordinateByLocationId(result.getId()));
        result.setCreationDate(Daos.convertToDate(source.next()));
        result.setUpdateDate(Daos.convertToDate(source.next()));
        return result;
    }

    private HarbourDTO toHarbourDTO(Iterator<Object> source) {
        HarbourDTO result = ReefDbBeanFactory.newHarbourDTO();
        result.setCode((String) source.next());
        result.setName((String) source.next());
        return result;
    }

    private void saveGeometry(LocationDTO location, MonitoringLocation target) {

        if (Geometries.isValid(location.getCoordinate())) {

            if (Geometries.isPoint(location.getCoordinate())) {

                // point
                if (target.getMonLocPoints() != null && target.getMonLocPoints().size() == 1) {

                    MonLocPoint pointToUpdate = target.getMonLocPoints().iterator().next();
                    if (!Geometries.equals(location.getCoordinate(), pointToUpdate.getMonLocPosition())) {
                        pointToUpdate.setMonLocPosition(Geometries.getPointPosition(location.getCoordinate()));
                        getSession().update(pointToUpdate);
                    }

                } else {
                    // create a monloc point
                    MonLocPoint point = MonLocPoint.Factory.newInstance();
                    point.setMonitoringLocation(target);
                    point.setMonLocPosition(Geometries.getPointPosition(location.getCoordinate()));
                    getSession().save(point);
                    target.getMonLocPoints().clear();
                    target.addMonLocPoints(point);
                }
                target.getMonLocAreas().clear();

            } else {

                // area
                if (target.getMonLocAreas() != null && target.getMonLocAreas().size() == 1) {

                    MonLocArea areaToUpdate = target.getMonLocAreas().iterator().next();
                    if (!Geometries.equals(location.getCoordinate(), areaToUpdate.getMonLocPosition())) {
                        areaToUpdate.setMonLocPosition(Geometries.getAreaPosition(location.getCoordinate()));
                        getSession().update(areaToUpdate);
                    }

                } else {
                    // create a monloc area
                    MonLocArea area = MonLocArea.Factory.newInstance();
                    area.setMonitoringLocation(target);
                    area.setMonLocPosition(Geometries.getAreaPosition(location.getCoordinate()));
                    getSession().save(area);
                    target.getMonLocAreas().clear();
                    target.addMonLocAreas(area);
                }
                target.getMonLocPoints().clear();
            }

        } else {
            target.getMonLocPoints().clear();
            target.getMonLocAreas().clear();
        }

        target.getMonLocLines().clear();
    }
}
