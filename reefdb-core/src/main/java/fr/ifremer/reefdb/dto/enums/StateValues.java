package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.StateDTO;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Etat values.
 */
public enum StateValues {

    CONTROLLED(n("reefdb.property.state.controlled"), "control"),
    VALIDATED(n("reefdb.property.state.validated"), "accept");

    private final String i18nKey;
    private final String iconAction;

    StateValues(final String i18nKey, final String iconAction) {
        this.i18nKey = i18nKey;
        this.iconAction = iconAction;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.i18nKey);
    }

    public String getIconAction() {
        return iconAction;
    }

    public StateDTO toStateDTO() {
        StateDTO state = ReefDbBeanFactory.newStateDTO();
        state.setId(ordinal());
        state.setName(getLabel());
        state.setIconName(getIconAction());
        return state;
    }
    /**
     * <p>getState.</p>
     *
     * @param label a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.enums.StateValues} object.
     */
    public static StateValues getState(final String label) {
        if (label != null) {
            for (final StateValues stateValue : StateValues.values()) {
                if (label.equals(stateValue.getLabel())) {
                    return stateValue;
                }
            }
        }
        return null;
    }
}
