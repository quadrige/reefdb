package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.dao.system.synchronization.SynchronizationStatus;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.SynchronizationStatusDTO;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Partage enum.
 */
public enum SynchronizationStatusValues {

    // TODO fix action icons
	DIRTY(SynchronizationStatus.DIRTY, n("reefdb.core.enums.synchronizationStatusValues.dirty"), "overlay-dirty"),
    READY_TO_SYNCHRONIZE(SynchronizationStatus.READY_TO_SYNCHRONIZE, n("reefdb.core.enums.synchronizationStatusValues.dirty"), "overlay-dirty" /*mantis #26500*/),
	SYNCHRONIZED_FILE(SynchronizationStatus.SYNCHRONIZED_WITH_FILE, n("reefdb.core.enums.synchronizationStatusValues.synchroFile"), "overlay-synchro-file"),
	SYNCHRONIZED(SynchronizationStatus.SYNCHRONIZED ,n("reefdb.core.enums.synchronizationStatusValues.synchro"), "overlay-synchro"),
	DELETED(SynchronizationStatus.DELETED ,n("reefdb.core.enums.synchronizationStatusValues.deleted"), "overlay-deleted");

	private final SynchronizationStatus synchronizationStatus;
	private final String keyLabel;
	private final String iconAction;
	
	SynchronizationStatusValues(SynchronizationStatus synchronizationStatus, String keyLabel, String iconAction) {
        this.synchronizationStatus = synchronizationStatus;
		this.keyLabel = keyLabel;
		this.iconAction = iconAction;
	}

	/**
	 * <p>Getter for the field <code>code</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getCode() {
		return synchronizationStatus.getValue();
	}

	/**
	 * <p>getLabel.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getLabel() {
		return t(this.keyLabel);
	}
	
	/**
	 * <p>Getter for the field <code>iconAction</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getIconAction() {
		return iconAction;
	}

    /**
     * <p>toSynchronizationStatusDTO.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.SynchronizationStatusDTO} object.
     */
    public SynchronizationStatusDTO toSynchronizationStatusDTO() {
        SynchronizationStatusDTO dto = ReefDbBeanFactory.newSynchronizationStatusDTO();
        dto.setId(ordinal());
        dto.setCode(getCode());
        dto.setName(getLabel());
        dto.setIconName(getIconAction());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param synchronizationStatus a {@link fr.ifremer.reefdb.dto.SynchronizationStatusDTO} object.
     * @return a boolean.
     */
    public boolean equals(SynchronizationStatusDTO synchronizationStatus) {
        return synchronizationStatus != null && getCode().equals(synchronizationStatus.getCode());
    }

    /**
     * <p>getByCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues} object.
     */
    public static SynchronizationStatusValues getByCode(String code) {
        for (SynchronizationStatusValues enumValue: SynchronizationStatusValues.values()) {
            if (StringUtils.isNotBlank(enumValue.getCode()) && enumValue.getCode().equals(code)) {
                return enumValue;
            }
        }
        return null;
    }

    /**
     * <p>toSynchronizationStatusDTO.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.SynchronizationStatusDTO} object.
     */
    public static SynchronizationStatusDTO toSynchronizationStatusDTO(String code) {
        SynchronizationStatusValues enumValue = getByCode(code);
        if (enumValue != null) {
            return enumValue.toSynchronizationStatusDTO();
        }
        return null;
    }

    /**
     * <p>fromOrdinal.</p>
     *
     * @param ordinal a int.
     * @return a {@link fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues} object.
     */
    public static SynchronizationStatusValues fromOrdinal(int ordinal) {
        if (ordinal > SynchronizationStatusValues.values().length -1) {
            throw new IllegalArgumentException(String.format("Bad ordinal value [%s] for enumeration [%s]",
                    ordinal,
                    SynchronizationStatusValues.class.getSimpleName()));
        }
        return SynchronizationStatusValues.values()[ordinal];
    }
}
