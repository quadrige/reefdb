package fr.ifremer.reefdb.dto.data;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Interface to aware bean to get the bean coordinate
 * <p/>
 * Created by Ludovic on 03/07/2015.
 */
public interface CoordinateAreaAware {

    /** Constant <code>PROPERTY_MIN_LATITUDE="minLatitude"</code> */
    String PROPERTY_MIN_LATITUDE = "minLatitude";
    /** Constant <code>PROPERTY_MIN_LONGITUDE="minLongitude"</code> */
    String PROPERTY_MIN_LONGITUDE = "minLongitude";
    /** Constant <code>PROPERTY_MAX_LATITUDE="maxLatitude"</code> */
    String PROPERTY_MAX_LATITUDE = "maxLatitude";
    /** Constant <code>PROPERTY_MAX_LONGITUDE="maxLongitude"</code> */
    String PROPERTY_MAX_LONGITUDE = "maxLongitude";

    /**
     * <p>getMinLatitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getMinLatitude();

    /**
     * <p>setMinLatitude.</p>
     *
     * @param minLatitude a {@link java.lang.Double} object.
     */
    void setMinLatitude(Double minLatitude);

    /**
     * <p>getMinLongitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getMinLongitude();

    /**
     * <p>setMinLongitude.</p>
     *
     * @param minLongitude a {@link java.lang.Double} object.
     */
    void setMinLongitude(Double minLongitude);

    /**
     * <p>getMaxLatitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getMaxLatitude();

    /**
     * <p>setMaxLatitude.</p>
     *
     * @param maxLatitude a {@link java.lang.Double} object.
     */
    void setMaxLatitude(Double maxLatitude);

    /**
     * <p>getMaxLongitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getMaxLongitude();

    /**
     * <p>setMaxLongitude.</p>
     *
     * @param maxLongitude a {@link java.lang.Double} object.
     */
    void setMaxLongitude(Double maxLongitude);

}
