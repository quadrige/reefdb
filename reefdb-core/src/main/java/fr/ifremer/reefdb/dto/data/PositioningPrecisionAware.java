package fr.ifremer.reefdb.dto.data;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


/**
 * Interface to aware bean to get the positioning precision
 * <p/>
 * Created by Ludovic on 14/10/2015.
 */
public interface PositioningPrecisionAware {

    /** Constant <code>PROPERTY_POSITIONING_PRECISION="positioningPrecision"</code> */
    String PROPERTY_POSITIONING_PRECISION = "positioningPrecision";

    /**
     * <p>getPositioningPrecision.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    String getPositioningPrecision();

}
