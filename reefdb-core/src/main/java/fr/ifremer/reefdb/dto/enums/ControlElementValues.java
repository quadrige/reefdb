package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RuleControlEntity;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * ElementControl enum.
 */
public enum ControlElementValues {

    SURVEY(RuleControlEntity.SURVEY, n("reefdb.core.enums.elementControlValues.observation")),
    SAMPLING_OPERATION(RuleControlEntity.SAMPLING_OPERATION, n("reefdb.core.enums.elementControlValues.samplingOperation")),
    MEASUREMENT(RuleControlEntity.MEASUREMENT, n("reefdb.core.enums.elementControlValues.measurement"));

    private final RuleControlEntity ruleControlEntity;
    private final String keyLabel;

    ControlElementValues(RuleControlEntity ruleControlEntity, String keyLabel) {
        this.ruleControlEntity = ruleControlEntity;
        this.keyLabel = keyLabel;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return ruleControlEntity.getValue();
    }

    /**
     * <p>toControlElementDTO.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO} object.
     */
    public ControlElementDTO toControlElementDTO() {
        ControlElementDTO dto = ReefDbBeanFactory.newControlElementDTO();
        dto.setId(ordinal());
        dto.setCode(getCode());
        dto.setName(getLabel());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param controlElement a {@link fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO} object.
     * @return a boolean.
     */
    public boolean equals(ControlElementDTO controlElement) {
        return controlElement != null && (getCode().equals(controlElement.getCode()));
    }

    public boolean equals(String controlElementCode) {
        return controlElementCode != null && (getCode().equals(controlElementCode));
    }

    /**
     * <p>toControlElementDTO.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlElementDTO} object.
     */
    public static ControlElementDTO toControlElementDTO(String code) {
        ControlElementValues value = getByCode(code);
        if (value != null) {
            return value.toControlElementDTO();
        }
        return null;
    }

    /**
     * <p>getByCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.enums.ControlElementValues} object.
     */
    public static ControlElementValues getByCode(String code) {
        for (ControlElementValues value : values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}
