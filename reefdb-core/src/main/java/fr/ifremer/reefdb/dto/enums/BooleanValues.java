package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.BooleanDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Boolean enum.
 */
public enum BooleanValues {

    YES(n("reefdb.core.enums.boolean.yes"), Boolean.TRUE),
    NO(n("reefdb.core.enums.boolean.no"), Boolean.FALSE);

    private final String keyLabel;
    private final Boolean value;

    BooleanValues(final String key, final Boolean value) {
        this.keyLabel = key;
        this.value = value;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getValue() {
        return value;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    public BooleanDTO toBooleanDTO() {
        BooleanDTO booleanDTO = ReefDbBeanFactory.newBooleanDTO();
        booleanDTO.setId(ordinal());
        booleanDTO.setValue(getValue());
        booleanDTO.setName(getLabel());
        return booleanDTO;
    }
}
