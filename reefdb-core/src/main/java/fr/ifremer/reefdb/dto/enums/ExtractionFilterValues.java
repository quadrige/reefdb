package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.filter.FilterTypeId;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Extraction filter enum.
 */
public enum ExtractionFilterValues {

    PERIOD(null, n("reefdb.core.enums.extractionFilter.period"), false),
    PROGRAM(FilterTypeId.PROGRAM, n("reefdb.core.enums.extractionFilter.program"), false),
    LOCATION(FilterTypeId.MONITORING_LOCATION, n("reefdb.core.enums.extractionFilter.location"), false),
    CAMPAIGN(FilterTypeId.CAMPAIGN, n("reefdb.core.enums.extractionFilter.campaign"), false),
    TAXON(FilterTypeId.TAXON_NAME, n("reefdb.core.enums.extractionFilter.taxon"), false),
    TAXON_GROUP(FilterTypeId.TAXON_GROUP, n("reefdb.core.enums.extractionFilter.taxonGroup"), false),
    DEPARTMENT(FilterTypeId.DEPARTMENT, n("reefdb.core.enums.extractionFilter.department"), false),
    PMFM(FilterTypeId.PMFM, n("reefdb.core.enums.extractionFilter.pmfm"), false),
    ORDER_ITEM_TYPE(FilterTypeId.ORDER_ITEM_TYPE, n("reefdb.core.enums.extractionFilter.orderItemType"), true);

    private final String keyLabel;
    private final FilterTypeId filterTypeId;
    private final boolean hidden;

    ExtractionFilterValues(FilterTypeId filterType, String keyLabel, boolean hidden) {
        this.keyLabel = keyLabel;
        this.filterTypeId = filterType;
        this.hidden = hidden;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>filterTypeId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFilterTypeId() {
        return this.filterTypeId != null ? this.filterTypeId.getValue() : -1;
    }

    /**
     * <p>isHidden.</p>
     *
     * @return a boolean.
     */
    public boolean isHidden() {
        return this.hidden;
    }

    /**
     * <p>getExtractionFilter.</p>
     *
     * @param filterTypeId a {@link java.lang.Integer} object.
     * @return a {@link fr.ifremer.reefdb.dto.enums.ExtractionFilterValues} object.
     */
    public static ExtractionFilterValues getExtractionFilter(final Integer filterTypeId) {
        for (final ExtractionFilterValues context : values()) {
            if (context.getFilterTypeId().equals(filterTypeId)) {
                return context;
            }
        }
        return null;
    }
}
