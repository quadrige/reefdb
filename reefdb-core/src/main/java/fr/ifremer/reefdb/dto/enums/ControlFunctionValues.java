package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.FunctionId;
import fr.ifremer.reefdb.dto.FunctionDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * ControlFunctionValues enum.
 */
public enum ControlFunctionValues {

    IS_EMPTY(FunctionId.EMPTY, n("reefdb.core.enums.functionControlValues.isEmpty")),
    IS_AMONG(FunctionId.IN, n("reefdb.core.enums.functionControlValues.isAmong")),
    MIN_MAX(FunctionId.MIN_MAX, n("reefdb.core.enums.functionControlValues.minMax")),
    MIN_MAX_DATE(FunctionId.MIN_MAX_DATE, n("reefdb.core.enums.functionControlValues.minMaxDate")),
    NOT_EMPTY(FunctionId.NOT_EMPTY, n("reefdb.core.enums.functionControlValues.notEmpty")),
    PRECONDITION_QUALITATIVE(FunctionId.PRECONDITION_QUALITATIVE, n("reefdb.core.enums.functionControlValues.preconditionQualitative")),
    PRECONDITION_NUMERICAL(FunctionId.PRECONDITION_NUMERICAL, n("reefdb.core.enums.functionControlValues.preconditionNumerical")),
    NOT_EMPTY_CONDITIONAL(FunctionId.NOT_EMPTY_CONDITIONAL, n("reefdb.core.enums.functionControlValues.notEmptyConditional"))
    ;

    private final FunctionId function;
    private final String keyLabel;

    ControlFunctionValues(FunctionId functionId, final String key) {
        this.function = functionId;
        this.keyLabel = key;
    }

    /**
     * <p>getFunctionId.</p>
     *
     * @return a int.
     */
    public int getFunctionId() {
        return this.function.getValue();
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>toFunctionDTO.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.FunctionDTO} object.
     */
    public FunctionDTO toFunctionDTO() {
        FunctionDTO dto = ReefDbBeanFactory.newFunctionDTO();
        dto.setId(getFunctionId());
        dto.setName(getLabel());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param function a {@link fr.ifremer.reefdb.dto.FunctionDTO} object.
     * @return a boolean.
     */
    public boolean equals(FunctionDTO function) {
        return function != null && getFunctionId() == function.getId();
    }

    /**
     * <p>toFunctionDTO.</p>
     *
     * @param functionId a {@link java.lang.Integer} object.
     * @return a {@link fr.ifremer.reefdb.dto.FunctionDTO} object.
     */
    public static FunctionDTO toFunctionDTO(Integer functionId) {
        ControlFunctionValues value = getFunctionValue(functionId);
        if (value != null) {
            return value.toFunctionDTO();
        }
        return null;
    }

    /**
     * <p>getFunctionValue.</p>
     *
     * @param functionId a {@link java.lang.Integer} object.
     * @return a {@link ControlFunctionValues} object.
     */
    public static ControlFunctionValues getFunctionValue(final Integer functionId) {
        if (functionId != null) {
            for (final ControlFunctionValues functionValue : ControlFunctionValues.values()) {
                if (functionId.equals(functionValue.getFunctionId())) {
                    return functionValue;
                }
            }
        }
        return null;
    }
}
