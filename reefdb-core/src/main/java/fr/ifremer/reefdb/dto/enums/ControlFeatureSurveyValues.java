package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RuleControlAttribute;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * ControlFeatureSurveyValues enum.
 */
public enum ControlFeatureSurveyValues {

    CAMPAIGN(RuleControlAttribute.CAMPAIGN, n("reefdb.core.enums.featureControlValues.campaign")),
    COMMENT(RuleControlAttribute.COMMENT, n("reefdb.core.enums.featureControlValues.comment")),
    VALIDATION_COMMENT(RuleControlAttribute.VALIDATION_COMMENT, n("reefdb.core.enums.featureControlValues.validationComment")),
    DATE(RuleControlAttribute.DATE, n("reefdb.core.enums.featureControlValues.date")),
    CONTROL_DATE(RuleControlAttribute.CONTROL_DATE, n("reefdb.core.enums.featureControlValues.controlDate")),
    UPDATE_DATE(RuleControlAttribute.UPDATE_DATE, n("reefdb.core.enums.featureControlValues.updateDate")),
    VALIDATION_DATE(RuleControlAttribute.VALIDATION_DATE, n("reefdb.core.enums.featureControlValues.validationDate")),
    TIME(RuleControlAttribute.TIME, n("reefdb.core.enums.featureControlValues.time")),
    LATITUDE_MAX_LOCATION(RuleControlAttribute.LATITUDE_MAX_LOCATION, n("reefdb.core.enums.featureControlValues.latitudeMaxLocation")),
    LATITUDE_MIN_LOCATION(RuleControlAttribute.LATITUDE_MIN_LOCATION, n("reefdb.core.enums.featureControlValues.latitudeMinLocation")),
    LATITUDE_REAL_SURVEY(RuleControlAttribute.SURVEY_LATITUDE_REAL, n("reefdb.core.enums.featureControlValues.latitudeReal")),
    LONGITUDE_MAX_LOCATION(RuleControlAttribute.LONGITUDE_MAX_LOCATION, n("reefdb.core.enums.featureControlValues.longitudeMaxLocation")),
    LONGITUDE_MIN_LOCATION(RuleControlAttribute.LONGITUDE_MIN_LOCATION, n("reefdb.core.enums.featureControlValues.longitudeMinLocation")),
    LONGITUDE_REAL_SURVEY(RuleControlAttribute.SURVEY_LONGITUDE_REAL, n("reefdb.core.enums.featureControlValues.longitudeReal")),
    NAME(RuleControlAttribute.NAME, n("reefdb.core.enums.featureControlValues.name")),
    DEPARTMENT(RuleControlAttribute.RECORDER_DEPARTMENT, n("reefdb.core.enums.featureControlValues.department")),
    POSITIONING(RuleControlAttribute.POSITIONING, n("reefdb.core.enums.featureControlValues.positioning")),
    POSITIONING_PRECISION(RuleControlAttribute.POSITIONING_PRECISION, n("reefdb.core.enums.featureControlValues.positioningPrecision")),
    PRECISE_DEPTH(RuleControlAttribute.BOTTOM_DEPTH, n("reefdb.core.enums.featureControlValues.preciseDepth")),
    PROGRAM(RuleControlAttribute.PROGRAM, n("reefdb.core.enums.featureControlValues.program")),
    LOCATION(RuleControlAttribute.LOCATION, n("reefdb.core.enums.featureControlValues.location"));

    private final RuleControlAttribute ruleControlAttribute;
    private final String keyLabel;

    ControlFeatureSurveyValues(RuleControlAttribute ruleControlAttribute, String key) {
        this.ruleControlAttribute = ruleControlAttribute;
        this.keyLabel = key;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return ruleControlAttribute.getValue();
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     */
    public ControlFeatureDTO toControlFeatureDTO() {
        ControlFeatureDTO dto = ReefDbBeanFactory.newControlFeatureDTO();
        dto.setId(ordinal());
        dto.setCode(getCode());
        dto.setName(getLabel());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param controlFeature a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     * @return a boolean.
     */
    public boolean equals(ControlFeatureDTO controlFeature) {
        return controlFeature != null && (getCode().equals(controlFeature.getCode()));
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     */
    public static ControlFeatureDTO toControlFeatureDTO(String code) {
        ControlFeatureSurveyValues value = getByCode(code);
        if (value != null) {
            return value.toControlFeatureDTO();
        }
        return null;
    }

    /**
     * <p>getByCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.enums.ControlFeatureSurveyValues} object.
     */
    public static ControlFeatureSurveyValues getByCode(String code) {
        for (final ControlFeatureSurveyValues enumValue : ControlFeatureSurveyValues.values()) {
            if (StringUtils.isNotBlank(enumValue.getCode()) && (enumValue.getCode().equals(code))) {
                return enumValue;
            }
        }
        return null;
    }
}
