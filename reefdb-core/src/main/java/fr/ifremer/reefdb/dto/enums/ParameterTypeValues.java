package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * <p>ParameterTypeValues class.</p>
 *
 */
public enum ParameterTypeValues {

    QUALITATIVE(n("reefdb.core.enums.qualitative")),
    CALCULATED(n("reefdb.core.enums.calculated")),
    TAXONOMIC(n("reefdb.core.enums.taxonomic"));

    private final String keyLabel;

    ParameterTypeValues(String keyLabel) {
        this.keyLabel = keyLabel;
    }

    /**
     * <p>Getter for the field <code>keyLabel</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKeyLabel() {
        return this.keyLabel;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }
}
