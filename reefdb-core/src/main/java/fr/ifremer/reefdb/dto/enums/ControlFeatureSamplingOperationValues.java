package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RuleControlAttribute;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * FeatureControlSampleValues enum.
 */
public enum ControlFeatureSamplingOperationValues {

    COMMENT(RuleControlAttribute.COMMENT, n("reefdb.core.enums.featureControlValues.comment")),
    TIME(RuleControlAttribute.TIME, n("reefdb.core.enums.featureControlValues.time")),
    LATITUDE_REAL(RuleControlAttribute.SAMPLING_OPERATION_LATITUDE_REAL, n("reefdb.core.enums.featureControlValues.latitudeReal")),
    LONGITUDE_REAL(RuleControlAttribute.SAMPLING_OPERATION_LONGITUDE_REAL, n("reefdb.core.enums.featureControlValues.longitudeReal")),
    GEAR(RuleControlAttribute.GEAR, n("reefdb.core.enums.featureControlValues.gear")),
    NAME(RuleControlAttribute.NAME, n("reefdb.core.enums.featureControlValues.name")),
    DEPARTMENT(RuleControlAttribute.SAMPLER_DEPARTMENT, n("reefdb.core.enums.featureControlValues.samplerDepartment")),
    POSITIONING(RuleControlAttribute.POSITIONING, n("reefdb.core.enums.featureControlValues.positioning")),
    POSITIONING_PRECISION(RuleControlAttribute.POSITIONING_PRECISION, n("reefdb.core.enums.featureControlValues.positioningPrecision")),
    DEPTH(RuleControlAttribute.DEPTH, n("reefdb.core.enums.featureControlValues.depth")),
    DEPTH_MAX(RuleControlAttribute.MAX_DEPTH, n("reefdb.core.enums.featureControlValues.depthMax")),
    DEPTH_MIN(RuleControlAttribute.MIN_DEPTH, n("reefdb.core.enums.featureControlValues.depthMin")),
    SIZE(RuleControlAttribute.SIZE, n("reefdb.core.enums.featureControlValues.size")),
    SIZE_UNIT(RuleControlAttribute.SIZE_UNIT, n("reefdb.core.enums.featureControlValues.sizeUnit"));

    private final RuleControlAttribute ruleControlAttribute;
    private final String keyLabel;

    ControlFeatureSamplingOperationValues(RuleControlAttribute ruleControlAttribute, String key) {
        this.ruleControlAttribute = ruleControlAttribute;
        this.keyLabel = key;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return ruleControlAttribute.getValue();
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     */
    public ControlFeatureDTO toControlFeatureDTO() {
        ControlFeatureDTO dto = ReefDbBeanFactory.newControlFeatureDTO();
        dto.setId(ordinal());
        dto.setCode(getCode());
        dto.setName(getLabel());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param controlFeature a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     * @return a boolean.
     */
    public boolean equals(ControlFeatureDTO controlFeature) {
        return controlFeature != null && (getCode().equals(controlFeature.getCode()));
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     */
    public static ControlFeatureDTO toControlFeatureDTO(String code) {
        ControlFeatureSamplingOperationValues value = getByCode(code);
        if (value != null) {
            return value.toControlFeatureDTO();
        }
        return null;
    }

    /**
     * <p>getByCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.enums.ControlFeatureSamplingOperationValues} object.
     */
    public static ControlFeatureSamplingOperationValues getByCode(String code) {
        for (ControlFeatureSamplingOperationValues feature : ControlFeatureSamplingOperationValues.values()) {
            if (StringUtils.isNotBlank(feature.getCode()) && (feature.getCode().equals(code))) {
                return feature;
            }
        }
        return null;
    }
}
