package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.filter.FilterTypeId;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Context filter enum.
 */
public enum FilterTypeValues {

    ANALYSIS_INSTRUMENT(FilterTypeId.ANALYSIS_INSTRUMENT, n("reefdb.core.enums.contextFilter.analysisInstrument")),
    SAMPLING_EQUIPMENT(FilterTypeId.SAMPLING_EQUIPMENT, n("reefdb.core.enums.contextFilter.samplingEquipment")),
    TAXON_GROUP(FilterTypeId.TAXON_GROUP, n("reefdb.core.enums.contextFilter.taxonGroup")),
    LOCATION(FilterTypeId.MONITORING_LOCATION, n("reefdb.core.enums.contextFilter.location")),
    PROGRAM(FilterTypeId.PROGRAM, n("reefdb.core.enums.contextFilter.program")),
    PMFM(FilterTypeId.PMFM, n("reefdb.core.enums.contextFilter.pmfm")),
    DEPARTMENT(FilterTypeId.DEPARTMENT, n("reefdb.core.enums.contextFilter.department")),
    TAXON(FilterTypeId.TAXON_NAME, n("reefdb.core.enums.contextFilter.taxon")),
    USER(FilterTypeId.QUSER, n("reefdb.core.enums.contextFilter.user")),
    CAMPAIGN(FilterTypeId.CAMPAIGN, n("reefdb.core.enums.contextFilter.campaign"));

    private final String keyLabel;
    private final FilterTypeId filterTypeId;


    FilterTypeValues(FilterTypeId filterType, String keyLabel) {
        this.keyLabel = keyLabel;
        this.filterTypeId = filterType;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>filterTypeId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getFilterTypeId() {
        return this.filterTypeId != null ? this.filterTypeId.getValue() : -1;
    }

    /**
     * <p>getContextFilter.</p>
     *
     * @param filterTypeId a {@link java.lang.Integer} object.
     * @return a {@link FilterTypeValues} object.
     */
    public static FilterTypeValues getFilterType(final Integer filterTypeId) {
        for (final FilterTypeValues filterType : values()) {
            if (filterType.getFilterTypeId().equals(filterTypeId)) {
                return filterType;
            }
        }
        return null;
    }
}
