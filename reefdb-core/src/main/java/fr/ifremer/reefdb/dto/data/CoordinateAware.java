package fr.ifremer.reefdb.dto.data;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Interface to aware bean to get the bean coordinate
 * <p/>
 * Created by Ludovic on 03/07/2015.
 */
public interface CoordinateAware {

    /** Constant <code>PROPERTY_LATITUDE="latitude"</code> */
    String PROPERTY_LATITUDE = "latitude";
    /** Constant <code>PROPERTY_LONGITUDE="longitude"</code> */
    String PROPERTY_LONGITUDE = "longitude";

    /**
     * <p>getLatitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getLatitude();

    /**
     * <p>setLatitude.</p>
     *
     * @param latitude a {@link java.lang.Double} object.
     */
    void setLatitude(Double latitude);

    /**
     * <p>getLongitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getLongitude();

    /**
     * <p>setLongitude.</p>
     *
     * @param longitude a {@link java.lang.Double} object.
     */
    void setLongitude(Double longitude);

}
