package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.system.rule.RuleControlAttribute;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * ControlFeatureMeasurementValues enum.
 */
public enum ControlFeatureMeasurementValues {

    ANALYST(RuleControlAttribute.ANALYST_DEPARTMENT, n("reefdb.core.enums.featureControlValues.analyst")),
    TAXON_GROUP(RuleControlAttribute.TAXON_GROUP, n("reefdb.core.enums.featureControlValues.taxonGroup")),
    TAXON(RuleControlAttribute.TAXON, n("reefdb.core.enums.featureControlValues.taxon")),
    PMFM(RuleControlAttribute.PMFM, n("reefdb.core.enums.featureControlValues.pmfm")),
    NUMERICAL_VALUE(RuleControlAttribute.NUMERICAL_VALUE, n("reefdb.core.enums.featureControlValues.numericalValue")),
    QUALITATIVE_VALUE(RuleControlAttribute.QUALITATIVE_VALUE, n("reefdb.core.enums.featureControlValues.qualitativeValue"));

    private final RuleControlAttribute ruleControlAttribute;
    private final String keyLabel;

    ControlFeatureMeasurementValues(RuleControlAttribute ruleControlAttribute, String key) {
        this.ruleControlAttribute = ruleControlAttribute;
        this.keyLabel = key;
    }

    /**
     * <p>getLabel.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return t(this.keyLabel);
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return ruleControlAttribute.getValue();
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     */
    public ControlFeatureDTO toControlFeatureDTO() {
        ControlFeatureDTO dto = ReefDbBeanFactory.newControlFeatureDTO();
        dto.setId(ordinal());
        dto.setCode(getCode());
        dto.setName(getLabel());
        return dto;
    }

    /**
     * <p>equals.</p>
     *
     * @param controlFeature a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     * @return a boolean.
     */
    public boolean equals(ControlFeatureDTO controlFeature) {
        return controlFeature != null && getCode().equals(controlFeature.getCode());
    }

    /**
     * <p>toControlFeatureDTO.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link fr.ifremer.reefdb.dto.configuration.control.ControlFeatureDTO} object.
     */
    public static ControlFeatureDTO toControlFeatureDTO(String code) {
        ControlFeatureMeasurementValues value = getByCode(code);
        if (value != null) {
            return value.toControlFeatureDTO();
        }
        return null;
    }

    /**
     * <p>getByCode.</p>
     *
     * @param code a {@link java.lang.String} object.
     * @return a {@link ControlFeatureMeasurementValues} object.
     */
    public static ControlFeatureMeasurementValues getByCode(String code) {
        for (ControlFeatureMeasurementValues enumValue : ControlFeatureMeasurementValues.values()) {
            if (StringUtils.isNotBlank(enumValue.getCode()) && enumValue.getCode().equals(code)) {
                return enumValue;
            }
        }
        return null;
    }
}
