package fr.ifremer.reefdb.dto.enums;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.SearchDateDTO;

import static org.nuiton.i18n.I18n.t;

/**
 * Search date values.
 */
public enum SearchDateValues {

    EQUALS(t("reefdb.core.enums.searchDateValues.equals")), // =
    BETWEEN(t("reefdb.core.enums.searchDateValues.between")), // Entre
    BEFORE(t("reefdb.core.enums.searchDateValues.before")), // <
    BEFORE_OR_EQUALS(t("reefdb.core.enums.searchDateValues.beforeOrEquals")), // <=
    AFTER(t("reefdb.core.enums.searchDateValues.after")), // >
    AFTER_OR_EQUALS(t("reefdb.core.enums.searchDateValues.afterOrEquals")); // >=

    private final String label;

    SearchDateValues(final String label) {
        this.label = label;
    }

    /**
     * <p>Getter for the field <code>label</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLabel() {
        return this.label;
    }

    public SearchDateDTO toSearchDateDTO() {
        SearchDateDTO result = ReefDbBeanFactory.newSearchDateDTO();
        result.setId(ordinal());
        result.setName(getLabel());
        return result;
    }

}
