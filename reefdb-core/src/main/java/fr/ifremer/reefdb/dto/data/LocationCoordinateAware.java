package fr.ifremer.reefdb.dto.data;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Interface to aware bean to get the location coordinate of the bean
 * <p/>
 * Created by Ludovic on 03/07/2015.
 */
public interface LocationCoordinateAware {

    /** Constant <code>PROPERTY_LOCATION_MIN_LATITUDE="locationMinLatitude"</code> */
    String PROPERTY_LOCATION_MIN_LATITUDE = "locationMinLatitude";
    /** Constant <code>PROPERTY_LOCATION_MAX_LATITUDE="locationMaxLatitude"</code> */
    String PROPERTY_LOCATION_MAX_LATITUDE = "locationMaxLatitude";
    /** Constant <code>PROPERTY_LOCATION_MIN_LONGITUDE="locationMinLongitude"</code> */
    String PROPERTY_LOCATION_MIN_LONGITUDE = "locationMinLongitude";
    /** Constant <code>PROPERTY_LOCATION_MAX_LONGITUDE="locationMaxLongitude"</code> */
    String PROPERTY_LOCATION_MAX_LONGITUDE = "locationMaxLongitude";

    /**
     * <p>getLocationMinLatitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getLocationMinLatitude();

    /**
     * <p>getLocationMaxLatitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getLocationMaxLatitude();

    /**
     * <p>getLocationMinLongitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getLocationMinLongitude();

    /**
     * <p>getLocationMaxLongitude.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    Double getLocationMaxLongitude();
}
