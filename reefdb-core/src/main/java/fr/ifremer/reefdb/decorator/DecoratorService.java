package fr.ifremer.reefdb.decorator;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * <p>DecoratorService interface.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public interface DecoratorService extends fr.ifremer.quadrige3.core.service.decorator.DecoratorService {

    /** Constant <code>COLLECTION_SIZE="collectionSize"</code> */
    String COLLECTION_SIZE = "collectionSize";

    /** Constant <code>WITH_SYMBOL="withSymbol"</code> */
    String WITH_SYMBOL = "withSymbol";

    /** Constant <code>WITH_CITATION="withCitation"</code> */
    String WITH_CITATION = "withCitation";

    String WITH_CITATION_AND_REFERENT = "withCitationAndReferent";

    String WITH_REFERENT = "withReferent";

    /** Constant <code>NAME_WITH_UNIT="nameWithUnit"</code> */
    String NAME_WITH_UNIT = "nameWithUnit";
    String NAME_WITH_ID = "nameWithId";

    /** Constant <code>DESCRIPTION="description"</code> */
    String DESCRIPTION = "description";
    
    /** Constant <code>DURATION_IN_MINUTES="durationInMinutes"</code> */
    String DURATION_IN_MINUTES = "durationInMinutes";
    
    /** Constant <code>DURATION_IN_DECIMAL_HOURS="durationInDecimalHours"</code> */
    String DURATION_IN_DECIMAL_HOURS = "durationInDecimalHours";

    /** Constant <code>TIME_IN_HOURS_MINUTES="timeInHoursMinutes"</code> */
    String TIME_IN_HOURS_MINUTES = "timeInHoursMinutes";
    
    /** Constant <code>CONCAT="concat"</code> */
    String CONCAT = "concat";

    /** Constant <code>WITH_COUNT="withCount"</code> */
    String WITH_COUNT = "withCount";

    /** Constant <code>FOR_EXTRACTION="forExtraction"</code> */
    String FOR_EXTRACTION = "forExtraction";

}
