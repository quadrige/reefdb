package fr.ifremer.reefdb.config;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.UnitId;
import fr.ifremer.quadrige3.core.dao.technical.QuadrigeEnumerationDef;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.net.URL;

import static org.nuiton.i18n.I18n.n;

/**
 * <p>ReefDbConfigurationOption class.</p>
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public enum ReefDbConfigurationOption implements ConfigOptionDef {

    /**
     * -------- READ-ONLY OPTIONS -----------
     */
    BASEDIR(
            "reefdb.basedir",
            n("reefdb.config.option.basedir.description"),
            "${user.home}/.reefdb",
            File.class),
    HELP_DIRECTORY(
            "reefdb.help.directory",
            n("reefdb.config.option.help.directory.description"),
            "${reefdb.basedir}/help",
            File.class),
    /*-----------------------------------------------------------------
     *-- DB OPTIONS ---------------------------------------------------
     *-----------------------------------------------------------------*/
    CACHE_ENABLED_AT_STARTUP(
            "reefdb.persistence.cache.enabled",
            n("reefdb.persistence.cache.enabled.description"),
            "true",
            Boolean.class,
            false),
    CACHE_CLEAN_AT_STARTUP(
            "reefdb.persistence.cache.clean",
            n("reefdb.persistence.cache.clean.description"),
            "false",
            Boolean.class),
    DB_CHECK_CONSTANTS(
            "reefdb.persistence.checkConstants.enable",
            n("reefdb.config.option.persistence.checkConstants.description"),
            String.valueOf(false),
            Boolean.class, false),
    VERSION(
            "reefdb.version",
            n("reefdb.config.option.version.description"),
            "",
            Version.class),
    SITE_URL(
            "reefdb.site.url",
            n("reefdb.config.option.site.url.description"),
            // "http://www.ifremer.fr/maven/reports/quadrige3/reefdb", if on Ifremer repository
            // Mantis #43116 use specific URL
            "http://wwz.ifremer.fr/quadrige2_support/BD-Recif",
            URL.class),
    ORGANIZATION_NAME(
            "reefdb.organizationName",
            n("reefdb.config.option.organizationName.description"),
            "",
            String.class),
    INCEPTION_YEAR(
            "reefdb.inceptionYear",
            n("reefdb.config.option.inceptionYear.description"),
            "2014",
            Integer.class),
    /*------------------------------------------------------------------------
     *-- READ-WRITE OPTIONS --------------------------------------------------
     *------------------------------------------------------------------------*/
    TIMEZONE(
            "reefdb.timezone",
            n("reefdb.config.option.timezone.description"),
            "",
            String.class,
            false),
    DB_BACKUP_EXTERNAL_DIRECTORY(
            "reefdb.persistence.db.backup.external.directory",
            n("reefdb.config.option.persistence.db.backup.external.directory.description"),
            null,
            File.class,
            false),
    DB_ENUMERATION_RESOURCE(
            "reefdb.persistence.db.enumeration.path",
            n("reefdb.config.option.persistence.db.enumeration.path.description"),
            "classpath*:quadrige3-db-enumerations.properties,classpath*:reefdb-db-enumerations.properties",
            String.class,
            false),
    DB_TIMEZONE(
            "reefdb.persistence.db.timezone",
            n("reefdb.config.option.persistence.db.timezone.description"),
            "", // no default, this option must be provided in config file (Mantis #46461)
            String.class,
            false),
    DB_VALIDATION_BULK_SIZE(
            "reefdb.persistence.db.validation.bulkSize",
            n("reefdb.config.option.persistence.db.validation.bulkSize.description"),
            "100",
            Integer.class,
            false),
    UNKNOWN_RECORDER_DEPARTMENT(
            "reefdb.department.recorder.default.id",
            n("reefdb.config.option.department.recorder.default.id.description"),
            "0",
            String.class,
            false),
    UI_CONFIG_FILE(
            "reefdb.ui.config.file",
            n("reefdb.config.option.ui.config.file.description"),
            "${quadrige3.data.directory}/reefdbUI.xml",
            File.class,
            false),
    UI_RECOMMENDED_WIDTH(
            "reefdb.ui.recommended.width",
            n("reefdb.config.option.ui.recommended.width.description"),
            String.valueOf(1280),
            Integer.class,
            false),
    UI_RECOMMENDED_HEIGHT(
            "reefdb.ui.recommended.height",
            n("reefdb.config.option.ui.recommended.height.description"),
            String.valueOf(1024),
            Integer.class,
            false),
    TABLES_CHECKBOX(
            "reefdb.ui.table.showCheckbox",
            n("reefdb.config.option.ui.table.showCheckbox.description"),
            String.valueOf(false),
            Boolean.class,
            false),
    AUTO_POPUP_NUMBER_EDITOR(
            "reefdb.ui.autoPopupNumberEditor",
            n("reefdb.config.option.ui.autoPopupNumberEditor.description"),
            String.valueOf(false),
            Boolean.class,
            false),
    SHOW_NUMBER_EDITOR_BUTTON(
            "reefdb.ui.showNumberEditorButton",
            n("reefdb.config.option.ui.showNumberEditorButton.description"),
            String.valueOf(true),
            Boolean.class,
            false),
    COLOR_ROW_READ_ONLY(
            "reefdb.ui.color.rowReadOnly",
            n("reefdb.config.option.ui.color.rowReadOnly.description"),
            new Color(230, 230, 230).toString(),
            Color.class,
            false),
    COLOR_ROW_INVALID(
            "reefdb.ui.color.rowInvalid",
            n("reefdb.config.option.ui.color.rowInvalid.description"),
            new Color(255, 204, 153).toString(),
            Color.class,
            false),
    COLOR_CELL_WITH_VALUE(
            "reefdb.ui.color.cellWithValue",
            n("reefdb.config.option.ui.color.cellWithValue.description"),
            new Color(128, 255, 128).toString(),
            Color.class,
            false),
    COLOR_ALTERNATE_ROW(
            "reefdb.ui.color.alternateRow",
            n("reefdb.config.option.ui.color.alternateRow.description"),
            new Color(255, 255, 232).toString(),
            Color.class,
            false),
    COLOR_SELECTED_ROW(
            "reefdb.ui.color.selectedRow",
            n("reefdb.config.option.ui.color.selectedRow.description"),
            new Color(0, 0, 128).toString(),
            Color.class,
            false),
    COLOR_BLOCKING_LAYER(
            "reefdb.ui.color.blockingLayer",
            n("reefdb.config.option.ui.color.blockingLayer.description"),
            Color.BLACK.toString(),
            Color.class,
            false),
    COLOR_COMPUTED_WEIGHTS(
            "reefdb.ui.color.computedWeights",
            n("reefdb.config.option.ui.color.computedWeights.description"),
            Color.BLUE.toString(),
            Color.class,
            false),
    COLOR_SELECTED_CELL(
            "reefdb.ui.color.selectedCell",
            n("reefdb.config.option.ui.color.selectedCell.description"),
            Color.BLACK.toString(),
            Color.class,
            false),
    COLOR_SELECTION_PANEL_BACKGROUND(
            "reefdb.ui.color.selectionPanelBackground",
            n("reefdb.config.option.ui.color.selectionPanelBackground.description"),
            new Color(144, 211, 253).toString(),
            Color.class,
            false),
    COLOR_CONTEXT_PANEL_BACKGROUND(
            "reefdb.ui.color.contextPanelBackground",
            n("reefdb.config.option.ui.color.contextPanelBackground.description"),
            new Color(136, 208, 238).toString(),
            Color.class,
            false),
    COLOR_EDITION_PANEL_BACKGROUND(
            "reefdb.ui.color.editionPanelBackground",
            n("reefdb.config.option.ui.color.editionPanelBackground.description"),
            new Color(210, 237, 254).toString(),
            Color.class,
            false),
    COLOR_EDITION_PANEL_BORDER(
            "reefdb.ui.color.editionPanelBorder",
            n("reefdb.config.option.ui.color.editionPanelBorder.description"),
            new Color(0, 144, 188).toString(),
            Color.class,
            false),
    COLOR_THEMATIC_LABEL(
            "reefdb.ui.color.thematicLabel",
            n("reefdb.config.option.ui.color.thematicLabel.description"),
            new Color(0, 0, 128).toString(),
            Color.class,
            false),
    COLOR_HIGHLIGHT_BUTTON_BORDER(
            "reefdb.ui.color.highlightButtonBorder",
            n("reefdb.config.option.ui.color.highlightButtonBorder.description"),
            new Color(209, 0, 209).toString(),
            Color.class,
            false),
    SHORTCUT_CLOSE_POPUP(
            "reefdb.ui.shortcut.closePopup",
            n("reefdb.config.option.ui.shortcut.closePopup.description"),
            "alt pressed F",
            KeyStroke.class,
            false),
    DATE_FORMAT(
            "reefdb.ui.dateFormat",
            n("reefdb.config.option.ui.dateFormat.description"),
            "dd/MM/yyyy",
            String.class,
            false),
    DATE_TIME_FORMAT(
            "reefdb.ui.dateTimeFormat",
            n("reefdb.config.option.ui.dateTimeFormat.description"),
            "dd/MM/yyyy HH:mm",
            String.class,
            false),
    LAST_OBSERVATION_ID(
            "reefdb.lastObservationId",
            n("reefdb.config.option.lastObservationId.description"),
            null,
            Integer.class,
            false),
    LAST_CONTEXT_ID(
            "reefdb.lastContextId",
            n("reefdb.config.option.lastContextId.description"),
            null,
            Integer.class,
            false),

    /* CONSTANT ENUMERATIONS */
    PMFM_ID_DEPTH_VALUES(
            "reefdb.pmfm.id.depthValues",
            n("reefdb.config.option.pmfm.id.depthValues.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmId.DEPTH_VALUES"),  // 630 (test) & 7119 (prod)
            Integer.class,
            false),
    UNIT_ID_NO_UNIT(
            "reefdb.unit.id.noUnit",
            n("reefdb.config.option.unit.id.noUnit.description"),
            String.format("${%s}", UnitId.NO_UNIT.getKey()),  // 99
            Integer.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_PMFM_NM(
            "reefdb.transcribingItemType.label.pmfmNm",
            n("reefdb.config.option.transcribingItemType.label.pmfmNm.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.REEFDB_PMFM_NM"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_PMFM_EXTRACTION(
            "reefdb.transcribingItemType.label.pmfmExtraction",
            n("reefdb.config.option.transcribingItemType.label.pmfmExtraction.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.REEFDB_PMFM_EXTRACTION"),
            String.class,
            false),
    TRANSCRIBING_ITEM_TYPE_LB_WORMS(
        "reefdb.transcribingItemType.label.worms",
        n("reefdb.config.option.transcribingItemType.label.worms.description"),
        String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.WORMS"),
        String.class,
        false),
    TRANSCRIBING_ITEM_TYPE_LB_TAXREF(
        "reefdb.transcribingItemType.label.taxRef",
        n("reefdb.config.option.transcribingItemType.label.taxRef.description"),
        String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.TAXREF"),
        String.class,
        false),
    TRANSCRIBING_ITEM_TYPE_LB_PAMPA(
        "reefdb.transcribingItemType.label.pampa",
        n("reefdb.config.option.transcribingItemType.label.pampa.description"),
        String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "TranscribingItemTypeLb.PAMPA"),
        String.class,
        false),

    /* constants used to calculate transition length */
    PMFM_IDS_TRANSITION_LENGTH(
            "reefdb.pmfm.ids.transitionLength",
            n("reefdb.config.option.pmfm.ids.transitionLength.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmIds.TRANSITION_LENGTH"),  // =transitionLengthPmfmId,startPositionPmfmId,endPositionPmfmId
            String.class,
            false),
//    PMFM_ID_TRANSITION_LENGTH(
//            "reefdb.pmfm.id.transitionLength",
//            n("reefdb.config.option.pmfm.id.transitionLength.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmId.TRANSITION_LENGTH"),  // 3265
//            Integer.class,
//            false),
//    PMFM_ID_START_POSITION(
//            "reefdb.pmfm.id.startPosition",
//            n("reefdb.config.option.pmfm.id.startPosition.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmId.START_POSITION"),  // 2288
//            Integer.class,
//            false),
//    PMFM_ID_END_POSITION(
//            "reefdb.pmfm.id.endPosition",
//            n("reefdb.config.option.pmfm.id.endPosition.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmId.END_POSITION"),  // 3175 ou 2294
//            Integer.class,
//            false),
    /* constants used to initialize data grid (PIT protocol) */
    PMFM_ID_PIT_TRANSITION(
            "reefdb.pmfm.ids.pitTransition",
            n("reefdb.config.option.pmfm.ids.pitTransition.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmIds.PIT_TRANSITION"),  // 13381
            Integer.class,
            false),
    PMFM_ID_PIT_TRANSECT_LENGTH(
            "reefdb.pmfm.ids.pitTransectLength",
            n("reefdb.config.option.pmfm.ids.pitTransectLength.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmIds.PIT_TRANSECT_LENGTH"),  // 2322
            Integer.class,
            false),
    PIT_TRANSECT_LENGTH_DEFAULT_VALUE(
            "reefdb.pitTransectLength.defaultValue",
            n("reefdb.config.option.pitTransectLength.defaultValue.description"),
            "6000", // cm
            Integer.class,
            false),
    PMFM_ID_PIT_ORIGIN(
            "reefdb.pmfm.ids.pitOrigin",
            n("reefdb.config.option.pmfm.ids.pitOrigin.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "pmfmIds.PIT_ORIGIN"),  // 2326
            Integer.class,
            false),
    PIT_ORIGIN_DEFAULT_VALUE(
            "reefdb.pitOrigin.defaultValue",
            n("reefdb.config.option.pitOrigin.defaultValue.description"),
            "0",
            Integer.class,
            false),

    /* ------------ ALTERNATIVE_TAXON --------------
     */
//    ALTERNATIVE_TAXON_ORIGIN_TAXREF("reefdb.alternativeTaxonOrigin.taxRef",
//            n("reefdb.config.option.alternativeTaxonOrigin.taxRef.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "AlternTaxonOriginCd.TAXREF"),
//            String.class,
//            false),
//    ALTERNATIVE_TAXON_ORIGIN_WORMS("reefdb.alternativeTaxonOrigin.worms",
//            n("reefdb.config.option.alternativeTaxonOrigin.worms.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "AlternTaxonOriginCd.WORMS"),
//            String.class,
//            false),
//    ALTERNATIVE_TAXON_ORIGIN_PAMPA("reefdb.alternativeTaxonOrigin.pampa",
//            n("reefdb.config.option.alternativeTaxonOrigin.pampa.description"),
//            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "AlternTaxonOriginCd.PAMPA"),
//            String.class,
//            false),

    // EXTRACTION

    EXTRACTION_FILE_PREFIX(
            "reefdb.extraction.file.prefix",
            n("reefdb.config.option.extraction.file.prefix.description"),
            "reefdb-extraction",
            String.class,
            false),
    EXTRACTION_FILE_EXTENSION(
            "reefdb.extraction.file.extension",
            n("reefdb.config.option.extraction.file.extension.description"),
            "csv",
            String.class,
            false),
    EXTRACTION_FILE_TYPE_CODE(
            "reefdb.extraction.fileType.code",
            n("reefdb.config.option.extraction.fileType.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "fileTypeCd.REEFDB_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_TABLE_TYPE_CODE(
            "reefdb.extraction.tableType.code",
            n("reefdb.config.option.extraction.tableType.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "tableTypeCd.REEFDB_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_GROUP_TYPE_PMFM_CODE(
            "reefdb.extraction.groupTypePmfm.code",
            n("reefdb.config.option.extraction.groupTypePmfm.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "groupTypePmfmCd.REEFDB_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_PROJECTION_SYSTEM_CODE(
            "reefdb.extraction.projectionSystem.code",
            n("reefdb.config.option.extraction.projectionSystem.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "projectionSystemCd.REEFDB_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_DEFAULT_ORDER_ITEM_TYPE_CODE(
            "reefdb.extraction.default.orderItemType.code",
            n("reefdb.config.option.extraction.default.orderItemType.code.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "orderItemTypeCd.REEFDB_EXTRACTION"),
            String.class,
            false),
    EXTRACTION_IGNORE_UNIT_IDS(
            "reefdb.extraction.ignore.unit.ids",
            n("reefdb.config.option.extraction.ignore.unit.ids.description"),
            String.format("${%s%s}", QuadrigeEnumerationDef.CONFIG_OPTION_PREFIX, "unitId.REEFDB_EXTRACTION_EXCLUSION"),
            String.class,
            false)
    ;

    /**
     * Configuration key.
     */
    private final String key;

    /**
     * I18n key of option description
     */
    private final String description;

    /**
     * Type of option
     */
    private final Class<?> type;

    /**
     * Default value of option.
     */
    private String defaultValue;

    /**
     * Flag to not keep option value on disk
     */
    private final boolean isTransient;

    /**
     * Flag to not allow option value modification
     */
    private final boolean isFinal;

    ReefDbConfigurationOption(String key,
                              String description,
                              String defaultValue,
                              Class<?> type,
                              boolean isTransient) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isTransient;
    }

    ReefDbConfigurationOption(String key,
                              String description,
                              String defaultValue,
                              Class<?> type) {
        this(key, description, defaultValue, type, true);
    }

    /** {@inheritDoc} */
    @Override
    public String getKey() {
        return key;
    }

    /** {@inheritDoc} */
    @Override
    public Class<?> getType() {
        return type;
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return description;
    }

    /** {@inheritDoc} */
    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTransient() {
        return isTransient;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFinal() {
        return isFinal;
    }

    /** {@inheritDoc} */
    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /** {@inheritDoc} */
    @Override
    public void setTransient(boolean bln) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /** {@inheritDoc} */
    @Override
    public void setFinal(boolean bln) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
