package fr.ifremer.reefdb.config;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.base.Splitter;
import fr.ifremer.common.synchro.config.SynchroConfigurationOption;
import fr.ifremer.quadrige3.core.config.QuadrigeConfigurationOption;
import fr.ifremer.quadrige3.core.config.QuadrigeConfigurations;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfiguration;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfigurationOption;
import fr.ifremer.quadrige3.core.dao.technical.hibernate.DateType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * ReefDb Configuration
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public final class ReefDbConfiguration extends QuadrigeCoreConfiguration {

    /**
     * Logger.
     */
    private static final Log LOG = LogFactory.getLog(ReefDbConfiguration.class);
    /**
     * Singleton pattern
     */
    private static ReefDbConfiguration instance;

    /**
     * <p>Constructor for ReefDbConfiguration.</p>
     *
     * @param applicationConfig a {@link org.nuiton.config.ApplicationConfig} object.
     */
    public ReefDbConfiguration(ApplicationConfig applicationConfig) {
        super(applicationConfig);
    }

    /**
     * <p>Constructor for ReefDbConfiguration.</p>
     *
     * @param file a {@link java.lang.String} object.
     * @param args a {@link java.lang.String} object.
     */
    public ReefDbConfiguration(String file, String... args) {
        super(file, args);

        // Verify base directory
        File reefdbBasedir = getBaseDirectory();
        if (reefdbBasedir == null) {
            reefdbBasedir = new File("");
        }
        if (!reefdbBasedir.isAbsolute()) {
            reefdbBasedir = new File(reefdbBasedir.getAbsolutePath());
        }
        if ("..".equals(reefdbBasedir.getName())) {
            reefdbBasedir = reefdbBasedir.getParentFile().getParentFile();
        }
        if (".".equals(reefdbBasedir.getName())) {
            reefdbBasedir = reefdbBasedir.getParentFile();
        }
        if (LOG.isInfoEnabled()) {
            LOG.info("Application basedir: " + reefdbBasedir);
        }
        applicationConfig.setOption(
                ReefDbConfigurationOption.BASEDIR.getKey(),
                reefdbBasedir.getAbsolutePath());

        // Init timezone (see mantis Allegro-ObsDeb #24623)
        initTimeZone();
    }

    /**
     * <p>Getter for the field <code>instance</code>.</p>
     *
     * @return a {@link fr.ifremer.reefdb.config.ReefDbConfiguration} object.
     */
    public static ReefDbConfiguration getInstance() {
        return instance;
    }

    /**
     * <p>Setter for the field <code>instance</code>.</p>
     *
     * @param instance a {@link fr.ifremer.reefdb.config.ReefDbConfiguration} object.
     */
    public static void setInstance(ReefDbConfiguration instance) {
        ReefDbConfiguration.instance = instance;
        QuadrigeCoreConfiguration.setInstance(instance);
    }

    /**
     * <p>overrideExternalModulesDefaultOptions.</p>
     */
    protected void overrideExternalModulesDefaultOptions() {
        super.overrideExternalModulesDefaultOptions();

        // Override quadrige3 BASE_DIRECTORY
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.BASEDIR.getKey(),
                String.format("${%s}", ReefDbConfigurationOption.BASEDIR.getKey()));

        // Override Sites
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.SITE_URL.getKey(),
                String.format("${%s}", ReefDbConfigurationOption.SITE_URL.getKey()));
        // Remap :
        //  - quadrige3.update.XXX to reefdb.update.XXX
        //  - quadrige3.install.XXX to reefdb.install.XXX
        //  - quadrige3.authentication.XXX to reefdb.authentication.XXX
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.update",
                ReefDbConfigurationOption.values(),
                "reefdb.update");
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.install",
                ReefDbConfigurationOption.values(),
                "reefdb.install");
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.authentication",
                ReefDbConfigurationOption.values(),
                "reefdb.authentication");

        // Override Hibernate queries file
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.HIBERNATE_CLIENT_QUERIES_FILE.getKey(),
                "reefdb-queries.hbm.xml");

        // Override enumeration files
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.DB_ENUMERATION_RESOURCE.getKey(),
                String.format("${%s}", ReefDbConfigurationOption.DB_ENUMERATION_RESOURCE.getKey()));

        // Override Db timezone
        applicationConfig.setDefaultOption(
                QuadrigeConfigurationOption.DB_TIMEZONE.getKey(),
                String.format("${%s}", ReefDbConfigurationOption.DB_TIMEZONE.getKey()));
        applicationConfig.setDefaultOption(
                SynchroConfigurationOption.DB_TIMEZONE.getKey(),
                String.format("${%s}", ReefDbConfigurationOption.DB_TIMEZONE.getKey()));

        // Override ALL synchronization options
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "synchro",
                ReefDbConfigurationOption.values(),
                "reefdb.synchro");
        QuadrigeConfigurations.remapOptionsToPrefix(applicationConfig,
                "quadrige3.synchro",
                ReefDbConfigurationOption.values(),
                "reefdb.synchro");

        remapOptionsForBackwardCompatibility();
    }

    /**
     * remap (add) property from older reefdb version < 3.0.0
     */
    private void remapOptionsForBackwardCompatibility() {

        // authentication options
        restoreOldOptions(QuadrigeCoreConfigurationOption.values(), "quadrige3.authentication", "reefdb.authentication");
        restoreOldOptions(QuadrigeCoreConfigurationOption.values(), "quadrige3.authentication", "quadrige2.authentication");

        // update options
        restoreOldOptions(QuadrigeCoreConfigurationOption.values(), "quadrige3.update", "reefdb.update");
        restoreOldOptions(QuadrigeCoreConfigurationOption.values(), "quadrige3.install", "reefdb.install");

        // synchronization options
        restoreOldOptions(QuadrigeConfigurationOption.values(), "quadrige3.synchronization", "reefdb.synchronization");
        restoreOldOptions(QuadrigeConfigurationOption.values(), "quadrige3.synchro", "quadrige2.synchro");

        // manage enumeration
        restoreOldOptions("quadrige3.enumeration", "quadrige2.enumeration");
    }

    private void restoreOldOptions(ConfigOptionDef[] optionDefs,
                                   String newKeyPrefix,
                                   String oldKeyPrefix) {

        for (ConfigOptionDef optionDef: optionDefs) {
            String newProperty = optionDef.getKey();

            // If prefix match
            if (newProperty.startsWith(newKeyPrefix)) {
                String endPart = newProperty.substring(newKeyPrefix.length());
                String oldProperty = oldKeyPrefix + endPart;

                // This property could be copy
                if (applicationConfig.hasOption(oldProperty)) {
                    applicationConfig.setOption(newProperty, applicationConfig.getOption(oldProperty));
                }
            }
        }
    }

    private void restoreOldOptions(String newKeyPrefix,
                                   String oldKeyPrefix) {

        Properties oldProperties = applicationConfig.getOptionStartsWith(oldKeyPrefix);
        if (oldProperties != null) {
            for (String oldKey: oldProperties.stringPropertyNames()) {

                String endPart = oldKey.substring(oldKeyPrefix.length());
                String newProperty = newKeyPrefix + endPart;

                // override this new property if not already exists
                if (!applicationConfig.hasOption(newProperty)) {
                    applicationConfig.setOption(newProperty, oldProperties.getProperty(oldKey));
                }

                // delete this old property from file
                addOptionToNotSave(oldKey);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public String getApplicationName() {
        return t("reefdb.application.name");
    }

    /** {@inheritDoc} */
    @Override
    public Version getVersion() {
        return applicationConfig.getOptionAsVersion(ReefDbConfigurationOption.VERSION.getKey());
    }

    /** {@inheritDoc} */
    @Override
    public KeyStroke getShortcutClosePopup() {
        return applicationConfig.getOptionAsKeyStroke(ReefDbConfigurationOption.SHORTCUT_CLOSE_POPUP.getKey());
    }

    /**
     * <p>getHelpResourceWithLocale.</p>
     *
     * @param value a {@link String} object.
     * @return a {@link String} object.
     */
    public String getHelpResourceWithLocale(String value) {
        File directory = new File(getHelpDirectory(),
                getI18nLocale().getLanguage());
        File result = new File(directory, value);
        return result.toString();
    }

    /**
     * Initialization default timezone, from configuration (mantis #24623)
     */
    private void initTimeZone() {

        String timeZone = applicationConfig.getOption(ReefDbConfigurationOption.TIMEZONE.getKey());
        if (StringUtils.isNotBlank(timeZone)) {
            if (LOG.isInfoEnabled()) {
                LOG.info(String.format("Using timezone [%s]", timeZone));
            }
            TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
            System.setProperty("user.timezone", timeZone);
        } else if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Using default timezone [%s]", System.getProperty("user.timezone")));
        }

        // Warn only and use user timezone if reefdb.persistence.db.timezone is not set (Mantis #46461)
        String dbTimeZoneOption = applicationConfig.getOption(ReefDbConfigurationOption.DB_TIMEZONE.getKey());
        TimeZone dbTimeZone;
        if (StringUtils.isBlank(dbTimeZoneOption)) {
            LOG.warn("Timezone for database is not set in configuration, now using user timezone");
            dbTimeZone = TimeZone.getDefault();
        } else {
            dbTimeZone = TimeZone.getTimeZone(dbTimeZoneOption);
        }
        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("Using timezone [%s] for database", dbTimeZone.toZoneId()));
        }
        DateType.setTimeZone(dbTimeZone);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /**
     * <p>getLastObservationId.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getLastObservationId() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.LAST_OBSERVATION_ID.getKey());
    }

    /**
     * <p>setLastObservationId.</p>
     *
     * @param observationId a {@link Integer} object.
     */
    public void setLastObservationId(Integer observationId) {
        String value = "";
        if (observationId != null) {
            value = observationId.toString();
        }
        applicationConfig.setOption(ReefDbConfigurationOption.LAST_OBSERVATION_ID.getKey(), value);
    }

    /**
     * <p>getLastContextId.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getLastContextId() {
        int lastContextId = applicationConfig.getOptionAsInt(ReefDbConfigurationOption.LAST_CONTEXT_ID.getKey());
        return lastContextId == 0 ? null : lastContextId;
    }

    /**
     * <p>setLastContextId.</p>
     *
     * @param contextId a {@link Integer} object.
     */
    public void setLastContextId(Integer contextId) {
        String value = "";
        if (contextId != null) {
            value = contextId.toString();
        }
        applicationConfig.setOption(ReefDbConfigurationOption.LAST_CONTEXT_ID.getKey(), value);
    }

    //------------------------------------------------------------------------//
    //--- DB-related ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getDbBackupExternalDirectory.</p>
     *
     * @return a {@link File} object.
     */
    public File getDbBackupExternalDirectory() {
        return applicationConfig.getOptionAsFile(ReefDbConfigurationOption.DB_BACKUP_EXTERNAL_DIRECTORY.getKey());
    }

    /**
     * <p>isDbCheckConstantsEnable.</p>
     *
     * @return a boolean.
     */
    public boolean isDbCheckConstantsEnable() {
        return applicationConfig.getOptionAsBoolean(ReefDbConfigurationOption.DB_CHECK_CONSTANTS.getKey());
    }

    /**
     * <p>setDbCheckConstantsEnable.</p>
     *
     * @param check a boolean.
     */
    public void setDbCheckConstantsEnable(boolean check) {
        applicationConfig.setOption(ReefDbConfigurationOption.DB_CHECK_CONSTANTS.getKey(), Boolean.toString(check));
    }

    /**
     * <p>getDbDirectory.</p>
     *
     * @return a {@link File} object.
     */
    public TimeZone getDbTimezone() {
        String timezone = applicationConfig.getOption(ReefDbConfigurationOption.DB_TIMEZONE.getKey());
        return StringUtils.isNotBlank(timezone) ? TimeZone.getTimeZone(timezone) : TimeZone.getDefault();
    }

    public int getMassiveProcessChunkSize() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.DB_VALIDATION_BULK_SIZE.getKey());
    }

    /**
     * <p>isCacheEnabledAtStartup.</p>
     *
     * @return a boolean.
     */
    public boolean isCacheEnabledAtStartup() {
        return applicationConfig.getOptionAsBoolean(ReefDbConfigurationOption.CACHE_ENABLED_AT_STARTUP.getKey());
    }

    /**
     * <p>isCleanCacheAtStartup.</p>
     *
     * @return a boolean.
     */
    public boolean isCleanCacheAtStartup() {
        return applicationConfig.getOptionAsBoolean(ReefDbConfigurationOption.CACHE_CLEAN_AT_STARTUP.getKey());
    }

    /**
     * <p>getSiteUrl.</p>
     *
     * @return {@link fr.ifremer.reefdb.config.ReefDbConfigurationOption#SITE_URL} value
     */
    public URL getSiteUrl() {
        return applicationConfig.getOptionAsURL(ReefDbConfigurationOption.SITE_URL.getKey());
    }

    /**
     * <p>getOrganizationName.</p>
     *
     * @return {@link fr.ifremer.reefdb.config.ReefDbConfigurationOption#ORGANIZATION_NAME} value
     */
    public String getOrganizationName() {
        return applicationConfig.getOption(ReefDbConfigurationOption.ORGANIZATION_NAME.getKey());
    }

    /**
     * <p>getInceptionYear.</p>
     *
     * @return {@link fr.ifremer.reefdb.config.ReefDbConfigurationOption#INCEPTION_YEAR} value
     */
    public int getInceptionYear() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.INCEPTION_YEAR.getKey());
    }


    //------------------------------------------------------------------------//
    //--- extraction-related --------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getExtractionFilePrefix.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionFilePrefix() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_FILE_PREFIX.getKey());
    }

    /**
     * <p>getExtractionFileExtension.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionFileExtension() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_FILE_EXTENSION.getKey());
    }

    /**
     * <p>getExtractionFileTypeCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionFileTypeCode() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_FILE_TYPE_CODE.getKey());
    }

    /**
     * <p>getExtractionGroupTypePmfmCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionGroupTypePmfmCode() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_GROUP_TYPE_PMFM_CODE.getKey());
    }

    /**
     * <p>getExtractionTableTypeCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionTableTypeCode() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_TABLE_TYPE_CODE.getKey());
    }

    /**
     * <p>getExtractionProjectionSystemCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionProjectionSystemCode() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_PROJECTION_SYSTEM_CODE.getKey());
    }

    /**
     * <p>getExtractionDefaultOrderItemTypeCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getExtractionDefaultOrderItemTypeCode() {
        return applicationConfig.getOption(ReefDbConfigurationOption.EXTRACTION_DEFAULT_ORDER_ITEM_TYPE_CODE.getKey());
    }

    /**
     * <p>getExtractionUnitIdsToIgnore.</p>
     *
     * @return a {@link List} object.
     */
    public List<Integer> getExtractionUnitIdsToIgnore() {
        return splitStringToIntegerList(ReefDbConfigurationOption.EXTRACTION_IGNORE_UNIT_IDS);
    }

    //------------------------------------------------------------------------//
    //--- UI-related ---------------------------------------------------------//
    //------------------------------------------------------------------------//

    /**
     * <p>getUIConfigFile.</p>
     *
     * @return {@link fr.ifremer.reefdb.config.ReefDbConfigurationOption#UI_CONFIG_FILE} value
     */
    public File getUIConfigFile() {
        return applicationConfig.getOptionAsFile(ReefDbConfigurationOption.UI_CONFIG_FILE.getKey());
    }

    /**
     * <p>getUIRecommendedWidth.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getUIRecommendedWidth() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.UI_RECOMMENDED_WIDTH.getKey());
    }

    /**
     * <p>getUIRecommendedHeight.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getUIRecommendedHeight() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.UI_RECOMMENDED_HEIGHT.getKey());
    }

    /**
     * <p>isShowTableCheckbox.</p>
     *
     * @return {@link fr.ifremer.reefdb.config.ReefDbConfigurationOption#TABLES_CHECKBOX} value
     */
    public boolean isShowTableCheckbox() {
        return applicationConfig.getOptionAsBoolean(ReefDbConfigurationOption.TABLES_CHECKBOX.getKey());
    }

    /**
     * <p>isAutoPopupNumberEditor.</p>
     *
     * @return a boolean.
     */
    public boolean isAutoPopupNumberEditor() {
        return applicationConfig.getOptionAsBoolean(ReefDbConfigurationOption.AUTO_POPUP_NUMBER_EDITOR.getKey());
    }

    /**
     * <p>isShowNumberEditorButton.</p>
     *
     * @return a boolean.
     */
    public boolean isShowNumberEditorButton() {
        return applicationConfig.getOptionAsBoolean(ReefDbConfigurationOption.SHOW_NUMBER_EDITOR_BUTTON.getKey());
    }

    /**
     * <p>getColorSelectedCell.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorSelectedCell() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_SELECTED_CELL.getKey());
    }

    /**
     * <p>getColorRowInvalid.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorRowInvalid() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_ROW_INVALID.getKey());
    }

    /**
     * <p>getColorRowReadOnly.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorRowReadOnly() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_ROW_READ_ONLY.getKey());
    }

    /**
     * <p>getColorCellWithValue.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorCellWithValue() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_CELL_WITH_VALUE.getKey());
    }

    /**
     * <p>getColorComputedWeights.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorComputedWeights() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_COMPUTED_WEIGHTS.getKey());
    }

    /**
     * <p>getColorComputedRow.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorComputedRow() {
        // same color as COLOR_COMPUTED_WEIGHTS
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_COMPUTED_WEIGHTS.getKey());
    }

    /**
     * <p>getColorBlockingLayer.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorBlockingLayer() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_BLOCKING_LAYER.getKey());
    }

    /**
     * <p>getColorAlternateRow.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorAlternateRow() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_ALTERNATE_ROW.getKey());
    }

    /**
     * <p>getColorSelectedRow.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorSelectedRow() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_SELECTED_ROW.getKey());
    }

    /**
     * <p>getColorSelectionPanelBackground.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorSelectionPanelBackground() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_SELECTION_PANEL_BACKGROUND.getKey());
    }

    /**
     * <p>getColorContextPanelBackground.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorContextPanelBackground() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_CONTEXT_PANEL_BACKGROUND.getKey());
    }

    /**
     * <p>getColorEditionPanelBackground.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorEditionPanelBackground() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_EDITION_PANEL_BACKGROUND.getKey());
    }

    /**
     * <p>getColorEditionPanelBorder.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorEditionPanelBorder() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_EDITION_PANEL_BORDER.getKey());
    }

    /**
     * <p>getColorThematicLabel.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorThematicLabel() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_THEMATIC_LABEL.getKey());
    }

    /**
     * <p>getColorHighlightButtonBorder.</p>
     *
     * @return a {@link Color} object.
     */
    public Color getColorHighlightButtonBorder() {
        return applicationConfig.getOptionAsColor(ReefDbConfigurationOption.COLOR_HIGHLIGHT_BUTTON_BORDER.getKey());
    }

    /**
     * <p>getShortCut.</p>
     *
     * @param actionName a {@link String} object.
     * @return a {@link KeyStroke} object.
     */
    public KeyStroke getShortCut(String actionName) {
        return applicationConfig.getOptionAsKeyStroke("reefdb.ui." + actionName);
    }

    /**
     * <p>getDateFormat.</p>
     *
     * @return a {@link String} object.
     */
    public String getDateFormat() {
        return applicationConfig.getOption(ReefDbConfigurationOption.DATE_FORMAT.getKey());
    }

    /**
     * <p>getDateTimeFormat.</p>
     *
     * @return a {@link String} object.
     */
    public String getDateTimeFormat() {
        return applicationConfig.getOption(ReefDbConfigurationOption.DATE_TIME_FORMAT.getKey());
    }

    /*
     ----  File and Directory methods ---
     */
    /**
     * <p>getNewTmpDirectory.</p>
     *
     * @param name a {@link String} object.
     * @return a {@link File} object.
     */
    public File getNewTmpDirectory(String name) {
        return new File(getTempDirectory(), name + "_" + System.nanoTime());
    }


    /**
     * <p>getHelpDirectory.</p>
     *
     * @return a {@link File} object.
     */
    public File getHelpDirectory() {
        return applicationConfig.getOptionAsFile(
                ReefDbConfigurationOption.HELP_DIRECTORY.getKey());
    }

    /**
     * <p>getUnknownRecorderDepartmentId.</p>
     *
     * @return a int.
     */
    public int getUnknownRecorderDepartmentId() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.UNKNOWN_RECORDER_DEPARTMENT.getKey());
    }

    // TODO les methodes suivantes sont-elles toujours utiles ?
    /**
     * <p>getTemporaryStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getTemporaryStatusCode() {
        return "TEMPORARY";
    }

    /**
     * <p>getEnableStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getEnableStatusCode() {
        return "ENABLE";
    }

    /**
     * <p>getDisableStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getDisableStatusCode() {
        return "DISABLE";
    }

    /**
     * <p>getDeletedStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getDeletedStatusCode() {
        return "DELETED";
    }

    /**
     * <p>getDirtySynchronizationStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getDirtySynchronizationStatusCode() {
        return "DIRTY";
    }

    /**
     * <p>getReadySynchronizationStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getReadySynchronizationStatusCode() {
        return "READY_TO_SYNCHRONIZE";
    }

    /**
     * <p>getSynchronizedSynchronizationStatusCode.</p>
     *
     * @return a {@link String} object.
     */
    public String getSynchronizedSynchronizationStatusCode() {
        return "SYNCHRONIZED";
    }

    /**
     * Constants
     *
     * @return a int.
     */
    public int getDepthValuesPmfmId() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.PMFM_ID_DEPTH_VALUES.getKey());
    }

    /**
     * <p>getTransitionLengthPmfmId.</p>
     *
     * @return a int.
     */
    @Deprecated
    public int getTransitionLengthPmfmId() {
//        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.PMFM_ID_TRANSITION_LENGTH.getKey());
        return 0;
    }

    /**
     * <p>getStartPositionPmfmId.</p>
     *
     * @return a int.
     */
    @Deprecated
    public int getStartPositionPmfmId() {
//        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.PMFM_ID_START_POSITION.getKey());
        return 0;
    }

    /**
     * <p>getEndPositionPmfmId.</p>
     *
     * @return a int.
     */
    @Deprecated
    public int getEndPositionPmfmId() {
//        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.PMFM_ID_END_POSITION.getKey());
        return 0;
    }

    public List<Integer[]> getCalculatedTransitionLengthPmfmTriplets() {

        List<Integer[]> result = new ArrayList<>();
        String configOption = applicationConfig.getOption(ReefDbConfigurationOption.PMFM_IDS_TRANSITION_LENGTH.getKey());
        if (StringUtils.isBlank(configOption)) return result;

        List<String> triplets = Splitter.on(";").omitEmptyStrings().splitToList(configOption);
        for (String triplet : triplets) {
            List<String> pmfmsIdsStr = Splitter.on(",").omitEmptyStrings().trimResults().splitToList(triplet);
            if (CollectionUtils.size(pmfmsIdsStr) == 3) {
                try {
                    Integer[] pmfmIds = new Integer[3];
                    pmfmIds[0] = Integer.parseInt(pmfmsIdsStr.get(0));
                    pmfmIds[1] = Integer.parseInt(pmfmsIdsStr.get(1));
                    pmfmIds[2] = Integer.parseInt(pmfmsIdsStr.get(2));

                    if (pmfmIds[0] < 0 || pmfmIds[1] < 0 || pmfmIds[2] < 0) continue;

                    result.add(pmfmIds);
                } catch (NumberFormatException e) {
                    LOG.warn(String.format("Problem parsing '%s' option value : %s", ReefDbConfigurationOption.PMFM_IDS_TRANSITION_LENGTH.getKey(), triplet));
                }
            } else {
                LOG.warn(String.format("Malformed '%s' option value : %s", ReefDbConfigurationOption.PMFM_IDS_TRANSITION_LENGTH.getKey(), triplet));
            }
        }

        return result;
    }

    public List<Integer> getPitTransitionPmfmIds() {
        return splitStringToIntegerList(ReefDbConfigurationOption.PMFM_ID_PIT_TRANSITION);
    }

    public List<Integer> getPitTransectLengthPmfmIds() {
        return splitStringToIntegerList(ReefDbConfigurationOption.PMFM_ID_PIT_TRANSECT_LENGTH);
    }

    public List<Integer> getPitOriginPmfmIds() {
        return splitStringToIntegerList(ReefDbConfigurationOption.PMFM_ID_PIT_ORIGIN);
    }

    private List<Integer> splitStringToIntegerList(ReefDbConfigurationOption option) {
        String value = applicationConfig.getOption(option.getKey());
        if (StringUtils.isBlank(value)) return new ArrayList<>();
        List<String> strings = Splitter.on(",").omitEmptyStrings().trimResults().splitToList(value);
        return strings.stream().filter(StringUtils::isNumeric).map(Integer::valueOf).collect(Collectors.toList());
    }

    public Integer getPitTransectLengthDefaultValue() {
        return (Integer) applicationConfig.getOption(ReefDbConfigurationOption.PIT_TRANSECT_LENGTH_DEFAULT_VALUE);
    }

    public Integer getPitOriginDefaultValue() {
        return (Integer) applicationConfig.getOption(ReefDbConfigurationOption.PIT_ORIGIN_DEFAULT_VALUE);
    }

    /**
     * <p>getNoUnitId.</p>
     *
     * @return a int.
     */
    public int getNoUnitId() {
        return applicationConfig.getOptionAsInt(ReefDbConfigurationOption.UNIT_ID_NO_UNIT.getKey());
    }

    /**
     * <p>getTranscribingItemTypeLbForPmfmNm.</p>
     *
     * @return a {@link String} object.
     */
    public String getTranscribingItemTypeLbForPmfmNm() {
        return applicationConfig.getOption(ReefDbConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PMFM_NM.getKey());
    }

    public String getTranscribingItemTypeLbForPmfmExtraction() {
        return applicationConfig.getOption(ReefDbConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PMFM_EXTRACTION.getKey());
    }

    public String getTranscribingItemTypeLbForTaxRef() {
        return applicationConfig.getOption(ReefDbConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_TAXREF.getKey());
    }

    public String getTranscribingItemTypeLbForWorms() {
        return applicationConfig.getOption(ReefDbConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_WORMS.getKey());
    }

    public String getTranscribingItemTypeLbForPampa() {
        return applicationConfig.getOption(ReefDbConfigurationOption.TRANSCRIBING_ITEM_TYPE_LB_PAMPA.getKey());
    }

    /**
     * <p>getAlternativeTaxonOriginTaxRef.</p>
     *
     * @return a {@link String} object.
     */
//    public String getAlternativeTaxonOriginTaxRef() {
//        return applicationConfig.getOption(ReefDbConfigurationOption.ALTERNATIVE_TAXON_ORIGIN_TAXREF.getKey());
//    }

    /**
     * <p>getAlternativeTaxonOriginWorms.</p>
     *
     * @return a {@link String} object.
     */
//    public String getAlternativeTaxonOriginWorms() {
//        return applicationConfig.getOption(ReefDbConfigurationOption.ALTERNATIVE_TAXON_ORIGIN_WORMS.getKey());
//    }

//    public String getAlternativeTaxonOriginPAMPA() {
//        return applicationConfig.getOption(ReefDbConfigurationOption.ALTERNATIVE_TAXON_ORIGIN_PAMPA.getKey());
//    }

}
