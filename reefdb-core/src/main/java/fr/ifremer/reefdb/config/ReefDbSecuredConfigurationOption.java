package fr.ifremer.reefdb.config;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.security.QuadrigeUserAuthority;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import org.nuiton.config.ConfigOptionDef;

import java.util.List;

/**
 * <p>ReefDbSecuredConfigurationOption class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class ReefDbSecuredConfigurationOption implements ConfigOptionDef {

    private final ConfigOptionDef delegate;
    private final List<QuadrigeUserAuthority> roles;

    /**
     * <p>Constructor for ReefDbSecuredConfigurationOption.</p>
     *
     * @param delegate a {@link org.nuiton.config.ConfigOptionDef} object.
     * @param role a {@link QuadrigeUserAuthority} object.
     */
    public ReefDbSecuredConfigurationOption(ConfigOptionDef delegate, QuadrigeUserAuthority role) {
        this.delegate = delegate;
        this.roles = Lists.newArrayList(role);
    }

    /**
     * <p>Constructor for ReefDbSecuredConfigurationOption.</p>
     *
     * @param delegate a {@link org.nuiton.config.ConfigOptionDef} object.
     * @param roles a {@link java.util.List} object.
     */
    public ReefDbSecuredConfigurationOption(ConfigOptionDef delegate, List<QuadrigeUserAuthority> roles) {
        this.delegate = delegate;
        this.roles = roles;
    }

    /** {@inheritDoc} */
    @Override
    public String getKey() {
        return delegate.getKey();
    }

    /** {@inheritDoc} */
    @Override
    public Class<?> getType() {
        return delegate.getType();
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegate.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public String getDefaultValue() {
        return delegate.getDefaultValue();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTransient() {
        return delegate.isTransient();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFinal() {
        return !SecurityContextHelper.hasAuthority(roles) || delegate.isFinal();
    }

    /** {@inheritDoc} */
    @Override
    public void setDefaultValue(String defaultValue) {
        if (!SecurityContextHelper.hasAuthority(roles)) {
            return;
        }
        delegate.setDefaultValue(defaultValue);
    }

    /** {@inheritDoc} */
    @Override
    public void setTransient(boolean isTransient) {
        if (!SecurityContextHelper.hasAuthority(roles)) {
            return;
        }
        delegate.setTransient(isTransient);
    }

    /** {@inheritDoc} */
    @Override
    public void setFinal(boolean isFinal) {
        if (!SecurityContextHelper.hasAuthority(roles)) {
            return;
        }
        delegate.setFinal(isFinal);
    }

}
