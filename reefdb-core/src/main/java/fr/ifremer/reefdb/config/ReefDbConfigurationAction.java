package fr.ifremer.reefdb.config;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.config.ConfigActionDef;

import fr.ifremer.reefdb.action.ReefDbHelpAction;

/**
 * <p>ReefDbConfigurationAction class.</p>
 *
 */
public enum ReefDbConfigurationAction implements ConfigActionDef {

    //si appli lancé avec -h ou --help alors exécute la méthode show de l'action ReefDbHelpAction
    HELP(ReefDbHelpAction.class.getName() + "#show", "Shows help", "-h", "--help");

    public final String action;
    public final String description;
    public final String[] aliases;

    ReefDbConfigurationAction(String action, String description, String... aliases) {
        this.action = action;
        this.description = description;
        this.aliases = aliases;
    }

    /** {@inheritDoc} */
    @Override
    public String getAction() {
        return action;
    }

    /** {@inheritDoc} */
    @Override
    public String[] getAliases() {
        return aliases;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
