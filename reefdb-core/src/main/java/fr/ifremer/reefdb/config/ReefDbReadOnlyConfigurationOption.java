package fr.ifremer.reefdb.config;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import org.nuiton.config.ConfigOptionDef;

/**
 * <p>ReefDbReadOnlyConfigurationOption class.</p>
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class ReefDbReadOnlyConfigurationOption implements ConfigOptionDef {

    private final ConfigOptionDef delegate;

    /**
     * <p>Constructor for ReefDbReadOnlyConfigurationOption.</p>
     *
     * @param delegate a {@link org.nuiton.config.ConfigOptionDef} object.
     */
    public ReefDbReadOnlyConfigurationOption(ConfigOptionDef delegate) {
        this.delegate = delegate;
    }

    /** {@inheritDoc} */
    @Override
    public String getKey() {
        return delegate.getKey();
    }

    /** {@inheritDoc} */
    @Override
    public Class<?> getType() {
        return delegate.getType();
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription() {
        return delegate.getDescription();
    }

    /** {@inheritDoc} */
    @Override
    public String getDefaultValue() {
        return delegate.getDefaultValue();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTransient() {
        return delegate.isTransient();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFinal() {
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void setDefaultValue(String defaultValue) {
    }

    /** {@inheritDoc} */
    @Override
    public void setTransient(boolean isTransient) {
    }

    /** {@inheritDoc} */
    @Override
    public void setFinal(boolean isFinal) {
    }

}
