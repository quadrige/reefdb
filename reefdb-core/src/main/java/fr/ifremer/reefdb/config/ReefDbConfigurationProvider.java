package fr.ifremer.reefdb.config;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.nuiton.i18n.I18n.l;

import java.util.Locale;

import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ConfigActionDef;
import org.nuiton.config.ConfigOptionDef;

/**
 * ReefDb config provider (for site generation).
 */
public class ReefDbConfigurationProvider implements ApplicationConfigProvider {

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return "reefdb";
    }

    /** {@inheritDoc} */
    @Override
    public String getDescription(Locale locale) {
        return l(locale, "reefdb.config");
    }

    /** {@inheritDoc} */
    @Override
    public ConfigOptionDef[] getOptions() {
        return ReefDbConfigurationOption.values();
    }

    /** {@inheritDoc} */
    @Override
    public ConfigActionDef[] getActions() {
        return ReefDbConfigurationAction.values();
    }
}
