package fr.ifremer.reefdb.dto;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.UnitId;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ludovic.pecquot@e-is.pro on 13/04/2017.
 */
public class ReefDbBeansTest {

    @Test
    @Ignore
    /**
     * Test de conversion d'unité de longueur
     * Ignoré car fonctionne uniquement avec les référentiel Ifremer
     * Ou alors il faut modifié le jeu de test
     */
    public void testConvertLengthValue() {

        BigDecimal result = ReefDbBeans.convertLengthValue(BigDecimal.valueOf(5), UnitId.METER.getValue(), UnitId.CENTIMENTER.getValue());
        Assert.assertEquals(500, result.intValue());

        result = ReefDbBeans.convertLengthValue(BigDecimal.valueOf(2), UnitId.METER.getValue(), UnitId.MILLIMETER.getValue());
        Assert.assertEquals(2000, result.intValue());

        result = ReefDbBeans.convertLengthValue(BigDecimal.valueOf(6000), UnitId.CENTIMENTER.getValue(), UnitId.METER.getValue());
        Assert.assertEquals(60, result.intValue());

    }

    @Test
    public void testReduce() {

        List<MeasurementDTO> beans = new ArrayList<>();
        {
            MeasurementDTO bean = ReefDbBeanFactory.newMeasurementDTO();
            bean.setId(1);
            beans.add(bean);
        }
        {
            MeasurementDTO bean = ReefDbBeanFactory.newMeasurementDTO();
            bean.setId(-2);
            beans.add(bean);
        }
        {
            MeasurementDTO bean = ReefDbBeanFactory.newMeasurementDTO();
            bean.setId(null);
            beans.add(bean);
        }
        {
            MeasurementDTO bean = ReefDbBeanFactory.newMeasurementDTO();
            bean.setId(4);
            beans.add(bean);
        }

        int minNegativeId = beans.stream()
            .filter(measurementDTO -> measurementDTO.getId() != null)
            .mapToInt(MeasurementDTO::getId)
            .min().orElse(0);

        Assert.assertEquals(-2, minNegativeId);

    }
}
