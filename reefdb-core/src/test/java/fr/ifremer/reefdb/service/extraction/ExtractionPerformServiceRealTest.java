package fr.ifremer.reefdb.service.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.enums.ExtractionOutputType;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Extraction Service Test Class
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public class ExtractionPerformServiceRealTest {

    private static final Log LOG = LogFactory.getLog(ExtractionPerformServiceRealTest.class);

    @ClassRule
//    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb("mantis49970", true);
    public static final ReefDbDatabaseResource dbResource = new ReefDbDatabaseResource("mantis49970", true) {
        @Override
        public String getBuildEnvironment() {
            return null;
        }
    };

    private ExtractionService service;

    private ExtractionPerformService performService;

    private ReefDbConfiguration config;

    @Before
    public void setUp() {
        service = ReefDbServiceLocator.instance().getExtractionService();
        performService = ReefDbServiceLocator.instance().getExtractionPerformService();
        config = ReefDbConfiguration.getInstance();
    }

    @Ignore
    @Test
    public void testConfig() {
        Assert.assertTrue(StringUtils.isNotBlank(config.getApplicationConfig().getOption("reefdb.core.test.extraction.path")));
    }

    @Ignore
    @Test
    public void performRealExtraction() {

        // load extraction
        List<ExtractionDTO> extractions = service.getExtractions(2, null);
        assertNotNull(extractions);
        assertEquals(1, extractions.size());
        ExtractionDTO extraction = extractions.get(0);
        assertNotNull(extraction);

        // limit some data
        FilterDTO periodFilter = ReefDbBeans.getFilterOfType(extraction, ExtractionFilterValues.PERIOD);
        if (periodFilter != null) {
            extraction.removeFilters(periodFilter);
        }
        ExtractionPeriodDTO period = ReefDbBeanFactory.newExtractionPeriodDTO();
        period.setStartDate(LocalDate.of(2019, 2, 5));
        period.setEndDate(LocalDate.of(2019, 2, 5));
        ReefDbBeans.setFilterElements(extraction, ExtractionFilterValues.PERIOD, ImmutableList.of(period));

        assertExtraction(extraction, ExtractionOutputType.SIMPLE);
        assertExtraction(extraction, ExtractionOutputType.COMPLETE);
//        assertExtraction(extraction, ExtractionOutputType.SINP);
//        assertExtraction(extraction, ExtractionOutputType.PAMPA);


    }

    private void assertExtraction(ExtractionDTO extraction, ExtractionOutputType outputType) {

        // get referent file
        String refFileDir = config.getApplicationConfig().getOption("reefdb.core.test.extraction.path");
        String refFileName = config.getApplicationConfig().getOption(String.format("reefdb.core.test.extraction.%s.%s", extraction.getId(), outputType));
        File refFile = new File(refFileDir, refFileName);

        // perform extraction
        File outputFile = new File(refFileDir, String.format("extraction-%s-%s-%s.csv", extraction.getId(), outputType, System.currentTimeMillis()));
        performService.performExtraction(extraction, outputType, outputFile, new ProgressionCoreModel());

        // compare files
//        LOG.debug("comparing with " + refFile);
//        FileAssert.assertEquals(refFile, outputFile);

    }

}
