package fr.ifremer.reefdb.service.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.ProgressionCoreModel;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.enums.ExtractionOutputType;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Extraction Service Test Class
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public class ExtractionPerformServiceTest {

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ExtractionPerformService performService;

    private ProgramStrategyService programStrategyService;

    private ReferentialService referentialService;

    private ReefDbConfiguration config;

    @Before
    public void setUp() {
        performService = ReefDbServiceLocator.instance().getExtractionPerformService();
        programStrategyService = ReefDbServiceLocator.instance().getProgramStrategyService();
        referentialService = ReefDbServiceLocator.instance().getReferentialService();
        config = ReefDbConfiguration.getInstance();
    }

    @Test
    public void performExtraction() {

        // create extraction
        ExtractionDTO extraction = ReefDbBeanFactory.newExtractionDTO();
        extraction.setName("extraction test");
        extraction.setId(1);

        // add periods
        ExtractionPeriodDTO period = ReefDbBeanFactory.newExtractionPeriodDTO();
        period.setStartDate(LocalDate.of(2014,1,1));
        period.setEndDate(LocalDate.of(2016,1,1));
        FilterDTO periodFilter = ReefDbBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(Collections.singletonList(period));
        extraction.addFilters(periodFilter);

        // add grouping
        FilterDTO groupingFilter = ReefDbBeanFactory.newFilterDTO();
        groupingFilter.setFilterTypeId(ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId());
        groupingFilter.setElements(Collections.singletonList(referentialService.getGroupingTypes().get(0)));
        groupingFilter.setFilterLoaded(true);
        extraction.addFilters(groupingFilter);

        // add filters
        FilterDTO filter1 = ReefDbBeanFactory.newFilterDTO();
        filter1.setFilterTypeId(ExtractionFilterValues.PROGRAM.getFilterTypeId());
        filter1.setElements(ImmutableList.of(programStrategyService.getWritableProgramByCode("REMIS")));
        filter1.setFilterLoaded(true);
        extraction.addFilters(filter1);

        // add another filter
        FilterDTO filter2 = ReefDbBeanFactory.newFilterDTO();
        filter2.setFilterTypeId(ExtractionFilterValues.LOCATION.getFilterTypeId());
        filter2.setElements(referentialService.getLocations(StatusFilter.ALL));
        filter2.setFilterLoaded(true);
        extraction.addFilters(filter2);

        File outputFile1 = new File(config.getTempDirectory(), "extraction1.csv");
        performService.performExtraction(extraction, ExtractionOutputType.SIMPLE, outputFile1, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile1, 6); // 1 header + 5 results

        // add pmfm filter
        FilterDTO filter3 = ReefDbBeanFactory.newFilterDTO();
        filter3.setFilterTypeId(ExtractionFilterValues.PMFM.getFilterTypeId());
        filter3.setElements(Lists.<QuadrigeBean>newArrayList(referentialService.getPmfm(21))); // only 155
        filter3.setFilterLoaded(true);
        extraction.addFilters(filter3);

        File outputFile2 = new File(config.getTempDirectory(), "extraction2.csv");
        performService.performExtraction(extraction, ExtractionOutputType.SIMPLE, outputFile2, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile2, 5); // 1 header + 4 results

        // add taxon group filter
        FilterDTO filter4 = ReefDbBeanFactory.newFilterDTO();
        filter4.setFilterTypeId(ExtractionFilterValues.TAXON_GROUP.getFilterTypeId());
        filter4.setElements(Lists.<QuadrigeBean>newArrayList(referentialService.getTaxonGroup(1))); // only Groupe trophique benthos
        filter4.setFilterLoaded(true);
        extraction.addFilters(filter4);

        File outputFile3 = new File(config.getTempDirectory(), "extraction3.csv");
        performService.performExtraction(extraction, ExtractionOutputType.SIMPLE, outputFile3, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile3, 3); // 1 header + 2 results

        // add filter taxon
        FilterDTO filter5 = ReefDbBeanFactory.newFilterDTO();
        filter5.setFilterTypeId(ExtractionFilterValues.TAXON.getFilterTypeId());
        filter5.setElements(Lists.<QuadrigeBean>newArrayList(referentialService.getTaxon(1))); // dummy taxon
        filter5.setFilterLoaded(true);
        extraction.addFilters(filter5);

        File outputFile4 = new File(config.getTempDirectory(), "extraction4.csv");
        try {
            performService.performExtraction(extraction, ExtractionOutputType.SIMPLE, outputFile4, new ProgressionCoreModel());
            fail("should throw exception");
        } catch (Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void performExtractionPAMPA() {

        // create extraction
        ExtractionDTO extraction = ReefDbBeanFactory.newExtractionDTO();
        extraction.setName("extraction test");
        extraction.setId(1);

        // add periods
        ExtractionPeriodDTO period = ReefDbBeanFactory.newExtractionPeriodDTO();
        period.setStartDate(LocalDate.of(2014,1,1));
        period.setEndDate(LocalDate.of(2016,1,1));
        FilterDTO periodFilter = ReefDbBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(Collections.singletonList(period));
        extraction.addFilters(periodFilter);

        // add grouping
        FilterDTO groupingFilter = ReefDbBeanFactory.newFilterDTO();
        groupingFilter.setFilterTypeId(ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId());
        groupingFilter.setElements(Collections.singletonList(referentialService.getGroupingTypes().get(0)));
        groupingFilter.setFilterLoaded(true);
        extraction.addFilters(groupingFilter);

        // add filters
        FilterDTO filter1 = ReefDbBeanFactory.newFilterDTO();
        filter1.setFilterTypeId(ExtractionFilterValues.PROGRAM.getFilterTypeId());
        filter1.setElements(ImmutableList.of(programStrategyService.getWritableProgramByCode("REMIS")));
        filter1.setFilterLoaded(true);
        extraction.addFilters(filter1);

        // add another filter
        FilterDTO filter2 = ReefDbBeanFactory.newFilterDTO();
        filter2.setFilterTypeId(ExtractionFilterValues.LOCATION.getFilterTypeId());
        filter2.setElements(referentialService.getLocations(StatusFilter.ALL));
        filter2.setFilterLoaded(true);
        extraction.addFilters(filter2);

        File outputFile1 = new File(config.getTempDirectory(), "extractionPAMPA.csv");
        performService.performExtraction(extraction, ExtractionOutputType.PAMPA, outputFile1, new ProgressionCoreModel());
        assertCsvFileNbLines(outputFile1, 6); // 1 header + 5 results
    }

    private void assertCsvFileNbLines(File csvFile, int expectedNbLines) {

        assertTrue(csvFile.canRead());
        try {
            BufferedReader reader = new BufferedReader(new FileReader(csvFile.getCanonicalFile()));
            int nbLines = 0;
            while (reader.readLine() != null) {
                nbLines++;
            }
            assertEquals(expectedNbLines, nbLines);

        } catch (IOException e) {
            fail(e.getLocalizedMessage());
        }
    }

}
