package fr.ifremer.reefdb.service.administration.context;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.context.ContextDTO;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class ContextServiceTest {

    private static final Log log = LogFactory.getLog(ContextServiceTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ContextService service;

    @Before
    public void setUp() {
        service = ReefDbServiceLocator.instance().getContextService();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void getLocationFilter() {
        FilterDTO f = service.getFilter(1001); // filter 1000
        Assert.assertNotNull(f);
        service.loadFilteredElements(f);

        List<LocationDTO> l = (List<LocationDTO>) f.getElements();
        Assert.assertEquals(2, l.size());
        Assert.assertEquals("Jetée est Dunkerque", ((LocationDTO) f.getElements().get(0)).getName());
        Assert.assertEquals("Cap Gris nez", ((LocationDTO) f.getElements().get(1)).getName());

        FilterDTO fp = service.getFilter(1002);
        Assert.assertNotNull(fp);
        service.loadFilteredElements(fp);

        List<ProgramDTO> p = (List<ProgramDTO>) fp.getElements();
        Assert.assertEquals(2, p.size());
        Assert.assertEquals("REBENT", ((ProgramDTO) fp.getElements().get(0)).getCode());
        Assert.assertEquals("REMIS", ((ProgramDTO) fp.getElements().get(1)).getCode());
    }

    @Test
    public void saveAndDeleteFilters() {
        FilterDTO f1 = service.getFilter(1001);

        FilterDTO f2 = ReefDbBeanFactory.newFilterDTO();
        LocationDTO l1 = ReefDbBeanFactory.newLocationDTO();
        l1.setId(2); // Digue du Braek
        l1.setLabel("L1");
        f2.setFilterTypeId(FilterTypeValues.LOCATION.getFilterTypeId());
        f2.setName("test new");
        f2.setElements(Lists.newArrayList(l1));
        f2.setDirty(true);

        List<FilterDTO> filters = Lists.newArrayList(f1, f2);
//        ReefDbServiceLocator.instance().getDataContext().setRecorderPersonId(1);
        service.saveFilters(filters);

        filters.remove(f1);
        f2 = filters.get(0);

        Integer newFilterId = f2.getId();

        Assert.assertNotNull(newFilterId);

        List<FilterDTO> filtersToRemove = Lists.newArrayList();
        filtersToRemove.add(f2);

        service.deleteFilters(filtersToRemove);

        f2 = service.getFilter(newFilterId);

        Assert.assertNull(f2);

    }

    @Test
    public void getContexts() {
        List<ContextDTO> contexts = service.getAllContexts();
        Assert.assertTrue(contexts.size() >= 2);

        ContextDTO c1 = service.getContext(1001);
        Assert.assertNotNull(c1);
        Assert.assertEquals(3, c1.getFilters().size());

    }

    @Test
    public void saveContexts() {
        List<ContextDTO> contexts = service.getAllContexts();

        ContextDTO newCtxt = ReefDbBeanFactory.newContextDTO();
        newCtxt.setDirty(true);
        newCtxt.setName("Test NEW");

        Collection<FilterDTO> filters = Lists.newArrayList();
        filters.addAll(service.getAllLocationFilter());
        filters.addAll(service.getAllProgramFilter());

        newCtxt.setFilters(filters);

        contexts.add(newCtxt);

        service.saveContexts(contexts);

        Assert.assertNotNull(newCtxt.getId());

    }

    @Test
    public void deleteContexts() {
        List<ContextDTO> contexts = service.getAllContexts();
        List<ContextDTO> contextsToDelete = Lists.newArrayList();

        int contextNbBefore = contexts.size();

        for (ContextDTO c : contexts) {
            if (c.getId() < 1000) {
                contextsToDelete.add(c);
            }
        }

        int nbToDelete = contextsToDelete.size();

        service.deleteContexts(contextsToDelete);

        int contextNbAfter = service.getAllContexts().size();

        Assert.assertEquals(contextNbAfter, contextNbBefore - nbToDelete);

    }

    @Test
    public void exportImportFilters() {
        List<FilterDTO> filters = Lists.newArrayList();
        FilterDTO f1 = service.getFilter(1004);
        filters.add(f1);
        Assert.assertFalse(filters.isEmpty());
        int filtersNb = filters.size();

        File exportFile = new File(dbResource.getResourceDirectory(), "filters.dat");
        service.exportFilter(filters, exportFile);

        service.importFilter(exportFile, f1.getFilterTypeId());
        service.deleteFilters(filters);
        List<FilterDTO> importedFilters = service.importFilter(exportFile, f1.getFilterTypeId());
        Assert.assertEquals(filtersNb, importedFilters.size());

        FilterDTO importedFilter = importedFilters.get(0);
        Assert.assertEquals(1, importedFilter.getElements().size());

    }

    @Test
    public void duplicateFilter() {

        FilterDTO filter = service.getFilter(1001);
        service.loadFilteredElements(filter);

        int nbLoc = filter.getElements().size();

        FilterDTO newFilter = service.duplicateFilter(filter);

        Assert.assertNull(newFilter.getId());
        Assert.assertEquals(nbLoc, newFilter.getElements().size());

    }

}
