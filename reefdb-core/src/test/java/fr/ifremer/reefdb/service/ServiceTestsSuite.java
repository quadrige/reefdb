package fr.ifremer.reefdb.service;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.service.administration.context.ContextServiceTest;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyServiceWriteTest;
import fr.ifremer.reefdb.service.extraction.ExtractionServiceTest;
import fr.ifremer.reefdb.service.referential.ReferentialServiceReadTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Ludovic
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ContextServiceTest.class,
        ReferentialServiceReadTest.class,
        ProgramStrategyServiceWriteTest.class,
        ExtractionServiceTest.class
})
public class ServiceTestsSuite {

}
