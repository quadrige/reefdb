package fr.ifremer.reefdb.service.rulescontrol;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2019 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

/**
 * @author peck7 on 02/01/2019.
 */
public class RuleListServiceTest {

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private RuleListService service;

    private ReefDbConfiguration config;

    static final String SMALL_STRING = "SMALL_STRING";
    static final String LONG_STRING = "LONG_STRING_123456789012346579801234567890";

    @Before
    public void setUp() {
        service = ReefDbServiceLocator.instance().getRuleListService();
        config = ReefDbConfiguration.getInstance();
    }

    @Test
    public void getNextRuleCode() {

        String smallString = SMALL_STRING;
        String longString = LONG_STRING;

        MutableInt index = service.getUniqueMutableIndex();
        String newCode = service.getNextRuleCode(smallString, index);
        Assert.assertEquals(SMALL_STRING, smallString);
        Assert.assertNotNull(newCode);
        Assert.assertTrue(newCode.length() < 40);
        Assert.assertEquals(String.format("%S_%s", smallString, index.getValue() - 1), newCode);

        newCode = service.getNextRuleCode(longString, index);
        Assert.assertEquals(LONG_STRING, longString);
        Assert.assertNotNull(newCode);
        Assert.assertEquals(40, newCode.length());
        Assert.assertEquals(String.format("%s_%s", longString.substring(0, 29), index.getValue() - 1), newCode);
    }

}
