package fr.ifremer.reefdb.service.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.DepthDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Ludovic
 */
public class ReferentialServiceReadTest {

    private static final Log log = LogFactory.getLog(ReferentialServiceReadTest.class);
    
    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();    

    private ReferentialService service;
    
    private ReefDbConfiguration config;
    
    @Before
    public void setUp() {
        config = ReefDbConfiguration.getInstance();
        service = ReefDbServiceLocator.instance().getReferentialService();
    }
    
    
    @Test
    public void getAllDepthValues() {
        
        List<DepthDTO> depthValues = service.getDepths();
        assertNotNull(depthValues);
        assertTrue(depthValues.size() > 0); // 6 dans le dataset
        
    }
}
