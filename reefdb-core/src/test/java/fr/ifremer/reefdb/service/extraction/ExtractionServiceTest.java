package fr.ifremer.reefdb.service.extraction;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBean;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.ExtractionFilterValues;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionDTO;
import fr.ifremer.reefdb.dto.system.extraction.ExtractionPeriodDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.administration.program.ProgramStrategyService;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Extraction Service Test Class
 * <p/>
 * Created by Ludovic on 03/12/2015.
 */
public class ExtractionServiceTest {

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ExtractionService service;

    private ProgramStrategyService programStrategyService;

    private ReferentialService referentialService;

    @Before
    public void setUp() {
        service = ReefDbServiceLocator.instance().getExtractionService();
        programStrategyService = ReefDbServiceLocator.instance().getProgramStrategyService();
        referentialService = ReefDbServiceLocator.instance().getReferentialService();
    }

    @Test
    public void createExtraction() {

        ExtractionDTO extraction = ReefDbBeanFactory.newExtractionDTO();
        extraction.setName("extraction test");

        // add periods
        ExtractionPeriodDTO period1 = ReefDbBeanFactory.newExtractionPeriodDTO();
        period1.setStartDate(LocalDate.of(2015,1,1));
        period1.setEndDate(LocalDate.of(2015,12,31));
        ExtractionPeriodDTO period2 = ReefDbBeanFactory.newExtractionPeriodDTO();
        period2.setStartDate(LocalDate.of(2014,1,1));
        period2.setEndDate(LocalDate.of(2014,12,31));
        FilterDTO periodFilter = ReefDbBeanFactory.newFilterDTO();
        periodFilter.setFilterTypeId(ExtractionFilterValues.PERIOD.getFilterTypeId());
        periodFilter.setElements(ImmutableList.of(period1, period2));
        extraction.addFilters(periodFilter);

        // add grouping
        FilterDTO groupingFilter = ReefDbBeanFactory.newFilterDTO();
        groupingFilter.setFilterTypeId(ExtractionFilterValues.ORDER_ITEM_TYPE.getFilterTypeId());
        groupingFilter.setElements(Collections.singletonList(referentialService.getGroupingTypes().get(0)));
        groupingFilter.setFilterLoaded(true);
        extraction.addFilters(groupingFilter);

        ExtractionDTO reloadedExtraction = saveAndReload(extraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // modify date range
        period1.setStartDate(LocalDate.of(2015,3,2));
        period1.setEndDate(LocalDate.of(2015,8,28));
        reloadedExtraction = saveAndReload(extraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // add filters
        FilterDTO filter1 = ReefDbBeanFactory.newFilterDTO();
        filter1.setFilterTypeId(ExtractionFilterValues.PROGRAM.getFilterTypeId());
        filter1.setElements(programStrategyService.getWritablePrograms()); // 2 programs
        filter1.setFilterLoaded(true);
        extraction.addFilters(filter1);

        reloadedExtraction = saveAndReload(extraction);

        // load elements
        service.loadFilteredElements(reloadedExtraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // add another filter
        FilterDTO filter2 = ReefDbBeanFactory.newFilterDTO();
        filter2.setFilterTypeId(ExtractionFilterValues.PMFM.getFilterTypeId());
        filter2.setElements(referentialService.getPmfms(StatusFilter.ACTIVE).subList(2, 5)); // 3 pmfms
        filter2.setFilterLoaded(true);
        extraction.addFilters(filter2);

        reloadedExtraction = saveAndReload(extraction);

        // load elements
        service.loadFilteredElements(reloadedExtraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // remove first filter
        extraction.removeFilters(filter1);
        reloadedExtraction = saveAndReload(extraction);

        // load elements
        service.loadFilteredElements(reloadedExtraction);
        assertExtractionEquals(extraction, reloadedExtraction);

        // delete extraction
        service.deleteExtractions(Lists.newArrayList(extraction.getId()));
    }

    private ExtractionDTO saveAndReload(ExtractionDTO extraction) {
        extraction.setDirty(true);
        service.saveExtractions(Lists.newArrayList(extraction));

        assertNotNull(extraction.getId());
        assertFalse(extraction.isDirty());

        // reload
        List<ExtractionDTO> extractions = service.getExtractions(extraction.getId(), null);

        assertNotNull(extractions);
        assertEquals(1, extractions.size());

        return extractions.get(0);
    }

    private void assertExtractionEquals(ExtractionDTO expectedExtraction, ExtractionDTO extractionToTest) {
        assertNotNull(expectedExtraction);
        assertNotNull(extractionToTest);
        assertEquals(expectedExtraction.getId(), extractionToTest.getId());
        assertEquals(expectedExtraction.getName(), extractionToTest.getName());
        assertEquals(expectedExtraction.getUser(), extractionToTest.getUser());
        assertEquals(expectedExtraction.isDirty(), extractionToTest.isDirty());
        assertFiltersEquals(expectedExtraction.getFilters(), extractionToTest.getFilters());
    }

    private void assertFiltersEquals(Collection<FilterDTO> expectedFilters, Collection<FilterDTO> filtersToTest) {
        if (CollectionUtils.isEmpty(expectedFilters)) {
            assertTrue(CollectionUtils.isEmpty(filtersToTest));
            return;
        } else {
            assertTrue(CollectionUtils.isNotEmpty(filtersToTest));
            assertEquals(expectedFilters.size(), filtersToTest.size());
        }

        Map<Integer, FilterDTO> filtersToTestMap = ReefDbBeans.mapByProperty(filtersToTest, FilterDTO.PROPERTY_FILTER_TYPE_ID);
        for (FilterDTO expectedFilter : expectedFilters) {
            FilterDTO filterToTest = filtersToTestMap.get(expectedFilter.getFilterTypeId());
            assertNotNull(filterToTest);
            assertEquals(expectedFilter.getFilterTypeId(), filterToTest.getFilterTypeId());
            assertEquals(expectedFilter.getName(), filterToTest.getName());

            if (CollectionUtils.isEmpty(expectedFilter.getElements())) {
                assertTrue(CollectionUtils.isEmpty(filterToTest.getElements()));
            } else {
                assertTrue(CollectionUtils.isNotEmpty(filterToTest.getElements()));
                assertEquals(expectedFilter.getElements().size(), filterToTest.getElements().size());

                QuadrigeBean[] expectedElements = expectedFilter.getElements().toArray(new QuadrigeBean[0]);
                QuadrigeBean[] elementsToTest = filterToTest.getElements().toArray(new QuadrigeBean[0]);
                Arrays.sort(expectedElements);
                Arrays.sort(elementsToTest);
                assertArrayEquals(expectedElements, elementsToTest);
            }
        }
    }

}
