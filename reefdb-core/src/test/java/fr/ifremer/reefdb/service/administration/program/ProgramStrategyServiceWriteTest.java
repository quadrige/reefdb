package fr.ifremer.reefdb.service.administration.program;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.ui.core.dto.QuadrigeBeanFactory;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.ProgramDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.*;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author Ludovic
 */
public class ProgramStrategyServiceWriteTest {

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();
    private static final Log log = LogFactory.getLog(ProgramStrategyServiceWriteTest.class);
    private static final String PROGRAM_CODE = "TEST";
    private static final String STRATEGY_1_NAME = "strategy 1 test";
    private static final String STRATEGY_2_NAME = "strategy 2 test";
    private ProgramStrategyService service;
    private ReferentialService referentialService;

    @Before
    public void setUp() {
        service = ReefDbServiceLocator.instance().getProgramStrategyService();
        referentialService = ReefDbServiceLocator.instance().getReferentialService();
    }

    @Test
    public void createProgramAndStrategies() {

        if (log.isDebugEnabled()) {
            log.debug("createProgramAndStrategies start");
        }

        ProgramDTO program = ReefDbBeanFactory.newProgramDTO();
        List<ProgramDTO> programs = Lists.newArrayList(program);
        program.setCode(PROGRAM_CODE);
        program.setName("program test");
        program.setComment("comment");
        program.setStatus(Daos.getStatus(StatusCode.LOCAL_ENABLE));

        // add locations
        program.setLocations(referentialService.getLocations(StatusFilter.ACTIVE));

        program.setDirty(true);
        service.savePrograms(programs);

        // create a strategy
        StrategyDTO strategy1 = ReefDbBeanFactory.newStrategyDTO();
        strategy1.setName(STRATEGY_1_NAME);
        strategy1.setComment("comment");
        for (LocationDTO location : program.getLocations()) {
            AppliedStrategyDTO as = ReefDbBeanFactory.newAppliedStrategyDTO();
            as.setId(location.getId());
            as.setLabel(location.getLabel());
            as.setName(location.getName());
            as.setComment(location.getComment());
            strategy1.addAppliedStrategies(as);
        }
        program.addStrategies(strategy1);

        if (log.isDebugEnabled()) {
            log.debug("save a program");
        }
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        ProgramDTO reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);

        // Add a strategy with another location list , add applied periods and pmfmStrategy
        StrategyDTO strategy2 = ReefDbBeanFactory.newStrategyDTO();
        strategy2.setName(STRATEGY_2_NAME);
        strategy2.setComment("with pmfm strategies");
        for (LocationDTO location : program.getLocations()) {
            AppliedStrategyDTO as = ReefDbBeanFactory.newAppliedStrategyDTO();
            as.setId(location.getId());
            as.setLabel(location.getLabel());
            as.setName(location.getName());
            as.setComment(location.getComment());
            strategy2.addAppliedStrategies(as);
        }
        // remove a location
        strategy2.getAppliedStrategies().remove(0);
        
        // add applied period
        List<DepartmentDTO> allDepartments = referentialService.getDepartments();
        for (AppliedStrategyDTO appliedStrategy : strategy2.getAppliedStrategies()) {
            LocalDate startDate = LocalDate.of(2014, RandomUtils.nextInt(1, 13), RandomUtils.nextInt(1, 31));
            appliedStrategy.setStartDate(startDate);
            appliedStrategy.setEndDate(startDate.plusDays(40));
            appliedStrategy.setDepartment(allDepartments.get(RandomUtils.nextInt(0, allDepartments.size())));
        }
        program.addStrategies(strategy2);

        // add 10 Pmfm
        List<PmfmDTO> allPmfms = new ArrayList<>(referentialService.getPmfms(StatusFilter.ACTIVE));
        for (int i = 0; i < 10; i++) {
            PmfmStrategyDTO pmfmStrategy = ReefDbBeanFactory.newPmfmStrategyDTO();
            pmfmStrategy.setPmfm(allPmfms.remove(RandomUtils.nextInt(0, allPmfms.size())));
            pmfmStrategy.setAnalysisDepartment(i % 2 == 0 ? null : allDepartments.get(RandomUtils.nextInt(0, allDepartments.size())));
            pmfmStrategy.setSurvey(RandomUtils.nextInt(0, 2) == 1);
            pmfmStrategy.setSampling(RandomUtils.nextInt(0, 2) == 1);
            pmfmStrategy.setGrouping(RandomUtils.nextInt(0, 2) == 1);
            pmfmStrategy.setUnique(RandomUtils.nextInt(0, 2) == 1);

            strategy2.addPmfmStrategies(pmfmStrategy);
        }
        strategy2.setPmfmStrategiesLoaded(true);

        // change program property
        program.setComment("with a second strategy with 10 pmfm strategies");

        if (log.isDebugEnabled()) {
            log.debug("update program with pmfm strategies");
        }
        // save
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);

        // get strategies
        Optional<StrategyDTO> optionalStrategy1 = reloadedProgram.getStrategies().stream().filter(strategy -> strategy.getName().equals(STRATEGY_1_NAME)).findFirst();
        Assert.assertTrue(optionalStrategy1.isPresent());
        strategy1 = optionalStrategy1.get();
        Optional<StrategyDTO> optionalStrategy2 = reloadedProgram.getStrategies().stream().filter(strategy -> strategy.getName().equals(STRATEGY_2_NAME)).findFirst();
        Assert.assertTrue(optionalStrategy2.isPresent());
        strategy2 = optionalStrategy2.get();

        // update/remove some properties
        strategy1.getAppliedStrategies().remove(2);
        strategy1.setComment("1 location removed");
        strategy2.getPmfmStrategies().remove(0);
        strategy2.getPmfmStrategies().remove(2);
        strategy2.getPmfmStrategies().remove(6);
        strategy2.getPmfmStrategies(0).setSampling(true);
        strategy2.getPmfmStrategies(0).setSurvey(true);
        strategy2.getPmfmStrategies(0).setUnique(true);
        strategy2.getPmfmStrategies(0).setGrouping(true);
        strategy2.getPmfmStrategies(1).setSampling(false);
        strategy2.getPmfmStrategies(1).setSurvey(false);
        strategy2.getPmfmStrategies(1).setUnique(false);
        strategy2.getPmfmStrategies(1).setGrouping(false);
        strategy2.getPmfmStrategies(2).setPmfm(allPmfms.remove(0));
        strategy2.getAppliedStrategies(0).setStartDate(null);
        strategy2.getAppliedStrategies(0).setEndDate(null);
        strategy2.getAppliedStrategies(1).setDepartment(null);
        strategy2.getAppliedStrategies(2).setStartDate(strategy2.getAppliedStrategies(2).getStartDate().plusDays(2));

        if (log.isDebugEnabled()) {
            log.debug("update program with updated strategies, applied strategies & pmfm strategies");
        }
        // save
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);

        // remove a monLocProg
        program.getLocations().remove(0);

        if (log.isDebugEnabled()) {
            log.debug("update program with removing 1 location on program");
        }

        // save
        program.setDirty(true);
        service.savePrograms(programs);

        // reload and compare
        reloadedProgram = loadFullProgram(PROGRAM_CODE);

        // deep compare
        assertProgramEquals(program, reloadedProgram);

        assertEquals(reloadedProgram.sizeLocations(), reloadedProgram.getStrategies(0).sizeAppliedStrategies());
        assertEquals(reloadedProgram.sizeLocations(), reloadedProgram.getStrategies(1).sizeAppliedStrategies());

        if (log.isDebugEnabled()) {
            log.debug("createProgramAndStrategies end");
        }

    }

    @Test
    public void savePrograms() {
        ProgramDTO programDTO = loadFullProgram(dbResource.getFixtures().getProgramCode());
        Assume.assumeTrue(programDTO.getStrategies().size() > 1);

        StatusDTO status = QuadrigeBeanFactory.newStatusDTO();
        status.setCode(StatusCode.LOCAL_ENABLE.getValue());
        programDTO.setCode(PROGRAM_CODE);
        programDTO.setStatus(status);
        programDTO.setDirty(true);
        service.savePrograms(Lists.newArrayList(programDTO));

        // Keep only the first strategy
        programDTO.setStrategies(Lists.newArrayList(programDTO.getStrategies().iterator().next()));

        programDTO.setDirty(true);
        service.savePrograms(Lists.newArrayList(programDTO));
    }

    @Test
    public void deleteProgram() {
        if (log.isDebugEnabled()) {
            log.debug("remove start");
        }

        service.deletePrograms(Lists.newArrayList(PROGRAM_CODE));

        if (log.isDebugEnabled()) {
            log.debug("remove end");
        }
    }

    private ProgramDTO loadFullProgram(String programCode) {

        ProgramDTO program = service.getWritableProgramByCode(programCode);

        // program has minimum properties
        assertFalse(program.isStrategiesLoaded());
        assertFalse(program.isLocationsLoaded());
        assertTrue(program.isStrategiesEmpty());
        assertTrue(program.isLocationsEmpty());

        // load strategies and applied strategies (=locations)
        service.loadStrategiesAndLocations(program);
        assertTrue(program.isStrategiesLoaded());
        assertTrue(program.isLocationsLoaded());

        // load applied periods (=applied strategies) for each strategy
        for (StrategyDTO strategy : program.getStrategies()) {
            assertFalse(strategy.isPmfmStrategiesLoaded());
            assertTrue(strategy.isPmfmStrategiesEmpty());
            service.loadAppliedPeriods(program, strategy);
            service.loadPmfmStrategy(strategy);
            assertTrue(strategy.isAppliedStrategiesLoaded());
            assertTrue(strategy.isPmfmStrategiesLoaded());
        }

        return program;
    }

    private void assertProgramEquals(ProgramDTO expectedProgram, ProgramDTO programToTest) {
        assertEquals(expectedProgram.getCode(), programToTest.getCode());
        assertEquals(expectedProgram.getName(), programToTest.getName());
        assertEquals(expectedProgram.getComment(), programToTest.getComment());
        assertEquals(expectedProgram.getStatus().getCode(), programToTest.getStatus().getCode());

        assertStrategiesEquals(expectedProgram.getStrategies(), programToTest.getStrategies());
        assertLocationsEquals(expectedProgram.getLocations(), programToTest.getLocations());
    }

    private void assertStrategiesEquals(Collection<StrategyDTO> expectedStrategies, Collection<StrategyDTO> strategiesToTest) {
        assertNotNull(expectedStrategies);
        assertNotNull(strategiesToTest);
        assertNotSame(expectedStrategies, strategiesToTest);
        assertEquals(expectedStrategies.size(), strategiesToTest.size());
        Map<Integer, StrategyDTO> strategiesToTestMap = ReefDbBeans.mapById(strategiesToTest);
        for (StrategyDTO expectedStrategy : expectedStrategies) {
            StrategyDTO strategyToTest = strategiesToTestMap.get(expectedStrategy.getId());
            assertNotNull(strategyToTest);
            assertEquals(expectedStrategy.getName(), strategyToTest.getName());
            assertEquals(expectedStrategy.getComment(), strategyToTest.getComment());

            assertAppliedStrategiesEquals(expectedStrategy.getAppliedStrategies(), strategyToTest.getAppliedStrategies());
            assertPmfmStrategiesEquals(expectedStrategy.getPmfmStrategies(), strategyToTest.getPmfmStrategies());
        }
    }
    
    private void assertLocationsEquals(Collection<LocationDTO> expectedLocations, Collection<LocationDTO> locationsToTest) {
        assertNotNull(expectedLocations);
        assertNotNull(locationsToTest);
        assertNotSame(expectedLocations, locationsToTest);
        assertTrue(expectedLocations.size() <= locationsToTest.size());
        Map<Integer, LocationDTO> map = ReefDbBeans.mapById(locationsToTest);
        for (LocationDTO expectedLocation : expectedLocations) {
            LocationDTO appliedLocationToTest = map.get(expectedLocation.getId());
            assertNotNull(appliedLocationToTest);
            assertEquals(expectedLocation.getLabel(), appliedLocationToTest.getLabel());
            assertEquals(expectedLocation.getName(), appliedLocationToTest.getName());
        }
    }

    private void assertAppliedStrategiesEquals(Collection<AppliedStrategyDTO> expectedAppliedStrategies, Collection<AppliedStrategyDTO> appliedStrategiesToTest) {
        assertNotNull(expectedAppliedStrategies);
        assertNotNull(appliedStrategiesToTest);
        assertNotSame(expectedAppliedStrategies, appliedStrategiesToTest);
        assertTrue(expectedAppliedStrategies.size() <= appliedStrategiesToTest.size());
        Map<Integer, AppliedStrategyDTO> map = ReefDbBeans.mapById(appliedStrategiesToTest);
        for (AppliedStrategyDTO expectedAppliedStrategy : expectedAppliedStrategies) {
            AppliedStrategyDTO appliedStrategyToTest = map.get(expectedAppliedStrategy.getId());
            assertNotNull(appliedStrategyToTest);
            assertEquals(expectedAppliedStrategy.getAppliedStrategyId(), appliedStrategyToTest.getAppliedStrategyId());
            assertEquals(expectedAppliedStrategy.getLabel(), appliedStrategyToTest.getLabel());
            assertEquals(expectedAppliedStrategy.getName(), appliedStrategyToTest.getName());
            assertEquals(expectedAppliedStrategy.getComment(), appliedStrategyToTest.getComment());
            assertEquals(expectedAppliedStrategy.getDepartment(), appliedStrategyToTest.getDepartment());
            assertEquals(expectedAppliedStrategy.getStartDate(), appliedStrategyToTest.getStartDate());
            assertEquals(expectedAppliedStrategy.getEndDate(), appliedStrategyToTest.getEndDate());
        }
    }

    private void assertPmfmStrategiesEquals(Collection<PmfmStrategyDTO> expectedPmfmStrategies, Collection<PmfmStrategyDTO> pmfmStrategiesToTest) {
        assertNotNull(expectedPmfmStrategies);
        assertNotNull(pmfmStrategiesToTest);
        assertNotSame(expectedPmfmStrategies, pmfmStrategiesToTest);
        assertEquals(expectedPmfmStrategies.size(), pmfmStrategiesToTest.size());
        Map<Integer, PmfmStrategyDTO> map = ReefDbBeans.mapById(pmfmStrategiesToTest);
        for (PmfmStrategyDTO expectedPmfmStrategy : expectedPmfmStrategies) {
            PmfmStrategyDTO pmfmStrategyToTest = map.get(expectedPmfmStrategy.getId());
            assertNotNull(pmfmStrategyToTest);
            assertEquals(expectedPmfmStrategy.getPmfm(), pmfmStrategyToTest.getPmfm());
            assertEquals(expectedPmfmStrategy.getAnalysisDepartment(), pmfmStrategyToTest.getAnalysisDepartment());
            assertEquals(expectedPmfmStrategy.isSurvey(), pmfmStrategyToTest.isSurvey());
            assertEquals(expectedPmfmStrategy.isSampling(), pmfmStrategyToTest.isSampling());
            assertEquals(expectedPmfmStrategy.isGrouping(), pmfmStrategyToTest.isGrouping());
            assertEquals(expectedPmfmStrategy.isUnique(), pmfmStrategyToTest.isUnique());
        }
    }
}
