package fr.ifremer.reefdb.dao.referential.monitoringLocation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MonitoringLocationDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(MonitoringLocationDaoReadTest.class);
    
    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();
    
    private ReefDbMonitoringLocationDao mlDao; 
    
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        mlDao = ReefDbServiceLocator.instance().getService("reefDbMonitoringLocationDao", ReefDbMonitoringLocationDao.class);
    }
    
    @Test
    public void getAllLocationsByProgramCode() {
        List<LocationDTO> monitoringLocations  = mlDao.getLocationsByCampaignAndProgram(null, null, StatusFilter.ALL);
        assertNotNull(monitoringLocations);
        assertEquals(4, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        
        monitoringLocations  = mlDao.getLocationsByCampaignAndProgram(null, "REMIS", StatusFilter.ALL);
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());

        monitoringLocations  = mlDao.getLocationsByCampaignAndProgram(1, "REMIS", StatusFilter.ALL);
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());
    }

    @Test
    public void getAllReferentialLocations() {
        // national
        List<LocationDTO> monitoringLocations  = mlDao.getAllLocations(StatusFilter.NATIONAL.toStatusCodes());
        assertNotNull(monitoringLocations);
        assertEquals(4, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        
        // local
        monitoringLocations  = mlDao.getAllLocations(StatusFilter.LOCAL.toStatusCodes());
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
    }

    @Test
    public void getAllReferentialLocationByCriteria() {
        // national
        List<LocationDTO> monitoringLocations  = mlDao.findLocations(StatusFilter.NATIONAL.toStatusCodes(), null, null, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(4, monitoringLocations.size());
        if (log.isDebugEnabled()) {
            for (LocationDTO dto: monitoringLocations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.NATIONAL.toStatusCodes(), "ZONESMARINES", null, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(3, monitoringLocations.size());
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.NATIONAL.toStatusCodes(), "ZONESMARINES", 9, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(1, monitoringLocations.size());
        assertEquals((Integer)1, monitoringLocations.get(0).getId());
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.NATIONAL.toStatusCodes(), null, 9, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(1, monitoringLocations.size());
        assertEquals((Integer)1, monitoringLocations.get(0).getId());
        
        monitoringLocations  = mlDao.findLocations(StatusFilter.NATIONAL.toStatusCodes(), null, null, "REMIS", null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());

        // local
        monitoringLocations  = mlDao.findLocations(StatusFilter.LOCAL.toStatusCodes(), null, null, null, null, null, false);
        assertNotNull(monitoringLocations);
        assertEquals(2, monitoringLocations.size());
    }


}
