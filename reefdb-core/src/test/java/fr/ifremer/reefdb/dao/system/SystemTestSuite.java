package fr.ifremer.reefdb.dao.system;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dao.system.context.ContextDaoReadTest;
import fr.ifremer.reefdb.dao.system.filter.FilterDaoReadTest;
import fr.ifremer.reefdb.dao.system.filter.FilterDaoWriteTest;
import fr.ifremer.reefdb.dao.system.rule.RuleDaoReadTest;
import fr.ifremer.reefdb.dao.system.rule.RuleListDaoReadTest;
import fr.ifremer.reefdb.dao.system.rule.RuleListDaoWriteTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Ludovic
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        RuleListDaoReadTest.class,
        RuleListDaoWriteTest.class,
        RuleDaoReadTest.class,
        ContextDaoReadTest.class,
        FilterDaoReadTest.class,
        FilterDaoWriteTest.class
})
public class SystemTestSuite {

}
