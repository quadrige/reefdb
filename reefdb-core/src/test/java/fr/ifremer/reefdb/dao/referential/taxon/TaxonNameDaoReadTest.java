package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.TaxonDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class TaxonNameDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(TaxonNameDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbTaxonNameDao dao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dao = ReefDbServiceLocator.instance().getService("reefDbTaxonNameDao", ReefDbTaxonNameDao.class);
    }
    
    @Test
    public void getAllTaxonName() {
        List<TaxonDTO> taxons = dao.getAllTaxonNames();
        assertNotNull(taxons);
        Assert.assertTrue(taxons.size() > 15);
    }

    @Test
    public void getTaxonNameByTaxonGroupId() {
        LocalDate refDate = LocalDate.now();
        List<TaxonDTO> taxons = (List<TaxonDTO>) dao.getAllTaxonNamesMapByTaxonGroupId(refDate).get(6);
        assertNotNull(taxons);
        assertEquals(2, taxons.size());
        if (log.isDebugEnabled()) {
            log.debug("taxons for taxon group id = 6");
            for (TaxonDTO dto : taxons) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        taxons = (List<TaxonDTO>) dao.getAllTaxonNamesMapByTaxonGroupId(refDate).get(7);
        assertNotNull(taxons);
        assertEquals(0, taxons.size());
        if (log.isDebugEnabled()) {
            log.debug("no taxon for taxon group id = 7 because historical record has an end date");
        }
        taxons = (List<TaxonDTO>) dao.getAllTaxonNamesMapByTaxonGroupId(refDate).get(9);
        assertNotNull(taxons);
        assertEquals(1, taxons.size());
        if (log.isDebugEnabled()) {
            log.debug("taxons for taxon group id = 9");
            for (TaxonDTO dto : taxons) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        taxons = (List<TaxonDTO>) dao.getAllTaxonNamesMapByTaxonGroupId(refDate).get(1);
        assertNotNull(taxons);
        assertEquals(0, taxons.size());
        if (log.isDebugEnabled()) {
            log.debug("no taxons for taxon group id = 1");
        }

        // with a reference date
        refDate = LocalDate.of(1990,1,1);
        taxons = (List<TaxonDTO>) dao.getAllTaxonNamesMapByTaxonGroupId(refDate).get(7);
        assertNotNull(taxons);
        assertEquals(1, taxons.size());
        if (log.isDebugEnabled()) {
            log.debug("taxons for taxon group id = 7 on reference date = " + refDate);
            for (TaxonDTO dto : taxons) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        TaxonDTO taxon = taxons.get(0);

        // test a reverse map
        Multimap<Integer, TaxonDTO> map = dao.getAllTaxonNamesMapByTaxonGroupId(refDate);
        Multimap<TaxonDTO, Integer> inverseMap = Multimaps.invertFrom(map, ArrayListMultimap.create());

        List<Integer> taxonGroupIds = (List<Integer>) inverseMap.get(taxon);
        assertNotNull(taxonGroupIds);
        assertEquals(1, taxonGroupIds.size());
        assertEquals(7, (int) taxonGroupIds.get(0));

        // reverse map without reference date (the taxon 6 is in group 6 after 1995-08-06)
        refDate = LocalDate.now();
        map = dao.getAllTaxonNamesMapByTaxonGroupId(refDate);
        inverseMap = Multimaps.invertFrom(map, ArrayListMultimap.create());

        taxonGroupIds = (List<Integer>) inverseMap.get(taxon);
        assertNotNull(taxonGroupIds);
        assertEquals(1, taxonGroupIds.size());
        assertEquals(6, (int) taxonGroupIds.get(0));
    }

    @Test
    public void getTaxonNameById() {
        TaxonDTO taxon = dao.getTaxonNameByReferenceId(12);
        assertNotNull(taxon);
        assertEquals("Clitellata", taxon.getName());
        
        try {
            dao.getTaxonNameByReferenceId(20);
            fail("should throw a DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
        
    }
}
