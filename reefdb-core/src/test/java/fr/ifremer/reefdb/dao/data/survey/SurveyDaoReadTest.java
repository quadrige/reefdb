package fr.ifremer.reefdb.dao.data.survey;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class SurveyDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(SurveyDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }

    @Test
    public void getSurvey() {

        List<String> programCodes = Collections.singletonList(dbResource.getFixtures().getProgramCode());

        List<SurveyDTO> surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, null, null, true);
        assertNotNull(surveys);
        assertTrue(surveys.size() >= 3); // should be 3 but if SamplingOperationDaoWriteTest is executed before, it should be more surveys

        // filter survey date > 2014-01-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, Date.valueOf("2014-01-01"), null, true);
        assertNotNull(surveys);
        int nbSurveys = surveys.size();
        for (SurveyDTO survey : surveys) {
            // the survey with id=1 must not be here
            assertNotEquals((Integer) 1, survey.getId());
        }

        // filter survey date >= 2014-01-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, Date.valueOf("2014-01-01"), null, false);
        assertNotNull(surveys);
        assertTrue(nbSurveys < surveys.size());
        boolean found = false;
        for (SurveyDTO survey : surveys) {
            // the survey with id=1 must be here
            if (survey.getId() == 101) {
                found = true;
                break;
            }
        }
        assertTrue(found);

        // filter survey date < 2016-01-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, null, Date.valueOf("2016-01-01"), true);
        assertNotNull(surveys);
        nbSurveys = surveys.size();
        for (SurveyDTO survey : surveys) {
            // the survey with id=3 must not be here
            assertNotEquals((Integer) 103, survey.getId());
        }

        // filter survey date <= 2016-01-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, null, Date.valueOf("2016-01-01"), false);
        assertNotNull(surveys);
        assertTrue(nbSurveys < surveys.size());
        found = false;
        for (SurveyDTO survey : surveys) {
            // the survey with id=3 must be here
            if (survey.getId() == 103) {
                found = true;
                break;
            }
        }
        assertTrue(found);

        // filter survey date between 2014-01-01 & 2015-01-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, Date.valueOf("2014-01-01"), Date.valueOf("2015-01-01"), false);
        assertNotNull(surveys);
        assertEquals(2, surveys.size()); // the id=1 & id=2
        boolean found1 = false;
        boolean found2 = false;
        for (SurveyDTO survey : surveys) {
            if (survey.getId() == 101) {
                found1 = true;
            }
            if (survey.getId() == 102) {
                found2 = true;
            }
        }
        assertTrue(found1);
        assertTrue(found2);

        // filter survey date = 2015-01-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, Date.valueOf("2015-01-01"), Date.valueOf("2015-01-01"), false);
        assertNotNull(surveys);
        assertEquals(1, surveys.size());
        assertEquals((Integer) 102, surveys.get(0).getId());

        // filter survey date = 2015-02-01
        surveys = surveyDao.getSurveysByCriteria(null, programCodes, null, null, null, null, Date.valueOf("2015-02-01"), Date.valueOf("2015-02-01"), false);
        assertNotNull(surveys);
        assertEquals(0, surveys.size());

        // load a detailed survey
        SurveyDTO surveyDetail = surveyDao.getSurveyById(101, false);
        assertNotNull(surveyDetail);

    }

}
