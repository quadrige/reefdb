package fr.ifremer.reefdb.dao.data.survey;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.administration.user.ReefDbQuserDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

public class CampaignDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(CampaignDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbCampaignDao campaignDao;
    private ReefDbQuserDao quserDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        campaignDao = ReefDbServiceLocator.instance().getService("reefDbCampaignDao", ReefDbCampaignDao.class);
        quserDao = ReefDbServiceLocator.instance().getService("reefDbQuserDao", ReefDbQuserDao.class);
    }

    @Test
    public void saveCampaign() {
        CampaignDTO campaign = ReefDbBeanFactory.newCampaignDTO();
        String testName = "campaign test";
        campaign.setName(testName);
        LocalDate testDate = LocalDate.of(2019, 1, 1);
        campaign.setStartDate(testDate);
        PersonDTO testUser = quserDao.getUserById(dbResource.getFixtures().getUserIdWithDataForSynchro());
        campaign.setManager(testUser);
        campaign.setDirty(true);

        campaignDao.saveCampaign(campaign);
        Assert.assertNotNull(campaign.getId());

        List<CampaignDTO> campaigns = campaignDao.getCampaignsByName(testName);
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(1, campaigns.size());
        campaign = campaigns.get(0);

        Assert.assertEquals(testName, campaign.getName());
        Assert.assertEquals(testDate, campaign.getStartDate());
        Assert.assertNull(campaign.getEndDate());
        Assert.assertNotNull(campaign.getManager());
        Assert.assertEquals(dbResource.getFixtures().getUserIdWithDataForSynchro(), campaign.getManager().getId().intValue());
        Assert.assertNotNull(campaign.getRecorderDepartment());
        Assert.assertEquals(2, campaign.getRecorderDepartment().getId().intValue());

    }

}
