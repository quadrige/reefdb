/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.dao.system.filter;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.dto.referential.LocationDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

/**
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class FilterDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(FilterDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbFilterDao filterDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        filterDao = ReefDbServiceLocator.instance().getService("reefDbFilterDao", ReefDbFilterDao.class);
    }

    @Test
    public void saveFilter() {

        // test new one
        FilterDTO f = newFilterDTO();
        f.setFilterTypeId(FilterTypeValues.LOCATION.getFilterTypeId());
        f.setDirty(true);

        filterDao.saveFilter(f, 1); // user 1
        Integer newFilterId = f.getId();
        Assert.assertNotNull(newFilterId);
        log.info("Filter saved with id " + newFilterId);

        // test update
        f = filterDao.getFilterById(newFilterId); // new Filter
        Assert.assertNotNull(f);
        Assert.assertNotNull(f.getId());
        f.setName("test update");

        filterDao.saveFilter(f, 1);

        Assert.assertEquals("test update", f.getName());

        filterDao.deleteFilters(ImmutableList.of(f.getId()));
        // deleted filter should not exist anymore
        f = filterDao.getFilterById(newFilterId);
        Assert.assertNull(f);

        // TODO try to save other filter types (program, ...)

    }

    @Test
    public void deleteFilter() {
        List<FilterDTO> filters = filterDao.getAllContextFilters(null, FilterTypeValues.DEPARTMENT.getFilterTypeId());
        Assert.assertNotNull(filters);
        Assert.assertTrue(filters.size() >= 1);
        for (FilterDTO f : filters) {
            Integer filterId = f.getId();
            if (filterId > 1000) {
                filterDao.deleteFilters(ImmutableList.of(f.getId()));
                f = filterDao.getFilterById(filterId);
                Assert.assertNull(f);
            }
        }

    }

    private FilterDTO newFilterDTO() {
        FilterDTO f = ReefDbBeanFactory.newFilterDTO();
        LocationDTO l1 = ReefDbBeanFactory.newLocationDTO();
        l1.setId(2); // Digue du Braek
        l1.setLabel("L1");

        f.setFilterTypeId(FilterTypeValues.LOCATION.getFilterTypeId());
        f.setName("test new");

        f.setElements(Lists.newArrayList(l1));

        return f;
    }

}
