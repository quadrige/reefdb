package fr.ifremer.reefdb.dao;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public class ReefDbDatabaseFixtures {

    public ReefDbDatabaseFixtures() {
       
    }

    public int getUserIdWithDataForSynchro() {
        return 2; /* = C.Belin - resp. prog REMIS */
    }

    public int getPmfmId(int index) {
        if (index == 0) {
            return 630; // DEPTHVALUES + have a transcribing item
        }
        throw new IllegalArgumentException("no PMFM found in fixtures, for index=" + index);
    }

    public int getPmfmIdWithTranscribing() {
        return getPmfmId(0);
    }

    public int getUnvalidatedSurveyId() {
        return 103;
    }

    public int getValidatedSurveyId() {
        return 110;
    }

    public String getProgramCode() {
        return "REMIS";
    }

    public String getRuleListCode(int index) {
        switch (index) {
            case 0:
                return "RULELIST1"; // base rule list
            case 1:
                return "RULELIST2"; // rule list with preconditions
        }
        throw new IllegalArgumentException("no rule list code found in fixtures, for index=" + index);
    }
}
