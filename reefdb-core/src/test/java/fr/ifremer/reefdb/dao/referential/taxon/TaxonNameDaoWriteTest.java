package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class TaxonNameDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(TaxonNameDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbTaxonNameDao dao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dao = ReefDbServiceLocator.instance().getService("reefDbTaxonNameDao", ReefDbTaxonNameDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }


    @Test
    public void replaceTaxon() {

        Integer SOURCE_TAXON_NAME_ID = 14;
        Integer SOURCE_REF_TAXON_ID = 6;

        Integer TARGET_TAXON_NAME_ID = 2;
        Integer TARGET_REF_TAXON_ID = 2;

        assertTrue(dao.isReferenceTaxonUsedInReferential(SOURCE_REF_TAXON_ID, SOURCE_TAXON_NAME_ID));
        assertTrue(dao.isReferenceTaxonUsedInData(SOURCE_REF_TAXON_ID));
        assertTrue(dao.isReferenceTaxonUsedInValidatedData(SOURCE_REF_TAXON_ID));

        try {
            dao.replaceTemporaryTaxon(SOURCE_TAXON_NAME_ID, SOURCE_REF_TAXON_ID, TARGET_TAXON_NAME_ID, TARGET_REF_TAXON_ID, true);
            fail("should throw exception");
        } catch (Exception e) {
            assertNotNull(e);
        }

        dao.replaceTemporaryTaxon(SOURCE_TAXON_NAME_ID, SOURCE_REF_TAXON_ID, TARGET_TAXON_NAME_ID, TARGET_REF_TAXON_ID, false);
        assertTrue(dao.isReferenceTaxonUsedInReferential(SOURCE_REF_TAXON_ID, SOURCE_TAXON_NAME_ID));
        assertTrue(dao.isReferenceTaxonUsedInData(SOURCE_REF_TAXON_ID));
        assertTrue(dao.isReferenceTaxonUsedInValidatedData(SOURCE_REF_TAXON_ID));

        // load unvalidated survey
        {
            SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
            assertNotNull(unvalidatedSurvey);
            assertNull(unvalidatedSurvey.getValidationDate());

            boolean targetRefTaxonFound = false;
            assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
            for (SamplingOperationDTO samplingOperation : unvalidatedSurvey.getSamplingOperations()) {

                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        assertNull(measurement.getValidationDate());
                        if (measurement.getTaxon() != null) {
                            assertNotEquals(SOURCE_REF_TAXON_ID, measurement.getTaxon().getReferenceTaxonId());
                            if (Objects.equals(measurement.getTaxon().getReferenceTaxonId(), TARGET_REF_TAXON_ID)) {
                                targetRefTaxonFound = true;
                            }
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        assertNull(measurement.getValidationDate());
                        if (measurement.getTaxon() != null) {
                            assertNotEquals(SOURCE_REF_TAXON_ID, measurement.getTaxon().getReferenceTaxonId());
                            if (Objects.equals(measurement.getTaxon().getReferenceTaxonId(), TARGET_REF_TAXON_ID)) {
                                targetRefTaxonFound = true;
                            }
                        }
                    }
                }
            }
            assertTrue(targetRefTaxonFound);
        }

        // load validated survey
        {
            SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
            assertNotNull(validatedSurvey);
            assertNotNull(validatedSurvey.getValidationDate());

            boolean sourceRefTaxonFound = false;
            assertTrue(CollectionUtils.isNotEmpty(validatedSurvey.getSamplingOperations()));
            for (SamplingOperationDTO samplingOperation : validatedSurvey.getSamplingOperations()) {

                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        assertNotNull(measurement.getValidationDate());
                        if (measurement.getTaxon() != null) {
                            assertNotEquals(TARGET_REF_TAXON_ID, measurement.getTaxon().getReferenceTaxonId());
                            if (Objects.equals(measurement.getTaxon().getReferenceTaxonId(), SOURCE_REF_TAXON_ID)) {
                                sourceRefTaxonFound = true;
                            }
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        assertNotNull(measurement.getValidationDate());
                        if (measurement.getTaxon() != null) {
                            assertNotEquals(TARGET_REF_TAXON_ID, measurement.getTaxon().getReferenceTaxonId());
                            if (Objects.equals(measurement.getTaxon().getReferenceTaxonId(), SOURCE_REF_TAXON_ID)) {
                                sourceRefTaxonFound = true;
                            }
                        }
                    }
                }
            }
            assertTrue(sourceRefTaxonFound);
        }

    }
}
