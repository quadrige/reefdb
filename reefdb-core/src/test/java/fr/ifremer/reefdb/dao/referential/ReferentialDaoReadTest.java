package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.quadrige3.ui.core.dto.referential.StatusDTO;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.*;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.List;

import static org.junit.Assert.*;

public class ReferentialDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(ReferentialDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbReferentialDao dao;
    private ReefDbAnalysisInstrumentDao analysisInstrumentDao;
    private ReefDbSamplingEquipmentDao samplingEquipmentDao;
    private ReefDbUnitDao unitDao;


    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dao = ReefDbServiceLocator.instance().getService("reefDbReferentialDao", ReefDbReferentialDao.class);
        analysisInstrumentDao = ReefDbServiceLocator.instance().getService("reefDbAnalysisInstrumentDao", ReefDbAnalysisInstrumentDao.class);
        samplingEquipmentDao = ReefDbServiceLocator.instance().getService("reefDbSamplingEquipmentDao", ReefDbSamplingEquipmentDao.class);
        unitDao = ReefDbServiceLocator.instance().getService("reefDbUnitDao", ReefDbUnitDao.class);
    }

    @Test
    public void getAllDepthLevel() {
        List<LevelDTO> depthLevels = dao.getAllDepthLevels();
        assertNotNull(depthLevels);
        assertEquals(3, depthLevels.size());

        if (log.isDebugEnabled()) {
            for (LevelDTO dto : depthLevels) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        LevelDTO depthLevel = dao.getDepthLevelById(1);
        assertNotNull(depthLevel);
        assertEquals("Immergé", depthLevel.getName());

        try {
            dao.getDepthLevelById(10);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }
    
    @Test
    public void getAllDepthValues() {
        List<DepthDTO> depthValues = dao.getAllDepthValues();
        assertNotNull(depthValues);
        assertTrue(depthValues.size() > 0); // 6 in dataset
        
        DepthDTO depthValue = dao.getDepthValueById(101);
        assertNotNull(depthValue);
        
        depthValue = dao.getDepthValueById(200);
        assertNull(depthValue);
    }
    
    @Test
    public void getAllUnit() {
        List<UnitDTO> units = unitDao.getAllUnits(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(units);
        assertEquals(16, units.size());

        if (log.isDebugEnabled()) {
            for (UnitDTO dto : units) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        units = unitDao.findUnits(null, StatusFilter.LOCAL.intersect(StatusCode.LOCAL_DISABLE.getValue()));
        assertNotNull(units);
        assertEquals(0, units.size());

        units = unitDao.findUnits(2, StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(units);
        assertEquals(1, units.size());
        assertEquals((Integer) 2, units.get(0).getId());

        UnitDTO unit = unitDao.getUnitById(1);
        assertNotNull(unit);
        assertEquals("%", unit.getSymbol());

        try {
            unitDao.getUnitById(20);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void getAllSamplingEquipment() {
        List<SamplingEquipmentDTO> samplingEquipments = samplingEquipmentDao.getAllSamplingEquipments(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(samplingEquipments);
        assertEquals(6, samplingEquipments.size());

        if (log.isDebugEnabled()) {
            for (SamplingEquipmentDTO dto : samplingEquipments) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        samplingEquipments = samplingEquipmentDao.findSamplingEquipments(StatusFilter.ACTIVE.toStatusCodes(), null, 1);
        assertNotNull(samplingEquipments);
        assertEquals(0, samplingEquipments.size());

        samplingEquipments = samplingEquipmentDao.findSamplingEquipments(StatusFilter.ACTIVE.toStatusCodes(), 2, null);
        assertNotNull(samplingEquipments);
        assertEquals(1, samplingEquipments.size());
        
        SamplingEquipmentDTO samplingEquipment = samplingEquipmentDao.getSamplingEquipmentById(3);
        assertNotNull(samplingEquipment);
        assertEquals("Benne", samplingEquipment.getName());

        try {
            samplingEquipmentDao.getSamplingEquipmentById(7);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void getAllAnalysisInstrument() {
        List<AnalysisInstrumentDTO> analysisInstruments = analysisInstrumentDao.getAllAnalysisInstruments(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(analysisInstruments);
        assertEquals(5, analysisInstruments.size());

        if (log.isDebugEnabled()) {
            for (AnalysisInstrumentDTO dto : analysisInstruments) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        analysisInstruments = analysisInstrumentDao.findAnalysisInstruments(StatusFilter.NATIONAL.intersect(StatusCode.DISABLE.getValue()), null);
        assertNotNull(analysisInstruments);
        assertEquals(1, analysisInstruments.size());
        assertEquals((Integer) 4, analysisInstruments.get(0).getId());
        
        analysisInstruments = analysisInstrumentDao.findAnalysisInstruments(StatusFilter.ACTIVE.toStatusCodes(), 3);
        assertNotNull(analysisInstruments);
        assertEquals(1, analysisInstruments.size());
        assertEquals((Integer) 3, analysisInstruments.get(0).getId());
        

        AnalysisInstrumentDTO dto = analysisInstrumentDao.getAnalysisInstrumentById(2);
        assertNotNull(dto);
        assertEquals("Microscope optique", dto.getName());
        assertEquals("1", dto.getStatus().getCode());

        dto = analysisInstrumentDao.getAnalysisInstrumentById(4);
        assertNotNull(dto);
        assertEquals("0", dto.getStatus().getCode());

        try {
            analysisInstrumentDao.getAnalysisInstrumentById(5);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void getAllStatus() {
        List<StatusDTO> status = dao.getAllStatus(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(status);
        assertEquals(2, status.size());

        if (log.isDebugEnabled()) {
            for (StatusDTO dto : status) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

    }

    @Test
    public void getAllQualityLevel() {
        List<QualityLevelDTO> qualityLevels = dao.getAllQualityFlags(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(qualityLevels);
        assertEquals(4, qualityLevels.size());

        qualityLevels = dao.findQualityFlags(StatusFilter.NATIONAL.intersect(StatusCode.DISABLE.getValue()), null);
        assertNotNull(qualityLevels);
        assertEquals(0, qualityLevels.size());
        
        qualityLevels = dao.findQualityFlags(StatusFilter.NATIONAL.intersect(StatusCode.ENABLE.getValue()), "3");
        assertNotNull(qualityLevels);
        assertEquals(1, qualityLevels.size());
        assertEquals("douteux", qualityLevels.get(0).getName());
        
        QualityLevelDTO qualityLevel = dao.getQualityFlagByCode("0");
        assertNotNull(qualityLevel);
        assertEquals("non qualifié", qualityLevel.getName());
        
        if (log.isDebugEnabled()) {
            for (QualityLevelDTO dto : qualityLevels) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

    }

    @Test
    public void getAllPositioningSystem() {
        List<PositioningSystemDTO> positioningSystems = dao.getAllPositioningSystems();
        assertNotNull(positioningSystems);
        assertEquals(12, positioningSystems.size());

        if (log.isDebugEnabled()) {
            for (PositioningSystemDTO dto : positioningSystems) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

    }

    @Test
    public void getAllGroupingType() {
        List<GroupingTypeDTO> groupingTypes = dao.getAllGroupingTypes();
        assertNotNull(groupingTypes);
        assertEquals(3, groupingTypes.size());
        if (log.isDebugEnabled()) {
            for (GroupingTypeDTO dto : groupingTypes) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        final String groupingTypeCode = "ZONESMARINES";
        boolean found = false;
        for (GroupingTypeDTO groupingType : groupingTypes) {
            if (groupingTypeCode.equals(groupingType.getCode())) {
                found = true;
                break;
            }
        }
        assertTrue(found);

        List<GroupingDTO> groupings = dao.getGroupingsByType(groupingTypeCode);
        assertNotNull(groupings);
        assertEquals(2, groupings.size());
        if (log.isDebugEnabled()) {
            for (GroupingDTO dto : groupings) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

    }
}
