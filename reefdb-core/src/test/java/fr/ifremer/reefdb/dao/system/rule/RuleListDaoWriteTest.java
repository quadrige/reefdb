package fr.ifremer.reefdb.dao.system.rule;

/*
 * #%L
 * ReefDb :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.configuration.control.ControlRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.PreconditionRuleDTO;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.dto.configuration.control.RulePmfmDTO;
import fr.ifremer.reefdb.dto.enums.ControlElementValues;
import fr.ifremer.reefdb.dto.enums.ControlFeatureMeasurementValues;
import fr.ifremer.reefdb.dto.enums.ControlFunctionValues;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.referential.ReferentialService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.*;

public class RuleListDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(RuleListDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbRuleListDao ruleListDao;
    private ReferentialService referentialService;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        ruleListDao = ReefDbServiceLocator.instance().getService("reefDbRuleListDao", ReefDbRuleListDao.class);
        referentialService = ReefDbServiceLocator.instance().getReferentialService();
    }

    @Test
    public void saveRuleListWithPreconditionedQualitativeRules() {

        // RULELIST2 comes from test dataset only
        RuleListDTO ruleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(1));
        assertNotNull(ruleList);
        assertEquals("rule list with preconditions", ruleList.getDescription());

        assertEquals(1, ruleList.sizeControlRules());
        ControlRuleDTO controlRule = ruleList.getControlRules(0);
        assertTrue(controlRule.isActive());
        assertEquals("PRECOND1", controlRule.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION_QUALITATIVE.equals(controlRule.getFunction()));
        assertTrue(ControlElementValues.MEASUREMENT.equals(controlRule.getControlElement()));
        assertTrue(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(controlRule.getControlFeature()));

        assertEquals(2, controlRule.sizeRulePmfms());
        assertEquals(4, referentialService.getUniquePmfmIdFromPmfm(controlRule.getRulePmfms(0).getPmfm()).intValue());
        assertEquals(24, referentialService.getUniquePmfmIdFromPmfm(controlRule.getRulePmfms(1).getPmfm()).intValue());

        assertEquals(2, controlRule.sizePreconditions());
        PreconditionRuleDTO precondition1 = ReefDbBeans.findById(controlRule.getPreconditions(), 1);
        assertNotNull(precondition1);
        assertEquals(1, precondition1.getId().intValue());
        assertTrue(precondition1.isActive());
        assertTrue(precondition1.isBidirectional());
        assertEquals("SALMOI1", precondition1.getBaseRule().getCode());
        assertEquals("2", precondition1.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO1", precondition1.getUsedRule().getCode());
        assertEquals("4", precondition1.getUsedRule().getAllowedValues());

        PreconditionRuleDTO precondition2 = ReefDbBeans.findById(controlRule.getPreconditions(), 2);
        assertNotNull(precondition2);
        assertEquals(2, precondition2.getId().intValue());
        assertTrue(precondition2.isActive());
        assertFalse(precondition2.isBidirectional());
        assertEquals("SALMOI2", precondition2.getBaseRule().getCode());
        assertEquals("3", precondition2.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO2", precondition2.getUsedRule().getCode());
        assertEquals("5,6", precondition2.getUsedRule().getAllowedValues());

        // modify the last value
        precondition2.getUsedRule().setAllowedValues("5");

        // save
        ruleListDao.saveRuleList(ruleList, dbResource.getFixtures().getUserIdWithDataForSynchro());

        // reload
        RuleListDTO reloadedRuleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(1));
        assertNotNull(reloadedRuleList);
        assertEquals("rule list with preconditions", reloadedRuleList.getDescription());

        assertEquals(1, reloadedRuleList.sizeControlRules());
        ControlRuleDTO reloadedControlRule = reloadedRuleList.getControlRules(0);
        assertTrue(reloadedControlRule.isActive());
        assertEquals("PRECOND1", reloadedControlRule.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION_QUALITATIVE.equals(reloadedControlRule.getFunction()));
        assertTrue(ControlElementValues.MEASUREMENT.equals(reloadedControlRule.getControlElement()));
        assertTrue(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(reloadedControlRule.getControlFeature()));

        assertEquals(2, reloadedControlRule.sizeRulePmfms());
        assertEquals(4, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRule.getRulePmfms(0).getPmfm()).intValue());
        assertEquals(24, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRule.getRulePmfms(1).getPmfm()).intValue());

        assertEquals(2, reloadedControlRule.sizePreconditions());
        PreconditionRuleDTO reloadedPrecondition1 = ReefDbBeans.findById(reloadedControlRule.getPreconditions(), 1);
        assertNotNull(reloadedPrecondition1);
        assertEquals(1, reloadedPrecondition1.getId().intValue());
        assertTrue(reloadedPrecondition1.isActive());
        assertTrue(reloadedPrecondition1.isBidirectional());
        assertEquals("SALMOI1", reloadedPrecondition1.getBaseRule().getCode());
        assertEquals("2", reloadedPrecondition1.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO1", reloadedPrecondition1.getUsedRule().getCode());
        assertEquals("4", reloadedPrecondition1.getUsedRule().getAllowedValues());

        PreconditionRuleDTO reloadedPrecondition2 = ReefDbBeans.findById(reloadedControlRule.getPreconditions(), 2);
        assertNotNull(reloadedPrecondition2);
        assertEquals(2, reloadedPrecondition2.getId().intValue());
        assertTrue(reloadedPrecondition2.isActive());
        assertFalse(reloadedPrecondition2.isBidirectional());
        assertEquals("SALMOI2", reloadedPrecondition2.getBaseRule().getCode());
        assertEquals("3", reloadedPrecondition2.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO2", reloadedPrecondition2.getUsedRule().getCode());
        assertEquals("5", reloadedPrecondition2.getUsedRule().getAllowedValues());

        // remove a precondition
        reloadedControlRule.removePreconditions(reloadedPrecondition1);

        // save
        ruleListDao.saveRuleList(reloadedRuleList, dbResource.getFixtures().getUserIdWithDataForSynchro());

        // reload
        RuleListDTO reloadedRuleListBis = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(1));
        assertNotNull(reloadedRuleListBis);
        assertEquals("rule list with preconditions", reloadedRuleListBis.getDescription());

        assertEquals(1, reloadedRuleListBis.sizeControlRules());
        ControlRuleDTO reloadedControlRuleBis = reloadedRuleListBis.getControlRules(0);
        assertTrue(reloadedControlRuleBis.isActive());
        assertEquals("PRECOND1", reloadedControlRuleBis.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION_QUALITATIVE.equals(reloadedControlRuleBis.getFunction()));
        assertTrue(ControlElementValues.MEASUREMENT.equals(reloadedControlRuleBis.getControlElement()));
        assertTrue(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.equals(reloadedControlRuleBis.getControlFeature()));

        assertEquals(2, reloadedControlRuleBis.sizeRulePmfms());
        assertEquals(4, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRuleBis.getRulePmfms(0).getPmfm()).intValue());
        assertEquals(24, referentialService.getUniquePmfmIdFromPmfm(reloadedControlRuleBis.getRulePmfms(1).getPmfm()).intValue());

        assertEquals(1, reloadedControlRuleBis.sizePreconditions());
        PreconditionRuleDTO reloadedPrecondition2Bis = ReefDbBeans.findById(reloadedControlRuleBis.getPreconditions(), 2);
        assertNotNull(reloadedPrecondition2Bis);
        assertEquals(2, reloadedPrecondition2Bis.getId().intValue());
        assertTrue(reloadedPrecondition2Bis.isActive());
        assertFalse(reloadedPrecondition2Bis.isBidirectional());
        assertEquals("SALMOI2", reloadedPrecondition2Bis.getBaseRule().getCode());
        assertEquals("3", reloadedPrecondition2Bis.getBaseRule().getAllowedValues());
        assertEquals("SALMOITAXO2", reloadedPrecondition2Bis.getUsedRule().getCode());
        assertEquals("5", reloadedPrecondition2Bis.getUsedRule().getAllowedValues());

    }

    @Test
    public void saveRuleListWithPreconditionedNumericalRules() {

        // RULELIST1 comes from test dataset only
        RuleListDTO ruleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(0));
        assertNotNull(ruleList);
        assertEquals("rule list 1", ruleList.getDescription());

        assertEquals(1, ruleList.sizeControlRules());
        ControlRuleDTO controlRule = ruleList.getControlRules(0);
        assertTrue(controlRule.isActive());
        assertEquals("RULE1", controlRule.getCode());

        // add preconditioned rules
        ControlRuleDTO precondRule1_1 = ReefDbBeanFactory.newControlRuleDTO();
        precondRule1_1.setCode("PRECOND_RULE1_1");
        precondRule1_1.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        precondRule1_1.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        precondRule1_1.setFunction(ControlFunctionValues.IS_AMONG.toFunctionDTO());
        precondRule1_1.addRulePmfms(getRulePmfm(4));
        precondRule1_1.setAllowedValues("2");
        ControlRuleDTO precondRule1_2 = ReefDbBeanFactory.newControlRuleDTO();
        precondRule1_2.setCode("PRECOND_RULE1_2");
        precondRule1_2.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        precondRule1_2.setControlFeature(ControlFeatureMeasurementValues.NUMERICAL_VALUE.toControlFeatureDTO());
        precondRule1_2.setFunction(ControlFunctionValues.MIN_MAX.toFunctionDTO());
        precondRule1_2.addRulePmfms(getRulePmfm(3));
        precondRule1_2.setMin(10);
        precondRule1_2.setMax(19);
        ControlRuleDTO precondRule2_1 = ReefDbBeanFactory.newControlRuleDTO();
        precondRule2_1.setCode("PRECOND_RULE2_1");
        precondRule2_1.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        precondRule2_1.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        precondRule2_1.setFunction(ControlFunctionValues.IS_AMONG.toFunctionDTO());
        precondRule2_1.addRulePmfms(getRulePmfm(4));
        precondRule2_1.setAllowedValues("3");
        ControlRuleDTO precondRule2_2 = ReefDbBeanFactory.newControlRuleDTO();
        precondRule2_2.setCode("PRECOND_RULE2_2");
        precondRule2_2.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        precondRule2_2.setControlFeature(ControlFeatureMeasurementValues.NUMERICAL_VALUE.toControlFeatureDTO());
        precondRule2_2.setFunction(ControlFunctionValues.MIN_MAX.toFunctionDTO());
        precondRule2_2.addRulePmfms(getRulePmfm(3));
        precondRule2_2.setMin(20);
        precondRule2_2.setMax(29);

        ControlRuleDTO precondRule = ReefDbBeanFactory.newControlRuleDTO();
        precondRule.setCode("PRECOND_RULE");
        precondRule.setActive(true);
        precondRule.setControlElement(ControlElementValues.MEASUREMENT.toControlElementDTO());
        precondRule.setControlFeature(ControlFeatureMeasurementValues.QUALITATIVE_VALUE.toControlFeatureDTO());
        precondRule.setFunction(ControlFunctionValues.PRECONDITION_NUMERICAL.toFunctionDTO());
        PreconditionRuleDTO precondition1 = ReefDbBeanFactory.newPreconditionRuleDTO();
        precondition1.setActive(true);
        precondition1.setBidirectional(true);
        precondition1.setRule(precondRule);
        precondition1.setName(precondRule.getCode());
        precondition1.setBaseRule(precondRule1_1);
        precondition1.setUsedRule(precondRule1_2);
        precondRule.addPreconditions(precondition1);
        PreconditionRuleDTO precondition2 = ReefDbBeanFactory.newPreconditionRuleDTO();
        precondition2.setActive(true);
        precondition2.setBidirectional(true);
        precondition2.setRule(precondRule);
        precondition2.setName(precondRule.getCode());
        precondition2.setBaseRule(precondRule2_1);
        precondition2.setUsedRule(precondRule2_2);
        precondRule.addPreconditions(precondition2);

        ruleList.addControlRules(precondRule);
        ruleList.setDescription("rule list 1 with preconditions");

        // save
        ruleListDao.saveRuleList(ruleList, dbResource.getFixtures().getUserIdWithDataForSynchro());

        // reload
        RuleListDTO reloadedRuleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(0));
        assertNotNull(reloadedRuleList);
        assertEquals("rule list 1 with preconditions", reloadedRuleList.getDescription());

        assertEquals(2, reloadedRuleList.sizeControlRules());
        ControlRuleDTO reloadedControlRule = reloadedRuleList.getControlRules(0);
        assertTrue(reloadedControlRule.isActive());
        assertEquals("RULE1", reloadedControlRule.getCode());

        // get preconditioned rule
        reloadedControlRule = reloadedRuleList.getControlRules(1);
        assertTrue(reloadedControlRule.isActive());
        assertEquals("PRECOND_RULE", reloadedControlRule.getCode());
        assertTrue(ControlFunctionValues.PRECONDITION_NUMERICAL.equals(reloadedControlRule.getFunction()));
        assertEquals(2, reloadedControlRule.sizePreconditions());

        // assert precondition 1
        assertPrecondition(precondition1, reloadedControlRule.getPreconditions(0));
        assertPrecondition(precondition2, reloadedControlRule.getPreconditions(1));

        // assert pmfms
        assertEquals(2, reloadedControlRule.sizeRulePmfms());
        assertPmfm(precondRule1_1.getRulePmfms(0).getPmfm(), reloadedControlRule.getRulePmfms(0).getPmfm());
        assertPmfm(precondRule1_2.getRulePmfms(0).getPmfm(), reloadedControlRule.getRulePmfms(1).getPmfm());
    }

    private RulePmfmDTO getRulePmfm(int pmfmId) {
        RulePmfmDTO rulePmfm = ReefDbBeanFactory.newRulePmfmDTO();
        rulePmfm.setPmfm(referentialService.getPmfm(pmfmId));
        return rulePmfm;
    }

    private void assertPrecondition(PreconditionRuleDTO expectedPrecondition, PreconditionRuleDTO precondition) {
        assertEquals(expectedPrecondition.getName(), precondition.getName());
        assertEquals(expectedPrecondition.getRule().getCode(), precondition.getRule().getCode());
        assertEquals(expectedPrecondition.getBaseRule().getCode(), precondition.getBaseRule().getCode());
        assertEquals(expectedPrecondition.getUsedRule().getCode(), precondition.getUsedRule().getCode());
    }

    private void assertPmfm(PmfmDTO expectedPmfm, PmfmDTO pmfm) {
        assertEquals(expectedPmfm.getParameter().getCode(), pmfm.getParameter().getCode());
        assertEquals(expectedPmfm.getMatrix().getId(), pmfm.getMatrix().getId());
        assertEquals(expectedPmfm.getFraction().getId(), pmfm.getFraction().getId());
        assertEquals(expectedPmfm.getMethod().getId(), pmfm.getMethod().getId());
    }
}
