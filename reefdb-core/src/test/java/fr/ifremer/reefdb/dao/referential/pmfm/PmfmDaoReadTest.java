package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.referential.pmfm.*;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.util.TimeLog;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class PmfmDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(PmfmDaoReadTest.class);
    private static final TimeLog timeLog = new TimeLog(PmfmDaoReadTest.class, 5000, 1000);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbPmfmDao pmfmDao;
    private ReefDbParameterDao parameterDao;
    private ReefDbMatrixDao matrixDao;
    private ReefDbFractionDao fractionDao;
    private ReefDbMethodDao methodDao;
    private ReefDbUnitDao unitDao;
    private ReefDbQualitativeValueDao qualitativeValueDao;
    private CacheService cacheService;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        pmfmDao = ReefDbServiceLocator.instance().getService("reefDbPmfmDao", ReefDbPmfmDao.class);
        parameterDao = ReefDbServiceLocator.instance().getService("reefDbParameterDao", ReefDbParameterDao.class);
        matrixDao = ReefDbServiceLocator.instance().getService("reefDbMatrixDao", ReefDbMatrixDao.class);
        fractionDao = ReefDbServiceLocator.instance().getService("reefDbFractionDao", ReefDbFractionDao.class);
        methodDao = ReefDbServiceLocator.instance().getService("reefDbMethodDao", ReefDbMethodDao.class);
        unitDao = ReefDbServiceLocator.instance().getService("reefDbUnitDao", ReefDbUnitDao.class);
        cacheService = ReefDbServiceLocator.instance().getCacheService();
        qualitativeValueDao = ReefDbServiceLocator.instance().getService("reefDbQualitativeValueDao", ReefDbQualitativeValueDao.class);
    }

    @Test
    public void getParameterGroup() {
        List<ParameterGroupDTO> parameterGroups = parameterDao.getAllParameterGroups(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(parameterGroups);
        assertEquals(8, parameterGroups.size());
        if (log.isDebugEnabled()) {
            for (ParameterGroupDTO dto : parameterGroups) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        ParameterGroupDTO parameterGroup = parameterDao.getParameterGroupById(3);
        assertNotNull(parameterGroup);
        assertEquals("Biologie", parameterGroup.getName());

        parameterGroup = parameterDao.getParameterGroupById(5);
        assertNotNull(parameterGroup);
        assertEquals("Métaux", parameterGroup.getName());
        assertNotNull(parameterGroup.getParentParameterGroup());
        assertEquals((Integer) 2, parameterGroup.getParentParameterGroup().getId());
    }

    @Test
    public void getMethod() {
        List<MethodDTO> methods = methodDao.getAllMethods(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(methods);
        assertEquals(10, methods.size());
        if (log.isDebugEnabled()) {
            for (MethodDTO dto : methods) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        MethodDTO method = methodDao.getMethodById(6);
        assertNotNull(method);
        assertEquals("Gravimétie 500°C", method.getName());

        try {
            methodDao.getMethodById(10);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }

    }

    @Test
    public void getFraction() {
        List<FractionDTO> fractions = fractionDao.getAllFractions(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(fractions);
        assertEquals(6, fractions.size());
        if (log.isDebugEnabled()) {
            for (FractionDTO dto : fractions) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        FractionDTO fraction1 = ReefDbBeans.findById(fractions, 1);
        assertNotNull(fraction1);
        assertEquals("Aucune", fraction1.getName());
        assertEquals(4, fraction1.sizeMatrixes());

        FractionDTO fraction = fractionDao.getFractionById(1);
        assertNotNull(fraction);
        assertEquals("Aucune", fraction.getName());
        assertEquals(4, fraction.sizeMatrixes());

        try {
            fractionDao.getFractionById(10);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void testFractionCache() {

        long startTime = TimeLog.getTime();

        List<FractionDTO> fractions = fractionDao.getAllFractions(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(fractions);
        assertEquals(6, fractions.size());
        startTime = timeLog.log(startTime, "fractions loaded");

        cacheService.clearAllCaches();
        startTime = timeLog.log(startTime, "cache cleared");
        matrixDao.getAllMatrices(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "caches loaded");

        fractions = fractionDao.getAllFractions(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(fractions);
        assertEquals(6, fractions.size());
        startTime = timeLog.log(startTime, "fractions loaded");

    }

    @Test
    public void getMatrix() {
        List<MatrixDTO> matrices = matrixDao.getAllMatrices(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(matrices);
        assertEquals(6, matrices.size());
        if (log.isDebugEnabled()) {
            for (MatrixDTO dto : matrices) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        MatrixDTO matrix = matrixDao.getMatrixById(2);
        assertNotNull(matrix);
        assertEquals("Bivalve", matrix.getName());

        try {
            matrixDao.getMatrixById(10);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void getQualitativeValue() {
        List<QualitativeValueDTO> qualValues = qualitativeValueDao.getQualitativeValuesByParameterCode("SALMOI");
        assertNotNull(qualValues);
        assertEquals(3, qualValues.size()); // 2 actives & 1 inactive
        if (log.isDebugEnabled()) {
            for (QualitativeValueDTO dto : qualValues) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
        qualValues = qualitativeValueDao.getQualitativeValuesByParameterCode("BAD");
        assertNotNull(qualValues);
        assertEquals(0, qualValues.size());

        QualitativeValueDTO qualValue = qualitativeValueDao.getQualitativeValueById(1);
        assertNotNull(qualValue);
        assertEquals("Présentes", qualValue.getName());
    }

    @Test
    public void getParameter() {
        List<ParameterDTO> parameters = parameterDao.getAllParameters(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(parameters);
        assertTrue(parameters.size() >= 10);
        if (log.isDebugEnabled()) {
            for (ParameterDTO dto : parameters) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        try {
            parameterDao.getParameterGroupById(10);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }

        ParameterDTO parameter = parameterDao.getParameterByCode("AL");
        assertNotNull(parameter);
        assertEquals("Aluminium total", parameter.getName());
        assertNotNull(parameter.getParameterGroup());
        assertEquals((Integer) 5, parameter.getParameterGroup().getId());

        try {
            parameterDao.getParameterByCode("BAD");
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void getPmfm() {

        List<PmfmDTO> allPmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(allPmfms);
        assertTrue(allPmfms.size() >= 25);
        if (log.isDebugEnabled()) {
            for (PmfmDTO dto : allPmfms) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        try {
            PmfmDTO pmfm = pmfmDao.getPmfmById(1000);
            fail("should throw a DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }

        PmfmDTO pmfm = pmfmDao.getPmfmById(13);
        assertNotNull(pmfm);
        assertEquals("BIOMZOO", pmfm.getParameter().getCode());
        assertNotNull(pmfm.getParameter().getParameterGroup());
        assertEquals((Integer) 6, pmfm.getParameter().getParameterGroup().getId());
        assertNotNull(pmfm.getParameter().getParameterGroup().getParentParameterGroup());
        assertEquals((Integer) 3, pmfm.getParameter().getParameterGroup().getParentParameterGroup().getId());
        assertEquals((Integer) 3, pmfm.getMatrix().getId());
        assertEquals((Integer) 2, pmfm.getFraction().getId());
        assertEquals((Integer) 6, pmfm.getMethod().getId());
        if (log.isDebugEnabled()) {
            log.debug(ToStringBuilder.reflectionToString(pmfm, ToStringStyle.SHORT_PREFIX_STYLE));
        }

        pmfm = pmfmDao.getPmfmById(29);
        assertNotNull(pmfm);
        assertEquals("ASTAXO", pmfm.getParameter().getCode());
        assertEquals((Integer) 1, pmfm.getMatrix().getId());
        assertEquals((Integer) 1, pmfm.getFraction().getId());
        assertEquals((Integer) 3, pmfm.getMethod().getId());
        if (log.isDebugEnabled()) {
            log.debug(ToStringBuilder.reflectionToString(pmfm, ToStringStyle.SHORT_PREFIX_STYLE));
        }

        // find by criteria
        List<PmfmDTO> pmfms = pmfmDao.findPmfms(null, null, null, null, null, null, StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(pmfms);
        assertEquals(allPmfms.size(), pmfms.size());

        // TODO rewrite tests with new method

//        pmfms = pmfmDao.findPmfms(null, null, null, null, null, StatusFilter.ACTIVE.toStatusCodes());
//        assertNotNull(pmfms);
//        assertEquals(6, pmfms.size());
//
//        pmfms = pmfmDao.findPmfms(StatusFilter.ACTIVE.toStatusCodes(), 4, "ACEPHTE", false, false, false, 2, 1, 2);
//        assertNotNull(pmfms);
//        assertEquals(1, pmfms.size());
//        assertEquals((Integer) 146, pmfms.get(0).getId());
//
//        pmfms = pmfmDao.findPmfms(StatusFilter.ACTIVE.toStatusCodes(), null, null, true, null, null, null, null, null);
//        assertNotNull(pmfms);
//        assertEquals(12, pmfms.size());
//
//        pmfms = pmfmDao.findPmfms(StatusFilter.ACTIVE.toStatusCodes(), null, null, true, false, null, null, null, null);
//        assertNotNull(pmfms);
//        assertEquals(9, pmfms.size());
//
//        pmfms = pmfmDao.findPmfms(StatusFilter.ACTIVE.toStatusCodes(), null, null, true, true, null, null, null, null);
//        assertNotNull(pmfms);
//        assertEquals(3, pmfms.size());
//
//        pmfms = pmfmDao.findPmfms(StatusFilter.ACTIVE.toStatusCodes(), null, null, true, true, true, null, null, null);
//        assertNotNull(pmfms);
//        assertEquals(0, pmfms.size());
//
//        pmfms = pmfmDao.findPmfms(StatusFilter.NATIONAL.intersect(StatusCode.DISABLE.getValue()), null, "BIOMZOOTAXO", true, false, false, null, 2, null);
//        assertNotNull(pmfms);
//        assertEquals(1, pmfms.size());
//        assertEquals((Integer) 166, pmfms.get(0).getId());

    }

    @Test
    public void getPmfmWithTranscribing() {

        List<PmfmDTO> allPmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(allPmfms);

        Map<Integer, PmfmDTO> pmfmByIds = ReefDbBeans.mapById(allPmfms);
        PmfmDTO pmfmWithTranscribing = pmfmByIds.get(dbResource.getFixtures().getPmfmIdWithTranscribing());
        assertNotNull(pmfmWithTranscribing);
        assertNotNull(pmfmWithTranscribing.getName());
        assertNotNull(pmfmWithTranscribing.getParameter());
        assertNotNull(pmfmWithTranscribing.getParameter().getName());

        boolean nameStartWithParameterName = pmfmWithTranscribing.getName().startsWith(pmfmWithTranscribing.getParameter().getName());
        boolean isTranscribing = !nameStartWithParameterName;
        assertTrue(isTranscribing);
    }

    @Test
    public void testPmfmCache() {

        long startTime = TimeLog.getTime();

        // clear cache
        cacheService.clearAllCaches();
        startTime = timeLog.log(startTime, "cache cleared");

        // load all pmfms by query way using caches (3 time)
        List<PmfmDTO> pmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "pmfms loaded");
        pmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "pmfms loaded");
        pmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "pmfms loaded");

        // clear cache
        cacheService.clearAllCaches();
        startTime = timeLog.log(startTime, "cache cleared");

        // load caches
        parameterDao.getAllParameterGroups(StatusFilter.ACTIVE.toStatusCodes());
        parameterDao.getAllParameters(StatusFilter.ACTIVE.toStatusCodes());
        matrixDao.getAllMatrices(StatusFilter.ACTIVE.toStatusCodes());
        fractionDao.getAllFractions(StatusFilter.ACTIVE.toStatusCodes());
        methodDao.getAllMethods(StatusFilter.ACTIVE.toStatusCodes());
        unitDao.getAllUnits(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "caches loaded");

        // load all pmfms by query way using caches (3 time)
        pmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "pmfms loaded");
        pmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "pmfms loaded");
        pmfms = pmfmDao.getAllPmfms(StatusFilter.ACTIVE.toStatusCodes());
        startTime = timeLog.log(startTime, "pmfms loaded");

    }

}
