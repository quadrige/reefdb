/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.ifremer.reefdb.dao.system.filter;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.configuration.filter.FilterDTO;
import fr.ifremer.reefdb.dto.enums.FilterTypeValues;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author Lionel Touseau <lionel.touseau@e-is.pro>
 */
public class FilterDaoReadTest extends AbstractDaoTest {
    
    private static final Log log = LogFactory.getLog(FilterDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbFilterDao filterDao;
    
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        filterDao = ReefDbServiceLocator.instance().getService("reefDbFilterDao", ReefDbFilterDao.class);
    }
    
    @Test
    public void getAllFilter() {
        List<FilterDTO> locFilters = filterDao.getAllContextFilters(1001, FilterTypeValues.LOCATION.getFilterTypeId()); // context 1
        assertNotNull(locFilters);
        assertEquals(1, locFilters.size());
        
        locFilters = filterDao.getAllContextFilters(1002, FilterTypeValues.LOCATION.getFilterTypeId()); // context 2
        Assert.assertTrue(locFilters == null || locFilters.isEmpty());
        
        List<FilterDTO> progFilters = filterDao.getAllContextFilters(null, FilterTypeValues.PROGRAM.getFilterTypeId());
        
        assertEquals(1, progFilters.size());
        assertEquals("Program Filter", progFilters.get(0).getName());
        
    }
    
    @Test
    public void getFilterById() {
        FilterDTO f = filterDao.getFilterById(1001); // filter 1000
        Assert.assertNotNull(f);
        Assert.assertEquals(FilterTypeValues.LOCATION.getFilterTypeId(), f.getFilterTypeId());

        FilterDTO fp = filterDao.getFilterById(1002);
        Assert.assertNotNull(fp);
        Assert.assertEquals(FilterTypeValues.PROGRAM.getFilterTypeId(), fp.getFilterTypeId());

    }
    
}
