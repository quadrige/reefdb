package fr.ifremer.reefdb.dao;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.security.SecurityContextHelper;
import fr.ifremer.quadrige3.core.service.ServiceLocator;
import fr.ifremer.quadrige3.core.test.DatabaseResource;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.config.ReefDbConfigurationOption;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.List;

public class ReefDbDatabaseResource extends DatabaseResource {

    public static final String MODULE_NAME = "reefdb-core";
    public static final String HSQLDB_SRC_DATABASE_DIRECTORY = String.format(HSQLDB_SRC_DATABASE_DIRECTORY_PATTERN, MODULE_NAME);

    private final ReefDbDatabaseFixtures fixtures = new ReefDbDatabaseFixtures();

    public static ReefDbDatabaseResource readDb() {
        return new ReefDbDatabaseResource("");
    }

    public static ReefDbDatabaseResource readDb(String dbName, boolean enableServices) {
        if (!enableServices) {
            return new ReefDbDatabaseResource(dbName, "beanRefFactoryWithNoService.xml", "beanRefFactoryWithNoService", false);
        }
        return new ReefDbDatabaseResource(dbName);
    }

    public static ReefDbDatabaseResource writeDb() {
        return new ReefDbDatabaseResource("", true);
    }

    public static ReefDbDatabaseResource writeDb(String dbName) {
        return new ReefDbDatabaseResource(dbName, true);
    }

    public static ReefDbDatabaseResource writeDb(String dbName, boolean enableServices) {
        if (!enableServices) {
            return new ReefDbDatabaseResource(dbName, "beanRefFactoryWithNoService.xml", "beanRefFactoryWithNoService", true);
        }
        return new ReefDbDatabaseResource(dbName, true);
    }

    public static ReefDbDatabaseResource noDb() {
        return new ReefDbDatabaseResource(
                "", "beanRefFactoryWithNoDb.xml", "beanRefFactoryWithNoDb");
    }

    protected ReefDbDatabaseResource(String dbName) {
        super(dbName, null, null, false);
    }

    protected ReefDbDatabaseResource(String dbName, boolean writeDb) {
        super(dbName, null, null, writeDb);
    }

    protected ReefDbDatabaseResource(String dbName, String beanFactoryReferenceLocation,
                                     String beanRefFactoryReferenceId) {
        super(dbName, beanFactoryReferenceLocation, beanRefFactoryReferenceId, false);
    }

    protected ReefDbDatabaseResource(String dbName, String beanFactoryReferenceLocation,
                                     String beanRefFactoryReferenceId,
                                     boolean writeDb) {
        super(dbName, beanFactoryReferenceLocation, beanRefFactoryReferenceId, writeDb);
    }

    @Override
    protected String getConfigFilesPrefix() {
        return MODULE_NAME + "-test";
    }

    @Override
    protected String getI18nBundleName() {
        return MODULE_NAME + "-i18n";
    }


    @Override
    protected String getModuleDirectory() {
        return MODULE_NAME;
    }

    @Override
    public String getBuildEnvironment() {
        return BUILD_ENVIRONMENT_DEFAULT;
    }

    public ReefDbDatabaseFixtures getFixtures() {
        return fixtures;
    }

    @Override
    protected void before(Description description) throws Throwable {
        // Call inherited method
        super.before(description);

        // Initialize default context
        if (getBeanFactoryReferenceLocation() == null) {
            ServiceLocator.setInstance(ReefDbServiceLocator.instance());
        }

        // Set Authentication (fake user)
        SecurityContextHelper.authenticate("demo", "demo");

        ReefDbServiceLocator.instance().getDataContext().setRecorderPersonId(1001);
        ReefDbServiceLocator.instance().getDataContext().setRecorderDepartmentId(2);

        log.debug(ReefDbConfiguration.getInstance().getJdbcUrl());
    }

    @Override
    protected String[] getConfigArgs() {
        List<String> configArgs = Lists.newArrayList(Arrays.asList(super.getConfigArgs()));
        configArgs.addAll(Lists.newArrayList(
                "--option", ReefDbConfigurationOption.BASEDIR.getKey(), getResourceDirectory().getAbsolutePath()
//                , "--option", QuadrigeCoreConfigurationOption.AUTHENTICATION_DISABLED.getKey(), Boolean.toString(true)
                , "--option", ReefDbConfigurationOption.DB_ENUMERATION_RESOURCE.getKey(), "classpath*:quadrige3-db-enumerations.properties,classpath*:reefdb-db-test-enumerations.properties"
        ));
        return configArgs.toArray(new String[0]);
    }

    /**
     * Convenience methods that could be override to initialize other configuration
     *
     * @param configFilename
     */
    @Override
    protected void initConfiguration(String configFilename) {
        String[] configArgs = getConfigArgs();
        ReefDbConfiguration reefDbConfiguration = new ReefDbConfiguration(configFilename, configArgs);
        ReefDbConfiguration.setInstance(reefDbConfiguration);
    }

}
