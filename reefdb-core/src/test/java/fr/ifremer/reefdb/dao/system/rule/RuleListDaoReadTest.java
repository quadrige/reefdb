package fr.ifremer.reefdb.dao.system.rule;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.configuration.control.RuleListDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.List;

import static org.junit.Assert.*;

public class RuleListDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(RuleListDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbRuleListDao ruleListDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        ruleListDao = ReefDbServiceLocator.instance().getService("reefDbRuleListDao", ReefDbRuleListDao.class);
    }

    @Test
    public void getAllRuleList() {

        List<RuleListDTO> ruleLists = ruleListDao.getRuleLists();
        assertNotNull(ruleLists);
        assertEquals(2, ruleLists.size());
        if (log.isDebugEnabled()) {
            for (RuleListDTO dto : ruleLists) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
     
        // RULELIST1 comes from test dataset only
        RuleListDTO ruleList = ruleListDao.getRuleList(dbResource.getFixtures().getRuleListCode(0));
        assertNotNull(ruleList);
        assertEquals("rule list 1", ruleList.getDescription());
        
        try {
            ruleListDao.getRuleList("RULELISTxxx");
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }
    
}
