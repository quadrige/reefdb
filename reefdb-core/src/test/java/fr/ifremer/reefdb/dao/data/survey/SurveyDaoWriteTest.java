package fr.ifremer.reefdb.dao.data.survey;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.administration.program.ReefDbProgramDao;
import fr.ifremer.reefdb.dao.administration.user.ReefDbDepartmentDao;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.referential.monitoringLocation.ReefDbMonitoringLocationDao;
import fr.ifremer.reefdb.dao.technical.Geometries;
import fr.ifremer.reefdb.dto.CoordinateDTO;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.enums.SynchronizationStatusValues;
import fr.ifremer.reefdb.dto.referential.DepthDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SurveyDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(SurveyDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbSurveyDao surveyDao;
    private ReefDbCampaignDao campaignDao;
    private ReefDbProgramDao programDao;
    private ReefDbMonitoringLocationDao mlDao;
    private ReefDbReferentialDao refDao;
    private ReefDbDepartmentDao departmentDao;

    private Integer testSurveyId;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
        campaignDao = ReefDbServiceLocator.instance().getService("reefDbCampaignDao", ReefDbCampaignDao.class);
        programDao = ReefDbServiceLocator.instance().getService("reefDbProgramDao", ReefDbProgramDao.class);
        mlDao = ReefDbServiceLocator.instance().getService("reefDbMonitoringLocationDao", ReefDbMonitoringLocationDao.class);
        refDao = ReefDbServiceLocator.instance().getService("reefDbReferentialDao", ReefDbReferentialDao.class);
        departmentDao = ReefDbServiceLocator.instance().getService("reefDbDepartmentDao", ReefDbDepartmentDao.class);
        setCommitOnTearDown(true);
    }

    @Test
    public void createUpdateSaveSurvey() {

        // create a survey bean
        SurveyDTO survey = ReefDbBeanFactory.newSurveyDTO();

        survey.setName("test survey");
        survey.setComment("test survey comment");
        survey.setDate(LocalDate.of(2015,2, 5));
        survey.setTime(1252);
        survey.setPreciseDepth(15.2);
//        survey.setControlDate(Date.valueOf("2015-02-06"));
//        survey.setValidationDate(Date.valueOf("2015-02-07"));
//        survey.setValidationComment("validation date comment");
        survey.setCampaign(campaignDao.getAllCampaigns().get(0));
        survey.setProgram(programDao.getProgramByCode("REMIS"));
        survey.setOccasion(null);
        survey.setLocation(mlDao.getLocationById(1));
        survey.setDepth(null);
        survey.setPositioning(refDao.getAllPositioningSystems().get(0));
        CoordinateDTO surveyCoordinate = ReefDbBeanFactory.newCoordinateDTO();
        surveyCoordinate.setMinLongitude(2.3488000);
        surveyCoordinate.setMinLatitude(48.8534100);
        survey.setCoordinate(surveyCoordinate);
        survey.setRecorderDepartment(departmentDao.getDepartmentById(1));
        survey.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

        SurveyDTO surveyBase = ReefDbBeans.clone(survey);

        // save
        survey = surveyDao.save(survey);

        assertNotNull(survey);
        assertNotNull(survey.getId());
        testSurveyId = survey.getId();

        // assert all values after save
        assertSurveyEquals(surveyBase, survey, false);

        // reload
        survey = getSurveyTest();
        assertNotNull(survey);

        // assert all values
        assertSurveyEquals(surveyBase, survey, false);

        // modify all values
        survey.setName("test survey 2");
        survey.setComment("test survey comment 2");
        survey.setDate(LocalDate.of(2015,1,1));
        survey.setTime(1252);
        survey.setPreciseDepth(15.2);
//        survey.setControlDate(Date.valueOf("2015-03-08"));
//        survey.setValidationDate(Date.valueOf("2015-04-16"));
//        survey.setValidationComment("validation date comment 2");
        survey.setCampaign(campaignDao.getAllCampaigns().get(1));
        survey.setProgram(programDao.getProgramByCode("REBENT"));
        survey.setOccasion(null);
        survey.setLocation(mlDao.getLocationById(2));
        DepthDTO depthValue = ReefDbBeanFactory.newDepthDTO();
        depthValue.setId(102);
        depthValue.setName("de 2 à 3");
        survey.setDepth(depthValue);
        survey.setPositioning(refDao.getAllPositioningSystems().get(1));
        surveyCoordinate.setMinLongitude(2.3488000);
        surveyCoordinate.setMinLatitude(48.8534100);
        surveyCoordinate.setMaxLongitude(2.3555000);
        surveyCoordinate.setMaxLatitude(48.8539200);
        survey.setCoordinate(surveyCoordinate);
        survey.setRecorderDepartment(departmentDao.getDepartmentById(3));
        survey.setSynchronizationStatus(SynchronizationStatusValues.DIRTY.toSynchronizationStatusDTO());

        surveyBase = ReefDbBeans.clone(survey);

        // update
        survey = surveyDao.save(survey);

        assertNotNull(survey);
        assertNotNull(survey.getId());
        assertEquals(testSurveyId, survey.getId());

        // assert all values before save
        assertSurveyEquals(surveyBase, survey, false);

        // reload
        survey = getSurveyTest();
        assertNotNull(survey);

        // assert all values
        assertSurveyEquals(surveyBase, survey, true);

        /*
         * DETAILED PART
         */
        SurveyDTO surveyDetail = surveyDao.getSurveyById(testSurveyId, false);
        assertNotNull(surveyDetail);
        assertEquals(testSurveyId, surveyDetail.getId()); // ça c'est du test ^^

        // check base fields
        assertSurveyEquals(surveyBase, surveyDetail, true);

        // set other detailed fields
        surveyDetail.setPositioningComment("positioning comment");

        // remove profondeur
        surveyDetail.setDepth(null);

        // coordinate
        surveyDetail.setCoordinate(null);
        surveyDetail.setPositioning(null);

        // clone
        SurveyDTO surveyDetailBase = ReefDbBeans.clone(surveyDetail);

        // save
        surveyDetail = surveyDao.save(surveyDetail);
        assertNotNull(surveyDetail);
        assertNotNull(surveyDetail.getId());
        assertEquals(testSurveyId, surveyDetail.getId());

        // compare to base
        assertSurveyEquals(surveyDetailBase, surveyDetail, false);

        // reload and compare
        surveyDetail = surveyDao.getSurveyById(testSurveyId, false);

        assertSurveyEquals(surveyDetailBase, surveyDetail, false);

        // modify all fields
        surveyDetail.setName("test survey 3");
        surveyDetail.setComment("test survey comment 3");
        surveyDetail.setDate(LocalDate.of(2015,4,7));
        surveyDetail.setTime(1953);
        surveyDetail.setPreciseDepth(12.90);
//        surveyDetail.setControlDate(Date.valueOf("2015-05-20"));
//        surveyDetail.setValidationDate(Date.valueOf("2015-05-21"));
//        surveyDetail.setValidationComment("validation date comment 3");
        surveyDetail.setCampaign(campaignDao.getAllCampaigns().get(2));
        surveyDetail.setProgram(programDao.getProgramByCode("RNOHYD"));
        surveyDetail.setOccasion(null);
        surveyDetail.setLocation(mlDao.getLocationById(3));
        surveyDetail.setDepth(null);
        surveyDetail.setPositioning(null);
        surveyDetail.setCoordinate(null);
        surveyDetail.setPositioningComment("positioning comment 2");

        // clone
        surveyDetailBase = ReefDbBeans.clone(surveyDetail);

        // save, reload and compare
        surveyDao.save(surveyDetail);
        surveyDetail = surveyDao.getSurveyById(testSurveyId, false);
        assertSurveyEquals(surveyDetailBase, surveyDetail, false);

        // some nulls
        surveyDetail.setControlDate(null);
        surveyDetail.setPositioningComment(null);

        // clone, save, reload and compare
        surveyDetailBase = ReefDbBeans.clone(surveyDetail);
        surveyDao.save(surveyDetail);
        surveyDetail = surveyDao.getSurveyById(testSurveyId, false);
        assertSurveyEquals(surveyDetailBase, surveyDetail, false);
        
        // delete
        surveyDao.remove(Lists.newArrayList(testSurveyId));
        
        // reload survey by loading all surveys
        assertNull(getSurveyTest());
    }

    private SurveyDTO getSurveyTest() {

        // reload survey by loading all surveys
        List<SurveyDTO> surveys = surveyDao.getSurveysByCriteria(null, Arrays.asList("REMIS","RNOHYD","REBENT"), null, null, null, null, null, null, true);
        assertNotNull(surveys);

        SurveyDTO survey = null;
        for (SurveyDTO s : surveys) {
            if (testSurveyId.equals(s.getId())) {
                survey = s;
                break;
            }
        }
        return survey;
    }

    private void assertSurveyEquals(SurveyDTO surveyBase, SurveyDTO surveyToControl, boolean exceptDepartment) {

        assertEquals(surveyBase.getName(), surveyToControl.getName());
        assertEquals(surveyBase.getComment(), surveyToControl.getComment());
        assertEquals(surveyBase.getDate(), surveyToControl.getDate());
        assertEquals(surveyBase.getTime(), surveyToControl.getTime());
        assertEquals(surveyBase.getPreciseDepth(), surveyToControl.getPreciseDepth());
        assertEquals(surveyBase.getControlDate(), surveyToControl.getControlDate());
        assertEquals(surveyBase.getValidationDate(), surveyToControl.getValidationDate());
        assertEquals(surveyBase.getValidationComment(), surveyToControl.getValidationComment());
        assertEquals(surveyBase.getCampaign(), surveyToControl.getCampaign());
        assertEquals(surveyBase.getProgram(), surveyToControl.getProgram());
        assertEquals(surveyBase.getOccasion(), surveyToControl.getOccasion());
        assertEquals(surveyBase.getLocation(), surveyToControl.getLocation());
        assertEquals(surveyBase.getDepth(), surveyToControl.getDepth());
        assertEquals(surveyBase.getPositioning(), surveyToControl.getPositioning());
        assertEquals(surveyBase.getPositioningComment(), surveyToControl.getPositioningComment());
        assertCoordinateEquals(surveyBase.getCoordinate(), surveyToControl.getCoordinate());
        assertEquals(surveyBase.getSynchronizationStatus(), surveyToControl.getSynchronizationStatus());

        if (exceptDepartment) {
            assertNotEquals(surveyBase.getRecorderDepartment(), surveyToControl.getRecorderDepartment());
        } else {
            assertEquals(surveyBase.getRecorderDepartment(), surveyToControl.getRecorderDepartment());
        }

    }

    private void assertCoordinateEquals(CoordinateDTO coordinateBase, CoordinateDTO coordinateToControl) {
        if (coordinateBase == null) {
            assertFalse(Geometries.isValid(coordinateToControl));
            return;
        } else {
            assertNotNull(coordinateToControl);
        }
        assertTrue(Geometries.equals(coordinateBase, coordinateToControl));
    }

}
