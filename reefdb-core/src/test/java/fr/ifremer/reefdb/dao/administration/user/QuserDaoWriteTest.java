package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class QuserDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(QuserDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbQuserDao quserDao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        quserDao = ReefDbServiceLocator.instance().getService("reefDbQuserDao", ReefDbQuserDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }

    @Test
    public void replaceUser() {

        Integer SOURCE_QUSER_ID = 2;
        Integer TARGET_QUSER_ID = 7;

        assertTrue(quserDao.isUserUsedInProgram(SOURCE_QUSER_ID));
        assertFalse(quserDao.isUserUsedInRules(SOURCE_QUSER_ID));
        assertTrue(quserDao.isUserUsedInData(SOURCE_QUSER_ID));
        assertTrue(quserDao.isUserUsedInValidatedData(SOURCE_QUSER_ID));

        try {
            quserDao.replaceTemporaryUserFks(SOURCE_QUSER_ID, TARGET_QUSER_ID, true);
            fail("should throw Exception");
        } catch (Exception e) {
            assertNotNull(e);
        }

        quserDao.replaceTemporaryUserFks(SOURCE_QUSER_ID, TARGET_QUSER_ID, false);

        assertTrue(quserDao.isUserUsedInProgram(SOURCE_QUSER_ID));
        assertFalse(quserDao.isUserUsedInRules(SOURCE_QUSER_ID));
        assertTrue(quserDao.isUserUsedInData(SOURCE_QUSER_ID));
        assertTrue(quserDao.isUserUsedInValidatedData(SOURCE_QUSER_ID));

        SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
        assertNotNull(unvalidatedSurvey);
        assertNull(unvalidatedSurvey.getValidationDate());
        assertFalse(unvalidatedSurvey.isObserversEmpty());
        boolean sourceNotFound = true;
        boolean targetFound = false;
        for (PersonDTO user : unvalidatedSurvey.getObservers()) {
            if (Objects.equals(user.getId(), SOURCE_QUSER_ID)) {
                sourceNotFound = false;
            } else if (Objects.equals(user.getId(), TARGET_QUSER_ID)) {
                targetFound = true;
            }
        }
        assertTrue(sourceNotFound);
        assertTrue(targetFound);

        SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
        assertNotNull(validatedSurvey);
        assertNotNull(validatedSurvey.getValidationDate());
        assertFalse(validatedSurvey.isObserversEmpty());
        boolean sourceFound = false;
        boolean targetNotFound = true;
        for (PersonDTO user : validatedSurvey.getObservers()) {
            if (Objects.equals(user.getId(), SOURCE_QUSER_ID)) {
                sourceFound = true;
            } else if (Objects.equals(user.getId(), TARGET_QUSER_ID)) {
                targetNotFound = false;
            }
        }
        assertTrue(sourceFound);
        assertTrue(targetNotFound);

    }

}
