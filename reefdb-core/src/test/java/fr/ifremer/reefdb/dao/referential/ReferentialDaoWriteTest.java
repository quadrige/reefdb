package fr.ifremer.reefdb.dao.referential;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class ReferentialDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(ReferentialDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbReferentialDao dao;
    private ReefDbAnalysisInstrumentDao analysisInstrumentDao;
    private ReefDbSamplingEquipmentDao samplingEquipmentDao;
    private ReefDbUnitDao unitDao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dao = ReefDbServiceLocator.instance().getService("reefDbReferentialDao", ReefDbReferentialDao.class);
        analysisInstrumentDao = ReefDbServiceLocator.instance().getService("reefDbAnalysisInstrumentDao", ReefDbAnalysisInstrumentDao.class);
        samplingEquipmentDao = ReefDbServiceLocator.instance().getService("reefDbSamplingEquipmentDao", ReefDbSamplingEquipmentDao.class);
        unitDao = ReefDbServiceLocator.instance().getService("reefDbUnitDao", ReefDbUnitDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }

    @Test
    public void replaceAnalysisInstrument() {

        Integer SOURCE_ANALYSIS_INSTRUMENT_ID = 1;
        Integer TARGET_ANALYSIS_INSTRUMENT_ID = 2;

        assertTrue(analysisInstrumentDao.isAnalysisInstrumentUsedInProgram(SOURCE_ANALYSIS_INSTRUMENT_ID));
        assertTrue(analysisInstrumentDao.isAnalysisInstrumentUsedInData(SOURCE_ANALYSIS_INSTRUMENT_ID));
        assertTrue(analysisInstrumentDao.isAnalysisInstrumentUsedInValidatedData(SOURCE_ANALYSIS_INSTRUMENT_ID));

        try {
            analysisInstrumentDao.replaceTemporaryAnalysisInstrument(SOURCE_ANALYSIS_INSTRUMENT_ID, TARGET_ANALYSIS_INSTRUMENT_ID, true);
            fail("should throw ConstraintViolationException");
        } catch (ConstraintViolationException e) {
            assertNotNull(e);
        }

        analysisInstrumentDao.replaceTemporaryAnalysisInstrument(SOURCE_ANALYSIS_INSTRUMENT_ID, TARGET_ANALYSIS_INSTRUMENT_ID, false);

        assertTrue(analysisInstrumentDao.isAnalysisInstrumentUsedInProgram(SOURCE_ANALYSIS_INSTRUMENT_ID));
        assertTrue(analysisInstrumentDao.isAnalysisInstrumentUsedInData(SOURCE_ANALYSIS_INSTRUMENT_ID));
        assertTrue(analysisInstrumentDao.isAnalysisInstrumentUsedInValidatedData(SOURCE_ANALYSIS_INSTRUMENT_ID));

        /* TODO : quand les AnalysisInstrument seront présent dans les mesures, continuer ce test
        SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(UNVALIDATED_SURVEY_ID); // get by fixture
        assertNull(unvalidatedSurvey.getValidationDate());
        assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
        for (SamplingOperationDTO samplingOperation: unvalidatedSurvey.getSamplingOperations()) {
            if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                for (MeasurementDTO measurement: samplingOperation.getMeasurements()) {
                    assertNotNull(measurement.getAnalysisInstrument());
                    // etc...
                }
            }
        }
        */
    }

    @Test
    public void replaceSamplingEquipment() {

        Integer SOURCE_SAMPLING_EQUIPMENT_ID = 5;
        Integer TARGET_SAMPLING_EQUIPMENT_ID = 2;

        assertTrue(samplingEquipmentDao.isSamplingEquipmentUsedInData(SOURCE_SAMPLING_EQUIPMENT_ID));
        assertTrue(samplingEquipmentDao.isSamplingEquipmentUsedInValidatedData(SOURCE_SAMPLING_EQUIPMENT_ID));

        try {
            samplingEquipmentDao.replaceTemporarySamplingEquipment(SOURCE_SAMPLING_EQUIPMENT_ID, TARGET_SAMPLING_EQUIPMENT_ID, true);
            fail("should throw ConstraintViolationException");
        } catch (ConstraintViolationException e) {
            assertNotNull(e);
        }

        samplingEquipmentDao.replaceTemporarySamplingEquipment(SOURCE_SAMPLING_EQUIPMENT_ID, TARGET_SAMPLING_EQUIPMENT_ID, false);

        assertTrue(samplingEquipmentDao.isSamplingEquipmentUsedInData(SOURCE_SAMPLING_EQUIPMENT_ID));
        assertTrue(samplingEquipmentDao.isSamplingEquipmentUsedInValidatedData(SOURCE_SAMPLING_EQUIPMENT_ID));

        SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
        assertNotNull(unvalidatedSurvey);
        assertNull(unvalidatedSurvey.getValidationDate());
        assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
        boolean targetFound = false;
        for (SamplingOperationDTO samplingOperation: unvalidatedSurvey.getSamplingOperations()) {
            assertNotNull(samplingOperation.getSamplingEquipment());
            assertNotEquals(SOURCE_SAMPLING_EQUIPMENT_ID, samplingOperation.getSamplingEquipment().getId());
            if (Objects.equals(samplingOperation.getSamplingEquipment().getId(), TARGET_SAMPLING_EQUIPMENT_ID)) {
                targetFound = true;
            }
        }
        assertTrue(targetFound);

        SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
        assertNotNull(validatedSurvey);
        assertNotNull(validatedSurvey.getValidationDate());
        assertTrue(CollectionUtils.isNotEmpty(validatedSurvey.getSamplingOperations()));
        for (SamplingOperationDTO samplingOperation: validatedSurvey.getSamplingOperations()) {
            assertNotNull(samplingOperation.getSamplingEquipment());
            assertEquals(SOURCE_SAMPLING_EQUIPMENT_ID, samplingOperation.getSamplingEquipment().getId());
        }

    }

    @Test
    public void replaceUnit() {

        Integer SOURCE_UNIT_ID = 9;
        Integer TARGET_UNIT_ID = 10;

        assertTrue(unitDao.isUnitUsedInReferential(SOURCE_UNIT_ID));
        assertTrue(unitDao.isUnitUsedInData(SOURCE_UNIT_ID));
        assertTrue(unitDao.isUnitUsedInValidatedData(SOURCE_UNIT_ID));

        try {
            unitDao.replaceTemporaryUnit(SOURCE_UNIT_ID, TARGET_UNIT_ID, true);
            fail("should throw ConstraintViolationException");
        } catch (ConstraintViolationException e) {
            assertNotNull(e);
        }

        unitDao.replaceTemporaryUnit(SOURCE_UNIT_ID, TARGET_UNIT_ID, false);

        assertFalse(unitDao.isUnitUsedInReferential(SOURCE_UNIT_ID));
        assertTrue(unitDao.isUnitUsedInData(SOURCE_UNIT_ID));
        assertTrue(unitDao.isUnitUsedInValidatedData(SOURCE_UNIT_ID));

        SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
        assertNotNull(unvalidatedSurvey);
        assertNull(unvalidatedSurvey.getValidationDate());
        assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
        boolean targetFound = false;
        for (SamplingOperationDTO samplingOperation: unvalidatedSurvey.getSamplingOperations()) {
            assertNotNull(samplingOperation.getSizeUnit());
            assertNotEquals(SOURCE_UNIT_ID, samplingOperation.getSizeUnit().getId());
            if (Objects.equals(samplingOperation.getSizeUnit().getId(), TARGET_UNIT_ID)) {
                targetFound = true;
            }
        }
        assertTrue(targetFound);

        SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
        assertNotNull(validatedSurvey);
        assertNotNull(validatedSurvey.getValidationDate());
        assertTrue(CollectionUtils.isNotEmpty(validatedSurvey.getSamplingOperations()));
        for (SamplingOperationDTO samplingOperation: validatedSurvey.getSamplingOperations()) {
            assertNotNull(samplingOperation.getSizeUnit());
            assertEquals(SOURCE_UNIT_ID, samplingOperation.getSizeUnit().getId());
        }
    }

}
