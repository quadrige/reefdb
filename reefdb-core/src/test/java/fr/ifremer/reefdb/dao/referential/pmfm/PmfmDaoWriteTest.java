package fr.ifremer.reefdb.dao.referential.pmfm;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.referential.StatusCode;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dao.referential.ReefDbUnitDao;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.PmfmDTO;
import fr.ifremer.reefdb.dto.referential.pmfm.QualitativeValueDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.*;

/**
 * Created by Ludovic on 30/07/2015.
 */
public class PmfmDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(PmfmDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbPmfmDao pmfmDao;
    private ReefDbParameterDao parameterDao;
    private ReefDbMatrixDao matrixDao;
    private ReefDbFractionDao fractionDao;
    private ReefDbMethodDao methodDao;
    private ReefDbUnitDao unitDao;
    private ReefDbQualitativeValueDao qualitativeValueDao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        pmfmDao = ReefDbServiceLocator.instance().getService("reefDbPmfmDao", ReefDbPmfmDao.class);
        parameterDao = ReefDbServiceLocator.instance().getService("reefDbParameterDao", ReefDbParameterDao.class);
        matrixDao = ReefDbServiceLocator.instance().getService("reefDbMatrixDao", ReefDbMatrixDao.class);
        fractionDao = ReefDbServiceLocator.instance().getService("reefDbFractionDao", ReefDbFractionDao.class);
        methodDao = ReefDbServiceLocator.instance().getService("reefDbMethodDao", ReefDbMethodDao.class);
        unitDao = ReefDbServiceLocator.instance().getService("reefDbUnitDao", ReefDbUnitDao.class);
        qualitativeValueDao = ReefDbServiceLocator.instance().getService("reefDbQualitativeValueDao", ReefDbQualitativeValueDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
        setCommitOnTearDown(true);
    }

    @Test
    public void createPmfm() {

        List<PmfmDTO> pmfmsToSave = Lists.newArrayList();
        List<PmfmDTO> allPmfms = Lists.newArrayList(pmfmDao.getAllPmfms(ImmutableList.of(StatusCode.LOCAL_ENABLE.getValue(), StatusCode.LOCAL_DISABLE.getValue())));

        PmfmDTO pmfm1 = ReefDbBeanFactory.newPmfmDTO();
        pmfm1.setParameter(parameterDao.getParameterByCode("AL"));
        pmfm1.setMatrix(matrixDao.getMatrixById(1));
        pmfm1.setFraction(fractionDao.getFractionById(1));
        pmfm1.setMethod(methodDao.getMethodById(1));
        pmfm1.setUnit(unitDao.getUnitById(1));

        PmfmDTO pmfm2 = ReefDbBeanFactory.newPmfmDTO();
        pmfm2.setParameter(parameterDao.getParameterByCode("BIOMZOO"));
        pmfm2.setMatrix(matrixDao.getMatrixById(2));
        pmfm2.setFraction(fractionDao.getFractionById(2));
        pmfm2.setMethod(methodDao.getMethodById(2));
        pmfm2.setUnit(unitDao.getUnitById(2));
        QualitativeValueDTO qv1 = qualitativeValueDao.getQualitativeValueById(1);
        pmfm2.addQualitativeValues(qv1);
        QualitativeValueDTO qv2 = qualitativeValueDao.getQualitativeValueById(2);
        pmfm2.addQualitativeValues(qv2);

        pmfmsToSave.add(pmfm1);
        pmfmsToSave.add(pmfm2);
        allPmfms.addAll(pmfmsToSave);

        pmfm1.setDirty(true);
        pmfm2.setDirty(true);
        pmfmDao.savePmfms(pmfmsToSave);
        List<PmfmDTO> reloadedPmfms = pmfmDao.getAllPmfms(ImmutableList.of(StatusCode.LOCAL_ENABLE.getValue(), StatusCode.LOCAL_DISABLE.getValue()));
        assertPmfmsEquals(allPmfms, reloadedPmfms);

        // remove 1 qualitative value
        pmfm2.removeQualitativeValues(qv1);
        pmfm2.setDirty(true);

        pmfmDao.savePmfms(pmfmsToSave);
        reloadedPmfms = pmfmDao.getAllPmfms(ImmutableList.of(StatusCode.LOCAL_ENABLE.getValue(), StatusCode.LOCAL_DISABLE.getValue()));
        assertPmfmsEquals(allPmfms, reloadedPmfms);

    }

    private void assertPmfmsEquals(List<PmfmDTO> pmfms1, List<PmfmDTO> pmfms2) {
        assertTrue(CollectionUtils.isNotEmpty(pmfms1));
        assertTrue(CollectionUtils.isNotEmpty(pmfms2));
        assertEquals(pmfms1.size(), pmfms2.size());

        Map<Integer, PmfmDTO> pmfms2Map = ReefDbBeans.mapById(pmfms2);
        for (PmfmDTO pmfm1 : pmfms1) {
            assertNotNull(pmfm1.getId());
            PmfmDTO pmfm2 = pmfms2Map.get(pmfm1.getId());
            assertNotNull(pmfm2);
            assertPmfmEquals(pmfm1, pmfm2);
        }
    }

    private void assertPmfmEquals(PmfmDTO pmfm1, PmfmDTO pmfm2) {
        assertFalse(pmfm1 == pmfm2);
        assertEquals(pmfm1.getId(), pmfm2.getId());
        assertEquals(pmfm1.getStatus(), pmfm2.getStatus());
        assertEquals(pmfm1.getParameter(), pmfm2.getParameter());
        assertEquals(pmfm1.getMatrix(), pmfm2.getMatrix());
        assertEquals(pmfm1.getFraction(), pmfm2.getFraction());
        assertEquals(pmfm1.getMethod(), pmfm2.getMethod());
        assertEquals(pmfm1.getUnit(), pmfm2.getUnit());
        assertQualitativeValuesEquals(pmfm1.getQualitativeValues(), pmfm2.getQualitativeValues());
    }

    private void assertQualitativeValuesEquals(Collection<QualitativeValueDTO> values1, Collection<QualitativeValueDTO> values2) {
        assertEquals(values1.size(), values2.size());

        List<Integer> values2Ids = ReefDbBeans.collectIds(values2);
        for (QualitativeValueDTO value1 : values1) {
            assertNotNull(value1.getId());
            assertTrue(values2Ids.contains(value1.getId()));
        }
    }

    @Test
    public void replacePmfm() {

        Integer SOURCE_PMFM_ID = 21;
        Integer TARGET_PMFM_ID = 22;

        assertTrue(pmfmDao.isPmfmUsedInProgram(SOURCE_PMFM_ID));
        assertTrue(pmfmDao.isPmfmUsedInData(SOURCE_PMFM_ID));
        assertTrue(pmfmDao.isPmfmUsedInValidatedData(SOURCE_PMFM_ID));

        try {
            pmfmDao.replaceTemporaryPmfm(SOURCE_PMFM_ID, TARGET_PMFM_ID, true);
            fail("should throw exception");
        } catch (Exception e) {
            assertNotNull(e);
        }

        pmfmDao.replaceTemporaryPmfm(SOURCE_PMFM_ID, TARGET_PMFM_ID, false);

        assertTrue(pmfmDao.isPmfmUsedInProgram(SOURCE_PMFM_ID));
        assertTrue(pmfmDao.isPmfmUsedInData(SOURCE_PMFM_ID));
        assertTrue(pmfmDao.isPmfmUsedInValidatedData(SOURCE_PMFM_ID));

        // load unvalidated survey
        {
            SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
            assertNotNull(unvalidatedSurvey);
            assertNull(unvalidatedSurvey.getValidationDate());

            boolean targetPmfmFound = false;
            assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
            for (SamplingOperationDTO samplingOperation : unvalidatedSurvey.getSamplingOperations()) {

                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        assertNull(measurement.getValidationDate());
                        assertNotNull(measurement.getPmfm());
                        assertNotEquals(SOURCE_PMFM_ID, measurement.getPmfm().getId());
                        if (Objects.equals(measurement.getPmfm().getId(), TARGET_PMFM_ID)) {
                            targetPmfmFound = true;
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        assertNull(measurement.getValidationDate());
                        assertNotNull(measurement.getPmfm());
                        assertNotEquals(SOURCE_PMFM_ID, measurement.getPmfm().getId());
                        if (Objects.equals(measurement.getPmfm().getId(), TARGET_PMFM_ID)) {
                            targetPmfmFound = true;
                        }
                    }
                }
            }
            assertTrue(targetPmfmFound);
        }

        // load validated survey
        {
            SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
            assertNotNull(validatedSurvey);
            assertNotNull(validatedSurvey.getValidationDate());

            boolean sourcePmfmFound = false;
            assertTrue(CollectionUtils.isNotEmpty(validatedSurvey.getSamplingOperations()));
            for (SamplingOperationDTO samplingOperation : validatedSurvey.getSamplingOperations()) {

                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        assertNotNull(measurement.getValidationDate());
                        assertNotNull(measurement.getPmfm());
                        assertNotEquals(TARGET_PMFM_ID, measurement.getPmfm().getId());
                        if (Objects.equals(measurement.getPmfm().getId(), SOURCE_PMFM_ID)) {
                            sourcePmfmFound = true;
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        assertNotNull(measurement.getValidationDate());
                        assertNotEquals(TARGET_PMFM_ID, measurement.getPmfm().getId());
                        if (Objects.equals(measurement.getPmfm().getId(), SOURCE_PMFM_ID)) {
                            sourcePmfmFound = true;
                        }
                    }
                }
            }
            assertTrue(sourcePmfmFound);
        }

    }
}
