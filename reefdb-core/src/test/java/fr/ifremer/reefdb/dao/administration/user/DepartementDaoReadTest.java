package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.DepartmentDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.List;

import static org.junit.Assert.*;

public class DepartementDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(DepartementDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbDepartmentDao departmentDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        departmentDao = ReefDbServiceLocator.instance().getService("reefDbDepartmentDao", ReefDbDepartmentDao.class);
    }

    @Test
    public void getAllDepartment() {
        List<DepartmentDTO> departments = departmentDao.getAllDepartments(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(departments);
        assertEquals(13, departments.size());

        if (log.isDebugEnabled()) {
            for (DepartmentDTO program : departments) {
                log.debug(ToStringBuilder.reflectionToString(program, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        DepartmentDTO department = departmentDao.getDepartmentById(3);
        assertNotNull(department);
        assertEquals("PDG-RBE", department.getCode());

        try {
            departmentDao.getDepartmentById(15);
            fail("should throw DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }

    }

}
