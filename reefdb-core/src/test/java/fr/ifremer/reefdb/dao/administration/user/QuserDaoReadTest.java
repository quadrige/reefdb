package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.PersonDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import fr.ifremer.reefdb.service.StatusFilter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class QuserDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(QuserDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbQuserDao quserDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        quserDao = ReefDbServiceLocator.instance().getService("reefDbQuserDao", ReefDbQuserDao.class);
    }

    @Test
    public void getAllUsers() {
        List<PersonDTO> persons = quserDao.getAllUsers(StatusFilter.ACTIVE.toStatusCodes());
        assertNotNull(persons);
        assertEquals(10, persons.size());

        if (log.isDebugEnabled()) {
            for (PersonDTO person : persons) {
                log.debug(ToStringBuilder.reflectionToString(person, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

    }

}
