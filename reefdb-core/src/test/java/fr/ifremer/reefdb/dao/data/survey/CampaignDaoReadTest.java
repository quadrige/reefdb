package fr.ifremer.reefdb.dao.data.survey;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.quadrige3.core.dao.technical.Dates;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.data.survey.CampaignDTO;
import fr.ifremer.reefdb.dto.data.survey.OccasionDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CampaignDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(CampaignDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbCampaignDao campaignDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        campaignDao = ReefDbServiceLocator.instance().getService("reefDbCampaignDao", ReefDbCampaignDao.class);
    }

    @Test
    public void getAllCampaign() {
        List<CampaignDTO> campaigns = campaignDao.getAllCampaigns();
        assertNotNull(campaigns);
        assertEquals(8, campaigns.size());
        if (log.isDebugEnabled()) {
            for (CampaignDTO dto : campaigns) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }
    }

    @Test
    public void getCampaignsByIds() {
        List<CampaignDTO> campaigns = campaignDao.getCampaignsByIds(ImmutableList.of(1));
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(1, campaigns.size());
        Assert.assertEquals(1, campaigns.get(0).getId().intValue());

        campaigns = campaignDao.getCampaignsByIds(ImmutableList.of(2,3,4));
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(3, campaigns.size());
        Assert.assertEquals(2, campaigns.get(0).getId().intValue());
        Assert.assertEquals(3, campaigns.get(1).getId().intValue());
        Assert.assertEquals(4, campaigns.get(2).getId().intValue());
    }

    @Test
    public void getCampaignsByName() {
        List<CampaignDTO> campaigns = campaignDao.getCampaignsByName("RNO-DUNKERQUE");
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(1, campaigns.size());
        Assert.assertEquals(4, campaigns.get(0).getId().intValue());
    }

    @Test
    public void getCampaignsByCriteria() {
        List<CampaignDTO> campaigns = campaignDao.getCampaignsByCriteria("GCRMN-RNMRUN", null, null, false, null, null, false, true);
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(3, campaigns.size());
        Assert.assertEquals(6, campaigns.get(0).getId().intValue());
        Assert.assertEquals(7, campaigns.get(1).getId().intValue());
        Assert.assertEquals(8, campaigns.get(2).getId().intValue());

        Date testDate = Dates.safeParseDate("01/01/2005", "dd/MM/yyyy");
        campaigns = campaignDao.getCampaignsByCriteria(null, testDate, testDate, true,
                null, null, false, true);
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(1, campaigns.size());
        Assert.assertEquals(3, campaigns.get(0).getId().intValue());

        campaigns = campaignDao.getCampaignsByCriteria(null,
                Dates.safeParseDate("01/01/2012", "dd/MM/yyyy"), Dates.safeParseDate("02/03/2012", "dd/MM/yyyy"), false,
                Dates.safeParseDate("01/05/2015", "dd/MM/yyyy"), Dates.safeParseDate("01/06/2015", "dd/MM/yyyy"), false,
                false);
        Assert.assertNotNull(campaigns);
        Assert.assertEquals(1, campaigns.size());
        Assert.assertEquals(7, campaigns.get(0).getId().intValue());

    }

    @Test
    public void getOccasion() {

        List<OccasionDTO> occasions = campaignDao.getAllOccasions();
        assertNotNull(occasions);
//        assertEquals(0, occasions.size());

        if (log.isDebugEnabled()) {
            for (OccasionDTO dto : occasions) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        // TODO may be add some occasion in dataset
    }

}
