package fr.ifremer.reefdb.dao.administration.user;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepartementDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(DepartementDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbDepartmentDao departmentDao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        departmentDao = ReefDbServiceLocator.instance().getService("reefDbDepartmentDao", ReefDbDepartmentDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }

    @Test
    public void replaceDepartment() {

        Integer SOURCE_DEP_ID = 10;
        Integer TARGET_DEP_ID = 11;

        assertTrue(departmentDao.isDepartmentUsedInProgram(SOURCE_DEP_ID));
        assertTrue(departmentDao.isDepartmentUsedInRules(SOURCE_DEP_ID));
        assertTrue(departmentDao.isDepartmentUsedInData(SOURCE_DEP_ID));
        assertTrue(departmentDao.isDepartmentUsedInValidatedData(SOURCE_DEP_ID));

        try {
            departmentDao.replaceTemporaryDepartmentFks(SOURCE_DEP_ID, TARGET_DEP_ID, true);
            fail("should throw ConstraintViolationException");
        } catch (ConstraintViolationException e) {
            assertNotNull(e);
        }

        departmentDao.replaceTemporaryDepartmentFks(SOURCE_DEP_ID, TARGET_DEP_ID, false);

        assertTrue(departmentDao.isDepartmentUsedInProgram(SOURCE_DEP_ID));
        assertTrue(departmentDao.isDepartmentUsedInRules(SOURCE_DEP_ID));
        assertTrue(departmentDao.isDepartmentUsedInData(SOURCE_DEP_ID));
        assertTrue(departmentDao.isDepartmentUsedInValidatedData(SOURCE_DEP_ID));


        // load unvalidated survey to assert new department
        SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
        assertNotNull(unvalidatedSurvey);
        assertNull(unvalidatedSurvey.getValidationDate());
        assertNotNull(unvalidatedSurvey.getRecorderDepartment());
        assertEquals(TARGET_DEP_ID, unvalidatedSurvey.getRecorderDepartment().getId());

        assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
        for (SamplingOperationDTO samplingOperation: unvalidatedSurvey.getSamplingOperations()) {
            assertNotNull(samplingOperation.getSamplingDepartment());
            assertEquals(TARGET_DEP_ID, samplingOperation.getSamplingDepartment().getId());

            if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                for (MeasurementDTO measurement: samplingOperation.getMeasurements()) {
                    assertNull(measurement.getValidationDate());
                    assertNotNull(measurement.getAnalyst());
                    assertEquals(TARGET_DEP_ID, measurement.getAnalyst().getId());
                }
            }
            if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                for (MeasurementDTO measurement: samplingOperation.getIndividualMeasurements()) {
                    assertNull(measurement.getValidationDate());
                    assertNotNull(measurement.getAnalyst());
                    assertEquals(TARGET_DEP_ID, measurement.getAnalyst().getId());
                }
            }
        }

        // load validated survey to assert new department
        SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
        assertNotNull(validatedSurvey);
        assertNotNull(validatedSurvey.getValidationDate());
        assertNotNull(validatedSurvey.getRecorderDepartment());
        assertEquals(SOURCE_DEP_ID, validatedSurvey.getRecorderDepartment().getId());

        assertTrue(CollectionUtils.isNotEmpty(validatedSurvey.getSamplingOperations()));
        for (SamplingOperationDTO samplingOperation: validatedSurvey.getSamplingOperations()) {
            assertNotNull(samplingOperation.getSamplingDepartment());
            assertEquals(SOURCE_DEP_ID, samplingOperation.getSamplingDepartment().getId());

            if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                for (MeasurementDTO measurement: samplingOperation.getMeasurements()) {
                    assertNotNull(measurement.getValidationDate());
                    assertNotNull(measurement.getAnalyst());
                    assertEquals(SOURCE_DEP_ID, measurement.getAnalyst().getId());
                }
            }
            if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                for (MeasurementDTO measurement: samplingOperation.getIndividualMeasurements()) {
                    assertNotNull(measurement.getValidationDate());
                    assertNotNull(measurement.getAnalyst());
                    assertEquals(SOURCE_DEP_ID, measurement.getAnalyst().getId());
                }
            }
        }

    }

}
