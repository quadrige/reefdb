package fr.ifremer.reefdb.dao.referential.monitoringLocation;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.*;

public class MonitoringLocationDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(MonitoringLocationDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbMonitoringLocationDao mlDao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        mlDao = ReefDbServiceLocator.instance().getService("reefDbMonitoringLocationDao", ReefDbMonitoringLocationDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }

    @Test
    public void replaceLocations() {

        Integer SOURCE_LOCATION_ID = 1;
        Integer TARGET_LOCATION_ID = 2;

        assertTrue(mlDao.isLocationUsedInProgram(SOURCE_LOCATION_ID));
        assertTrue(mlDao.isLocationUsedInData(SOURCE_LOCATION_ID));
        assertTrue(mlDao.isLocationUsedInValidatedData(SOURCE_LOCATION_ID));

        try {
            mlDao.replaceTemporaryLocation(SOURCE_LOCATION_ID, TARGET_LOCATION_ID, true);
            fail("should throw Exception");
        } catch (Exception e) {
            assertNotNull(e);
        }

        mlDao.replaceTemporaryLocation(SOURCE_LOCATION_ID, TARGET_LOCATION_ID, false);

        assertTrue(mlDao.isLocationUsedInProgram(SOURCE_LOCATION_ID));
        assertTrue(mlDao.isLocationUsedInData(SOURCE_LOCATION_ID));
        assertTrue(mlDao.isLocationUsedInValidatedData(SOURCE_LOCATION_ID));

        SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
        assertNotNull(unvalidatedSurvey);
        assertNull(unvalidatedSurvey.getValidationDate());
        assertNotNull(unvalidatedSurvey.getLocation());
        assertEquals(TARGET_LOCATION_ID, unvalidatedSurvey.getLocation().getId());

        SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
        assertNotNull(validatedSurvey);
        assertNotNull(validatedSurvey.getValidationDate());
        assertNotNull(validatedSurvey.getLocation());
        assertEquals(SOURCE_LOCATION_ID, validatedSurvey.getLocation().getId());

    }

}
