package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.service.technical.CacheService;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.referential.TaxonGroupDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.List;

import static org.junit.Assert.*;

public class TaxonGroupDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(TaxonGroupDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbTaxonGroupDao dao;
    private CacheService cacheService;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dao = ReefDbServiceLocator.instance().getService("reefDbTaxonGroupDao", ReefDbTaxonGroupDao.class);
        cacheService= ReefDbServiceLocator.instance().getCacheService();
    }

    @Test
    public void getAllTaxonGroupDTOs() {
        List<TaxonGroupDTO> TaxonGroupDTOs = dao.getAllTaxonGroups();
        assertNotNull(TaxonGroupDTOs);
        assertEquals(7, TaxonGroupDTOs.size());

        if (log.isDebugEnabled()) {
            for (TaxonGroupDTO dto : TaxonGroupDTOs) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        for (TaxonGroupDTO TaxonGroupDTO : TaxonGroupDTOs) {
            // find TaxonGroupDTO id=6
            if (TaxonGroupDTO.getId() == 6) {
                assertNotNull(TaxonGroupDTO.getTaxons());
                assertEquals(2, TaxonGroupDTO.sizeTaxons());
                // find TaxonGroupDTO id=2
            }
            else if (TaxonGroupDTO.getId() == 2) {
                assertNotNull(TaxonGroupDTO.getTaxons());
                assertEquals(0, TaxonGroupDTO.sizeTaxons());
            }
        }
    }
    
    @Test
    public void getTaxonGroupDTOById() {
        TaxonGroupDTO TaxonGroupDTO = dao.getTaxonGroupById(2);
        assertNotNull(TaxonGroupDTO);
        assertEquals("Carnivore", TaxonGroupDTO.getName());
        
        try {
            dao.getTaxonGroupById(20);
            fail("should throw a DataRetrievalFailureException");
        } catch (DataRetrievalFailureException ex) {
            assertNotNull(ex);
        }
    }

}
