package fr.ifremer.reefdb.dao.administration;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import fr.ifremer.reefdb.dao.administration.program.ProgramDaoReadTest;
import fr.ifremer.reefdb.dao.administration.strategy.StrategyDaoReadTest;
import fr.ifremer.reefdb.dao.administration.user.DepartementDaoReadTest;
import fr.ifremer.reefdb.dao.administration.user.DepartementDaoWriteTest;
import fr.ifremer.reefdb.dao.administration.user.QuserDaoReadTest;
import fr.ifremer.reefdb.dao.administration.user.QuserDaoWriteTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Ludovic
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ProgramDaoReadTest.class,
        DepartementDaoReadTest.class,
        DepartementDaoWriteTest.class,
        QuserDaoReadTest.class,
        QuserDaoWriteTest.class,
        StrategyDaoReadTest.class
})
public class AdministrationTestSuite {

}
