package fr.ifremer.reefdb.dao.referential.taxon;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.data.survey.ReefDbSurveyDao;
import fr.ifremer.reefdb.dto.data.measurement.MeasurementDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.dto.data.survey.SurveyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class TaxonGroupDaoWriteTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(TaxonGroupDaoWriteTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();

    private ReefDbTaxonGroupDao dao;
    private ReefDbSurveyDao surveyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dao = ReefDbServiceLocator.instance().getService("reefDbTaxonGroupDao", ReefDbTaxonGroupDao.class);
        surveyDao = ReefDbServiceLocator.instance().getService("reefDbSurveyDao", ReefDbSurveyDao.class);
    }

    @Test
    public void replaceTaxonGroup() {

        Integer SOURCE_TAXON_GROUP_ID = 1;
        Integer TARGET_TAXON_GROUP_ID = 2;

        assertTrue(dao.isTaxonGroupUsedInReferential(SOURCE_TAXON_GROUP_ID));
        assertTrue(dao.isTaxonGroupUsedInData(SOURCE_TAXON_GROUP_ID));
        assertTrue(dao.isTaxonGroupUsedInValidatedData(SOURCE_TAXON_GROUP_ID));

        try {
            dao.replaceTemporaryTaxonGroup(SOURCE_TAXON_GROUP_ID, TARGET_TAXON_GROUP_ID, true);
            fail("should throw exception");
        } catch (Exception e) {
            assertNotNull(e);
        }

        dao.replaceTemporaryTaxonGroup(SOURCE_TAXON_GROUP_ID, TARGET_TAXON_GROUP_ID, false);

        assertFalse(dao.isTaxonGroupUsedInReferential(SOURCE_TAXON_GROUP_ID));
        assertTrue(dao.isTaxonGroupUsedInData(SOURCE_TAXON_GROUP_ID));
        assertTrue(dao.isTaxonGroupUsedInValidatedData(SOURCE_TAXON_GROUP_ID));

        // load unvalidated survey
        {
            SurveyDTO unvalidatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getUnvalidatedSurveyId(), false);
            assertNotNull(unvalidatedSurvey);
            assertNull(unvalidatedSurvey.getValidationDate());

            boolean targetTaxonGroupFound = false;
            assertTrue(CollectionUtils.isNotEmpty(unvalidatedSurvey.getSamplingOperations()));
            for (SamplingOperationDTO samplingOperation : unvalidatedSurvey.getSamplingOperations()) {

                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        assertNull(measurement.getValidationDate());
                        if (measurement.getTaxonGroup() != null) {
                            assertNotEquals(SOURCE_TAXON_GROUP_ID, measurement.getTaxonGroup());
                            if (Objects.equals(measurement.getTaxonGroup().getId(), TARGET_TAXON_GROUP_ID)) {
                                targetTaxonGroupFound = true;
                            }
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        assertNull(measurement.getValidationDate());
                        if (measurement.getTaxonGroup() != null) {
                            assertNotEquals(SOURCE_TAXON_GROUP_ID, measurement.getTaxonGroup().getId());
                            if (Objects.equals(measurement.getTaxonGroup().getId(), TARGET_TAXON_GROUP_ID)) {
                                targetTaxonGroupFound = true;
                            }
                        }
                    }
                }
            }
            assertTrue(targetTaxonGroupFound);
        }

        // load validated survey
        {
            SurveyDTO validatedSurvey = surveyDao.getSurveyById(dbResource.getFixtures().getValidatedSurveyId(), false);
            assertNotNull(validatedSurvey);
            assertNotNull(validatedSurvey.getValidationDate());

            boolean sourceTaxonGroupFound = false;
            assertTrue(CollectionUtils.isNotEmpty(validatedSurvey.getSamplingOperations()));
            for (SamplingOperationDTO samplingOperation : validatedSurvey.getSamplingOperations()) {

                if (CollectionUtils.isNotEmpty(samplingOperation.getMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getMeasurements()) {
                        assertNotNull(measurement.getValidationDate());
                        if (measurement.getTaxonGroup() != null) {
                            assertNotEquals(TARGET_TAXON_GROUP_ID, measurement.getTaxonGroup().getId());
                            if (Objects.equals(measurement.getTaxonGroup().getId(), SOURCE_TAXON_GROUP_ID)) {
                                sourceTaxonGroupFound = true;
                            }
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(samplingOperation.getIndividualMeasurements())) {
                    for (MeasurementDTO measurement : samplingOperation.getIndividualMeasurements()) {
                        assertNotNull(measurement.getValidationDate());
                        if (measurement.getTaxonGroup() != null) {
                            assertNotEquals(TARGET_TAXON_GROUP_ID, measurement.getTaxonGroup().getId());
                            if (Objects.equals(measurement.getTaxonGroup().getId(), SOURCE_TAXON_GROUP_ID)) {
                                sourceTaxonGroupFound = true;
                            }
                        }
                    }
                }
            }
            assertTrue(sourceTaxonGroupFound);
        }

    }

}
