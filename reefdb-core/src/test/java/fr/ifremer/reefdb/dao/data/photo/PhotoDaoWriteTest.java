package fr.ifremer.reefdb.dao.data.photo;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.dao.technical.Files;
import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dao.referential.ReefDbReferentialDao;
import fr.ifremer.reefdb.dao.technical.Daos;
import fr.ifremer.reefdb.dto.ReefDbBeanFactory;
import fr.ifremer.reefdb.dto.ReefDbBeans;
import fr.ifremer.reefdb.dto.data.photo.PhotoDTO;
import fr.ifremer.reefdb.dto.data.sampling.SamplingOperationDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class PhotoDaoWriteTest extends AbstractDaoTest {
    
//    private static final Log log = LogFactory.getLog(PhotoDaoWriteTest.class);
    
    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.writeDb();
    
    private ReefDbConfiguration config;
    private ReefDbPhotoDao photoDao;
    private ReefDbReferentialDao referentialDao;
    private static final int SURVEY_ID = 103;
    private static final int SAMPLING_OPERATION_1_ID = 101;
    private static final int SAMPLING_OPERATION_2_ID = 102;
    
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        config = ReefDbConfiguration.getInstance();
        photoDao = ReefDbServiceLocator.instance().getService("reefDbPhotoDao", ReefDbPhotoDao.class);
        referentialDao = ReefDbServiceLocator.instance().getService("reefDbReferentialDao", ReefDbReferentialDao.class);
        setCommitOnTearDown(true);
    }
    
    @Test
    public void createPhoto() throws IOException, URISyntaxException {
        
        Path sourcePhoto1 = getPhoto("/photo/poisson1.jpg");
        Path sourcePhoto2 = getPhoto("/photo/poisson2.jpg");
        Path sourcePhoto3 = getPhoto("/photo/poisson3.jpg");

        Path testPhoto1 = createTestPhoto(sourcePhoto1);
        Path testPhoto2 = createTestPhoto(sourcePhoto2);
        Path testPhoto3 = createTestPhoto(sourcePhoto3);
        assertNotNull(testPhoto1);
        assertNotNull(testPhoto2);
        assertNotNull(testPhoto3);
        
        PhotoDTO photo1 = ReefDbBeanFactory.newPhotoDTO();
        photo1.setDirty(true);
        photo1.setDate(new Date());
        photo1.setDirection("NW");
        photo1.setCaption("legende 1");
        photo1.setName("photo 1");
        photo1.setPhotoType(referentialDao.getPhotoTypeByCode("PAN"));
        photo1.setSamplingOperation(null); // link by survey
        photo1.setFullPath(testPhoto1.toString());
        PhotoDTO photo2 = ReefDbBeanFactory.newPhotoDTO();
        photo2.setDirty(true);
        photo2.setDate(new Date());
        photo2.setDirection("NE");
        photo2.setCaption("legende 2");
        photo2.setName("photo 2");
        photo2.setPhotoType(referentialDao.getPhotoTypeByCode("SOL"));
        SamplingOperationDTO prel1 = ReefDbBeanFactory.newSamplingOperationDTO();
        prel1.setId(SAMPLING_OPERATION_1_ID);
        photo2.setSamplingOperation(prel1);
        photo2.setFullPath(testPhoto2.toString());
        PhotoDTO photo3 = ReefDbBeanFactory.newPhotoDTO();
        photo3.setDirty(true);
        photo3.setDate(new Date());
        photo3.setDirection("SE");
        photo3.setCaption("legende 3");
        photo3.setName("photo 3");
        photo3.setPhotoType(referentialDao.getPhotoTypeByCode("MACRO"));
        SamplingOperationDTO prel2 = ReefDbBeanFactory.newSamplingOperationDTO();
        prel2.setId(SAMPLING_OPERATION_2_ID);
        photo3.setSamplingOperation(prel2);
        photo3.setFullPath(testPhoto3.toString());
        
        List<PhotoDTO> photos = Lists.newArrayList(photo1,photo2,photo3);
        
        // save
        photoDao.savePhotosBySurveyId(SURVEY_ID, photos);
        assertNotNull(photo1.getId());
        assertNotNull(photo2.getId());
        assertNotNull(photo3.getId());
        
        // test generated file path
        String filePath1 = Daos.computePhotoFilePath(Daos.SURVEY_OBJECT_TYPE, SURVEY_ID, photo1.getId(), "jpg");
        String filePath2 = Daos.computePhotoFilePath(Daos.SAMPLING_OPERATION_OBJECT_TYPE, SAMPLING_OPERATION_1_ID, photo2.getId(), "jpg");
        String filePath3 = Daos.computePhotoFilePath(Daos.SAMPLING_OPERATION_OBJECT_TYPE, SAMPLING_OPERATION_2_ID, photo3.getId(), "jpg");
        assertNotNull(filePath1);
        assertNotNull(filePath2);
        assertNotNull(filePath3);
        assertEquals(filePath1, photo1.getPath());
        assertEquals(filePath2, photo2.getPath());
        assertEquals(filePath3, photo3.getPath());
        
        // reload photos
        photos = photoDao.getPhotosBySurveyId(SURVEY_ID);
        assertNotNull(photos);
        assertEquals(3, photos.size());

        // compare photo
        Map<Integer, PhotoDTO> map = ReefDbBeans.mapById(photos);
        assertPhotoEquals(photo1, map.get(photo1.getId()));
        assertPhotoEquals(photo2, map.get(photo2.getId()));
        assertPhotoEquals(photo3, map.get(photo3.getId()));
        
        // compare saved photo file to original files
        Path persistedPhotoFile1 = config.getDbPhotoDirectory().toPath().resolve(filePath1);
        Path persistedPhotoFile2 = config.getDbPhotoDirectory().toPath().resolve(filePath2);
        Path persistedPhotoFile3 = config.getDbPhotoDirectory().toPath().resolve(filePath3);
        assertFileEquals(sourcePhoto1, persistedPhotoFile1);
        assertFileEquals(sourcePhoto2, persistedPhotoFile2);
        assertFileEquals(sourcePhoto3, persistedPhotoFile3);
        
        // delete photo of sampling operation
        photoDao.removeBySamplingOperationId(SAMPLING_OPERATION_1_ID);
        
        // reload and check size
        photos = photoDao.getPhotosBySurveyId(SURVEY_ID);
        assertNotNull(photos);
        assertEquals(2, photos.size());
        // check photo deletation
        assertFalse(java.nio.file.Files.exists(persistedPhotoFile2));
        // assert other files still exists
        assertTrue(java.nio.file.Files.exists(persistedPhotoFile1));
        assertTrue(java.nio.file.Files.exists(persistedPhotoFile3));
    }

    private Path getPhoto(String source) throws URISyntaxException {
        return Paths.get(getClass().getResource(source).toURI());
    }

    private Path createTestPhoto(Path photo) throws IOException {
        Path testFile = config.getTempDirectory().toPath().resolve(photo.getFileName().toString());
        Files.copyFile(photo, testFile);
        return testFile;
    }

    private void assertPhotoEquals(PhotoDTO expectedPhoto, PhotoDTO photoToTest) {

        assertNotSame(expectedPhoto, photoToTest);
        
        assertEquals(expectedPhoto.getId(), photoToTest.getId());
        assertEquals(expectedPhoto.getName(), photoToTest.getName());
        assertEquals(expectedPhoto.getPhotoType(), photoToTest.getPhotoType());
        assertEquals(expectedPhoto.getCaption(), photoToTest.getCaption());
        assertEquals(expectedPhoto.getDate(), photoToTest.getDate());
        assertEquals(expectedPhoto.getDirection(), photoToTest.getDirection());
        assertEquals(expectedPhoto.getPath(), photoToTest.getPath());
        assertEquals(expectedPhoto.getSamplingOperation(), photoToTest.getSamplingOperation());
        
    }
    
    private void assertFileEquals(Path expectedFile, Path fileToTest) {
        
        try {
            assertTrue(FileUtils.contentEquals(expectedFile.toFile(), fileToTest.toFile()));
        } catch (IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        
    }
    
    @Test
    public void deleteAllPhotos() {
        photoDao.removeBySurveyId(SURVEY_ID);
    }
}
