package fr.ifremer.reefdb.dao.administration.strategy;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.quadrige3.core.test.AbstractDaoTest;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.dto.configuration.programStrategy.AppliedStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.PmfmStrategyDTO;
import fr.ifremer.reefdb.dto.configuration.programStrategy.StrategyDTO;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

/**
 *
 * @author Ludovic
 */
public class StrategyDaoReadTest extends AbstractDaoTest {

    private static final Log log = LogFactory.getLog(StrategyDaoReadTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    private ReefDbStrategyDao strategyDao;

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        strategyDao = ReefDbServiceLocator.instance().getService("reefDbStrategyDao", ReefDbStrategyDao.class);
    }

    @Test
    public void getStrategiesByProgramCode() {

        if (log.isDebugEnabled()) {
            log.debug("getStrategiesByProgramCode");
        }

        List<StrategyDTO> strategies = strategyDao.getStrategiesByProgramCode("REMIS");
        assertNotNull(strategies);
        assertEquals(2, strategies.size());

        if (log.isDebugEnabled()) {
            for (StrategyDTO dto : strategies) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        // TODO inspect deeply
        strategies = strategyDao.getStrategiesByProgramCode("REBENT");
        assertNotNull(strategies);
        assertEquals(0, strategies.size());

    }

    @Test
    public void getAppliedStrategiesByProgramCode() {

        if (log.isDebugEnabled()) {
            log.debug("getAppliedStrategiesByProgramCode");
        }

        List<AppliedStrategyDTO> locations = strategyDao.getAppliedStrategiesByProgramCode("REMIS");
        assertNotNull(locations);
        assertEquals(3, locations.size());

        if (log.isDebugEnabled()) {
            for (AppliedStrategyDTO dto : locations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        int nbLocation1 = 0;
        int nbLocation2 = 0;
        for (AppliedStrategyDTO location : locations) {
            if (location.getId() == 1) {
                nbLocation1++;
            }
            else if (location.getId() == 2) {
                nbLocation2++;
            }
        }
        assertEquals(2, nbLocation1);
        assertEquals(1, nbLocation2);
    }

    @Test
    public void getAppliedStrategiesByStrategyId() {

        if (log.isDebugEnabled()) {
            log.debug("getAppliedStrategiesByStrategyId");
        }

        List<AppliedStrategyDTO> locations = strategyDao.getAppliedStrategiesByStrategyId(3);
        assertNotNull(locations);
        assertEquals(1, locations.size());

        if (log.isDebugEnabled()) {
            for (AppliedStrategyDTO dto : locations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        assertEquals((Integer) 1, locations.get(0).getId());
        assertNull(locations.get(0).getStartDate());
        assertNull(locations.get(0).getEndDate());
        assertNotNull(locations.get(0).getDepartment());
        assertEquals((Integer) 5, locations.get(0).getDepartment().getId());
    }

    @Test
    public void getAppliedPeriodsByStrategyId() {

        if (log.isDebugEnabled()) {
            log.debug("getAppliedPeriodsByStrategyId");
        }

        List<AppliedStrategyDTO> locations = strategyDao.getAppliedPeriodsByStrategyId(3);
        assertNotNull(locations);
        assertEquals(1, locations.size());

        if (log.isDebugEnabled()) {
            for (AppliedStrategyDTO dto : locations) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        assertEquals((Integer) 1, locations.get(0).getId());
        assertEquals(LocalDate.of(2004,9,1), locations.get(0).getStartDate());
        assertEquals(LocalDate.of(2010,7,1), locations.get(0).getEndDate());
        assertNotNull(locations.get(0).getDepartment());
        assertEquals((Integer) 5, locations.get(0).getDepartment().getId());
    }

    @Test
    public void getPmfmsByStrategy() {

        if (log.isDebugEnabled()) {
            log.debug("getPmfmsByStrategy");
        }

        List<PmfmStrategyDTO> pmfms = strategyDao.getPmfmsAppliedStrategy(1);
        assertNotNull(pmfms);
        assertEquals(4, pmfms.size());

        if (log.isDebugEnabled()) {
            for (PmfmStrategyDTO dto : pmfms) {
                log.debug(ToStringBuilder.reflectionToString(dto, ToStringStyle.SHORT_PREFIX_STYLE));
            }
        }

        PmfmStrategyDTO pmfm = null;
        for (PmfmStrategyDTO dto: pmfms) {
            if (dto.getId() == 1) {
                pmfm = dto;
                break;
            }
        }
        assertNotNull(pmfm);
        
        // TODO test booleans
        pmfm.isSurvey();
        pmfm.isSampling();
        pmfm.isGrouping();
        pmfm.isUnique();

        //TODO remplacer ?      
//        assertNotNull(pmfm.getParemetre());
//        assertEquals("ACEPHTE", pmfm.getParemetre().getCode());
//        assertNotNull(pmfm.getSupport());
//        assertEquals((Integer) 1, pmfm.getSupport().getId());
//        assertNotNull(pmfm.getFraction());
//        assertEquals((Integer) 1, pmfm.getFraction().getId());
//        assertNotNull(pmfm.getMethode());
//        assertEquals((Integer) 1, pmfm.getMethode().getId());

        // TODO add unit
//        assertNotNull(pmfm.getUnite());
//        assertEquals((Integer) 9, pmfm.getUnite().getId()); 
        
    }

}
