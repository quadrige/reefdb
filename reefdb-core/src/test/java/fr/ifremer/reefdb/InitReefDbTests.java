package fr.ifremer.reefdb;

/*-
 * #%L
 * Reef DB :: Core
 * %%
 * Copyright (C) 2014 - 2018 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.quadrige3.core.config.QuadrigeCoreConfigurationOption;
import fr.ifremer.quadrige3.core.test.InitTests;
import fr.ifremer.reefdb.config.ReefDbConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import fr.ifremer.reefdb.service.ReefDbServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.List;

/**
 * @author peck7 on 02/01/2018.
 */
public class InitReefDbTests extends InitTests {

    private static final Log LOG = LogFactory.getLog(InitReefDbTests.class);

    public static void main(String[] args) {
        InitReefDbTests initReefDbTests = new InitReefDbTests();
        try {
            initReefDbTests.before();
        } catch (Throwable t) {
            LOG.error(t.getLocalizedMessage(), t);
        }
    }

    public InitReefDbTests() {
        super();
        setTargetDbDirectory(ReefDbDatabaseResource.HSQLDB_SRC_DATABASE_DIRECTORY);
    }

    @Override
    protected void initServiceLocator() {
        ReefDbServiceLocator.initReefDbDefault();
    }

    @Override
    protected String getModuleName() {
        return ReefDbDatabaseResource.MODULE_NAME;
    }

    @Override
    protected String getDbEnumerationResource() {
        return "classpath*:quadrige3-db-enumerations.properties,classpath*:reefdb-db-test-enumerations.properties";
    }

    @Override
    protected String[] getConfigArgs() {
        List<String> args = Lists.newArrayList();
        args.addAll(Arrays.asList(super.getConfigArgs()));
        args.addAll(ImmutableList.of("--option", QuadrigeCoreConfigurationOption.AUTHENTICATION_DISABLED.getKey(), Boolean.toString(true)));

        return args.toArray(new String[args.size()]);
    }

    @Override
    protected QuadrigeConfiguration createConfig() {
        ReefDbConfiguration reefDbConfiguration = new ReefDbConfiguration(getModuleName() + "-test-write.properties", getConfigArgs());
        ReefDbConfiguration.setInstance(reefDbConfiguration);

        return QuadrigeConfiguration.getInstance();

    }
}
