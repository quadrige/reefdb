package fr.ifremer.reefdb;

/*
 * #%L
 * Reef DB :: Core
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import fr.ifremer.common.synchro.dao.Daos;
import fr.ifremer.quadrige3.core.config.QuadrigeConfiguration;
import fr.ifremer.reefdb.dao.ReefDbDatabaseResource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.DatabaseSequenceFilter;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * Use this test to export configured database in XML format
 *
 * @author Ludovic
 */
public class ExportDbTest {

    private static final Log log = LogFactory.getLog(ExportDbTest.class);

    @ClassRule
    public static final ReefDbDatabaseResource dbResource = ReefDbDatabaseResource.readDb();

    @Test
    public void fullDatabaseExport() {
        File file = new File("target", "dbDump" + System.currentTimeMillis() + ".xml");
        log.info("Exporting DB data... [" + file.getAbsolutePath() + "]");

        try {
            file.createNewFile();
            Connection jdbcConnection = Daos.createConnection(QuadrigeConfiguration.getInstance().getConnectionProperties());
            IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
            connection.getConfig().setProperty("http://www.dbunit.org/properties/datatypeFactory", new HsqldbDataTypeFactory());
            ITableFilter filter = new DatabaseSequenceFilter(connection);
            IDataSet dataset = new FilteredDataSet(filter, connection.createDataSet());
            FlatXmlDataSet.write(dataset, new FileOutputStream(file));
        } catch (DatabaseUnitException | IOException | SQLException ex) {
            log.error(ex.getLocalizedMessage(), ex);
            Assert.fail("Export Failed");
        }

        log.info("DB data successfully exported to [" + file.getAbsolutePath() + "]");
    }

}
