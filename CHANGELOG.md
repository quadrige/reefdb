## Sprint 118 - v4.2.2

- Le server de synchronisation à utiliser est la version **3.8.14**


## Sprint 117 - v4.2.1

- Le server de synchronisation à utiliser est la version **3.8.14**


## Sprint 116 - v4.2.0

- Le server de synchronisation à utiliser est la version **3.8.13**


## Sprint 115 - v4.1.1

- Le server de synchronisation à utiliser est la version **3.8.12**


## Sprint 114 - v4.1.0

- Renommage des options d'initialisation de la grille de saisie des mesures sur individus sur un réplicat:

  - reefdb.pmfm.id.pitTransition => reefdb.pmfm.id**s**.pitTransition 
  - reefdb.pmfm.id.pitTransectLength => reefdb.pmfm.id**s**.pitTransectLength 
  - reefdb.pmfm.id.pitOrigin => reefdb.pmfm.id**s**.pitOrigin


- Le server de synchronisation à utiliser est la version **3.8.11**


## Sprint 113 - v4.0.1

- Le server de synchronisation à utiliser est la version **3.8.10**


## Sprint 112 - v3.12.6

- Le server de synchronisation à utiliser est la version **3.8.7**


## Sprint 111 - v3.12.5

- Le server de synchronisation à utiliser est la version **3.8.6**


## Sprint 110 - v3.12.4

- Le server de synchronisation à utiliser est la version **3.8.5**


## Sprint 109 - v3.12.3

- Le server de synchronisation à utiliser est la version **3.8.4**


## Sprint 108 - v3.12.2

- Le server de synchronisation à utiliser est la version **3.8.3**


## Sprint 107 - v3.12.1

- Le server de synchronisation à utiliser est la version **3.8.2**


## Sprint 105 - v3.12.0

- Le server de synchronisation à utiliser est la version **3.8.0**


## Sprint 104 - v3.11.9

- Le server de synchronisation à utiliser est la version **3.8.0**


## Sprint 103 - v3.11.8

- Le server de synchronisation à utiliser est la version **3.7.17**


## Sprint 102 - v3.11.7

- Le server de synchronisation à utiliser est la version **3.7.16**


## Sprint 101 - v3.11.6

- Le server de synchronisation à utiliser est la version **3.7.15**


## Sprint 100 - v3.11.5

- Le server de synchronisation à utiliser est la version **3.7.14**


## Sprint 99 - v3.11.4

- Le server de synchronisation à utiliser est la version **3.7.13**
- Mise à jour du modèle de base de données en version **3.4.0** à effectuer par Quadmire


## Sprint 98 - v3.11.3

- Le server de synchronisation à utiliser est la version **3.7.12**


## Sprint 97 - v3.11.2

- Le server de synchronisation à utiliser est la version **3.7.10**
- Mise à jour du modèle de base de données en version **3.3.7**
- Si l'importation des référentiels ne récupère pas tous les moratoires :
  > supprimer le fichier `version.appup` se trouvant dans le répertoire `data/db` de votre installation, puis importer les référentiels depuis le serveur.


## Sprint 96 - v3.11.1

- Le server de synchronisation à utiliser est la version **3.7.9**
- Mise à jour du modèle de base de données en version **3.3.6**


## Sprint 95 - v3.11.0

- Le server de synchronisation à utiliser est la version **3.7.8**


## Sprint 94 - v3.10.2

- Le server de synchronisation à utiliser est la version **3.7.7**


## Sprint 93 - v3.10.1

- Le server de synchronisation à utiliser est la version **3.7.7**


## Sprint 92 - v3.10.0

- Le server de synchronisation à utiliser est la version **3.7.6**


## Sprint 91 - v3.9.9

- Pas de mise à jour de modèle


## Sprint 90 - v3.9.8

- Pas de mise à jour de modèle


## Sprint 89 - v3.9.7

- Pas de mise à jour de modèle


## Sprint 88 - v3.9.5 & v3.9.6

- Pas de mise à jour de modèle


## Sprint 87 - v3.9.4

- Pas de mise à jour de modèle


## Sprint 86 - v3.9.3

- Pas de mise à jour de modèle


## Sprint 85 - v3.8.3

- Pas de mise à jour de modèle

- Version corrective devant utiliser le server de synchronisation 3.6.3.2 uniquement


## Sprint 84 - v3.8.2

- Pas de mise à jour de modèle

- Version corrective devant utiliser le server de synchronisation 3.6.3.1 uniquement


## Sprint 83 - v3.9.2

- Pas de mise à jour de modèle

- Une nouvelle option a été ajoutée pour gérer les transcodages des référentiels suivant :

        reefdb.transcribingItemType.label.pmfmExtraction
        
    Sa valeur par défaut est `REEFDB-PMFM.PMFM_EXTRACTION`. Couplée avec l'option `reefdb.transcribingItemType.label.pmfmNm`, elle permet de transcoder les libellé des PSFMU lors des extractions avec la priorité suivante:

    1- libellé transcodé pour extraction   
    2- libellé transcodé pour écran de saisie    
    3- libellé interne du psfmu (issu du transcodage pour écran de saisie)    
    4- libellé du paramètre
    
- Pour simplifier la configuration du serveur de synchronisation, l'option `quadrige3.synchro.import.referential.transcribingItemType.label.includes` 
    qui doit contenir tous les libellés des types de transcodage à importer, accepte désormais le caractère '%'.
        
        quadrige3.synchro.import.referential.transcribingItemType.label.includes=REEFDB% (tous les types de transcodage commençant par REEFDB seront importés)


## Sprint 82 - v3.9.1

- Pas de mise à jour de modèle


## Sprint 81 - v3.9.0

- Pas de mise à jour de modèle


## Sprint 80 - v3.8.1

- Pas de mise à jour de modèle

- La configuration du server de synchronisation (>= 3.6.3) doit **impérativement** avoir les deux options suivantes :
  
          quadrige3.persistence.db.script=classpath:fr/ifremer/quadrige3/core/db/changelog/hsqldb/quadrige3.script
          quadrige3.persistence.liquibase.changelog.path=classpath:fr/ifremer/quadrige3/core/db/changelog/hsqldb/db-changelog-master.xml


## Sprint 79 - v3.8.0

- Une mise à jour de la base de données cliente HsqlDb s'exécutera au démarrage de l'application


## Sprint 78 - v3.7.5

- Pas de mise à jour de modèle

- Ajout d'un module séparé `reefdb-converter` pour convertir les fichiers de contextes et filtres provenant d'une version ancienne de ReefDb (ex: 3.6.2) pour les rendre compatibles avec la version 3.7.5


## Sprint 77 - v3.7.4

- Aucune mise à jour de base de données


## Sprint 76 - v3.7.3

- Pas de mise à jour de modèle

- Le répertoire OLD contenant les anciennes versions de l'application est supprimé.
    Si des fichiers autres que ceux de l'historique sont présents dans ce répertoire, ils seront supprimés également !


## Sprint 75 - v3.7.2

- Aucune mise à jour de base de données


## Sprint 74 - v3.7.1

- Aucune mise à jour de base de données


## Sprint 73 - v3.7.0

- Aucune mise à jour de base de données


## Sprint 72 - v3.6.2

- Aucune mise à jour de base de données


## Sprint 71 - v3.6.1

- Aucune mise à jour de base de données


## Sprint 70 - v3.6.0

- Aucune mise à jour de base de données


## Sprint 69 - v3.5.3

- Aucune mise à jour de base de données


## Sprint 68 - v3.5.2

- Aucune mise à jour de base de données


## Sprint 67 - v3.5.1

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    - Exception à l'exportation des données vers le système central (Mantis #46165)

    Pour cela télécharger le module quadrige3-batches-server version 3.2.3 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 67 - v3.5.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle :
    - Adaptations nécessaires aux évolutions du modèle pour les PSFMU (Mantis #45005)

    Pour cela télécharger le module quadrige3-batches-server version 3.2.2 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 66 - v3.4.3

- Aucune mise à jour de base de données


## Sprint 65 - v3.4.2

- Aucune mise à jour de base de données


## Sprint 64 - v3.4.1

- Aucune mise à jour de base de données


## Sprint 63 - v3.4.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #45048)
      
    Pour cela télécharger le module quadrige3-batches-server version 3.1.20 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- La mise à jour de la base de données cliente HsqlDb s'exécutera au démarrage de l'application


## Sprint 62 - v3.3.4

- Aucune mise à jour de base de données


## Sprint 61 - v3.3.3

- Aucune mise à jour de base de données


## Sprint 60 - v3.3.2

- Aucune mise à jour de base de données


## Sprint 59 - v3.3.1

- Aucune mise à jour de base de données


## Sprint 58 - v3.3.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #42327)

    Pour cela télécharger le module quadrige3-batches-server version 3.1.13 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- La mise à jour de la base de données cliente HsqlDb s'exécutera au démarrage de l'application

    /!\ La nouvelle fonctionnalité de gestion des listes de règles de contrôle nationales va SUPPRIMER les règles locales existantes.
    Veuillez conserver la sauvegarde de votre base de données lors de la phase de migration.

    /!\ L'option de configuration : 'quadrige3.synchro.import.tables.rules.enable' doit maintenant être activée sur le serveur de synchro et l'application
    
        quadrige3.synchro.import.tables.rules.enable=true


## Sprint 57 - v3.2.2

- Aucune mise à jour de base de données


## Sprint 56 - v3.2.0

- Aucune mise à jour de base de données


## Sprint 55 - v3.1.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #41334)

    Pour cela télécharger le module quadrige3-batches-server version 3.1.6 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- La mise à jour de la base de données cliente HsqlDb s'exécutera au démarrage de l'application


## Sprint 54 - v3.0.3

- Aucune mise à jour de base de données


## Sprint 53 - v3.0.2

- Aucune mise à jour de base de données


## Sprint 52 - v3.0.1

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #37797)
    
    Pour cela télécharger le module quadrige3-core-server version 3.1.0 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- Le processus de mise à jour automatique du client va modifier les options de configuration

- Un nouveau lanceur a été mis en place, veuillez exécuter le script update_runtime.bat lorsque vous y serez invité.


## Sprint 51 - v3.0.0

- Fusion avec le projet Quagrige3-Core : Utilisation du tronc commun utilisé par Dali

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #40033)

    Pour cela télécharger le module quadrige3-core-server version 3.0.3 et exécuter la commande :
    
        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- Renommage des options de configuration:
        
        reefdb.authentication.* -> quadrige3.authentication.*
        reefdb.synchronization.* -> quadrige3.synchronization.*
        reefdb.synchro.* -> quadrige3.synchro.*
        reefdb.update.* -> quadrige3.update.*
        reefdb.install.* -> quadrige3.install.*

- Valeurs d'option à vérifier:

        quadrige3.admin.email=assistance.bdrecif@ifremer.fr (par défaut: q2support@ifremer.fr)

- Valeurs d'option à ajouter:
        
        quadrige3.synchro.import.tables.rules.enable=false

- Attention: la base de données provenant d'une version antérieure ne sera pas ouverte.

    * Veuillez réinstaller une base vierge depuis le serveur
    * Si vous souhaitez toutefois ouvrir votre ancienne base locale, avant l'ouverture de l'application,
        renommez les fichiers dans le dossier data/db, tous les fichiers quadrige3.* en quadrige3.*


## Sprint 50 - v2.8.1

- Correction des mantis 
    
    - \#39754 (Remplacement d'un taxon)
    - \#39620 (Message à la fin d'une importation de données)

- Aucune mise à jour de base de données


## Sprint 49 - v2.7.6

- Correction des mantis fils du mantis #39307 (#39386, #39388, #39390)
- Correction des mantis 

    - \#39417 (Libellé du groupe de taxon)
    - \#39416 (Remplacer le libellé BD Récif OI par BD Récif)
    - \#39393 (Passage d'un programme national rattaché à des données du référentiel local )
    - \#39297 (Importer/Exporter les données vers un fichier > Simplification des CU)

- Une mise à jour des données de la base HSQLDB (client) est faite autmatiquement :
    Elle supprime les référence de taxon locaux sans libellés (mantis #39386)


## Sprint 48 - v2.7.5

- Correction du mantis #39038 (initialisation de la grille de saisie).
- Correction d'un problème de gestion de doublon à l'exportation des données (régression détectée par le projet Dali) cf. Mantis #39013.

- Aucune mise à jour de base de données


## Sprint 47 - v2.7.4

- Correction du mantis #38932 (écran figé après une exportation).

- Aucune mise à jour de base de données


## Sprint 46 - v2.7.3

- Correction du mantis #38852 (sur la timezone).

- Aucune mise à jour de base de données


## Sprint 45 - v2.7.2

- Aucune mise à jour de base de données


## Sprint 44 - v2.7.1

- Définition du fuseau horaire à utiliser pour les dates dans la base de données (Mantis évolution #36465)

    Nouvelle option de configuration (côté client) :

    - reefdb.persistence.db.timezone: Fuseau horaire de la base de données (par défaut: ${user.timezone} = en fonction de l'OS)
    Exemple: si la base de données est en France, il faut positionner la valeur à 'Europe/Paris'

    - Plus besoin d'utiliser l'option de configuration 'reefdb.timezone' pour contourner le bug

    - Les dates stockée en base locales sont :
        - Conservées de bout en bout dans le fuseau horaire de la base de données (cf option précédente) s'il s'agit d'une date système (ex: UPDATE_DT)
        - ou bien converties vers le fuseau de la base de données, depuis l'heure locale du poste. Pour leur affichage elles sont alors reconverties dans le fuseau horaire du poste. (ex: VALIDATION_DT, CONTROL_DT).
        - ou bien non converties (ex: l'heure du passage SURVEY_TIME)


- Aucune mise à jour de base de données


#Sprint 43 - v2.7.0

- Nouvelles options de configuration (côté client) pour gérer les tentatives de reconnexion réseau (Mantis #35441):
        
    - quadrige3.synchronization.retry.count   : Nombre de tentative de reocnnexion (par défaut: 10)
    - quadrige3.synchronization.retry.timeout : Délai (en Ms) entre chaque nouvelle tentative (par défaut: 5000ms)
    - quadrige3.admin.email : Email de l'assistance (par défaut: assistance.bdrecif@ifremer.fr)

- Aucune mise à jour de base de données


## Sprint 42 - v2.6.2

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #36232)

    Pour cela télécharger le module quadrige3-core-server version 2.5.2 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 41 - v2.6.1

- Aucune mise à jour de base de données


## Sprint 40 - v2.6

- Aucune mise à jour de base de données


## Sprint 39 - v2.5

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #34994 & #34705)

    Pour cela télécharger le module quadrige3-core-server version 2.5 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>

- Synchro: Ajout du paramètre 'quadrige3.synchro.server.timezone' dans le fichier de configuration de l'application pour fixer la zone pour l'instance Tomcat.
      Veiller à fixer ce paramètre en fonction de l'emplacement géographique du serveur en exploitation. Par exemple pour la France:
      quadrige3.synchro.server.timezone=Europe/Paris

- Nouvelle fonctionnalité d'initialisation de la grille de saisie des mesures sur individus sur un réplicat:
      Elle est activée selon les conditions décrite dans le CU 'Initialiser la grille de saisie'.
      les paramètres applicatifs lié à ces conditions sont:
        
     - reefdb.pmfm.id.pitTransition (ou quadrige3.enumeration.pmfmId.PIT_TRANSITION) : id du psfm de transition (par défaut = 13381)
     - reefdb.pmfm.id.pitTransectLength (ou quadrige3.enumeration.pmfmId.PIT_TRANSECT_LENGTH) : id du psfm de la longueur du transect (par défaut = 2322)
     - reefdb.pmfm.id.pitOrigin (ou quadrige3.enumeration.pmfmId.PIT_ORIGIN) : id du psfm de l'origine (par défaut = 2326)
     - reefdb.pitTransectLength.defaultValue : valeur par défaut de la longueur de transect en centimètre (par défaut = 6000)
     - reefdb.pitOrigin.defaultValue : valeur par défaut de l'origine en centimètre (par défaut = 0)


## Sprint 38 - v2.4

- Aucune mise à jour de base de données
- Synchro: Ajout des millisecondes et de la 'timezone' dans le format date de Gson


## Sprint 37 - v2.3

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #33435 & #33608)

    Pour cela télécharger le module quadrige3-core-server version 2.3 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 36 - v2.2

- Nouvel outil disponible sur le serveur de synchronisation (Mantis #32601)
        
     - Sur la page d'accueil du serveur de synchro
     - Cliquer sur le bouton Documentation puis sur 'Documentation pour la synchronisation des tables'


## Sprint 35 - v2.1

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #32136)

    Pour cela télécharger le module quadrige3-core-server version 2.1 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 34 - v2.0

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #32027)

    Pour cela télécharger le module quadrige3-core-server version 2.0 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 33 - v1.9

- La base de données quadrige3 (ORACLE) doit être patchée afin de répondre aux modifications de modèle (Mantis #31694)

    Pour cela télécharger le module quadrige3-core-server version 1.9 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 31 - v1.7

- Dans le cas d'installation ou de réinstallation d'une base de données, une synchronisation des référentiels est exécutée systématiquement.
    Veillez à ce que le poste de l'utilisateur soit connecté au réseau.

- La base de données quadrige3 (ORACLE) doit être patchée afin de mofifier la contrainte d'unicité sur PMFM

    Pour cela, télécharger le module quadrige3-core-server version 1.7 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 30 - v1.6

- Fragmentation de la synchronisation (phase import des données dans la base locale) :
    - Ajout d'une option de configuration : quadrige3.synchro.import.data.maxRootRowCount
    
        Permet de fixer le nombre de têtes de grappe (SURVEY) par lot à importer, par exemple 300.
        Pour désactiver le traitement par lot, mettre cette option à 0.

- Ajout d'une chaine i18n à traduire : reefdb.error.context.import.error=Le fichier que vous avez spécifié n’est pas un fichier de contexte ${reefdb.application.name}.
    (Prendre exemple de la chaine reefdb.error.filter.import.error)

- Le format du fichier i18n/reefdb-i18n.csv a été modifié pour répondre à la norme CSV : séparateur ';' et guillemets retirés


## Sprint 28 - v1.4

- La base de données quadrige3 (ORACLE) doit être patchée afin d'ajouter des triggers pour améliorer la compatibilité Q²

    Pour cela télécharger le module quadrige3-core-server version 1.4 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 27 - v1.3

- La base de données quadrige3 (ORACLE) doit être patchée afin d'ajouter les triggers génériques

    Pour cela télécharger le module quadrige3-core-server version 1.3 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 25 - v1.1

- La base de données quadrige3 (ORACLE) doit être patchée. 

    Pour cela télécharger le module quadrige3-core-server version 1.1 et exécuter la commande :

        'launch.(bat|sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 19 - v0.23

- Pas de nouveauté dans la configuration.


## Sprint 18 - v0.22

- Une nouvelle variable de configuration est à mettre à jour dans le fichier de configuration (côté client) :

    - Editer le fichier de config (côté client), en ajoutant la ligne suivante :

        quadrige3.enumeration.orderItemTypeCd.REEFDB_EXTRACTION=<code à utiliser pour le tris des stations>
        (à l'Ifremer, il faut a priori mettre la valeur 'OCEANS' - mais cela doit être confirmer)


## Sprint 17

- Les tables suivantes ont été ajoutées à la liste des tables de référentiel synchronisées:
    - EXTRACT_TABLE_TYPE
    - EXTRACT_FILE_TYPE
    - EXTRACT_GROUP_TYPE_PMFM

    Ces tables servent à la persistence des extractions dans la base de données locale.
    Il faut mettre à jour l'UPDATE_DATE des lignes de ces 3 tables afin que les clients récupèrent ces référentiels.


## Sprint 16

- La base de données quadrige3 (ORACLE) doit être patchée. 

    Pour cela télécharger le module quadrige3-core-server version 0.20 et exécuter la commande :

        'launch.(.bat|.sh) --schema-update' ajouter si besoin les options -u <user> -p <password> -db <jdbc_url>


## Sprint 15

- La propriété pour limiter les programmes a changé de place : elle est maintenant côté serveur
    (en remplacement de 'reefdb.synchronization.program.codes' côté client) :

     Editer le fichier de config (côté serveur), en ajoutant la ligne suivante :

        quadrige3.synchro.program.codes=PROG_CD1,PROG_CD2


## Sprint 14 (v0.18.1)

- Patch de la base de données Oracle avec quadrige3-core-server-0.18.1-standalone
        
        launch.bat --schema-update



## Sprint 13 (v0.17)

- Mise en place de la base d'installation générée par le serveur de synchro.

    Editer le fichier de config (côté client), en ajoutant la ligne suivante :

        reefdb.install.db.url=${reefdb.synchronization.site.url}/download/install/db.properties


## Sprint 12 (v0.16)

- Authentification: mantis 26660
    
    Si une URL d'authentification reefdb.authentication.intranet.site.url ou reefdb.authentication.extranet.site.url pointe sur le serveur de synchronisation,
    Veuillez ajouter '/service/auth' à la fin de l'URL. ex:
      
        reefdb.authentication.intranet.site.url=${reefdb.synchronization.site.url}/service/auth
        reefdb.authentication.extranet.site.url=${reefdb.synchronization.site.url}/service/auth


## Sprint 11 (v0.15)

- Transfert: Pour ajouter le filtrage par programme (mantis #26735):
    
    Editer le fichicr de config (côté client), en ajoutant la ligne suivante :

        reefdb.synchronization.program.codes=PROG_CD1,PROG_CD2


## Sprint 10 (v0.14)

- Création d'une base de données d'installation (BDD Hsqldb avec les référentiels nationaux)

    - Lancer le serveur de synchronization
    - Déclencher la génération, en appellant l'adresse web correspondante, par exemple :
        - dans un navigateur (authentification administrateur demandée): http://<URL_SERVEUR>/rest/install/newDB
        - en ligne de commande (Linux) : wget http://<LOGIN>:<PWD>@<HOST_PORT_SERVER>/rest/install/newDB