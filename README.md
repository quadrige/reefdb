Recompiler le projet
--------------------
Pour reconstuire le projet reefdb :

```
mvn clean install
```
 
ATTENTION: La toute première fois, un message vous indiquera de relancer 
que les librairies magicdraw ont été installées dans le repository maven local.
Vous devrez donc relancer la compilation une seconde fois.
   
Faire une nouvelle version mineure
----------------------------------

Cette release va déployer sur le dépot nexus les artifacts.
Elle va également générer et deployer les fichiers utiles pour la  mise à jour automatique, et les déployer sur le site distant.

IMPORTANT: Pour déployer à l'Ifremer (la configuration par défaut) il faut donc être connecté via domicile.ifremer.fr et avoir lancer la redirection "Java Secure Application Manager".

```
mvn release:prepare -Darguments="-DperformRelease -DskipTests"
```
``` 
mvn release:perform -Darguments="-DperformRelease -DperformFullRelease -DperformFullRelease64 -DskipTests" -B
```
 
Do it faster:

```
mvn release:prepare -Darguments="-DperformRelease -Dmaven.test.skip -Dmaven.javadoc.skip"
```
``` 
mvn release:perform -Darguments="-DperformRelease -DperformFullRelease64 -Dmaven.test.skip -Dmaven.javadoc.skip -Dmaven.deploy.skip=false -Dsource.skip=false" -B
```
 
Déploiement sur le repository EIS
-------------------------------------

```
mvn release:prepare -Darguments="-DperformRelease -Dmaven.test.skip -Dmaven.javadoc.skip"
```
``` 
mvn release:perform -Darguments="-DperformRelease -Dmaven.test.skip -Dmaven.javadoc.skip -Dmaven.deploy.skip=false -Peis-deploy,!deploy-bundles-to-forge"
```
 
From checkout dir:
```
mvn install -DperformRelease -DperformFullRelease -DperformFullRelease64 -Dmaven.test.skip -Dmaven.javadoc.skip -Dandromda.run.skip
```
