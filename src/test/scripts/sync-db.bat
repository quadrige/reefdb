@echo off

set M2_HOME=C:\dev\adagio\maven\maven-3.1.1
set JAVA_HOME=C:\Program Files\Java\jdk1.7.0_72
set PATH=%M2_HOME%\bin;%JAVA_HOME%\bin;%PATH%

set JAVA_OPTS=-Dquadrige3.persistence.jdbc.url=jdbc:hsqldb:hsql://localhost/quadrige3-prod -Dreefdb.persistence.enable=true
set JAVA_OPTS=%JAVA_OPTS% -XX:MaxPermSize=128M -XX:PermSize=64M -XX:MaxNewSize=32M -XX:NewSize=32M -Xms256m -Xmx512m

if %1.==install. (
	echo Generating code from UML Model...
	TITLE [quadrige3-core-client] Generating code from UML Model...
	START /wait cmd /c mvn install -q -pl *client -DskipTests
	echo Generating code from UML Model [Done]
)

rem remove test DB
RMDIR /Q /S quadrige3-core-client\src\test\db

rem generate DB
echo Generating a new HSQLDB database...
TITLE [quadrige3-core-client] Generating a new HSQLDB database...
START /wait cmd /c mvn -Psql -q -pl *client andromdapp:schema -DexecuteScripts
echo Generating a new HSQLDB database [Done]

rem Copy to server DB files
RMDIR /Q /S quadrige3-core-client\src\test\db-server
XCOPY /Y /I quadrige3-core-client\src\test\db quadrige3-core-client\src\test\db-server

rem Copy to server-prod DB files
RMDIR /Q /S quadrige3-core-client\src\test\db-server-prod
XCOPY /Y /I quadrige3-core-client\src\test\db quadrige3-core-client\src\test\db-server-prod

rem restart server DB
echo Restarting HSQLDB database server...
TITLE Restarting HSQLDB database server...
START /wait cmd /c "cd quadrige3-core-client\src\test\ & stopServer.bat & START cmd /c startServer.bat & ping 1.1.1.1 -n 1 -w 10000 > nul"
echo Restarting HSQLDB database server [Done]

rem Synchronize quadrige3
echo Synchronize HSQLDB database from Q2blavenie...
rem START /wait cmd /c mvn -pl *client test -Dtest=ReferentialSynchroServiceWriteTest#synchronizeFromOracle -Dandromda.run.skip -Dquadrige3.persistence.jdbc.url=jdbc:hsqldb:hsql://localhost/quadrige3 -Dsynchro.import.jdbc.username=Q2blavenie -Dsynchro.import.jdbc.password=Q2blavenie -Dsynchro.import.jdbc.schema=Q2blavenie > sync-db-Q2Blavenie.log
call mvn -pl *client test -q -Dtest=ReferentialSynchroServiceWriteTest#synchronizeFromOracle -Dandromda.run.skip -Dquadrige3.persistence.jdbc.url=jdbc:hsqldb:hsql://localhost/quadrige3 -Dsynchro.import.jdbc.username=Q2blavenie -Dsynchro.import.jdbc.password=Q2blavenie  -Dsynchro.import.jdbc.schema=Q2blavenie

rem Synchronize quadrige3-prod
echo Synchronize HSQLDB database from Q2_DBA...
rem START /wait cmd /c mvn -pl *client test -Dtest=ReferentialSynchroServiceWriteTest#synchronizeFromOracle -Dandromda.run.skip -Dquadrige3.persistence.jdbc.url=jdbc:hsqldb:hsql://localhost/quadrige3-prod -Dsynchro.import.jdbc.username=Q2_DBA  -Dsynchro.import.jdbc.password=Q2_DBA -Dsynchro.import.jdbc.schema=Q2_DBA  > sync-db-Q2_DBA.log
call mvn -pl *client test -q -Dtest=ReferentialSynchroServiceWriteTest#synchronizeFromOracle -Dandromda.run.skip -Dquadrige3.persistence.jdbc.url=jdbc:hsqldb:hsql://localhost/quadrige3-prod -Dsynchro.import.jdbc.username=Q2_DBA  -Dsynchro.import.jdbc.password=Q2_DBA -Dsynchro.import.jdbc.schema=Q2_DBA
echo Restarting HSQLDB database server [done]
