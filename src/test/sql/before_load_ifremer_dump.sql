ALTER TABLE filter MODIFY ( FILTER_NM varchar2(100)); -- 50
ALTER TABLE event MODIFY ( event_dc varchar2(300)); -- 255
ALTER TABLE order_item MODIFY ( order_item_nm varchar2(150)); -- 100
ALTER TABLE alrt_order_item MODIFY ( order_item_nm varchar2(150)); -- 100
ALTER TABLE method MODIFY ( method_dc varchar2(300)); -- 255
ALTER TABLE method MODIFY ( method_nm varchar2(150)); -- 100

ALTER TABLE analysis_instrument MODIFY ( anal_inst_nm varchar2(150)); -- 100
ALTER TABLE programme MODIFY ( prog_dc varchar2(300)); -- 255
ALTER TABLE parameter MODIFY ( par_dc varchar2(300)); -- 255
ALTER TABLE sandre_analysis_instrument_exp MODIFY ( sandre_anal_inst_lb varchar2(150)); -- 100
ALTER TABLE sandre_analysis_instrument_imp MODIFY ( sandre_anal_inst_lb varchar2(150)); -- 100
ALTER TABLE sandre_analyst_exp MODIFY ( sandre_analyst_lb varchar2(150)); -- 100
ALTER TABLE sandre_analyst_imp MODIFY ( sandre_analyst_lb varchar2(150)); -- 100
ALTER TABLE sandre_method_exp MODIFY ( sandre_method_lb varchar2(300)); -- 255
ALTER TABLE sandre_method_imp MODIFY ( sandre_method_lb varchar2(300)); -- 255
ALTER TABLE sandre_sampler_exp MODIFY ( sandre_sampler_lb varchar2(150)); -- 100
ALTER TABLE sandre_sampler_imp MODIFY ( sandre_sampler_lb varchar2(150)); -- 100

alter table sandre_taxon_group_exp modify(taxon_group_id null);
alter table sandre_taxon_group_imp modify(taxon_group_id null);

-- Disable all constraints
BEGIN
  FOR c IN
  (SELECT c.owner, c.table_name, c.constraint_name
   FROM user_constraints c, user_tables t
   WHERE c.table_name = t.table_name
   AND c.status = 'ENABLED'
   ORDER BY c.constraint_type DESC)
  LOOP
    dbms_utility.exec_ddl_statement('alter table "' || c.owner || '"."' || c.table_name || '" disable constraint ' || c.constraint_name);
  END LOOP;
END;
/