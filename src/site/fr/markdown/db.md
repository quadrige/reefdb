Relation entre les versions de l'applicative et de la base
==========================================================

Présentation
------------

L'application permet de faire des migrations de schémas, i.e
que lors de l'ouverture (ou import) d'une base, on vérifie la version du schéma
requit par l'applicatif.

Si la version du schéma de la base est inférieure à celle de l'applicatif, on
propose alors une migration de schéma.

Historique des versions de base
-------------------------------

Le tableau suivant résume les différents liens entre les versions :

| Version BD Récif    | Version BD Test | Version schema BD         |
|---------------------|-----------------|---------------------------|
| 0.1                 | 2015.03.06      | 2.4.0                     |
| 2.7.3               | 2015.03.27-2    | 2.4.0.42                  |

Consulter le [log complet](../../quadrige3-core/fr/versions.html)

Légende :

- *Version BD Récif* : Version de l'applicatif
- *Version BD Test* : Version de la dernière base utilisée pour les tests unitaires.
- *Version schema BD* : Version du schéma (de la base embarquée HSQLDB) compatible avec l'applicatif.

Documentation
-------------

Voici la documentation disponible, concernant la base de données embarquée :

- [Tables de la base de données](../../quadrige3-core/quadrige3-core-client/hibernate/tables/index.html) embarquée (HSQLDB);
- [Entitées Hibernate](../../quadrige3-core/quadrige3-core-client/hibernate/entities/index.html), utilisées dans le code et les requêtes Hibernate.
