Accueil
=======

![splash](../images/splash.png)

Présentation
------------

Bienvenue sur le site de l'application BD Récif Océan Indien.

**BD Récif** est un outil de saisie de données utilisé pour le suivi et la surveillance des [récifs coraliens](https://fr.wikipedia.org/wiki/R%C3%A9cif_corallien).
Il est utilisé notamment en océan indien, par l'[Ifrecor](http://www.ifrecor.com), l'[Ifremer](http://www.ifremer.fr) et le [MNHN](http://www.mnhn.fr).


Installation
------------

- Télécharger la [dernière version stable](https://www.ifremer.fr/quadrige3_resources/reefdb/download/reefdb.zip) de l'application ou [une autre version](https://forge.ifremer.fr/frs/?group_id=252).

- Après téléchargement, décompresser l'archive dans un dossier.

Note : Aucun pré-requis logiciel n'est nécessaire au lancement de BD Récif. Nous
préconisons cependant d'avoir au moins 1Go de mémoire.


Utilisation
-----------

### Lancement

- Sous windows, double-cliquez sur le fichier **reefdb.exe**.

- Sous linux, exécuter le fichier **reefdb.sh**.


### Première utilisation

Lors d'une première utilisation, l'application démarre et affiche l'écran de
gestion des bases de travail. Il suffit alors d'installer une base via l'action
**Installer**. La dernière base disponible sera alors téléchargée puis installer.

A noter que cette opération peut-être longue (temps du téléchargement), soyez
patient.

Une fois la base téléchargée puis installée, l'application est pleinement
fonctionnel.

Note: Il est aussi possible de restaurer une base de données depuis un fichier zip via l'action **Restaurer**.


### Besoin d'aide ?

Pour comprendre le fonctionnement et l'usage des écrans de l'application, consultez [le manuel utilisateur](http://wwz.ifremer.fr/quadrige3_support/BD-Recif/Manuel-utilisateur-Fiches-Techniques).

En cas de problème technique, [contactez le support](http://wwz.ifremer.fr/quadrige3_support) de la cellule d'administration de Quadrige².


Captures d'écran
----------------

- Ecran de saisie principale :

![screenshot](../images/screenshot_home.png)