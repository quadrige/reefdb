Home
====

![splash](./images/splash.png)

Presentation
------------

Welcome to the technical web site of the Reef DB application.

**Reef DB** (*BD Récif* in french) aims to manage data on [Coral reef](https://en.wikipedia.org/wiki/Coral_reef). 
It is used in particular in Indian Ocean by french institutes ([Ifrecor](http://www.ifrecor.com), [Ifremer](http://www.ifremer.fr), [MNHN](http://www.mnhn.fr)).


Installation
------------

- Download the [latest release](https://www.ifremer.fr/quadrige3_resources/reefdb/download/reefdb.zip) or [another release](https://forge.ifremer.fr/frs/?group_id=252);

- Unzip the downloaded archive in a folder.


Use
---

### Launch

- Under Window, click on file **reefdb.exe**.

- Under linux, launch the file **reefdb.sh**.


### First use

When first used, the application starts and displays the screen of
management of the bases of work. Just install a database via the action
**Install**. The last available database will then be downloaded and installed.

This operation can be long (download time): be patient.

Note: It is also possible to restore a database from a zip file via the action **Restore**.


### Need help ?

To be in the use of the screens of the application, refer to [the user manual](http://wwz.ifremer.fr/quadrige3_support/BD-Recif/Manuel-utilisateur-Fiches-Techniques) (french).

In case of a technical problem, [contact the the support](http://wwz.ifremer.fr/quadrige3_support) of the Quadrige² administration team.


Screenshot
----------

- Main entry screen:

![screenshot](./images/screenshot_home.png)