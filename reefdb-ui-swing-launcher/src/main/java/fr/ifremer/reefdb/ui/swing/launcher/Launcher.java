package fr.ifremer.reefdb.ui.swing.launcher;

/*
 * #%L
 * Reef DB :: UI Launcher
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Launcher application that do post-update logic, launch application and listen to exit code
 *
 * @author Ludovic Pecquot <ludovic.pecquot@e-is.pro>
 */
public class Launcher {

    private static final String APP_DIR = "application";
    private static final String OLD_APP_DIR = "reefdb";
    private static final String DB_DIR = "data/db";
    private static final String DB_NAME = "quadrige3";
    private static final String OLD_DB_NAME = "quadrige2";
    private static final String NEW_DIR = "NEW";
    private static final String CONFIG_DIR = "config";
    private static final String UPDATER_JAR = "updater.jar";
    private static final String UPDATER_LOGFILE = "updater.log";
    private static final String LAUNCHER_PROPERTIES = "launcher.properties";

    private static final String PROPERTY_NAME = "NAME";
    private static final String PROPERTY_JAR = "JAR";
    private static final String PROPERTY_JAVA_OPTS = "JAVA_OPTS";
    private static final String PROPERTY_LOGFILE = "LOGFILE";

    private static final int NORMAL_EXIT_CODE = 0;
    private static final int UPDATE_EXIT_CODE = 88;
    private static final int STOP_EXIT_CODE = 90;

    private static final String FULL_LAUNCH_MODE = "full";
    private static final String SILENT_LAUNCH_MODE = "silent";

    private static final String EXPORT_DB_ARG = "--export-db";
    private static final String EXPORT_DB_LOGFILE = "export-db.log";

    private static final String DEBUG_ARG = "--debug";
    private static final String DEBUG_LOGFILE = "launcher.log";
    private static final String DEBUG_JAVA_OPTS = "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000";

    private static ResourceBundle bundle;
    private Path baseDir;
    private Properties properties;

    private boolean oldAppFound;

    private boolean debugMode;

    /**
     * <p>main.</p>
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        List<String> arguments = new ArrayList<>();
        if (args != null) {
            arguments.addAll(Arrays.asList(args));
        }

        Launcher launcher = new Launcher();
        launcher.execute(arguments);
    }

    /**
     * <p>execute.</p>
     *
     * @param arguments a {@link java.util.List} object.
     */
    private void execute(List<String> arguments) {
        try {

            // Get the resource bundle
            bundle = ResourceBundle.getBundle("i18n/launcher", new UTF8Control());

            // Get base dir (from where the exe file is launched)
            baseDir = Paths.get(System.getProperty("user.dir"));

            // execute upgrade
            upgrade();

            // Read launcher properties
            properties = readLauncherProperties();

            if (arguments.contains("/?") || arguments.contains("-?") || arguments.contains("/h") || arguments.contains("-h")) {
                JOptionPane.showMessageDialog(null,
                        new JTextArea(getString("launcher.help", getTitle(), getName(), EXPORT_DB_ARG)));
                System.exit(NORMAL_EXIT_CODE);
            }

            int exitCode;

            // parse arguments
            if (arguments.contains(EXPORT_DB_ARG)) {

                // enter export db mode
                exitCode = executeApplication(SILENT_LAUNCH_MODE, arguments, EXPORT_DB_LOGFILE);
                JOptionPane.showMessageDialog(null, getString("launcher.exportDb", EXPORT_DB_LOGFILE));
            } else {

                // launch Application fully
                exitCode = launchApplication(arguments);
            }

            if (exitCode != NORMAL_EXIT_CODE) {
                // show exit code 
                throw new RuntimeException(getString("launcher.exception", exitCode));
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), getTitle(), JOptionPane.ERROR_MESSAGE);
        }
    }

    // upgrade from reefdb < 3.0.0 to application
    private void upgrade() throws IOException {

        // Patch application dir
        Path oldAppDir = baseDir.resolve(OLD_APP_DIR);
        if (Files.isDirectory(oldAppDir)) {
            // rename ./reefdb to ./application
            oldAppFound = true;
            Files.move(oldAppDir, baseDir.resolve(APP_DIR));
        }
        Path oldAppNewDir = baseDir.resolve(NEW_DIR).resolve(OLD_APP_DIR);
        if (Files.isDirectory(oldAppNewDir)) {
            // rename ./NEW/reefdb to ./NEW/application
            Files.move(oldAppNewDir, baseDir.resolve(NEW_DIR).resolve(APP_DIR));
        }

        // Patch DB name
        Path dbDir = baseDir.resolve(DB_DIR);
        if (Files.isDirectory(dbDir)) {
            // find old db name
            for (Path oldDbFile : Files.list(dbDir).filter(path -> path.getFileName().toString().startsWith(OLD_DB_NAME)).collect(Collectors.toList())) {
                String fileName = oldDbFile.getFileName().toString();
                int extensionIndex = fileName.lastIndexOf(".");
                String extension = extensionIndex != -1 ? fileName.substring(extensionIndex) : "";
                // Rename file
                Files.move(oldDbFile, oldDbFile.resolveSibling(DB_NAME + extension));
            }
        }

    }

    private String getName() {
        String name = properties != null ? properties.getProperty(PROPERTY_NAME) : null;
        return name != null ? name : "<unknown_application_name>";
    }

    private String getTitle() {
        return getString("launcher.title", getName());
    }

    private int launchApplication(List<String> arguments) throws Exception {

        int exitCode;
        boolean relaunch;
        String logFile = null;
        do {
            // call update process
            exitCode = executeUpdate();

            // if updater has detected runtime update, stop now
            if (exitCode == STOP_EXIT_CODE) {
                System.exit(STOP_EXIT_CODE);
            }

            if (exitCode != NORMAL_EXIT_CODE) {
                // show exit code 
                throw new RuntimeException(getString("launcher.updaterError", getName(), UPDATER_LOGFILE));
            }

            // parse arguments
            debugMode = arguments.remove(DEBUG_ARG);
            if (debugMode) {
                logFile = DEBUG_LOGFILE;
            }

            // launch application
            exitCode = executeApplication(FULL_LAUNCH_MODE, arguments, logFile);

            relaunch = exitCode == UPDATE_EXIT_CODE;

        } while (relaunch);

        return exitCode;

    }

    private int executeUpdate() throws Exception {

        Path appUpdaterPath = baseDir.resolve(NEW_DIR).resolve(APP_DIR).resolve(UPDATER_JAR);
        // use the updater from new version if exists
        Path updaterPath = Files.exists(appUpdaterPath) ? appUpdaterPath : baseDir.resolve(APP_DIR).resolve(UPDATER_JAR);
        Path appLauncherPropertiesPath = updaterPath.resolveSibling(LAUNCHER_PROPERTIES);
        if (Files.exists(appLauncherPropertiesPath))
            // use also the new properties
            Files.copy(appLauncherPropertiesPath, baseDir.resolve(LAUNCHER_PROPERTIES));

        // copy updater to base dir
        Path updaterExecPath = baseDir.resolve(UPDATER_JAR);
        Files.copy(updaterPath, updaterExecPath, StandardCopyOption.REPLACE_EXISTING);

        List<String> launchArgs = new ArrayList<>();
        launchArgs.add(getJavaExecutable().toString());
        launchArgs.add("-jar");
        launchArgs.add(updaterExecPath.toString());

        ProcessBuilder pb = new ProcessBuilder(launchArgs);

        File updaterLogFile = new File(baseDir.toFile(), UPDATER_LOGFILE);
        pb.redirectOutput(ProcessBuilder.Redirect.to(updaterLogFile));
        pb.redirectError(ProcessBuilder.Redirect.appendTo(updaterLogFile));

        int exitCode = pb.start().waitFor();

        Files.delete(updaterExecPath);
        Files.deleteIfExists(baseDir.resolve(LAUNCHER_PROPERTIES));

        return exitCode;
    }

    private int executeApplication(String launchMode, List<String> launcherArgs, String launcherLogFile) throws Exception {

        // Read updated launcher properties
        properties = readLauncherProperties();

        String jarFileName = properties.getProperty(PROPERTY_JAR);
        Path jarFile = baseDir.resolve(jarFileName);

        String logFileName = properties.getProperty(PROPERTY_LOGFILE);
        Path logFile = baseDir.resolve(logFileName);

        List<String> javaOptions = new ArrayList<>();
        String javaOpts = properties.getProperty(PROPERTY_JAVA_OPTS);
        if (javaOpts != null && !javaOpts.trim().isEmpty()) {
            String[] javaOptsArray = javaOpts.replaceAll("\"", "").split(" ");
            for (String opt : javaOptsArray) {
                if (opt != null && !opt.trim().isEmpty()) {
                    javaOptions.add(opt);
                }
            }
        }

        // call java -jar application-?.jar
        List<String> command = new ArrayList<>();
        command.add(getJavaExecutable().toString());
        command.addAll(javaOptions);
        if (debugMode) {
            command.add(DEBUG_JAVA_OPTS);
        }
        command.add("-D" + getName().toLowerCase() + ".log.file=" + logFile.toString());
        command.add("-jar");
        command.add(jarFile.toString());
        command.add("--option");
        command.add("quadrige3.launch.mode");
        command.add(launchMode);
        command.add("--option");
        command.add(getName().toLowerCase() + ".basedir");
        command.add(baseDir.toString());
        command.add("--option");
        command.add("config.path"); // This option is used to override org.nuiton.config.ApplicationConfig.getConfigPath()
        command.add(baseDir.resolve(CONFIG_DIR).toString());
        command.addAll(launcherArgs);
        ProcessBuilder pb = new ProcessBuilder(command);

        pb.inheritIO();
        // redirect streams to specified log file
        if (launcherLogFile != null) {
            File redirectLogFile = new File(baseDir.toFile(), launcherLogFile);
            pb.redirectOutput(ProcessBuilder.Redirect.to(redirectLogFile));
            pb.redirectError(ProcessBuilder.Redirect.appendTo(redirectLogFile));
        }

        // Execute process
        return pb.start().waitFor();
    }

    private Properties readLauncherProperties() throws Exception {

        Path localLauncherFile = baseDir.resolve(LAUNCHER_PROPERTIES);
        Path applicationLauncherFile = baseDir.resolve(APP_DIR).resolve(LAUNCHER_PROPERTIES);

        if (!Files.exists(localLauncherFile) && !Files.exists(applicationLauncherFile)) {
            throw new IOException(getString("launcher.fileNotExists", LAUNCHER_PROPERTIES));
        }

        // Read application file
        Properties applicationProperties = new Properties();
        if (Files.isReadable(applicationLauncherFile)) {
            try (Reader reader = new FileReader(applicationLauncherFile.toFile())) {
                applicationProperties.load(reader);
            }
        }

        Properties launcherProperties = new Properties(applicationProperties);
        if (Files.isReadable(localLauncherFile)) {
            try (Reader reader = new FileReader(localLauncherFile.toFile())) {
                launcherProperties.load(reader);
            }
        }

        // Assert properties exists
        if (!oldAppFound) assertPropertyExists(launcherProperties, PROPERTY_NAME);
        assertPropertyExists(launcherProperties, PROPERTY_JAR);
        assertPropertyExists(launcherProperties, PROPERTY_LOGFILE);

        return launcherProperties;
    }

    private void assertPropertyExists(Properties properties, String propertyName) {
        String property = properties.getProperty(propertyName);
        if (property == null || property.trim().length() == 0)
            throw new NullPointerException(getString("launcher.propertyNotFound", propertyName, LAUNCHER_PROPERTIES));
    }

    private Path getJavaExecutable() {

        Path javaHome = baseDir.resolve("jre");
        return javaHome.resolve("bin").resolve("javaw.exe");
    }

    private static String getString(String resourceName, Object... params) {
        return MessageFormat.format(bundle.getString(resourceName), params);
    }

    private static class UTF8Control extends ResourceBundle.Control {

        @Override
        public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IOException {
            // The below is a copy of the default implementation.
            String bundleName = toBundleName(baseName, locale);
            String resourceName = toResourceName(bundleName, "properties");
            ResourceBundle bundle = null;
            InputStream stream = null;
            if (reload) {
                URL url = loader.getResource(resourceName);
                if (url != null) {
                    URLConnection connection = url.openConnection();
                    if (connection != null) {
                        connection.setUseCaches(false);
                        stream = connection.getInputStream();
                    }
                }
            } else {
                stream = loader.getResourceAsStream(resourceName);
            }
            if (stream != null) {
                try {
                    // Only this line is changed to make it to read properties files as UTF-8.
                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
                } finally {
                    stream.close();
                }
            }
            return bundle;
        }

    }
}
